﻿Public Class Herramientas
    Public Shared path As String

    Public Shared Function llena_ds(ByVal xml_data As String) As DataSet
        Dim ds As New DataSet
        Dim buf() As Byte = Encoding.UTF8.GetBytes(xml_data)
        Dim ms As New System.IO.MemoryStream(buf, 0, buf.Length)
        ds.ReadXml(ms)

        Return ds
    End Function

    Public Shared Function llena_ds(ByVal xml_data As String, ByVal tipo As String) As DataSet
        Dim ds As Object = Nothing

        Select Case tipo

            Case "Call_New_RQ"
                ds = New Call_New_RQ
            Case "Call_New_RS"
                ds = New Call_New_RS

            Case "Call_Finish_RQ"
                ds = New Call_Finish_RQ
            Case "Call_Finish_RS"
                ds = New Call_Finish_RS

            Case "Call_AddReservations_RQ"
                ds = New Call_AddReservations_RQ
            Case "Call_AddReservations_RS"
                ds = New Call_AddReservations_RS

            Case "Call_getCalls_RQ"
                ds = New Call_getCalls_RQ
            Case "Call_getCalls_RS"
                ds = New Call_getCalls_RS

            Case "Call_GetOne_RQ"
                ds = New Call_GetOne_RQ
            Case "Call_GetOne_RS"
                ds = New Call_GetOne_RS
            Case "Call_Log_Insert_RQ"
                ds = New Call_Log_Insert_RQ
            Case "Call_Log_Insert_RS"
                ds = New Call_Log_Insert_RS
            Case "Call_Log_Get_RQ"
                ds = New Call_Log_Get_RQ
            Case "Call_Log_Get_RS"
                ds = New Call_Log_Get_RS
            Case "Call_Log_Detail_RQ"
                ds = New Call_Log_Detail_RQ
            Case "Call_Log_Detail_RS"
                ds = New Call_Log_Detail_RS
            Case "Call_Hotel_GetAvailability_RQ"
                ds = New Call_Hotel_GetAvailability_RQ
            Case "Call_Hotel_GetAvailability_RS"
                ds = New Call_Hotel_GetAvailability_RS
            Case "Call_PMS_Retry_RQ"
                ds = New Call_PMS_Retry_RQ
            Case "Call_PMS_Retry_RS"
                ds = New Call_PMS_Retry_RS
            Case "Call_ReportProduction_RQ"
                ds = New Call_ReportProduction_RQ
            Case "Call_ReportProduction_RS"
                ds = New Call_ReportProduction_RS
            Case "Call_SegmentsCompany_Insert_RQ"
                ds = New Call_SegmentsCompany_Insert_RQ
            Case "Call_SegmentsCompany_Insert_RS"
                ds = New Call_SegmentsCompany_Insert_RS
            Case "Call_SegmentsCompany_List_RQ"
                ds = New Call_SegmentsCompany_List_RQ
            Case "Call_SegmentsCompany_List_RS"
                ds = New Call_SegmentsCompany_List_RS
            Case "Call_SegmentsCompany_Update_RQ"
                ds = New Call_SegmentsCompany_Update_RQ
            Case "Call_SegmentsCompany_Update_RS"
                ds = New Call_SegmentsCompany_Update_RS
            Case "Call_SegmentsCompany_Delete_RQ"
                ds = New Call_SegmentsCompany_Delete_RQ
            Case "Call_SegmentsCompany_Delete_RS"
                ds = New Call_SegmentsCompany_Delete_RS

            Case Else
                ' MsgBox("llena_ds - tipo no definido")

        End Select

        Dim buf() As Byte = Encoding.UTF8.GetBytes(xml_data)
        Dim ms As New System.IO.MemoryStream(buf, 0, buf.Length)
        ds.ReadXml(ms)

        Return ds
    End Function

    Public Shared Sub logmsg(message As String)
        Try
            Dim objWriter As System.IO.StreamWriter = New System.IO.StreamWriter(path & "\logs\" & Now.ToString("yyyMMdd") & ".txt", True)
            objWriter.WriteLine(Now & ": " & vbCrLf & message & vbCrLf)
            objWriter.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class
