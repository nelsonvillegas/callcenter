﻿Imports System
Imports System.Web.Services.Protocols

Public Class SoapAuthHeader
    Inherits SoapHeader : Implements IDisposable

    Private _usr As String = ""
    Private _pass As String = ""
    Public Property usr() As String
        Get
            Return _usr
        End Get
        Set(ByVal value As String)
            _usr = value
        End Set
    End Property
    Public Property pass() As String
        Get
            Return _pass
        End Get
        Set(ByVal value As String)
            _pass = value
        End Set
    End Property

    Public Sub Dispose() Implements System.IDisposable.Dispose
        Me._usr = Nothing
        Me._pass = Nothing
    End Sub

End Class
