﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

'' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
'' <System.Web.Script.Services.ScriptService()> _
'<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
'<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
'<ToolboxItem(False)> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
Public Class adminCalls
    Inherits System.Web.Services.WebService

    Public AuthHeader As SoapAuthHeader

    <WebMethod(), SoapHeader("AuthHeader")> _
    Public Function SubmitXML(ByVal xml As String) As String
        Try
            Dim auth As Authentication = New Authentication()
            If Not auth.Authenticate(AuthHeader) Then
                Return "no autentico"
            End If

            Dim doc As New System.Xml.XmlDocument
            doc.LoadXml(xml)

            Dim res As System.Xml.XmlDocument = SubmitXML(doc)
            Return res.DocumentElement.OuterXml
        Catch ex As Exception
            'Return ex.Message '"error, RQ no definido"
            Herramientas.logmsg(ex.Message)
        End Try
    End Function

    Private Function submitXML(ByVal docRQ As System.Xml.XmlDocument) As System.Xml.XmlDocument
        Dim req_call As Object = Herramientas.llena_ds(docRQ.DocumentElement.OuterXml, docRQ.DocumentElement.Name)
        Dim res_call As Object
        Herramientas.path = Context.Request.PhysicalApplicationPath

        Select Case docRQ.DocumentElement.Name
            Case "Call_New_RQ"
                Dim RQ_DS As Call_new_RQ = req_call
                Dim r As Call_new_RQ.CallRow = RQ_DS.Tables("Call").Rows(0)

                Dim id As Integer
                If r.Create Then
                    Dim idOperador As Integer = r.Item("idOperador")
                    id = (New llamadasTableAdapters.uvcc_llamadaTableAdapter).sp_uvcc_nuevo(idOperador)
                Else
                    id = -1
                End If

                Dim RS_DS As New Call_New_RS
                Dim r2 As Call_New_RS.CallRow = RS_DS.Tables("Call").NewRow
                r2.Item("idCall") = id
                RS_DS.Tables("Call").Rows.Add(r2)

                res_call = RS_DS

            Case "Call_Finish_RQ"
                Dim RQ_DS As Call_Finish_RQ = req_call
                Dim r As Call_Finish_RQ.CallRow = RQ_DS.Tables("Call").Rows(0)

                Dim correcto As Boolean = True

                'Dim rows As Integer = (New llamadasTableAdapters.uvcc_llamadaTableAdapter).sp_uvcc_finish(r.Item("idCall"), r.Item("idOperador"), r.Item("nombreCliente"), r.Item("sexo"), r.Item("ayuda"), r.Item("telefono"), r.Item("correo"), r.Item("comentarios"), r.Item("paquete"), r.Item("inicio"), r.Item("fin"), r.Item("nombreHotel"))
                Dim rows As Integer = (New llamadasTableAdapters.uvcc_llamadaTableAdapter).sp_uvcc_finish(r.Item("idCall"), r.Item("idOperador"), r.Item("nombreCliente"), r.Item("sexo"), r.Item("ayuda"), r.Item("telefono"), r.Item("correo"), r.Item("comentarios"), r.Item("paquete"), r.Item("inicio"), r.Item("fin"), r.Item("nombreHotel"))


                Dim RS_DS As New Call_Finish_RS
                Dim r2 As Call_Finish_RS.CallRow = RS_DS.Tables("Call").NewRow
                r2.Item("Correcto") = correcto
                RS_DS.Tables("Call").Rows.Add(r2)

                res_call = RS_DS

            Case "Call_AddReservations_RQ"
                Dim RQ_DS As Call_AddReservations_RQ = req_call
                Dim correcto As Boolean = True

                'begin tran
                Dim ta As New llamadasTableAdapters.uvcc_llamadaTableAdapter
                ta.Connection.Open()
                ta.Transaction = ta.Connection.BeginTransaction(IsolationLevel.ReadUncommitted, "reservationsTran")

                Try
                    'add reservation
                    For Each r As Call_AddReservations_RQ.ReservationRow In RQ_DS.Tables("Reservation").Rows
                        Dim idCallReservation As Integer
                        Dim idEmp As Integer = 0
                        If Not IsDBNull(r.Item("id_Empresa")) Then idEmp = CInt(r.Item("id_Empresa"))
                        Dim idMedio As Integer = 0
                        If Not IsDBNull(r.Item("id_Medio")) Then idMedio = CInt(r.Item("id_Medio"))
                        Dim idMedioPromocion As Integer = 0
                        If Not IsDBNull(r.Item("id_MedioPromocion")) Then idMedioPromocion = CInt(r.Item("id_MedioPromocion"))
                        Dim idSegmento As Integer = 0
                        If Not IsDBNull(r.Item("id_Segmento")) Then idSegmento = CInt(r.Item("id_Segmento"))

                        idCallReservation = ta.sp_uvcc_addReservation(r.Item("idCall"), r.Item("noReservacion"), r.Item("rubro"), idEmp, idMedio, idMedioPromocion, idSegmento)

                        If idCallReservation = Nothing Then Throw New Exception("error!!!!")
                    Next

                    ta.Transaction.Commit()
                Catch ex As Exception
                    ta.Transaction.Rollback()
                    correcto = False
                End Try

                'response
                Dim RS_DS As New Call_AddReservations_RS
                Dim r2 As Call_AddReservations_RS.ReservationRow = RS_DS.Tables("Reservation").NewRow
                r2.Item("Correcto") = correcto
                RS_DS.Tables("Reservation").Rows.Add(r2)

                res_call = RS_DS

            Case "Call_getCalls_RQ"
                Dim RQ_DS As Call_getCalls_RQ = req_call
                Dim r As Call_getCalls_RQ.CallsRow = RQ_DS.Tables("Calls").Rows(0)

                Dim dt As DataTable = Nothing
                If Not r.idOperador = Nothing AndAlso r.idOperador > 0 Then
                    Dim ini As Object
                    Dim fin As Object
                    'If r.ini = "" Then ini = Nothing Else ini = DateTime.ParseExact(r.ini, CapaPresentacion.Common.DateDataFormat, Nothing)
                    'If r.fin = "" Then fin = Nothing Else fin = DateTime.ParseExact(r.fin, CapaPresentacion.Common.DateDataFormat, Nothing)
                    If r.ini = "" Then ini = Nothing Else ini = DateTime.ParseExact(r.ini, "yyyyMMdd", Nothing)
                    If r.fin = "" Then fin = Nothing Else fin = DateTime.ParseExact(r.fin, "yyyyMMdd", Nothing)


                    dt = (New llamadasTableAdapters.uvcc_llamadaTableAdapter).GetData_getCalls(r.idOperador, ini, fin)
                End If

                Dim RS_DS As New Call_getCalls_RS

                Dim r2 As Call_getCalls_RS.CallsRow = RS_DS.Tables("Calls").NewRow
                r2.Item("Calls_Id") = 0
                RS_DS.Tables("Calls").Rows.Add(r2)

                For Each dt_r As DataRow In dt.Rows
                    Dim r3 As Call_getCalls_RS.CallRow = RS_DS.Tables("Call").NewRow
                    r3.Item("ayuda") = dt_r.Item("ayuda")
                    r3.Item("comentarios") = dt_r.Item("comentarios")
                    r3.Item("correo") = dt_r.Item("correo")
                    r3.Item("fin") = dt_r.Item("fin")
                    r3.Item("idCall") = dt_r.Item("idCall")
                    r3.Item("idOperador") = dt_r.Item("idOperador")
                    r3.Item("inicio") = dt_r.Item("inicio")
                    r3.Item("nombreCliente") = dt_r.Item("nombreCliente")
                    r3.Item("paquete") = dt_r.Item("paquete")
                    r3.Item("sexo") = dt_r.Item("sexo")
                    r3.Item("telefono") = dt_r.Item("telefono")
                    r3.Item("duracion") = dt_r.Item("duracion")
                    r3.Item("reservaciones") = dt_r.Item("reservaciones")
                    If dt.Columns.Contains("Operador") Then r3.Item("Operador") = dt_r.Item("Operador")
                    r3.SetParentRow(r2)
                    RS_DS.Tables("Call").Rows.Add(r3)
                Next

                res_call = RS_DS

            Case "Call_GetOne_RQ"
                Dim RQ_DS As Call_GetOne_RQ = req_call
                Dim r As Call_GetOne_RQ.CallRow = RQ_DS.Tables("Call").Rows(0)

                Dim dt As DataTable = Nothing
                Dim RS_DS As New Call_GetOne_RS

                If Not r.idCall = Nothing AndAlso r.idCall > 0 Then
                    dt = (New llamadasTableAdapters.uvcc_llamadaTableAdapter).GetData_GetOneCall(r.idCall)
                    'Dim r2 As Call_GetOne_RS.CallRow = RS_DS.Tables("Call").NewRow
                    ''r2.Item("Calls_Id") = 0
                    'RS_DS.Tables("Call").Rows.Add(r2)

                    'For Each dt_r As DataRow In dt.Rows
                    Dim r3 As Call_GetOne_RS.CallRow = RS_DS.Tables("Call").NewRow
                    If dt.Rows(0).Item("ayuda") IsNot System.DBNull.Value Then r3.Item("ayuda") = dt.Rows(0).Item("ayuda") Else r3.Item("ayuda") = 0
                    r3.Item("comentarios") = dt.Rows(0).Item("comentarios")
                    r3.Item("correo") = dt.Rows(0).Item("correo")
                    If dt.Rows(0).Item("fin") IsNot System.DBNull.Value Then r3.Item("fin") = dt.Rows(0).Item("fin") Else r3.Item("fin") = dt.Rows(0).Item("inicio")
                    r3.Item("idCall") = dt.Rows(0).Item("idCall")
                    r3.Item("idOperador") = dt.Rows(0).Item("idOperador")
                    r3.Item("inicio") = dt.Rows(0).Item("inicio")
                    r3.Item("nombreCliente") = dt.Rows(0).Item("nombreCliente")
                    If dt.Rows(0).Item("paquete") IsNot System.DBNull.Value Then r3.Item("paquete") = dt.Rows(0).Item("paquete") Else r3.Item("paquete") = 0
                    r3.Item("sexo") = dt.Rows(0).Item("sexo")
                    r3.Item("telefono") = dt.Rows(0).Item("telefono")
                    'r3.Item("duracion") = dt.Rows(0).Item("duracion")
                    'r3.Item("reservaciones") = dt.Rows(0).Item("reservaciones")
                    'r3.SetParentRow(r2)
                    RS_DS.Tables("Call").Rows.Add(r3)
                    'Next
                Else
                    Dim r3 As Call_GetOne_RS.CallRow = RS_DS.Tables("Call").NewRow
                    r3.Item("idCall") = 0
                    r3.Item("inicio") = Now
                    r3.Item("fin") = Now
                    RS_DS.Tables("Call").Rows.Add(r3)
                End If

                Dim dt2 As DataTable = Nothing
                If Not IsNothing(r.idCall) Then
                    If r.IsnoReservacionNull Then
                        dt2 = (New llamadasTableAdapters.uvcc_reservoTableAdapter).GetData_getReservations(r.idCall, Nothing)
                    Else
                        dt2 = (New llamadasTableAdapters.uvcc_reservoTableAdapter).GetData_getReservations(r.idCall, r.noReservacion)
                    End If

                End If

                Dim r4 As Call_GetOne_RS.ReservationsRow = RS_DS.Tables("Reservations").NewRow
                'r2.Item("Calls_Id") = 0
                RS_DS.Tables("Reservations").Rows.Add(r4)

                For Each dt_r As DataRow In dt2.Rows
                    Dim r5 As Call_GetOne_RS.ReservationRow = RS_DS.Tables("Reservation").NewRow
                    r5.Item("idReservo") = dt_r.Item("idReservo")
                    r5.Item("idCall") = dt_r.Item("idCall")
                    r5.Item("noReservacion") = dt_r.Item("noReservacion")
                    r5.Item("rubro") = dt_r.Item("rubro")
                    r5.Item("id_Empresa") = dt_r.Item("id_Empresa")
                    r5.Item("id_Medio") = dt_r.Item("id_Medio")
                    r5.Item("id_MedioPromocion") = dt_r.Item("id_MedioPromocion")
                    r5.Item("id_Segmento") = dt_r.Item("id_Segmento")
                    Dim pend_datos, pend_status As Boolean
                    pend_status = False
                    pend_datos = False
                    If Not dt_r.IsNull("ultima_dat_tipo") Then
                        pend_datos = IIf(dt_r("ultima_dat_estatus") <> 1, True, False)
                    End If
                    If Not dt_r.IsNull("ultima_est_tipo") Then
                        pend_datos = IIf(dt_r("ultima_est_estatus") <> 1, True, False)
                    End If
                    r5.PMS_OperacionesPendientes = IIf(pend_datos Or pend_status, True, False)

                    r5.SetParentRow(r4)
                    RS_DS.Tables("Reservation").Rows.Add(r5)
                Next
                '



                res_call = RS_DS
            Case "Call_Log_Insert_RQ"
                Dim RQ_DS As Call_Log_Insert_RQ = req_call
                Dim r As Call_Log_Insert_RQ.LogRow = RQ_DS.Log(0)

                Dim correcto As Boolean = True

                Dim rows As Integer = (New llamadasTableAdapters.uvcc_logTableAdapter).Insert(r.idOperador, r.NoReservacion, r.Operation, r.Data_Before, r.Data_After, Date.Now, r.Comments)
                If rows = 0 Then correcto = False
                Dim RS_DS As New Call_Log_Insert_RS
                Dim r2 As Call_Log_Insert_RS.ResponseRow = RS_DS.Response.NewRow
                r2.Success = correcto
                RS_DS.Response.AddResponseRow(r2)

                res_call = RS_DS

            Case "Call_Log_Get_RQ"
                Dim RQ_DS As Call_Log_Get_RQ = req_call
                Dim RS_DS As New Call_Log_Get_RS
                Dim idOperador As Integer = -1
                If Not RQ_DS.Log(0).IsidOperadorNull Then idOperador = RQ_DS.Log(0).idOperador
                Dim Operation As Integer = -1
                If Not RQ_DS.Log(0).IsOperationNull Then Operation = RQ_DS.Log(0).Operation
                Dim DateBegin As Date = Nothing
                If Not RQ_DS.Log(0).IsDateBeginNull Then DateBegin = RQ_DS.Log(0).DateBegin
                Dim DateEnd As Date = Nothing
                If Not RQ_DS.Log(0).IsDateEndNull Then DateEnd = RQ_DS.Log(0).DateEnd
                Dim noReservacion As String = ""
                If Not RQ_DS.Log(0).IsnoReservacionNull Then noReservacion = RQ_DS.Log(0).noReservacion
                Dim data As DataTable = (New llamadasTableAdapters.uvcc_logTableAdapter).GetData_GetLog(IIf(idOperador <> -1, idOperador, Nothing), IIf(DateBegin.Year > 2000, DateBegin, Nothing), IIf(DateEnd.Year > 2000, DateEnd, Nothing), IIf(noReservacion.Trim <> "", noReservacion, Nothing), IIf(Operation <> -1, Operation, Nothing))
                'Dim data As DataTable = (New llamadasTableAdapters.uvcc_logTableAdapter).GetData_GetLog(RQ_DS.Log(0).idOperador, RQ_DS.Log(0).DateBegin, RQ_DS.Log(0).DateEnd, RQ_DS.Log(0).noReservacion, RQ_DS.Log(0).Operation)
                For Each dt_r As DataRow In data.Rows
                    Dim r As Call_Log_Get_RS.LogRow = RS_DS.Log.NewRow
                    r.idLog = dt_r("idLog")
                    r.idOperador = dt_r("idOperador")
                    If Not dt_r.IsNull("Operador") Then r._Operator = dt_r("Operador")
                    r.noReservacion = dt_r("noReservacion")
                    r.DateOperation = dt_r("RegisterDate")
                    r.Operation = dt_r("Operation")
                    r.Comments = dt_r("Comments")
                    RS_DS.Log.AddLogRow(r)
                Next
                res_call = RS_DS
            Case "Call_Log_Detail_RQ"
                Dim RQ_DS As Call_Log_Detail_RQ = req_call
                Dim RS_DS As New Call_Log_Detail_RS
                Dim idLog As Integer = -1
                If Not RQ_DS.Log(0).IsidLogNull Then idLog = RQ_DS.Log(0).idLog
                If idLog <> -1 Then
                    Dim data As DataTable = (New llamadasTableAdapters.uvcc_logTableAdapter).GetData_GetLogDetail(idLog)
                    For Each dt_r As DataRow In data.Rows
                        Dim r As Call_Log_Detail_RS.LogRow = RS_DS.Log.NewRow
                        r.idLog = dt_r("idLog")
                        r.idOperador = dt_r("idOperador")
                        If Not dt_r.IsNull("Operador") Then r._Operator = dt_r("Operador")
                        r.noReservacion = dt_r("noReservacion")
                        r.DateOperation = dt_r("RegisterDate")
                        r.Operation = dt_r("Operation")
                        r.Comments = dt_r("Comments")
                        r.DataBefore = dt_r("Data_Before")
                        r.DataAfter = dt_r("Data_After")
                        RS_DS.Log.AddLogRow(r)
                    Next
                End If
                res_call = RS_DS
            Case "Call_PMS_Retry_RQ"
                res_call = PMS_Retry(req_call)
            Case "Call_Hotel_GetAvailability_RQ"
                res_call = Hotel_GetAvailability(req_call)
            Case "Call_ReportProduction_RQ"
                res_call = ReportProduction(req_call)
            Case "Call_SegmentsCompany_Insert_RQ"
                res_call = SegmentsCompanyInsert(req_call)
            Case "Call_SegmentsCompany_List_RQ"
                res_call = SegmentsCompanyGetList(req_call)
            Case "Call_SegmentsCompany_Update_RQ"
                res_call = SegmentsCompanyUpdate(req_call)
            Case "Call_SegmentsCompany_Delete_RQ"
                res_call = SegmentsCompanyDelete(req_call)
        End Select

        Dim docRS As New System.Xml.XmlDocument
        docRS.LoadXml(res_call.GetXml)
        Return docRS
    End Function

    Function PMS_Retry(req_Call As Call_PMS_Retry_RQ) As Call_PMS_Retry_RS
        Dim RQ_DS As Call_PMS_Retry_RQ = req_Call
        Dim r As Call_PMS_Retry_RQ.RequestRow = RQ_DS.Request(0)

        Dim correcto As Boolean = True

        Dim rows As Integer = (New llamadasTableAdapters.uvcc_reservoTableAdapter).PMS_Retry(r.noReservacion)
        If rows = 0 Then correcto = False
        Dim RS_DS As New Call_PMS_Retry_RS
        Dim r2 As Call_PMS_Retry_RS.ResponseRow = RS_DS.Response.NewRow
        r2.Success = correcto
        RS_DS.Response.AddResponseRow(r2)
        Return RS_DS
    End Function

    Function Hotel_GetAvailability(req_Call As Call_Hotel_GetAvailability_RQ) As Call_Hotel_GetAvailability_RS
        Dim RQ_DS As Call_Hotel_GetAvailability_RQ = req_Call
        Dim RS_DS As New Call_Hotel_GetAvailability_RS
        Dim idHotel As Integer = -1
        If Not RQ_DS.Hotel(0).IsidHotelNull Then idHotel = RQ_DS.Hotel(0).idHotel
        If idHotel <> -1 Then
            Dim data As DataSet = GetHotelAvailability(RQ_DS.Hotel(0).DateBegin, RQ_DS.Hotel(0).DateEnd, idHotel)
            If Not data Is Nothing AndAlso data.Tables.Count = 2 Then
                For Each dt_r As DataRow In data.Tables(0).Rows
                    Dim r As Call_Hotel_GetAvailability_RS.RoomsRow = RS_DS.Rooms.NewRow
                    r.idRoom = dt_r("idtipohabitacion_hotel")
                    r.Name = dt_r("nombrehabitacion")
                    RS_DS.Rooms.AddRoomsRow(r)
                Next
                For Each dt_r As DataRow In data.Tables(1).Rows
                    Dim r As Call_Hotel_GetAvailability_RS.AvailabilityRow = RS_DS.Availability.NewRow
                    r.idRoom = dt_r("idtipohabitacion_hotel")
                    r._Date = dt_r("fecha")
                    r.Inventary = dt_r("habitaciones")
                    If dt_r("estatus") <> "" And dt_r("estatus") <> "O" Then
                        r.Availables = "X"
                    Else
                        r.Availables = dt_r("dispo")
                    End If

                    r.Reserved = dt_r("reservasroom")
                    RS_DS.Availability.AddAvailabilityRow(r)
                Next
            End If
        End If
        Return RS_DS
    End Function

    Function GetHotelAvailability(ByVal inicia As String, ByVal termina As String, ByVal idhotel As Global.System.Nullable(Of Integer)) As DataSet
        Dim adapter As New SqlClient.SqlDataAdapter
        Dim sqlComm As New SqlClient.SqlCommand
        sqlComm = New Global.System.Data.SqlClient.SqlCommand()
        sqlComm.Connection = New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OzUnivisitConnectionString").ConnectionString)
        sqlComm.CommandText = "dbo.sp_uvcc_getHotelAvailability"
        sqlComm.CommandType = Global.System.Data.CommandType.StoredProcedure
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@RETURN_VALUE", Global.System.Data.SqlDbType.Int, 4, Global.System.Data.ParameterDirection.ReturnValue, 10, 0, Nothing, Global.System.Data.DataRowVersion.Current, False, Nothing, "", "", ""))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@inicia", Global.System.Data.SqlDbType.[Char], 10, Global.System.Data.ParameterDirection.Input, 0, 0, Nothing, Global.System.Data.DataRowVersion.Current, False, Nothing, "", "", ""))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@termina", Global.System.Data.SqlDbType.[Char], 10, Global.System.Data.ParameterDirection.Input, 0, 0, Nothing, Global.System.Data.DataRowVersion.Current, False, Nothing, "", "", ""))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idhotel", Global.System.Data.SqlDbType.Int, 4, Global.System.Data.ParameterDirection.Input, 10, 0, Nothing, Global.System.Data.DataRowVersion.Current, False, Nothing, "", "", ""))

        adapter.SelectCommand = sqlComm
        If (inicia Is Nothing) Then
            adapter.SelectCommand.Parameters(1).Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters(1).Value = CType(inicia, String)
        End If
        If (termina Is Nothing) Then
            adapter.SelectCommand.Parameters(2).Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters(2).Value = CType(termina, String)
        End If
        If (idhotel.HasValue = True) Then
            adapter.SelectCommand.Parameters(3).Value = CType(idhotel.Value, Integer)
        Else
            adapter.SelectCommand.Parameters(3).Value = Global.System.DBNull.Value
        End If
        Dim ds As DataSet = New DataSet
        adapter.Fill(ds)
        Return ds
    End Function

    Private Function ReportProduction(req_Call As Call_ReportProduction_RQ) As Call_ReportProduction_RS
        Dim RQ_DS As Call_ReportProduction_RQ = req_Call
        Dim RS_DS As New Call_ReportProduction_RS
        Dim data As DataSet = (New AccesoDatos).GetDataReportProduction(RQ_DS)
        If Not data Is Nothing AndAlso data.Tables.Count = 1 Then
            For Each dt_r As DataRow In data.Tables(0).Rows
                Dim r As Call_ReportProduction_RS.ResponseRow = RS_DS.Response.NewRow
                r.idHotel = dt_r("idhotel")
                r.Hotel = dt_r("Hotel")
                If req_Call.Request(0).ByAgent Then
                    r.Agent = dt_r("Agente")
                    r.AgentEmail = dt_r("AgenteEmail")
                End If
                r.SegmentCompany = dt_r("EmpresaSegmento")
                r.Segment = dt_r("Segmento")
                ' r.NoReservation = dt_r("NoReservacion")
                r.Reservations = dt_r("Reservas")
                r.Rooms = dt_r("Cuartos")
                r.Nights = dt_r("Noches")
                r.RoomsByNight = dt_r("CuartosNoche")
                r.Persons = dt_r("Personas")
                r.Adults = dt_r("Adultos")
                r.Childs = dt_r("Niños")
                RS_DS.Response.AddResponseRow(r)
            Next
        End If
        Return RS_DS
    End Function

    Private Function SegmentsCompanyInsert(RQ_DS As Call_SegmentsCompany_Insert_RQ) As Call_SegmentsCompany_Insert_RS
        Dim r As Call_SegmentsCompany_Insert_RQ.RequestRow = RQ_DS.Request.Rows(0)
        Dim id As Integer
        Dim code As String = ""
        Dim idMedium As Integer = Nothing
        If Not r.IsidMediumNull Then idMedium = r.idMedium
        Dim idCorporate As Integer = Nothing
        If Not r.IsidCorporateNull Then idCorporate = r.idCorporate
        Dim Name As String = Nothing
        If Not r.IsNameNull Then Name = r.Name
        Dim idSegment As Integer = Nothing
        If Not r.IsidSegmentNull Then idSegment = r.idSegment
        Dim Address As String
        If Not r.IsAddressNull Then Address = r.Address
        Dim City As String
        If Not r.IsCityNull Then City = r.City
        Dim PostalCode As String
        If Not r.IsPostalCodeNull Then PostalCode = r.PostalCode
        Dim Phone As String = Nothing
        If Not r.IsPhoneNull Then Phone = r.Phone
        Dim RFC As String = Nothing
        If Not r.IsRFCNull Then RFC = r.RFC
        Dim Type As String = Nothing
        If Not r.IsTypeNull Then Type = r.Type

        id = (New llamadasTableAdapters.uvcc_empresasTableAdapter).sp_uvcc_empresas_insert(code, idMedium, idCorporate, Name, idSegment, Address, City, PostalCode, Phone, RFC, Type)
        Herramientas.logmsg("id returned:" & id & " code:" & code)
        Dim RS_DS As New Call_SegmentsCompany_Insert_RS
        If id > 0 Then
            Dim r2 As Call_SegmentsCompany_Insert_RS.ResponseRow = RS_DS.Response.NewResponseRow
            r2.Code = code
            r2.idCompany = id
            RS_DS.Response.Rows.Add(r2)
        Else
            Dim r2 As Call_SegmentsCompany_Insert_RS.ErrorRow = RS_DS._Error.NewErrorRow
            r2.Message = code
            RS_DS._Error.Rows.Add(r2)
        End If
        Return RS_DS
    End Function

    Private Function SegmentsCompanyUpdate(RQ_DS As Call_SegmentsCompany_Update_RQ) As Call_SegmentsCompany_Update_RS
        Dim r As Call_SegmentsCompany_Update_RQ.RequestRow = RQ_DS.Request.Rows(0)
        Dim id As Integer = r.idCompany

        Dim idMedium As Integer = Nothing
        If Not r.IsidMediumNull Then idMedium = r.idMedium
        Dim idCorporate As Integer = Nothing
        If Not r.IsidCorporateNull Then idCorporate = r.idCorporate
        Dim Name As String = Nothing
        If Not r.IsNameNull Then Name = r.Name
        Dim idSegment As Integer = Nothing
        If Not r.IsidSegmentNull Then idSegment = r.idSegment
        Dim Address As String
        If Not r.IsAddressNull Then Address = r.Address
        Dim City As String
        If Not r.IsCityNull Then City = r.City
        Dim PostalCode As String
        If Not r.IsPostalCodeNull Then PostalCode = r.PostalCode
        Dim Phone As String = Nothing
        If Not r.IsPhoneNull Then Phone = r.Phone
        Dim RFC As String = Nothing
        If Not r.IsRFCNull Then RFC = r.RFC
        Dim Type As String = Nothing
        If Not r.IsTypeNull Then Type = r.Type

        Dim res As Boolean = (New AccesoDatos).SegmentsCompanyUpdate(id, idMedium, idCorporate, Name, idSegment, Address, City, PostalCode, Phone, RFC, Type)
        Dim RS_DS As New Call_SegmentsCompany_Update_RS
        If res Then
            Dim r2 As Call_SegmentsCompany_Update_RS.ResponseRow = RS_DS.Response.NewResponseRow
            r2.Status = 1
            RS_DS.Response.Rows.Add(r2)
        Else
            Dim r2 As Call_SegmentsCompany_Update_RS.ErrorRow = RS_DS._Error.NewErrorRow
            r2.Message = "No se pudo actualizar"
            RS_DS._Error.Rows.Add(r2)
        End If
        Return RS_DS
    End Function

    Private Function SegmentsCompanyDelete(RQ_DS As Call_SegmentsCompany_Delete_RQ) As Call_SegmentsCompany_Delete_RS
        Dim r As Call_SegmentsCompany_Delete_RQ.RequestRow = RQ_DS.Request.Rows(0)
        Dim id As Integer = r.idCompany


        Dim res As Boolean = (New AccesoDatos).SegmentsCompanyDelete(id)
        Dim RS_DS As New Call_SegmentsCompany_Delete_RS
        If res Then
            Dim r2 As Call_SegmentsCompany_Delete_RS.ResponseRow = RS_DS.Response.NewResponseRow
            r2.Status = 1
            RS_DS.Response.Rows.Add(r2)
        Else
            Dim r2 As Call_SegmentsCompany_Delete_RS.ErrorRow = RS_DS._Error.NewErrorRow
            r2.Message = "No se pudo actualizar"
            RS_DS._Error.Rows.Add(r2)
        End If
        Return RS_DS
    End Function

    Private Function SegmentsCompanyGetList(RQ_DS As Call_SegmentsCompany_List_RQ) As Call_SegmentsCompany_List_RS
        Dim RS_DS As New Call_SegmentsCompany_List_RS
        Dim data As DataSet = (New AccesoDatos).GetSegmentsCompanyList(RQ_DS)
        If Not data Is Nothing AndAlso data.Tables.Count = 1 Then
            For Each dt_r As DataRow In data.Tables(0).Rows
                Dim r As Call_SegmentsCompany_List_RS.ResponseRow = RS_DS.Response.NewRow
                r.idCompany = dt_r("id_Empresa")
                r.Code = dt_r("Codigo")
                r.Name = dt_r("nombre")
                r.idSegment = dt_r("id_Segmento")
                r.Segment = dt_r("Segmento")
               
                RS_DS.Response.AddResponseRow(r)
            Next
        End If
        Return RS_DS
    End Function


End Class