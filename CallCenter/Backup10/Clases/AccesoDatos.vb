﻿Public Class AccesoDatos
    Public Function GetDataReportProduction(RQ As Call_ReportProduction_RQ) As DataSet
        Dim adapter As New SqlClient.SqlDataAdapter
        Dim sqlComm As New SqlClient.SqlCommand
        sqlComm = New Global.System.Data.SqlClient.SqlCommand()
        sqlComm.Connection = New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OzUnivisitConnectionString").ConnectionString)
        sqlComm.CommandText = "dbo.sp_uvcc_ReportProductionAgent"
        sqlComm.CommandType = Global.System.Data.CommandType.StoredProcedure
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@RETURN_VALUE", Global.System.Data.SqlDbType.Int, 4, Global.System.Data.ParameterDirection.ReturnValue, 10, 0, Nothing, Global.System.Data.DataRowVersion.Current, False, Nothing, "", "", ""))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@fechaini", Global.System.Data.SqlDbType.NVarChar, 8))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@fechafin", Global.System.Data.SqlDbType.NVarChar, 8))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idSegmento", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@SegmentoEmpresa", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idCorporativo", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@SourceList", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@SourceCodeList", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Agente", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Hotel", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@ByAgent", Global.System.Data.SqlDbType.Bit))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@id_Empresa", Global.System.Data.SqlDbType.Int))
        adapter.SelectCommand = sqlComm

        adapter.SelectCommand.Parameters(1).Value = CType(RQ.Request(0).DateBegin, String)
        adapter.SelectCommand.Parameters(2).Value = CType(RQ.Request(0).DateEnd, String)
        If Not RQ.Request(0).IsidSegmentNull Then adapter.SelectCommand.Parameters(3).Value = CType(RQ.Request(0).idSegment, Integer)
        If Not RQ.Request(0).IsSegmentCompanyNull Then adapter.SelectCommand.Parameters(4).Value = CType(RQ.Request(0).SegmentCompany, String)
        If Not RQ.Request(0).IsidCorporateNull Then adapter.SelectCommand.Parameters(5).Value = CType(RQ.Request(0).idCorporate, Integer)
        If Not RQ.Request(0).IsSourceListNull Then adapter.SelectCommand.Parameters(6).Value = CType(RQ.Request(0).SourceList, String)
        If Not RQ.Request(0).IsSourceCodeListNull Then adapter.SelectCommand.Parameters(7).Value = CType(RQ.Request(0).SourceCodeList, String)
        If Not RQ.Request(0).IsAgentNameNull Then adapter.SelectCommand.Parameters(8).Value = CType(RQ.Request(0).AgentName, String)
        If Not RQ.Request(0).IsHotelNameNull Then adapter.SelectCommand.Parameters(9).Value = CType(RQ.Request(0).HotelName, String)
        If Not RQ.Request(0).IsByAgentNull Then adapter.SelectCommand.Parameters(10).Value = CType(RQ.Request(0).ByAgent, Boolean)
        If Not RQ.Request(0).Isid_EmpresaNull Then adapter.SelectCommand.Parameters(11).Value = CType(RQ.Request(0).id_Empresa, Integer)
        Dim ds As DataSet = New DataSet
        adapter.Fill(ds)
        Return ds
    End Function

    Public Function GetSegmentsCompanyList(RQ As Call_SegmentsCompany_List_RQ) As DataSet
        Dim adapter As New SqlClient.SqlDataAdapter
        Dim sqlComm As New SqlClient.SqlCommand
        sqlComm = New Global.System.Data.SqlClient.SqlCommand()
        sqlComm.Connection = New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OzUnivisitConnectionString").ConnectionString)
        sqlComm.CommandText = "dbo.sp_uvcc_empresas_getList"
        sqlComm.CommandType = Global.System.Data.CommandType.StoredProcedure
        'sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@RETURN_VALUE", Global.System.Data.SqlDbType.Int, 4, Global.System.Data.ParameterDirection.ReturnValue, 10, 0, Nothing, Global.System.Data.DataRowVersion.Current, False, Nothing, "", "", ""))
        'sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idMedio", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idCorporativo", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Nombre", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@id_Segmento", Global.System.Data.SqlDbType.Int))


        adapter.SelectCommand = sqlComm

        If Not RQ.Request(0).IsidCorporateNull Then adapter.SelectCommand.Parameters("@idCorporativo").Value = CType(RQ.Request(0).idCorporate, Integer)
        If Not RQ.Request(0).IsSearchtextNull Then adapter.SelectCommand.Parameters("@Nombre").Value = CType(RQ.Request(0).Searchtext, String)
        If Not RQ.Request(0).IsidSegmentNull Then adapter.SelectCommand.Parameters("@id_Segmento").Value = CType(RQ.Request(0).idSegment, Integer)
        Dim ds As DataSet = New DataSet
        adapter.Fill(ds)
        Return ds
    End Function

    Public Function SegmentsCompanyUpdate(idEmpresa As Global.System.Nullable(Of Integer), ByVal idMedio As Global.System.Nullable(Of Integer), ByVal idCorporativo As Global.System.Nullable(Of Integer), ByVal Nombre As String, ByVal id_Segmento As Global.System.Nullable(Of Integer), ByVal Domicilio As String, ByVal Ciudad As String, ByVal CP As String, ByVal Telefono As String, ByVal RFC As String, ByVal Tipo As String) As Boolean
        'Herramientas.logmsg("SegmentsCompanyUpdate begin")
        Dim adapter As New SqlClient.SqlDataAdapter
        Dim sqlComm As New SqlClient.SqlCommand
        sqlComm = New SqlClient.SqlCommand()
        sqlComm.Connection = New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OzUnivisitConnectionString").ConnectionString)
        sqlComm.CommandText = "dbo.sp_uvcc_empresas_update"
        sqlComm.CommandType = Global.System.Data.CommandType.StoredProcedure
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@id_Empresa", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idMedio", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@idCorporativo", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Nombre", Global.System.Data.SqlDbType.NVarChar, 120))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@id_Segmento", Global.System.Data.SqlDbType.Int))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Domicilio", Global.System.Data.SqlDbType.NVarChar, 80))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Ciudad", Global.System.Data.SqlDbType.NVarChar, 50))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@CP", Global.System.Data.SqlDbType.NVarChar, 10))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Telefono", Global.System.Data.SqlDbType.NVarChar, 20))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@RFC", Global.System.Data.SqlDbType.NVarChar, 15))
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@Tipo", Global.System.Data.SqlDbType.NVarChar, 2))
        'Herramientas.logmsg("SegmentsCompanyUpdate parameters definition ok")
        adapter.SelectCommand = sqlComm
        adapter.SelectCommand.Parameters("@id_Empresa").Value = CType(idEmpresa.Value, Integer)
        If (idMedio.HasValue = True) Then
            adapter.SelectCommand.Parameters("@idMedio").Value = CType(idMedio.Value, Integer)
        Else
            adapter.SelectCommand.Parameters("@idMedio").Value = Global.System.DBNull.Value
        End If
        If (idCorporativo.HasValue = True) Then
            adapter.SelectCommand.Parameters("@idCorporativo").Value = CType(idCorporativo.Value, Integer)
        Else
            adapter.SelectCommand.Parameters("@idCorporativo").Value = Global.System.DBNull.Value
        End If
        If (Nombre Is Nothing) Then
            adapter.SelectCommand.Parameters("@Nombre").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@Nombre").Value = CType(Nombre, String)
        End If
        If (id_Segmento.HasValue = True) Then
            adapter.SelectCommand.Parameters("@id_Segmento").Value = CType(id_Segmento.Value, Integer)
        Else
            adapter.SelectCommand.Parameters("@id_Segmento").Value = Global.System.DBNull.Value
        End If
        If (Domicilio Is Nothing) Then
            adapter.SelectCommand.Parameters("@Domicilio").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@Domicilio").Value = CType(Domicilio, String)
        End If
        If (Ciudad Is Nothing) Then
            adapter.SelectCommand.Parameters("@Ciudad").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@Ciudad").Value = CType(Ciudad, String)
        End If
        If (CP Is Nothing) Then
            adapter.SelectCommand.Parameters("@CP").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@CP").Value = CType(CP, String)
        End If
        If (Telefono Is Nothing) Then
            adapter.SelectCommand.Parameters("@Telefono").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@Telefono").Value = CType(Telefono, String)
        End If
        If (RFC Is Nothing) Then
            adapter.SelectCommand.Parameters("@RFC").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@RFC").Value = CType(RFC, String)
        End If
        If (Tipo Is Nothing) Then
            adapter.SelectCommand.Parameters("@Tipo").Value = Global.System.DBNull.Value
        Else
            adapter.SelectCommand.Parameters("@Tipo").Value = CType(Tipo, String)
        End If
        Dim previousConnectionState As Global.System.Data.ConnectionState = adapter.SelectCommand.Connection.State
        If ((adapter.SelectCommand.Connection.State And Global.System.Data.ConnectionState.Open) _
                    <> Global.System.Data.ConnectionState.Open) Then
            adapter.SelectCommand.Connection.Open()
        End If
        Dim returnValue As Integer
        Try
            ' Herramientas.logmsg("SegmentsCompanyUpdate id" & adapter.SelectCommand.Parameters("@id_Empresa").Value & " name=" & adapter.SelectCommand.Parameters("@nombre").Value)
            returnValue = adapter.SelectCommand.ExecuteNonQuery
        Catch ex As Exception
            Herramientas.logmsg(ex.Message)
        Finally
            If (previousConnectionState = Global.System.Data.ConnectionState.Closed) Then
                adapter.SelectCommand.Connection.Close()
            End If
        End Try
        Herramientas.logmsg("SegmentsCompanyUpdate returnvalue" & CStr(returnValue))
        If returnValue > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function SegmentsCompanyDelete(idEmpresa As Global.System.Nullable(Of Integer)) As Boolean
        Herramientas.logmsg("SegmentsCompanyUpdate delete")
        Dim adapter As New SqlClient.SqlDataAdapter
        Dim sqlComm As New SqlClient.SqlCommand
        sqlComm = New SqlClient.SqlCommand()
        sqlComm.Connection = New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("OzUnivisitConnectionString").ConnectionString)
        sqlComm.CommandText = "dbo.sp_uvcc_empresas_delete"
        sqlComm.CommandType = Global.System.Data.CommandType.StoredProcedure
        sqlComm.Parameters.Add(New Global.System.Data.SqlClient.SqlParameter("@id_Empresa", Global.System.Data.SqlDbType.Int))

        adapter.SelectCommand = sqlComm
        adapter.SelectCommand.Parameters("@id_Empresa").Value = CType(idEmpresa.Value, Integer)
        Dim previousConnectionState As Global.System.Data.ConnectionState = adapter.SelectCommand.Connection.State
        If ((adapter.SelectCommand.Connection.State And Global.System.Data.ConnectionState.Open) _
                    <> Global.System.Data.ConnectionState.Open) Then
            adapter.SelectCommand.Connection.Open()
        End If
        Dim returnValue As Integer
        Try

            returnValue = adapter.SelectCommand.ExecuteNonQuery
            Herramientas.logmsg("SegmentsCompanyDelete return value: " & returnValue.ToString)
        Catch ex As Exception
            Herramientas.logmsg(ex.Message)
        Finally
            If (previousConnectionState = Global.System.Data.ConnectionState.Closed) Then
                adapter.SelectCommand.Connection.Close()
            End If
        End Try

        If returnValue > 1 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
