Public Class ComboSearch

    Public lista_lugares As New List(Of Object)
    Public ListBox1 As ListBox = Nothing
    Private X_Offset As Integer = 0
    Private Y_Offset As Integer = 0

    Public Event llena()
    Public Event Selected()
    Private bLlena As Boolean = True

    Private Sub fija_pos()
        ListBox1.Location = New Point(Me.Location.X + X_Offset, Me.Location.Y + Me.TextBox1.Height + 1 + Y_Offset)
    End Sub

    Private Sub desp(ByVal s As Object, ByVal e As EventArgs) Handles TextBox1.Enter
        If TextBox1.Text.Length >= 3 Then ListBox1.Visible = True
    End Sub

    Private Sub ocul(ByVal s As Object, ByVal e As EventArgs) Handles TextBox1.Leave
        If Not ListBox1.Focused Then ListBox1.Visible = False
    End Sub

    Private Sub ocul_lb(ByVal s As Object, ByVal e As EventArgs)
        ListBox1.Visible = False
    End Sub

    Private Sub lb_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ListBox1.Visible Then
            bLlena = False
            TextBox1.Text = ListBox1.Text
            bLlena = True
            RaiseEvent Selected()
        End If

    End Sub

    Private Sub lb_KeyUp(ByVal s As Object, ByVal e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            ' TextBox1.Focus()
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub lb_MouseDown(ByVal s As Object, ByVal e As MouseEventArgs)
        ' TextBox1.Focus()
        SendKeys.Send("{TAB}")
    End Sub

    Private Sub set_ListBox(ByVal lb1 As ListBox)
        ListBox1 = lb1
        ListBox1.Visible = False
        ListBox1.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        fija_pos()
        AddHandler ListBox1.LostFocus, AddressOf ocul_lb
        AddHandler ListBox1.KeyUp, AddressOf lb_KeyUp
        AddHandler ListBox1.VisibleChanged, AddressOf lb_VisibleChanged
        AddHandler ListBox1.MouseDown, AddressOf lb_MouseDown
    End Sub

    Public Sub crea_lista(ByVal ctl_to_add As Control, ByVal X_Offset As Integer, ByVal Y_Offset As Integer)
        Me.X_Offset = X_Offset
        Me.Y_Offset = Y_Offset

        Dim lb As New ListBox
        lb.Size = New Size(212, 135)
        ctl_to_add.Controls.Add(lb)
        set_ListBox(lb)
        lb.BringToFront()
        Me.TextBox1.Width = Me.Width
    End Sub

    Public Function selected_valor_Distancia() As Double
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim lugar As herramientas.lugares = Me.lista_lugares(Me.ListBox1.SelectedIndex)
        Return CDbl(lugar.distancia)
    End Function

    Public Function selected_valor_RefPoint() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim lugar As herramientas.lugares = Me.lista_lugares(Me.ListBox1.SelectedIndex)
        Return lugar.RefPoint
    End Function

    Public Function selected_valor_Codigo() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim lugar As herramientas.lugares = Me.lista_lugares(Me.ListBox1.SelectedIndex)
        Return lugar.codigo
    End Function

    Public Function selected_valor_Ciudad() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim lugar As herramientas.lugares = Me.lista_lugares(Me.ListBox1.SelectedIndex)
        Return lugar.ciudad
    End Function

    Public Function selected_valor_idCiudad() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing
        Try
            Dim lugar As herramientas.lugares = Me.lista_lugares(Me.ListBox1.SelectedIndex)
            Return lugar.idCiudad
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function selected_valor_Pais() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim lugar As herramientas.lugares = Me.lista_lugares(Me.ListBox1.SelectedIndex)
        Return lugar.pais
    End Function

    'Private Sub ocul_lb_mouse(ByVal s As Object, ByVal e As MouseEventArgs)
    '    ListBox1.Visible = False
    'End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Up AndAlso ListBox1.SelectedIndex <> 0 AndAlso ListBox1.SelectedIndex <> -1 Then ListBox1.SelectedIndex -= 1
        ''''If e.KeyCode = Keys.Up Then TextBox1.SelectionStart += 1

        If e.KeyCode = Keys.Down AndAlso ListBox1.SelectedIndex <> ListBox1.Items.Count - 1 Then ListBox1.SelectedIndex += 1
        ''''If e.KeyCode = Keys.Down AndAlso TextBox1.SelectionStart <> 0 Then TextBox1.SelectionStart -= 1
    End Sub

    Private Sub TextBox1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyUp
        desp(Nothing, Nothing)
        'If TextBox1.Text.Length < 3 Then ocul(Nothing, Nothing)

        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        'If afecta Then
        If TextBox1.Text.Length >= 3 Then
            ''''Dim ini As Integer = TextBox1.SelectionStart
            If bLlena Then
                RaiseEvent llena()
                If ListBox1.SelectedIndex = -1 Then ListBox1.SelectedIndex = 0
            End If

            'TextBox1.SelectionLength = 0
            ''''TextBox1.SelectionStart = ini
        Else
            'afecta = False
            ''''Dim ini As Integer = TextBox1.SelectionStart

            ListBox1.Items.Clear()

            ''''TextBox1.SelectionStart = ini
        End If
        'End If
    End Sub

    Private Sub ComboSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'AddHandler TextBox1.GotFocus, AddressOf desp
        'AddHandler TextBox1.LostFocus, AddressOf ocul
        'AddHandler Me.Parent.MouseDown, AddressOf ocul_lb_mouse
    End Sub

    Private Sub ComboSearch_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        If Not ListBox1 Is Nothing Then fija_pos()
    End Sub

End Class
