
RUTAS PARA PUBLICAR

http://intranet.ozvirtual.net/callcenter/
http://localhost/uv_cc_install/



=======================================================================================
Qu� hacer si se cambia, modifica, actualiza la base de datos de los clientes locales.

En el archivo:
callcenter/{callcenter v2}/data/crea_db.sql

-	En el sp actualizar_info
	Cambiar: values(getdate(),X)
	donde X es la nueva version de la bd

-	En el sp consultar_info
	Cambiar: values(@fecha,X)
	Cambiar: select @fecha fecha,X db_version 
	donde X es la nueva version de la bd

En el archivo:
callcenter/{callcenter v2}/Module1.vb

-	la variable Public CONST_BD_VERSION As Integer = X
	Cambiar: If info_db Is Nothing OrElse info_db.db_version <> X Then
	donde X es la nueva version de la bd



=======================================================================================
Qu� hacer si se cambian las rutas de los servicios web

A la hora de instalar el sistema en un equipo cliente, al iniciarlo por primera vez, se
crea un archivo llamado: ws_data.xml

C:\Documents and Settings\Administrador\Configuraci�n local\Apps\2.0\1CJ4CK2J.Y77\
D2Q9R9AH.LZD\call..tion_95471164d38750cf_0001.0000_fbf78e521419c39e\ws_data.xml

Debido a que este archivo lo crea el  sistema una vez instalado y ejecutado por primera
vez, este archivo no pertenece a la instalaci�n y por lo tanto al actualizar el sistema
con clickOnce, este archivo no ser� actualizado ya que no es parte de la instalaci�n.

Si en tiempo de dise�o se cambian las rutas a los servicios web, al actualizar el sis -
tema en el equipo cliente y al iniciar, el sistema cargara las rutas del actual archivo 
ws_data.xml (archivo desactualizado) en lugar de tomar las rutas que se cambiaron en la
aplicaci�n.

Para que el archivo ws_data.xml se actualice y tome las nuevas rutas, antes de publicar
con clickOnce en visual studio, Proyect/callcenter v2 properties../application/assembly 
information

"Incrementar assembly versi�n y file versi�n." !!!!!!!!!!!!!!!!!!!!!!!!!

De esta forma, el sistema actualizado al  detectar que su versi�n es diferente a la que
se encuentra en el archivo ws_data.xml, el sistema actualizara el archivo ws_data.xml
