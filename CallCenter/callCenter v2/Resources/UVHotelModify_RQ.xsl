<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelModify>
      <HotelModify>
        <HotelHeader>
          <ServiceProvider>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/HotelHeader/ServiceProvider"/>
          </ServiceProvider>
          <Source>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/HotelHeader/Source"/>
          </Source>
          <SourceCode>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/HotelHeader/SourceCode"/>
          </SourceCode>
          <Language>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/HotelHeader/Language"/>
          </Language>
          <AccessCode>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/HotelHeader/AccessCode"/>
          </AccessCode>
          <Corporativo>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/HotelHeader/Corporativo"/>
          </Corporativo>
          <IgnoreValidations>
            1
          </IgnoreValidations>
        </HotelHeader>
        <Customer>
          <UserId>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/UserId"/>
          </UserId>
          <PhoneHome>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/PhoneHome"/>
          </PhoneHome>
          <Email>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/Email"/>
          </Email>
          <Address>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/Address"/>
          </Address>
          <LastName>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/LastName"/>
          </LastName>
          <FirstName>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/FirstName"/>
          </FirstName>
          <PostalCode>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/PostalCode"/>
          </PostalCode>
          <Country>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/Country"/>
          </Country>
          <Estate>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/Estate"/>
          </Estate>
          <City>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Customer/City"/>
          </City>
        </Customer>
        <Reservation>
          <CheckInDate>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CheckInDate"/>
          </CheckInDate>
          <CheckOutDate>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CheckOutDate"/>
          </CheckOutDate>
          <PropertyNumber>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/PropertyNumber"/>
          </PropertyNumber>
          <ChainCode>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/ChainCode"/>
          </ChainCode>
          <RateCode>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/RateCode"/>
          </RateCode>
          <RAwayAdult>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/RAwayAdult"/>
          </RAwayAdult>
          <RAwayChild>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/RAwayChild"/>
          </RAwayChild>
          <RAwayCrib>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/RAwayCrib"/>
          </RAwayCrib>
          <ConfirmNumber>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/ConfirmNumber"/>
          </ConfirmNumber>
          <CreditCardExpiration>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CreditCardExpiration"/>
          </CreditCardExpiration>
          <CreditCardHolder>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CreditCardHolder"/>
          </CreditCardHolder>
          <CreditCardNumber>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CreditCardNumber"/>
          </CreditCardNumber>
          <CreditCardNumberVerify>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CreditCardNumberVerify"/>
          </CreditCardNumberVerify>
          <CreditCardType>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CreditCardType"/>
          </CreditCardType>
          <GuarDep>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/GuarDep"/>
          </GuarDep>
          <ReservedBySpecificPayment>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/ReservedBySpecificPayment"/>
          </ReservedBySpecificPayment>
          <PaymentInformation>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/PaymentInformation"/>
          </PaymentInformation>
          <xsl:if test="Hotel_Modify_RQ/HotelModify/Reservation/ReserveByBankDepositCCT='Y'">           
			  <xsl:if test="Hotel_Modify_RQ/HotelModify/Reservation/DepositAmountCCT!=''">
				  <ReserveByBankDepositCCT>Y</ReserveByBankDepositCCT>
			  </xsl:if>				  
            <DepositAmountCCT>
              <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/DepositAmountCCT"/>
            </DepositAmountCCT>
            <DepositInfoCCT>
              <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/DepositInfoCCT"/>
            </DepositInfoCCT>
          </xsl:if>
			<xsl:if test="Hotel_Modify_RQ/HotelModify/Reservation/ReserveByBankDeposit='Y'">
					<ReserveByBankDeposit>Y</ReserveByBankDeposit>
					<DepositLimitDate>
						<xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/DepositLimitDate"/>
					</DepositLimitDate>
					<DepositLimitTime>
						<xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/DepositLimitTime"/>
					</DepositLimitTime>
			</xsl:if>			
          <CommentByVendor>
            <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CommentByVendor"/>
          </CommentByVendor>
			<RecLoc>
				<xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/RecLoc"/>
			</RecLoc>
			<xsl:if test="Hotel_Modify_RQ/HotelModify/Reservation/RecLoc!=''">
				<Total>
					<xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/Total"/>
				</Total>
				<Money>
					<xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/Money"/>
				</Money>				
			</xsl:if>
			<xsl:if test="Hotel_Modify_RQ/HotelModify/Reservation/TaxPercentage!=''">
				<TaxPercentage>
					<xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/TaxPercentage"/>
				</TaxPercentage>
			</xsl:if>
      <xsl:if test="Hotel_Modify_RQ/HotelModify/Reservation/CodigoReferenciaCCT!=''">
        <CodigoReferenciaCCT>
          <xsl:value-of select="Hotel_Modify_RQ/HotelModify/Reservation/CodigoReferenciaCCT"/>
        </CodigoReferenciaCCT>
      </xsl:if>
		</Reservation>
        <Rooms>
          <xsl:for-each select="Hotel_Modify_RQ/HotelModify/Rooms/Room">
            <Room>
              <Children>
                <xsl:value-of select="Children"/>
              </Children>
              <Adults>
                <xsl:value-of select="Adults"/>
              </Adults>
              <Preferences>
                <xsl:value-of select="Preferences"/>
              </Preferences>
              <idtraveler>
                <xsl:value-of select="idtraveler"/>
              </idtraveler>
              <ChildrenAges>
                <xsl:value-of select="ChildrenAges"/>
              </ChildrenAges>
            </Room>
          </xsl:for-each>
        </Rooms>
      </HotelModify>
    </reqHotelModify>
  </xsl:template>
</xsl:stylesheet>