<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelDisplay>
      <HotelDisplay>
        <ChainCode>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/ChainCode"/>
        </ChainCode>
        <ConfirmNumber>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/ConfirmNumber"/>
        </ConfirmNumber>
        <Source>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/Source"/>
        </Source>
        <Language>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/Language"/>
        </Language>
        <AltCurrency>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/AltCurrency"/>
        </AltCurrency>
        <PropertyNumber>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/PropertyNumber"/>
        </PropertyNumber>
        <ReservationId>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/ReservationId"/>
        </ReservationId>
        <Corporativo>
          <xsl:value-of select="Hotel_Display_RQ/HotelDisplay/Corporativo"/>
        </Corporativo>
      </HotelDisplay>
    </reqHotelDisplay>
  </xsl:template>
</xsl:stylesheet>