<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelReactiveReservation>
      <ReactiveReservation>
        <ConfirmNumber>
          <xsl:value-of select="Hotel_Reactive_RQ/ReactiveReservation/ConfirmNumber"/>
        </ConfirmNumber>
        <PropertyNumber>
          <xsl:value-of select="Hotel_Reactive_RQ/ReactiveReservation/PropertyNumber"/>
        </PropertyNumber>
        <Corporativo>
          <xsl:value-of select="Hotel_Reactive_RQ/ReactiveReservation/Corporativo"/>
        </Corporativo>
      </ReactiveReservation>
    </reqHotelReactiveReservation>
  </xsl:template>
</xsl:stylesheet>