<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelCancel>
      <HotelCancel>
        <ServiceProvider>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/ServiceProvider"/>
        </ServiceProvider>
        <ChainCode>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/ChainCode"/>
        </ChainCode>
        <ConfirmNumber>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/ConfirmNumber"/>
        </ConfirmNumber>
        <Source>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/Source"/>
        </Source>
        <SourceCode>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/SourceCode"/>
        </SourceCode>
        <Language>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/Language"/>
        </Language>
        <PropertyNumber>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/PropertyNumber"/>
        </PropertyNumber>
        <Corporativo>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/Corporativo"/>
        </Corporativo>
        <CxWithError>
          <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/CxWithError"/>
        </CxWithError>
		  <CXCause>
			  <xsl:value-of select="Hotel_Cancel_RQ/HotelCancel/CXCause"/>
		  </CXCause>
			</HotelCancel>
      <Customer>
        <UserId>
          <xsl:value-of select="Hotel_Cancel_RQ/Customer/UserId"/>
        </UserId>
      </Customer>
    </reqHotelCancel>
  </xsl:template>
</xsl:stylesheet>