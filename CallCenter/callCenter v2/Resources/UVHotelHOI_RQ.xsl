<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://tempuri.org/Hotel_HOI_RQ.xsd">
  <xsl:template match="/">
    <reqHotelIndex>
      <Index>
        <HotelHeader>
          <Language>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:Language"/>
          </Language>
          <Code>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:Code"/>
          </Code>
          <Source>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:Source"/>
          </Source>
          <SearchBy>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:SearchBy"/>
          </SearchBy>
          <Corporativo>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:Corporativo"/>
          </Corporativo>
          <ChainCode>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:ChainCode"/>
          </ChainCode>
          <AltCurrency>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelHeader/a:AltCurrency"/>
          </AltCurrency>
        </HotelHeader>
        <HotelRequest>
          <StartDate>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelRequest/a:StartDate"/>
          </StartDate>
          <EndDate>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelRequest/a:EndDate"/>
          </EndDate>
          <Nights>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelRequest/a:Nights"/>
          </Nights>
          <Adults>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelRequest/a:Adults"/>
          </Adults>
          <Children>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelRequest/a:Children"/>
          </Children>
          <NumRooms>
            <xsl:value-of select="a:Hotel_HOI_RQ/a:HotelRequest/a:NumRooms"/>
          </NumRooms>
          <Rooms>
            <xsl:for-each select="a:Hotel_HOI_RQ/a:Room">
              <Room>
                <Childern>
                  <xsl:value-of select="a:Children"/>
                </Childern>
                <Adults>
                  <xsl:value-of select="a:Adults"/>
                </Adults>
                  <ChildAry>
                    <Age>
                      <xsl:value-of select="../a:ChildAry/a:Age"/>
                    </Age>
                  </ChildAry>
              </Room>
            </xsl:for-each>
          </Rooms>
        </HotelRequest>
      </Index>
    </reqHotelIndex>
  </xsl:template>
</xsl:stylesheet>