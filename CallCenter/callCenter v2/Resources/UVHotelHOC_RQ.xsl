<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<reqHotelComplete>
			<CompleteAvailability>
				<HotelHeader>
          <ServiceProvider>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/ServiceProvider"/>
          </ServiceProvider>
          <CheckinDate>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/CheckinDate"/>
          </CheckinDate>
					<CheckoutDate>
						<xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/CheckoutDate"/>
					</CheckoutDate>
          <AccessCode>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/AccessCode"/>
          </AccessCode>
          <Language>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/Language"/>
          </Language>
          <AltCurrency>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/AltCurrency"/>
          </AltCurrency>
					<Source>
						<xsl:text>POR</xsl:text>
					</Source>
					<xsl:if test="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/IdAgency">
						<IdAgency>
							<xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/IdAgency"/>
						</IdAgency>
					</xsl:if>
					<xsl:if test="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/IATA">
						<IATA>
							<xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/IATA"/>
						</IATA>
					</xsl:if>
					<!--<xsl:if test="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/AccessCode">
						<AccessCode>
							<xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/AccessCode"/>
						</AccessCode>
					</xsl:if>-->
          <IdPortal>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/IdPortal"/>
          </IdPortal>
          <Corporativo>
            <xsl:value-of select="Hotel_HOC_RQ/CompleteAvailability/HotelHeader/Corporativo"/>
          </Corporativo>
				</HotelHeader>				
					<xsl:for-each select="Hotel_HOC_RQ/CompleteAvailability/HotelFilter">
					<HotelFilter>
						<Segment>							
							<xsl:value-of select="Segment"/>							
						</Segment>
					</HotelFilter>
					</xsl:for-each>				
				<HotelRequests>
					<xsl:for-each select="Hotel_HOC_RQ/CompleteAvailability/HotelRequests/HotelRequest">
						<HotelRequest>
							<PropertyNumber>
								<xsl:value-of select="PropertyNumber"/>
							</PropertyNumber>
						</HotelRequest>
					</xsl:for-each>
				</HotelRequests>
				<HotelRooms>
					<xsl:for-each select="Hotel_HOC_RQ/CompleteAvailability/HotelRooms/Room">
						<Room>
							<Adults>
								<xsl:value-of select="Adults"/>
							</Adults>
							<Children>
								<xsl:value-of select="Children"/>
							</Children>
						</Room>
					</xsl:for-each>
				</HotelRooms>
			</CompleteAvailability>
		</reqHotelComplete>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2004. Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios/><MapperMetaTag><MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="UVHotelHOC_RQ.xml" destSchemaRoot="reqHotelComplete" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no" ><SourceSchema srcSchemaPath="..\Hotel_HOC_RQ.xsd" srcSchemaRoot="Hotel_HOC_RQ" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/></MapperInfo><MapperBlockPosition><template match="/"><block path="reqHotelComplete/CompleteAvailability/HotelRequests/xsl:for&#x2D;each" x="184" y="190"/><block path="reqHotelComplete/CompleteAvailability/HotelRooms/xsl:for&#x2D;each" x="156" y="275"/></template></MapperBlockPosition></MapperMetaTag>
</metaInformation>
-->