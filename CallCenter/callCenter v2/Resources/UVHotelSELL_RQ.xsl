<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelSell>
      <HotelSell>
        <Customer>
          <PhoneWork>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/PhoneWork"/>
          </PhoneWork>
          <PhoneHome>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/PhoneHome"/>
          </PhoneHome>
          <Email>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/Email"/>
          </Email>
          <Address>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/Address"/>
          </Address>
          <LastName>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/LastName"/>
          </LastName>
          <FirstName>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/FirstName"/>
          </FirstName>
          <PostalCode>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/PostalCode"/>
          </PostalCode>
          <Country>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/Country"/>
          </Country>
          <City>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/City"/>
          </City>
          <Estate>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Customer/Estate"/>
          </Estate>
        </Customer>
        <HotelHeader>
          <ServiceProvider>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/ServiceProvider"/>
          </ServiceProvider>
          <Source>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/Source"/>
          </Source>
          <SourceCode>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/SourceCode"/>
          </SourceCode>
          <Language>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/Language"/>
          </Language>
          <!--<IdPortal>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/IdPortal"/>
          </IdPortal>-->
          <AccessCode>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/AccessCode"/>
          </AccessCode>
          <Corporativo>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/HotelHeader/Corporativo"/>
          </Corporativo>
        </HotelHeader>
        <Reservation>
          <CheckInDate>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CheckInDate"/>
          </CheckInDate>
          <CheckOutDate>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CheckOutDate"/>
          </CheckOutDate>
          <PropertyNumber>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/PropertyNumber"/>
          </PropertyNumber>
          <ChainCode>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/ChainCode"/>
          </ChainCode>
          <CreditCardExpiration>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CreditCardExpiration"/>
          </CreditCardExpiration>
          <CreditCardHolder>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CreditCardHolder"/>
          </CreditCardHolder>
          <CreditCardNumber>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CreditCardNumber"/>
          </CreditCardNumber>
          <CreditCardNumberVerify>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CreditCardNumberVerify"/>
          </CreditCardNumberVerify>
          <CreditCardType>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/CreditCardType"/>
          </CreditCardType>
          <RateCode>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/RateCode"/>
          </RateCode>
          <RAwayAdult>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/RAwayAdult"/>
          </RAwayAdult>
          <RAwayChild>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/RAwayChild"/>
          </RAwayChild>
          <RAwayCrib>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/RAwayCrib"/>
          </RAwayCrib>
          <GuarDep>
            <xsl:value-of select="Hotel_Sell_RQ/HotelSell/Reservation/GuarDep"/>
          </GuarDep>
        </Reservation>
        <Rooms>
          <xsl:for-each select="Hotel_Sell_RQ/HotelSell/Rooms/Room">
            <Room>
              <Children>
                <xsl:value-of select="Children"/>
              </Children>
              <Adults>
                <xsl:value-of select="Adults"/>
              </Adults>
              <Preferences>
                <xsl:value-of select="Preferences"/>
              </Preferences>
            </Room>
          </xsl:for-each>
        </Rooms>
      </HotelSell>
    </reqHotelSell>
  </xsl:template>
</xsl:stylesheet>