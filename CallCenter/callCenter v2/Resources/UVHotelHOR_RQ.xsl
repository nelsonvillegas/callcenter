<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelRules>
      <HotelRules>
        <HotelHeader>
          <ServiceProvider>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/ServiceProvider"/>
          </ServiceProvider>
          <CheckinDate>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/CheckinDate"/>
          </CheckinDate>
          <CheckoutDate>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/CheckoutDate"/>
          </CheckoutDate>
          <AccessCode>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/AccessCode"/>
          </AccessCode>
          <Language>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/Language"/>
          </Language>
          <RateCode>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/RateCode"/>
          </RateCode>
          <PropertyNumber>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/PropertyNumber"/>
          </PropertyNumber>
          <Source>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/Source"/>
          </Source>
          <Nights>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/Nights"/>
          </Nights>
          <ChainCode>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/ChainCode"/>
          </ChainCode>
          <Corporativo>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/Corporativo"/>
          </Corporativo>
          <AltCurrency>
            <xsl:value-of select="Hotel_HOR_RQ/HotelRules/HotelHeader/AltCurrency"/>
          </AltCurrency>
        </HotelHeader>
        <HotelRooms>
          <xsl:for-each select="Hotel_HOR_RQ/HotelRules/HotelRooms/Room">
            <Room>
              <Adults>
                <xsl:value-of select="Adults"/>
              </Adults>
              <Children>
                <xsl:value-of select="Children"/>
              </Children>
            </Room>
          </xsl:for-each>
        </HotelRooms>
      </HotelRules>
    </reqHotelRules>
  </xsl:template>
</xsl:stylesheet>