<?xml version='1.0' ?>
<!--<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelDescription>
      <Description>
        <HotelHeader>
          <ServiceProvider>
            <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/ServiceProvider"/>
          </ServiceProvider>
          <Language>
            <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/Language"/>
          </Language>
          <Corporativo>
            <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/Corporativo"/>
          </Corporativo>
        </HotelHeader>
        <HotelProperty>
          <Hotel>
            <PropertyNumber>
              <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/PropertyNumber"/>
            </PropertyNumber>
          </Hotel>
        </HotelProperty>
        <HotelFilter>
          <xsl:for-each select="Hotel_HOD_RQ/Keywords">
            <Keywords>
              <Keyword>
                <xsl:value-of select="Keyword"/>
              </Keyword>
            </Keywords>
          </xsl:for-each>
        </HotelFilter>
      </Description>
    </reqHotelDescription>
  </xsl:template>
</xsl:stylesheet>-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <reqHotelDescription>
      <Description>
        <HotelHeader>
            <PropertyNumber>
              <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/PropertyNumber"/>
            </PropertyNumber>
          <ServiceProvider>
            <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/ServiceProvider"/>
          </ServiceProvider>
          <Language>
            <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/Language"/>
          </Language>
          <Corporativo>
            <xsl:value-of select="Hotel_HOD_RQ/Description/HotelHeader/Corporativo"/>
          </Corporativo>
        </HotelHeader>
        <HotelFilter>
          <xsl:for-each select="Hotel_HOD_RQ/Keywords">
            <Keywords>
              <Keyword>
                <xsl:value-of select="Keyword"/>
              </Keyword>
            </Keywords>
          </xsl:for-each>
        </HotelFilter>
      </Description>
    </reqHotelDescription>
  </xsl:template>
</xsl:stylesheet>