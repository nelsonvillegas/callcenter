Imports System.Data.SqlClient

Public Class Form1_callCenter

    Public configura As Boolean = False
    Public PreguntaAlCerrar As Boolean = True
    Public datoscliente As dataBusqueda_paquete

    Private Thread_SearchCity As Threading.Thread

    Private Sub toolClick(ByVal key As String)
        misForms.exe_accion(Nothing, key)
    End Sub

    Private Sub UltraTabbedMdiManager1_TabActivated(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinTabbedMdi.MdiTabEventArgs) Handles UltraTabbedMdiManager1.TabActivated
        'set ultimo tab activo
        If e.Tab.Form.Name.ToLower = "busquedahotel" Then misForms.busquedaHotel_ultimoActivo = e.Tab.Form
        If e.Tab.Form.Name.ToLower = "tarifas" Then misForms.tarifas = e.Tab.Form
        If e.Tab.Form.Name.ToLower = "busquedavuelo" Then misForms.busquedaVuelo_ultimoActivo = e.Tab.Form
        If e.Tab.Form.Name.ToLower = "busquedaactividad" Then misForms.busquedaActividad_ultimoActivo = e.Tab.Form
        If e.Tab.Form.Name.ToLower = "eventosactividades" Then misForms.eventosActividades_ultimoActivo = e.Tab.Form
        If e.Tab.Form.Name.ToLower = "busquedaautos" Then misForms.busquedaAutos_ultimoActivo = e.Tab.Form
        If e.Tab.Form.Name.ToLower = "tarifasauto" Then misForms.tarifasAutos_ultimoActivo = e.Tab.Form
        If e.Tab.Form.Name.ToLower <> "reservaciones" Then
            'deshabilitar el menu 
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_detalles").SharedProps.Visible = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Visible = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Visible = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reactivar").SharedProps.Visible = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Visible = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambios").SharedProps.Visible = False
        Else
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_detalles").SharedProps.Visible = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Visible = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Visible = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reactivar").SharedProps.Visible = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Visible = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambios").SharedProps.Visible = True
        End If
        'CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Visible = False
        'CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Enabled = False
        'Dim existe As Boolean = False
        'For Each frm As Form In misForms.form1.MdiChildren
        '    If frm.Name = "busquedaHotel" Then existe = True
        'Next
        'If Not existe Then misForms.busquedaHotel_ultimoActivo = Nothing

        'vaidaciones
        'If misForms.busquedaHotel_ultimoActivo IsNot Nothing Then
        '    'despliega barra de estado
        '    'If e.Tab.Form.Name = "busquedaHotel" AndAlso CType(e.Tab.Form, busquedaHotel).total > 100 Then CType(e.Tab.Form, busquedaHotel).total = 100

        '    If e.Tab.Form.Name = "busquedaHotel" Then
        '        If CType(e.Tab.Form, busquedaHotel).total = 100 Then
        '            misForms.form1.UltraStatusBar1.Panels("pnl_3").ProgressBarInfo.Value = 100
        '            misForms.form1.UltraStatusBar1.Panels("pnl_3").Visible = False
        '            misForms.form1.UltraStatusBar1.Refresh()
        '        Else
        '            Dim vis As Boolean
        '            If misForms.busquedaHotel_ultimoActivo.UltraGrid1.DataSource Is Nothing Then vis = False Else vis = True

        '            misForms.form1.UltraStatusBar1.Panels("pnl_3").ProgressBarInfo.Value = CType(e.Tab.Form, busquedaHotel).total
        '            misForms.form1.UltraStatusBar1.Panels("pnl_3").Visible = vis
        '            misForms.form1.UltraStatusBar1.Refresh()
        '        End If
        '    Else
        '        misForms.form1.UltraStatusBar1.Panels("pnl_3").Visible = False
        '        misForms.form1.UltraStatusBar1.Refresh()
        '    End If
        'End If






        If e.Tab.Form.Name.ToLower = "busquedahotel" Then
            Dim sel As Boolean
            If CType(e.Tab.Form, busquedaHotel).UltraGrid1.ActiveRow Is Nothing Then sel = False Else sel = True

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_informacionGeneral").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_habitaciones").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_galeria").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_mapa").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_hotel_disponibilidad").SharedProps.Enabled = sel

            If Not sel Then
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Enabled = False
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas").SharedProps.Enabled = False
            End If
        End If

        If e.Tab.Form.Name.ToLower = "tarifas" Then
            Dim sel As Boolean
            If CType(e.Tab.Form, tarifas).UltraGrid1.ActiveRow Is Nothing Then sel = False Else sel = True

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ver_detalles").SharedProps.Enabled = sel

        End If

        If e.Tab.Form.Name = "busquedaVuelo" Then
            Dim sel As Boolean = CType(e.Tab.Form, busquedaVuelo).Uc_itinerario1.TieneSelected

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar_vuelo").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas_vuelo").SharedProps.Enabled = sel
        End If

        If e.Tab.Form.Name = "busquedaActividad" Then
            Dim sel As Boolean
            If CType(e.Tab.Form, busquedaActividad).UltraGrid1.ActiveRow Is Nothing Then sel = False Else sel = True

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_eventosActividad").SharedProps.Enabled = sel
        End If

        If e.Tab.Form.Name = "eventosActividades" Then
            Dim sel As Boolean
            If CType(e.Tab.Form, eventosActividades).UltraGrid1.ActiveRow Is Nothing Then sel = False Else sel = True

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarActividad").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasActividad").SharedProps.Enabled = sel
        End If

        If e.Tab.Form.Name = "busquedaAutos" Then
            Dim sel As Boolean
            If CType(e.Tab.Form, busquedaAutos).UltraGrid1.ActiveRow Is Nothing Then sel = False Else sel = True

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifasAuto").SharedProps.Enabled = sel
        End If

        If e.Tab.Form.Name = "tarifasAuto" Then
            Dim sel As Boolean
            If CType(e.Tab.Form, tarifasAuto).UltraGrid1.ActiveRow Is Nothing Then sel = False Else sel = True

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarAuto").SharedProps.Enabled = sel
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasAuto").SharedProps.Enabled = sel
        End If

        UltraToolbarsManager1.Ribbon.Tabs("ribbon2").Visible = CONST_ENABLED_HOTELES
        UltraToolbarsManager1.Ribbon.Tabs("ribbon5").Visible = CONST_ENABLED_VUELOS
        UltraToolbarsManager1.Ribbon.Tabs("ribbon6").Visible = CONST_ENABLED_ACTIVIDADES
        UltraToolbarsManager1.Ribbon.Tabs("ribbon7").Visible = CONST_ENABLED_AUTOS
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ingles").SharedProps.Enabled = CONST_ENABLED_INGLES
    End Sub

    Private Sub UltraTabbedMdiManager1_TabClosed(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinTabbedMdi.MdiTabEventArgs) Handles UltraTabbedMdiManager1.TabClosed
        'muesta/oculta el TAB de hoteles
        Dim cont_busq As Integer = 0
        Dim cont_tari As Integer = 0
        Dim cont_buVu As Integer = 0
        Dim cont_buAc As Integer = 0
        Dim cont_evAc As Integer = 0
        Dim cont_buAu As Integer = 0
        Dim cont_taAu As Integer = 0
        For Each gru As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In UltraTabbedMdiManager1.TabGroups
            For Each tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab In gru.Tabs
                If tab.Form.Name = "busquedaHotel" Then
                    If CType(tab.Form, busquedaHotel).UltraGrid1.ActiveRow IsNot Nothing Then
                        cont_busq += 1
                    End If
                End If
                If tab.Form.Name = "tarifas" Then cont_tari += 1
                If tab.Form.Name = "busquedaVuelo" Then cont_buVu += 1
                If tab.Form.Name = "busquedaActividad" Then cont_buAc += 1
                If tab.Form.Name = "eventosActividades" Then cont_evAc += 1
                If tab.Form.Name = "busquedaAutos" Then cont_buAu += 1
                If tab.Form.Name = "tarifasAuto" Then cont_taAu += 1

                If misForms.tarifas Is Nothing AndAlso tab.Form.Name = "tarifas" Then misForms.tarifas = tab.Form
                If Not misForms.tarifas Is Nothing AndAlso misForms.tarifas.UltraGrid1.ActiveRow Is Nothing AndAlso Not misForms.tarifas.UltraGrid1.Rows Is Nothing AndAlso misForms.tarifas.UltraGrid1.Rows.Count > 0 Then misForms.tarifas.UltraGrid1.ActiveRow = misForms.tarifas.UltraGrid1.Rows(0)
            Next
        Next

        Dim ena As Boolean

        If cont_busq = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_informacionGeneral").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_habitaciones").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_galeria").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_mapa").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_hotel_disponibilidad").SharedProps.Enabled = ena

        If cont_tari = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ver_detalles").SharedProps.Enabled = ena

        If cont_buVu = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar_vuelo").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas_vuelo").SharedProps.Enabled = ena

        If cont_buAc = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_eventosActividad").SharedProps.Enabled = ena

        If cont_evAc = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarActividad").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasActividad").SharedProps.Enabled = ena

        If cont_buAu = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifasAuto").SharedProps.Enabled = ena

        If cont_taAu = 0 Then ena = False Else ena = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarAuto").SharedProps.Enabled = ena
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasAuto").SharedProps.Enabled = ena

    End Sub

    Private Sub UltraToolbarsManager1_ToolClick(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinToolbars.ToolClickEventArgs) Handles UltraToolbarsManager1.ToolClick
        toolClick(e.Tool.Key)
    End Sub

    Private Sub UltraToolbarsManager1_MouseEnterElement(ByVal sender As Object, ByVal e As Infragistics.Win.UIElementEventArgs) Handles UltraToolbarsManager1.MouseEnterElement
        Dim tool As Infragistics.Win.UltraWinToolbars.ToolBase = UltraToolbarsManager1.ToolFromPoint(Cursor.Position)

        If tool Is Nothing Then Return

        Select Case tool.Key
            'Case "cmb_lugar"
            '    misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_lugar")
            'Case "txt_codigoPromocion"
            '    misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_codProm")
            'Case "txt_habitaciones"
            '    misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_habitaciones")
            'Case "txt_adultos"
            '    misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_adultos")
            'Case "txt_ninosHabitaci�n"
            '    misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_ninos")
            'Case "pum_filtrarHoteles"
            '    misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_filtrar")
            Case "btn_buscar"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_buscar")
            Case "btn_informacionGeneral"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_infoGral")
            Case "btn_habitaciones"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_habitacionesInfo")
            Case "btn_galeria"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_galeria")
            Case "btn_mapa"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_mapa")
            Case "btn_tarifas"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_tarifas")
            Case "btn_reservar"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_reservar")
            Case "btn_reglas"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_reglas")
            Case "btn_reservaciones"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_reservaciones")
            Case "btn_detalles"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_detalles")
            Case "btn_modificar"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_modificar")
            Case "btn_cancelar"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_cancelar")
            Case "btn_configurarAgente"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_configAgente")
            Case "btn_configurarConexion"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_configConex")
            Case "btn_espa�ol"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_espa�ol")
            Case "btn_ingles"
                misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = CapaPresentacion.Idiomas.get_str("str_0189_INFO_ingles")
            Case "btn_ver_detalles"
                '   Mostrar detalles de los dias de las tarifas
        End Select
    End Sub

    Private Sub UltraToolbarsManager1_MouseLeaveElement(ByVal sender As Object, ByVal e As Infragistics.Win.UIElementEventArgs) Handles UltraToolbarsManager1.MouseLeaveElement
        Try
            If misForms.form1.UltraStatusBar1.Panels.Exists("pnl_2") Then misForms.form1.UltraStatusBar1.Panels("pnl_2").Text = ""
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("UltraToolbarsManager1_MouseLeaveElement - Form1_callCenter", ex.Message)
        End Try
    End Sub

    Private Sub SearchCity()
        Dim ds As DataSet = herramientas.get_lugares_2(True, "la p")
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Thread_SearchCity = New Threading.Thread(AddressOf SearchCity)
        Thread_SearchCity.Start()

        Me.Text = CapaAccesoDatos.XML.localData.NombreSistema
        UltraStatusBar1.Panels("pnl_1").Text = CapaPresentacion.Idiomas.get_str("str_386_version") + ": " + My.Application.Info.Version.ToString

        'agrega el boton de Cerrar al header del MDITab y quita el icono del formulario
        UltraTabbedMdiManager1.CreationFilter = New TabbedMDICloseButtonCreationFilter(Me.UltraTabbedMdiManager1)

        Me.Cursor = Cursors.WaitCursor
        UltraToolbarsManager1.Cursor = Cursors.WaitCursor

        misForms.form1 = Me
        CapaPresentacion.Common.RibbonTools.inicializar()





        'modifica la interfaz en caso de que solo se permita modificar la conexion
        If configura Then
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon1")
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon2")
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon3")
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon5")
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon6")
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon7")
            UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon8")

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_configurarAgente").SharedProps.Enabled = False
        Else
            'TODO: MISION rewards
            If dataOperador.idCorporativo <> "1" Then UltraToolbarsManager1.Ribbon.Tabs.Remove("ribbon8")
            Dim ws As New webService_services(username, password) 'System.Configuration.ConfigurationManager.AppSettings("username"), ConfigurationManager.AppSettings("password")
            If Not ws.es_ok() Then
                UltraToolbarsManager1.Ribbon.Tabs("ribbon3").Visible = False

                'herramientas.cambiar_idioma(Nothing)
                CapaPresentacion.Idiomas.cambiar_systema()
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0131_errServicioWeb"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Dim dockArea As New Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedRight)
                dockArea.Size = New Size(300, 200)
                dockArea.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup
                UltraDockManager1.DockAreas.Add(dockArea)

                'herramientas.cambiar_idioma(Nothing)
                CapaPresentacion.Idiomas.cambiar_systema()


                lista_colores.Add(Color.FromArgb(200, 255, 165, 0))     'anaranjado
                lista_colores.Add(Color.FromArgb(200, 135, 229, 135))   'verde
                lista_colores.Add(Color.FromArgb(200, 255, 192, 203))   'rosa
                lista_colores.Add(Color.FromArgb(200, 255, 243, 0))     'amarillo
                lista_colores.Add(Color.FromArgb(200, 135, 135, 229))   'azul
                lista_colores.Add(Color.FromArgb(200, 222, 104, 104))   'rojo
            End If

            misForms.exe_accion(Nothing, "btn_nuevaLlamada")
        End If

        If CapaPresentacion.Idiomas.IsCulturaEspanol Then CapaPresentacion.Common.RibbonTools.get_StateButtonTool("btn_espa�ol").Checked = True Else CapaPresentacion.Common.RibbonTools.get_StateButtonTool("btn_ingles").Checked = True

        Me.Cursor = Cursors.Arrow
        UltraToolbarsManager1.Cursor = Cursors.Arrow
    End Sub

    Private Sub Form1_callCenter_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        misForms.form1 = Nothing
        ws_login = Nothing
    End Sub

    Private Sub Form1_callCenter_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        CapaAccesoDatos.XML.localData.Moneda = moneda_selected

        If e.Cancel Then Return

        If UltraTabbedMdiManager1.TabGroups.Count = 0 Then Return

        For Each g As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In UltraTabbedMdiManager1.TabGroups
            If g.Tabs.Count = 0 Then Return
        Next

        If PreguntaAlCerrar Then
            If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0191_confirmSalir"), CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

End Class
