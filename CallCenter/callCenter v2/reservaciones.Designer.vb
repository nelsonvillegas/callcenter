<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Reservaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Reservaciones))
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook()
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ValueListItem1 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem2 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem3 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lbl_segmento = New System.Windows.Forms.Label()
        Me.lbl_empresa = New System.Windows.Forms.Label()
        Me.cmbSegmento = New System.Windows.Forms.ComboBox()
        Me.uce_source = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.lblSource = New System.Windows.Forms.Label()
        Me.opt_transacciones = New Infragistics.Win.UltraWinEditors.UltraOptionSet()
        Me.btn_actualizar = New Infragistics.Win.Misc.UltraButton()
        Me.btn_excelExport = New Infragistics.Win.Misc.UltraButton()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.txt_cliente = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_cliente = New System.Windows.Forms.Label()
        Me.grp_filtros = New Infragistics.Win.Misc.UltraExpandableGroupBox()
        Me.UltraExpandableGroupBoxPanel2 = New Infragistics.Win.Misc.UltraExpandableGroupBoxPanel()
        Me.lbl_compania = New System.Windows.Forms.Label()
        Me.btn_limpiar = New System.Windows.Forms.Button()
        Me.txt_compania = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_ciudad = New System.Windows.Forms.Label()
        Me.txt_ciudad = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.txt_numConfirm = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_numConfirm = New System.Windows.Forms.Label()
        Me.lbl_fechaBusqueda = New System.Windows.Forms.Label()
        Me.uce_tipoFecha = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.lbl_llegada = New System.Windows.Forms.Label()
        Me.dtp_in = New System.Windows.Forms.DateTimePicker()
        Me.lbl_salida = New System.Windows.Forms.Label()
        Me.dtp_out = New System.Windows.Forms.DateTimePicker()
        Me.UltraGridExcelExporter1 = New Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(Me.components)
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.UltraExpandableGroupBox1 = New Infragistics.Win.Misc.UltraExpandableGroupBox()
        Me.UltraExpandableGroupBoxPanel1 = New Infragistics.Win.Misc.UltraExpandableGroupBoxPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grp_transacciones = New Infragistics.Win.Misc.UltraExpandableGroupBox()
        Me.UltraExpandableGroupBoxPanel3 = New Infragistics.Win.Misc.UltraExpandableGroupBoxPanel()
        Me.lbl_transaccionespms = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.uce_rubro = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.CmbSearch1 = New callCenter.cmbSearch()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_source, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_transacciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_cliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grp_filtros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_filtros.SuspendLayout()
        Me.UltraExpandableGroupBoxPanel2.SuspendLayout()
        CType(Me.txt_compania, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ciudad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_numConfirm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_tipoFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraExpandableGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraExpandableGroupBox1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grp_transacciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_transacciones.SuspendLayout()
        Me.UltraExpandableGroupBoxPanel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.uce_rubro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraGrid1
        '
        Me.UltraGrid1.ContextMenuStrip = Me.ContextMenuStrip1
        Appearance1.BackColor = System.Drawing.Color.White
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance1
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance3.ImageBackground = CType(resources.GetObject("Appearance3.ImageBackground"), System.Drawing.Image)
        Appearance3.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance3.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance3.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance3
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance4.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance4
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance6.FontData.Name = "Trebuchet MS"
        Appearance6.FontData.SizeInPoints = 9.0!
        Appearance6.ForeColor = System.Drawing.Color.White
        Appearance6.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance6
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance7.FontData.BoldAsString = "True"
        Appearance7.FontData.Name = "Trebuchet MS"
        Appearance7.FontData.SizeInPoints = 10.0!
        Appearance7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance7.ImageBackground = CType(resources.GetObject("Appearance7.ImageBackground"), System.Drawing.Image)
        Appearance7.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance7.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance7
        Appearance8.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance8
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance5.BackColor2 = System.Drawing.SystemColors.Control
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance5.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance5
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance10.BackColor = System.Drawing.SystemColors.Window
        Appearance10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance10
        Appearance11.BackColor = System.Drawing.SystemColors.Highlight
        Appearance11.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance11
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance13.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance13
        Appearance15.BorderColor = System.Drawing.Color.Transparent
        Appearance15.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance15
        Appearance16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance16.ImageBackground = CType(resources.GetObject("Appearance16.ImageBackground"), System.Drawing.Image)
        Appearance16.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance16.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance16
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance22.BackColor = System.Drawing.SystemColors.Control
        Appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance22.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance22
        Appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance23.FontData.BoldAsString = "True"
        Appearance23.FontData.Name = "Trebuchet MS"
        Appearance23.FontData.SizeInPoints = 10.0!
        Appearance23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance23.ImageBackground = CType(resources.GetObject("Appearance23.ImageBackground"), System.Drawing.Image)
        Appearance23.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance23.TextHAlignAsString = "Left"
        Appearance23.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance23
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance28.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance28
        Appearance29.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance29
        Appearance30.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance30
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance31.BorderColor = System.Drawing.Color.Transparent
        Appearance31.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance31
        Appearance32.BorderColor = System.Drawing.Color.Transparent
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance32
        Appearance33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance33
        Appearance34.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance34.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance34
        Appearance35.ImageBackground = CType(resources.GetObject("Appearance35.ImageBackground"), System.Drawing.Image)
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance36.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance38.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance38
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 132)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(721, 268)
        Me.UltraGrid1.TabIndex = 1
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'lbl_segmento
        '
        Me.lbl_segmento.AutoSize = True
        Me.lbl_segmento.BackColor = System.Drawing.Color.Transparent
        Me.lbl_segmento.Location = New System.Drawing.Point(2, 96)
        Me.lbl_segmento.Name = "lbl_segmento"
        Me.lbl_segmento.Size = New System.Drawing.Size(58, 13)
        Me.lbl_segmento.TabIndex = 17
        Me.lbl_segmento.Text = "Segmento:"
        '
        'lbl_empresa
        '
        Me.lbl_empresa.AutoSize = True
        Me.lbl_empresa.BackColor = System.Drawing.Color.Transparent
        Me.lbl_empresa.Location = New System.Drawing.Point(2, 67)
        Me.lbl_empresa.Name = "lbl_empresa"
        Me.lbl_empresa.Size = New System.Drawing.Size(51, 13)
        Me.lbl_empresa.TabIndex = 18
        Me.lbl_empresa.Text = "Empresa:"
        '
        'cmbSegmento
        '
        Me.cmbSegmento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSegmento.FormattingEnabled = True
        Me.cmbSegmento.Location = New System.Drawing.Point(102, 93)
        Me.cmbSegmento.Name = "cmbSegmento"
        Me.cmbSegmento.Size = New System.Drawing.Size(162, 21)
        Me.cmbSegmento.TabIndex = 16
        '
        'uce_source
        '
        Me.uce_source.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_source.Location = New System.Drawing.Point(71, 96)
        Me.uce_source.Name = "uce_source"
        Me.uce_source.Size = New System.Drawing.Size(121, 21)
        Me.uce_source.TabIndex = 14
        '
        'lblSource
        '
        Me.lblSource.AutoSize = True
        Me.lblSource.BackColor = System.Drawing.Color.Transparent
        Me.lblSource.Location = New System.Drawing.Point(7, 100)
        Me.lblSource.Name = "lblSource"
        Me.lblSource.Size = New System.Drawing.Size(41, 13)
        Me.lblSource.TabIndex = 13
        Me.lblSource.Text = "Origen:"
        '
        'opt_transacciones
        '
        Appearance12.BackColor = System.Drawing.Color.Transparent
        Me.opt_transacciones.Appearance = Appearance12
        Me.opt_transacciones.BackColor = System.Drawing.Color.Transparent
        Me.opt_transacciones.BackColorInternal = System.Drawing.Color.Transparent
        Me.opt_transacciones.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        ValueListItem1.CheckState = System.Windows.Forms.CheckState.Checked
        ValueListItem1.DisplayText = "Todas"
        ValueListItem2.DataValue = "1"
        ValueListItem2.DisplayText = "Con transacciones pendientes"
        ValueListItem3.DataValue = "0"
        ValueListItem3.DisplayText = "Sin transacciones pendientes"
        Me.opt_transacciones.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem1, ValueListItem2, ValueListItem3})
        Me.opt_transacciones.ItemSpacingVertical = 10
        Me.opt_transacciones.Location = New System.Drawing.Point(3, 18)
        Me.opt_transacciones.Name = "opt_transacciones"
        Me.opt_transacciones.Size = New System.Drawing.Size(175, 71)
        Me.opt_transacciones.TabIndex = 0
        Me.opt_transacciones.Text = "Todas"
        Me.opt_transacciones.UseAppStyling = False
        '
        'btn_actualizar
        '
        Appearance9.BackColor = System.Drawing.Color.Transparent
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_actualizar.Appearance = Appearance9
        Me.btn_actualizar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_actualizar.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_actualizar.Location = New System.Drawing.Point(6, 73)
        Me.btn_actualizar.Name = "btn_actualizar"
        Me.btn_actualizar.Size = New System.Drawing.Size(109, 41)
        Me.btn_actualizar.TabIndex = 19
        Me.btn_actualizar.Text = "Buscar"
        '
        'btn_excelExport
        '
        Appearance2.Image = "logo_excel.png"
        Appearance2.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_excelExport.Appearance = Appearance2
        Me.btn_excelExport.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_excelExport.Enabled = False
        Me.btn_excelExport.ImageList = Me.ImageList1
        Me.btn_excelExport.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_excelExport.Location = New System.Drawing.Point(4, 31)
        Me.btn_excelExport.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_excelExport.Name = "btn_excelExport"
        Me.btn_excelExport.Size = New System.Drawing.Size(111, 41)
        Me.btn_excelExport.TabIndex = 13
        Me.btn_excelExport.Text = "Exportar "
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "logo_excel.png")
        '
        'txt_cliente
        '
        Me.txt_cliente.AlwaysInEditMode = True
        Me.txt_cliente.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_cliente.Location = New System.Drawing.Point(102, 39)
        Me.txt_cliente.Name = "txt_cliente"
        Me.txt_cliente.Size = New System.Drawing.Size(162, 19)
        Me.txt_cliente.TabIndex = 2
        '
        'lbl_cliente
        '
        Me.lbl_cliente.AutoSize = True
        Me.lbl_cliente.BackColor = System.Drawing.Color.Transparent
        Me.lbl_cliente.Location = New System.Drawing.Point(2, 42)
        Me.lbl_cliente.Name = "lbl_cliente"
        Me.lbl_cliente.Size = New System.Drawing.Size(42, 13)
        Me.lbl_cliente.TabIndex = 0
        Me.lbl_cliente.Text = "Cliente:"
        '
        'grp_filtros
        '
        Me.grp_filtros.Controls.Add(Me.UltraExpandableGroupBoxPanel2)
        Me.grp_filtros.Expanded = False
        Me.grp_filtros.ExpandedSize = New System.Drawing.Size(244, 113)
        Me.grp_filtros.HeaderClickAction = Infragistics.Win.Misc.GroupBoxHeaderClickAction.ToggleExpansion
        Appearance17.Cursor = System.Windows.Forms.Cursors.Hand
        Me.grp_filtros.HeaderHotTrackingAppearance = Appearance17
        Me.grp_filtros.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.LeftOutsideBorder
        Me.grp_filtros.Location = New System.Drawing.Point(544, 10)
        Me.grp_filtros.Margin = New System.Windows.Forms.Padding(10, 10, 2, 2)
        Me.grp_filtros.Name = "grp_filtros"
        Me.grp_filtros.ShowFocus = False
        Me.grp_filtros.Size = New System.Drawing.Size(23, 113)
        Me.grp_filtros.TabIndex = 21
        Me.grp_filtros.Text = "Filtros"
        Me.grp_filtros.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraExpandableGroupBoxPanel2
        '
        Me.UltraExpandableGroupBoxPanel2.Controls.Add(Me.lbl_compania)
        Me.UltraExpandableGroupBoxPanel2.Controls.Add(Me.btn_limpiar)
        Me.UltraExpandableGroupBoxPanel2.Controls.Add(Me.txt_compania)
        Me.UltraExpandableGroupBoxPanel2.Controls.Add(Me.lbl_ciudad)
        Me.UltraExpandableGroupBoxPanel2.Controls.Add(Me.txt_ciudad)
        Me.UltraExpandableGroupBoxPanel2.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraExpandableGroupBoxPanel2.Name = "UltraExpandableGroupBoxPanel2"
        Me.UltraExpandableGroupBoxPanel2.Size = New System.Drawing.Size(216, 107)
        Me.UltraExpandableGroupBoxPanel2.TabIndex = 0
        Me.UltraExpandableGroupBoxPanel2.Visible = False
        '
        'lbl_compania
        '
        Me.lbl_compania.AutoSize = True
        Me.lbl_compania.BackColor = System.Drawing.Color.Transparent
        Me.lbl_compania.Location = New System.Drawing.Point(3, 10)
        Me.lbl_compania.Name = "lbl_compania"
        Me.lbl_compania.Size = New System.Drawing.Size(57, 13)
        Me.lbl_compania.TabIndex = 0
        Me.lbl_compania.Text = "Compa�ia:"
        '
        'btn_limpiar
        '
        Me.btn_limpiar.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btn_limpiar.Location = New System.Drawing.Point(3, 65)
        Me.btn_limpiar.Name = "btn_limpiar"
        Me.btn_limpiar.Size = New System.Drawing.Size(75, 23)
        Me.btn_limpiar.TabIndex = 6
        Me.btn_limpiar.Text = "Limpiar"
        Me.btn_limpiar.UseVisualStyleBackColor = True
        '
        'txt_compania
        '
        Me.txt_compania.AlwaysInEditMode = True
        Me.txt_compania.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_compania.Location = New System.Drawing.Point(61, 7)
        Me.txt_compania.Name = "txt_compania"
        Me.txt_compania.Size = New System.Drawing.Size(147, 19)
        Me.txt_compania.TabIndex = 4
        '
        'lbl_ciudad
        '
        Me.lbl_ciudad.AutoSize = True
        Me.lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudad.Location = New System.Drawing.Point(3, 39)
        Me.lbl_ciudad.Name = "lbl_ciudad"
        Me.lbl_ciudad.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudad.TabIndex = 0
        Me.lbl_ciudad.Text = "Ciudad:"
        '
        'txt_ciudad
        '
        Me.txt_ciudad.AlwaysInEditMode = True
        Me.txt_ciudad.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ciudad.Location = New System.Drawing.Point(73, 35)
        Me.txt_ciudad.Name = "txt_ciudad"
        Me.txt_ciudad.Size = New System.Drawing.Size(133, 19)
        Me.txt_ciudad.TabIndex = 5
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.loading
        Me.PictureBox2.Location = New System.Drawing.Point(8, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Padding = New System.Windows.Forms.Padding(1)
        Me.PictureBox2.Size = New System.Drawing.Size(102, 17)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'txt_numConfirm
        '
        Me.txt_numConfirm.AlwaysInEditMode = True
        Me.txt_numConfirm.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_numConfirm.Location = New System.Drawing.Point(102, 13)
        Me.txt_numConfirm.Name = "txt_numConfirm"
        Me.txt_numConfirm.Size = New System.Drawing.Size(162, 19)
        Me.txt_numConfirm.TabIndex = 3
        '
        'lbl_numConfirm
        '
        Me.lbl_numConfirm.AutoSize = True
        Me.lbl_numConfirm.BackColor = System.Drawing.Color.Transparent
        Me.lbl_numConfirm.Location = New System.Drawing.Point(2, 16)
        Me.lbl_numConfirm.Name = "lbl_numConfirm"
        Me.lbl_numConfirm.Size = New System.Drawing.Size(99, 13)
        Me.lbl_numConfirm.TabIndex = 0
        Me.lbl_numConfirm.Text = "N�m. Confirmaci�n:"
        '
        'lbl_fechaBusqueda
        '
        Me.lbl_fechaBusqueda.AutoSize = True
        Me.lbl_fechaBusqueda.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fechaBusqueda.Location = New System.Drawing.Point(5, 14)
        Me.lbl_fechaBusqueda.Name = "lbl_fechaBusqueda"
        Me.lbl_fechaBusqueda.Size = New System.Drawing.Size(61, 13)
        Me.lbl_fechaBusqueda.TabIndex = 10
        Me.lbl_fechaBusqueda.Text = "Buscar por:"
        '
        'uce_tipoFecha
        '
        Me.uce_tipoFecha.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_tipoFecha.Location = New System.Drawing.Point(71, 12)
        Me.uce_tipoFecha.Name = "uce_tipoFecha"
        Me.uce_tipoFecha.Size = New System.Drawing.Size(121, 21)
        Me.uce_tipoFecha.TabIndex = 3
        '
        'lbl_llegada
        '
        Me.lbl_llegada.AutoSize = True
        Me.lbl_llegada.BackColor = System.Drawing.Color.Transparent
        Me.lbl_llegada.Location = New System.Drawing.Point(5, 43)
        Me.lbl_llegada.Name = "lbl_llegada"
        Me.lbl_llegada.Size = New System.Drawing.Size(41, 13)
        Me.lbl_llegada.TabIndex = 0
        Me.lbl_llegada.Text = "Desde:"
        '
        'dtp_in
        '
        Me.dtp_in.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_in.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_in.Location = New System.Drawing.Point(71, 40)
        Me.dtp_in.Name = "dtp_in"
        Me.dtp_in.ShowCheckBox = True
        Me.dtp_in.Size = New System.Drawing.Size(121, 20)
        Me.dtp_in.TabIndex = 1
        '
        'lbl_salida
        '
        Me.lbl_salida.AutoSize = True
        Me.lbl_salida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_salida.Location = New System.Drawing.Point(5, 71)
        Me.lbl_salida.Name = "lbl_salida"
        Me.lbl_salida.Size = New System.Drawing.Size(38, 13)
        Me.lbl_salida.TabIndex = 0
        Me.lbl_salida.Text = "Hasta:"
        '
        'dtp_out
        '
        Me.dtp_out.Checked = False
        Me.dtp_out.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_out.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_out.Location = New System.Drawing.Point(71, 68)
        Me.dtp_out.Name = "dtp_out"
        Me.dtp_out.ShowCheckBox = True
        Me.dtp_out.Size = New System.Drawing.Size(121, 20)
        Me.dtp_out.TabIndex = 2
        '
        'UltraExpandableGroupBox1
        '
        Me.UltraExpandableGroupBox1.Controls.Add(Me.UltraExpandableGroupBoxPanel1)
        Me.UltraExpandableGroupBox1.ExpandedSize = New System.Drawing.Size(0, 0)
        Me.UltraExpandableGroupBox1.Location = New System.Drawing.Point(676, -159)
        Me.UltraExpandableGroupBox1.Name = "UltraExpandableGroupBox1"
        Me.UltraExpandableGroupBox1.Size = New System.Drawing.Size(200, 185)
        Me.UltraExpandableGroupBox1.TabIndex = 21
        Me.UltraExpandableGroupBox1.Text = "UltraExpandableGroupBox1"
        '
        'UltraExpandableGroupBoxPanel1
        '
        Me.UltraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraExpandableGroupBoxPanel1.Location = New System.Drawing.Point(3, 19)
        Me.UltraExpandableGroupBoxPanel1.Name = "UltraExpandableGroupBoxPanel1"
        Me.UltraExpandableGroupBoxPanel1.Size = New System.Drawing.Size(194, 163)
        Me.UltraExpandableGroupBoxPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.grp_transacciones)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel1.Controls.Add(Me.grp_filtros)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox3)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1121, 131)
        Me.FlowLayoutPanel1.TabIndex = 21
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.uce_source)
        Me.GroupBox1.Controls.Add(Me.lblSource)
        Me.GroupBox1.Controls.Add(Me.lbl_fechaBusqueda)
        Me.GroupBox1.Controls.Add(Me.uce_tipoFecha)
        Me.GroupBox1.Controls.Add(Me.lbl_llegada)
        Me.GroupBox1.Controls.Add(Me.dtp_in)
        Me.GroupBox1.Controls.Add(Me.lbl_salida)
        Me.GroupBox1.Controls.Add(Me.dtp_out)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 2)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(10, 2, 2, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(201, 123)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'grp_transacciones
        '
        Me.grp_transacciones.Controls.Add(Me.UltraExpandableGroupBoxPanel3)
        Me.grp_transacciones.Expanded = False
        Me.grp_transacciones.ExpandedSize = New System.Drawing.Size(204, 113)
        Me.grp_transacciones.HeaderClickAction = Infragistics.Win.Misc.GroupBoxHeaderClickAction.ToggleExpansion
        Appearance14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.grp_transacciones.HeaderHotTrackingAppearance = Appearance14
        Me.grp_transacciones.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.LeftOutsideBorder
        Me.grp_transacciones.Location = New System.Drawing.Point(223, 10)
        Me.grp_transacciones.Margin = New System.Windows.Forms.Padding(10, 10, 2, 2)
        Me.grp_transacciones.Name = "grp_transacciones"
        Me.grp_transacciones.ShowFocus = False
        Me.grp_transacciones.Size = New System.Drawing.Size(23, 113)
        Me.grp_transacciones.TabIndex = 22
        Me.grp_transacciones.Text = "Transacciones"
        Me.grp_transacciones.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraExpandableGroupBoxPanel3
        '
        Me.UltraExpandableGroupBoxPanel3.Controls.Add(Me.lbl_transaccionespms)
        Me.UltraExpandableGroupBoxPanel3.Controls.Add(Me.opt_transacciones)
        Me.UltraExpandableGroupBoxPanel3.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraExpandableGroupBoxPanel3.Name = "UltraExpandableGroupBoxPanel3"
        Me.UltraExpandableGroupBoxPanel3.Size = New System.Drawing.Size(176, 107)
        Me.UltraExpandableGroupBoxPanel3.TabIndex = 0
        Me.UltraExpandableGroupBoxPanel3.Visible = False
        '
        'lbl_transaccionespms
        '
        Me.lbl_transaccionespms.AutoSize = True
        Me.lbl_transaccionespms.BackColor = System.Drawing.Color.Transparent
        Me.lbl_transaccionespms.Location = New System.Drawing.Point(3, 4)
        Me.lbl_transaccionespms.Name = "lbl_transaccionespms"
        Me.lbl_transaccionespms.Size = New System.Drawing.Size(118, 13)
        Me.lbl_transaccionespms.TabIndex = 19
        Me.lbl_transaccionespms.Text = "Transacciones en PMS"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CmbSearch1)
        Me.GroupBox2.Controls.Add(Me.lbl_segmento)
        Me.GroupBox2.Controls.Add(Me.lbl_empresa)
        Me.GroupBox2.Controls.Add(Me.cmbSegmento)
        Me.GroupBox2.Controls.Add(Me.txt_cliente)
        Me.GroupBox2.Controls.Add(Me.lbl_cliente)
        Me.GroupBox2.Controls.Add(Me.txt_numConfirm)
        Me.GroupBox2.Controls.Add(Me.lbl_numConfirm)
        Me.GroupBox2.Location = New System.Drawing.Point(258, 2)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(10, 2, 2, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(274, 120)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_actualizar)
        Me.GroupBox3.Controls.Add(Me.btn_excelExport)
        Me.GroupBox3.Controls.Add(Me.PictureBox2)
        Me.GroupBox3.Location = New System.Drawing.Point(579, 2)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(10, 2, 2, 2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(124, 120)
        Me.GroupBox3.TabIndex = 21
        Me.GroupBox3.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel1.Controls.Add(Me.uce_rubro)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(721, 132)
        Me.Panel1.TabIndex = 0
        '
        'uce_rubro
        '
        Me.uce_rubro.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_rubro.Location = New System.Drawing.Point(1037, 95)
        Me.uce_rubro.Name = "uce_rubro"
        Me.uce_rubro.Size = New System.Drawing.Size(121, 21)
        Me.uce_rubro.TabIndex = 4
        Me.uce_rubro.Visible = False
        '
        'CmbSearch1
        '
        Me.CmbSearch1.Location = New System.Drawing.Point(102, 65)
        Me.CmbSearch1.Name = "CmbSearch1"
        Me.CmbSearch1.Size = New System.Drawing.Size(162, 19)
        Me.CmbSearch1.TabIndex = 25
        '
        'Reservaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(721, 400)
        Me.Controls.Add(Me.UltraGrid1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Reservaciones"
        Me.Text = "Reservaciones"
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_source, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_transacciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_cliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grp_filtros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_filtros.ResumeLayout(False)
        Me.UltraExpandableGroupBoxPanel2.ResumeLayout(False)
        Me.UltraExpandableGroupBoxPanel2.PerformLayout()
        CType(Me.txt_compania, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ciudad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_numConfirm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_tipoFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraExpandableGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraExpandableGroupBox1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grp_transacciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_transacciones.ResumeLayout(False)
        Me.UltraExpandableGroupBoxPanel3.ResumeLayout(False)
        Me.UltraExpandableGroupBoxPanel3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.uce_rubro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents dtp_in As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_llegada As System.Windows.Forms.Label
    Friend WithEvents lbl_salida As System.Windows.Forms.Label
    Friend WithEvents dtp_out As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_compania As System.Windows.Forms.Label
    Friend WithEvents lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents lbl_numConfirm As System.Windows.Forms.Label
    Friend WithEvents lbl_cliente As System.Windows.Forms.Label
    Friend WithEvents txt_numConfirm As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_cliente As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_compania As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_ciudad As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_limpiar As System.Windows.Forms.Button
    Friend WithEvents uce_tipoFecha As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_fechaBusqueda As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents uce_source As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lblSource As System.Windows.Forms.Label
    Friend WithEvents UltraGridExcelExporter1 As Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter
    Friend WithEvents btn_excelExport As Infragistics.Win.Misc.UltraButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents cmbSegmento As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_empresa As System.Windows.Forms.Label
    Friend WithEvents lbl_segmento As System.Windows.Forms.Label
    Friend WithEvents btn_actualizar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents opt_transacciones As Infragistics.Win.UltraWinEditors.UltraOptionSet
    Friend WithEvents grp_filtros As Infragistics.Win.Misc.UltraExpandableGroupBox
    Friend WithEvents UltraExpandableGroupBoxPanel2 As Infragistics.Win.Misc.UltraExpandableGroupBoxPanel
    Friend WithEvents UltraExpandableGroupBox1 As Infragistics.Win.Misc.UltraExpandableGroupBox
    Friend WithEvents UltraExpandableGroupBoxPanel1 As Infragistics.Win.Misc.UltraExpandableGroupBoxPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grp_transacciones As Infragistics.Win.Misc.UltraExpandableGroupBox
    Friend WithEvents UltraExpandableGroupBoxPanel3 As Infragistics.Win.Misc.UltraExpandableGroupBoxPanel
    Friend WithEvents lbl_transaccionespms As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents uce_rubro As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents CmbSearch1 As callCenter.cmbSearch
End Class
