<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
				Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(login))
				Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
				Me.lbl_conacyt = New System.Windows.Forms.Label()
				Me.PictureBox3 = New System.Windows.Forms.PictureBox()
				Me.lbl_version = New System.Windows.Forms.Label()
				Me.PictureBox2 = New System.Windows.Forms.PictureBox()
				Me.btn_configurar = New System.Windows.Forms.Button()
				Me.lbl_mesnsage = New System.Windows.Forms.Label()
				Me.btn_cerrar = New System.Windows.Forms.Button()
				Me.btn_entrar = New System.Windows.Forms.Button()
				Me.PictureBox1 = New System.Windows.Forms.PictureBox()
				Me.lbl_pass = New System.Windows.Forms.Label()
				Me.txt_password = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
				Me.lbl_usr = New System.Windows.Forms.Label()
				Me.txt_usuario = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
				CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
				Me.UltraGroupBox1.SuspendLayout()
				CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
				CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
				CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
				CType(Me.txt_password, System.ComponentModel.ISupportInitialize).BeginInit()
				CType(Me.txt_usuario, System.ComponentModel.ISupportInitialize).BeginInit()
				Me.SuspendLayout()
				'
				'UltraGroupBox1
				'
				Me.UltraGroupBox1.Controls.Add(Me.lbl_conacyt)
				Me.UltraGroupBox1.Controls.Add(Me.PictureBox3)
				Me.UltraGroupBox1.Controls.Add(Me.lbl_version)
				Me.UltraGroupBox1.Controls.Add(Me.PictureBox2)
				Me.UltraGroupBox1.Controls.Add(Me.btn_configurar)
				Me.UltraGroupBox1.Controls.Add(Me.lbl_mesnsage)
				Me.UltraGroupBox1.Controls.Add(Me.btn_cerrar)
				Me.UltraGroupBox1.Controls.Add(Me.btn_entrar)
				Me.UltraGroupBox1.Controls.Add(Me.PictureBox1)
				Me.UltraGroupBox1.Controls.Add(Me.lbl_pass)
				Me.UltraGroupBox1.Controls.Add(Me.txt_password)
				Me.UltraGroupBox1.Controls.Add(Me.lbl_usr)
				Me.UltraGroupBox1.Controls.Add(Me.txt_usuario)
				Me.UltraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
				Me.UltraGroupBox1.Location = New System.Drawing.Point(0, 0)
				Me.UltraGroupBox1.Name = "UltraGroupBox1"
				Me.UltraGroupBox1.Size = New System.Drawing.Size(367, 286)
				Me.UltraGroupBox1.TabIndex = 0
				Me.UltraGroupBox1.Text = "Iniciar sesi�n"
				Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
				'
				'lbl_conacyt
				'
				Me.lbl_conacyt.BackColor = System.Drawing.Color.Transparent
				Me.lbl_conacyt.Location = New System.Drawing.Point(35, 291)
				Me.lbl_conacyt.Name = "lbl_conacyt"
				Me.lbl_conacyt.Size = New System.Drawing.Size(179, 65)
				Me.lbl_conacyt.TabIndex = 9
				Me.lbl_conacyt.Text = "Proyecto apoyado por el programa de est�mulos a la investigaci�n, desarrollo tecn" & _
						"ol�gico e innovaci�n del Conacyt"
				Me.lbl_conacyt.Visible = False
				'
				'PictureBox3
				'
				Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
				Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
				Me.PictureBox3.Location = New System.Drawing.Point(220, 291)
				Me.PictureBox3.Name = "PictureBox3"
				Me.PictureBox3.Size = New System.Drawing.Size(100, 76)
				Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
				Me.PictureBox3.TabIndex = 8
				Me.PictureBox3.TabStop = False
				Me.PictureBox3.Visible = False
				'
				'lbl_version
				'
				Me.lbl_version.AutoSize = True
				Me.lbl_version.BackColor = System.Drawing.Color.Transparent
				Me.lbl_version.Location = New System.Drawing.Point(298, 1)
				Me.lbl_version.Name = "lbl_version"
				Me.lbl_version.Size = New System.Drawing.Size(39, 13)
				Me.lbl_version.TabIndex = 7
				Me.lbl_version.Text = "Label3"
				'
				'PictureBox2
				'
				Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.loading
				Me.PictureBox2.Location = New System.Drawing.Point(133, 209)
				Me.PictureBox2.Name = "PictureBox2"
				Me.PictureBox2.Size = New System.Drawing.Size(100, 15)
				Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
				Me.PictureBox2.TabIndex = 6
				Me.PictureBox2.TabStop = False
				Me.PictureBox2.Visible = False
				'
				'btn_configurar
				'
				Me.btn_configurar.Location = New System.Drawing.Point(38, 248)
				Me.btn_configurar.Name = "btn_configurar"
				Me.btn_configurar.Size = New System.Drawing.Size(75, 23)
				Me.btn_configurar.TabIndex = 5
				Me.btn_configurar.Text = "Configurar"
				Me.btn_configurar.UseVisualStyleBackColor = True
				'
				'lbl_mesnsage
				'
				Me.lbl_mesnsage.BackColor = System.Drawing.Color.Transparent
				Me.lbl_mesnsage.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
				Me.lbl_mesnsage.ForeColor = System.Drawing.Color.Red
				Me.lbl_mesnsage.Location = New System.Drawing.Point(3, 223)
				Me.lbl_mesnsage.Name = "lbl_mesnsage"
				Me.lbl_mesnsage.Size = New System.Drawing.Size(361, 11)
				Me.lbl_mesnsage.TabIndex = 0
				Me.lbl_mesnsage.Text = "cargando.."
				Me.lbl_mesnsage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
				'
				'btn_cerrar
				'
				Me.btn_cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
				Me.btn_cerrar.Location = New System.Drawing.Point(245, 248)
				Me.btn_cerrar.Name = "btn_cerrar"
				Me.btn_cerrar.Size = New System.Drawing.Size(75, 23)
				Me.btn_cerrar.TabIndex = 4
				Me.btn_cerrar.Text = "Cerrar"
				Me.btn_cerrar.UseVisualStyleBackColor = True
				'
				'btn_entrar
				'
				Me.btn_entrar.Location = New System.Drawing.Point(164, 248)
				Me.btn_entrar.Name = "btn_entrar"
				Me.btn_entrar.Size = New System.Drawing.Size(75, 23)
				Me.btn_entrar.TabIndex = 3
				Me.btn_entrar.Text = "Entrar"
				Me.btn_entrar.UseVisualStyleBackColor = True
				'
				'PictureBox1
				'
				Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
				Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
				Me.PictureBox1.Location = New System.Drawing.Point(12, 32)
				Me.PictureBox1.Name = "PictureBox1"
				Me.PictureBox1.Size = New System.Drawing.Size(343, 109)
				Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
				Me.PictureBox1.TabIndex = 4
				Me.PictureBox1.TabStop = False
				'
				'lbl_pass
				'
				Me.lbl_pass.AutoSize = True
				Me.lbl_pass.BackColor = System.Drawing.Color.Transparent
				Me.lbl_pass.Location = New System.Drawing.Point(35, 177)
				Me.lbl_pass.Name = "lbl_pass"
				Me.lbl_pass.Size = New System.Drawing.Size(64, 13)
				Me.lbl_pass.TabIndex = 4
				Me.lbl_pass.Text = "Contrase�a:"
				'
				'txt_password
				'
				Me.txt_password.AlwaysInEditMode = True
				Me.txt_password.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
				Me.txt_password.Location = New System.Drawing.Point(140, 174)
				Me.txt_password.Name = "txt_password"
				Me.txt_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
				Me.txt_password.Size = New System.Drawing.Size(180, 19)
				Me.txt_password.TabIndex = 2
				'
				'lbl_usr
				'
				Me.lbl_usr.AutoSize = True
				Me.lbl_usr.BackColor = System.Drawing.Color.Transparent
				Me.lbl_usr.Location = New System.Drawing.Point(35, 151)
				Me.lbl_usr.Name = "lbl_usr"
				Me.lbl_usr.Size = New System.Drawing.Size(99, 13)
				Me.lbl_usr.TabIndex = 4
				Me.lbl_usr.Text = "Nombre de usuario:"
				'
				'txt_usuario
				'
				Me.txt_usuario.AlwaysInEditMode = True
				Me.txt_usuario.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
				Me.txt_usuario.Location = New System.Drawing.Point(140, 147)
				Me.txt_usuario.Name = "txt_usuario"
				Me.txt_usuario.Size = New System.Drawing.Size(180, 19)
				Me.txt_usuario.TabIndex = 1
				'
				'login
				'
				Me.AcceptButton = Me.btn_entrar
				Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
				Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
				Me.BackColor = System.Drawing.SystemColors.Control
				Me.CancelButton = Me.btn_cerrar
				Me.ClientSize = New System.Drawing.Size(367, 286)
				Me.Controls.Add(Me.UltraGroupBox1)
				Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
				Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
				Me.Name = "login"
				Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
				Me.Text = "login"
				CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
				Me.UltraGroupBox1.ResumeLayout(False)
				Me.UltraGroupBox1.PerformLayout()
				CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
				CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
				CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
				CType(Me.txt_password, System.ComponentModel.ISupportInitialize).EndInit()
				CType(Me.txt_usuario, System.ComponentModel.ISupportInitialize).EndInit()
				Me.ResumeLayout(False)

		End Sub
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents lbl_pass As System.Windows.Forms.Label
    Friend WithEvents txt_password As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_usr As System.Windows.Forms.Label
    Friend WithEvents txt_usuario As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_cerrar As System.Windows.Forms.Button
    Friend WithEvents btn_entrar As System.Windows.Forms.Button
    Friend WithEvents lbl_mesnsage As System.Windows.Forms.Label
    Friend WithEvents btn_configurar As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_version As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_conacyt As System.Windows.Forms.Label
End Class
