Public Class dataCall_
    Private _iniciada As Boolean = False
    Private _confirmando As Boolean = False

    'llamada
    Private _idCall As Integer
    Private _fechaInicio As DateTime
    Private _fechaTermina As DateTime

    'cliente
    Private _nombre As String
    Private _masculino As Boolean = True
    Private _desea As Integer
    Private _telefono As String
    Private _correo As String
    Private _deseaPaquete As Boolean = True
    Private _comentarios As String
    Private _id_MedioPromocion As String
    Private _HotelName As String = ""
    Private _reservacion_paquete As New dataBusqueda_paquete
    Private _grid As Infragistics.Win.UltraWinGrid.UltraGrid

    Public ReadOnly Property iniciada() As Boolean
        Get
            Return _iniciada
        End Get
    End Property

    Public Property confirmando() As Boolean
        Get
            Return _confirmando
        End Get
        Set(ByVal value As Boolean)
            _confirmando = value
        End Set
    End Property

    Public Property idCall() As Integer
        Get
            Return _idCall
        End Get
        Set(ByVal value As Integer)
            _idCall = value
            _iniciada = True
        End Set
    End Property

    Public Property fechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property

    Public Property fechaTermina() As DateTime
        Get
            Return _fechaTermina
        End Get
        Set(ByVal value As DateTime)
            _fechaTermina = value
        End Set
    End Property

    Public Property grid() As Infragistics.Win.UltraWinGrid.UltraGrid
        Get
            Return _grid
        End Get
        Set(ByVal value As Infragistics.Win.UltraWinGrid.UltraGrid)
            _grid = value
        End Set
    End Property

    Public btnHot As Infragistics.Win.Misc.UltraButton
    Public btnVue As Infragistics.Win.Misc.UltraButton
    Public btnAut As Infragistics.Win.Misc.UltraButton
    Public btnAct As Infragistics.Win.Misc.UltraButton

    Public Property id_MedioPromocion() As String
        Get
            Return _id_MedioPromocion
        End Get
        Set(ByVal value As String)
            _id_MedioPromocion = value
        End Set
    End Property

    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property masculino() As Boolean
        Get
            Return _masculino
        End Get
        Set(ByVal value As Boolean)
            _masculino = value
        End Set
    End Property

    Public Property desea() As Integer
        Get
            Return _desea
        End Get
        Set(ByVal value As Integer)
            _desea = value
        End Set
    End Property

    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

    Public Property correo() As String
        Get
            Return _correo
        End Get
        Set(ByVal value As String)
            _correo = value
        End Set
    End Property

    Public Property deseaPaquete() As Boolean
        Get
            Return _deseaPaquete
        End Get
        Set(ByVal value As Boolean)
            _deseaPaquete = value
        End Set
    End Property

    Public Property comentarios() As String
        Get
            Return _comentarios
        End Get
        Set(ByVal value As String)
            _comentarios = value
        End Set
    End Property

    Public Property nombreHotel() As String
        Get
            Return _HotelName
        End Get
        Set(ByVal value As String)
            _HotelName = value
        End Set
    End Property


    Public ReadOnly Property reservacion_paquete() As dataBusqueda_paquete
        Get
            Return _reservacion_paquete
        End Get
    End Property

    '-----

    Public Event After_UpdateGrid()

    Private Function calcula_total(ByVal subT As String, ByVal mone As String, ByVal taxe As String, ByVal plus As String) As String
        If subT IsNot Nothing AndAlso subT.Contains("$") Then subT = subT.Replace("$", "").Substring(0, subT.Replace("$", "").IndexOf(" "))

        Dim total As Double
        If plus IsNot Nothing AndAlso CBool(plus) Then
            total = CDbl(subT)
        Else
            total = IIf(subT IsNot Nothing, CDbl(subT), 0) + IIf(taxe IsNot Nothing, CDbl(taxe), 0)
        End If

        Return CapaPresentacion.Common.moneda.FormateaMoneda(total, mone) ' String.Format("{0:C}", total) + " " + mone
    End Function

    Private Function TieneHotelNoConfirmado() As Boolean
        For Each datos_busqueda As dataBusqueda_hotel_test In reservacion_paquete.lista_Hoteles
            If datos_busqueda.confirmada Then Continue For
            Return True
        Next
        Return False
    End Function

    Private Function TieneVueloNoConfirmado() As Boolean
        For Each datos_busqueda As dataBusqueda_vuelo In reservacion_paquete.lista_Vuelos
            If datos_busqueda.confirmada Then Continue For
            Return True
        Next
        Return False
        Return False
    End Function

    Private Function TieneAutoNoConfirmado() As Boolean
        For Each datos_busqueda As dataBusqueda_auto_test In reservacion_paquete.lista_Autos
            If datos_busqueda.confirmada Then Continue For
            Return True
        Next
        Return False
    End Function

    Private Function TieneActividadNoConfirmada() As Boolean
        For Each datos_busqueda As dataBusqueda_actividad_test In reservacion_paquete.lista_Actividades
            If datos_busqueda.confirmada Then Continue For
            Return True
        Next
    End Function

    Private Sub update_grid()
        Dim col_id As New DataColumn("id", System.Type.GetType("System.Int32"))
        col_id.AutoIncrement = True
        col_id.AutoIncrementSeed = 1
        col_id.AutoIncrementStep = 1

        Dim ds As New DataSet("midata")
        ds.Tables.Add(New DataTable("mitabla"))
        ds.Tables(0).Columns.AddRange(New DataColumn() {col_id, New DataColumn("Tipo"), New DataColumn("Fecha"), New DataColumn("Lugar"), New DataColumn("Total"), New DataColumn("Confirmada")})

        'hoteles
        For Each data As dataBusqueda_hotel_test In _reservacion_paquete.lista_Hoteles
            Dim str_confirm As String
            If data.confirmada Then str_confirm = CapaPresentacion.Idiomas.get_str("str_0227_si") Else str_confirm = CapaPresentacion.Idiomas.get_str("str_0228_no")

            Dim total As String = ""
            If Not IsNothing(data.AltTotalStay) Then
                total = calcula_total(CDec(data.AltTotalStay) + CDec(data.AltTotalExtras), data.AltCurrency, data.AltTax, data.PlusTax)
            End If


            Dim dr As DataRow = ds.Tables(0).NewRow()
            data.t_id = dr.Item("id")
            dr.Item("Tipo") = CapaPresentacion.Idiomas.get_str("str_0032_hotel")
            If Not IsNothing(data.llegada) Then dr.Item("Fecha") = data.llegada.ToString("dd/MMM/yyyy")
            If Not IsNothing(data.ciudad_nombre) Then dr.Item("Lugar") = data.ciudad_nombre
            dr.Item("Total") = total
            dr.Item("Confirmada") = str_confirm
            ds.Tables(0).Rows.Add(dr)
        Next

        'vuelos
        For Each data As dataBusqueda_vuelo In _reservacion_paquete.lista_Vuelos
            Dim str_confirm As String
            If data.confirmada Then str_confirm = CapaPresentacion.Idiomas.get_str("str_0227_si") Else str_confirm = CapaPresentacion.Idiomas.get_str("str_0228_no")
            Dim i_min As Integer = 0
            Dim i_max As Integer = 0

            Dim s1 As String = ""
            Dim s2 As String = ""
            Dim s3 As String = ""

            If data.vuelo IsNot Nothing Then
                For i As Integer = 0 To data.vuelo.vuelo_ow.lista_escala.Count - 1
                    If CDate(data.vuelo.vuelo_ow.lista_escala(i).ArrivalDateTime) < CDate(data.vuelo.vuelo_ow.lista_escala(i_min).ArrivalDateTime) Then i_min = i
                    If CDate(data.vuelo.vuelo_ow.lista_escala(i).ArrivalDateTime) > CDate(data.vuelo.vuelo_ow.lista_escala(i_max).ArrivalDateTime) Then i_max = i
                Next
                Dim min_date As DateTime = CDate(data.vuelo.vuelo_ow.lista_escala(i_min).ArrivalDateTime)
                s1 = min_date.ToString("dd/MMM/yyyy")
                s2 = data.vuelo.vuelo_ow.lista_escala(i_min).dep_LocationCode + " - " + data.vuelo.vuelo_ow.lista_escala(i_max).arr_LocationCode
                s3 = data.vuelo.AltCantidad
            End If

            Dim dr As DataRow = ds.Tables(0).NewRow()
            data.t_id = dr.Item("id")
            dr.Item("Tipo") = CapaPresentacion.Idiomas.get_str("str_0301_vuelo")
            dr.Item("Fecha") = s1
            dr.Item("Lugar") = s2
            dr.Item("Total") = s3
            dr.Item("Confirmada") = str_confirm
            ds.Tables(0).Rows.Add(dr)
        Next

        'actividades
        For Each data As dataBusqueda_actividad_test In _reservacion_paquete.lista_Actividades
            Dim str_confirm As String
            If data.confirmada Then str_confirm = CapaPresentacion.Idiomas.get_str("str_0227_si") Else str_confirm = CapaPresentacion.Idiomas.get_str("str_0228_no")

            Dim dr As DataRow = ds.Tables(0).NewRow()
            data.t_id = dr.Item("id")
            dr.Item("Tipo") = CapaPresentacion.Idiomas.get_str("str_0285_actividad")

            Dim parse_from As String = ""
            Dim parse_to As String = ""

            If data.DayReservar.Length = 8 Then
                parse_from += CapaPresentacion.Common.DateDataFormat
                parse_to += "dd/MMM/yyyy"
            Else
                parse_from += "yyyyMMddHHmm"
                parse_to += "dd/MMM/yyyy HH:mm"
            End If
            dr.Item("Fecha") = DateTime.ParseExact(data.DayReservar, parse_from, Nothing).ToString(parse_to)
            dr.Item("Lugar") = data.ciudadActividad
            dr.Item("Total") = data.costoTotalFormato 'Format(CDbl(data.RateTypePrice) * (CDbl(data.PercentTax) / 100 + 1), "c") + " " + data.CurrencyCode
            dr.Item("Confirmada") = str_confirm
            ds.Tables(0).Rows.Add(dr)
        Next

        'autos
        For Each data As dataBusqueda_auto_test In _reservacion_paquete.lista_Autos
            Dim str_confirm As String
            If data.confirmada Then str_confirm = CapaPresentacion.Idiomas.get_str("str_0227_si") Else str_confirm = CapaPresentacion.Idiomas.get_str("str_0228_no")

            Dim dr As DataRow = ds.Tables(0).NewRow()
            data.t_id = dr.Item("id")
            dr.Item("Tipo") = CapaPresentacion.Idiomas.get_str("str_302_auto")
            dr.Item("Fecha") = data.fechaRecoger.ToString("dd/MMM/yyyy")
            dr.Item("Lugar") = data.ciudadRecoger
            dr.Item("Total") = data.TotalTotal
            dr.Item("Confirmada") = str_confirm
            ds.Tables(0).Rows.Add(dr)
        Next

        FIJA_GRID(_grid, ds)

        If Not CONST_ENABLED_HOTELES Then
            FIJA_BTN(btnHot, False)
            'Else If TieneHotelNoConfirmado() Then FIJA_BTN(btnHot, False) 
        Else
            FIJA_BTN(btnHot, True)
        End If

        If Not CONST_ENABLED_VUELOS Then FIJA_BTN(btnVue, False) Else If TieneVueloNoConfirmado() Then FIJA_BTN(btnVue, False) Else FIJA_BTN(btnVue, True)
        If Not CONST_ENABLED_AUTOS Then FIJA_BTN(btnAut, False) Else If TieneAutoNoConfirmado() Then FIJA_BTN(btnAut, False) Else FIJA_BTN(btnAut, True)
        If Not CONST_ENABLED_ACTIVIDADES Then FIJA_BTN(btnAct, False) Else If TieneActividadNoConfirmada() Then FIJA_BTN(btnAct, False) Else FIJA_BTN(btnAct, True)
        'act

        RaiseEvent After_UpdateGrid()
    End Sub

    Delegate Sub FIJA_GRID_Callback(ByVal ctl As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
    Private Sub FIJA_GRID(ByVal ctl As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
        Try
            If ctl.InvokeRequired Then
                Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                ctl.Parent.Parent.Invoke(d, New Object() {ctl, ds})
            Else
                ctl.DataSource = ds
                ctl.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns

                For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In ctl.Rows
                    If r.Cells("Confirmada").Value = CapaPresentacion.Idiomas.get_str("str_0227_si") Then r.Appearance.ForeColor = Color.Gray
                Next

                If ctl.DisplayLayout.Bands(0).Columns.Contains("id") Then ctl.DisplayLayout.Bands(0).Columns("id").Hidden = True
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_BTN_Callback(ByVal ctl As Infragistics.Win.Misc.UltraButton, ByVal enab As Boolean)
    Private Sub FIJA_BTN(ByVal ctl As Infragistics.Win.Misc.UltraButton, ByVal enab As Boolean)
        Try
            If ctl.InvokeRequired Then
                Dim d As New FIJA_BTN_Callback(AddressOf FIJA_BTN)
                ctl.Parent.Parent.Invoke(d, New Object() {ctl, enab})
            Else
                ctl.Enabled = enab
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Function ya_tiene_noConfirmada(ByVal rubro As enumServices) As Boolean
        'esta funcion es por que solo se puede reservar un hotel un auto y un vuelo a la vez por paquete

        Select Case rubro
            Case enumServices.Hotel
                For Each item As dataBusqueda_hotel_test In _reservacion_paquete.lista_Hoteles
                    If Not item.confirmada Then Return True
                Next
            Case enumServices.Auto
                For Each item As dataBusqueda_auto_test In _reservacion_paquete.lista_Autos
                    If Not item.confirmada Then Return True
                Next
            Case enumServices.Vuelo
                For Each item As dataBusqueda_vuelo In _reservacion_paquete.lista_Vuelos
                    If Not item.confirmada Then Return True
                Next
            Case enumServices.Actividad
                'si se pueden todas las actividades que se desee
        End Select

        Return False
    End Function

    Public Function add_rvaHotel(ByVal data As dataBusqueda_hotel_test, ByVal allow_multiple As Boolean) As Boolean
        If Not allow_multiple AndAlso ya_tiene_noConfirmada(enumServices.Hotel) Then
            Return False
        Else
            _reservacion_paquete.lista_Hoteles.Add(data)
            update_grid()
            Return True
        End If
    End Function

    Public Function add_rvaVuelo(ByVal data As dataBusqueda_vuelo, ByVal allow_multiple As Boolean) As Boolean
        If Not allow_multiple AndAlso ya_tiene_noConfirmada(enumServices.Vuelo) Then
            Return False
        Else
            _reservacion_paquete.lista_Vuelos.Add(data)
            update_grid()
            Return True
        End If
    End Function

    Public Sub add_rvaActividad(ByVal data As dataBusqueda_actividad_test)
        'este si permite multiples reservaciones
        _reservacion_paquete.lista_Actividades.Add(data)
        update_grid()
    End Sub

    Public Function add_rvaAuto(ByVal data As dataBusqueda_auto_test, ByVal allow_multiple As Boolean) As Boolean
        If Not allow_multiple AndAlso ya_tiene_noConfirmada(enumServices.Auto) Then
            Return False
        Else
            _reservacion_paquete.lista_Autos.Add(data)
            update_grid()
            Return True
        End If
    End Function

    Public Sub rem_rvaHotel(ByVal t_id As Integer)
        Dim data_to_rem As dataBusqueda_hotel_test

        For Each data As dataBusqueda_hotel_test In _reservacion_paquete.lista_Hoteles
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0264_errCancelRes"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                data_to_rem = data
                Exit For
            End If
        Next

        If data_to_rem IsNot Nothing Then _reservacion_paquete.lista_Hoteles.Remove(data_to_rem)
        update_grid()
    End Sub

    Public Sub rem_rvaVuelo(ByVal t_id As Integer)
        Dim data_to_rem As dataBusqueda_vuelo

        For Each data As dataBusqueda_vuelo In _reservacion_paquete.lista_Vuelos
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0264_errCancelRes"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                data_to_rem = data
                Exit For
            End If
        Next

        If data_to_rem IsNot Nothing Then _reservacion_paquete.lista_Vuelos.Remove(data_to_rem)
        update_grid()
    End Sub

    Public Sub rem_rvaActividad(ByVal t_id As Integer)
        Dim data_to_rem As dataBusqueda_actividad_test

        For Each data As dataBusqueda_actividad_test In _reservacion_paquete.lista_Actividades
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0264_errCancelRes"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                data_to_rem = data
                Exit For
            End If
        Next

        If data_to_rem IsNot Nothing Then _reservacion_paquete.lista_Actividades.Remove(data_to_rem)
        update_grid()
    End Sub

    Public Sub rem_rvaAuto(ByVal t_id As Integer)
        Dim data_to_rem As dataBusqueda_auto_test

        For Each data As dataBusqueda_auto_test In _reservacion_paquete.lista_Autos
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0264_errCancelRes"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                data_to_rem = data
                Exit For
            End If
        Next

        If data_to_rem IsNot Nothing Then _reservacion_paquete.lista_Autos.Remove(data_to_rem)
        update_grid()
    End Sub

    Public Function get_dataBusquedaHotel(ByVal t_id As Integer) As dataBusqueda_hotel_test
        Dim data_to_return As dataBusqueda_hotel_test = Nothing

        For Each data As dataBusqueda_hotel_test In _reservacion_paquete.lista_Hoteles
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_303_errModResConf"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    data_to_return = Nothing
                    Exit For
                End If

                data_to_return = data
                Exit For
            End If
        Next

        Return data_to_return
    End Function

    Public Function get_dataBusquedaVuelo(ByVal t_id As Integer) As dataBusqueda_vuelo
        Dim data_to_return As dataBusqueda_vuelo = Nothing

        For Each data As dataBusqueda_vuelo In _reservacion_paquete.lista_Vuelos
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_303_errModResConf"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    data_to_return = Nothing
                    Exit For
                End If

                data_to_return = data
                Exit For
            End If
        Next

        Return data_to_return
    End Function

    Public Function get_dataBusquedaActividad(ByVal t_id As Integer) As dataBusqueda_actividad_test
        Dim data_to_return As dataBusqueda_actividad_test = Nothing

        For Each data As dataBusqueda_actividad_test In _reservacion_paquete.lista_Actividades
            If data.t_id = t_id Then
                If data.confirmada Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_303_errModResConf"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    data_to_return = Nothing
                    Exit For
                End If

                data_to_return = data
                Exit For
            End If
        Next

        Return data_to_return
    End Function

    'Public Function get_dataBusquedaAuto(ByVal t_id As Integer) As dataBusqueda_auto_test
    '    Dim data_to_return As dataBusqueda_auto_test = Nothing

    '    For Each data As dataBusqueda_auto_test In _reservacion_paquete.lista_Autos
    '        If data.t_id = t_id Then
    '            If data.confirmada Then
    '                data_to_return = Nothing
    '                Exit For
    '            End If

    '            data_to_return = data
    '            Exit For
    '        End If
    '    Next

    '    Return data_to_return
    'End Function

    Public Sub confirma_rvas()
        Try
            For Each data As dataBusqueda_hotel_test In _reservacion_paquete.lista_Hoteles
                data.confirmada = True
                _reservacion_paquete.Rubro = enumRubrosPassport.Hotel
            Next

            For Each data As dataBusqueda_vuelo In _reservacion_paquete.lista_Vuelos
                data.confirmada = True
                _reservacion_paquete.Rubro = enumRubrosPassport.Vuelos
            Next

            For Each data As dataBusqueda_actividad_test In _reservacion_paquete.lista_Actividades
                data.confirmada = True
                _reservacion_paquete.Rubro = enumRubrosPassport.Actividades
            Next

            For Each data As dataBusqueda_auto_test In _reservacion_paquete.lista_Autos
                data.confirmada = True
                _reservacion_paquete.Rubro = enumRubrosPassport.Autos
            Next

            update_grid()
        Catch ex As Exception
            'messagebox("error - confirma_rvas")
        End Try
    End Sub

    Public Sub vacia()
        _reservacion_paquete.lista_Hoteles.Clear()
        _reservacion_paquete.lista_Vuelos.Clear()
        _reservacion_paquete.lista_Actividades.Clear()
        _reservacion_paquete.lista_Autos.Clear()
        _reservacion_paquete = New dataBusqueda_paquete
    End Sub

    '-----

    Public Function Clone() As dataCall_
        Dim temp As New dataCall_

        temp._iniciada = _iniciada
        temp._idCall = _idCall
        temp._fechaInicio = _fechaInicio
        temp._fechaTermina = _fechaTermina
        temp._nombre = _nombre
        temp._masculino = _masculino
        temp._desea = _desea
        temp._telefono = _telefono
        temp._correo = _correo
        temp._deseaPaquete = _deseaPaquete
        temp._comentarios = _comentarios
        temp._reservacion_paquete.lista_Hoteles = temp._reservacion_paquete.lista_Hoteles
        temp._reservacion_paquete.lista_Vuelos = _reservacion_paquete.lista_Vuelos
        temp._reservacion_paquete.lista_Actividades = _reservacion_paquete.lista_Actividades
        temp._reservacion_paquete.lista_Autos = _reservacion_paquete.lista_Autos
        temp._grid = _grid
        temp._id_MedioPromocion = _id_MedioPromocion
        Return temp
    End Function

End Class
