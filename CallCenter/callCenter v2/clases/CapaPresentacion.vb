Public Class CapaPresentacion

    Public Class Common

        Public Shared ReadOnly Property DateDataFormat() As String
            Get
                Return "yyyyMMdd"
            End Get
        End Property

        Public Shared ReadOnly Property DateRulesFormat() As String
            Get
                Return "yyyy/MM/dd"
            End Get
        End Property

        'Delegate Sub FijaProgressBarCallback(ByVal frmInvoker As Form, ByVal valor As Integer, ByVal visible As Boolean)
        'Public Shared Sub FijaProgressBar(ByVal frmInvoker As Form, ByVal valor As Integer, ByVal visible As Boolean)
        '    Try
        '        If misForms.form1.UltraStatusBar1.InvokeRequired Then
        '            Dim d As New FijaProgressBarCallback(AddressOf FijaProgressBar)
        '            frmInvoker.Invoke(d, New Object() {frmInvoker, valor, visible})
        '        Else
        '            If valor > 100 Then valor = 100

        '            If frmInvoker Is misForms.busquedaHotel_ultimoActivo Then
        '                misForms.form1.UltraStatusBar1.Panels("pnl_3").ProgressBarInfo.Value = valor
        '                misForms.form1.UltraStatusBar1.Refresh()
        '                misForms.form1.UltraStatusBar1.Panels("pnl_3").Visible = visible
        '            End If
        '        End If
        '    Catch ex As Exception
        '        'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
        '        MsgBox("errooooor")
        '    End Try
        'End Sub

        Delegate Sub FIJA_CMB_Callback(ByVal txt As Infragistics.Win.UltraWinEditors.UltraComboEditor, ByVal text As String)
        Private Shared Sub FIJA_CMB(ByVal cmb As Infragistics.Win.UltraWinEditors.UltraComboEditor, ByVal value As String)
            Try
                If cmb.InvokeRequired Then
                    Dim d As New FIJA_CMB_Callback(AddressOf FIJA_CMB)
                    cmb.Invoke(d, New Object() {cmb, value})
                Else
                    cmb.Value = value
                End If
            Catch ex As Exception
                MsgBox("Error: " & ex.ToString)
            End Try
        End Sub

        Delegate Sub FIJA_TXT_Callback(ByVal txt As Infragistics.Win.UltraWinEditors.UltraTextEditor, ByVal text As String)
        Private Shared Sub FIJA_TXT(ByVal txt As Infragistics.Win.UltraWinEditors.UltraTextEditor, ByVal text As String)
            Try
                If txt.InvokeRequired Then
                    Dim d As New FIJA_TXT_Callback(AddressOf FIJA_TXT)
                    txt.Invoke(d, New Object() {txt, text})
                Else
                    txt.Text = text
                End If
            Catch ex As Exception
                MsgBox("Error: " & ex.ToString)
            End Try
        End Sub

        Delegate Sub Fija_CTLVISIBLE_Callback(ByVal ctl As Control, ByVal visible As Boolean)
        Public Shared Sub Fija_CTLVISIBLE(ByVal ctl As Control, ByVal visible As Boolean)
            Try
                If ctl.InvokeRequired Then
                    Dim d As New Fija_CTLVISIBLE_Callback(AddressOf Fija_CTLVISIBLE)
                    ctl.Invoke(d, New Object() {ctl, visible})
                Else
                    ctl.Visible = visible
                End If
            Catch ex As Exception
                'CapaLogicaNegocios.LogManager.agregar_registro(" - ", ex.Message)
                MsgBox("Error: " & ex.ToString)
            End Try
        End Sub

        Delegate Sub Fija_BrowserCode_Callback(ByVal wbr As WebBrowser, ByVal cod As String)
        Public Shared Sub Fija_BrowserCode(ByVal wbr As WebBrowser, ByVal cod As String)
            Try
                If wbr.InvokeRequired Then
                    Dim d As New Fija_BrowserCode_Callback(AddressOf Fija_BrowserCode)
                    wbr.Invoke(d, New Object() {wbr, cod})
                Else
                    wbr.DocumentText = cod
                End If
            Catch ex As Exception
                'CapaLogicaNegocios.LogManager.agregar_registro(" - ", ex.Message)
                MsgBox("Error: " & ex.ToString)
            End Try
        End Sub

        Public Shared Sub FormatUltraGrid(ByVal Grid As Infragistics.Win.UltraWinGrid.UltraGrid)
            Grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None
            Grid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
            Grid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None
            Grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect
        End Sub

        Public Shared Sub SetTabColor(ByVal frm As Form)
            Dim miColor As Color = herramientas.color_disponible
            misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.TabAppearance.BackColor = miColor
            misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.TabAppearance.BackColor2 = Color.FromArgb(100, 191, 219, 255)
            misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.TabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
            misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.SelectedTabAppearance.BackColor = miColor
            misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.SelectedTabAppearance.BackColor2 = Color.White
            misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.SelectedTabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        End Sub

        Public Shared Function HTMLDecode(ByVal sText) As String
            'falta reg ex para br y p - si espacios, si mayus, etc
            sText = sText.ToString.Replace("<p>", vbCrLf).Replace("<br>", vbCrLf).Replace("<BR>", vbCrLf).Replace("<br />", vbCrLf)

            Dim regEx As New System.Text.RegularExpressions.Regex("<.*?>")
            sText = regEx.Replace(sText, "")

            'For I = 1 To 255
            '    sText = Replace(sText, "&" + Chr(I) + "acute;", Chr(I))
            'Next

            sText = Replace(sText, "&quot;", Chr(34))
            sText = Replace(sText, "&lt;", Chr(60))
            sText = Replace(sText, "&gt;", Chr(62))
            sText = Replace(sText, "&amp;", Chr(38))
            sText = Replace(sText, "&nbsp;", Chr(32))
            sText = Replace(sText, "&apos;", "'")
            '''''''''''''''''''''''''''''''''''''
            sText = Replace(sText, "&ntilde;", "�")
            sText = Replace(sText, "&Ntilde;", "�")
            sText = Replace(sText, "&aacute;", "�")
            sText = Replace(sText, "&eacute;", "�")
            sText = Replace(sText, "&iacute;", "�")
            sText = Replace(sText, "&oacute;", "�")
            sText = Replace(sText, "&uacute;", "�")
            sText = Replace(sText, "&Aacute;", "�")
            sText = Replace(sText, "&Eacute;", "�")
            sText = Replace(sText, "&Iacute;", "�")
            sText = Replace(sText, "&Oacute;", "�")
            sText = Replace(sText, "&Uacute;", "�")
            sText = Replace(sText, "&euro;", "�")
            sText = Replace(sText, "&iquest;", "�")
            sText = Replace(sText, "&rdquo;", """")
            sText = Replace(sText, "&ldquo;", """")

            HTMLDecode = sText
        End Function

        Public Shared Function cargar_paises(ByVal uce As Infragistics.Win.UltraWinEditors.UltraComboEditor, Optional ByVal idCountry As String = "MX") As Boolean
            uce.Clear()
            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "consulta_paises"
            cmd.Connection = conn

            Dim reader As System.Data.SqlClient.SqlDataReader
            Dim previousConnectionState As ConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                reader = cmd.ExecuteReader()
                Using reader
                    Dim i As Integer = 0
                    While reader.Read
                        Dim idPais As String = ""
                        Dim nombre As String = ""

                        If Not reader.GetValue(0) Is System.DBNull.Value Then
                            idPais = reader.GetValue(0)
                        End If

                        If Not reader.GetValue(1) Is System.DBNull.Value Then
                            nombre = reader.GetValue(1)
                        End If

                        uce.Items.ValueList.ValueListItems.Add(idPais, nombre)

                        If idPais = idCountry Then
                            uce.SelectedIndex = i
                        End If

                        i += 1
                    End While
                End Using
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0037", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                Return False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try

            Return True
        End Function

        Public Shared Function cargar_estados(ByVal uce As Infragistics.Win.UltraWinEditors.UltraComboEditor, ByVal idCountry As String) As Boolean
            uce.Clear()
            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "select idEstado,nombre from estados where idPais='" & idCountry & "' order by nombre"
            cmd.Connection = conn

            Dim reader As System.Data.SqlClient.SqlDataReader
            Dim previousConnectionState As ConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                reader = cmd.ExecuteReader()
                Using reader
                    Dim i As Integer = 0
                    While reader.Read
                        Dim idEstado As String = ""
                        Dim nombre As String = ""

                        If Not reader.GetValue(0) Is System.DBNull.Value Then
                            idEstado = reader.GetValue(0)
                        End If

                        If Not reader.GetValue(1) Is System.DBNull.Value Then
                            nombre = reader.GetValue(1)
                        End If

                        uce.Items.ValueList.ValueListItems.Add(idEstado, nombre)
                        i += 1
                    End While
                    If uce.Items.Count > 0 Then
                        uce.SelectedIndex = 0
                    End If
                End Using
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0037", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                Return False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try

            Return True
        End Function

        Public Shared Function cargar_ciudad_por_cp(ByVal codigopostal As String, ByVal txtCiudad As Infragistics.Win.UltraWinEditors.UltraTextEditor, ByRef cmbEstado As Infragistics.Win.UltraWinEditors.UltraComboEditor, ByRef cmbPaises As Infragistics.Win.UltraWinEditors.UltraComboEditor) As Boolean

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "obtener_ciudad_por_cp"
            cmd.Parameters.Add("@codigopostal", SqlDbType.NVarChar).Value = codigopostal
            cmd.Connection = conn

            Dim reader As System.Data.SqlClient.SqlDataReader
            Dim previousConnectionState As ConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                reader = cmd.ExecuteReader()
                Using reader
                    reader.Read()
                    Dim idEstado As String = ""
                    Dim Ciudad As String = ""

                    If Not reader.GetValue(0) Is System.DBNull.Value Then
                        Ciudad = reader.GetValue(0)
                    End If

                    If Not reader.GetValue(1) Is System.DBNull.Value Then
                        idEstado = reader.GetValue(1)
                    End If

                    FIJA_CMB(cmbPaises, "MX")
                    FIJA_CMB(cmbEstado, idEstado)
                    FIJA_TXT(txtCiudad, Ciudad)


                    'cmbEstado.Value = idEstado

                End Using
            Catch ex As Exception
                Return False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try

            Return True
        End Function

        Public Class RibbonTools

            Private Shared tools As Infragistics.Win.UltraWinToolbars.RootToolsCollection

            Public Shared Sub inicializar()
                tools = misForms.form1.UltraToolbarsManager1.Tools
            End Sub

            Public Shared Function get_ButtonTool(ByVal key As String) As Infragistics.Win.UltraWinToolbars.ButtonTool
                Return CType(tools(key), Infragistics.Win.UltraWinToolbars.ButtonTool)
            End Function

            Public Shared Function get_StateButtonTool(ByVal key As String) As Infragistics.Win.UltraWinToolbars.StateButtonTool
                Return CType(tools(key), Infragistics.Win.UltraWinToolbars.StateButtonTool)
            End Function

        End Class

        Public Shared Sub NumberGrid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid)
            Dim cont As Integer = 1
            For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In grid.Rows
                r.Cells("Numero").Value = cont.ToString
                cont += 1
            Next

            'Return dt.DefaultView
        End Sub

        Public Class moneda

            Public Shared lista As List(Of String()) = GetArrPaises()

            Public Shared Sub FillUceMoneda(ByRef uce_moneda As Infragistics.Win.UltraWinEditors.UltraComboEditor)
                uce_moneda.Items.Clear()
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")
                'uce_moneda.Items.Add("", "")

                For Each pais As String() In lista
                    uce_moneda.Items.Add(pais(0), pais(1))

                    If moneda_selected = pais(0) Then
                        uce_moneda.SelectedIndex = uce_moneda.Items.Count - 1
                    End If
                Next

                If uce_moneda.SelectedIndex = -1 Then uce_moneda.SelectedIndex = 0
            End Sub

            Public Shared Function FormateaMoneda(ByVal cantidad As Double, ByVal moneda As String) As String
                Dim cultura As String = GetCulturaByCode(moneda)
                Dim culturaInfo As New System.Globalization.CultureInfo(cultura)
                culturaInfo.NumberFormat.CurrencySymbol = ""

                Return cantidad.ToString("c", culturaInfo) + " " + moneda
            End Function

            Public Shared Function FormateaMoneda(ByVal cantidad As String, ByVal moneda As String) As String
                Return FormateaMoneda(CDbl(cantidad), moneda)
            End Function

            Public Shared Function GetPaisByCode(ByVal Codigo As String) As String()
                For Each pais As String() In lista
                    If pais(0) = Codigo Then Return pais
                Next

                Return Nothing
            End Function

            Public Shared Function GetCulturaByCode(ByVal Codigo As String) As String
                For Each pais As String() In lista
                    If pais(0) = Codigo Then Return pais(2)
                Next

                Return Nothing
            End Function

            Private Shared Function GetArrPaises() As List(Of String())
                Dim lista As New List(Of String())

                lista.Add(PopPais("MXN", "MXN - Mexico - Pesos", "es-MX"))
                lista.Add(PopPais("EUR", "EUR - ECC - Euro", "es-ES"))
                lista.Add(PopPais("USD", "USD - Estados Unidos - Dolares", "en-US"))
                'lista.Add(PopPais("AED", "AED - Arab Emir - Dirham", "-------------------"))
                lista.Add(PopPais("ARS", "ARS - Argentine - Peso", "es-AR"))
                lista.Add(PopPais("AUD", "AUD - Australia - Dollars", "de-AT"))
                lista.Add(PopPais("BOB", "BOB - Bolivian - Boliviano", "es-BO"))
                lista.Add(PopPais("BRL", "BRL - Brazil - Real", "pt-BR"))
                lista.Add(PopPais("BGN", "BGN - Bulgaria - Leva", "bg-BG"))
                lista.Add(PopPais("CAD", "CAD - Canada - Dollars", "en-CA"))
                lista.Add(PopPais("CLP", "CLP - Chilean - Peso", "es-CL"))
                lista.Add(PopPais("CNY", "CNY - China - Renminbi", "zh-TW"))
                lista.Add(PopPais("COP", "COP - Colombian - Peso", "es-CO"))
                lista.Add(PopPais("HRK", "HRK - Croatia - Kuna", "hr-HR"))
                'lista.Add(PopPais("CYP", "CYP - Cyprus - Pound", "-----------------------"))
                lista.Add(PopPais("CZK", "CZK - Czech - Koruna", "cs-CZ"))
                'lista.Add(PopPais("DKK", "DKK - Danmark - Kroner", "---------------------"))
                lista.Add(PopPais("DOP", "DOP - Dominican - Pesos", "es-DO"))
                lista.Add(PopPais("EGP", "EGP - Egyptian - Pound", "ar-EG"))
                lista.Add(PopPais("EEK", "EEK - Estonia - Kroon", "et-EE"))
                lista.Add(PopPais("HKD", "HKD - Hong Kong - Dollars", "zh-HK"))
                lista.Add(PopPais("HUF", "HUF - Hungarian - Forint", "hu-HU"))
                lista.Add(PopPais("ISK", "ISK - Iceland - Krona", "is-IS"))
                lista.Add(PopPais("INR", "INR - India - Rupees", "hi-IN"))
                lista.Add(PopPais("IDR", "IDR - Indonesia - Rupiah", "id-ID"))
                lista.Add(PopPais("ILS", "ILS - Israel-Shekel", "he-IL"))
                lista.Add(PopPais("JPY", "JPY - Japan - Yen", "ja-JP"))
                lista.Add(PopPais("LVL", "LVL - Latvia - Lat", "lv-LV"))
                lista.Add(PopPais("LYD", "LYD - Libya - Dinars", "ar-LY"))
                lista.Add(PopPais("LTL", "LTL - Lithuania - Litas", "lt-LT"))
                lista.Add(PopPais("MYR", "MYR - Malaysia - Ringgit", "ms-MY"))
                lista.Add(PopPais("MTL", "MTL - Maltese - Lira", "mt-MT"))
                'lista.Add(PopPais("MUR", "MUR - Mautitius - Rupees", "-------------------"))
                lista.Add(PopPais("MAD", "MAD - Morocco - Dirhams", "ar-MA"))
                lista.Add(PopPais("NZD", "NZD - New Zealand - Dollars", "en-NZ"))
                lista.Add(PopPais("NOK", "NOK - Norwegia - Kroner", "nb-NO"))
                'lista.Add(PopPais("OMR", "OMR - Omani - Rial", "-------------------------"))
                lista.Add(PopPais("PEN", "PEN - Peruvian - New Sol", "es-PE"))
                lista.Add(PopPais("PHP", "PHP - Philippines - Peso", "en-PH"))
                lista.Add(PopPais("PLN", "PLN - Poland - Zlotych", "pl-PL"))
                lista.Add(PopPais("RON", "RON - Romania - New Lei", "ro-RO"))
                lista.Add(PopPais("RUB", "RUB - Russian - Rouble", "ru-RU"))
                lista.Add(PopPais("SKK", "SKK - Slovakia - Koruny", "sk-SK"))
                lista.Add(PopPais("ZAR", "ZAR - South African - Rand", "af-ZA"))
                lista.Add(PopPais("KRW", "KRW - South Korea - Won", "ko-KR"))
                'lista.Add(PopPais("SDG", "SDG - Sudan - Pounds", "----------------------"))
                lista.Add(PopPais("SEK", "SEK - Sweden - Krona", "se-SE"))
                'lista.Add(PopPais("CHF", "CHF - Switzerland - Francs", "----------------"))
                lista.Add(PopPais("TWD", "TWD - Taiwan - Dollars", "zh-TW"))
                lista.Add(PopPais("THB", "THB - Thailand - Baht", "th-TH"))
                lista.Add(PopPais("TRY", "TRY - Turk - Lira", "tr-TR"))
                lista.Add(PopPais("GBP", "GBP - UK - Pounds", "uk-UA"))
                lista.Add(PopPais("VEB", "VEB - Venezuela - Bolivar", "es-VE"))

                Return lista
            End Function

            Private Shared Function PopPais(ByVal Codigo As String, ByVal Moneda As String, ByVal Cultura As String) As String()
                Dim pais(3) As String

                pais(0) = Codigo
                pais(1) = Moneda
                pais(2) = Cultura

                Return pais
            End Function

        End Class

        Public Shared Function GetDateTime(ByVal strDate As String) As DateTime
            Return DateTime.ParseExact(strDate, DateRulesFormat, Nothing)
        End Function

    End Class

    Public Class Hotels

        Public Class SearchHotel

            Delegate Sub FIJA_GRID_Callback(ByVal frmInvoker As Form, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal todos As Boolean)
            Public Shared Sub FIJA_GRID(ByVal frmInvoker As Form, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal todos As Boolean)
                Try
                    If grid.InvokeRequired Then
                        Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                        frmInvoker.Invoke(d, New Object() {frmInvoker, grid, todos})
                    Else
                        CapaLogicaNegocios.Hotels.SearchHotel.formatea_grid(grid, todos)
                        CapaPresentacion.Common.Fija_CTLVISIBLE(CType(frmInvoker, busquedaHotel).PictureBox2, False)
                        CType(frmInvoker, busquedaHotel).rbt_todosHoteles.Enabled = True
                        CType(frmInvoker, busquedaHotel).rbt_tarifas.Enabled = True
                        CType(frmInvoker, busquedaHotel).rbt_hotelesDisponibles.Enabled = True
                        CapaPresentacion.Hotels.SearchHotel.FilterGrid(frmInvoker)
                    End If
                Catch ex As Exception
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub

            Delegate Sub FIJA_FORM_Callback(ByVal frm As busquedaHotel, ByVal ds As DataSet)
            Public Shared Sub FIJA_FORM(ByVal frm As busquedaHotel, ByVal ds As DataSet)
                Try
                    If frm.InvokeRequired Then
                        Dim d As New FIJA_FORM_Callback(AddressOf FIJA_FORM)
                        frm.Invoke(d, New Object() {frm, ds})
                    Else
                        CapaLogicaNegocios.Hotels.SearchHotel.load_data(frm, ds)
                    End If
                Catch ex As Exception
                    'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_0155_ atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub

            Delegate Sub FIJA_CONVENIOS_Callback(ByVal frm As busquedaHotel, ByVal ds As DataSet, ByVal ultima As String)
            Public Shared Sub FIJA_CONVENIOS(ByVal frm As busquedaHotel, ByVal ds As DataSet, ByVal ultima As String)
                Try
                    If frm.InvokeRequired Then
                        Dim d As New FIJA_CONVENIOS_Callback(AddressOf FIJA_CONVENIOS)
                        frm.Invoke(d, New Object() {frm, ds, ultima})
                    Else
                        CapaLogicaNegocios.Hotels.SearchHotel.cargar_convenios(frm, ds, ultima)
                    End If
                Catch ex As Exception
                    'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_0155_ atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub
            Delegate Sub FIJA_RATESPLAN_Callback(ByVal frm As busquedaHotel, ByVal ds As DataSet)
            Public Shared Sub FIJA_RATESPLAN(ByVal frm As busquedaHotel, ByVal ds As DataSet)
                Try
                    If frm.InvokeRequired Then
                        Dim d As New FIJA_RATESPLAN_Callback(AddressOf FIJA_RATESPLAN)
                        frm.Invoke(d, New Object() {frm, ds})
                    Else
                        CapaLogicaNegocios.Hotels.SearchHotel.cargar_ratesplan(frm, ds)
                    End If
                Catch ex As Exception
                    'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_0155_ atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub

            Public Shared Sub InitializeForm(ByVal frm As busquedaHotel)
                CapaLogicaNegocios.Hotels.SearchHotel.SearchDates(frm)
                CapaPresentacion.Common.SetTabColor(frm)

                frm.botones = New String() {"btn_informacionGeneral", "btn_habitaciones", "btn_galeria", "btn_mapa", "btn_tarifas", "btn_hotel_disponibilidad"} '"btn_reservar", "btn_reglas"
                CapaPresentacion.Idiomas.cambiar_busquedaHotel(frm)
                frm.data_lbl_totalHoteles.Text = ""

                frm.ComboSearch1.crea_lista(frm, 0, 0)
                AddHandler frm.ComboSearch1.llena, AddressOf frm.llena

                For Each ctl As Control In frm.Panel1.Controls
                    Select Case ctl.GetType.Name
                        Case "Label", "PictureBox", "Button"
                            'estos no ejecutan el performClick
                        Case Else
                            AddHandler ctl.KeyPress, AddressOf frm.ctlKeyPress
                    End Select
                Next
                AddHandler frm.ComboSearch1.TextBox1.KeyPress, AddressOf frm.ctlKeyPress
                AddHandler frm.ComboSearch1.ListBox1.KeyPress, AddressOf frm.ctlKeyPress
                AddHandler frm.Uc_habitaciones1.prop_txt_adultos.KeyPress, AddressOf frm.ctlKeyPress
                AddHandler frm.Uc_habitaciones1.prop_txt_habitaciones.KeyPress, AddressOf frm.ctlKeyPress
                AddHandler frm.Uc_habitaciones1.prop_txt_ninos.KeyPress, AddressOf frm.ctlKeyPress

                CapaPresentacion.Common.moneda.FillUceMoneda(frm.uce_moneda)

                frm.uce_distancia.Items.Clear()
                frm.uce_distancia.Items.Add("1", "1")
                frm.uce_distancia.Items.Add("10", "10")
                frm.uce_distancia.Items.Add("20", "20")
                frm.uce_distancia.Items.Add("30", "30")
                frm.uce_distancia.Items.Add("40", "40")
                frm.uce_distancia.Items.Add("50", "50")
                frm.uce_distancia.Items.Add("100", "100")
                frm.uce_distancia.Items.Add("150", "150")
                If frm.uce_distancia.SelectedIndex = -1 Then frm.uce_distancia.SelectedIndex = 0
                If dataOperador.Hoteles.Count = 0 Then
                    frm.rbt_Hotel.Visible = False
                Else
                    frm.rbt_Hotel.Checked = True
                End If

            End Sub

            Public Shared Sub loadCopyForm(ByVal frm As busquedaHotel)
                frm.dtp_in.Value = frm.datosBusqueda_hotel.llegada
                frm.dtp_out.Value = frm.datosBusqueda_hotel.salida
                herramientas.fechas_inteligentes(frm.dtp_in, frm.dtp_out, frm.dtp_out, 1, frm.txt_noches)
                frm.Uc_habitaciones1.Habitaciones = frm.datosBusqueda_hotel.habitaciones_
            End Sub

            Public Shared Sub FilterGrid(ByVal frm As busquedaHotel)
                Dim dv As DataView = frm.UltraGrid1.DataSource
                If dv Is Nothing Then Return

                dv.Sort = "Tarifas DESC, PropertyName"
                If frm.datosBusqueda_hotel.todosHoteles Then
                    dv.RowFilter = ""
                Else
                    dv.RowFilter = "Tarifas <> ''"
                End If
                If frm.rbt_tarifas.Checked Then
                    Dim min, max As Double
                    min = frm.txtMin.Text
                    max = frm.txtMax.Text
                    dv.RowFilter = "((" & min & " >= TarifaMenorOrdenar And " & min & " <= TarifaMayorOrdenar) Or (" & max & " >= TarifaMenorOrdenar And " & max & " <= TarifaMayorOrdenar) Or (" & min & " <= TarifaMenorOrdenar And " & max & " >= TarifaMenorOrdenar)) and Tarifas <> ''"
                    'dv.RowFilter &= "AND (TarifaMenorOrdenar<=" & frm.txtMin.Text & " AND TarifaMayorOrdenar<=" & frm.txtMax.Text & ")"
                End If
                frm.UltraGrid1.DataSource = dv

                For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In frm.UltraGrid1.Rows
                    If r.Cells("Tarifas").Value Is System.DBNull.Value OrElse r.Cells("Tarifas").Value = "" Then r.Appearance.ForeColor = Color.Gray
                Next
            End Sub

            Public Shared Sub EnableButtons()
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_informacionGeneral").SharedProps.Enabled = True
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_habitaciones").SharedProps.Enabled = True
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_galeria").SharedProps.Enabled = True
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_mapa").SharedProps.Enabled = True
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Enabled = True
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_hotel_disponibilidad").SharedProps.Enabled = True
            End Sub

            Public Shared Sub SetSearchItemData(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test)
                datosBusqueda_hotel.ServiceProvider = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("ServiceProvider").Value.ToString()
                datosBusqueda_hotel.PropertyNumber = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("PropertyNumber").Value.ToString()
                datosBusqueda_hotel.PropertyName = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("PropertyName").Value.ToString()
                datosBusqueda_hotel.Address = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("Address").Value.ToString()
                datosBusqueda_hotel.CityName = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("CityName").Value.ToString()
                datosBusqueda_hotel.CountryCode = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("CountryCode").Value.ToString()
                datosBusqueda_hotel.ChainCode = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("ChainCode").Value.ToString()
            End Sub

            Public Shared Sub SetControlsData(ByVal frm As busquedaHotel)
                frm.datosBusqueda_hotel.rubro = enumServices.Hotel
                frm.datosBusqueda_hotel.confirmada = False
                '
                If frm.datosBusqueda_hotel.ninosEdades = Nothing Then frm.datosBusqueda_hotel.ninosEdades = ""
                frm.datosBusqueda_hotel.llegada = frm.dtp_in.Value
                frm.datosBusqueda_hotel.salida = frm.dtp_out.Value
                frm.datosBusqueda_hotel.noches = frm.txt_noches.Text
                If frm.rbt_ciudad.Checked Then frm.datosBusqueda_hotel.buscaPor = "C" Else frm.datosBusqueda_hotel.buscaPor = "A"
                frm.datosBusqueda_hotel.todosHoteles = frm.rbt_todosHoteles.Checked
                frm.datosBusqueda_hotel.ciudad_codigo = frm.ComboSearch1.selected_valor_Codigo
                If frm.datosBusqueda_hotel.buscaPor = "C" Then
                    frm.datosBusqueda_hotel.RefPoint = frm.ComboSearch1.selected_valor_RefPoint
                    If frm.ComboSearch1.selected_valor_Distancia <> 0 Then
                        frm.datosBusqueda_hotel.buscaPor = "R"
                    End If
                End If
                frm.datosBusqueda_hotel.ciudad_nombre = frm.ComboSearch1.ListBox1.Text
                frm.datosBusqueda_hotel.codProm = frm.txt_codProm.Text
                If frm.txt_Conv.Text.IndexOf("--") >= 0 Then
                    If (frm.txt_Conv.Text.EndsWith(".")) Then
                        frm.datosBusqueda_hotel.Convenio = frm.txt_Conv.Text.Substring(0, frm.txt_Conv.Text.IndexOf("--")).Trim()
                    Else
                        frm.datosBusqueda_hotel.Convenio = frm.txt_Conv.Text.Substring(frm.txt_Conv.Text.IndexOf("--") + 2).Trim()
                    End If
                Else
                    frm.datosBusqueda_hotel.Convenio = frm.txt_Conv.Text
                End If

                If frm.txt_Rateplan.Text.IndexOf("--") >= 0 Then
                    If (frm.txt_Rateplan.Text.EndsWith(".")) Then
                        frm.datosBusqueda_hotel.RateCode = frm.txt_Rateplan.Text.Substring(0, frm.txt_Rateplan.Text.IndexOf("--")).Trim()
                    Else
                        frm.datosBusqueda_hotel.RateCode = frm.txt_Rateplan.Text.Substring(frm.txt_Rateplan.Text.IndexOf("--") + 2).Trim()
                    End If
                Else
                    frm.datosBusqueda_hotel.RateCode = frm.txt_Rateplan.Text
                End If
                If (Not frm.dsRateplans Is Nothing AndAlso frm.dsRateplans.Tables.Contains("Ratesplan")) Then
                    Dim dt As DataTable = frm.dsRateplans.Tables("Ratesplan")
                    For Each row As DataRow In dt.Rows
                        If row("codigoTarifa") = frm.datosBusqueda_hotel.RateCode Then
                            If Not row.IsNull("AccessCode") Then frm.datosBusqueda_hotel.codProm = row("AccessCode")
                            Exit For
                        End If
                    Next
                End If
                frm.datosBusqueda_hotel.habitaciones_ = frm.Uc_habitaciones1.Habitaciones ' .copy() - no e snesesario por que la propiedad ya regresa con el copy
                frm.datosBusqueda_hotel.monedaBusqueda = frm.uce_moneda.SelectedItem.DataValue
                frm.datosBusqueda_hotel.distancia = frm.uce_distancia.SelectedItem.DataValue
                frm.datosBusqueda_hotel.idArea = frm.cmbArea.SelectedItem.DataValue
                If frm.rbt_Hotel.Checked Then
                    frm.datosBusqueda_hotel.hotel_nombre = frm.cmbHotel.Text
                    frm.datosBusqueda_hotel.buscaPor = "R"
                    frm.datosBusqueda_hotel.RefPoint = dataOperador.Hoteles(frm.cmbHotel.Value).Refpoint
                    frm.datosBusqueda_hotel.ciudad_nombre = dataOperador.Hoteles(frm.cmbHotel.Value).Ciudad
                    frm.datosBusqueda_hotel.ciudad_codigo = dataOperador.Hoteles(frm.cmbHotel.Value).IATA
                Else
                    frm.datosBusqueda_hotel.hotel_nombre = ""
                End If
            End Sub

            Public Shared Function FormatDS_Search(ByVal ds As DataSet)
                'agrega unas columna
                ds.Tables("Property").Columns.Add("laCategoria", GetType(Bitmap))
                ds.Tables("Property").Columns.Add("Tarifas", GetType(String))
                ds.Tables("Property").Columns.Add("TarifaMenorOrdenar", GetType(Double))
                ds.Tables("Property").Columns.Add("Numero", GetType(Integer))
                ds.Tables("Property").Columns.Add("laCategoriaNumero", GetType(Integer))
                If Not ds.Tables("Property").Columns.Contains("CountryCode") Then ds.Tables("Property").Columns.Add("CountryCode", GetType(String))
                ds.Tables("Property").Columns.Add("TarifaMayorOrdenar", GetType(Double))

                Dim cont As Integer = 1
                For Each r As DataRow In ds.Tables("Property").Rows
                    Dim s As String = r.Item("Category").ToString
                    s = s.Substring(s.LastIndexOf("/") + 1, s.Length - s.LastIndexOf("/") - 1)
                    Select Case s
                        Case "Star1.gif"
                            r.Item("laCategoria") = Global.callCenter.My.Resources.Resources._1
                        Case "Star2.gif"
                            r.Item("laCategoria") = Global.callCenter.My.Resources.Resources._2
                        Case "Star3.gif"
                            r.Item("laCategoria") = Global.callCenter.My.Resources.Resources._3
                        Case "Star4.gif"
                            r.Item("laCategoria") = Global.callCenter.My.Resources.Resources._4
                        Case "Star5.gif"
                            r.Item("laCategoria") = Global.callCenter.My.Resources.Resources._5
                        Case Else
                            r.Item("laCategoria") = Global.callCenter.My.Resources.Resources.Star0
                    End Select

                    r.Item("Numero") = cont.ToString
                    cont += 1

                    If s = "" Then
                        r.Item("laCategoriaNumero") = 0
                    Else
                        r.Item("laCategoriaNumero") = CInt(s.Replace("Star", "").Replace(".gif", ""))
                    End If
                Next

                'mueve algunas columnas
                ds.Tables("Property").Columns("Tarifas").SetOrdinal(ds.Tables("Property").Columns("laCategoria").Ordinal)
                ds.Tables("Property").Columns("Numero").SetOrdinal(0)

                Return ds
            End Function

            Public Shared Sub FormatUltraGrid(ByVal frm As busquedaHotel, ByVal ds As DataSet)
                'llena UltraGrid
                frm.UltraGrid1.DataSource = ds.Tables("Property").DefaultView
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 1 Then frm.UltraGrid1.DisplayLayout.Bands(1).Hidden = True

                'resize primera ves las cols
                For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In frm.UltraGrid1.DisplayLayout.Bands(0).Columns
                    col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
                Next

                If Not frm.UltraGrid1.ActiveRow Is Nothing AndAlso frm.UltraGrid1.Rows.Count > 0 Then frm.UltraGrid1.ActiveRow = frm.UltraGrid1.Rows(0)
            End Sub

            Public Shared Sub autocompletarConveios(ByVal frm As busquedaHotel, ByVal dtAutocomplete As DataTable, ByVal start As String)
                If dtAutocomplete Is Nothing Then
                    Return
                End If

                Dim coleccion As New AutoCompleteStringCollection()
                For Each row As DataRow In dtAutocomplete.Rows
                    Dim referencia As String = Convert.ToString(row("Referencia"))
                    Dim empresa As String = Convert.ToString(row("Empresa"))
                    If referencia.Length > 0 AndAlso Char.ToLower(referencia(0)) = start Then
                        coleccion.Add(String.Format("{0} -- {1}.", referencia, empresa))
                    End If
                    If empresa.Length > 0 AndAlso Char.ToLower(empresa(0)) = start Then
                        coleccion.Add(String.Format("{0} -- {1} ", empresa, referencia))
                    End If
                Next

                '   Establecer sugerencias
                frm.txt_Conv.AutoCompleteCustomSource = coleccion
                frm.txt_Conv.AutoCompleteMode = AutoCompleteMode.Suggest
                frm.txt_Conv.AutoCompleteSource = AutoCompleteSource.CustomSource

                'frm.txt_Conv.Text = frm.txt_Conv.Text
                'frm.txt_Conv.AppendText(String.Empty)
                If (frm.txt_Conv.Focused) Then
                    SendKeys.Flush()
                    'frm.txt_Conv.Focus()
                    'frm.txt_Conv.Text = frm.txt_Conv.Text.Trim()
                    SendKeys.Send(" ")
                    SendKeys.SendWait("{BACKSPACE}")

                End If
            End Sub

            Public Shared Sub autocompletarRatesplan(ByVal frm As busquedaHotel, ByVal dtAutocomplete As DataTable)
                If dtAutocomplete Is Nothing Then
                    frm.txt_Rateplan.AutoCompleteMode = AutoCompleteMode.None
                    Return
                End If

                Dim coleccion As New AutoCompleteStringCollection()
                For Each row As DataRow In dtAutocomplete.Rows
                    Dim codigoTarifa As String = Convert.ToString(row("codigoTarifa"))
                    Dim descripcion As String = Convert.ToString(row("Description"))
                    If codigoTarifa.Length > 0 Then
                        coleccion.Add(String.Format("{0} -- {1}.", codigoTarifa, descripcion))
                    End If
                    If descripcion.Length > 0 Then
                        coleccion.Add(String.Format("{0} -- {1} ", descripcion, codigoTarifa))
                    End If
                Next

                '   Establecer sugerencias
                frm.txt_Rateplan.AutoCompleteCustomSource = coleccion
                frm.txt_Rateplan.AutoCompleteMode = AutoCompleteMode.Suggest
                frm.txt_Rateplan.AutoCompleteSource = AutoCompleteSource.CustomSource

                If (frm.txt_Rateplan.Focused) Then
                    SendKeys.Flush()
                    SendKeys.Send(" ")
                    SendKeys.SendWait("{BACKSPACE}")
                End If

            End Sub

            Public Shared Sub TipForm(ByVal frm As busquedaHotel)
                Dim tip As String = ""
                tip += frm.ComboSearch1.ListBox1.Text + vbCrLf + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0063_fecha") + " " + frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy") + " - " + frm.datosBusqueda_hotel.salida.ToString("dd/MMM/yyyy") + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0114_codPromocion") + " " + frm.datosBusqueda_hotel.codProm + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0067_totalHoteles") + " " + frm.data_lbl_totalHoteles.Text + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0065_adultos") + " " + frm.datosBusqueda_hotel.habitaciones_.TotalAdultos.ToString + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0113_ni�os") + " " + frm.datosBusqueda_hotel.habitaciones_.TotalNinos.ToString + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0008_habitaciones") + ": " + frm.datosBusqueda_hotel.habitaciones_.TotalHabitaciones.ToString + vbCrLf
                misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).ToolTip = tip
            End Sub

            Public Shared Function NumberDV(ByVal dv As DataView) As DataView
                Dim dt As DataTable = dv.ToTable
                Dim cont As Integer = 1
                For Each r As DataRow In dt.Rows
                    r.Item("Numero") = cont.ToString
                    cont += 1
                Next

                Return dt.DefaultView
            End Function

        End Class

        Public Class HotelRates

            Delegate Sub FIJA_FORM_Callback(ByVal frm As tarifas, ByVal ds As DataSet)
            Public Shared Sub FIJA_FORM(ByVal frm As tarifas, ByVal ds As DataSet)
                Try
                    If frm.InvokeRequired Then
                        Dim d As New FIJA_FORM_Callback(AddressOf FIJA_FORM)
                        frm.Invoke(d, New Object() {frm, ds})
                    Else
                        load_data(frm, ds)
                    End If
                Catch ex As Exception
                    'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub

            Private Shared Sub load_data(ByVal frm As tarifas, ByVal ds As DataSet)
                Try
                    If ds Is Nothing Then
                        frm.UltraGrid1.DataSource = ds
                        CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)
                        Exit Sub
                    End If

                    If Not ds.Tables("error") Is Nothing Then
                        Dim r As DataRow = ds.Tables("error").Rows(0)
                        MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Else
                        If Not ds Is Nothing AndAlso ds.Tables.Contains("property") AndAlso ds.Tables("property").Rows.Count > 0 AndAlso ds.Tables("property").Columns.Contains("TaxIncluded") AndAlso ds.Tables("property").Rows(0).Item("TaxIncluded").ToString.ToUpper = "N" Then
                            frm.taxInc = False
                        Else
                            frm.taxInc = True
                        End If

                        If Not ds.Tables.Contains("RatePlan") Then
                            frm.UltraGrid1.DataSource = Nothing
                            CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)

                            Exit Sub
                        End If

                        ds = CapaPresentacion.Hotels.HotelRates.FormatDS_Rates(frm, ds)
                        CapaPresentacion.Hotels.HotelRates.FormatUltraGrid(frm, ds)
                        CapaPresentacion.Common.NumberGrid(frm.UltraGrid1)
                        CapaPresentacion.Idiomas.cambiar_tarifas(frm)
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0038", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                    frm.UltraGrid1.DataSource = Nothing
                    'Return False                    -              solo si se quiere que no se muestre el frm vacio
                Finally
                    CapaPresentacion.Hotels.HotelRates.set_control(frm, ds) '      -              aqui x si no hay ds.Tables("RatePlan") 

                    If Not frm.individual Then
                        CapaPresentacion.Hotels.HotelRates.TipForm(frm)
                    End If

                    For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In frm.UltraGrid1.DisplayLayout.Bands(0).Columns
                        col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
                    Next
                End Try
                frm.lbl_info.Text = frm.Uc_habitaciones1.Habitaciones.TotalHabitaciones.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0008_habitaciones") + ", " + frm.Uc_habitaciones1.Habitaciones.TotalAdultos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + frm.Uc_habitaciones1.Habitaciones.TotalNinos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit")
                CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)
            End Sub

            Public Shared Function FormatDS_Rates(ByVal frm As tarifas, ByVal ds As DataSet) As DataSet
                'agrega una columna
                ds.Tables("RatePlan").Columns.Add("nombreHabitacion")
                ds.Tables("RatePlan").Columns("nombreHabitacion").SetOrdinal(2)
                For Each r As DataRow In ds.Tables("RatePlan").Rows
                    Dim r_parent As DataRow = r.GetParentRow("RoomType_RatePlan")
                    r.Item("nombreHabitacion") = r_parent.Item("RoomName")
                Next

                'cambiar formato fecha DIA
                For Each r As DataRow In ds.Tables("Price").Rows
                    r.Item("Day") = DateTime.ParseExact(r.Item("Day"), "yyyy/MM/dd", Nothing).ToString("dd-MMM")
                Next

                'pone el tipo de moneda
                ds.Tables("RatePlan").Columns.Add("TotalTotal")
                ds.Tables("RatePlan").Columns.Add("TotalTotalSinDesc")
                ds.Tables("RatePlan").Columns.Add("TotalOrdenar", GetType(Double))
                ds.Tables("RatePlan").Columns.Add("Numero", GetType(Integer))
                ds.Tables("RatePlan").Columns("Numero").SetOrdinal(0)
                ds.Tables("RatePlan").Columns.Add("RowType")
                For Each r As DataRow In ds.Tables("RatePlan").Rows
                    'frm.datosBusqueda_hotel.RateCode = r.Item("PlanCode")

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Para el Hoc en los portales se muestra el campo AltTotal de Price (por cada uno de los d�as) y para 
                    'sacar el promedio lo dividimos entre el n�mero de d�as.
                    '
                    'El campo AltPackPrice lo tomamos �nicamente cuando el segmento es "K" y el campo StartDate no viene 
                    'en null, es decir cuando es paquete
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    'If CDbl(r.Item("DescPromotion")) > 0 Then
                    '    Dim ds_hor As DataSet = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)
                    '    If ds_hor IsNot Nothing AndAlso ds_hor.Tables.Contains("Error") AndAlso ds_hor.Tables("Error").Rows.Count > 0 Then Continue For

                    '    Dim cant As Double
                    '    cant = CDbl(ds_hor.Tables("Property").Rows(0).Item("AltTotalRate"))
                    '    r.Item("TotalTotal") = Format(cant, "c") + " " + ds.Tables("Property").Rows(0).Item("AltCurrency")
                    'Else
                    Dim cant As Double = 0
                    Dim cantSD As Double = 0

                    Dim cont As Integer = 0
                    Dim contSD As Integer = 0
                    For Each r_price As DataRow In r.GetChildRows("RatePlan_Price")
                        If r_price.Item("Available") <> "Y" Then Continue For

                        cant += CDbl(r_price.Item("AltTotal2"))
                        If CDbl(r_price.Item("AltTotal2")) > 0 Then cont += 1
                        cantSD += CDbl(r_price.Item("AltTotal"))
                        contSD += 1
                    Next

                    If r.Item("Segment") = "K" AndAlso r.Table.Columns.Contains("StartDate") AndAlso r.Table.Columns.Contains("Nights") AndAlso Not r.IsNull("Nights") Then
                        cant = CDbl(r.Item("AltPackPrice")) 
                        cont = 1
                        ' para el valor sin descuento que valor asignarle??
                        'cantSD=???
                        r.Item("PlanName") += " / " + CapaPresentacion.Idiomas.get_str("str_390_Pkg") + " " + r.Item("Nights") + " " + CapaPresentacion.Idiomas.get_str("str_0140_noches")
                    End If

                    If cont > 0 Then
                        cant = cant / cont
                        cantSD = cantSD / contSD
                    End If

                    If cont = 0 Then
                        r.Item("TotalTotal") = "" ' "NA"
                        r.Item("TotalOrdenar") = 0
                        r.Item("TotalTotalSinDesc") = ""
                        r.Item("RowType") = "Norm"
                    Else
                        'r.Item("TotalTotal") = Format(cant, "c") + " " + ds.Tables("Property").Rows(0).Item("AltCurrency")
                        'Dim moneda As String = ds.Tables("Property").Rows(0).Item("AltCurrency")
                        'r.Item("TotalTotal") = cant.ToString("c", New System.Globalization.CultureInfo(callCenter.moneda.GetCulturaByCode(moneda))) + " " + moneda
                        r.Item("TotalTotal") = CapaPresentacion.Common.moneda.FormateaMoneda(cant, ds.Tables("Property").Rows(0).Item("AltCurrency"))
                        r.Item("TotalOrdenar") = cant
                        r.Item("TotalTotalSinDesc") = CapaPresentacion.Common.moneda.FormateaMoneda(cantSD, ds.Tables("Property").Rows(0).Item("AltCurrency"))
                        If cant < cantSD AndAlso cant > 0 Then
                            r.Item("RowType") = "Desc"
                        Else
                            r.Item("RowType") = "Norm"
                        End If
                    End If
                    'End If

                    If r.Table.Columns.Contains("Message") AndAlso Not r.IsNull("Message") AndAlso r.Item("Message") <> "" Then
                        'Select Case Trim(r.Item("Message")).Split(" ")(0) 'r.Item("Message")
                        '    Case "005" '"005 - Close Dates"
                        '        r.Item("TotalTotal") = Idiomas.get_str("str_406_ClosedDates")
                        '        'TODO: recursos mensajes de error
                        '    Case "002" '"Min days"
                        '        r.Item("TotalTotal") = Idiomas.get_str("str_459")
                        '    Case Else
                        '        r.Item("TotalTotal") = r.Item("Message")
                        'End Select
                        Try
                            If (r.Item("Message").ToString.IndexOf("-") > -1) Then
                                Select Case CInt(r.Item("Message").Substring(0, r.Item("Message").IndexOf("-")))
                                    Case 1
                                        r.Item("TotalTotal") = String.Format(Idiomas.get_str("HOTEL000280"), r.Item("MaxDays"))
                                    Case 2
                                        r.Item("TotalTotal") = String.Format(Idiomas.get_str("HOTEL000281"), r.Item("MinDays"))
                                    Case 3
                                        r.Item("TotalTotal") = String.Format(Idiomas.get_str("HOTEL000282"), r.Item("AdvBooking"))
                                    Case 4
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000285")
                                    Case 5
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000286")
                                    Case 6
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000283")
                                    Case 7
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000284")
                                    Case 19
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000287")
                                    Case 32
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000634")
                                    Case 33
                                        r.Item("TotalTotal") = Idiomas.get_str("HOTEL000635")
                                    Case Else
                                        r.Item("TotalTotal") = r.Item("Message")
                                End Select
                            End If
                        Catch ex As Exception

                        End Try

                    End If
                Next
                ds.Tables("RatePlan").Columns("AltAvgRate").SetOrdinal(ds.Tables("RatePlan").Columns("TotalStay").Ordinal)

                'agrega una columna
                ds.Tables("RatePlan").Columns.Add("GuarDep2")
                ds.Tables("RatePlan").Columns("GuarDep2").SetOrdinal(3)
                For Each r As DataRow In ds.Tables("RatePlan").Rows
                    r.Item("GuarDep2") = r.Item("GuarDep")
                Next

                'eliminar columnas
                'ds.Tables("RatePlan").Columns.Remove("xxxxxxx")

                ds.Tables("RatePlan").DefaultView.Sort = "AltAvgRate, nombreHabitacion"
                ds = copiar_ds_DefaultView(ds)

                Return ds
            End Function

            Private Shared Function copiar_ds_DefaultView(ByVal ds As DataSet) As DataSet
                Dim ds_new As New DataSet
                For Each dt As DataTable In ds.Tables
                    ds_new.Tables.Add(dt.DefaultView.ToTable)
                Next

                Return ds
            End Function

            Public Shared Sub FormatUltraGrid(ByVal frm As tarifas, ByVal ds As DataSet)
                frm.UltraGrid1.DataSource = ds
                frm.UltraGrid1.DataMember = "RatePlan"
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 1 Then frm.UltraGrid1.DisplayLayout.Bands(1).Hidden = True

                'elimina unos ROWs si no estan disponibles
                Dim i As Integer = 0
                While i < frm.UltraGrid1.Rows.Count
                    If frm.UltraGrid1.Rows(i).Cells("Available").Value.ToString.ToUpper = "N" AndAlso frm.UltraGrid1.Rows(i).Cells("Segment").Value.ToString.ToUpper <> "K" Then
                        frm.UltraGrid1.Rows(i).Delete(False)
                    Else
                        i += 1
                    End If
                End While

                'selecciona el primer Row
                If frm.UltraGrid1.Rows.Count > 0 Then
                    frm.UltraGrid1.ActiveRow = frm.UltraGrid1.Rows(0)
                End If

                'agrega un tooltip con la descripcion de CancelPrior
                For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In frm.UltraGrid1.Rows
                    '
                    '
                    '   PARA ESTO SE TIENE QUE HACER EL HOR
                    '
                    '
                    'Dim str As String = r.Cells("CancelPrior").Value
                    'If str.Contains("-") Then
                    '    Dim arr() As String = str.Split("-")

                    '    If arr.Length = 2 Then
                    '        Select Case arr(1).ToUpper
                    '            Case "D"
                    '                If arr(0) = "0" Then
                    '                    str = CapaPresentacion.Idiomas.get_str("str_0159_noCancel")
                    '                Else
                    '                    str = CapaPresentacion.Idiomas.get_str("str_0160_cancelXXdias").Replace("@@", arr(0))
                    '                End If
                    '            Case "H"
                    '                str = CapaPresentacion.Idiomas.get_str("str_0161_cancelXXhoras").Replace("@@", arr(0))
                    '            Case "T"
                    '                If arr(0).Length = 4 Then
                    '                    str = CapaPresentacion.Idiomas.get_str("str_0162_cancelXXin").Replace("@@", arr(0).Substring(0, 2) + ":" + arr(0).Substring(2, 2))
                    '                End If
                    '        End Select
                    '    End If
                    'End If
                    'r.Cells("CancelPrior").Value = str

                    Select Case r.Cells("GuarDep2").Value
                        Case "G"
                            r.Cells("GuarDep2").Value = CapaPresentacion.Idiomas.get_str("str_0105_garantia")
                        Case "D"
                            r.Cells("GuarDep2").Value = CapaPresentacion.Idiomas.get_str("str_0106_deposito")
                        Case "N"
                            r.Cells("GuarDep2").Value = CapaPresentacion.Idiomas.get_str("str_0108_ninguno")
                    End Select
                Next

                If frm.UltraGrid1.Rows.Count = 0 Then
                    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Enabled = False
                    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas").SharedProps.Enabled = False
                    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ver_detalles").SharedProps.Enabled = False
                Else
                    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Enabled = True
                    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas").SharedProps.Enabled = True
                    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ver_detalles").SharedProps.Enabled = True
                End If
            End Sub

            Public Shared Sub TipForm(ByVal frm As tarifas)
                Dim tip As String = ""
                tip += frm.data_lbl_hotel.Text + vbCrLf + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0063_fecha") + " " + frm.data_lbl_fechas.Text + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0114_codPromocion") + " " + frm.data_lbl_codProm.Text + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0036_tarifas") + ": " + frm.data_lbl_totalTarifas.Text + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0065_adultos") + " " + frm.Uc_habitaciones1.Habitaciones.TotalAdultos.ToString + vbCrLf 'data_txt_adultos.Text 
                tip += CapaPresentacion.Idiomas.get_str("str_0113_ni�os") + " " + frm.Uc_habitaciones1.Habitaciones.TotalNinos.ToString + vbCrLf
                tip += CapaPresentacion.Idiomas.get_str("str_0008_habitaciones") + ": " + frm.Uc_habitaciones1.Habitaciones.TotalHabitaciones.ToString + vbCrLf
                misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).ToolTip = tip
            End Sub

            Public Shared Sub InitializeForm(ByVal frm As tarifas)
                If Not frm.individual Then misForms.busquedaHotel_ultimoActivo.lista_tarifas.Add(frm)

                If frm.individual Then frm.Uc_habitaciones1.Habitaciones = frm.datosBusqueda_hotel.habitaciones_.copy

                CapaPresentacion.Common.FormatUltraGrid(frm.UltraGrid1)

                If Not frm.individual Then CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Enabled = True
                If Not frm.individual Then
                    frm.botones = New String() {"btn_reservar", "btn_reglas", "btn_ver_detalles"}
                Else
                    frm.botones = New String() {"btn_reservar"}
                End If

                inicializar_controles(frm)
                CapaPresentacion.Idiomas.cambiar_tarifas(frm)
            End Sub

            Public Shared Sub set_control(ByVal frm As tarifas, ByVal ds As DataSet)
                frm.data_lbl_fechas.Text = frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy") + " - " + frm.datosBusqueda_hotel.salida.ToString("dd/MMM/yyyy")
                frm.data_lbl_hotel.Text = frm.datosBusqueda_hotel.PropertyName
                frm.data_lbl_cadena.Text = frm.datosBusqueda_hotel.ChainCode
                frm.Uc_habitaciones1.Habitaciones = frm.datosBusqueda_hotel.habitaciones_.copy
                If ds Is Nothing OrElse Not ds.Tables.Contains("property") OrElse Not ds.Tables("property").Columns.Contains("currency") OrElse ds.Tables("property").Rows.Count = 0 Then
                    frm.data_lbl_tarifasEn.Text = ""
                Else
                    frm.data_lbl_tarifasEn.Text = ds.Tables("property").Rows(0).Item("AltCurrency")
                End If
                frm.data_lbl_codProm.Text = frm.datosBusqueda_hotel.codProm
                frm.lblConvenioData.Text = frm.datosBusqueda_hotel.Convenio
                frm.data_lbl_totalTarifas.Text = frm.UltraGrid1.Rows.Count

                If frm.individual Then frm.btn_aceptar.Visible = True Else frm.btn_aceptar.Visible = False
            End Sub

            Private Shared Sub inicializar_controles(ByVal frm As tarifas)
                frm.data_lbl_cadena.Text = frm.datosBusqueda_hotel.ChainCode
                frm.data_lbl_codProm.Text = frm.datosBusqueda_hotel.codProm
                frm.lblConvenioData.Text = frm.datosBusqueda_hotel.Convenio
                frm.data_lbl_hotel.Text = frm.datosBusqueda_hotel.PropertyName
                frm.data_lbl_fechas.Text = frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy") + " - " + frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy")
                frm.data_lbl_tarifasEn.Text = ""
                frm.data_lbl_totalTarifas.Text = ""
                frm.lbl_info.Text = frm.Uc_habitaciones1.Habitaciones.TotalHabitaciones.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0008_habitaciones") + ", " + frm.Uc_habitaciones1.Habitaciones.TotalAdultos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + frm.Uc_habitaciones1.Habitaciones.TotalNinos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit")
            End Sub

        End Class

        Public Class HotelRules

            Public Shared Sub FormatBrowser(ByVal frm As reglas, ByVal ds As DataSet)
                Dim str As String = ""

                If ds Is Nothing OrElse ds.Tables.Count = 0 Then
                    Exit Sub
                End If

                If Not ds.Tables("error") Is Nothing Then
                    Dim r As DataRow = ds.Tables("error").Rows(0)

                    If r.Item("ErrorCode") = "6" AndAlso r.Item("Message") = "No Availability" Then
                        str += CapaPresentacion.Idiomas.get_str("str_320_errSolCom")
                    Else
                        str += r.Item("Message")
                    End If
                Else
                    Dim r As DataRow = ds.Tables("Property").Rows(0)

                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_0032_hotel") + "</b>: " + r.Item("HotelName") + "<br /><br />"
                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_458") + "</b>: <br /><br />" + ds.Tables("Room").Rows(0).Item("RatePlanDescription") + "<br /><br />"

                    If ds.Tables("Room").Rows(0).Item("isUvNetRate") Then
                        str += CapaPresentacion.Idiomas.get_str("str_442_netrates") 'str_442_netrates
                    Else
                        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_321_polCan") + "</b> <br />"
                        str += r.Item("CancelationPoliticies") + "<br /><br />"

                        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_322_polGar") + "</b> <br />"
                        str += r.Item("GuarantyPoliticies") + "<br /><br />"

                        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_323_polCC") + "</b> <br />"
                        str += r.Item("CreditCardPoliticies") + "<br /><br />"
                    End If

                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_324_carExtra") + "</b> <br />"
                    str += r.Item("ExtraCharges") + "<br /><br />"

                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_325_edadMaxima") + "</b> <br />"
                    str += r.Item("MaxAgeOfChildren") + " " + CapaPresentacion.Idiomas.get_str("str_381_YearsOld") + " <br /><br />"

                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_454") + "</b> <br />"
                    str += CapaPresentacion.Idiomas.get_str("str_0150_llegada") + ": " + r.Item("Checkin").ToString.Substring(0, 2) + ":" + r.Item("Checkin").ToString.Substring(2, 2) + "<br />"
                    str += CapaPresentacion.Idiomas.get_str("str_0151_salida") + ": " + r.Item("Checkout").ToString.Substring(0, 2) + ":" + r.Item("Checkout").ToString.Substring(2, 2) + "<br /><br />"

                    str += "<b>" & CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + "</b> " + IIf(r.Item("plustax"), CapaPresentacion.Idiomas.get_str("str_0176_incluidos"), r.Item("altTaxes") & " " & r.Item("altMoney")) + "<br /><br />"

                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_0117_direccion") + "</b> <br />"
                    str += r.Item("Address") + "<br />"
                    str += CapaPresentacion.Idiomas.get_str("str_0045_cp") + " " + r.Item("PostalCode") + "<br />"
                    str += r.Item("City") + ", " + r.Item("State") + ", " + r.Item("Country") + "<br />"

                    str += CapaPresentacion.Idiomas.get_str("str_0251_moneda") + ": " + r.Item("Money") + "<br /><br />"

                    str += "<b>" + CapaPresentacion.Idiomas.get_str("str_326_infoDep") + "</b> <br />"
                    str += r.Item("DepositInfo") + "<br /><br />"
                End If

                CapaPresentacion.Common.Fija_BrowserCode(frm.WebBrowser1, CONST_WB_TOP + str + CONST_WB_BOTTOM)
            End Sub

            Public Shared Sub inicializar_controles(ByVal frm As reglas)
                frm.datosBusqueda_hotel.noches = (frm.datosBusqueda_hotel.salida - frm.datosBusqueda_hotel.llegada).Days.ToString

                frm.data_lbl_adultos.Text = frm.datosBusqueda_hotel.habitaciones_.TotalAdultos
                frm.data_lbl_fechas.Text = frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy") + " - " + frm.datosBusqueda_hotel.salida.ToString("dd/MMM/yyyy")
                frm.data_lbl_ninos.Text = frm.datosBusqueda_hotel.habitaciones_.TotalNinos
                frm.data_lbl_noches.Text = frm.datosBusqueda_hotel.noches
                frm.data_lbl_habitaciones.Text = frm.datosBusqueda_hotel.habitaciones_.TotalHabitaciones
            End Sub

        End Class

        Public Class HotelBook

            Public Shared Function inicializar_controles(ByVal frm As reservar) As Boolean
                frm.dtp_in.Value = frm.datosBusqueda_hotel.llegada
                frm.dtp_out.Value = frm.datosBusqueda_hotel.salida
                frm.data_lbl_in.Text = frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy")
                frm.data_lbl_out.Text = frm.datosBusqueda_hotel.salida.ToString("dd/MMM/yyyy")
                frm.data_lbl_habitaciones.Text = frm.datosBusqueda_hotel.habitaciones_.TotalHabitaciones
                frm.data_lbl_adultos.Text = frm.datosBusqueda_hotel.habitaciones_.TotalAdultos
                frm.data_lbl_ninos.Text = frm.datosBusqueda_hotel.habitaciones_.TotalNinos
                frm.data_lbl_cadena.Text = frm.datosBusqueda_hotel.ChainCode
                frm.data_lbl_hotel.Text = frm.datosBusqueda_hotel.PropertyName
                frm.Uc_habitaciones1.Habitaciones = frm.datosBusqueda_hotel.habitaciones_.copy  ' si es nesesario por que la propiedad que regresa con copy solo es para Uc_habitaciones1
                frm.txt_codigoTarifa.Text = frm.datosBusqueda_hotel.RateCode
                frm.data_lbl_codProm.Text = frm.datosBusqueda_hotel.codProm
                frm.lblConvenioData.Text = frm.datosBusqueda_hotel.Convenio

                frm.dtp_DepFecha.Value = frm.datosBusqueda_hotel.llegada.AddDays(-1)
                frm.dtp_DepHora.Value = CDate(frm.datosBusqueda_hotel.llegada.AddDays(-1).ToString("yyyy/MM/dd") & " 12:00")


                frm.rbt_otra.Enabled = False
                frm.rbt_depositoBancario.Enabled = False
                frm.rbt_ninguno.Enabled = False
                frm.rbt_tarjetaCredito.Enabled = True
                frm.rbt_depositoBancario.Checked = False
                frm.rbt_ninguno.Checked = False
                frm.rbt_tarjetaCredito.Checked = True

                frm.rbt_garantia.Enabled = False 'True
                frm.rbt_deposito.Enabled = False 'True

                frm.chkDeposito.Checked = False
                frm.chkDeposito.Visible = False

                frm.rbt_garantia.Checked = True
                Select Case frm.datosBusqueda_hotel.GuarDep.ToUpper
                    Case "G"

                    Case "D"
                        frm.rbt_garantia.Checked = False
                        frm.rbt_deposito.Checked = True
                    Case "N"
                        frm.rbt_ninguno.Enabled = True
                        frm.rbt_ninguno.Checked = True
                        frm.rbt_garantia.Checked = False
                        frm.rbt_deposito.Checked = False
                    Case Else
                        frm.rbt_ninguno.Checked = True
                        frm.rbt_garantia.Checked = False
                        frm.rbt_deposito.Checked = False
                End Select

                If frm.datosBusqueda_hotel.AllowBankDeposit Or frm.datosBusqueda_hotel.AllowBankDepositCCT Then
                    frm.rbt_depositoBancario.Enabled = True
                End If
                If frm.datosBusqueda_hotel.AllowSpecificPayment Then
                    frm.rbt_otra.Enabled = True
                End If
                If frm.dsRules Is Nothing Then
                    frm.dsRules = obtenGenerales_hoteles.obten_general_HOR(frm.datosBusqueda_hotel)

                End If
                'forzar para mision
                If dataOperador.idCorporativo = "1" Then
                    frm.rbt_ninguno.Enabled = True
                    frm.rbt_tarjetaCredito.Enabled = True
                    frm.rbt_depositoBancario.Enabled = True
                    frm.rbt_otra.Enabled = True
                End If

                If Not SetRules(frm) Then Return False

                If frm.datosBusqueda_hotel.IsCopy Then
                    frm.rbt_ninguno.Checked = False
                    frm.rbt_tarjetaCredito.Checked = False
                    frm.rbt_depositoBancario.Checked = False
                    frm.rbt_otra.Checked = False
                    Select Case frm.datosBusqueda_hotel.DepositType
                        Case enumDepositType.None
                            frm.rbt_ninguno.Checked = True
                        Case enumDepositType.CreditCard
                            frm.rbt_tarjetaCredito.Checked = True
                        Case enumDepositType.BankDeposit
                            frm.rbt_depositoBancario.Checked = True
                        Case enumDepositType.Other
                            frm.rbt_otra.Checked = True
                            frm.txtOtra.Text = frm.datosBusqueda_hotel.PaymentOther
                    End Select

                    frm.txt_peticion.Text = frm.datosBusqueda_hotel.cust_Preferences
                    frm.txt_comentario.Text = frm.datosBusqueda_hotel.Comments

                    If frm.datosBusqueda_hotel.id_Empresa <> "" Then
                        Dim idseg As String = herramientas.get_idSegmentoEmpresa(frm.datosBusqueda_hotel.id_Empresa)
                        If frm.datosBusqueda_hotel.id_Segmento <> "" Then idseg = frm.datosBusqueda_hotel.id_Segmento
                        For x As Integer = 0 To frm.cmbSegmento.Items.Count - 1
                            If CType(frm.cmbSegmento.Items.Item(x), ListData)._Value = idseg Then
                                frm.cmbSegmento.SelectedIndex = x
                                frm.cmbSegmento.Visible = True
                                frm.lblSegmento.Visible = True

                                Exit For
                            End If
                        Next
                        'For x As Integer = 1 To frm.cmbEmpresas.Items.Count - 1
                        '    If CType(frm.cmbEmpresas.Items.Item(x), ListData)._Value = frm.datosBusqueda_hotel.id_Empresa Then
                        '        frm.cmbEmpresas.SelectedIndex = x
                        '        Exit For
                        '    End If
                        'Next
                        frm.CmbSearch1.TextBox1.Text = herramientas.get_Empresa(frm.datosBusqueda_hotel.id_Empresa)
                        frm.CmbSearch1.ListBox1.Items.Clear()
                        frm.CmbSearch1.lista.Clear()
                        frm.CmbSearch1.ListBox1.Items.Add(New ListData(frm.CmbSearch1.TextBox1.Text, frm.datosBusqueda_hotel.id_Empresa))
                        frm.CmbSearch1.lista.Add(New ListData(frm.CmbSearch1.TextBox1.Text, frm.datosBusqueda_hotel.id_Empresa))
                        frm.CmbSearch1.ListBox1.SelectedIndex = 0
                    Else
                        frm.CmbSearch1.TextBox1.Text = CapaPresentacion.Idiomas.get_str("str_437_ninguna")
                    End If
                    If frm.datosBusqueda_hotel.id_Medio <> "" Then
                        For x As Integer = 1 To frm.cmbMedios.Items.Count - 1
                            If CType(frm.cmbMedios.Items.Item(x), ListData)._Value = frm.datosBusqueda_hotel.id_Medio Then
                                frm.cmbMedios.SelectedIndex = x
                                Exit For
                            End If
                        Next
                    End If
                    frm.txtFrecuentCode.Text = frm.datosBusqueda_hotel.frecuentCode
                    frm.txtDeposito.Text = frm.datosBusqueda_hotel.DepositAmountCCT
                    frm.txtInfoDeposito.Text = frm.datosBusqueda_hotel.DepositInfoCCT

                Else
                    If Not IsNothing(misForms.newCall.datos_call.reservacion_paquete) Then frm.txt_peticion.Text = misForms.newCall.datos_call.reservacion_paquete.nota
                End If

                frm.btn_cambiar.Visible = False
                frm.lbl_mensaje.Visible = False
                frm.gpb_nuevaBusqueda.Visible = False
                frm.lbl_codProm.Visible = False
                frm.data_lbl_codProm.Visible = False
                frm.lblConvenio.Visible = False
                frm.lblConvenioData.Visible = False
                If frm.datosBusqueda_hotel.AllowBankDeposit Then
                    'If frm.datosBusqueda_hotel.llegada.ToString("yyyy/MM/dd") = Now.ToString("yyyy/MM/dd") Then
                    '    MsgBox("La opcion de deposito bancario," & vbCrLf & "no es aceptada cuando la fecha de llegada" & vbCrLf & "es el dia actual.")
                    'ElseIf Now.ToString("yyyyMMdd") >= frm.datosBusqueda_hotel.llegada.AddDays(-2).ToString("yyyyMMdd") Then
                    '    MsgBox("La opcion de deposito bancario," & vbCrLf & "solo es recomendable cuando la fecha de llegada" & vbCrLf & "es mayor a 2 dias de la fecha actual.")
                    'End If

                    If Now.ToString("yyyyMMdd") >= frm.datosBusqueda_hotel.llegada.AddDays(-2).ToString("yyyyMMdd") Then
                        MsgBox("La opcion de deposito bancario," & vbCrLf & "solo es recomendable cuando la fecha de llegada" & vbCrLf & "es mayor a 2 dias de la fecha actual.")
                    End If
                End If

                Return True
            End Function

            Public Shared Function SetRules(ByVal frm As reservar) As Boolean
                If frm.dsRules Is Nothing OrElse frm.dsRules.Tables.Count = 0 Then
                    Return False
                End If

                If Not frm.dsRules.Tables("error") Is Nothing Then
                    Dim r As DataRow = frm.dsRules.Tables("error").Rows(0)
                    MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'Me.Cursor = Cursors.Arrow
                    Return False
                Else
                    Dim r As DataRow = frm.dsRules.Tables("Property").Rows(0)
                    frm.WebBrowser1.DocumentText = CONST_WB_TOP + r.Item("DepositInfo") + CONST_WB_BOTTOM

                    'Dim tari As String = String.Format("{0:C}", CDbl(r.Item("AltTotalRate")) / datosBusqueda_hotel.noches) + " " + r.Item("AltMoney")
                    Dim tari As String = CStr(CDbl(r.Item("AltTotalRate")) / CDbl(frm.datosBusqueda_hotel.noches))
                    If r.Table.Columns.Contains("AltTotalRate") AndAlso _
                       r.Table.Columns.Contains("AltMoney") AndAlso _
                       r.Table.Columns.Contains("AltTotalExtras") AndAlso _
                       r.Table.Columns.Contains("PlusTax") AndAlso _
                       r.Table.Columns.Contains("AltTaxes") Then
                        fija_labelsTarifa(frm, tari, r.Item("AltTotalRate"), r.Item("AltMoney"), r.Item("AltTaxes"), r.Item("PlusTax"), r.Item("AltTotalExtras"))
                    End If

                    frm.datosBusqueda_hotel.AltTotalStay = r.Item("AltTotalRate")
                    frm.datosBusqueda_hotel.AltCurrency = r.Item("AltMoney")
                    frm.datosBusqueda_hotel.AltTax = r.Item("AltTaxes")
                    frm.datosBusqueda_hotel.PlusTax = r.Item("PlusTax")
                    frm.datosBusqueda_hotel.MoneyExchange = 1
                    If r.Item("AltTotalRate") > 0 Then
                        frm.datosBusqueda_hotel.MoneyExchange = CDec(r.Item("TotalRate") / (r.Item("AltTotalRate"))).ToString
                    End If

                    'datosBusqueda_hotel.AltTotalExtras = r.Table.Columns.Contains("AltTotalExtras")
                    '                                                   Contains
                    '                                                   Contains
                    '                                                   Contains
                    '                                                   Contains
                    '                                                   Contains
                    '                                                   Contains
                    '                                                   Contains
                    '                                                   Contains
                    frm.datosBusqueda_hotel.AltTotalExtras = r.Item("AltTotalExtras")

                    Return True
                End If
            End Function

            Public Shared Sub fija_labelsTarifa(ByVal frm As reservar, ByVal tari As String, ByVal subT As String, ByVal mone As String, ByVal taxe As String, ByVal plus As String, ByVal extra As String)
                tari = tari.Trim()
                'If Not tari.Contains("$") Then tari = String.Format("{0:C}", CDbl(tari.Split(" ")(0))) + " " + mone
                'If Not tari.Contains("$") Then tari = moneda.FormateaMoneda(CDbl(tari.Split(" ")(0)), mone)
                tari = CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tari), mone)
                'If subT.Contains("$") Then subT = subT.Replace("$", "").Substring(0, subT.Replace("$", "").IndexOf(" "))

                Try
                    subT = CStr(CDbl(subT) + CDbl(extra))
                Catch ex As Exception
                    'MsgBox("cachar este error 2")
                End Try

                Dim total As Double
                If CBool(plus) Then
                    total = CDbl(subT)
                Else
                    total = CDbl(subT) + CDbl(taxe)
                End If

                frm.lbl_info_tarifa.Text = CapaPresentacion.Idiomas.get_str("str_0097_tarifa") + " " + tari
                'lbl_info_subtotal.Text = CapaPresentacion.Idiomas.get_str("str_0198_subtotal") + " " + String.Format("{0:C}", CDbl(subT)) + " " + mone
                frm.lbl_info_subtotal.Text = CapaPresentacion.Idiomas.get_str("str_0198_subtotal") + " " + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(subT), mone)
                If CBool(plus) Then
                    frm.lbl_info_impuestos.Text = CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + " " + CapaPresentacion.Idiomas.get_str("str_0176_incluidos")
                Else
                    frm.lbl_info_impuestos.Text = CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + " " + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(taxe), mone)
                End If
                frm.lbl_info_total.Text = CapaPresentacion.Idiomas.get_str("str_0145_total") + " " + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(total), mone)
                frm.lbl_info_personasExtra.Text = CapaPresentacion.Idiomas.get_str("str_327_perExtra") + ": " + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(extra), mone)
            End Sub

            Public Shared Function display_controles(ByVal frm As reservar)
                Try
                    frm.dtp_in.Value = frm.datosBusqueda_hotel.llegada
                    frm.dtp_out.Value = frm.datosBusqueda_hotel.salida
                    frm.data_lbl_in.Text = frm.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy")
                    frm.data_lbl_out.Text = frm.datosBusqueda_hotel.salida.ToString("dd/MMM/yyyy")
                    frm.data_lbl_habitaciones.Text = frm.datosBusqueda_hotel.habitaciones_.TotalHabitaciones
                    frm.data_lbl_adultos.Text = frm.datosBusqueda_hotel.habitaciones_.TotalAdultos
                    frm.data_lbl_ninos.Text = frm.datosBusqueda_hotel.habitaciones_.TotalNinos
                    frm.data_lbl_cadena.Text = frm.datosBusqueda_hotel.ChainCode
                    frm.data_lbl_hotel.Text = frm.datosBusqueda_hotel.PropertyName
                    frm.Uc_habitaciones1.Habitaciones = frm.datosBusqueda_hotel.habitaciones_.copy ' si es nesesario por que la propiedad que regresa con copy solo es para Uc_habitaciones1
                    frm.txt_codigoTarifa.Text = frm.datosBusqueda_hotel.RateCode

                    frm.lbl_mensaje.Visible = True
                    frm.Uc_habitaciones1.Visible = True
                    frm.lbl_codProm.Visible = True
                    frm.data_lbl_codProm.Visible = True
                    frm.lblConvenioData.Visible = True
                    frm.data_lbl_codProm.Text = frm.datosBusqueda_hotel.codProm
                    frm.lblConvenioData.Text = frm.datosBusqueda_hotel.Convenio
                    frm.txt_peticion.Text = frm.datosBusqueda_hotel.cust_Preferences

                    frm.dtp_in.Enabled = True
                    frm.dtp_out.Enabled = True
                    frm.Uc_habitaciones1.Enabled = True

                    Select Case frm.datosBusqueda_hotel.GuarDep
                        Case "G"
                            frm.rbt_garantia.Checked = True
                            frm.rbt_tarjetaCredito.Checked = True
                        Case "D"
                            frm.rbt_deposito.Checked = True
                            frm.rbt_tarjetaCredito.Checked = True
                        Case "N"
                            'frm.rbt_deposito.Checked = True    <-Por que D:!?
                            frm.rbt_ninguno.Checked = True
                    End Select

                    frm.rbt_deposito.Enabled = False
                    frm.rbt_depositoBancario.Enabled = False
                    frm.rbt_ninguno.Enabled = False
                    frm.rbt_tarjetaCredito.Enabled = False
                    frm.rbt_garantia.Enabled = False
                    frm.rbt_otra.Enabled = False

                    If frm.tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado Then
                        'habilitar cambio de forma de pago
                        'frm.rbt_deposito.Enabled = True
                        frm.rbt_depositoBancario.Enabled = False
                        frm.rbt_ninguno.Enabled = True
                        frm.rbt_tarjetaCredito.Enabled = True
                        'frm.rbt_garantia.Enabled = True
                        frm.rbt_otra.Enabled = True

                    End If
                    If frm.tipoAccion <> enumReservar_TipoAccion.modificar Then
                        If frm.datosBusqueda_hotel.AllowBankDeposit Or frm.datosBusqueda_hotel.AllowBankDepositCCT Then
                            frm.rbt_depositoBancario.Enabled = True
                        End If
                        If frm.datosBusqueda_hotel.AllowSpecificPayment = False Then
                            frm.rbt_otra.Enabled = False
                        End If
                    End If
                    Dim ds2 As DataSet = obten_NOgeneral_HOR(frm.datosBusqueda_hotel)
                    frm.dsRules = ds2
                    If (frm.tipoAccion = enumReservar_TipoAccion.modificar And frm.cargando) Or frm.datosBusqueda_hotel.OnRequest Then
                        Dim tari As String = CStr(CDbl(frm.datosBusqueda_hotel.AltTotalStay) / (CDbl(frm.datosBusqueda_hotel.noches)))
                        moneda_selected = frm.datosBusqueda_hotel.AltCurrency
                        fija_labelsTarifa(frm, tari, frm.datosBusqueda_hotel.AltTotalStay, frm.datosBusqueda_hotel.AltCurrency, frm.datosBusqueda_hotel.AltTax, frm.datosBusqueda_hotel.PlusTax, frm.datosBusqueda_hotel.AltTotalExtras)
                    Else
                        SetRules2(frm)
                    End If

                    'forzar
                    frm.rbt_ninguno.Enabled = True
                    frm.rbt_tarjetaCredito.Enabled = True
                    frm.rbt_depositoBancario.Enabled = True
                    frm.rbt_otra.Enabled = True



                    frm.rbt_ninguno.Checked = False
                    frm.rbt_tarjetaCredito.Checked = False
                    frm.rbt_depositoBancario.Checked = False
                    frm.rbt_otra.Checked = False
                    Select Case frm.datosBusqueda_hotel.DepositType
                        Case enumDepositType.None
                            frm.rbt_ninguno.Checked = True
                        Case enumDepositType.CreditCard
                            frm.rbt_tarjetaCredito.Checked = True
                        Case enumDepositType.BankDeposit
                            frm.rbt_depositoBancario.Checked = True
                        Case enumDepositType.Other
                            frm.rbt_otra.Checked = True
                            frm.txtOtra.Text = frm.datosBusqueda_hotel.PaymentOther
                    End Select
                    frm.dtp_DepFecha.Value = Now
                    frm.dtp_DepHora.Value = Now
                    If frm.datosBusqueda_hotel.DepositLimitDate.ToString("yyyy") <> "0001" Then
                        frm.dtp_DepFecha.Value = frm.datosBusqueda_hotel.DepositLimitDate
                        frm.dtp_DepHora.Value = frm.datosBusqueda_hotel.DepositLimitDate
                        frm.chkDeposito.Enabled = True
                    End If

                    frm.txt_comentario.Text = frm.datosBusqueda_hotel.Comments


                    frm.chkDeposito.Checked = False
                    If frm.rbt_depositoBancario.Checked And frm.datosBusqueda_hotel.DepositLimitDate.ToString("yyyy") = "0001" Then frm.chkDeposito.Checked = True

                    If frm.datosBusqueda_hotel.DepositType = enumDepositType.CreditCard And frm.tipoAccion = enumReservar_TipoAccion.modificar Then
                        frm.rbt_ninguno.Enabled = False
                        frm.rbt_tarjetaCredito.Enabled = False
                        frm.rbt_depositoBancario.Enabled = False
                        frm.rbt_otra.Enabled = False
                    End If
                    frm.cmbSegmento.Visible = False
                    frm.lblSegmento.Visible = False

                    If frm.datosBusqueda_hotel.id_Empresa <> "" Then
                        Dim idseg As String = herramientas.get_idSegmentoEmpresa(frm.datosBusqueda_hotel.id_Empresa)
                        If frm.datosBusqueda_hotel.id_Segmento <> "" Then idseg = frm.datosBusqueda_hotel.id_Segmento
                        For x As Integer = 0 To frm.cmbSegmento.Items.Count - 1
                            If CType(frm.cmbSegmento.Items.Item(x), ListData)._Value = idseg Then
                                frm.cmbSegmento.SelectedIndex = x
                                frm.cmbSegmento.Visible = True
                                frm.lblSegmento.Visible = True

                                Exit For
                            End If
                        Next
                        'For x As Integer = 1 To frm.cmbEmpresas.Items.Count - 1
                        '    If CType(frm.cmbEmpresas.Items.Item(x), ListData)._Value = frm.datosBusqueda_hotel.id_Empresa Then
                        '        frm.cmbEmpresas.SelectedIndex = x
                        '        Exit For
                        '    End If
                        'Next
                        frm.CmbSearch1.TextBox1.Text = herramientas.get_Empresa(frm.datosBusqueda_hotel.id_Empresa)
                        frm.CmbSearch1.ListBox1.Items.Clear()
                        frm.CmbSearch1.lista.Clear()
                        frm.CmbSearch1.ListBox1.Items.Add(New ListData(frm.CmbSearch1.TextBox1.Text, frm.datosBusqueda_hotel.id_Empresa))
                        frm.CmbSearch1.lista.Add(New ListData(frm.CmbSearch1.TextBox1.Text, frm.datosBusqueda_hotel.id_Empresa))
                        frm.CmbSearch1.ListBox1.SelectedIndex = 0
                    Else
                        frm.CmbSearch1.TextBox1.Text = CapaPresentacion.Idiomas.get_str("str_437_ninguna")
                    End If
                    If frm.datosBusqueda_hotel.id_Medio <> "" Then
                        For x As Integer = 1 To frm.cmbMedios.Items.Count - 1
                            If CType(frm.cmbMedios.Items.Item(x), ListData)._Value = frm.datosBusqueda_hotel.id_Medio Then
                                frm.cmbMedios.SelectedIndex = x
                                Exit For
                            End If
                        Next
                    End If
                    frm.txtFrecuentCode.Text = frm.datosBusqueda_hotel.frecuentCode
                    frm.txtDeposito.Text = frm.datosBusqueda_hotel.DepositAmountCCT
                    frm.txtInfoDeposito.Text = frm.datosBusqueda_hotel.DepositInfoCCT
                    'frm.WebBrowser1.DocumentText = CONST_WB_TOP + frm.datosBusqueda_hotel.DepositInfoCCT + CONST_WB_BOTTOM
                    If Not IsNothing(frm.dsRules.Tables("Property")) AndAlso Not IsNothing(frm.dsRules.Tables("Property").Rows(0).Item("DepositInfo")) Then
                        frm.WebBrowser1.DocumentText = CONST_WB_TOP + frm.dsRules.Tables("Property").Rows(0).Item("DepositInfo") + CONST_WB_BOTTOM
                    Else
                        frm.WebBrowser1.DocumentText = CONST_WB_TOP + CONST_WB_BOTTOM
                    End If
                    frm.btn_cambiar.Visible = True
                    frm.cambiaron = False
                    Return True
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0039", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                    frm.cambiaron = False
                    Return False
                End Try
            End Function

            Private Shared Sub SetRules2(ByVal frm As reservar)
                If frm.dsRules.Tables.Count = 0 Then
                    Return
                End If

                If Not frm.dsRules.Tables("error") Is Nothing Then
                    Dim r As DataRow = frm.dsRules.Tables("error").Rows(0)
                    Dim mostro As Boolean = False
                    Dim str_msg As String = ""

                    If r.Item("Message").ToString.Contains("No Exceede Max People") AndAlso r.Item("ErrorCode") = "7" Then
                        'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_328_errMaxPer"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        str_msg = CapaPresentacion.Idiomas.get_str("str_328_errMaxPer")
                        mostro = True
                    End If

                    If r.Item("Message").ToString.Contains("Min Number Rooms Restricted") AndAlso r.Item("ErrorCode") = "32" Then
                        'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_330_errMinPer"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        str_msg = CapaPresentacion.Idiomas.get_str("str_330_errMinPer")
                        mostro = True
                    End If


                    If Not mostro Then str_msg = r.Item("Message")
                    MessageBox.Show(str_msg, "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'Me.Cursor = Cursors.Arrow
                    frm.lbl_mensaje.Text = str_msg
                    frm.btn_reservar.Enabled = False
                    Return
                Else
                    frm.lbl_mensaje.Text = ""
                    frm.btn_reservar.Enabled = True

                    Dim r As DataRow = frm.dsRules.Tables("Property").Rows(0)
                    frm.WebBrowser1.DocumentText = CONST_WB_TOP + r.Item("DepositInfo") + CONST_WB_BOTTOM

                    'Dim tari As String = String.Format("{0:C}", CDbl(r.Item("AltTotalRate")) / datosBusqueda_hotel.noches) + " " + r.Item("AltMoney")
                    Dim tari As String = CStr(CDbl(r.Item("AltTotalRate")) / CDbl(frm.datosBusqueda_hotel.noches))
                    If r.Table.Columns.Contains("AltTotalRate") AndAlso _
                       r.Table.Columns.Contains("AltMoney") AndAlso _
                       r.Table.Columns.Contains("AltTotalExtras") AndAlso _
                       r.Table.Columns.Contains("PlusTax") AndAlso _
                       r.Table.Columns.Contains("AltTaxes") Then
                        fija_labelsTarifa(frm, tari, r.Item("AltTotalRate"), r.Item("AltMoney"), r.Item("AltTaxes"), r.Item("PlusTax"), r.Item("AltTotalExtras"))
                    End If

                    frm.datosBusqueda_hotel.AltTotalStay = r.Item("AltTotalRate")
                    frm.datosBusqueda_hotel.AltCurrency = r.Item("AltMoney")
                    frm.datosBusqueda_hotel.AltTax = r.Item("AltTaxes")
                    frm.datosBusqueda_hotel.PlusTax = r.Item("PlusTax")
                End If
            End Sub

            Public Shared Function obten_NOgeneral_HOR(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
                Dim data As New datos_HOR
                data.ServiceProvider = datosBusqueda_hotel.ServiceProvider
                data.CheckinDate = datosBusqueda_hotel.llegada.ToString(CapaPresentacion.Common.DateDataFormat)
                data.CheckoutDate = datosBusqueda_hotel.salida.ToString(CapaPresentacion.Common.DateDataFormat)
                data.AccessCode = datosBusqueda_hotel.codProm
                data.ReferenceContract = datosBusqueda_hotel.Convenio
                data.RateCode = datosBusqueda_hotel.RateCode
                data.PropertyNumber = datosBusqueda_hotel.PropertyNumber
                data.Nights = datosBusqueda_hotel.salida.Subtract(datosBusqueda_hotel.llegada).TotalDays
                data.ChainCode = datosBusqueda_hotel.ChainCode
                data.AltCurrency = datosBusqueda_hotel.monedaBusqueda

                Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_HOR(data, datosBusqueda_hotel)
                Return ds
            End Function

            Public Shared Sub refresh_datosBusqueda(ByVal frm As reservar)
                Dim garantiaTarifa As String = ""
                Dim Dep As enumDepositType
                If frm.rbt_tarjetaCredito.Checked Then
                    Dep = enumDepositType.CreditCard
                    'If frm.rbt_garantia.Checked Then
                    garantiaTarifa = "G"
                    'Else
                    '    garantiaTarifa = "D"
                    'End If
                End If
                If frm.rbt_depositoBancario.Checked Then
                    Dep = enumDepositType.BankDeposit
                    garantiaTarifa = "D"
                    If frm.chkDeposito.Checked Then garantiaTarifa = "G"
                End If
                If frm.rbt_ninguno.Checked Then
                    Dep = enumDepositType.None
                    garantiaTarifa = "N"
                End If
                frm.datosBusqueda_hotel.PaymentOther = ""
                If frm.rbt_otra.Checked Then
                    Dep = enumDepositType.Other
                    garantiaTarifa = "N"
                    'TODO: dato del pago
                    frm.datosBusqueda_hotel.PaymentOther = frm.txtOtra.Text
                End If
                frm.datosBusqueda_hotel.DepositType = Dep
                frm.datosBusqueda_hotel.GuarDep = garantiaTarifa
                frm.datosBusqueda_hotel.cust_Preferences = frm.txt_peticion.Text

                frm.datosBusqueda_hotel.llegada = frm.dtp_in.Value
                frm.datosBusqueda_hotel.salida = frm.dtp_out.Value


                Dim tar As Decimal
                If frm.datosBusqueda_hotel.OnRequest Then tar = frm.datosBusqueda_hotel.AltTotalStay / (frm.datosBusqueda_hotel.noches * frm.datosBusqueda_hotel.habitaciones_.TotalHabitaciones)

                frm.datosBusqueda_hotel.habitaciones_ = frm.Uc_habitaciones1.Habitaciones.copy
                frm.datosBusqueda_hotel.noches = (frm.datosBusqueda_hotel.salida - frm.datosBusqueda_hotel.llegada).Days.ToString
                If frm.datosBusqueda_hotel.OnRequest Then
                    frm.datosBusqueda_hotel.AltTotalStay = tar * frm.datosBusqueda_hotel.noches * frm.datosBusqueda_hotel.habitaciones_.TotalHabitaciones
                    frm.datosBusqueda_hotel.AltTax = frm.datosBusqueda_hotel.AltTotalStay * (CDec(frm.datosBusqueda_hotel.Tax) / 100)
                End If




                frm.datosBusqueda_hotel.Comments = frm.txt_comentario.Text
                frm.datosBusqueda_hotel.DepositLimitDate = CDate(frm.dtp_DepFecha.Value.ToString("yyyy/MM/dd") & " " & frm.dtp_DepHora.Value.ToString("HH:mm"))
                frm.datosBusqueda_hotel.DepositAmountCCT = ""
                frm.datosBusqueda_hotel.DepositInfoCCT = ""
                If frm.chkDeposito.Checked Then
                    frm.datosBusqueda_hotel.DepositLimitDate = Nothing
                    frm.datosBusqueda_hotel.DepositAmountCCT = frm.txtDeposito.Text
                    frm.datosBusqueda_hotel.DepositInfoCCT = frm.txtInfoDeposito.Text
                End If


                frm.datosBusqueda_hotel.id_Empresa = ""
                frm.datosBusqueda_hotel.id_Medio = ""
                frm.datosBusqueda_hotel.id_Segmento = ""
                If frm.pnlCatalogos.Visible Then
                    'If Not frm.cmbEmpresas.SelectedItem Is Nothing Then frm.datosBusqueda_hotel.id_Empresa = frm.cmbEmpresas.SelectedItem._value
                    frm.datosBusqueda_hotel.id_Empresa = frm.CmbSearch1.selected_valor
                    If frm.datosBusqueda_hotel.id_Empresa <> "" AndAlso Not frm.cmbSegmento.SelectedItem Is Nothing Then frm.datosBusqueda_hotel.id_Segmento = frm.cmbSegmento.SelectedItem._value
                    If Not frm.cmbMedios.SelectedItem Is Nothing Then frm.datosBusqueda_hotel.id_Medio = frm.cmbMedios.SelectedItem._value
                End If
                frm.datosBusqueda_hotel.frecuentCode = frm.txtFrecuentCode.Text

            End Sub

        End Class

    End Class

    Public Class Calls

        Public Shared Sub ActivaNewCall()
            Dim tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab = misForms.form1.UltraTabbedMdiManager1.TabFromForm(misForms.newCall)
            misForms.form1.UltraTabbedMdiManager1.TabGroups(tab.TabGroup.Index).SelectedTab = tab
        End Sub

    End Class

End Class