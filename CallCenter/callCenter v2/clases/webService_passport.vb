Imports System.Xml

Public Class webService_passport

    Public Enum SessionState
        OnLine = 0
        LogOut = 1
        Expired = 2
        DoesntExists = 3
    End Enum

    Public Enum FilterReservationDates
        Checkin
        RegisterDate
        CheckOut
    End Enum

    Private Const TicketKey As String = "TK_KEY"
    'Private Const TicketUserKey As String = "TK_USERKEY"
    Private Const TicketCreditKey As String = "TK_CREDITKEY"
    'Private Const TicketTravelersKey As String = "TK_TRAVELERKEY"  - sust por - p_userTD
    Private Const TicketMaincontactKey As String = "TK_MAINCONTACTKEY"
    Private Const TicketGroupKey As String = "TK_GROUPKEY"
    'Private Const TicketAgencyKey As String = "TK_AGENCYKEY"

    Private _UserData As ResUserLogin                   'Informacion del Usuario.
    'Private _UserCreditCardData As ResCreditCard        'Informacion del Usuario.
    Private _UserTravelerData As ResTraveler            'Informacion del Usuario.
    'Private _MainContact As ResTraveler
    'Private _UserGroupInfo As ResGroupInfo
    Private _AgenciesInfo As ResAgencies

    Private Shared Passport As WS_PASSPORT.Passport      'por medio de aqui se haran las llamadas
    Private Shared Auth As WS_PASSPORT.AuthHeader
    Private _LastMessage As String = ""


    Private p_ticket As String = ""
    Private p_userTD As ResTraveler
    Private p_userD As ResUserLogin
    'Private p_AgenciesInfo As ResAgencies



#Region "Constructores y Inicializadores"
    'This constructor method set up all information neeeded to acces passport web services.
    Private Function Initialize() As Boolean
        Try
            If Passport Is Nothing Then Passport = New WS_PASSPORT.Passport
            If Auth Is Nothing Then Auth = New WS_PASSPORT.AuthHeader

            'Dim cfg As PortalServiceProvidersCfg = New PortalServiceProvidersCfg(0, enumServices.Passport)
            Auth.AuthenticationName = username
            Auth.AuthenticationPassword = password
            Passport.AuthHeaderValue = Auth
            Passport.Url = CapaAccesoDatos.XML.wsData.UrlWsPassport
            Return True
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0079", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return False
        End Try
    End Function

    Public Sub New(ByRef res As Boolean)
        res = Initialize()
    End Sub

    'Public Sub New(ByVal Ticket As String)
    '    Initialize()
    '    SignInWithTicket(Ticket)
    'End Sub
#End Region

#Region "Metodo privado para el signin con ticket."
    'Private Function SignInWithTicket(ByVal Tk As String) As Boolean
    '    Dim dsReqTicket As New ReqTicket
    '    Dim drTicket As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow

    '    drTicket.TicketId = Tk
    '    drTicket.Language = "EN"
    '    dsReqTicket.Ticket.AddTicketRow(drTicket)

    '    Dim xmlresp As XmlElement = Passport.KeepAlive(New XmlDataDocument(dsReqTicket).DocumentElement)

    '    If Not xmlresp Is Nothing Then
    '        Dim TicketReader As New XmlTextReader(xmlresp.OuterXml, XmlNodeType.Element, Nothing)
    '        Dim respTK As New ResTicket
    '        respTK.ReadXml(TicketReader)
    '        'si el ticket esta activo en el servidor entonces
    '        If respTK.Ticket(0).Status = SessionState.OnLine Then
    '            'tengo los datos en la session??
    '            If TypeOf (Current.Session(TicketUserKey)) Is ResUserLogin Then
    '                _UserData = CType(Current.Session(TicketUserKey), ResUserLogin)
    '            Else 'pide la informacion de nuevo y dejala en el cache. por medio del ticket. 
    '                'se perdio la informacion
    '                Me._LastMessage = "Session has Expired"
    '                Return False
    '            End If
    '            If Current.Session(TicketKey) <> Tk Then Current.Session(TicketKey) = Tk
    '            Return True
    '        Else 'no se encontro el ticket notificalo
    '            Me._LastMessage = respTK.Ticket(0).Status
    '            Return False
    '        End If
    '    Else 'Error en conexion
    '        Me._LastMessage = "Authentication Server Connection Error"
    '        Return False
    '    End If
    'End Function
#End Region

#Region "Propiedades Privadas"
    'Propiedad principal para accessar a la informacion del usuario en Session
    Private ReadOnly Property UserInfo() As ResUserLogin
        Get
            If TypeOf (p_userD) Is ResUserLogin Then
                _UserData = CType(p_userD, ResUserLogin)
                Return _UserData
            Else
                Return Nothing
            End If
        End Get
    End Property

    ''Propiedad principal para accessar a la informacion del usuario en Session
    Private ReadOnly Property UserTravelerInfo() As ResTraveler
        Get
            If TypeOf (p_userTD) Is ResTraveler Then
                _UserTravelerData = CType(p_userTD, ResTraveler)
                Return _UserTravelerData
            Else
                Return Nothing
            End If
        End Get
    End Property

    'Private ReadOnly Property MainContact() As ResTraveler
    '    Get
    '        If TypeOf (Current.Session(TicketMaincontactKey)) Is ResTraveler Then
    '            _MainContact = CType(Current.Session(TicketMaincontactKey), ResTraveler)
    '            Return _MainContact
    '        Else
    '            Return Nothing
    '        End If
    '    End Get
    'End Property

    ''Propiedad principal para accessar a la informacion del usuario en Session
    'Private ReadOnly Property UserCreditCardInfo() As ResCreditCard
    '    Get
    '        If TypeOf (Current.Session(TicketCreditKey)) Is ResCreditCard Then
    '            _UserCreditCardData = CType(Current.Session(TicketCreditKey), ResCreditCard)
    '            Return _UserCreditCardData
    '        Else
    '            Return Nothing
    '        End If
    '    End Get
    'End Property

    ''Propiedad principal para accessar a la informacion de grupo del usuario en session
    'Private ReadOnly Property UserGroupInfo() As ResGroupInfo
    '    Get
    '        If TypeOf (Current.Session(TicketGroupKey)) Is ResGroupInfo Then
    '            _UserGroupInfo = CType(Current.Session(TicketGroupKey), ResGroupInfo)
    '            Return _UserGroupInfo
    '        Else
    '            Return Nothing
    '        End If
    '    End Get
    'End Property

    'Private ReadOnly Property AgenciesInfo() As ResAgencies
    '    Get
    '        If TypeOf (p_agenciesI) Is ResAgencies Then
    '            _AgenciesInfo = CType(p_agenciesI, ResAgencies)
    '            Return _AgenciesInfo
    '        Else
    '            Return Nothing
    '        End If
    '    End Get
    'End Property
#End Region

#Region "Propiedades Publicas"
    Public ReadOnly Property get_p_ticket() As String
        Get
            Return p_ticket
        End Get
    End Property

    'Public Function HasAgencySelected(ByRef drAgency As DataRow) As Boolean
    '    If IsAgencyCompany Then
    '        Dim dr() As DataRow
    '        dr = AgenciesInfo.Agency.Select("IsSelected=" & True)
    '        If dr.Length > 0 Then
    '            drAgency = dr(0)
    '            Return True
    '        End If
    '    End If
    '    Return False
    'End Function

    Public ReadOnly Property LastMessage() As String
        Get
            Return _LastMessage
        End Get
    End Property

    'Public Shared ReadOnly Property TicketId() As String
    '    Get
    '        Return HttpContext.Current.Session(TicketKey)
    '        'If UserInfo Is Nothing Then
    '        '    Return ""
    '        'Else
    '        '    Return UserInfo.Ticket(0).TicketId
    '        'End If
    '    End Get
    'End Property

    Public ReadOnly Property UserRoles() As ResUserLogin.RoleDataTable
        Get
            If Not UserInfo Is Nothing Then
                Return UserInfo.Role
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property IsAuthenticated() As Boolean
        Get
            If UserInfo Is Nothing Then
                UserLogin(dataOperador.user, dataOperador.pass)
            End If

            If UserInfo Is Nothing Then
                Return False
            Else
                If Me.KeepAlive <> SessionState.OnLine Then
                    UserLogin(dataOperador.user, dataOperador.pass)
                End If
                If Me.KeepAlive = SessionState.OnLine Then
                    Return True
                Else
                    UserLogOut()
                    Return False
                End If
            End If
        End Get
    End Property

    Public ReadOnly Property UserId() As Integer
        Get
            If UserInfo Is Nothing Then
                UserLogin(dataOperador.user, dataOperador.pass)
            End If
            If UserInfo Is Nothing Then
                Return 0
            Else
                If Me.KeepAlive <> SessionState.OnLine Then
                    UserLogin(dataOperador.user, dataOperador.pass)
                End If
                If Me.KeepAlive = SessionState.OnLine Then
                    Return UserInfo.UserAccount(0).UserId
                Else
                    Return 0
                End If
            End If
        End Get
    End Property

    Public ReadOnly Property IsAdministrator() As Boolean
        Get
            Return GetRole("Supervisor")
        End Get
    End Property

    Public ReadOnly Property IsUser() As Boolean
        Get
            Return GetRole("User")
        End Get
    End Property

    Public ReadOnly Property IsCustomer() As Boolean
        Get
            Return GetRole("Customer")
        End Get
    End Property

    Public ReadOnly Property IsAgent() As Boolean
        Get
            Return GetRole("Agent")
        End Get
    End Property

    Public ReadOnly Property IsAgencyCompany() As Boolean
        Get
            Return GetRole("AgencyCompany")
        End Get
    End Property

    Public ReadOnly Property IsUserAssociation() As Boolean
        Get
            Return GetRole("UserAssociation")
        End Get
    End Property

    Public ReadOnly Property IsUserChain() As Boolean
        Get
            Return GetRole("UserChain")
        End Get
    End Property

    Public ReadOnly Property IsUnregisterUser() As Boolean
        Get
            Return GetRole("UnregisterUser")
        End Get
    End Property

    'Public ReadOnly Property IsAgencyCompany() As Boolean
    '    Get
    '        If (New PortalPartnersCfg).AllowAgency Then
    '            Return GetRole("AgencyCompany")
    '        End If
    '        Return False
    '    End Get
    'End Property

    Public ReadOnly Property IsManager() As Boolean
        Get
            Return GetRole("Manager")
        End Get
    End Property

    Public ReadOnly Property IsManagerGroup() As Boolean
        Get
            Return GetRole("ManagerGroup")
        End Get
    End Property

    Public ReadOnly Property IsMemberGroup() As Boolean
        Get
            Return GetRole("MemberGroup")
        End Get
    End Property

    Public ReadOnly Property IdAsociacion() As String
        Get
            Try
                If UserInfo.UserAccount(0).IsidAsociacionNull Then
                    Return "-1"
                Else
                    Return UserInfo.UserAccount(0).idAsociacion
                End If
            Catch ex As Exception
                Return "-1"
            End Try
        End Get
    End Property

    Public ReadOnly Property IsAgentCorporateCCT() As Boolean
        Get
            Return GetRole("AgentCorporateCCT")
        End Get
    End Property

#End Region

#Region "metodos Privados"
    Private Function GetRole(ByVal role As String) As Boolean
        If Me.IsAuthenticated Then
            Dim dr As ResUserLogin.RoleRow
            For Each dr In UserInfo.Role
                If dr.RoleName = role Then
                    Return True
                End If
            Next
        End If
        Return False
    End Function
#End Region

#Region "Metodos Publicos Para loguearse, desloguearse y controlar la session"
    Public Function getUserAccessInfo() As ResUserLogin.UserAccountRow
        If Not UserInfo Is Nothing Then
            Return UserInfo.UserAccount(0)
        Else
            UserLogin(dataOperador.user, dataOperador.pass)
            If Not UserInfo Is Nothing Then
                Return UserInfo.UserAccount(0)
            Else
                Return Nothing
            End If
        End If
    End Function

    'Method to Log In the User
    Public Function UserLogin(ByVal User As String, ByVal Password As String) As Boolean
        Dim data As New XmlDataDocument(New ReqUserLogin)
        Dim ds As ReqUserLogin = CType(data.DataSet, ReqUserLogin) 'enlace
        Dim dr As ReqUserLogin.AuthenticateDataRow
        Try 
            dr = ds.AuthenticateData.NewAuthenticateDataRow
            dr.AccessId = User
            dr.Password = Password
            dr.GetTravelers = False
            dr.GetRoles = True
            dr.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            ds.AuthenticateData.AddAuthenticateDataRow(dr)
            Dim s As String = Passport.UserLogin(data.DocumentElement).OuterXml
            Dim text As New XmlTextReader(s, XmlNodeType.Element, Nothing)
            If Not text Is Nothing Then
                Dim respuesta As New XmlDataDocument(New ResUserLogin)
                Dim resp As ResUserLogin = CType(respuesta.DataSet, ResUserLogin)
                resp.ReadXml(text)
                If resp._Error.Count > 0 Then
                    _LastMessage = resp._Error(0).Message
                Else
                    'guarda la id del operador
                    dataOperador.user_ID = resp.UserAccount.Rows(0).Item("UserId")
                    If Not IsDBNull(resp.UserAccount.Rows(0).Item("idCorporativo")) Then dataOperador.idCorporativo = resp.UserAccount.Rows(0).Item("idCorporativo")
                    ' If resp.UserAccount.Columns.Contains("idCallcenter") Then dataOperador.idCallcenter = resp.UserAccount.Rows(0).Item("idCallcenter")
                    If Not IsDBNull(resp.UserAccount.Rows(0).Item("mostrarCatalogo")) Then
                        dataOperador.MostrarCatalogos = IIf(resp.UserAccount.Rows(0).Item("mostrarCatalogo") = "false", False, True)
                    End If

                    _UserData = New ResUserLogin
                    _UserData.ReadXml(New XmlTextReader(resp.GetXml, XmlNodeType.Document, Nothing))
                    '---Current.Session(TicketKey) = _UserData.Ticket(0).TicketId

                    p_userD = _UserData  'Current.Session(TicketUserKey) = _UserData
                    p_ticket = _UserData.Ticket(0).TicketId
                    Return True
                End If
            End If
            'aqui podria hacer la peticion de los travelers
            'tambien las delas tarjetas. pero se hara en otra opcion
        Catch ex As System.Threading.ThreadAbortException
            ' no hacer nada
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0080", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try

        _UserData = Nothing
        Return False
    End Function

    'Methos to Log Out The User
    Public Sub UserLogOut()
        Dim doc_ReqTicket As New XmlDataDocument(New ReqTicket)
        Dim dsReqTicket As ReqTicket = CType(doc_ReqTicket.DataSet, ReqTicket)
        Dim drk As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow()
        Try
            drk.TicketId = _UserData.Ticket(0).TicketId
            drk.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dsReqTicket.Ticket.AddTicketRow(drk)
            Dim op As XmlElement = Passport.LogOut(doc_ReqTicket.DocumentElement)
            If Not op Is Nothing Then
                Dim TicketReader As New XmlTextReader(op.OuterXml, XmlNodeType.Element, Nothing)
                Dim respTrav As New ResTicket
            Else
                Dim i As Integer = 0
            End If
        Catch ex As Exception
            'no existian datos para el signout
            CapaLogicaNegocios.ErrorManager.Manage("E0081", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
        _UserData = Nothing
        '_UserCreditCardData = Nothing
        '_UserTravelerData = Nothing
        'Current.Session.Remove(TicketKey)
        p_userD = Nothing 'Current.Session.Remove(TicketUserKey)
        'Current.Session.Remove(TicketCreditKey)
        p_userTD = Nothing
        'Current.Session.Remove(TicketMaincontactKey)
        'Current.Session.Remove(TicketGroupKey)
        'p_agenciesI = Nothing 'Current.Session.Remove(TicketAgencyKey)
        'Current.Session.Remove("USRAGENCYLOGG")
    End Sub

    'This functions tries to keep session login Open in passport server
    Public Shadows Function KeepAlive() As SessionState
        Try
            If Not p_ticket Is Nothing AndAlso p_ticket <> "" Then
                Dim doc_ReqTicket As New XmlDataDocument(New ReqTicket)
                Dim dsReqTicket As ReqTicket = CType(doc_ReqTicket.DataSet, ReqTicket)
                'Dim drt As ResUserLogin.TicketRow
                Dim drk As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow()
                drk.TicketId = p_ticket
                drk.Language = CapaPresentacion.Idiomas.cultura.ToUpper
                dsReqTicket.Ticket.AddTicketRow(drk)
                Dim xmlResponse As XmlElement = Passport.KeepAlive(doc_ReqTicket.DocumentElement)
                If Not xmlResponse Is Nothing Then
                    Dim ticketreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
                    Dim resptk As New ResTicket
                    resptk.ReadXml(ticketreader)
                    If resptk.Ticket.Count > 0 Then
                        '//Sino si genero el ticket
                        Return resptk.Ticket(0).Status
                    End If
                End If
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0082", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
        Return SessionState.DoesntExists
    End Function
#End Region

#Region "Metodos P�blicos De manipulacion de viajeros a traves del ws"
    Public Function getUserTravelers() As ResTraveler.TravelerDataTable
        Try
            If UserInfo Is Nothing Then
                UserLogin(dataOperador.user, dataOperador.pass)
            Else
                If Me.KeepAlive <> SessionState.OnLine Then
                    UserLogin(dataOperador.user, dataOperador.pass)
                End If
            End If
            If Not UserInfo Is Nothing Then 'debe haber almenos la informaicon del usuario
                'que exista la informacion y ademas las llaves iguales.
                If UserTravelerInfo Is Nothing OrElse UserInfo.Ticket(0).TicketId <> UserTravelerInfo.Ticket(0).TicketId Then
                    'retrieve data.
                    Dim data As New XmlDataDocument(New ReqTicket)
                    Dim ds As ReqTicket = CType(data.DataSet, ReqTicket)
                    Dim dr As ReqTicket.TicketRow
                    dr = ds.Ticket.NewTicketRow
                    dr.TicketId = UserInfo.Ticket(0).TicketId
                    dr.Language = CapaPresentacion.Idiomas.cultura.ToUpper
                    ds.Ticket.AddTicketRow(dr)
                    Dim text As New XmlTextReader(Passport.GetTravelers(data.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
                    If Not text Is Nothing Then
                        Dim respuesta As New XmlDataDocument(New ResTraveler)
                        Dim resp As ResTraveler = CType(respuesta.DataSet, ResTraveler)
                        resp.ReadXml(text)
                        If resp.Ticket.Count > 0 AndAlso resp.Ticket(0).Status <> Me.SessionState.OnLine Then
                            UserLogOut()
                            _LastMessage = "El ticket no tiene status online!!!!!"
                            Return Nothing
                        ElseIf resp._Error.Count > 0 Then
                            _LastMessage = resp._Error(0).Message
                            Return Nothing
                        Else
                            _UserTravelerData = New ResTraveler
                            _UserTravelerData.ReadXml(New XmlTextReader(resp.GetXml, XmlNodeType.Document, Nothing))
                            p_userTD = _UserTravelerData
                        End If
                    End If
                End If
                Return UserTravelerInfo.Traveler
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0083", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
        Return Nothing
    End Function

    '12 Febrero: se perdi� el cambio del passport que traia el usuario por id..
    Public Function getUserTravelerById(ByVal id As Long) As ResTraveler.TravelerDataTable
        Try

            If Not UserInfo Is Nothing Then 'debe haber almenos la informaicon del usuario
                'que exista la informacion y ademas las llaves iguales.
                'retrieve data.
                Dim data As New XmlDataDocument(New ReqSearchTravelerByClientId)
                Dim ds As ReqSearchTravelerByClientId = CType(data.DataSet, ReqSearchTravelerByClientId)
                Dim dr As ReqSearchTravelerByClientId.TicketRow
                Dim drUser As ReqSearchTravelerByClientId.userRow

                drUser = ds.user.NewuserRow
                ds.user.AdduserRow(drUser)
                dr = ds.Ticket.NewTicketRow
                dr.TicketId = UserInfo.Ticket(0).TicketId
                dr.Language = "EN"
                ds.Ticket.AddTicketRow(dr)
                dr.SetParentRow(drUser)
                Dim drT As ReqSearchTravelerByClientId.TravelerRow
                drT = ds.Traveler.NewTravelerRow
                drT.ClientId = id
                drT.Status = 0
                ds.Traveler.AddTravelerRow(drT)
                drT.SetParentRow(drUser)
                Dim text As New XmlTextReader(Passport.GetTravelerByClientId(data.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
                If Not text Is Nothing Then
                    Dim respuesta As New XmlDataDocument(New ResTraveler)
                    Dim resp As ResTraveler = CType(respuesta.DataSet, ResTraveler)
                    resp.ReadXml(text)
                    If resp.Ticket.Count > 0 AndAlso resp.Ticket(0).Status <> Me.SessionState.OnLine Then
                        UserLogOut()
                        _LastMessage = "El ticket no tiene status online"
                    ElseIf resp._Error.Count > 0 Then
                        _LastMessage = resp._Error(0).Message
                    Else
                        Return resp.Traveler
                    End If
                End If
            End If
        Catch ex As Exception
            'PortalServiceTracer.ServiceTracer("Passport: error en la funci�n getUserTravelerById:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
            CapaLogicaNegocios.ErrorManager.Manage("E0084", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return Nothing
    End Function

    'Public Function FindTravelersByAgency(ByVal name As String, ByVal agency As Integer) As ResTraveler.TravelerDataTable
    '    Try
    '        If Not UserInfo Is Nothing Then 'debe haber almenos la informaicon del usuario

    '            Dim data As New XmlDataDocument(New ReqSearchTraveler)
    '            Dim ds As ReqSearchTraveler = CType(data.DataSet, ReqSearchTraveler)
    '            Dim drU As ReqSearchTraveler.UserRow = ds.User.NewRow

    '            Dim drk As ReqSearchTraveler.TicketRow = ds.Ticket.NewTicketRow
    '            drk.TicketId = p_ticket
    '            drk.Language = idiomas.cultura.ToUpper
    '            ds.Ticket.AddTicketRow(drk)

    '            Dim drt As ReqSearchTraveler.TravelerRow = ds.Traveler.NewTravelerRow
    '            drt.Status = 0
    '            drt.Nombre = name.Trim
    '            drt.AgencyId = agency
    '            ds.Traveler.AddTravelerRow(drt)

    '            Dim text As New XmlTextReader(Passport.GetTravelersByName(data.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
    '            If Not text Is Nothing Then
    '                Dim respuesta As New XmlDataDocument(New ResTraveler)
    '                Dim resp As ResTraveler = CType(respuesta.DataSet, ResTraveler)
    '                resp.ReadXml(text)
    '                If resp.Ticket.Count > 0 AndAlso resp.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                    UserLogOut()
    '                    _LastMessage = "El ticket no tiene status online"
    '                    Return Nothing
    '                ElseIf resp._Error.Count > 0 Then
    '                    _LastMessage = resp._Error(0).Message
    '                    Return Nothing
    '                Else
    '                    Dim TravelerDt As ResTraveler
    '                    TravelerDt = New ResTraveler
    '                    TravelerDt.ReadXml(New XmlTextReader(resp.GetXml, XmlNodeType.Document, Nothing))
    '                    Return TravelerDt.Traveler
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        messagebox.show(ex.Message)
    '    End Try

    '    Return Nothing
    'End Function

    '
    '
    '
    '    'This functions add in memory a new traveles  -  if you want to save you must invoke to userupdate

    Public Function UserUpdateTraveler(ByVal data As datos_viajero, Optional ByRef NewIdCliente As String = "") As Boolean
        Try
            Dim dtr As New XmlDataDocument(New ReqTraveler)
            Dim dstr As ReqTraveler = CType(dtr.DataSet, ReqTraveler)
            Dim rUser As ReqTraveler.UserRow
            rUser = dstr.User.AddUserRow
            dstr.Ticket.AddTicketRow(p_ticket, CapaPresentacion.Idiomas.cultura.ToUpper, rUser)
            Dim rTrav As ReqTraveler.TravelerRow
            rTrav = dstr.Traveler.NewTravelerRow
            With rTrav
                .FirstName = data.FName
                .LastName = data.LName
                .Email = data.Email
                .EmailType = data.emailtype
                .Phone = data.Phone
                .CellPhone = "" 'data.cellPhone
                .WorkPhone = "" ' data.workphone
                .PostalCode = data.postalcode
                .Address = data.address
                .City = data.city
                .State = data.state
                .Country = data.IdCountry
                .ClientId = data.idcliente
            End With
            dstr.Traveler.AddTravelerRow(rTrav)
            Dim x As XmlElement = Passport.CreateOrUpdateTraveler(dtr.DocumentElement)

            Dim ResTrvReader As New XmlTextReader(x.OuterXml, XmlNodeType.Element, Nothing)
            Dim rstrv As New ResTraveler
            rstrv.ReadXml(ResTrvReader)
            If rstrv.Ticket.Count > 0 AndAlso rstrv.Ticket(0).Status <> Me.SessionState.OnLine Then
                UserLogOut()
                _LastMessage = "El ticket no tiene status online"
            ElseIf rstrv._Error.Rows.Count > 0 Then
                _LastMessage = rstrv._Error(0).Message
                MessageBox.Show(_LastMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                'para recargar la informacion de los viajeros
                NewIdCliente = rstrv.Traveler(0).ClientId
                p_userTD = Nothing
                Return True
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0085", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
        Return False
    End Function

    '    Public Function UpdateTravelers(ByVal dtr As XmlDataDocument) As ResTraveler.TravelerDataTable
    '        Try
    '            Dim x As XmlElement = Passport.CreateOrUpdateTraveler(dtr.DocumentElement)
    '            Dim ResTrvReader As New XmlTextReader(x.OuterXml, XmlNodeType.Element, Nothing)
    '            Dim rstrv As New ResTraveler
    '            rstrv.ReadXml(ResTrvReader)
    '            If rstrv.Ticket.Count > 0 AndAlso rstrv.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                UserLogOut()
    '                _LastMessage = "El ticket no tiene status online"
    '            ElseIf rstrv._Error.Rows.Count > 0 Then
    '                _LastMessage = rstrv._Error(0).Message
    '                PortalServiceTracer.ServiceTracer("Passport: No se pudo actualizar la informaci�n del viajero :  " & _LastMessage, PortalServiceTacerErrorTypes.Warning)
    '            Else
    '                'para recargar la informacion de los viajeros
    '                Return rstrv.Traveler
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n UserUpdateTraveler idcliente:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function

    '    Public Function UserDeleteTraveler(ByVal idcliente As Integer) As Boolean
    '        Try
    '            Dim dtr As New XmlDataDocument(New reqDeleteTraveler)
    '            Dim dstr As reqDeleteTraveler = CType(dtr.DataSet, reqDeleteTraveler)
    '            Dim rUser As reqDeleteTraveler.UserRow
    '            rUser = dstr.User.AddUserRow
    '            dstr.Ticket.AddTicketRow(TicketId, "EN", rUser)
    '            Dim rTrav As reqDeleteTraveler.TravelerRow
    '            rTrav = dstr.Traveler.NewTravelerRow
    '            With rTrav
    '                .ClientId = idcliente
    '            End With
    '            dstr.Traveler.AddTravelerRow(rTrav)
    '            Dim x As XmlElement = Passport.DeleteTraveler(dtr.DocumentElement)
    '            Dim ResTrvReader As New XmlTextReader(x.OuterXml, XmlNodeType.Element, Nothing)
    '            Dim rstrv As New ResDeleteTraveler
    '            rstrv.ReadXml(ResTrvReader)
    '            If rstrv.Ticket.Count > 0 AndAlso rstrv.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                UserLogOut()
    '                _LastMessage = "El ticket no tiene status online"
    '            ElseIf rstrv._Error.Rows.Count > 0 Then
    '                _LastMessage = rstrv._Error(0).Message
    '            Else
    '                'para recargar la informacion de los viajeros

    '                Current.Session(TicketTravelersKey) = Nothing
    '                Return True
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n UserDeleteTraveler idcliente:  " & idcliente & ": " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return False
    '    End Function

    '    Public Function CreateCredential(ByVal IdCliente As Integer, ByVal Codigo As String, ByVal emailtype As Byte, ByVal email As String, ByRef pass As String) As Boolean
    '        Try

    '            Dim xdoc As New XmlDataDocument(New ReqAccessCredential)
    '            Dim dsCredential As ReqAccessCredential = CType(xdoc.DataSet, ReqAccessCredential)
    '            Dim drAC As ReqAccessCredential.AccessCredentialRow

    '            drAC = dsCredential.AccessCredential.AddAccessCredentialRow(Me.TicketId, IdCliente, Codigo, emailtype, email, "EN")

    '            Dim Resp As XmlElement = Passport.CreateUserCredential(xdoc.DocumentElement)
    '            If Not Resp Is Nothing Then
    '                Dim TicketReader As New XmlTextReader(Resp.OuterXml, XmlNodeType.Element, Nothing)
    '                Dim RsAC As New ResAccessCredential
    '                RsAC.ReadXml(TicketReader)
    '                If RsAC._Error.Count > 0 Then
    '                    Me._LastMessage = RsAC._Error(0).Message
    '                    Return False
    '                End If
    '                If RsAC.User.Rows.Count > 0 Then
    '                    pass = crypto.DecryptString128Bit(RsAC.User(0).AccessId, crypto.PublicKey)
    '                    'para recargar la informacion de los viajeros
    '                    Current.Session(TicketTravelersKey) = Nothing
    '                    Return True
    '                End If
    '                Return True
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n CreateCredential idcliente:  " & IdCliente & ": " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return False
    '    End Function

    '    Public Function FindTravelers(ByVal name As String) As ResTraveler.TravelerDataTable
    '        Try
    '            If Not UserInfo Is Nothing Then 'debe haber almenos la informaicon del usuario

    '                Dim data As New XmlDataDocument(New ReqSearchTraveler)
    '                Dim ds As ReqSearchTraveler = CType(data.DataSet, ReqSearchTraveler)
    '                Dim drU As ReqSearchTraveler.UserRow = ds.User.NewRow

    '                Dim drk As ReqSearchTraveler.TicketRow = ds.Ticket.NewTicketRow
    '                drk.TicketId = Current.Session(TicketKey)
    '                drk.Language = "EN"
    '                ds.Ticket.AddTicketRow(drk)

    '                Dim drt As ReqSearchTraveler.TravelerRow = ds.Traveler.NewTravelerRow
    '                drt.Status = 0
    '                drt.Nombre = name.Trim
    '                ds.Traveler.AddTravelerRow(drt)

    '                Dim text As New XmlTextReader(Passport.GetTravelersByName(data.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
    '                If Not text Is Nothing Then
    '                    Dim respuesta As New XmlDataDocument(New ResTraveler)
    '                    Dim resp As ResTraveler = CType(respuesta.DataSet, ResTraveler)
    '                    resp.ReadXml(text)
    '                    If resp.Ticket.Count > 0 AndAlso resp.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                        UserLogOut()
    '                        _LastMessage = "El ticket no tiene status online"
    '                        Return Nothing
    '                    ElseIf resp._Error.Count > 0 Then
    '                        _LastMessage = resp._Error(0).Message
    '                        Return Nothing
    '                    Else
    '                        Dim TravelerDt As ResTraveler
    '                        TravelerDt = New ResTraveler
    '                        TravelerDt.ReadXml(New XmlTextReader(resp.GetXml, XmlNodeType.Document, Nothing))
    '                        Return TravelerDt.Traveler
    '                    End If
    '                End If
    '            End If

    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n FindTravelers:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function
#End Region

#Region "Metodos P�blicos De manipulacion de las tarjetas de credito a traves del ws"
    '    'This functions add in memory a new Credit Card    'if you want to save you must invoke to userupdate
    '    Public Function UserUpdateCreditCard(ByVal CardType As Byte, ByVal FirstName As String, ByVal LastName As String, ByVal CardNumber As String, _
    '    ByVal ExpiresMonth As String, ByVal ExpiresYear As String) As Boolean
    '        '//------------------------------------------------------
    '        '//UPDATE CREDIT CARD
    '        '//------------------------------------------------------
    '        Try
    '            Dim dcc As New XmlDataDocument(New ReqCreditCard)
    '            Dim dscc As ReqCreditCard = CType(dcc.DataSet, ReqCreditCard)
    '            Dim r As ReqCreditCard.CreditCardsRow
    '            r = dscc.CreditCards.AddCreditCardsRow
    '            dscc.Ticket.AddTicketRow(TicketId, "EN", r)
    '            Dim rcc As ReqCreditCard.CreditCardRow
    '            rcc = dscc.CreditCard.NewCreditCardRow
    '            With rcc
    '                .CardType = CardType
    '                .FirstName = FirstName
    '                .LastName = LastName
    '                .CardNumber = CardNumber
    '                .ExpiresMonth = ExpiresMonth
    '                .ExpiresYear = ExpiresYear
    '            End With
    '            dscc.CreditCard.AddCreditCardRow(rcc)
    '            rcc.SetParentRow(r)
    '            Dim xRes As New XmlTextReader(Passport.CreateOrUpdateCreditCard(dcc.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
    '            Dim resCC As New ResCreditCard
    '            resCC.ReadXml(xRes)
    '            If resCC.Ticket.Count > 0 AndAlso resCC.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                UserLogOut()
    '                _LastMessage = "El ticket no tiene status online"
    '            ElseIf resCC._Error.Count > 0 Then
    '                _LastMessage = resCC._Error(0).Message
    '            Else
    '                'limpiar la sessi�n para que vuelva a cargarlas
    '                Current.Session(TicketCreditKey) = Nothing
    '                Return True
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n UserUpdateCreditCard: " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return False
    '        '//END UPDATE CREDITCARD
    '    End Function

    '    Public Function UserDeleteCreditCard(ByVal CredCard As String) As Boolean
    '        '' //DELETE CREDIT CARD
    '        Try
    '            Dim docDelcc As New XmlDataDocument(New ReqDeleteCreditCard)
    '            Dim dsDelCC As ReqDeleteCreditCard = CType(docDelcc.DataSet, ReqDeleteCreditCard)
    '            Dim delccU As ReqDeleteCreditCard.UserRow
    '            delccU = dsDelCC.User.AddUserRow
    '            '//ticket
    '            dsDelCC.Ticket.AddTicketRow(TicketId, "EN", delccU)
    '            '//creditcard
    '            dsDelCC.CreditCard.AddCreditCardRow(CredCard, delccU)
    '            Dim x As XmlElement = Passport.DeleteCreditCard(docDelcc.DocumentElement)
    '            Dim xmlRes As New XmlTextReader(x.OuterXml, XmlNodeType.Element, Nothing)
    '            Dim ResDelCC As New ResDeleteCreditCard
    '            ResDelCC.ReadXml(xmlRes)
    '            If ResDelCC.Ticket.Count > 0 AndAlso ResDelCC.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                UserLogOut()
    '                _LastMessage = "El ticket no tiene status online"
    '            ElseIf ResDelCC._Error.Count > 0 Then
    '                _LastMessage = ResDelCC._Error(0).Message
    '            Else
    '                'si la elimin� hay que limpiar la sessi�n para que vuelva a cargarlas
    '                Current.Session(TicketCreditKey) = Nothing
    '                Return ResDelCC.CreditCard(0).Status
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n UserDeleteCreditCard: " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return False
    '    End Function

    '    Public Function getUserCreditCards() As ResCreditCard.CreditCardDataTable
    '        Try
    '            If Not UserInfo Is Nothing Then
    '                'que exista la informacion y ademas las llaves iguales.
    '                If UserCreditCardInfo Is Nothing OrElse UserInfo.Ticket(0).TicketId <> UserCreditCardInfo.Ticket(0).TicketId Then
    '                    'retrieve data.
    '                    Dim data As New XmlDataDocument(New ReqTicket)
    '                    Dim ds As ReqTicket = CType(data.DataSet, ReqTicket)
    '                    Dim dr As ReqTicket.TicketRow
    '                    dr = ds.Ticket.NewTicketRow
    '                    dr.TicketId = UserInfo.Ticket(0).TicketId
    '                    dr.Language = "EN"
    '                    ds.Ticket.AddTicketRow(dr)
    '                    Dim text As New XmlTextReader(Passport.GetCreditCards(data.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
    '                    If Not text Is Nothing Then
    '                        Dim respuesta As New XmlDataDocument(New ResCreditCard)
    '                        Dim resp As ResCreditCard = CType(respuesta.DataSet, ResCreditCard)
    '                        resp.ReadXml(text)
    '                        If resp.Ticket.Count > 0 AndAlso resp.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                            UserLogOut()
    '                            _LastMessage = "El ticket no tiene status online"
    '                            Return Nothing
    '                        ElseIf resp._Error.Count > 0 Then
    '                            _LastMessage = resp._Error(0).Message
    '                            Return Nothing
    '                        Else
    '                            _UserCreditCardData = New ResCreditCard
    '                            _UserCreditCardData.ReadXml(New XmlTextReader(resp.GetXml, XmlNodeType.Document, Nothing))
    '                            Current.Session(TicketCreditKey) = _UserCreditCardData
    '                        End If
    '                    End If
    '                End If
    '                Return UserCreditCardInfo.CreditCard
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n getUserCreditCards: " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function
#End Region

#Region "Metodos  P�blicos De manipulacion de la cuenta a traves del ws"
    '    'This retrieves a forgotten password
    '    Public Function UserRetrivePassword(ByVal User As String) As String
    '        Try
    '            Dim XMForPass As New XmlDataDocument(New ReqForgotPassword)
    '            Dim dsfg As ReqForgotPassword = CType(XMForPass.DataSet, ReqForgotPassword)
    '            dsfg.User.AddUserRow(User, "ES")
    '            Dim XmlForPass As XmlElement = Passport.ForgotPassword(XMForPass.DocumentElement)
    '            If Not XmlForPass Is Nothing Then
    '                Dim TicketReader As New XmlTextReader(XmlForPass.OuterXml, XmlNodeType.Element, Nothing)
    '                Dim respTrav As New ResForgotPassword
    '                respTrav.ReadXml(TicketReader)
    '                If respTrav._Error.Count > 0 Or respTrav.UserAccount.Rows.Count = 0 Then
    '                    _LastMessage = respTrav._Error(0).Message
    '                    Return ""
    '                Else
    '                    Return crypto.DecryptString128Bit(respTrav.UserAccount(0).Password, crypto.PublicKey)

    '                End If
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n UserRetrivePassword usuario: " & User & ": " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return ""
    '    End Function

    '    'Estas funciones seran solo si el cliente autoriza el registro en los portales. 
    '    'o si ya esta registrado.. 
    '    '*****************************************************************************

    '    'Al crear la cuenta automaticamente obtiene el ticket para la clase passport
    '    Public Function UserCreateAccount(ByVal UserData As ReqCreateAccount) As Boolean
    '        Try
    '            Dim doc_createAcc As New XmlDataDocument
    '            doc_createAcc.LoadXml(UserData.GetXml)
    '            Dim xmlACresp As XmlElement = Passport.CreateAccount(doc_createAcc.DocumentElement)
    '            If Not xmlACresp Is Nothing Then
    '                Dim TicketReader As New XmlTextReader(xmlACresp.OuterXml, XmlNodeType.Element, Nothing)
    '                Dim respTK As New ResCreateAccount
    '                respTK.ReadXml(TicketReader)
    '                _UserData = New ResUserLogin
    '                _UserData.Merge(respTK)
    '                'respTK.Merge(_UserData)
    '                If _UserData._Error.Count = 0 Then
    '                    Current.Session(TicketKey) = _UserData.Ticket(0).TicketId
    '                    Current.Session(TicketUserKey) = _UserData

    '                    Return True
    '                Else
    '                    _LastMessage = _UserData._Error(0).Message
    '                    Return False
    '                End If
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n UserCreateAccount request: " & UserData.GetXml & ": " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '            Return False
    '        End Try
    '        Return True
    '    End Function

    Public Function UserChangePassword(ByVal oldPass As String, ByVal NewPass As String) As Boolean
        Try
            Dim dtr As New XmlDataDocument(New ReqChangePassword)
            Dim dstr As ReqChangePassword = CType(dtr.DataSet, ReqChangePassword)
            Dim rPsw As ReqChangePassword.ChangePasswordRow
            rPsw = dstr.ChangePassword.NewChangePasswordRow
            With rPsw
                .TicketId = p_ticket
                .OldPassword = oldPass
                .NewPassword = NewPass
                .Language = "EN"
            End With
            dstr.ChangePassword.AddChangePasswordRow(rPsw)
            Dim x As XmlElement = Passport.ChangePassword(dtr.DocumentElement)
            Dim rspsw As New ResChangePassword
            Dim ResPswReader As New XmlTextReader(x.OuterXml, XmlNodeType.Element, Nothing)
            rspsw.ReadXml(ResPswReader)
            If rspsw.Traveler.Rows.Count > 0 Then
                Return rspsw.Traveler(0).PasswordChanged
            ElseIf rspsw.Ticket.Count > 0 AndAlso rspsw.Ticket(0).Status <> Me.SessionState.OnLine Then
                'UserLogOut()
                UserLogin(dataOperador.user, dataOperador.pass)
                _LastMessage = "Conexion recuperada, intente nuevamente" '"El ticket no tiene status online"
            ElseIf rspsw._Error.Rows.Count > 0 Then
                _LastMessage = rspsw._Error(0).Message
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0086", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try

        Return False
    End Function

    Public Function UserChangeEmail(ByVal email As String, ByVal emailtype As Boolean) As Boolean
        Try
            Dim xdocchange As New XmlDataDocument(New ReqChangeEmailContact)
            Dim dschangEmail As ReqChangeEmailContact = CType(xdocchange.DataSet, ReqChangeEmailContact)
            Dim drch As ReqChangeEmailContact.EmailContactRow
            drch = dschangEmail.EmailContact.AddEmailContactRow(p_ticket, email, emailtype, "EN")
            Dim xmlChange As XmlElement = Passport.ChangeEmailContact(xdocchange.DocumentElement)
            If Not xmlChange Is Nothing Then
                Dim TicketReader As New XmlTextReader(xmlChange.OuterXml, XmlNodeType.Element, Nothing)
                Dim dsResChange As New ResChangeEmailContact
                dsResChange.ReadXml(TicketReader)
                If dsResChange.User.Rows.Count > 0 AndAlso dsResChange.User(0).EmailChanged Then
                    If dsResChange.User(0).EmailChanged = True Then
                        Me.UserInfo.UserAccount(0).Email = email
                        Me.UserInfo.UserAccount(0).EmailType = emailtype
                    End If
                    Return True
                ElseIf dsResChange.Ticket.Count > 0 AndAlso dsResChange.Ticket(0).Status <> Me.SessionState.OnLine Then
                    'UserLogOut()
                    UserLogin(dataOperador.user, dataOperador.pass)
                    _LastMessage = "Conexion recuperada nuevamente" '"El ticket no tiene status online"
                ElseIf dsResChange._Error.Rows.Count > 0 Then
                    Me._LastMessage = dsResChange._Error(0).Message
                End If
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0087", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
        Return False
    End Function

    '    Public Function getMainContact() As ResTraveler.TravelerDataTable
    '        Try
    '            If Not UserInfo Is Nothing Then 'debe haber almenos la informaicon del usuario
    '                'que exista la informacion y ademas las llaves iguales.
    '                If MainContact Is Nothing Then
    '                    'retrieve data.
    '                    Dim data As New XmlDataDocument(New ReqTicket)
    '                    Dim ds As ReqTicket = CType(data.DataSet, ReqTicket)
    '                    Dim dr As ReqTicket.TicketRow
    '                    dr = ds.Ticket.NewTicketRow
    '                    dr.TicketId = UserInfo.Ticket(0).TicketId
    '                    dr.Language = "EN"
    '                    ds.Ticket.AddTicketRow(dr)
    '                    Dim text As New XmlTextReader(Passport.GetMainTraveler(data.DocumentElement).OuterXml, XmlNodeType.Element, Nothing)
    '                    If Not text Is Nothing Then
    '                        Dim respuesta As New XmlDataDocument(New ResTraveler)
    '                        Dim resp As ResTraveler = CType(respuesta.DataSet, ResTraveler)
    '                        resp.ReadXml(text)
    '                        If resp.Ticket.Count > 0 AndAlso resp.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                            UserLogOut()
    '                            _LastMessage = "El ticket no tiene status online"
    '                            Return Nothing
    '                        ElseIf resp._Error.Count > 0 Then
    '                            _LastMessage = resp._Error(0).Message
    '                            Return Nothing
    '                        Else
    '                            _MainContact = New ResTraveler
    '                            _MainContact.ReadXml(New XmlTextReader(resp.GetXml, XmlNodeType.Document, Nothing))
    '                            Current.Session(TicketMaincontactKey) = _MainContact
    '                        End If
    '                    End If
    '                End If
    '                Return MainContact.Traveler
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error en la funci�n getMainContact:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function
#End Region

#Region "M�todos P�blicos de manipulaci�n de Grupos"
    '    Public Function GetGroupInfo() As ResGroupInfo.GroupRow
    '        Try
    '            If Not UserInfo Is Nothing Then
    '                'que exista la informacion y ademas las llaves iguales.
    '                If UserGroupInfo Is Nothing OrElse UserInfo.Ticket(0).TicketId <> UserGroupInfo.Ticket(0).TicketId Then
    '                    Dim doc_ReqTicket As New XmlDataDocument(New ReqTicket)
    '                    Dim dsReqTicket As ReqTicket = CType(doc_ReqTicket.DataSet, ReqTicket)
    '                    Dim drt As ResUserLogin.TicketRow
    '                    Dim drk As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow()
    '                    drk.TicketId = Current.Session(TicketKey)
    '                    drk.Language = "EN"
    '                    dsReqTicket.Ticket.AddTicketRow(drk)
    '                    Dim xmlResponse As XmlElement = Passport.GetGroupInfo(doc_ReqTicket.DocumentElement)
    '                    If Not xmlResponse Is Nothing Then
    '                        Dim groupreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
    '                        Dim respgr As New ResGroupInfo
    '                        respgr.ReadXml(groupreader)
    '                        If respgr.Group.Count > 0 Then
    '                            _UserGroupInfo = New ResGroupInfo
    '                            _UserGroupInfo.ReadXml(New XmlTextReader(respgr.GetXml, XmlNodeType.Document, Nothing))
    '                            Current.Session(TicketGroupKey) = _UserGroupInfo
    '                        ElseIf respgr.Ticket.Count > 0 AndAlso respgr.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                            UserLogOut()
    '                            _LastMessage = "El ticket no tiene status online"
    '                        ElseIf respgr._Error.Count > 0 Then
    '                            _LastMessage = respgr._Error(0).Message
    '                        End If
    '                    End If
    '                End If
    '                If UserGroupInfo.Group.Count > 0 Then
    '                    Return UserGroupInfo.Group(0)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error al llamar la funci�n GetGroupInfo:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function

    '    Public Function GetMembersGroup() As ResGroupInfo.MemberDataTable
    '        Try
    '            If Not UserInfo Is Nothing Then
    '                'que exista la informacion y ademas las llaves iguales.
    '                If UserGroupInfo Is Nothing OrElse UserInfo.Ticket(0).TicketId <> UserGroupInfo.Ticket(0).TicketId Then
    '                    Dim doc_ReqTicket As New XmlDataDocument(New ReqTicket)
    '                    Dim dsReqTicket As ReqTicket = CType(doc_ReqTicket.DataSet, ReqTicket)
    '                    Dim drt As ResUserLogin.TicketRow
    '                    Dim drk As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow()
    '                    drk.TicketId = Current.Session(TicketKey)
    '                    drk.Language = "EN"
    '                    dsReqTicket.Ticket.AddTicketRow(drk)
    '                    Dim xmlResponse As XmlElement = Passport.GetGroupInfo(doc_ReqTicket.DocumentElement)
    '                    If Not xmlResponse Is Nothing Then
    '                        Dim groupreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
    '                        Dim respgr As New ResGroupInfo
    '                        respgr.ReadXml(groupreader)
    '                        If respgr.Group.Count > 0 Then
    '                            _UserGroupInfo = New ResGroupInfo
    '                            _UserGroupInfo.ReadXml(New XmlTextReader(respgr.GetXml, XmlNodeType.Document, Nothing))
    '                            Current.Session(TicketGroupKey) = _UserGroupInfo
    '                            Return UserGroupInfo.Member
    '                        ElseIf respgr.Ticket.Count > 0 AndAlso respgr.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                            UserLogOut()
    '                            _LastMessage = "El ticket no tiene status online"
    '                            Return Nothing
    '                        ElseIf respgr._Error.Count > 0 Then
    '                            _LastMessage = respgr._Error(0).Message
    '                            Return Nothing
    '                        End If
    '                    End If
    '                End If
    '                Return UserGroupInfo.Member
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error al llamar la funci�n GetMembersGroup:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function
#End Region

#Region "M�todos P�blicos de manipulaci�n de Reservaciones"
    Public Function GetReservation(ByVal StartDate As Date, ByVal EndDate As Date, ByVal rubro As enumRubrosPassport, ByVal tipo As FilterReservationDates) As ResReservations
        Try
            If Not UserInfo Is Nothing Then
                'que exista la informacion y ademas las llaves iguales.

                Dim doc_ReqRes As New XmlDataDocument(New ReqReservations)
                Dim dsReqRes As ReqReservations = CType(doc_ReqRes.DataSet, ReqReservations)
                Dim drt As ResUserLogin.TicketRow
                Dim drk As ReqReservations.TicketRow = dsReqRes.Ticket.NewTicketRow
                drk.TicketId = p_ticket 'Current.Session(TicketKey)
                drk.Language = "EN"
                dsReqRes.Ticket.AddTicketRow(drk)
                Dim drR As ReqReservations.ReservationRow = dsReqRes.Reservation.NewReservationRow
                drR.StartDate = StartDate.ToString("yyyy/MM/dd")
                drR.EndDate = EndDate.ToString("yyyy/MM/dd")
                drR.Rubro = -1
                If rubro <> enumRubrosPassport.Portal Then
                    drR.Rubro = rubro
                End If
                'Dim cfg As New PortalPartnersCfg
                'If cfg.CorporateName <> "" Then
                '    drR.Corporativo = cfg.CorporateName
                'End If
                If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then
                    drR.Corporativo = ws_login.getUserAccessInfo.idCorporativo
                End If
                drR.Type = tipo

                dsReqRes.Reservation.AddReservationRow(drR)
                Dim xmlResponse As XmlElement = Passport.GetReservationData(doc_ReqRes.DocumentElement)
                If Not xmlResponse Is Nothing Then
                    Dim Reservationreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
                    Dim respres As New ResReservations
                    respres.ReadXml(Reservationreader)
                    If respres._Error.Count = 0 Then
                        Return respres
                    ElseIf respres.Ticket.Count > 0 AndAlso respres.Ticket(0).Status <> Me.SessionState.OnLine Then
                        UserLogOut()
                        _LastMessage = "El ticket no tiene status online"
                    Else
                        _LastMessage = respres._Error(0).Message
                    End If
                End If
            End If
        Catch ex As Exception
            'PortalServiceTracer.ServiceTracer("Passport: No se pudo obtener el listado de reservaciones: " & ex.Message, PortalServiceTacerErrorTypes.Warning)
        End Try
        Return Nothing
    End Function

    '    Public Function GetGroupReservation(ByVal StartDate As Date, ByVal EndDate As Date, ByVal rubro As Rubros, ByVal tipo As FilterReservationDates, ByVal idclient As Integer) As ResReservations

    '        Try
    '            If Not UserInfo Is Nothing Then
    '                Dim doc_ReqRes As New XmlDataDocument(New ReqReservations)
    '                Dim dsReqRes As ReqReservations = CType(doc_ReqRes.DataSet, ReqReservations)
    '                Dim drt As ResUserLogin.TicketRow
    '                Dim drk As ReqReservations.TicketRow = dsReqRes.Ticket.NewTicketRow
    '                drk.TicketId = Current.Session(TicketKey)
    '                drk.Language = "EN"
    '                dsReqRes.Ticket.AddTicketRow(drk)
    '                Dim drR As ReqReservations.ReservationRow = dsReqRes.Reservation.NewReservationRow
    '                drR.StartDate = StartDate.ToString("yyyy/MM/dd")
    '                drR.EndDate = EndDate.ToString("yyyy/MM/dd")
    '                drR.Rubro = -1
    '                If rubro <> Rubros.Portal Then
    '                    drR.Rubro = rubro
    '                End If
    '                Dim cfg As New PortalPartnersCfg
    '                If cfg.CorporateName <> "" Then
    '                    drR.Corporativo = cfg.CorporateName
    '                End If
    '                drR.Type = tipo
    '                dsReqRes.Reservation.AddReservationRow(drR)

    '                Dim drM As ReqReservations.MemberRow = dsReqRes.Member.NewMemberRow
    '                drM.ClientId = idclient
    '                dsReqRes.Member.AddMemberRow(drM)

    '                Dim xmlResponse As XmlElement = Passport.GetReservationData(doc_ReqRes.DocumentElement)
    '                If Not xmlResponse Is Nothing Then
    '                    Dim Reservationreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
    '                    Dim respres As New ResReservations
    '                    respres.ReadXml(Reservationreader)
    '                    If respres._Error.Count = 0 Then
    '                        Return respres
    '                    ElseIf respres.Ticket.Count > 0 AndAlso respres.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                        UserLogOut()
    '                        _LastMessage = "El ticket no tiene status online"
    '                    ElseIf respres._Error.Count > 0 Then
    '                        _LastMessage = respres._Error(0).Message
    '                    End If
    '                End If
    '            End If
    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: No se pudo obtener la informaci�n de grupo cliente:" & idclient & " " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function

    Public Function GetAgencyReservation(ByVal StartDate As Date, ByVal EndDate As Date, ByVal rubro As enumRubrosPassport, ByVal tipo As FilterReservationDates, ByVal idAgency As Integer) As ResReservations
        Try
            If Not UserInfo Is Nothing Then
                Dim doc_ReqRes As New XmlDataDocument(New ReqAgencyReservations)
                Dim dsReqRes As ReqAgencyReservations = CType(doc_ReqRes.DataSet, ReqAgencyReservations)
                'Dim drt As ResUserLogin.TicketRow
                Dim drk As ReqAgencyReservations.TicketRow = dsReqRes.Ticket.NewTicketRow
                drk.TicketId = p_ticket
                drk.Language = "EN"
                dsReqRes.Ticket.AddTicketRow(drk)
                Dim drR As ReqAgencyReservations.ReservationRow = dsReqRes.Reservation.NewReservationRow
                drR.StartDate = StartDate.ToString("yyyy/MM/dd")
                drR.EndDate = EndDate.ToString("yyyy/MM/dd")
                drR.Rubro = -1
                If rubro <> enumRubrosPassport.Portal Then
                    drR.Rubro = rubro
                End If
                'Dim cfg As New PortalPartnersCfg
                'If cfg.CorporateName <> "" Then
                '    drR.Corporativo = cfg.CorporateName
                'End If
                If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then
                    drR.Corporativo = ws_login.getUserAccessInfo.idCorporativo
                End If
                drR.Type = tipo
                drR.AgencyId = idAgency
                dsReqRes.Reservation.AddReservationRow(drR)

                Dim xmlResponse As XmlElement = Passport.GetAgencyReservationData(doc_ReqRes.DocumentElement)
                If Not xmlResponse Is Nothing Then
                    Dim Reservationreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
                    Dim respres As New ResReservations
                    respres.ReadXml(Reservationreader)
                    If respres._Error.Count = 0 Then
                        Return respres
                    ElseIf respres.Ticket.Count > 0 AndAlso respres.Ticket(0).Status <> Me.SessionState.OnLine Then
                        UserLogOut()
                        _LastMessage = "El ticket no tiene status online"
                    ElseIf respres._Error.Count > 0 Then
                        _LastMessage = respres._Error(0).Message
                    End If
                End If
            End If
        Catch ex As Exception
            'PortalServiceTracer.ServiceTracer("Passport: No se pudo obtener los viajes del agente: " & idAgency & " " & ex.Message, PortalServiceTacerErrorTypes.Warning)
        End Try
        Return Nothing
    End Function
#End Region

#Region "M�todos p�blicos de manipulaci�n de Agencias de viajes"
    Public Function GetAgencies() As ResAgencies.AgencyDataTable
        Try
            If UserInfo Is Nothing Then
                UserLogin(dataOperador.user, dataOperador.pass)
            Else
                If Me.KeepAlive <> SessionState.OnLine Then
                    UserLogin(dataOperador.user, dataOperador.pass)
                End If
            End If
            If Not UserInfo Is Nothing Then
                'que exista la informacion y ademas las llaves iguales.
                If _AgenciesInfo Is Nothing Then
                    Dim doc_ReqTicket As New XmlDataDocument(New ReqTicket)
                    Dim dsReqTicket As ReqTicket = CType(doc_ReqTicket.DataSet, ReqTicket)
                    Dim drt As ReqTicket.TicketRow
                    Dim drk As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow()
                    drk.TicketId = p_ticket 'Current.Session(TicketKey)
                    drk.Language = "EN"
                    dsReqTicket.Ticket.AddTicketRow(drk)
                    Dim xmlResponse As XmlElement = Passport.GetAgenciesByTicket(doc_ReqTicket.DocumentElement)
                    If Not xmlResponse Is Nothing Then
                        Dim Agencyreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
                        Dim resAgencies As New ResAgencies
                        resAgencies.ReadXml(Agencyreader)
                        If resAgencies.Agency.Count > 0 Then
                            _AgenciesInfo = New ResAgencies
                            _AgenciesInfo.ReadXml(New XmlTextReader(resAgencies.GetXml, XmlNodeType.Document, Nothing))
                            'p_agenciesI = _AgenciesInfo 'Current.Session(TicketAgencyKey) = _AgenciesInfo
                        ElseIf resAgencies.Ticket.Count > 0 AndAlso resAgencies.Ticket(0).Status <> Me.SessionState.OnLine Then
                            UserLogOut()
                            _LastMessage = "El ticket no tiene status online"
                        ElseIf resAgencies._Error.Count > 0 Then
                            _LastMessage = resAgencies._Error(0).Message
                        End If
                    End If
                End If
                If _AgenciesInfo IsNot Nothing AndAlso _AgenciesInfo.Agency.Count > 0 Then
                    Return _AgenciesInfo.Agency
                Else
                    Return Nothing
                End If
            End If
        Catch ex As Exception
            'MessageBox.Show("ERROR!!!!!!")
        End Try
        Return Nothing
    End Function

    '    Public Function GetAgenciesByTicket(ByVal Tk As String) As ResAgencies.AgencyDataTable
    '        Try
    '            'que exista la informacion y ademas las llaves iguales.
    '            If AgenciesInfo Is Nothing Then
    '                Dim doc_ReqTicket As New XmlDataDocument(New ReqTicket)
    '                Dim dsReqTicket As ReqTicket = CType(doc_ReqTicket.DataSet, ReqTicket)
    '                Dim drt As ReqTicket.TicketRow
    '                Dim drk As ReqTicket.TicketRow = dsReqTicket.Ticket.NewTicketRow()
    '                drk.TicketId = Tk
    '                drk.Language = "EN"
    '                dsReqTicket.Ticket.AddTicketRow(drk)
    '                Dim xmlResponse As XmlElement = Passport.GetAgenciesByTicket(doc_ReqTicket.DocumentElement)
    '                If Not xmlResponse Is Nothing Then
    '                    Dim Agencyreader As New XmlTextReader(xmlResponse.OuterXml, XmlNodeType.Element, Nothing)
    '                    Dim resAgencies As New ResAgencies
    '                    resAgencies.ReadXml(Agencyreader)
    '                    If resAgencies.Agency.Count > 0 Then
    '                        _AgenciesInfo = New ResAgencies
    '                        _AgenciesInfo.ReadXml(New XmlTextReader(resAgencies.GetXml, XmlNodeType.Document, Nothing))
    '                        Current.Session(TicketAgencyKey) = _AgenciesInfo
    '                    ElseIf resAgencies.Ticket.Count > 0 AndAlso resAgencies.Ticket(0).Status <> Me.SessionState.OnLine Then
    '                        UserLogOut()
    '                        _LastMessage = "El ticket no tiene status online"
    '                    ElseIf resAgencies._Error.Count > 0 Then
    '                        _LastMessage = resAgencies._Error(0).Message
    '                    End If
    '                End If
    '            End If
    '            If AgenciesInfo.Agency.Count > 0 Then
    '                Return AgenciesInfo.Agency
    '            End If

    '        Catch ex As Exception
    '            PortalServiceTracer.ServiceTracer("Passport: error al llamar la funci�n GetAgenciesByTicket:  " & ex.Message, PortalServiceTacerErrorTypes.Warning)
    '        End Try
    '        Return Nothing
    '    End Function

    '    Public Function SelectAgency(ByVal id As Integer)
    '        If Not AgenciesInfo Is Nothing Then
    '            For i As Integer = 0 To AgenciesInfo.Agency.Rows.Count - 1
    '                If AgenciesInfo.Agency(i).AgencyId = id Then
    '                    Current.Session("USRAGENCYLOGG") = id
    '                    Exit For
    '                End If
    '            Next
    '        End If
    '    End Function
#End Region

End Class