Imports System.Data.SqlClient

Public Class CapaAccesoDatos

    Public Class DB

        Public Shared Function try_conn(ByVal db As String) As Boolean
            Try
                Dim conn As New SqlConnection()
                conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL.ToString.ToUpper.Replace(CONST_DB_NAME.ToUpper, db) '"Data Source=" + server + ";database=master;Integrated Security=SSPI;Persist Security Info=False;"
               

                Dim cmd As New SqlCommand
                cmd.CommandType = CommandType.Text
                cmd.CommandText = "SELECT getdate()"
                cmd.Connection = conn

                Dim result As Object
                Dim previousConnectionState As ConnectionState = conn.State
                Try
                    If conn.State = ConnectionState.Closed Then
                        conn.Open()
                    End If
                    result = cmd.ExecuteScalar()
                Finally
                    If previousConnectionState = ConnectionState.Closed Then
                        conn.Close()
                    End If
                End Try
                CapaLogicaNegocios.ErrorManager.Manage("DEBUG", "Conexion OK", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                Return True
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0093*", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                Return False
            End Try
        End Function

        Public Shared Function exe_db_nonQ(ByVal db As String, ByVal q As String, ByVal desdeFile As Boolean) As Boolean
            Dim conn As New SqlConnection()

            Try
                conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL.ToUpper.Replace(CONST_DB_NAME.ToUpper, db)

                If desdeFile Then
                    Dim sr As System.IO.StreamReader

                    If System.IO.File.Exists(q) Then
                        sr = System.IO.File.OpenText(q)
                    Else
                        sr = System.IO.File.OpenText("..\..\" + q)
                    End If
                    q = sr.ReadToEnd
                    q = q.Replace("GO", " ")
                    sr.Close()
                End If

                Dim cmd As New SqlCommand
                cmd.CommandType = CommandType.Text
                cmd.CommandText = q
                cmd.Connection = conn

                Dim rowCount As Integer
                Dim previousConnectionState As ConnectionState
                previousConnectionState = conn.State

                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                rowCount = cmd.ExecuteNonQuery()

                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If

                Return True
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                conn.Close()
                Return False
            End Try
        End Function

        Public Shared Function exe_db_scal(ByVal db As String, ByVal q As String) As Object
            Try
                Dim conn As New SqlConnection()
                conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL.Replace(CONST_DB_NAME.ToUpper, db) 'str_conn(db)

                Dim cmd As New SqlCommand
                cmd.CommandType = CommandType.Text
                cmd.CommandText = q
                cmd.Connection = conn

                Dim result As Object
                Dim previousConnectionState As ConnectionState = conn.State

                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                result = cmd.ExecuteScalar()

                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If

                Return result
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0034", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                Return Nothing
            End Try
        End Function

        Public Shared Function exe_db_fill(ByVal db As String, ByVal q As String) As DataSet
            Dim conn As New SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL.Replace(CONST_DB_NAME.ToUpper, db) 'tools_db.str_conn(db)

            Dim cmd As New SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = q

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)

            Return ds
        End Function

        Public Shared Function getSP_toDS(ByVal cs As String, ByVal sp_name As String, ByVal params() As SqlParameter) As DataSet
            Try
                Dim conn As New SqlConnection()
                conn.ConnectionString = cs '"server=192.168.61.99;User id=UserProg;password=Ozpr0grammer;database=Ozhoteles"

                Dim cmd As New SqlCommand
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = sp_name
                For Each par As SqlParameter In params
                    cmd.Parameters.Add(par)
                Next
                cmd.Connection = conn

                Dim adapter As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                adapter.Fill(ds)

                Return ds
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

    End Class

    Public Class XML

        ''' <summary>
        ''' Propiedad usada para obtener la ruta en la que se almacenan los archivos XML de configuración local_data.xml y ws_data.xml.
        ''' Regresa la ruta del directorio uv_callcenter ubicado dentro del directorio “Documentos” del sistema.
        ''' <example>Ejemplo: C:\Users\Administrador\Documents\uv_callcenter</example>
        ''' </summary>
        Public Shared ReadOnly Property DataDocumentsPath() As String
            Get
                Dim ruta As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                If ruta Is Nothing Then ruta = ""
                ruta += "\uv_callcenter"

                If Not IO.Directory.Exists(ruta) Then IO.Directory.CreateDirectory(ruta)

                Return ruta
            End Get
        End Property

        ''' <summary>
        ''' Regresa la ruta del directorio data ubicado dentro del directorio de la Aplicación.
        ''' <example>Ejemplo: </example>
        ''' </summary>
        Public Shared ReadOnly Property DataAplicationPath() As String
            Get
                Return My.Application.Info.DirectoryPath + "\data"
            End Get
        End Property

        Public Enum DataType
            FilePath
            XMLString
        End Enum

        Public Shared Function GetDoc(ByVal data As String, ByVal type As DataType) As System.Xml.XmlDocument
            Dim doc As New System.Xml.XmlDocument

            Select Case type
                Case DataType.FilePath
                    doc.Load(data)
                Case DataType.XMLString
                    doc.LoadXml(data)
            End Select

            Return doc
        End Function

        Public Class localData

            Public Shared ReadOnly Property XMLfile_name() As String
                Get
                    Return DataDocumentsPath + "\local_data.xml"
                End Get
            End Property

            Public Shared ReadOnly Property Imagen_name() As String
                Get
                    Return DataDocumentsPath + "\Imagen.png"
                End Get
            End Property

            Private Shared Function doc() As System.Xml.XmlDocument
                Dim xmlD As New System.Xml.XmlDocument
                Dim def_xml As String = ""
                def_xml += "<datosWS>"
                'def_xml += "    <info name='servidorSQL'>Data Source=localhost\SQLEXPRESS;database=ventanaCallCenter;Integrated Security=SSPI;Persist Security Info=False;</info>"
                def_xml += "    <info name='servidorSQL'>Data Source=.\SQLExpress; Integrated Security=true; attachdbfilename=|DataDirectory|data\Callcenter.mdf; user instance=true;</info>"
                'Data Source=localhost\SQLExpress; integrated security=true; attachdbfilename=C:\USERS\USUARIO\DOCUMENTS\PRUEBABD\ventanaCallCenter.MDF; user instance=true;
                def_xml += "    <info name='moneda'>" + moneda_selected + "</info>"
                def_xml += "    <info name='NombreSistema'>Univisit Call Center</info>"
                def_xml += "    <info name='Imagen'></info>"
                def_xml += "    <info name='idioma'>" + CapaPresentacion.Idiomas.cultura + "</info>"
                def_xml += "    <info name='email'></info>"
                def_xml += "    <info name='ProfessionalsSQL_SERVER'>https://crs.univisit.com/rewards/wsRewards_1_0.asmx</info>"
                def_xml += "    <info name='ProfessionalsSQL_USER'>UvRewards</info>"
                def_xml += "    <info name='ProfessionalsSQL_PASSWORD'>0000</info>"
                def_xml += "    <info name='ProfessionalsSQL_DB'></info>"
                def_xml += "</datosWS>"

                If System.IO.File.Exists(XMLfile_name) Then
                    Try
                        xmlD.Load(XMLfile_name)

                        'Dim version As String = Nothing
                        'For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                        '    If ws_nodo.Attributes("name").Value = "version" Then
                        '        version = ws_nodo.InnerText
                        '    End If
                        'Next

                        'If version = Nothing OrElse Not version = My.Application.Info.Version.ToString Then
                        '    System.IO.File.Delete(XMLfile_name)
                        '    xmlD.LoadXml(def_xml)
                        '    xmlD.Save(XMLfile_name)
                        'End If
                        
                    Catch ex As Exception
                        xmlD.LoadXml(def_xml)
                    End Try
                Else
                    xmlD.LoadXml(def_xml)
                    xmlD.Save(XMLfile_name)
                End If

                Return xmlD
            End Function

            Private Shared Function get_nodo(ByVal name As String) As System.Xml.XmlNode
                Try
                    Dim xmlD As System.Xml.XmlDocument = doc()

                    For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                        If ws_nodo.Attributes("name").Value = name Then
                            Return ws_nodo
                        End If
                    Next

                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0035", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End Try

                Return Nothing
            End Function

            Private Shared Sub set_nodo(ByVal NodoName As String, ByVal NodoValue As String)
                Dim xmlD As System.Xml.XmlDocument = doc()
                Dim insertar As Boolean = True
                For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                    If ws_nodo.Attributes("name").Value = NodoName Then
                        ws_nodo.InnerText = NodoValue
                        insertar = False
                        Exit For
                    End If
                Next
                If insertar Then
                    'insertar
                    Dim nodo As System.Xml.XmlNode = xmlD.CreateElement("info")
                    Dim _name As System.Xml.XmlAttribute = xmlD.CreateAttribute("name")
                    _name.Value = NodoName
                    nodo.Attributes.Append(_name)
                    nodo.InnerText = NodoValue
                    xmlD.DocumentElement.AppendChild(nodo)
                End If
                xmlD.Save(XMLfile_name)
            End Sub

            'esta cadena debe traer en teoria el nombre de la base de datos que se especifico en el module1 - CONST_DB_NAME
            'pero es responsabilidad del usuario (en el modulo de Configurar Conexion) poner realmente esta bd
            ' aun asi, se le pone un Label indicando el nombre de la bd que debe usar
            Public Shared Property ServidorSQL() As String
                Get
                    'Return "Data Source=.\SQLExpress; Integrated Security=true; attachdbfilename=|DataDirectory|data\Callcenter.mdf; user instance=true;"

                    Dim nodo As System.Xml.XmlNode = get_nodo("servidorSQL")
                    If nodo Is Nothing OrElse nodo.InnerText = "" Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("servidorSQL", value)
                End Set
            End Property

            Public Shared Property Moneda() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("moneda")
                    If nodo Is Nothing OrElse nodo.InnerText = "" Then
                        Return "MXN"
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("moneda", value)
                End Set
            End Property

            Public Shared Property NombreSistema() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("NombreSistema")
                    If nodo Is Nothing Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("NombreSistema", value)
                End Set
            End Property

            Public Shared Property Imagen() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("Imagen")
                    If nodo Is Nothing Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("Imagen", value)
                End Set
            End Property

            Public Shared Property Idioma() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("idioma")
                    If nodo Is Nothing Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("idioma", value)
                End Set
            End Property

            Public Shared Property Email() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("email")
                    If nodo Is Nothing Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("email", value)
                End Set
            End Property

            Public Shared Property ProfessionalsSQL_SERVER() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("ProfessionalsSQL_SERVER")
                    If nodo Is Nothing Then
                        Return "dboz.ozvir.com"
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("ProfessionalsSQL_SERVER", value)
                End Set
            End Property

            Public Shared Property ProfessionalsSQL_USER() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("ProfessionalsSQL_USER")
                    If nodo Is Nothing Then
                        Return "hotelesmision"
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("ProfessionalsSQL_USER", value)
                End Set
            End Property
            Public Shared Property ProfessionalsSQL_PASSWORD() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("ProfessionalsSQL_PASSWORD")
                    If nodo Is Nothing Then
                        Return "ProfBD_M1si0n09"
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("ProfessionalsSQL_PASSWORD", value)
                End Set
            End Property
            Public Shared Property ProfessionalsSQL_DB() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("ProfessionalsSQL_DB")
                    If nodo Is Nothing Then
                        Return "hotelesmision_log"
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("ProfessionalsSQL_DB", value)
                End Set
            End Property


        End Class

        Public Class wsData

            Public Shared ReadOnly Property XMLfile_name() As String
                Get
                    Return DataDocumentsPath + "\ws_data.xml"
                End Get
            End Property

            Private Shared ReadOnly wsS As String = My.Settings.callCenter_WS_SERVICES_Service1
            Private Shared ReadOnly wsP As String = My.Settings.callCenter_WS_PASSPORT_Passport
            Private Shared ReadOnly wsC As String = My.Settings.callCenter_WS_CALL_adminCalls
            Private Shared ReadOnly wsU As String = My.Settings.callCenter_WS_DESTINOSUPDATE_destinosUpdate_service

            Private Shared Function doc() As System.Xml.XmlDocument
                Dim xmlD As New System.Xml.XmlDocument
                Dim def_xml As String = ""
                def_xml += "<datosWS>"
                'def_xml += "    <info name='version'>" + My.Application.Info.Version.ToString + "</info>"
                def_xml += "    <ws name='services'>" + wsS + "</ws>"
                def_xml += "    <ws name='passport'>" + wsP + "</ws>"
                def_xml += "    <ws name='calls'>" + wsC + "</ws>"
                def_xml += "    <ws name='update'>" + wsU + "</ws>"
                def_xml += "</datosWS>"

                If System.IO.File.Exists(XMLfile_name) Then
                    Try
                        xmlD.Load(XMLfile_name)

                        'Dim version As String = Nothing
                        'For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                        '    If ws_nodo.Attributes("name").Value = "version" Then
                        '        version = ws_nodo.InnerText
                        '        Exit For
                        '    End If
                        'Next

                        'If version = Nothing OrElse Not version = My.Application.Info.Version.ToString Then
                        '    System.IO.File.Delete(XMLfile_name)
                        '    xmlD.LoadXml(def_xml)
                        '    xmlD.Save(XMLfile_name)
                        'End If
                    Catch ex As Exception
                        xmlD.LoadXml(def_xml)
                    End Try
                Else
                    xmlD.LoadXml(def_xml)
                    xmlD.Save(XMLfile_name)
                End If

                Return xmlD
            End Function

            Private Shared Function get_nodo(ByVal name As String) As System.Xml.XmlNode
                Try
                    Dim xmlD As System.Xml.XmlDocument = doc()

                    For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                        If ws_nodo.Attributes("name").Value = name Then
                            Return ws_nodo
                        End If
                    Next
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0036", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End Try

                Return Nothing
            End Function

            Private Shared Sub set_nodo(ByVal NodoName As String, ByVal NodoValue As String)
                Dim xmlD As System.Xml.XmlDocument = doc()

                For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                    If ws_nodo.Attributes("name").Value = NodoName Then
                        ws_nodo.InnerText = NodoValue
                        Exit For
                    End If
                Next

                xmlD.Save(XMLfile_name)
            End Sub

            'Public Shared Property Version() As String
            '    Get
            '        Dim nodo As System.Xml.XmlNode = get_nodo("version")
            '        If nodo Is Nothing OrElse nodo.InnerText = "" Then
            '            Return ""
            '        Else
            '            Return nodo.InnerText
            '        End If
            '    End Get
            '    Set(ByVal value As String)
            '        set_nodo("version", value)
            '    End Set
            'End Property

            Public Shared Property UrlWsServices() As String
                Get
                    'Return "http://localhost:49814/services.asmx" 'TODO: Quitar al liberar
                    Dim nodo As System.Xml.XmlNode = get_nodo("services")
                    If nodo Is Nothing OrElse nodo.InnerText = "" Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("services", value)
                End Set
            End Property

            Public Shared Property UrlWsPassport() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("passport")
                    If nodo Is Nothing OrElse nodo.InnerText = "" Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("passport", value)
                End Set
            End Property

            Public Shared Property UrlWsCalls() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("calls")
                    If nodo Is Nothing OrElse nodo.InnerText = "" Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("calls", value)
                End Set
            End Property

            Public Shared Property UrlWsUpdate() As String
                Get
                    Dim nodo As System.Xml.XmlNode = get_nodo("update")
                    If nodo Is Nothing OrElse nodo.InnerText = "" Then
                        Return ""
                    Else
                        Return nodo.InnerText
                    End If
                End Get
                Set(ByVal value As String)
                    set_nodo("update", value)
                End Set
            End Property

        End Class

    End Class

End Class