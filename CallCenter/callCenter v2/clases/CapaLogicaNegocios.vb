Public Class CapaLogicaNegocios

    Public Class Common

        Public Shared Function transformar(ByVal codigo_xsl As String, ByVal str_xml As String) As String
            Dim _xslTransform As New System.Xml.Xsl.XslTransform
            Dim wstr As System.IO.StringWriter
            Dim xmlRes As New System.Xml.XmlUrlResolver

            'If System.IO.Directory.Exists("dataTransform") Then
            '    _xslTransform.Load("dataTransform\" + nombre_xsl)
            'Else
            '    _xslTransform.Load("..\..\dataTransform\" + nombre_xsl)
            'End If

            Dim buff() As Byte = System.Text.Encoding.UTF8.GetBytes(codigo_xsl)
            Dim memS As New System.IO.MemoryStream(buff, 0, buff.Length)
            Dim stylesheet As New System.Xml.XmlTextReader(memS)
            _xslTransform.Load(stylesheet)

            xmlRes.Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim doc As New System.Xml.XmlDocument
            doc.LoadXml(str_xml)
            wstr = New System.IO.StringWriter
            _xslTransform.Transform(doc, Nothing, wstr, xmlRes)
            Dim s As String = wstr.ToString
            wstr.Close()

            Return s
        End Function

        Public Shared Sub CloseAll()
            CloseAll("")
        End Sub

        Public Shared Sub CloseAll(ByVal ButThis As String)
            If ButThis Is Nothing Then ButThis = ""

            For Each x As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In misForms.form1.UltraTabbedMdiManager1.TabGroups
                Dim sal As Boolean = False
                While Not sal
                    If x.Tabs.Count = 0 Then sal = True
                    If x.Tabs.Count = 1 AndAlso x.Tabs(0).Form.Name = ButThis Then sal = True

                    Dim NoCallIndex As Integer = 0
                    For Each tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab In x.Tabs
                        If tab.Form.Name <> ButThis Then
                            NoCallIndex = tab.Index
                            Exit For
                        End If
                    Next

                    If Not sal AndAlso x.Tabs(NoCallIndex).Form.Name <> ButThis Then x.Tabs(NoCallIndex).Form.Close()
                End While
            Next
        End Sub

        Public Shared Function XMLFileToDS(ByVal FileName As String)
            Try
                Dim ds As New DataSet
                Dim doc As Xml.XmlDocument = CapaAccesoDatos.XML.GetDoc(FileName, CapaAccesoDatos.XML.DataType.FilePath)

                Dim buf() As Byte = System.Text.Encoding.UTF8.GetBytes(doc.DocumentElement.OuterXml)
                Dim ms As New System.IO.MemoryStream(buf, 0, buf.Length)
                ds.ReadXml(ms)

                Return ds
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

    End Class

    Public Class Reservations

        Public Shared Sub busca_reservaciones(ByVal param As Object)
            Dim last_msg As String = ""

            Dim ini As DateTime = param(0)
            Dim fin As DateTime = param(1)
            Dim tipofechaSTR As String = param(2)
            Dim rubro As String = param(3)
            Dim cliente As String = param(4)
            Dim numConfirm As String = param(5)
            If rubro = "-1" Then rubro = Nothing
            If cliente = "" Then cliente = Nothing
            If numConfirm = "" Then numConfirm = Nothing
            Dim frm As Reservaciones = CType(param(6), Reservaciones)
            Dim origen As String = param(7)
            If origen = "-1" Then origen = Nothing

            Dim transaccionesPms As String = IIf(param(8) IsNot Nothing AndAlso param(8) <> "", param(8), Nothing)
            Dim empresa As String = param(9)
            Dim segmento As String = IIf(param(10) = "", Nothing, param(10))
            Dim idUsuarioCallCenter As String = param(11)            
            Dim idEmpresa As String = param(12)


            Try
                Dim tipoFecha As enumFilterReservationDates
                Select Case tipofechaSTR
                    Case "1"
                        tipoFecha = enumFilterReservationDates.Checkin
                    Case "2"
                        tipoFecha = enumFilterReservationDates.CheckOut
                    Case "3"
                        tipoFecha = enumFilterReservationDates.RegisterDate
                End Select

                Dim ds As DataSet
                Dim agencia_id As Integer = CInt(dataOperador.agency_ID)

                'ds = ws_login.GetAgencyReservation(ini, fin, enumRubrosPassport.Paquetes, tipoFecha, agencia_id)
                'ds = ws_login.GetAgencyReservation(ini, fin, enumRubrosPassport.Hotel, tipoFecha, agencia_id)
                'ds = ws_login.GetAgencyReservation(ini, fin, enumRubrosPassport.Vuelos, tipoFecha, agencia_id)
                'ds = ws_login.GetAgencyReservation(ini, fin, enumRubrosPassport.Actividades, tipoFecha, agencia_id)
                'ds = ws_login.GetAgencyReservation(ini, fin, enumRubrosPassport.Autos, tipoFecha, agencia_id)

                'que en lugar de consultar ws_login.GetReservation, que consulte directo al sp
                '(el codigo lo tengo en el escritorio) y que traiga lso datos. usar alomejor la
                'clase de la bd para traerlos si nomas no se puede, usar ws services (o checar
                'primero si sw services ya tiene conexion a ozhoteles)

                'ds = ws_login.GetReservation(ini, fin, enumRubrosPassport.Paquetes, tipoFecha)
                'ds = get_more_reservations(ds, ini, fin, enumRubrosPassport.Hotel, tipoFecha)
                'ds = get_more_reservations(ds, ini, fin, enumRubrosPassport.Vuelos, tipoFecha)
                'ds = get_more_reservations(ds, ini, fin, enumRubrosPassport.Actividades, tipoFecha)
                'ds = get_more_reservations(ds, ini, fin, enumRubrosPassport.Autos, tipoFecha)

                Dim datosBusqueda_passport As New dataBusqueda_passport
                datosBusqueda_passport.idUsuario = dataOperador.user_ID_agent_ID
                datosBusqueda_passport.idCorporativo = dataOperador.idCorporativo
                datosBusqueda_passport.Fecha1 = ini
                datosBusqueda_passport.Fecha2 = fin
                datosBusqueda_passport.Type = tipoFecha
                datosBusqueda_passport.Rubro = rubro
                datosBusqueda_passport.Origen = origen
                datosBusqueda_passport.NombreClie = cliente
                datosBusqueda_passport.NumeroConf = numConfirm
                datosBusqueda_passport.idSegmento = segmento
                datosBusqueda_passport.NombreEmpresa = empresa
                datosBusqueda_passport.TransaccionesPMS = transaccionesPms
                datosBusqueda_passport.IdUsuarioCallCenter = idUsuarioCallCenter
                datosBusqueda_passport.idEmpresa = idEmpresa

                ds = CapaLogicaNegocios.GetWS.obten_passport_GetReservations(datosBusqueda_passport)

                Try
                    If ds IsNot Nothing AndAlso ds.Tables.Contains("Response") Then
                        With ds.Tables("Response").Columns
                            .Add("RubroTXT")
                            .Add("StatusTXT")
                            .Add("ServiceProvider")
                            .Add("CheckInTXT")
                            .Add("CheckOutTXT")
                            .Add("PendientesPMSTXT")
                            '.Add("Total")
                            If Not .Contains("Noches") Then .Add("Noches")
                            If Not .Contains("FechaRegistro") Then .Add("FechaRegistro")
                            If Not .Contains("FechaRegistroTXT") Then .Add("FechaRegistroTXT")
                            If Not .Contains("UsuarioReservo") Then .Add("UsuarioReservo")
                            If Not .Contains("Habitaciones") Then .Add("Habitaciones")
                            If Not .Contains("EmpresaConvenio") Then .Add("EmpresaConvenio")
                            If Not .Contains("CiudadCliente") Then .Add("CiudadCliente")
                            If Not .Contains("Impuesto") Then .Add("Impuesto")
                        End With
                    End If

                    If ds Is Nothing OrElse Not ds.Tables.Contains("Response") OrElse ds.Tables("Response").Rows.Count = 0 Then
                        Dim s1 As String = ""
                        Dim s2 As String = ""
                        Dim s3 As String = ""
                        If ds Is Nothing Then s1 = "ds is nothing - " Else If Not ds.Tables.Contains("Response") Then s2 = "ds no tiene tabla Response - " Else If ds.Tables("Response").Rows.Count = 0 Then s3 = "ds Response no tiene rows - "

                        CapaLogicaNegocios.ErrorManager.Manage("E0104", "REVISAR_01: " + s1 + s2 + s3, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                    Else
                        For Each r As DataRow In ds.Tables("Response").Rows
                            ' se hace la consulta especifica a cada row para sacar datos 
                            'como por ejemplo, el verdadero nombre del cliente y no del agente.

                            'para debuguear si hay error en algun row, que ponga todos los datos de ese row
                            'Dim ar() As Object = r.ItemArray
                            'Dim s2 As String = ""
                            'For Each o As Object In ar
                            '    If Not IsDBNull(o) Then s2 += CStr(o) + ","
                            'Next

                            'Select Case r.Item("idRubro")
                            '    Case enumRubrosPassport.Paquetes
                            '        consulta_paquetes(r)
                            '    Case enumRubrosPassport.Vuelos
                            '        consulta_vuelos(r)
                            '    Case enumRubrosPassport.Autos
                            '        consulta_autos(r)
                            '    Case enumRubrosPassport.Hotel
                            '        consulta_hoteles(r)
                            '    Case enumRubrosPassport.Actividades
                            '        consulta_actividad(r)
                            'End Select

                            'Select Case r.Item("Source")
                            '    Case "CCT"
                            '        r.Item("Source") = "Call Center"
                            'End Select

                            r.Item("CheckInTXT") = CDate(r.Item("CheckIn")).ToString("dd/MMM/yyyy")
                            r.Item("CheckOutTXT") = CDate(r.Item("CheckOut")).ToString("dd/MMM/yyyy")
                            If (Not r.IsNull("FechaRegistro")) Then
                                r.Item("FechaRegistroTXT") = CDate(r.Item("FechaRegistro")).ToString("dd/MMM/yyyy")
                            End If
                        Next
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0105", ex.Message + " ------------ REVISAR_01 en el FOR de Reservation", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End Try

                last_msg = "antes de rubroTXT y statusTXT"
                Dim dv As DataView
                Dim aux As Decimal
                If ds IsNot Nothing AndAlso ds.Tables.Contains("Response") AndAlso ds.Tables("Response").Rows.Count > 0 Then
                    For Each r As DataRow In ds.Tables("Response").Rows
                        If Not String.IsNullOrEmpty(r("tarifa")) AndAlso Decimal.TryParse(r("tarifa"), aux) Then
                            r("tarifa") = aux.ToString("c")
                        End If
                        If Not String.IsNullOrEmpty(r("Total")) AndAlso Decimal.TryParse(r("Total"), aux) Then
                            r("Total") = aux.ToString("c")
                        End If
                        If Not String.IsNullOrEmpty(r("Subtotal")) AndAlso Decimal.TryParse(r("Subtotal"), aux) Then
                            r("Subtotal") = aux.ToString("c")
                        End If
                        If Not String.IsNullOrEmpty(r("Impuesto")) AndAlso Decimal.TryParse(r("Impuesto"), aux) Then
                            r("Impuesto") = aux.ToString("c")
                        End If

                        If Not r.Table.Columns.Contains("idRubro") Then
                            CapaLogicaNegocios.ErrorManager.Manage("E0106", " ------------ REVISAR_01 no contiene campo: Rubro", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                            Continue For
                        End If
                        If Not r.Table.Columns.Contains("RubroTXT") Then
                            CapaLogicaNegocios.ErrorManager.Manage("E0107", " ------------ REVISAR_01 no contiene campo: RubroTXT", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                            Continue For
                        End If
                        If Not r.Table.Columns.Contains("Status") Then
                            CapaLogicaNegocios.ErrorManager.Manage("E0108", " ------------ REVISAR_01 no contiene campo: Status", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                            Continue For
                        End If
                        If Not r.Table.Columns.Contains("StatusTXT") Then
                            CapaLogicaNegocios.ErrorManager.Manage("E0109", " ------------ REVISAR_01 no contiene campo: StatusTXT", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                            Continue For
                        End If

                        last_msg = "antes de asignar rubroTXT"

                        Select Case r.Item("idRubro")
                            Case "9"
                                r.Item("RubroTXT") = "Paquete" ' If r.IsNull("RubroTXT") OrElse r.Item("RubroTXT") = "" Then
                            Case "23"
                                r.Item("RubroTXT") = "Vuelo"
                            Case "10"
                                r.Item("RubroTXT") = "Hotel"
                            Case "11"
                                r.Item("RubroTXT") = "Auto"
                            Case "27"
                                r.Item("RubroTXT") = "Actividad"
                        End Select

                        last_msg = "antes de asignar statusTXT"

                        r.Item("StatusTXT") = herramientas.GetSatatusText(r.Item("Status"))
                        r.Item("PendientesPMSTXT") = IIf(r.Item("PendientesPMS").ToString() = "1", CapaPresentacion.Idiomas.get_str("str_0227_si").ToUpper(), CapaPresentacion.Idiomas.get_str("str_0228_no").ToUpper())

                    Next

                    last_msg = "antes de asignar db"
                    'If rubro = "9" Then
                    '    ds.Tables("Response").DefaultView.RowFilter = "idreservacion=-1"
                    'End If

                    'Reorganizamos las columnas del dataset
                    With ds.Tables("Response")
                        .Columns("NoReservacion").SetOrdinal(0) 'Num. de Confrimacion
                        .Columns("CheckInTXT").SetOrdinal(1) 'CheckIn
                        .Columns("CheckOutTXT").SetOrdinal(2)   'CheckOut
                        .Columns("Noches").SetOrdinal(4) 'Noches
                        .Columns("Source").SetOrdinal(5) 'Origen de reservacion
                        .Columns("Cliente").SetOrdinal(6)   'Nombre del Cliente
                        .Columns("EmailCliente").SetOrdinal(7) 'Correo del Cliente
                        .Columns("nombre").SetOrdinal(8) 'Hotel
                        .Columns("Ciudad").SetOrdinal(9) 'Destino
                        .Columns("Habitaciones").SetOrdinal(10) 'Habitaciones
                        .Columns("UsuarioReservo").SetOrdinal(11)   'Reservado por
                        .Columns("FechaRegistroTXT").SetOrdinal(12) 'Fecha registro
                        .Columns("EmpresaConvenio").SetOrdinal(13) 'Empresa/Convenio
                        .Columns("Segmento").SetOrdinal(14) 'Segmento
                        .Columns("StatusTXT").SetOrdinal(15) 'Status
                        .Columns("PortalName").SetOrdinal(16)   'Portal
                        .Columns("pendientesPMS").SetOrdinal(17) 'Transacciones PMS
                        .Columns("pendientesPMSTXT").SetOrdinal(18) 'Transacciones PM                                                
                        .Columns("codigopostal").SetOrdinal(19) 'Transacciones PMS                        
                        .Columns("tarifa").SetOrdinal(20) 'Transacciones PMS
                        .Columns("Subtotal").SetOrdinal(21) 'Subtotal
                        .Columns("Impuesto").SetOrdinal(22) 'Subtotal
                        .Columns("Total").SetOrdinal(23) 'Total
                        If .Columns.Contains("EstadoCliente") Then .Columns("EstadoCliente").SetOrdinal(24) 'Estado del cliente
                        If .Columns.Contains("CiudadCliente") Then .Columns("CiudadCliente").SetOrdinal(25) 'Ciudad del cliente
                    End With

                    dv = ds.Tables("Response").DefaultView
                    dv.Sort = "idReservacion asc"
                    'UltraGrid1.DataSource = dv
                Else
                    dv = Nothing
                End If

                last_msg = "antes de col perform auto resize"

                FIJA_GRID(frm, frm.UltraGrid1, dv)

                last_msg = "antes de camb idioma"

                FIJA_PIC(frm, frm.PictureBox2, False)

                last_msg = "ok"
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0110", ex.Message + " /////////////////////// en el punto: " + last_msg, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                'MessageBox.Show("BORRAR MENSAJE DEL TRY-CATCH" + vbCrLf + ex.Message, CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Me.Cursor = Cursors.Arrow
                FIJA_GRID(frm, frm.UltraGrid1, Nothing)
            End Try

            'termino_tread = True
        End Sub

        'Private Function get_more_reservations(ByVal ds As ResReservations, ByVal ini As DateTime, ByVal fin As DateTime, ByVal tipoRubro As enumRubrosPassport, ByVal tipoFecha As enumFilterReservationDates) As ResReservations
        '    Dim ds_temp As ResReservations = ws_login.GetReservation(ini, fin, tipoRubro, tipoFecha)

        '    If ds_temp IsNot Nothing Then
        '        If ds Is Nothing Then
        '            ds = ds_temp
        '        Else
        '            ds.Merge(ds_temp)
        '        End If
        '    End If

        '    Return ds
        'End Function

        Delegate Sub FIJA_GRID_Callback(ByVal frm As Reservaciones, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
        Private Shared Sub FIJA_GRID(ByVal frm As Reservaciones, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            Try
                If grid.InvokeRequired Then
                    Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                    frm.Invoke(d, New Object() {frm, grid, dv})
                Else
                    formatea_grid(grid, dv)
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_PIC_Callback(ByVal frm As Reservaciones, ByVal pic As PictureBox, ByVal vis As Boolean)
        Private Shared Sub FIJA_PIC(ByVal frm As Reservaciones, ByVal pic As PictureBox, ByVal vis As Boolean)
            Try
                If pic.InvokeRequired Then
                    Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                    frm.Invoke(d, New Object() {frm, pic, vis})
                Else
                    pic.Visible = vis
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Private Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            grid.DataSource = dv

            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In grid.DisplayLayout.Bands(0).Columns
                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next
        End Sub

        'Delegate Sub FIJA_PICT_Callback(ByVal pic As PictureBox, ByVal vis As Boolean)
        'Private Sub FIJA_PICT(ByVal pic As PictureBox, ByVal vis As Boolean)
        '    Try
        '        If pic.InvokeRequired Then
        '            Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
        '            Me.Invoke(d, New Object() {pic, vis})
        '        Else
        '            pic.Visible = vis
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    End Try
        'End Sub

        Private Sub consulta_paquetes(ByVal r As DataRow)
            Dim dat_paquete As New dataBusqueda_paquete
            dat_paquete.itinerario = r.Item("NoReservacion")
            Dim ds_paquete As DataSet = obtenGenerales_paquetes.obten_general_Paquete_Description(dat_paquete)

            If ds_paquete.Tables.Contains("Error") AndAlso ds_paquete.Tables("Error").Rows.Count > 0 Then Return

            Try
                If ds_paquete.Tables("Package").Columns.Contains("HotelReservationId") AndAlso Not ds_paquete.Tables("Package").Rows(0).IsNull("HotelReservationId") Then
                    'obtiene datos de la reservacion - enter AndAlso

                    Dim datosDisplay As New datos_display
                    datosDisplay.ReservationId = ds_paquete.Tables("Package").Rows(0).Item("HotelReservationId")
                    Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)

                    'especifica el nombre dle cliente
                    If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Room") OrElse ds_display.Tables("Room").Rows.Count = 0 OrElse Not ds_display.Tables("Room").Columns.Contains("TravelerName") Then Return
                    r.Item("Cliente") = ds_display.Tables("Room").Rows(0).Item("TravelerName")
                    Return
                End If
            Catch ex As Exception
                MsgBox("1")
            End Try

            Try
                If ds_paquete.Tables("Package").Columns.Contains("CarReservationId") AndAlso Not ds_paquete.Tables("Package").Rows(0).IsNull("CarReservationId") Then
                    'obtiene datos de la reservacion - enter AndAlso
                    Dim datos_car As New dataBusqueda_auto_test
                    datos_car.IdReservation = ds_paquete.Tables("Package").Rows(0).Item("CarReservationId")
                    Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarReservation(datos_car)

                    'especifica nombre de la ciudad
                    If r.Item("City") = "" Then
                        If ds_display.Tables.Contains("OfficeStart") Then r.Item("City") = ds_display.Tables("OfficeStart").Rows(0).Item("CityStart")
                    End If

                    'especifica nombre de la compa�ia
                    If r.Item("CompanyName") = "" Then
                        If ds_display.Tables.Contains("Reservation") Then r.Item("CompanyName") = ds_display.Tables("Reservation").Rows(0).Item("CarVendor")
                    End If

                    'especifica el nombre dle cliente
                    If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables("Reservation").Columns.Contains("DriverName") Then Return
                    r.Item("ClientName") = ds_display.Tables("Reservation").Rows(0).Item("DriverName")
                    Return
                End If
            Catch ex As Exception
                MsgBox("2")
            End Try

            Try
                If ds_paquete.Tables("Package").Columns.Contains("ActivityReservationId") AndAlso Not ds_paquete.Tables("Package").Rows(0).IsNull("ActivityReservationId") Then
                    Dim datos_paq As New dataBusqueda_paquete
                    datos_paq.itinerario = ds_paquete.Tables("Package").Rows(0).Item("Itinerary")
                    Dim ds_irt As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_ITR2(datos_paq)

                    Dim datos_act As New dataBusqueda_actividad_test
                    datos_act.ReservationID = ds_irt.Tables("Reservation").Rows(0).Item("ReservationID")
                    Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_DSR2(datos_act)

                    'especifica nombre de la ciudad
                    If r.Item("City") = "" Then
                        If ds_display.Tables.Contains("Event") Then r.Item("City") = ds_display.Tables("Event").Rows(0).Item("City")
                    End If

                    'especifica nombre de la compa�ia
                    If r.Item("CompanyName") = "" Then
                        If ds_display.Tables.Contains("Property") Then r.Item("CompanyName") = ds_display.Tables("Property").Rows(0).Item("PropertyName")
                    End If

                    'especifica fecha 1
                    If r.IsNull("CheckIn") Then
                        Try
                            r.Item("CheckIn") = ds_display.Tables("Event").Rows(0).Item("StartDate")
                        Catch ex As Exception
                        End Try
                    End If

                    'especifica el nombre dle cliente
                    'If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables("Reservation").Columns.Contains("DriverName") Then Continue For
                    If r.Item("ClientName") = "" Then
                        r.Item("ClientName") = ds_display.Tables("CreditCard").Rows(0).Item("Name")
                    End If
                    Return
                End If
            Catch ex As Exception
                MsgBox("3")
            End Try


            'queda pendiente vuelo en paquetes


        End Sub

        Private Sub consulta_vuelos(ByVal r As DataRow)
            Try
                Dim dat_vuelo As New dataBusqueda_vuelo
                dat_vuelo.ReservationId = r.Item("idReservacion")
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_vuelo_AirReservationDetails(dat_vuelo)

                If Not ds_display.Tables.Contains("Passengers") OrElse ds_display.Tables("Passengers").Rows.Count = 0 Then Return 'Exit Select
                If Not ds_display.Tables.Contains("ReservationDetail") OrElse ds_display.Tables("ReservationDetail").Rows.Count = 0 Then Return 'Exit Select

                r.Item("Cliente") = ds_display.Tables("Passengers").Rows(0).Item("FName") + " " + ds_display.Tables("Passengers").Rows(0).Item("LName")
                r.Item("nombre") = ds_display.Tables("ReservationDetail").Rows(0).Item("airline")
            Catch ex As Exception
                MsgBox("error row vuelo")
            End Try
        End Sub

        Private Sub consulta_autos(ByVal r As DataRow)
            Try
                Dim dat_auto As New dataBusqueda_auto_test
                dat_auto.IdReservation = r.Item("idReservacion")
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarReservation(dat_auto)

                If Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 Then Return 'Exit Select
                'If Not ds_display.Tables.Contains("ReservationDetail") OrElse ds_display.Tables("ReservationDetail").Rows.Count = 0 Then Return 'Exit Select

                r.Item("cliente") = ds_display.Tables("Reservation").Rows(0).Item("DriverName")
                r.Item("nombre") = ds_display.Tables("Reservation").Rows(0).Item("CarVendor")
            Catch ex As Exception
                MsgBox("error row autos")
            End Try
        End Sub

        Private Sub consulta_hoteles(ByVal r As DataRow)
            Try
                Dim datosDisplay As New datos_display
                datosDisplay.ReservationId = r.Item("idReservacion")
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)

                If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Customer") OrElse ds_display.Tables("Customer").Rows.Count = 0 Then Return
                If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 Then Return

                'r.Item("ClientName") = ds_display.Tables("Customer").Rows(0).Item("FirstName") + " " + ds_display.Tables("Customer").Rows(0).Item("LastName")
                Try
                    r.Item("Cliente") = ds_display.Tables("Reservation").Rows(0).Item("CreditCardHolder")
                Catch ex As Exception
                End Try
                r.Item("ServiceProvider") = ds_display.Tables("Reservation").Rows(0).Item("Provider")
            Catch ex As Exception
                MsgBox("error row hotel")
            End Try
        End Sub

        Private Sub consulta_actividad(ByVal r As DataRow)
            'Try
            '    Dim dat_actividad As New dataBusqueda_actividad_test
            '    dat_actividad.ReservationID = r.Item("ReservationId")
            '    Dim ds_display As DataSet = get_ws.obten_actividad_DSR2(dat_actividad)
            '
            '    If Not ds_display.Tables.Contains("Event") OrElse ds_display.Tables("Event").Rows.Count = 0 Then Return
            '
            '    'Dim res As Boolean
            '    'Dim ws_pass As New webService_passport(res)
            '    Dim trav As ResTraveler.TravelerDataTable = ws_login.getUserTravelerById(CInt(ds_display.Tables("Event").Rows(0).Item("ClientID")))
            '
            '    'r.Item("ClientName") = ds_display.Tables("Reservation").Rows(0).Item("DriverName")
            '    'r.Item("CompanyName") = ds_display.Tables("Reservation").Rows(0).Item("CarVendor")
            'Catch ex As Exception
            '    MsgBox("error row hotel")
            'End Try
        End Sub

        Public Shared Sub update_filtros(ByVal frm As Reservaciones)
            Dim dv As DataView
            Try
                dv = CType(frm.UltraGrid1.DataSource, DataTable).DefaultView
            Catch ex As Exception
                dv = frm.UltraGrid1.DataSource
            End Try

            If dv IsNot Nothing Then
                Dim filtros As New List(Of String)
                'If txt_cliente.Text <> "" Then filtros.Add("ClientName like '*" + txt_cliente.Text + "*'")
                'If txt_numConfirm.Text <> "" Then filtros.Add("ReservationNumber like '*" + txt_numConfirm.Text + "*'")
                If frm.txt_compania.Text <> "" Then filtros.Add("nombre like '*" + frm.txt_compania.Text + "*'")
                If frm.txt_ciudad.Text <> "" Then filtros.Add("Ciudad like '*" + frm.txt_ciudad.Text + "*'")
                dv.RowFilter = ""
                For Each f As String In filtros
                    If dv.RowFilter = "" Then dv.RowFilter += f Else dv.RowFilter += " and " + f
                Next
            End If

            frm.UltraGrid1.DataSource = dv
        End Sub

        Public Shared Sub clear_filtros(ByVal frm As Reservaciones)
            'dtp_in.Value = DateTime.Now
            'dtp_out.Value = DateTime.Now
            'dtp_in.Checked = False
            'dtp_out.Checked = False
            'txt_cliente.Clear()
            'txt_numConfirm.Clear()
            frm.txt_ciudad.Clear()
            frm.txt_compania.Clear()
        End Sub

        'Public Sub actualizar_campo_cancelado()
        '    Dim conn As New SqlConnection()
        '    conn.ConnectionString = tools_db.str_conn("ventanaCallCenter")

        '    Dim cmd As New SqlCommand()
        '    cmd.Connection = conn
        '    cmd.CommandType = CommandType.StoredProcedure
        '    cmd.CommandText = "actualizar_cancelada"

        '    cmd.Parameters.Add("@noConfirm", SqlDbType.NVarChar, 15).Value = UltraGrid1.ActiveRow.Cells("noConfirm").Value.ToString

        '    Dim rowCount As Integer
        '    Dim previousConnectionState As ConnectionState
        '    previousConnectionState = conn.State
        '    Try
        '        If conn.State = ConnectionState.Closed Then
        '            conn.Open()
        '        End If
        '        rowCount = cmd.ExecuteNonQuery()
        '    Finally
        '        If previousConnectionState = ConnectionState.Closed Then
        '            conn.Close()
        '        End If
        '    End Try
        'End Sub

        Public Shared Sub llena_combos(ByVal frm As Reservaciones)
            Dim idx As Integer = frm.uce_tipoFecha.SelectedIndex
            frm.uce_tipoFecha.Items.Clear()
            frm.uce_tipoFecha.Items.Add("1", CapaPresentacion.Idiomas.get_str("str_336_arrDate"))
            frm.uce_tipoFecha.Items.Add("2", CapaPresentacion.Idiomas.get_str("str_337_depDate"))
            frm.uce_tipoFecha.Items.Add("3", CapaPresentacion.Idiomas.get_str("str_335_resDate"))
            frm.uce_tipoFecha.SelectedIndex = idx
            If frm.uce_tipoFecha.SelectedIndex = -1 Then frm.uce_tipoFecha.SelectedIndex = 2

            idx = frm.uce_rubro.SelectedIndex
            frm.uce_rubro.Items.Clear()
            'frm.uce_rubro.Items.Add("-1", CapaPresentacion.Idiomas.get_str("str_354_todos"))

            frm.uce_rubro.Items.Add("10", CapaPresentacion.Idiomas.get_str("str_0032_hotel"))
            frm.uce_rubro.Items.Add("9", CapaPresentacion.Idiomas.get_str("str_355_paquetes"))
            frm.uce_rubro.Items.Add("23", CapaPresentacion.Idiomas.get_str("str_0279_vuelos"))
            frm.uce_rubro.Items.Add("11", CapaPresentacion.Idiomas.get_str("str_0281_autos"))
            frm.uce_rubro.Items.Add("27", CapaPresentacion.Idiomas.get_str("str_0280_actividades"))
            frm.uce_rubro.SelectedIndex = idx
            If frm.uce_rubro.SelectedIndex = -1 Then frm.uce_rubro.SelectedIndex = 0

            'Combobox de seleccion de origen de reservacion
            idx = frm.uce_source.SelectedIndex
            frm.uce_source.Items.Clear()
            frm.uce_source.Items.Add("-1", CapaPresentacion.Idiomas.get_str("str_354_todos"))
            frm.uce_source.Items.Add("CCT", "CCT- Call Center")
            frm.uce_source.Items.Add("POR", "POR - Portales")
            frm.uce_source.Items.Add("UNI", "UNI - Unipantalla")
            frm.uce_source.Items.Add("WIZ", "WIZ - Globalizadores GDS")
            frm.uce_source.Items.Add("ADS", "ADS")
            frm.uce_source.Items.Add("IDS", "IDS")

            frm.uce_source.SelectedIndex = idx
            If frm.uce_source.SelectedIndex = -1 Then frm.uce_source.SelectedIndex = 0

        End Sub

    End Class

    Public Class Calls

        'Public Shared Sub VerificaNewCall()
        '    If misForms.newCall Is Nothing OrElse Not misForms.newCall.datos_call.iniciada Then
        '        If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0300_reqLlamada"), CapaPresentacion.Idiomas.get_ str("str_0155_ atencion"), MessageBoxButtons.YesNo, MessageBoxIcon.Information) = MsgBoxResult.Yes Then
        '            If misForms.newCall Is Nothing Then
        '                misForms.exe_accion(Nothing, "btn_nuevaLlamada")
        '            End If

        '            If Not misForms.newCall.datos_call.iniciada Then
        '                misForms.newCall.UltraButton1.PerformClick()
        '            End If
        '        End If
        '    End If
        'End Sub

        Public Shared Function SelectOption() As callCenter.CallOption.CallResult
            Dim res As callCenter.CallOption.CallResult = CallOption.CallResult.NewCall

            If misForms.newCall Is Nothing OrElse misForms.newCall.datos_call Is Nothing OrElse misForms.newCall.UltraButton1.Enabled Then
                Dim frm As New CallOption
                frm.ShowDialog()
                res = frm.Result

                'Select Case frm.Result
                '    Case CallOption.CallResult.NewCall

                '    Case CallOption.CallResult.BookNoCall

                '    Case CallOption.CallResult.Cancel

                'End Select

            End If

            '-----------------------------------------------

            Select Case res
                Case CallOption.CallResult.NewCall
                    If misForms.newCall Is Nothing Then
                        misForms.exe_accion(Nothing, "btn_nuevaLlamada")
                    End If

                    If Not misForms.newCall.datos_call.iniciada Then
                        misForms.newCall.UltraButton1.PerformClick()
                    End If

                Case CallOption.CallResult.BookNoCall
                    If misForms.newCall Is Nothing Then
                        misForms.exe_accion(Nothing, "btn_nuevaLlamada")
                    End If

                    If Not misForms.newCall.datos_call.iniciada Then
                        misForms.newCall.PrepareBookNoCall()
                    End If

                Case CallOption.CallResult.Cancel
                    '
            End Select

            If misForms.newCall IsNot Nothing Then misForms.newCall.ReservationOption = res

            Return res
        End Function

    End Class

    Public Class ReservationDetails

        Public Shared Sub busca_paquete_async(ByVal objFrm As Object)
            Try
                Dim frm As detalles = CType(objFrm, detalles)

                Dim output As String = ""
                Dim AltCurrency As String = ""
                If frm.uce_moneda.SelectedItem Is Nothing Then AltCurrency = moneda_selected Else AltCurrency = frm.uce_moneda.SelectedItem.DataValue
                Dim header_datos_arr() As String = New String() {"", "", "", "", False}
                FIJA_CHK_VIS(frm, frm.chkPorDia, False)
                If frm.datosBusqueda_paquete IsNot Nothing Then
                    Dim ds As DataSet = obtenGenerales_paquetes.obten_general_Paquete_Description(frm.datosBusqueda_paquete)
                    FIJA_CHK_VIS(frm, frm.chkPorDia, True)
                    output = inicializar_controles_paquetes(ds, AltCurrency, frm, header_datos_arr)
                    FIJA_CMB(frm, frm.cmb_reservacion, New String() {"9"}, New String() {CapaPresentacion.Idiomas.get_str("str_355_paquetes")})
                End If

                If frm.datosBusqueda_vuelo IsNot Nothing Then
                    'output = inicializar_controles_vuelos(datosBusqueda_vuelo)
                    output = agrega_vuelo(frm.datosBusqueda_vuelo.ReservationId)
                    FIJA_CMB(frm, frm.cmb_reservacion, New String() {"23"}, New String() {CapaPresentacion.Idiomas.get_str("str_0301_vuelo")})
                End If

                If frm.datosBusqueda_auto IsNot Nothing Then
                    'output = inicializar_controles_autos(datosBusqueda_auto)
                    output = agrega_auto(frm.datosBusqueda_auto.IdReservation, AltCurrency)
                    FIJA_CMB(frm, frm.cmb_reservacion, New String() {"11"}, New String() {CapaPresentacion.Idiomas.get_str("str_302_auto")})
                End If

                If frm.datosBusqueda_hotel IsNot Nothing Then
                    'output = inicializar_controles_hotel(datosBusqueda_hotel)
                    output = agrega_hotel(frm, frm.datosBusqueda_hotel.ReservationId, AltCurrency, header_datos_arr)
                    FIJA_CMB(frm, frm.cmb_reservacion, New String() {"10"}, New String() {CapaPresentacion.Idiomas.get_str("str_0032_hotel")})
                End If

                If frm.datosBusqueda_actividad IsNot Nothing Then
                    'output = inicializar_controles_actividad(datosBusqueda_actividad)
                    output = agrega_actividad(frm.datosBusqueda_actividad.ReservationID)
                    FIJA_CMB(frm, frm.cmb_reservacion, New String() {"27"}, New String() {CapaPresentacion.Idiomas.get_str("str_0285_actividad")})
                End If

                FIJA_WB(frm, frm.WebBrowser1, output)
                FIJA_PIC(frm, frm.PictureBox2, False)
                FIJA_LBL(frm, frm.data_lbl_origen, header_datos_arr(0))
                FIJA_LBL(frm, frm.data_lbl_portal, header_datos_arr(1))
                FIJA_LBL(frm, frm.data_lbl_rubro, header_datos_arr(2))
                FIJA_LBL(frm, frm.data_lbl_estado, header_datos_arr(3))
                If frm.Imprimir Then

                    frm.Invoke(frm.TerminoImpr)
                End If

                '.Text = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("StatusTXT").Value
                '.Text = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("Source").Value
                '.Text = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("PortalName").Value
                '.Text = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("RubroTXT").Value

                'Cargamos el log

                If frm.datosBusqueda_hotel.ReservationId.Trim <> "" Or Not frm.datosBusqueda_hotel.ReservationId Is Nothing Then
                    Dim dataLog As webService_call = New webService_call()
                    Dim dsLogs As Call_Log_Get_RS = dataLog.LogGet(noReservation:=frm.datosBusqueda_hotel.ReservationId)
                    If dsLogs.Tables.Contains("Log") AndAlso dsLogs.Tables("Log").Rows.Count > 0 Then
                        With dsLogs.Tables("Log").Columns
                            .Add("OperationTXT")
                            .Add("DateOperationTXT")
                            .Add("DetailsTXT")
                        End With
                        For Each r As DataRow In dsLogs.Tables("Log").Rows
                            r.Item("OperationTXT") = herramientas.GetOperationText(r.Item("Operation"))
                            r.Item("DateOperationTXT") = herramientas.FechaWindows(r.Item("DateOperation")) 'CDate(r.Item("DateOperation")).ToString("dd/MMM/yyyy HH:mm")
                            r.Item("DetailsTXT") = "Ver detalles"
                        Next

                        FIJA_GRID(frm, frm.UltraGrid1, dsLogs.Tables("Log").DefaultView)
                        FIJA_BTN(frm, frm.btn_mostrar_bitacora, True)
                    End If

                End If

                'Activamos el enlace por si hay transacciones pendientes de envio
                If header_datos_arr(4) = True Then
                    FIJA_BTN1(frm, frm.btn_solicita_transacciones, True)
                End If

            Catch ex As Exception
                If ex.GetType IsNot GetType(System.Threading.ThreadAbortException) Then
                    CapaLogicaNegocios.ErrorManager.Manage("E0092", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End If
            End Try
        End Sub

        Private Shared Function inicializar_controles_paquetes(ByVal ds As DataSet, ByVal AltCurrency As String, ByVal frm As detalles, ByRef header_datos_arr() As String) As String
            If ds Is Nothing Then
                Return ""
            End If

            Dim output As String = "<font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"

            If ds.Tables("error") Is Nothing Then
                Dim r As DataRow = ds.Tables("Package").Rows(0)
                Dim str_ori As String = ""
                Dim str_por As String = ""
                Dim str_rub As String = CapaPresentacion.Idiomas.get_str("str_355_paquetes") 'str_355_paquetes

                If ds.Tables("Hotel").Rows.Count > 0 Then
                    str_ori = ds.Tables("Hotel").Rows(0).Item("source")
                    Select Case str_ori
                        Case "CCT"
                            header_datos_arr(0) = "Call Center"
                        Case Else
                            header_datos_arr(0) = str_ori
                    End Select
                    header_datos_arr(1) = ds.Tables("Hotel").Rows(0).Item("Portal")
                End If

                header_datos_arr(2) = str_rub

                output += CapaPresentacion.Idiomas.get_str("str_0270_numItin") + " " + r.Item("Itinerary") + "<br /><font color='#ff0000'>"
                Select Case r.Item("Status")
                    Case "1"
                        output += CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + CapaPresentacion.Idiomas.get_str("str_332_reservado") + "<br />"
                        header_datos_arr(3) = CapaPresentacion.Idiomas.get_str("str_332_reservado")
                    Case "3"
                        output += CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + CapaPresentacion.Idiomas.get_str("str_333_cancelado") + "<br />"
                        header_datos_arr(3) = CapaPresentacion.Idiomas.get_str("str_333_cancelado")
                    Case "2"
                        output += CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + CapaPresentacion.Idiomas.get_str("str_334_noConfirm") + "<br />"
                        header_datos_arr(3) = CapaPresentacion.Idiomas.get_str("str_334_noConfirm")
                    Case Else
                        output += CapaPresentacion.Idiomas.get_str("str_0044_estado")
                End Select
                output += "</font>"
                'output += agrega_item(r, "HotelReservationId", "HtlStatus", "Hotel")
                'Dim arrh As String() = r.Item("HotelReservationId").ToString.Split(",")
                'Dim header_datos_arr() As String = New String() {"", "", "", ""}
                'For x As Integer = 0 To arrh.Length - 1
                '    If arrh(x) <> "" Then
                '        'output += agrega_hotel(frm, arrh(x), AltCurrency, header_datos_arr)
                '        output += "<br /><a id='" & arrh(x) & "' href='blank?" & arrh(x) & "'>ID: " & arrh(x) & "</a><br />"
                '    End If

                'Next
                If r.Item("HotelReservationId") = "-1" Then
                    output &= CapaPresentacion.Idiomas.get_str("str_0137_nombreCliente") & " <strong>" & ds.Tables("Cliente").Rows(0).Item("viajero_Nombre") & "</strong><br />"
                    output &= CapaPresentacion.Idiomas.get_str("str_0138_correoCliente") & " <strong>" & ds.Tables("Cliente").Rows(0).Item("viajero_email") & "</strong><br />"
                    output &= CapaPresentacion.Idiomas.get_str("str_423_User") & " <strong>" & ds.Tables("Cliente").Rows(0).Item("nombre") & "(" & ds.Tables("Cliente").Rows(0).Item("email") & ")</strong></font><br />"

                    Dim dtDias As New DataTable
                    dtDias.Columns.Add("fecha")
                    dtDias.Columns.Add("tipo")
                    dtDias.Columns.Add("id")

                    Dim outputresumen As String = ""
                    For Each h As DataRow In ds.Tables("Hotel").Rows
                        Dim sCheckin As String = CDate(h("checkin")).ToString("dd/MMM/yyyy")
                        Dim sCheckout As String = CDate(h("checkout")).ToString("dd/MMM/yyyy")
                        dtDias.Rows.Add(h("checkin"), "0", h("idReservacion"))
                        dtDias.Rows.Add(h("checkout"), "1", h("idReservacion"))
                        If Not frm.chkPorDia.Checked Then
                            outputresumen += "<table width='100%' border='0' cellpadding='8' cellspacing='0' bgcolor='#F3F4FE'>"
                            outputresumen += "  <tr>"
                            outputresumen += "    <td align='left' valign='top' bgcolor='#D9ECFF'><font color='#6291E8' face='Verdana, Geneva, sans-serif'><strong>" & h("Hotel") & "</strong>(" & h("ciudad") & "," & h("pais") & ")</font></td>"
                            outputresumen += "    <td align='right' valign='top' bgcolor='#D9ECFF'>&nbsp;</td>"
                            outputresumen += "    <td colspan='2' align='center' valign='middle' bgcolor='#D9ECFF'>&nbsp;</td>"
                            outputresumen += "  </tr>"
                            outputresumen += "  <tr>"
                            outputresumen += "    <td width='31%' align='left' valign='top'>"
                            outputresumen += "    <font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"
                            outputresumen += "    <p><strong>" & CapaPresentacion.Idiomas.get_str("str_336_arrDate") & ":</strong> " & sCheckin & " <br /> <strong>" & CapaPresentacion.Idiomas.get_str("str_337_depDate") & ":</strong> " & sCheckout & "</p>"
                            outputresumen += "    </font>	"
                            outputresumen += "    </td>"
                            outputresumen += "    <td width='17%' align='right' valign='top'>"
                            outputresumen += "    <font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"
                            outputresumen += "    <p><strong>" & CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") & "</strong> " & h("HtlNoreservacion") & "</p>"
                            outputresumen += "    </font>"
                            outputresumen += "    </td>"
                            outputresumen += "    <td width='17%' align='right' valign='top'>"
                            outputresumen += "    <font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"
                            outputresumen += "    <p>"
                            Dim status As String = "<strong>"
                            Select Case h("HtlStatus")
                                Case "1"
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong><font color='#ff0000'> " & CapaPresentacion.Idiomas.get_str("str_332_reservado") + "<br />"
                                Case "3"
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong><font color='#ff0000'> " & CapaPresentacion.Idiomas.get_str("str_333_cancelado") + "<br />"
                                Case "2"
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong><font color='#ff0000'> " & CapaPresentacion.Idiomas.get_str("str_334_noConfirm") + "<br />"
                                Case Else
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong>"
                            End Select
                            outputresumen += status & "</font></p>"
                            outputresumen += "    </font>"
                            outputresumen += "    </td>"
                            outputresumen += "    <td width='33%' align='right' valign='middle'><font color='#222222' size='2' face='Verdana, Arial, Helvetica, sans-serif'><a id='" & h("idReservacion") & "' name='H' href='blank?" & h("idReservacion") & "'>" & CapaPresentacion.Idiomas.get_str("str_0019_detalles") & "</a></font></td>"
                            outputresumen += "  </tr>"
                            outputresumen += "</table>"
                        End If
                    Next
                    outputresumen = "<div id='resumen'>" & outputresumen & "</div>"
                    If frm.chkPorDia.Checked Then
                        Dim outputresumenpordia As String = ""
                        dtDias.DefaultView.Sort = "fecha asc"
                        Dim ultFecha As String = ""
                        Dim _outputresumenpordia As String = ""
                        For Each res As DataRow In dtDias.DefaultView.Table.Rows
                            Dim h As DataRow = ds.Tables("Hotel").Select("idReservacion=" & res("id"))(0)
                            Dim sFecha As String = CDate(res("fecha")).ToString("dd/MMM/yyyy")
                            Dim sCheckin As String = CDate(h("checkin")).ToString("dd/MMM/yyyy")
                            Dim sCheckout As String = CDate(h("checkout")).ToString("dd/MMM/yyyy")
                            If ultFecha <> sFecha Then
                                If ultFecha <> "" Then
                                    outputresumenpordia += "<table width='100%' border='0' cellpadding='8' cellspacing='0' >"       'bgcolor='#F3F4FE'>"
                                    outputresumenpordia += "  <tr>"
                                    outputresumenpordia += "  <td width='100%'>" & ultFecha
                                    outputresumenpordia += "  </td>"
                                    outputresumenpordia += "  </tr>"
                                    outputresumenpordia += "  <tr>"
                                    outputresumenpordia += "  <td width='100%'>" & _outputresumenpordia
                                    outputresumenpordia += "  </td>"
                                    outputresumenpordia += "  </tr>"
                                    outputresumenpordia += "</table>"
                                End If
                                ultFecha = sFecha
                                _outputresumenpordia = ""
                            End If
                            _outputresumenpordia += "<table width='100%' border='0' cellpadding='8' cellspacing='0' bgcolor='#F3F4FE'>"
                            _outputresumenpordia += "  <tr>"
                            _outputresumenpordia += "    <td align='left' valign='top' bgcolor='#D9ECFF'><font color='#6291E8' face='Verdana, Geneva, sans-serif'><strong>" & IIf(res("tipo") = "0", "Checkin: ", "Checkout: ") & h("Hotel") & "</strong>(" & h("ciudad") & "," & h("pais") & ")</font></td>"
                            _outputresumenpordia += "    <td align='right' valign='top' bgcolor='#D9ECFF'>&nbsp;</td>"
                            _outputresumenpordia += "    <td colspan='2' align='center' valign='middle' bgcolor='#D9ECFF'>&nbsp;</td>"
                            _outputresumenpordia += "  </tr>"
                            _outputresumenpordia += "  <tr>"
                            _outputresumenpordia += "    <td width='31%' align='left' valign='top'>"
                            _outputresumenpordia += "    <font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"
                            _outputresumenpordia += "    <p><strong>" & CapaPresentacion.Idiomas.get_str("str_336_arrDate") & ":</strong> " & sCheckin & " <br /> <strong>" & CapaPresentacion.Idiomas.get_str("str_337_depDate") & ":</strong> " & sCheckout & "</p>"
                            _outputresumenpordia += "    </font>	"
                            _outputresumenpordia += "    </td>"
                            _outputresumenpordia += "    <td width='17%' align='right' valign='top'>"
                            _outputresumenpordia += "    <font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"
                            _outputresumenpordia += "    <p><strong>" & CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") & "</strong> " & h("HtlNoreservacion") & "</p>"
                            _outputresumenpordia += "    </font>"
                            _outputresumenpordia += "    </td>"
                            _outputresumenpordia += "    <td width='17%' align='right' valign='top'>"
                            _outputresumenpordia += "    <font color='#222222' size='2' face='Trebuchet MS, Lucida Sans Unicode, Lucida Grande, sans-serif'>"
                            _outputresumenpordia += "    <p>"
                            Dim status As String = "<strong>"
                            Select Case h("HtlStatus")
                                Case "1"
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong><font color='#ff0000'> " & CapaPresentacion.Idiomas.get_str("str_332_reservado") + "<br />"
                                Case "3"
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong><font color='#ff0000'> " & CapaPresentacion.Idiomas.get_str("str_333_cancelado") + "<br />"
                                Case "2"
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong><font color='#ff0000'> " & CapaPresentacion.Idiomas.get_str("str_334_noConfirm") + "<br />"
                                Case Else
                                    status = CapaPresentacion.Idiomas.get_str("str_0044_estado") & "</strong>"
                            End Select
                            _outputresumenpordia += status & "</font></p>"
                            _outputresumenpordia += "    </font>"
                            _outputresumenpordia += "    </td>"
                            _outputresumenpordia += "    <td width='33%' align='right' valign='middle'><font color='#222222' size='2' face='Verdana, Arial, Helvetica, sans-serif'><a id='" & h("idReservacion") & "' name='H' href='blank?" & h("idReservacion") & "'>" & CapaPresentacion.Idiomas.get_str("str_0019_detalles") & "</a></font></td>"
                            _outputresumenpordia += "  </tr>"
                            _outputresumenpordia += "</table>"
                        Next
                        If ultFecha <> "" Then
                            outputresumenpordia += "<table width='100%' border='0' cellpadding='8' cellspacing='0' >"       'bgcolor='#F3F4FE'>"
                            outputresumenpordia += "  <tr>"
                            outputresumenpordia += "  <td width='100%'>" & ultFecha
                            outputresumenpordia += "  </td>"
                            outputresumenpordia += "  </tr>"
                            outputresumenpordia += "  <tr>"
                            outputresumenpordia += "  <td width='100%'>" & _outputresumenpordia
                            outputresumenpordia += "  </td>"
                            outputresumenpordia += "  </tr>"
                            outputresumenpordia += "</table>"
                        End If
                        outputresumenpordia = "<div id='resumenpordia'>" & outputresumenpordia & "</div>"
                        output &= outputresumenpordia
                    Else
                        output &= outputresumen
                    End If

                    'output &= "<input type='radio' name='g' onclick=""javascript:document.getElementById('resumen').style.display='block';document.getElementById('resumenpordia').style.display='none';"">General</input>"
                    'output &= "<input type='radio' name='g' onclick=""javascript:document.getElementById('resumen').style.display='none';document.getElementById('resumenpordia').style.display='block';"" >Por dia</input><br />"



                Else
                    Dim header_datos_arr2() As String = New String() {"", "", "", ""}
                    output += agrega_hotel(frm, r.Item("HotelReservationId"), AltCurrency, header_datos_arr2)
                End If

                If r.Table.Columns.Contains("CarReservationId") AndAlso Not r.IsNull("CarReservationId") Then output += agrega_auto(r.Item("CarReservationId"), AltCurrency) ' agrega_item(r, "CarReservationId", "CarStatus", "Auto")
                If r.Table.Columns.Contains("FlightReservationId") AndAlso Not r.IsNull("FlightReservationId") Then output += agrega_item(r, "FlightReservationId", "FlightStatus", "Vuelo")

                ''''''''actividades
                If r.Table.Columns.Contains("ActivityReservationId") AndAlso Not r.IsNull("ActivityReservationId") AndAlso r.Item("ActivityReservationId") = "1" Then
                    Dim datos_paq As New dataBusqueda_paquete
                    datos_paq.itinerario = ds.Tables("Package").Rows(0).Item("Itinerary")
                    Dim ds_actividades As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_ITR2(datos_paq)

                    For Each r_actividad As DataRow In ds_actividades.Tables("Reservation").Rows
                        output += agrega_item(r_actividad, "ReservationID", "Status", "Actividad")
                    Next
                End If

                'If DataGridView1.Rows.Count = 0 Then PropertyGrid1.SelectedObject = Nothing
            End If

            Return output
        End Function

        Private Shared Function agrega_item(ByVal r As DataRow, ByVal campo_id As String, ByVal campo_estado As String, ByVal rubro As String) As String
            Dim estado As String
            If r.Table.Columns.Contains(campo_id) AndAlso r.Table.Columns.Contains(campo_estado) AndAlso Not r.IsNull(campo_id) AndAlso Not r.IsNull(campo_estado) Then
                'Select Case r.Item(campo_estado)
                '    Case "1"
                '        estado = "Reservado"
                '    Case "3"
                '        estado = "-Cancelado"
                '    Case Else
                '        estado = ""
                'End Select

                estado = herramientas.GetSatatusText(r.Item(campo_estado))

                'DataGridView1.Rows.Add(r.Item(campo_id), rubro, estado)
            End If

            Return ""
        End Function

        Private Shared Function agrega_hotel(ByVal frm As detalles, ByVal ResId As String, ByVal AltCurrency As String, ByRef header_datos_arr() As String) As String
            Dim datosDisplay As New datos_display
            datosDisplay.ReservationId = ResId
            datosDisplay.AltCurrency = AltCurrency
            Dim ds_display As DataSet
            If ResId = "" Then
                datosDisplay.ConfirmNumber = frm.datosBusqueda_hotel.ConfirmNumber
                datosDisplay.ChainCode = frm.datosBusqueda_hotel.ChainCode
                datosDisplay.PropertyNumber = frm.datosBusqueda_hotel.PropertyNumber

                ds_display = CapaLogicaNegocios.GetWS.obten_hotel_display(datosDisplay)
            Else
                ds_display = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)
            End If
            frm.datosBusqueda_hotel.ConfirmNumber = ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber")
            If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Room") OrElse ds_display.Tables("Room").Rows.Count = 0 Then
                Return "Hotel----"
            End If

            Dim wsCall As New webService_call()

            Dim dc As Call_GetOne_RS = wsCall.get_CallReservation(ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber"))
            Dim _Empresa, _Medio, _Segmento, _MedioPromocion As String
            _Empresa = ""
            _Medio = ""
            _Segmento = ""
            _MedioPromocion = ""
            Dim infoLlamada As String = ""
            If Not IsNothing(dc) AndAlso dc.Reservation.Rows.Count > 0 Then
                dc = wsCall.get_CallReservation(ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber"), dc.Reservation(0).idCall)
            End If
            If Not IsNothing(dc) AndAlso dc.Reservation.Rows.Count > 0 Then

                frm.DisplayRsXML_Despues = ds_display.GetXml()

                Dim logEntry As New webService_call
                If frm.registraBitacora = detalles.BitacoraAccion.Insertar Then
                    logEntry.LogInsert(detalles.BitacoraAccion.Insertar, ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber"), "", "", "")
                ElseIf frm.registraBitacora = detalles.BitacoraAccion.Editar Then
                    logEntry.LogInsert(detalles.BitacoraAccion.Editar, ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber"), "", frm.DisplayRsXML_Antes, frm.DisplayRsXML_Despues)
                End If

                If Not IsDBNull(dc.Reservation.Rows(0).Item("id_empresa")) Then _Empresa = herramientas.get_Empresa(dc.Reservation.Rows(0).Item("id_empresa"))
                If Not IsDBNull(dc.Reservation.Rows(0).Item("id_empresa")) Then
                    Dim idSeg As String = herramientas.get_idSegmentoEmpresa(dc.Reservation.Rows(0).Item("id_empresa"))
                    If Not dc.Reservation.Rows(0).IsNull("id_segmento") Then idSeg = dc.Reservation.Rows(0).Item("id_segmento")
                    _Segmento = herramientas.get_Segmento(idSeg)
                End If

                If Not IsDBNull(dc.Reservation.Rows(0).Item("id_Medio")) Then _Medio = herramientas.get_Medio(dc.Reservation.Rows(0).Item("id_Medio"))
                If Not IsDBNull(dc.Reservation.Rows(0).Item("id_MedioPromocion")) Then _MedioPromocion = herramientas.get_MedioPromocion(dc.Reservation.Rows(0).Item("id_MedioPromocion"))
                If Not dc._Call(0).nombreCliente Is Nothing AndAlso dc._Call(0).nombreCliente.Trim <> "" Then
                    infoLlamada += "    <tr>"
                    infoLlamada += "        <td colspan='2'><span style='font-size:14px'><b><input id='chkInfo' type='checkbox' onclick=""javascript:document.getElementById('info').style.display=((document.getElementById('chkInfo').checked==true)?'block':'none');"" CHECKED/>" + CapaPresentacion.Idiomas.get_str("str_0205_infoContacto") + ":</b><br />"
                    infoLlamada &= "<div id='info'>" & CapaPresentacion.Idiomas.get_str("str_0168_nombre") & ": " & dc._Call(0).nombreCliente & "<br />"
                    infoLlamada &= CapaPresentacion.Idiomas.get_str("str_0148_email") & ": " & dc._Call(0).correo & "<br />"
                    infoLlamada &= CapaPresentacion.Idiomas.get_str("str_0118_telefono") & ": " & dc._Call(0).telefono & "<br />"
                    infoLlamada &= CapaPresentacion.Idiomas.get_str("str_0237_inicio") & ": " & dc._Call(0).inicio & "<br />"
                    infoLlamada &= CapaPresentacion.Idiomas.get_str("str_0238_termina") & ": " & dc._Call(0).fin & "<br />"
                    infoLlamada &= CapaPresentacion.Idiomas.get_str("str_0236_duracion") & ": " & DateDiff(DateInterval.Minute, dc._Call(0).inicio, dc._Call(0).fin) & " " & CapaPresentacion.Idiomas.get_str("str_375_minutos") & "<br />"
                    infoLlamada &= CapaPresentacion.Idiomas.get_str("str_0229_comentarios") & ": " & dc._Call(0).comentarios & "<hr /></div></span></td>"
                    infoLlamada += "    </tr>"
                    infoLlamada += "    <tr>"
                    infoLlamada += "    <td></td>"
                    infoLlamada += "    </tr>"
                End If
            End If

            ''--------------------------------
            'If herramientas.verifica_campos(ds_display, "Reservation", 0, New String() {"Provider", "CheckInDate", "CheckOutDate", "AccessCode", "RateCode", "PropertyNumber", "ChainCode", "AltMoney"}) Then
            '    frm.datosBusqueda_hotel.ServiceProvider = ds_display.Tables("Reservation").Rows(0).Item("Provider")
            '    frm.datosBusqueda_hotel.llegada = CapaPresentacion.Common.GetDateTime(ds_display.Tables("Reservation").Rows(0).Item("CheckInDate"))
            '    frm.datosBusqueda_hotel.salida = CapaPresentacion.Common.GetDateTime(ds_display.Tables("Reservation").Rows(0).Item("CheckOutDate"))
            '    frm.datosBusqueda_hotel.codProm = ds_display.Tables("Reservation").Rows(0).Item("AccessCode")
            '    frm.datosBusqueda_hotel.RateCode = ds_display.Tables("Reservation").Rows(0).Item("RateCode")
            '    frm.datosBusqueda_hotel.PropertyNumber = ds_display.Tables("Reservation").Rows(0).Item("PropertyNumber")
            '    frm.datosBusqueda_hotel.noches = (frm.datosBusqueda_hotel.salida - frm.datosBusqueda_hotel.llegada).Days.ToString
            '    frm.datosBusqueda_hotel.ChainCode = ds_display.Tables("Reservation").Rows(0).Item("ChainCode")
            '    frm.datosBusqueda_hotel.monedaBusqueda = ds_display.Tables("Reservation").Rows(0).Item("AltMoney")
            '
            '    Dim habs As New uc_habitaciones.T_Habitaciones
            '    'hab.TotalHabitaciones = ds_display.Tables("Room").Rows.Count
            '    Dim ListaHabitaciones As New List(Of uc_habitaciones.T_Habitacion)
            '    For Each r_Room As DataRow In ds_display.Tables("Room").Rows
            '        Dim hab As New uc_habitaciones.T_Habitacion
            '        hab.NumAdultos()
            '        hab.NumNinos()
            '        hab.EdadesNinos()
            '    Next
            '
            '    frm.datosBusqueda_hotel.habitaciones_ = uc_habitaciones.T_Habitaciones.
            '
            '    Dim ds_rules As DataSet = obtenGenerales_hoteles.obten_general_HOR(frm.datosBusqueda_hotel)
            '    CapaPresentacion.Hotels.HotelRules.FormatBrowser(Nothing, ds_rules)
            'End If
            ''--------------------------------

            Dim str_ori As String = ""
            Dim str_por As String = ""
            Dim str_rub As String = ""
            Dim str_est As String = ""
            If herramientas.verifica_campos(ds_display, "HotelHeader", 0, New String() {"Source"}) Then str_ori = ds_display.Tables("HotelHeader").Rows(0).Item("Source")
            If herramientas.verifica_campos(ds_display, "HotelHeader", 0, New String() {"IdPortal"}) Then str_por = ds_display.Tables("HotelHeader").Rows(0).Item("IdPortal")
            str_rub = CapaPresentacion.Idiomas.get_str("str_0032_hotel")
            If herramientas.verifica_campos(ds_display, "Reservation", 0, New String() {"Status"}) Then str_est = ds_display.Tables("Reservation").Rows(0).Item("Status")
            Select Case str_ori
                Case "CCT"
                    header_datos_arr(0) = "Call Center"
                Case Else
                    header_datos_arr(0) = str_ori
            End Select
            header_datos_arr(1) = str_por
            header_datos_arr(2) = str_rub
            Dim estado As String
            Select Case str_est 'ds_display.Tables("Reservation").Rows(0).Item("Status")
                Case "1"
                    estado = CapaPresentacion.Idiomas.get_str("str_332_reservado")
                    If ds_display.Tables("Reservation").Rows(0).Item("CreditCardNumber") <> "" Then
                        estado += "/" + CapaPresentacion.Idiomas.get_str("str_471_garantizada")
                    Else
                        estado += "/" + CapaPresentacion.Idiomas.get_str("str_472_nogarantizada")
                    End If
                Case "3"
                    estado = CapaPresentacion.Idiomas.get_str("str_333_cancelado")
                    If ds_display.Tables("Reservation").Rows(0).Item("CxWithError") = "True" Then
                        estado &= "(" & CapaPresentacion.Idiomas.get_str("str_425_penal2") & ")"
                    End If
                Case "2"
                    estado = CapaPresentacion.Idiomas.get_str("str_334_noConfirm")
                Case "4"
                    estado = CapaPresentacion.Idiomas.get_str("str_414_InProcess")
                Case Else
                    estado = ""
            End Select
            header_datos_arr(3) = estado
            header_datos_arr(4) = False
            If (dc.Tables("Reservation").Rows.Count > 0) Then
                header_datos_arr(4) = dc.Tables("Reservation").Rows(0)("PMS_OperacionesPendientes")
            End If

            Dim dbl_total As Double = CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTotal"))
            'Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Reservation").Rows(0).Item("AltMoney")
            Dim str_total As String = CapaPresentacion.Common.moneda.FormateaMoneda(dbl_total, ds_display.Tables("Reservation").Rows(0).Item("AltMoney"))
            Dim _h As String = " HH:mm"
            If ds_display.Tables("Reservation").Rows(0).Item("ReservationDate").ToString.Length = 10 Then _h = ""
            Dim fecha_res As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("ReservationDate"), "yyyy/MM/dd" & _h, Nothing).ToString("dd/MMM/yyyy" & _h)
            Dim fecha_in_ As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("CheckInDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            Dim fecha_out As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("CheckOutDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            If Not IsNothing(frm.datosBusqueda_hotel) Then frm.datosBusqueda_hotel.Status = ds_display.Tables("Reservation").Rows(0).Item("Status")
            Dim cont As Integer = 0
            Dim moneda As String = ds_display.Tables("Reservation").Rows(0).Item("AltMoney")

            Dim ciudadCl As String = ""
            If ds_display.Tables("Customer").Columns.Contains("City") Then
                ciudadCl = ds_display.Tables("Customer").Rows(0)("City")
            End If
            Dim estadoCl As String = ds_display.Tables("Customer").Rows(0)("Estate")

            Dim FormaPago As String
            FormaPago = CapaPresentacion.Idiomas.get_str("str_0108_ninguno")
            If CBool(ds_display.Tables("Reservation").Rows(0).Item("ReservedBySpecificPayment")) Then
                FormaPago = ds_display.Tables("Reservation").Rows(0).Item("PaymentInformation")
            ElseIf ds_display.Tables("Reservation").Rows(0).Item("DepositReference") <> "" Then
                FormaPago = CapaPresentacion.Idiomas.get_str("str_0107_depositoBanc")
            ElseIf ds_display.Tables("Reservation").Rows(0).Item("CreditCardNumber") <> "" Then
                FormaPago = CapaPresentacion.Idiomas.get_str("str_0052_tarjetaCredito")
            End If

            'Campo Source en el HTML
            Dim source As String = ds_display.Tables("HotelHeader").Rows(0)("Source").ToString()
            Dim SourceField As String = ""
            Select Case source
                Case "CCT"
                    SourceField = "Call Center"
                Case "POR"
                    SourceField = "Web Site"
                Case "IDS"
                    SourceField = "Distribucion por Internet"
                Case "WIZ"
                    SourceField = "GDS"
                Case "ADS"
                    SourceField = "ADS"
                Case "HTL"
                    SourceField = "Reservacion Pasiva"
            End Select


            Dim datos As String = ""
            datos += "<table width='600px'>"
            Dim impr As Boolean = True 'frm.Imprimir
            If infoLlamada.Trim <> "" Then datos += infoLlamada

            If impr AndAlso ds_display.Tables("Reservation").Columns.Contains("CommentByVendor") Then
                If ds_display.Tables("Reservation").Rows(0).Item("CommentByVendor") <> "" Then
                    datos += "    <tr>"
                    datos += "        <td colspan='2'><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_0229_comentarios") + ":</b> " & ds_display.Tables("Reservation").Rows(0).Item("CommentByVendor") & "</span></td>"
                    datos += "    </tr>"
                End If
            End If
            'agregar campo de Source
            If SourceField <> "" Then
                datos += "<tr>"
                datos += "     <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_436_can") + ":" + SourceField + "</b></td>"
                datos += "</tr>"
            End If
            If impr Then
                If _Segmento <> "" Then
                    datos += "    <tr>"
                    datos += "        <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_436_seg") + ": " + _Segmento + "</b></span></td>"
                    datos += "    </tr>"
                End If
                If _Empresa <> "" Then
                    datos += "    <tr>"
                    datos += "        <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_434_comp") + ": " + _Empresa + "</b></span></td>"
                    datos += "    </tr>"
                End If
                If _Medio <> "" Then
                    datos += "    <tr>"
                    datos += "        <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_435_med") + ": " + _Medio + "</b></span></td>"
                    datos += "    </tr>"
                End If
                If _MedioPromocion <> "" Then
                    datos += "    <tr>"
                    datos += "        <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_441_promo") + ": " + _MedioPromocion + "</b></span></td>"
                    datos += "    </tr>"
                End If
            End If
            datos += "    <tr>"
            datos += "        <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") + " " + ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber") + "</b></span></td>"
            datos += "        <td><span style='font-size:14px; align:right;'><b>" + CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + estado + "</span></td>"
            datos += "    </tr>"
            If ds_display.Tables("Reservation").Columns.Contains("PmsNoReservation") AndAlso ds_display.Tables("Reservation").Rows(0).Item("PmsNoReservation").ToString.Trim <> "" Then
                datos += "    <tr>"
                datos += "        <td><span style='font-size:14px'><b>PMS " + CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") + " " + ds_display.Tables("Reservation").Rows(0).Item("PmsNoReservation") + "</b></span></td>"
                datos += "        <td><span style='font-size:14px; align:right;'></td>"
                datos += "    </tr>"
            End If
            If str_est = 3 Then
                datos += "    <tr>"
                datos += "        <td colspan='2'><span style='font-size:14px; color:red;'><b>" + CapaPresentacion.Idiomas.get_str("str_306_numCan") + ": " + ds_display.Tables("Reservation").Rows(0).Item("CXNumber") + "</span><br /><span style='font-size:10px;'>" & CapaPresentacion.Idiomas.get_str("str_465") + ": " + ds_display.Tables("Reservation").Rows(0).Item("CancellationReason") + "<span></td>"
                datos += "    </tr>"
            End If
            datos += "    <tr>"
            datos += "        <td colspan='2'><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_0105_garantia") + ":</b> " + FormaPago + "</span>"
            If ds_display.Tables("Reservation").Rows(0).Item("DepositReference") <> "" Then
                'TODO descomentar para siguiente version
                If ds_display.Tables("Reservation").Columns.Contains("DepositLimitDate") Then
                    Dim dld As String = ds_display.Tables("Reservation").Rows(0).Item("DepositLimitDate")
                    Dim fecha As Date = CDate(dld.Substring(0, 4) & "/" & dld.Substring(4, 2) & "/" & dld.Substring(6, 2) & " " & ds_display.Tables("Reservation").Rows(0).Item("DepositLimitTime"))

                    datos += " (" + CapaPresentacion.Idiomas.get_str("str_0268_numeroReferencia") + ": " & ds_display.Tables("Reservation").Rows(0).Item("DepositReference") & ")</td>"
                    datos += "    <tr>"
                    datos += "        <td colspan='2' style='color:red;'><span style='font-size:14px'>&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Idiomas.get_str("str_415_LimitDate") + ": " & fecha.ToString("dd/MMM/yyyy HH:mm") & "</span></td>"
                    datos += "    </tr>"
                End If
                ' If frm.Imprimir = False Then
                'TODO: Mostrar depositamount y depositinfo
                If ds_display.Tables("Reservation").Columns.Contains("DepositLimitDate") = False Then
                    datos += "    <tr>"
                    datos += "        <td colspan='2' ><span style='font-size:14px'>&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Idiomas.get_str("str_426_amountinfo") + ": " & FormatCurrency(ds_display.Tables("Reservation").Rows(0).Item("DepositAmount"), 2).Replace("$", "").Replace(",", "") & "</span></td>"
                    datos += "    </tr>"
                End If
                datos += "    <tr>"
                datos += "        <td colspan='2' ><span style='font-size:14px'>&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Idiomas.get_str("str_326_infoDep") + ": " & ds_display.Tables("Reservation").Rows(0).Item("DepositInfo") & "</span></td>"
                datos += "    </tr>"
                ' End If
            Else
                datos += "    </td>"
                datos += " </tr>"
            End If

            datos += "    <tr>"
            datos += "        <td><span style='font-size:14px'><b>" + CapaPresentacion.Idiomas.get_str("str_423_User") + ": " + ds_display.Tables("Customer").Rows(0).Item("FirstName") + " " + ds_display.Tables("Customer").Rows(0).Item("LastName") + "</b></span></td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td></td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td><b>" + ds_display.Tables("Reservation").Rows(0).Item("HotelName") + "</b></td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + ds_display.Tables("Reservation").Rows(0).Item("HotelAddress") + "</td>"
            datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_336_arrDate") + ": " + fecha_in_ + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + ds_display.Tables("Reservation").Rows(0).Item("CityName") + ", " + ds_display.Tables("Reservation").Rows(0).Item("CountryName") + "</td>"
            datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_337_depDate") + ": " + fecha_out + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0048_telefono") + " " + ds_display.Tables("Reservation").Rows(0).Item("HotelPhoneNumber") + "</td>"
            datos += "        <td style='text-align: right;'>" + CapaPresentacion.Idiomas.get_str("str_335_resDate") + ": " + fecha_res + "</td>"
            datos += "    </tr>"


            datos += "    <tr>"
            datos += "        <td colspan='2'>" + CapaPresentacion.Idiomas.get_str("str_338_msgHotelPol") + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='600px'>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0137_nombreCliente") + " " + ds_display.Tables("Room").Rows(0).Item("TravelerName") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0138_correoCliente") + " " + ds_display.Tables("Room").Rows(0).Item("EmailCliente") + "</td>"
            datos += "    </tr>"
            If ds_display.Tables("Reservation").Rows(0).Item("frecuentCode") <> "" Then
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_439_frecuent") + ": " + ds_display.Tables("Reservation").Rows(0).Item("frecuentCode") + "</td>"
                datos += "    </tr>"
            End If
            datos += "    <tr>"
            datos += "        <td style='color:#b71201'><br />" + CapaPresentacion.Idiomas.get_str("str_339_resumen") + "<br /><br /></td>"
            datos += "    </tr>"
            For Each r As DataRow In ds_display.Tables("Room").Rows
                Dim str_tar_dias As String = ""
                Dim tar, ini, fin As String
                tar = ""
                ini = ""
                fin = ""
                For Each r_tar As DataRow In r.GetChildRows("Room_Rates")(0).GetChildRows("Rates_Rate")
                    Dim subT As Double = 0
                    subT += CDbl(r_tar.Item("AltRateAdult")) '* CDbl(r.Item("Adults"))
                    subT += CDbl(r_tar.Item("AltRateChild")) '* CDbl(r.Item("Children"))
                    subT += CDbl(r_tar.Item("AltExtraRateAdult"))       '* CDbl(r.Item("ExtraAdults"))
                    subT += CDbl(r_tar.Item("AltExtraRateChild"))       '* CDbl(r.Item("ExtraChildren"))

                    If tar <> CStr(subT) Then
                        If tar <> "" Then str_tar_dias += IIf(ini = fin, ini, ini & "-" & fin) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tar), moneda) + "<br />"
                        tar = subT
                        ini = DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM")
                        fin = ini
                    Else
                        fin = DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM")
                    End If
                    'str_tar_dias += DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Common.moneda.FormateaMoneda(subT, moneda) + "<br />"
                Next
                If tar <> "" Then str_tar_dias += IIf(ini = fin, ini, ini & "-" & fin) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tar), moneda) + "<br />"
                cont += 1
                Dim strCliente As String = ""
                strCliente += r.Item("TravelerName") + "<br />"
                strCliente += r.Item("EmailCliente") + "<br />"
                strCliente += CapaPresentacion.Idiomas.get_str("str_0048_telefono") + " " + r.Item("homephone") + "<br />"
                strCliente += CapaPresentacion.Idiomas.get_str("str_0118_telefono_adicional") + " " + r.Item("workhome") + "<br />"
                strCliente += CapaPresentacion.Idiomas.get_str("str_0051_pericion") + " " + r.Item("Preferences") + "<br />"
                strCliente += CapaPresentacion.Idiomas.get_str("str_525") + ":" + ciudadCl + ", " + estadoCl

                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + "</td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_341_tarNoche") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_340_resBajo") + "</td>"
                datos += "        <td>" + r.Item("Adults") + IIf(r.Item("ExtraAdults") = "0", "", "(+" & r.Item("ExtraAdults") & ")") + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + r.Item("Children") + IIf(r.Item("ExtraChildren") = "0", "", "(+" & r.Item("ExtraChildren") & ")") + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + ", " + r.Item("NameRoom") + ", " + r.Item("RatePlanName") + "</td>"
                datos += "        <td>" + str_tar_dias + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>" + strCliente + "</td>"
                datos += "        <td></td>"
                datos += "        <td></td>"
                datos += "    </tr>"
            Next
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='600px'>"
            datos += "    <tr>"
            'datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_0198_subtotal") + " " + Format(CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTotal")) - CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTaxes")), "c") + " " + moneda + "</td>"
            datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_0198_subtotal") + " " + CapaPresentacion.Common.moneda.FormateaMoneda((CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTotal")) - CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTaxes"))), moneda) + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            'datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + " " + Format(CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTaxes")), "c") + " " + moneda + "</td>"
            datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + " " + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTaxes")), moneda) + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            'datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_0145_total") + " " + Format(CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTotal")), "c") + " " + moneda + "</td>"
            datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_0145_total") + " " + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(ds_display.Tables("Reservation").Rows(0).Item("AltTotal")), moneda) + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += politicas_WS(ds_display)
            datos += "<br />"
            datos += "<hr>"
            'datos += ""
            'datos += "<br />"
            'datos += ""
            'datos += politicas_fijas()

            Return datos
        End Function

        Private Shared Function agrega_actividad(ByVal ResId As String) As String
            Dim datos_actividad As New dataBusqueda_actividad_test
            datos_actividad.ReservationID = ResId
            Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_DSR2(datos_actividad)
            If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Event") OrElse ds_display.Tables("Event").Rows.Count = 0 Then
                Return "Actividad----"
            End If

            'Dim datosDisplay As New datos_display
            'datosDisplay.ReservationId = ResId
            'Dim ds_display As DataSet = get_ws.obten_hotel_display_resID(datosDisplay)
            'If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Room") OrElse ds_display.Tables("Room").Rows.Count = 0 Then
            '    Return "Hotel----"
            'End If

            '--------------

            Dim dbl_total As Double = CDbl(ds_display.Tables("Reservation").Rows(0).Item("Total"))
            'Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")
            Dim str_total As String = CapaPresentacion.Common.moneda.FormateaMoneda(dbl_total, ds_display.Tables("Property").Rows(0).Item("CurrencyCode"))
            Dim str_fechR As String = CDate(ds_display.Tables("Reservation").Rows(0).Item("Date")).ToString("dd/MMM/yyyy")
            'Dim str_fechI As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).GetChildRows("Reservation_Event")(0).Item("StartDate"), "MM/dd/yyyy", Nothing).ToString("yyyy/MM/dd")
            'Dim str_fechO As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("EndDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MM/dd")
            Dim moneda As String = ""

            'Dim dbl_total As Double = CDbl(ds_display.Tables("Reservation").Rows(0).Item("Total"))
            'Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Reservation").Rows(0).Item("Money")
            'Dim fecha_res As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("ReservationDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            'Dim fecha_in_ As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("CheckInDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            'Dim fecha_out As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("CheckOutDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            Dim estado As String
            Select Case ds_display.Tables("Reservation").Rows(0).Item("Status")
                Case "1"
                    estado = CapaPresentacion.Idiomas.get_str("str_332_reservado")
                Case "3"
                    estado = CapaPresentacion.Idiomas.get_str("str_333_cancelado")
                Case "2"
                    estado = CapaPresentacion.Idiomas.get_str("str_334_noConfirm")
                Case Else
                    estado = ""
            End Select
            'Dim cont As Integer = 0
            'Dim moneda As String = ds_display.Tables("Reservation").Rows(0).Item("Money")

            '-------------------

            'Dim datos As New reservaciones_display(ds_display.Tables("Reservation").Rows(0).Item("ReservationNumber"), "", str_fechR, str_fechI, "", ds_display.Tables("CreditCard").Rows(0).Item("Name"), ds_display.Tables("Reservation").Rows(0).GetChildRows("Reservation_Event")(0).Item("City"), str_total, ds_display.Tables("Property").Rows(0).Item("PropertyName"))
            'PropertyGrid1.SelectedObject = datos

            Dim datos As String = ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td><b>" + ds_display.Tables("Property").Rows(0).Item("PropertyName") + "</b></td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td><b>" + CapaPresentacion.Idiomas.get_str("str_335_resDate") + ": " + str_fechR + "</b></td>"
            datos += "        <td style='color:red; text-align: right;'>" + CapaPresentacion.Idiomas.get_str("str_0145_total") + " " + str_total + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") + " " + ds_display.Tables("Reservation").Rows(0).Item("ReservationNumber") + "</td>"
            datos += "        <td style='color:red; text-align: right;'>" + CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + estado + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0061_cliente") + ": " + ds_display.Tables("Customer").Rows(0).Item("Name") + " " + ds_display.Tables("Customer").Rows(0).Item("LastName") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0050_correo") + " " + ds_display.Tables("Customer").Rows(0).Item("Email") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0048_telefono") + " " + ds_display.Tables("Customer").Rows(0).Item("Phone") + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='800px'>"
            For Each r_event As DataRow In ds_display.Tables("Event").Rows
                Dim str_price As String = ""
                Dim price As Double
                For Each r_price As DataRow In r_event.GetChildRows("Event_Price")
                    price = r_price.Item("PriceAmount") * r_price.Item("Quantity")
                    str_price += r_price.Item("Quantity") + " " + r_price.Item("PriceName") + ": " + CapaPresentacion.Common.moneda.FormateaMoneda(price, ds_display.Tables("Property").Rows(0).Item("CurrencyCode")) + "<br /><br />"
                Next

                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0285_actividad") + ": " + r_event.Item("ActivityName") + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0288_evento") + ": " + r_event.Item("EventName") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>" + r_event.Item("ActivityLocation") + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0063_fecha") + DateTime.ParseExact(r_event.Item("StartDate"), "MM/dd/yyyy", Nothing).ToString("dd/MMM/yyyy") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td></td>"
                datos += "        <td>" + str_price + "</td>"
                datos += "    </tr>"
            Next
            datos += "</table>"

            Return datos
        End Function

        Private Shared Function agrega_auto(ByVal ResId As String, ByVal currency As String) As String
            Dim datos_car As New dataBusqueda_auto_test
            datos_car.IdReservation = ResId
            datos_car.CUR = currency
            Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarReservation(datos_car)
            If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("OfficeStart") OrElse ds_display.Tables("OfficeStart").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Rate") OrElse ds_display.Tables("Rate").Rows.Count = 0 Then
                Return "Auto----"
            End If

            '--------------

            'Dim dbl_total As Double = CDbl(ds_display.Tables("Reservation").Rows(0).Item("Total"))
            'Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")
            'Dim str_fechR As String = CDate(ds_display.Tables("Reservation").Rows(0).Item("Date")).ToString("dd/MMM/yyyy")
            'Dim moneda As String = ""
            'Dim estado As String
            'Select Case ds_display.Tables("Reservation").Rows(0).Item("Status")
            '    Case "1"
            '        estado = CapaPresentacion.Idiomas.get_str("str_332_reservado")
            '    Case "3"
            '        estado = CapaPresentacion.Idiomas.get_str("str_333_cancelado")
            '    Case "2"
            '        estado = CapaPresentacion.Idiomas.get_str("str_334_noConfirm")
            '    Case Else
            '        estado = ""
            'End Select

            Dim total_day As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountDay")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountDayQty"))
            Dim total_wee As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountWeek")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountWeekQty"))
            Dim total_exD As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraDay")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraDayQty"))
            Dim total_exH As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraHour")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraHourQty"))
            Dim dbl_total As Double = total_day + total_wee + total_exD + total_exH
            'Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Reservation").Rows(0).Item("Currency")
            Dim str_total As String = CapaPresentacion.Common.moneda.FormateaMoneda(dbl_total, ds_display.Tables("Reservation").Rows(0).Item("Currency"))
            Dim str_fechR As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("ReserveDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MMM/dd")
            Dim str_fechI As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("StartDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MMM/dd")
            Dim str_fechO As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("EndDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MMM/dd")
            Dim estado As String
            Select Case ds_display.Tables("Reservation").Rows(0).Item("Status")
                Case "1"
                    estado = CapaPresentacion.Idiomas.get_str("str_332_reservado")
                Case "3"
                    estado = CapaPresentacion.Idiomas.get_str("str_333_cancelado")
                Case "2"
                    estado = CapaPresentacion.Idiomas.get_str("str_334_noConfirm")
                Case Else
                    estado = ""
            End Select

            '-------------------

            Dim datos As String = ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0016_agencia") + ": " + ds_display.Tables("Reservation").Rows(0).Item("VendorName") + "</td>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_336_arrDate") + ": " + str_fechI + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td></td>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_337_depDate") + ": " + str_fechI + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td style='color:red;'>" + CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") + " " + ds_display.Tables("Reservation").Rows(0).Item("ConfReservation") + "</td>"
            datos += "        <td style='color:red;'>" + CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + estado + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_362_conductor") + ": " + ds_display.Tables("Reservation").Rows(0).Item("DriverName") + "</td>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_363_ciudadR") + ": " + ds_display.Tables("OfficeStart").Rows(0).Item("CityStart") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td></td>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_363_ciudadR") + ": " + ds_display.Tables("OfficeEnd").Rows(0).Item("CityEnd") + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>" + ds_display.Tables("Car").Rows(0).Item("CarName") + " " + ds_display.Tables("Car").Rows(0).Item("CarClass") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + ds_display.Tables("Car").Rows(0).Item("Description") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0145_total") + " " + str_total + "</td>"
            datos += "    </tr>"
            datos += "</table>"

            Return datos
        End Function

        Private Shared Function agrega_vuelo(ByVal ResId As String) As String
            Dim datos_vuelo As New dataBusqueda_vuelo
            datos_vuelo.ReservationId = ResId
            Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_vuelo_AirReservationDetails(datos_vuelo)
            If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 Then
                Return "Vuelo----"
            End If

            '--------------

            'Dim dbl_total As Double '= CDbl(ds_display.Tables("ReservationFees").Rows(0).Item("GrandTotal"))
            'Dim str_total As String '= Format(dbl_total, "c") '+ " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")
            'Dim str_fechR As String '= CDate(ds_display.Tables("Reservation").Rows(0).Item("RegistrationDate")).ToString("yyyy/MM/dd")
            While ds_display.Tables("Reservation").Rows(0).Item("depHour").ToString.Length < 4
                ds_display.Tables("Reservation").Rows(0).Item("depHour") = "0" + ds_display.Tables("Reservation").Rows(0).Item("depHour")
            End While
            While ds_display.Tables("Reservation").Rows(0).Item("retHour").ToString.Length < 4
                ds_display.Tables("Reservation").Rows(0).Item("retHour") = "0" + ds_display.Tables("Reservation").Rows(0).Item("retHour")
            End While
            Dim str_fechI As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("depDate") + ds_display.Tables("Reservation").Rows(0).Item("depHour"), "yyyy/MM/ddHHmm", Nothing).ToString("dd/MMM/yyyy HH:mm")
            Dim str_fechO As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("retDate") + ds_display.Tables("Reservation").Rows(0).Item("retHour"), "yyyy/MM/ddHHmm", Nothing).ToString("dd/MMM/yyyy HH:mm")
            'Dim str_aero As String
            'Dim name As String '= ds_display.Tables("Passengers").Rows(0).Item("FName") + " " + ds_display.Tables("Passengers").Rows(0).Item("LName")
            'If ds_display.Tables.Contains("ReservationFees") AndAlso ds_display.Tables("ReservationFees").Rows.Count > 0 Then
            '    dbl_total = CDbl(ds_display.Tables("ReservationFees").Rows(0).Item("GrandTotal"))
            'End If
            'If ds_display.Tables.Contains("Passengers") AndAlso ds_display.Tables("Passengers").Rows.Count > 0 Then
            '    name = ds_display.Tables("Passengers").Rows(0).Item("FName") + " " + ds_display.Tables("Passengers").Rows(0).Item("LName")
            'End If
            'If ds_display.Tables.Contains("ReservationDetail") AndAlso ds_display.Tables("ReservationDetail").Rows.Count > 0 Then
            '    str_aero = ds_display.Tables("ReservationDetail").Rows(0).Item("airline")
            'End If
            'str_total = Format(dbl_total, "c") '+ " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")
            'str_fechR = CDate(ds_display.Tables("Reservation").Rows(0).Item("RegistrationDate")).ToString("yyyy/MM/dd")
            'str_fechI = ds_display.Tables("Reservation").Rows(0).Item("depDate")http://www.ascodevida.com/Varios/862823
            'If ds_display.Tables("Reservation").Columns.Contains("retDate") Then
            '    str_fechO = ds_display.Tables("Reservation").Rows(0).Item("retDate")
            'End If
            Dim str_tipo As String
            If ds_display.Tables("Reservation").Rows(0).Item("retDate") = "RT" Then
                str_tipo = CapaPresentacion.Idiomas.get_str("str_0252_redondo")
            Else
                str_tipo = CapaPresentacion.Idiomas.get_str("str_0253_sencillo")
            End If
            Dim str_estado As String
            Select Case ds_display.Tables("Reservation").Rows(0).Item("status")
                Case "1"
                    str_estado = CapaPresentacion.Idiomas.get_str("str_332_reservado")
                Case "3"
                    str_estado = CapaPresentacion.Idiomas.get_str("str_333_cancelado")
                Case "2"
                    str_estado = CapaPresentacion.Idiomas.get_str("str_334_noConfirm")
                Case Else
                    str_estado = ""
            End Select
            'Dim str_fees As String = Format(CDbl(ds_display.Tables("ReservationFees").Rows(0).Item("GrandTotal")), "c")
            Dim str_fees As String = CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(ds_display.Tables("ReservationFees").Rows(0).Item("GrandTotal")), "---")

            '--------------

            Dim datos As String = ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion") + " " + ds_display.Tables("Reservation").Rows(0).Item("RvaNumber") + "</td>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0100_tipo") + " " + str_tipo + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td style='color:red;'>" + CapaPresentacion.Idiomas.get_str("str_0044_estado") + " " + str_estado + "</td>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_373_escalas") + " " + ds_display.Tables("Reservation").Rows(0).Item("Escalas") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0129_total") + ": " + str_fees + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>" + ds_display.Tables("Reservation").Rows(0).Item("depCity") + ", " + ds_display.Tables("Reservation").Rows(0).Item("depCountry") + "</td>"
            datos += "        <td>" + ds_display.Tables("Reservation").Rows(0).Item("arrCity") + ", " + ds_display.Tables("Reservation").Rows(0).Item("arrCountry") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + ds_display.Tables("Reservation").Rows(0).Item("StartAirp") + "</td>"
            datos += "        <td>" + ds_display.Tables("Reservation").Rows(0).Item("EndAirp") + "</td>"
            datos += "    </tr>"
            datos += "    <tr>"
            datos += "        <td>" + str_fechI + "</td>"
            datos += "        <td>" + str_fechO + "</td>"
            datos += "    </tr>"
            datos += "</table>"
            datos += ""
            datos += "<br />"
            datos += ""
            For Each r_ReservationDetail As DataRow In ds_display.Tables("ReservationDetail").Rows
                Dim str_flightType As String = ""
                If r_ReservationDetail.Item("fltNum") = "RT" Then
                    str_flightType = CapaPresentacion.Idiomas.get_str("str_0252_redondo")
                Else
                    str_flightType = CapaPresentacion.Idiomas.get_str("str_0253_sencillo")
                End If
                Dim str_class As String = ""
                Select Case r_ReservationDetail.Item("class")
                    Case "Y"
                        str_class = CapaPresentacion.Idiomas.get_str("str_0256_economica")
                    Case "C"
                        str_class = CapaPresentacion.Idiomas.get_str("str_0257_negocios")
                    Case "F"
                        str_class = CapaPresentacion.Idiomas.get_str("str_0258_primeraClase")
                End Select
                Dim str_aerolinea As String = CapaLogicaNegocios.Vuelos.GetAirlines(r_ReservationDetail.Item("airline"))
                While r_ReservationDetail.Item("depTime").ToString.Length < 4
                    r_ReservationDetail.Item("depTime") = "0" + r_ReservationDetail.Item("depTime")
                End While
                While r_ReservationDetail.Item("arrTime").ToString.Length < 4
                    r_ReservationDetail.Item("arrTime") = "0" + r_ReservationDetail.Item("arrTime")
                End While
                Dim str_depDate As String = DateTime.ParseExact(r_ReservationDetail.Item("depDate").ToString.Split("T")(0) + r_ReservationDetail.Item("depTime"), "yyyy-MM-ddHHmm", Nothing).ToString("dd/MMM/yyyy HH:mm")
                Dim str_arrDate As String = DateTime.ParseExact(r_ReservationDetail.Item("arrDate").ToString.Split("T")(0) + r_ReservationDetail.Item("arrTime"), "yyyy-MM-ddHHmm", Nothing).ToString("dd/MMM/yyyy HH:mm")

                datos += "<table width='800px'>"
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0301_vuelo") + ": " + r_ReservationDetail.Item("fltNum") + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0231_tipo") + ": " + str_flightType + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0253_clase") + ": " + str_class + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0236_duracion") + ": " + r_ReservationDetail.Item("duration") + " " + CapaPresentacion.Idiomas.get_str("str_375_minutos") + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_376_equipo") + ": " + r_ReservationDetail.Item("equip") + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0250_aerolinea") + ": " + str_aerolinea + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_376_equipo") + ": " + r_ReservationDetail.Item("depCity") + "</td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_376_equipo") + ": " + r_ReservationDetail.Item("arrCity") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0146_fecha") + ": " + str_depDate + "</td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0146_fecha") + ": " + str_arrDate + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td></td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ": " + Format(CDbl(r_ReservationDetail.Item("fareAdult")), "c") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td></td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + ": " + Format(CDbl(r_ReservationDetail.Item("farechild")), "c") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td></td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0254_mayores") + ": " + Format(CDbl(r_ReservationDetail.Item("FareSenior")), "c") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td></td>"
                datos += "        <td></td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + " " + Format(CDbl(r_ReservationDetail.Item("tax")), "c") + "</td>"
                datos += "    </tr>"
                datos += "    <tr>"
                datos += "        <td>&nbsp;</td>"
                datos += "    </tr>"
                datos += "</table>"
            Next
            datos += ""
            datos += "<br />"
            datos += ""
            For Each r_Passengers As DataRow In ds_display.Tables("Passengers").Rows
                Dim str_isPrincipal As String = ""
                If r_Passengers.Item("isPrincipal") = "Y" Then str_isPrincipal = CapaPresentacion.Idiomas.get_str("str_377_principal") Else str_isPrincipal = ""
                datos += "<table width='800px'>"
                datos += "    <tr>"
                datos += "        <td>" + r_Passengers.Item("FName") + r_Passengers.Item("LName") + "</td>"
                datos += "        <td>" + str_isPrincipal + "</td>"
                datos += "    </tr>"
                datos += "</table>"
            Next

            Return datos
        End Function


        Public Shared Function pms_transaction_retry(ByVal noReservation As String) As Boolean
            Dim wsCall As New webService_call
            Dim success As Boolean
            If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_483_TransacctionMessage"), CapaPresentacion.Idiomas.get_str("str_485_TransacctionTitle"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                If wsCall.PMSRetry(noReservation) Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_484_TransacctionSent"), CapaPresentacion.Idiomas.get_str("str_485_TransacctionTitle"), MessageBoxButtons.OK, MessageBoxIcon.Information)
                    success = True
                Else
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_486_TransacctionFailed"), CapaPresentacion.Idiomas.get_str("str_485_TransacctionTitle"), MessageBoxButtons.OK, MessageBoxIcon.Information)
                    success = False
                End If
            End If

            Return success
        End Function

        Public Shared Function log_getLogDetail(ByVal idLog As String) As DataSet
            Dim wsCall As New webService_call
            Dim dsLog As Call_Log_Detail_RS = wsCall.LogDetail(CInt(idLog))

            Dim dsResBefore As New DataSet
            Dim dsResAfter As New DataSet

            Try

                dsResBefore.ReadXml(New System.IO.StringReader(dsLog.Log.Rows(0)("DataBefore").ToString()))
                dsResAfter.ReadXml(New System.IO.StringReader(dsLog.Log.Rows(0)("DataAfter").ToString()))
                dsResAfter.Merge(dsResBefore)
            Catch ex As Exception

            End Try

            Dim _h As String = " HH:mm"
            If dsResBefore.Tables("Reservation").Rows(0).Item("ReservationDate").ToString.Length = 10 Then _h = ""
            Dim fecha_res As String = DateTime.ParseExact(dsResBefore.Tables("Reservation").Rows(0).Item("ReservationDate"), "yyyy/MM/dd" & _h, Nothing).ToString("dd/MMM/yyyy" & _h)
            Dim fecha_in_ As String = DateTime.ParseExact(dsResBefore.Tables("Reservation").Rows(0).Item("CheckInDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            Dim fecha_out As String = DateTime.ParseExact(dsResBefore.Tables("Reservation").Rows(0).Item("CheckOutDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")

            If dsResAfter.Tables("Reservation").Rows(0).Item("ReservationDate").ToString.Length = 10 Then _h = ""
            Dim fecha_res_2 As String = DateTime.ParseExact(dsResAfter.Tables("Reservation").Rows(0).Item("ReservationDate"), "yyyy/MM/dd" & _h, Nothing).ToString("dd/MMM/yyyy" & _h)
            Dim fecha_in_2 As String = DateTime.ParseExact(dsResAfter.Tables("Reservation").Rows(0).Item("CheckInDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
            Dim fecha_out_2 As String = DateTime.ParseExact(dsResAfter.Tables("Reservation").Rows(0).Item("CheckOutDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")

            Dim strHabitaciones As String = ""
            Dim strTarifas As String = ""
            Dim strCliente As String = ""

            Dim moneda As String = dsResBefore.Tables("Reservation").Rows(0).Item("AltMoney")
            Dim cont As Integer = 0
            For Each r As DataRow In dsResBefore.Tables("Room").Rows
                Dim str_tar_dias As String = ""
                Dim tar, ini, fin As String
                tar = ""
                ini = ""
                fin = ""
                For Each r_tar As DataRow In r.GetChildRows("Room_Rates")(0).GetChildRows("Rates_Rate")
                    Dim subT As Double = 0
                    subT += CDbl(r_tar.Item("AltRateAdult")) '* CDbl(r.Item("Adults"))
                    subT += CDbl(r_tar.Item("AltRateChild")) '* CDbl(r.Item("Children"))
                    subT += CDbl(r_tar.Item("AltExtraRateAdult"))       '* CDbl(r.Item("ExtraAdults"))
                    subT += CDbl(r_tar.Item("AltExtraRateChild"))       '* CDbl(r.Item("ExtraChildren"))

                    If tar <> CStr(subT) Then
                        If tar <> "" Then str_tar_dias += IIf(ini = fin, ini, ini & "-" & fin) + vbTab + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tar), moneda) + vbCrLf
                        tar = subT
                        ini = DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM")
                        fin = ini
                    Else
                        fin = DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM")
                    End If
                    'str_tar_dias += DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Common.moneda.FormateaMoneda(subT, moneda) + "<br />"
                Next
                If tar <> "" Then str_tar_dias += IIf(ini = fin, ini, ini & "-" & fin) + vbTab + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tar), moneda) + vbCrLf
                cont += 1

                If strHabitaciones <> "" Then strHabitaciones += vbCrLf
                strHabitaciones += CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + vbCrLf
                strHabitaciones += r.Item("NameRoom") + ", " + r.Item("RatePlanName") + vbCrLf
                strHabitaciones += r.Item("Adults") + IIf(r.Item("ExtraAdults") = "0", "", "(+" & r.Item("ExtraAdults") & ")") + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + r.Item("Children") + IIf(r.Item("ExtraChildren") = "0", "", "(+" & r.Item("ExtraChildren") & ")") + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + vbCrLf

                If strTarifas <> "" Then strTarifas += vbCrLf
                strTarifas += CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + vbCrLf
                strTarifas += str_tar_dias + vbCrLf

                If strCliente <> "" Then strCliente += vbCrLf
                strCliente += CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + vbCrLf
                strCliente += CapaPresentacion.Idiomas.get_str("str_0147_nombreCliente") + ": " + r.Item("TravelerName") + vbCrLf
                strCliente += CapaPresentacion.Idiomas.get_str("str_0148_email") + ": " + r.Item("EmailCliente") + vbCrLf
                strCliente += CapaPresentacion.Idiomas.get_str("str_0048_telefono") + " " + r.Item("homephone") + vbCrLf
                strCliente += CapaPresentacion.Idiomas.get_str("str_0118_telefono_adicional") + " " + r.Item("workhome") + vbCrLf
                strCliente += CapaPresentacion.Idiomas.get_str("str_0051_pericion") + " " + r.Item("Preferences") + vbCrLf

            Next

            Dim strHabitaciones2 As String = ""
            Dim strTarifas2 As String = ""
            Dim strCliente2 As String = ""

            Dim moneda2 As String = dsResAfter.Tables("Reservation").Rows(0).Item("AltMoney")
            Dim cont2 As Integer = 0
            For Each r As DataRow In dsResAfter.Tables("Room").Rows
                Dim str_tar_dias As String = ""
                Dim tar, ini, fin As String
                tar = ""
                ini = ""
                fin = ""
                For Each r_tar As DataRow In r.GetChildRows("Room_Rates")(0).GetChildRows("Rates_Rate")
                    Dim subT As Double = 0
                    subT += CDbl(r_tar.Item("AltRateAdult")) '* CDbl(r.Item("Adults"))
                    subT += CDbl(r_tar.Item("AltRateChild")) '* CDbl(r.Item("Children"))
                    subT += CDbl(r_tar.Item("AltExtraRateAdult"))       '* CDbl(r.Item("ExtraAdults"))
                    subT += CDbl(r_tar.Item("AltExtraRateChild"))       '* CDbl(r.Item("ExtraChildren"))

                    If tar <> CStr(subT) Then
                        If tar <> "" Then str_tar_dias += IIf(ini = fin, ini, ini & "-" & fin) + vbTab + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tar), moneda2) + vbCrLf
                        tar = subT
                        ini = DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM")
                        fin = ini
                    Else
                        fin = DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM")
                    End If
                    'str_tar_dias += DateTime.ParseExact(r_tar.Item("Date"), "yyyy/MM/dd", Nothing).ToString("dd/MMM") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + CapaPresentacion.Common.moneda.FormateaMoneda(subT, moneda) + "<br />"
                Next
                If tar <> "" Then str_tar_dias += IIf(ini = fin, ini, ini & "-" & fin) + vbTab + CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(tar), moneda2) + vbCrLf
                cont2 += 1

                If strHabitaciones2 <> "" Then strHabitaciones2 += vbCrLf
                strHabitaciones2 += CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont2) + vbCrLf
                strHabitaciones2 += r.Item("NameRoom") + ", " + r.Item("RatePlanName") + vbCrLf
                strHabitaciones2 += r.Item("Adults") + IIf(r.Item("ExtraAdults") = "0", "", "(+" & r.Item("ExtraAdults") & ")") + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + r.Item("Children") + IIf(r.Item("ExtraChildren") = "0", "", "(+" & r.Item("ExtraChildren") & ")") + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + vbCrLf

                If strTarifas2 <> "" Then strTarifas2 += vbCrLf
                strTarifas2 += CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont2) + vbCrLf
                strTarifas2 += str_tar_dias + vbCrLf

                If strCliente2 <> "" Then strCliente2 += vbCrLf
                strCliente2 += CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont2) + vbCrLf
                strCliente2 += CapaPresentacion.Idiomas.get_str("str_0147_nombreCliente") + ": " + r.Item("TravelerName") + vbCrLf
                strCliente2 += CapaPresentacion.Idiomas.get_str("str_0148_email") + ": " + r.Item("EmailCliente") + vbCrLf
                strCliente2 += CapaPresentacion.Idiomas.get_str("str_0048_telefono") + " " + r.Item("homephone") + vbCrLf
                strCliente2 += CapaPresentacion.Idiomas.get_str("str_0118_telefono_adicional") + " " + r.Item("workhome") + vbCrLf
                strCliente2 += CapaPresentacion.Idiomas.get_str("str_0051_pericion") + " " + r.Item("Preferences") + vbCrLf

            Next


            'Subtotales
            Dim strSubtotal As String = CapaPresentacion.Common.moneda.FormateaMoneda((CDbl(dsResBefore.Tables("Reservation").Rows(0).Item("AltTotal")) - CDbl(dsResBefore.Tables("Reservation").Rows(0).Item("AltTaxes"))), moneda)
            Dim strTax As String = CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(dsResBefore.Tables("Reservation").Rows(0).Item("AltTaxes")), moneda)
            Dim strTotal As String = CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(dsResBefore.Tables("Reservation").Rows(0).Item("AltTotal")), moneda)

            Dim strSubtotal2 As String = CapaPresentacion.Common.moneda.FormateaMoneda((CDbl(dsResAfter.Tables("Reservation").Rows(0).Item("AltTotal")) - CDbl(dsResAfter.Tables("Reservation").Rows(0).Item("AltTaxes"))), moneda2)
            Dim strTax2 As String = CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(dsResAfter.Tables("Reservation").Rows(0).Item("AltTaxes")), moneda2)
            Dim strTotal2 As String = CapaPresentacion.Common.moneda.FormateaMoneda(CDbl(dsResAfter.Tables("Reservation").Rows(0).Item("AltTotal")), moneda2)

            'Construimos una tabla con las columnas que cambiaron
            Dim dsChangesDetails As New DataSet()
            Dim tbChanges As New DataTable()
            tbChanges.Columns.Add(New DataColumn("id", System.Type.GetType("System.Int32")))
            tbChanges.Columns.Add("DataColumn")
            tbChanges.Columns.Add("DataValueBefore")
            tbChanges.Columns.Add("DataValueAfter")

            Dim diff As DataColumn = tbChanges.Columns.Add("diff")
            diff.DataType = System.Type.GetType("System.Int32")
            diff.DefaultValue = 0

            Dim pKey(1) As DataColumn
            pKey(0) = tbChanges.Columns("id")
            tbChanges.PrimaryKey = pKey
            dsChangesDetails.Tables.Add(tbChanges)


            tbChanges.Rows.Add(New Object() {0, CapaPresentacion.Idiomas.get_str("str_0032_hotel"), dsResBefore.Tables("Reservation").Rows(0).Item("HotelName"), dsResAfter.Tables("Reservation").Rows(0).Item("HotelName")}) 'Hotel Name
            tbChanges.Rows.Add(New Object() {1, CapaPresentacion.Idiomas.get_str("str_0004_ciudad"), dsResBefore.Tables("Reservation").Rows(0).Item("CityName") + ", " + dsResBefore.Tables("Reservation").Rows(0).Item("CountryName"), dsResAfter.Tables("Reservation").Rows(0).Item("CityName") + ", " + dsResAfter.Tables("Reservation").Rows(0).Item("CountryName")}) 'City

            tbChanges.Rows.Add(New Object() {2, CapaPresentacion.Idiomas.get_str("str_0135_numConfirmacion").Replace(":", ""), dsResBefore.Tables("Reservation").Rows(0).Item("ConfirmNumber"), dsResAfter.Tables("Reservation").Rows(0).Item("ConfirmNumber")}) 'Num de Confirmacion

            tbChanges.Rows.Add(New Object() {3, CapaPresentacion.Idiomas.get_str("str_336_arrDate"), fecha_in_, fecha_in_2}) 'checkin
            tbChanges.Rows.Add(New Object() {4, CapaPresentacion.Idiomas.get_str("str_337_depDate"), fecha_out, fecha_out_2}) 'checkout
            tbChanges.Rows.Add(New Object() {5, CapaPresentacion.Idiomas.get_str("str_335_resDate"), fecha_res, fecha_res_2}) 'reservation date

            tbChanges.Rows.Add(New Object() {6, CapaPresentacion.Idiomas.get_str("str_0008_habitaciones"), strHabitaciones, strHabitaciones2}) 'Habitaciones
            tbChanges.Rows.Add(New Object() {7, CapaPresentacion.Idiomas.get_str("str_0036_tarifas"), strTarifas, strTarifas2}) 'Tarifas
            tbChanges.Rows.Add(New Object() {8, CapaPresentacion.Idiomas.get_str("str_0147_nombreCliente"), strCliente, strCliente2}) 'Viajero

            tbChanges.Rows.Add(New Object() {9, CapaPresentacion.Idiomas.get_str("str_0198_subtotal").Replace(":", ""), strSubtotal, strSubtotal2}) 'Subtotal
            tbChanges.Rows.Add(New Object() {10, CapaPresentacion.Idiomas.get_str("str_0144_impuestos").Replace(":", ""), strTax, strTax2}) 'Impuestos
            tbChanges.Rows.Add(New Object() {11, CapaPresentacion.Idiomas.get_str("str_0145_total").Replace(":", ""), strTotal, strTotal2}) 'Total

            For Each row As DataRow In tbChanges.Rows
                If row("DataValueBefore") <> row("DataValueAfter") Then
                    row("diff") = 1
                End If

            Next

            tbChanges.AcceptChanges()

            Return dsChangesDetails
        End Function

        Delegate Sub FIJA_WB_Callback(ByVal frm As detalles, ByVal web As WebBrowser, ByVal contenido As String)
        Public Shared Sub FIJA_WB(ByVal frm As detalles, ByVal web As WebBrowser, ByVal contenido As String)
            Try
                If web.InvokeRequired Then
                    Dim d As New FIJA_WB_Callback(AddressOf FIJA_WB)
                    frm.Invoke(d, New Object() {frm, web, contenido})
                Else

                    Dim cad As String = CONST_WB_TOP + contenido + CONST_WB_BOTTOM
                    'cad = "<html><body>test</body></html>"

                    web.DocumentText = cad
                    'web.Navigate("file:///H:/test.html")


                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_PIC_Callback(ByVal frm As detalles, ByVal pic As PictureBox, ByVal vis As Boolean)
        Public Shared Sub FIJA_PIC(ByVal frm As detalles, ByVal pic As PictureBox, ByVal vis As Boolean)
            Try
                If pic.InvokeRequired Then
                    Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                    frm.Invoke(d, New Object() {frm, pic, vis})
                Else
                    pic.Visible = vis
                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_LBL_Callback(ByVal frm As detalles, ByVal lbl As Label, ByVal tex As String)
        Public Shared Sub FIJA_LBL(ByVal frm As detalles, ByVal lbl As Label, ByVal tex As String)
            Try
                If lbl.InvokeRequired Then
                    Dim d As New FIJA_LBL_Callback(AddressOf FIJA_LBL)
                    frm.Invoke(d, New Object() {frm, lbl, tex})
                Else
                    lbl.Text = tex
                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_CMB_Callback(ByVal frm As detalles, ByVal cmb As Infragistics.Win.UltraWinEditors.UltraComboEditor, ByVal values() As String, ByVal titles() As String)
        Public Shared Sub FIJA_CMB(ByVal frm As detalles, ByVal cmb As Infragistics.Win.UltraWinEditors.UltraComboEditor, ByVal values() As String, ByVal titles() As String)
            Try
                If cmb.InvokeRequired Then
                    Dim d As New FIJA_CMB_Callback(AddressOf FIJA_CMB)
                    frm.Invoke(d, New Object() {frm, cmb, values, titles})
                Else
                    cmb.Items.Clear()
                    For i As Integer = 0 To values.Length - 1
                        cmb.Items.Add(values(i), titles(i))
                    Next
                    cmb.SelectedIndex = 0
                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_CHK_Callback(ByVal frm As detalles, ByVal cmb As CheckBox, ByVal vis As Boolean)
        Public Shared Sub FIJA_CHK_VIS(ByVal frm As detalles, ByVal cmb As CheckBox, ByVal vis As Boolean)
            Try
                If cmb.InvokeRequired Then
                    Dim d As New FIJA_CHK_Callback(AddressOf FIJA_CHK_VIS)
                    frm.Invoke(d, New Object() {frm, cmb, vis})
                Else
                    cmb.Visible = vis
                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_BTN_Callback(ByVal frm As detalles, ByVal btn As LinkLabel, ByVal vis As Boolean)
        Public Shared Sub FIJA_BTN(ByVal frm As detalles, ByVal btn As LinkLabel, ByVal vis As Boolean)
            Try
                If btn.InvokeRequired Then
                    Dim d As New FIJA_BTN_Callback(AddressOf FIJA_BTN)
                    frm.Invoke(d, New Object() {frm, btn, vis})
                Else
                    btn.Enabled = vis
                    If (btn.Enabled = True) Then
                        frm.btn_mostrar_bitacora.Text = CapaPresentacion.Idiomas.get_str("str_481_logLabelShow")
                    Else
                        frm.btn_mostrar_bitacora.Text = CapaPresentacion.Idiomas.get_str("str_482_logSin")
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_BTN1_Callback(ByVal frm As detalles, ByVal btn As LinkLabel, ByVal vis As Boolean)
        Public Shared Sub FIJA_BTN1(ByVal frm As detalles, ByVal btn As LinkLabel, ByVal vis As Boolean)
            Try
                If btn.InvokeRequired Then
                    Dim d As New FIJA_BTN1_Callback(AddressOf FIJA_BTN1)
                    frm.Invoke(d, New Object() {frm, btn, vis})
                Else
                    btn.Enabled = vis
                    If (btn.Enabled = True) Then
                        frm.btn_solicita_transacciones.Text = CapaPresentacion.Idiomas.get_str("str_487_TransacctionCon")
                    Else
                        frm.btn_solicita_transacciones.Text = CapaPresentacion.Idiomas.get_str("str_487_TransacctionSin")
                    End If
                End If
            Catch ex As Exception
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub


        Delegate Sub FIJA_GRID_Callback(ByVal frm As detalles, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
        Private Shared Sub FIJA_GRID(ByVal frm As detalles, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            Try
                If grid.InvokeRequired Then
                    Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                    frm.Invoke(d, New Object() {frm, grid, dv})
                Else
                    formatea_grid(grid, dv)
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Private Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            grid.DataSource = dv

            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In grid.DisplayLayout.Bands(0).Columns
                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next
        End Sub

        Private Shared Function politicas_WS(ByVal ds As DataSet) As String
            Dim datos As String = ""

            datos += "<table width='800px'>"
            datos += "    <tr>"
            datos += "        <td>"
            If ds.Tables("Reservation").Rows(0).Item("IsNetRateUV") Then
                datos += "              <p>" + CapaPresentacion.Idiomas.get_str("str_442_netrates") + "</p>"
            Else
                datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_0195_cancelPol") + "</b></p>"
                datos += "              <p>" + ds.Tables("Reservation").Rows(0).Item("CancelationPolicies") + "</p>"
            End If

            'datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_0197_ccPol") + "</b></p>"
            'datos += "              <p>" + ds.Tables("Reservation").Rows(0).Item("CreditCardPolicies") + "</p>"
            'datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_0196_garantPol") + "</b></p>"
            'datos += "              <p>" + ds.Tables("Reservation").Rows(0).Item("GuaranteePolicies") + "</p>"
            'datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_412_PolExtra") + "</b></p>"
            'datos += "              <p>" + ds.Tables("Reservation").Rows(0).Item("ExtraChargesPolicies") + "</p>"

            datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_0143_descripcionTar") + "</b></p>"
            datos += "              <p>" + ds.Tables("Reservation").Rows(0).Item("RatePlanName") + "</p>"
            datos += "              <p>" + ds.Tables("Reservation").Rows(0).Item("RatePlanDescription") + "</p>"
            datos += "        </td>"
            datos += "    </tr>"
            datos += "</table>"

            Return datos

            'Try
            '    Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)

            '    If ds.Tables.Count = 0 Then
            '        Return ""
            '    End If

            '    If Not ds.Tables("error") Is Nothing Then
            '        Return ""
            '    Else
            '        Dim r As DataRow = ds.Tables("Property").Rows(0)
            '        Dim str As String = ""

            '        str += "<table width='800px'>"
            '        str += "    <tr>"
            '        str += "        <td>"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_0032_hotel") + "</b>: " + r.Item("HotelName") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_321_polCan") + "</b> <br />"
            '        str += r.Item("CancelationPoliticies") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_322_polGar") + "</b> <br />"
            '        str += r.Item("GuarantyPoliticies") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_323_polCC") + "</b> <br />"
            '        str += r.Item("CreditCardPoliticies") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_324_carExtra") + "</b> <br />"
            '        str += r.Item("ExtraCharges") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_325_edadMaxima") + "</b> <br />"
            '        str += r.Item("MaxAgeOfChildren") + " A�os <br /><br />"

            '        str += CapaPresentacion.Idiomas.get_str("str_0150_llegada") + ": " + r.Item("Checkin").ToString.Substring(0, 2) + ":" + r.Item("Checkin").ToString.Substring(2, 2) + "<br />"
            '        str += CapaPresentacion.Idiomas.get_str("str_0151_salida") + ": " + r.Item("Checkout").ToString.Substring(0, 2) + ":" + r.Item("Checkout").ToString.Substring(2, 2) + "<br /><br />"

            '        str += CapaPresentacion.Idiomas.get_str("str_0144_impuestos") + " " + r.Item("Taxes") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_0117_direccion") + "</b> <br />"
            '        str += r.Item("Address") + "<br />"
            '        str += CapaPresentacion.Idiomas.get_str("str_0045_cp") + " " + r.Item("PostalCode") + "<br />"
            '        str += r.Item("City") + ", " + r.Item("State") + ", " + r.Item("Country") + "<br />"

            '        str += CapaPresentacion.Idiomas.get_str("str_0251_moneda") + ": " + r.Item("Money") + "<br /><br />"

            '        str += "<b>" + CapaPresentacion.Idiomas.get_str("str_326_infoDep") + "</b> <br />"
            '        str += r.Item("DepositInfo") + "<br /><br />"

            '       str += "        </td>"
            '       str += "    </tr>"
            '       str += "</table>"

            '        Return str
            '    End If
            'Catch ex As Exception
            '    Return ""
            'End Try
        End Function

    End Class

    Public Class Reports
        Public Shared Sub busca_reservaciones_produccion(ByVal param As Object)
            Dim last_msg As String = ""

           
            Dim ini As DateTime = param(0)
            Dim fin As DateTime = param(1)
            Dim origen As String = param(2)
            Dim nombre_agente As String = param(3)
            Dim nombre_hotel As String = param(4)
            Dim segmento As String = param(5)
            Dim agrupado_agente As Boolean = CBool(param(6))
            Dim id_Empresa As Integer = param(7)
            Dim frm As ProductionReport = CType(param(8), ProductionReport)

            Dim corporativo_id As Integer = CInt(dataOperador.idCorporativo)

            If origen = "-1" Then origen = ""
            If segmento = "" Then segmento = "-1"

            Try
                Dim dv As DataView
                Dim wsCall As New webService_call
                Dim ds As DataSet = wsCall.ReportProduction(ini, fin, segmento, "", corporativo_id, origen, "", nombre_agente, nombre_hotel, agrupado_agente, id_Empresa)
                'Dim ds As DataSet = wsCall.ReportProduction(Now.AddDays(-10), Now, 1, "", 1, "", "", "", "", True)
                If ds IsNot Nothing AndAlso ds.Tables.Contains("Response") Then

                    Dim tbl As DataTable = New DataTable("TypedResponse")
                    tbl.Columns.Add(New DataColumn("idHotel", GetType(System.String)))
                    tbl.Columns.Add(New DataColumn("Hotel", GetType(System.String)))
                    tbl.Columns.Add(New DataColumn("Agent", GetType(System.String)))
                    tbl.Columns.Add(New DataColumn("AgentEmail", GetType(System.String)))
                    tbl.Columns.Add(New DataColumn("SegmentCompany", GetType(System.String)))
                    tbl.Columns.Add(New DataColumn("Segment", GetType(System.String)))
                    tbl.Columns.Add(New DataColumn("Reservations", GetType(System.Int32)))
                    tbl.Columns.Add(New DataColumn("Rooms", GetType(System.Int32)))
                    tbl.Columns.Add(New DataColumn("Nights", GetType(System.Int32)))
                    tbl.Columns.Add(New DataColumn("RoomsByNight", GetType(System.Int32)))
                    tbl.Columns.Add(New DataColumn("Persons", GetType(System.Int32)))
                    tbl.Columns.Add(New DataColumn("Adults", GetType(System.Int32)))
                    tbl.Columns.Add(New DataColumn("Childs", GetType(System.Int32)))

                    For Each r As DataRow In ds.Tables("Response").Rows
                        tbl.LoadDataRow(r.ItemArray(), True)
                    Next

                    dv = tbl.DefaultView
                Else
                    dv = Nothing
                End If
                FIJA_GRID(frm, frm.UltraGrid1, dv)
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0110", ex.Message + " /////////////////////// en el punto: " + last_msg, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                FIJA_GRID(frm, frm.UltraGrid1, Nothing)
            End Try

            FIJA_PIC(frm, frm.PictureBox2, False)
        End Sub

        Public Shared Sub llena_combos(ByVal frm As ProductionReport)
            Dim idx As Integer

            'Combobox de seleccion de origen de reservacion
            idx = frm.uce_source.SelectedIndex
            frm.uce_source.Items.Clear()
            frm.uce_source.Items.Add("-1", CapaPresentacion.Idiomas.get_str("str_354_todos"))
            frm.uce_source.Items.Add("CCT", "CCT- Call Center")
            frm.uce_source.Items.Add("POR", "POR - Portales")
            frm.uce_source.Items.Add("UNI", "UNI - Unipantalla")
            frm.uce_source.Items.Add("WIZ", "WIZ - Globalizadores GDS")
            frm.uce_source.Items.Add("ADS", "ADS")
            frm.uce_source.Items.Add("IDS", "IDS")

            frm.uce_source.SelectedIndex = idx
            If frm.uce_source.SelectedIndex = -1 Then frm.uce_source.SelectedIndex = 0

            'Combobox segmentos
            idx = frm.cmbSegmento.SelectedIndex
            frm.cmbSegmento.Items.Clear()
            frm.cmbSegmento.DisplayMember = "Name"
            frm.cmbSegmento.ValueMember = "_Value"

            frm.cmbSegmento.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_354_todos"), ""))
            For Each dr As DataRow In herramientas.get_Segmentos(dataOperador.idCorporativo).Tables(0).Rows
                frm.cmbSegmento.Items.Add(New ListData(dr("Nombre"), dr("id_Segmento")))
            Next
            frm.cmbSegmento.SelectedIndex = idx
            If frm.cmbSegmento.SelectedIndex = -1 Then frm.cmbSegmento.SelectedIndex = 0

        End Sub

        Private Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            grid.DataSource = dv

            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In grid.DisplayLayout.Bands(0).Columns
                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next
        End Sub


        Delegate Sub FIJA_GRID_Callback(ByVal frm As ProductionReport, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
        Private Shared Sub FIJA_GRID(ByVal frm As ProductionReport, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            Try
                If grid.InvokeRequired Then
                    Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                    frm.Invoke(d, New Object() {frm, grid, dv})
                Else
                    formatea_grid(grid, dv)
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_PIC_Callback(ByVal frm As ProductionReport, ByVal pic As PictureBox, ByVal vis As Boolean)
        Private Shared Sub FIJA_PIC(ByVal frm As ProductionReport, ByVal pic As PictureBox, ByVal vis As Boolean)
            Try
                If pic.InvokeRequired Then
                    Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                    frm.Invoke(d, New Object() {frm, pic, vis})
                Else
                    pic.Visible = vis
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

    End Class

    Public Class SegmentsCompany
        Public Shared Sub SegmentsCompanyList(ByVal param As Object)
            Dim last_msg As String = ""
            Dim Nombre As String = CType(param(0), String)
            Dim Segmento As String = CType(param(1), String)
            Dim frm As SegmentCompanyList = CType(param(2), SegmentCompanyList)
            Try
                Dim ws As New webService_call
                Dim ds As Call_SegmentsCompany_List_RS = ws.SegmentCompanyList(Nombre, segmento, dataOperador.idCorporativo)
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then FIJA_GRID(frm, frm.UltraGrid1, ds.Tables(0).DefaultView)
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0110", ex.Message + " /////////////////////// en el punto: " + last_msg, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                FIJA_GRID(frm, frm.UltraGrid1, Nothing)
            End Try

            FIJA_PIC(frm, frm.PictureBox2, False)
        End Sub
        Private Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            grid.DataSource = dv

            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In grid.DisplayLayout.Bands(0).Columns
                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next
            'Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
            'System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(CapaPresentacion.Idiomas._cultura)

            'If grid.DisplayLayout.Bands.Count > 0 AndAlso grid.DisplayLayout.Bands(0).Columns.Count > 0 Then
            '    grid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
            '    With grid.DisplayLayout.Bands(0)
            '        .Columns("Code").Header.Caption = rm.GetString("str_497")
            '        .Columns("Name").Header.Caption = rm.GetString("str_0168_nombre")
            '        .Columns("Segment").Header.Caption = rm.GetString("str_490")

            '        .Columns("idCompany").Hidden = True
            '        .Columns("idSegment").Hidden = True
            '    End With
            'End If
        End Sub


        Delegate Sub FIJA_GRID_Callback(ByVal frm As SegmentCompanyList, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
        Private Shared Sub FIJA_GRID(ByVal frm As SegmentCompanyList, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal dv As DataView)
            Try
                If grid.InvokeRequired Then
                    Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                    frm.Invoke(d, New Object() {frm, grid, dv})
                Else
                    formatea_grid(grid, dv)
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

        Delegate Sub FIJA_PIC_Callback(ByVal frm As SegmentCompanyList, ByVal pic As PictureBox, ByVal vis As Boolean)
        Private Shared Sub FIJA_PIC(ByVal frm As SegmentCompanyList, ByVal pic As PictureBox, ByVal vis As Boolean)
            Try
                If pic.InvokeRequired Then
                    Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                    frm.Invoke(d, New Object() {frm, pic, vis})
                Else
                    pic.Visible = vis
                End If
            Catch ex As Exception
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End Sub

    End Class
End Class