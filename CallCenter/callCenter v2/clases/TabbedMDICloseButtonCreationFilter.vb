Imports System.Drawing

Class TabbedMDICloseButtonCreationFilter
    Implements Infragistics.Win.IUIElementCreationFilter

    Private mdiManager As Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager

    Public Sub New(ByVal manager As Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager)
        Me.mdiManager = manager
    End Sub

    Private Sub SwapChildElementsVertically(ByVal pe As Infragistics.Win.UIElement, ByVal startIndex As Integer)
        Dim e1 As Infragistics.Win.UIElement = pe.ChildElements(startIndex)
        Dim e2 As Infragistics.Win.UIElement = pe.ChildElements(startIndex + 1)

        If TypeOf e2 Is Infragistics.Win.ImageAndTextUIElement.ImageAndTextDependentTextUIElement Then
            Dim r1 As Rectangle, r2 As Rectangle

            Select Case Me.mdiManager.TabGroupSettings.TabOrientation
                Case Infragistics.Win.UltraWinTabs.TabOrientation.[Default], Infragistics.Win.UltraWinTabs.TabOrientation.TopLeft, Infragistics.Win.UltraWinTabs.TabOrientation.TopRight, Infragistics.Win.UltraWinTabs.TabOrientation.BottomLeft, Infragistics.Win.UltraWinTabs.TabOrientation.BottomRight
                    r1 = New Rectangle(e1.Rect.X, e2.Rect.Y, e2.Rect.Width, e2.Rect.Height)
                    r2 = New Rectangle(e1.Rect.X + e2.Rect.Width, e1.Rect.Y, e1.Rect.Width, e1.Rect.Height)
                    e1.Rect = r2
                    e2.Rect = r1
                    pe.ChildElements.Reverse(startIndex, 2)
                    Exit Select
                Case Infragistics.Win.UltraWinTabs.TabOrientation.LeftTop, Infragistics.Win.UltraWinTabs.TabOrientation.LeftBottom
                    r2 = New Rectangle(e1.Rect.X, e2.Rect.Y, e1.Rect.Width, e1.Rect.Height)
                    r1 = New Rectangle(e2.Rect.X, e2.Rect.Y + e1.Rect.Height, e2.Rect.Width, e2.Rect.Height)
                    e1.Rect = r2
                    e2.Rect = r1
                    pe.ChildElements.Reverse(startIndex, 2)
                    Exit Select
                Case Infragistics.Win.UltraWinTabs.TabOrientation.RightTop, Infragistics.Win.UltraWinTabs.TabOrientation.RightBottom
                    r1 = New Rectangle(e2.Rect.X, e1.Rect.Y, e2.Rect.Width, e2.Rect.Height)
                    r2 = New Rectangle(e1.Rect.X, e1.Rect.Y + e2.Rect.Height, e1.Rect.Width, e1.Rect.Height)
                    e1.Rect = r2
                    e2.Rect = r1
                    pe.ChildElements.Reverse(startIndex, 2)
                    Exit Select
            End Select
        End If
    End Sub

    Public Sub AfterCreateChildElements1(ByVal parent As Infragistics.Win.UIElement) Implements Infragistics.Win.IUIElementCreationFilter.AfterCreateChildElements
        If TypeOf parent Is Infragistics.Win.ImageAndTextUIElement.ImageAndTextDependentImageUIElement Then
            Dim imageElement As Infragistics.Win.ImageAndTextUIElement.ImageAndTextDependentImageUIElement = CType(parent, Infragistics.Win.ImageAndTextUIElement.ImageAndTextDependentImageUIElement)
            Dim parentElement As Infragistics.Win.UIElement = imageElement.Parent

            'replace image element with button element
            Dim buttonElement As New NewMdiTabCloseButton(parentElement)
            buttonElement.Rect = imageElement.Rect
            parentElement.ChildElements.Remove(imageElement)
            parentElement.ChildElements.Insert(0, buttonElement)

            'swap the locations of the button element and text element.
            If parentElement.ChildElements.Count >= 2 Then
                SwapChildElementsVertically(parentElement, 0)
            End If
        End If
    End Sub

    Public Function BeforeCreateChildElements1(ByVal parent As Infragistics.Win.UIElement) As Boolean Implements Infragistics.Win.IUIElementCreationFilter.BeforeCreateChildElements
        Return False
    End Function
End Class

'New close button UIElement which will close the clicked tab instead of the active tab
Class NewMdiTabCloseButton
    Inherits Infragistics.Win.UltraWinTabs.TabCloseButtonUIElement
    Public Sub New(ByVal parent As Infragistics.Win.UIElement)
        MyBase.New(parent)
    End Sub

    Protected Overloads Overrides Sub OnClick()
        Dim tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab = DirectCast(Me.GetContext(GetType(Infragistics.Win.UltraWinTabbedMdi.MdiTab)), Infragistics.Win.UltraWinTabbedMdi.MdiTab)
        If tab.IsFormActive Then
            MyBase.OnClick()
        Else
            tab.Close()
        End If
    End Sub
End Class
