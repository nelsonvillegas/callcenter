Public Class obtenGenerales_hoteles

    Public Shared Function obten_general_display(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
        Dim data As New datos_display()
        data.ChainCode = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("ChainCode").Value
        data.ConfirmNumber = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("noConfirm").Value
        data.PropertyNumber = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("PropertyNumber").Value

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_display(data)
        Return ds
    End Function

    Public Shared Function obten_general_HOI(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
        'validaciones
        'Dim buscarPor As String
        'If datosBusqueda_hotel.porCiudad Then buscarPor = "C" Else buscarPor = "A"
        'validaciones

        Dim data As New datos_HOI
        data.Code = datosBusqueda_hotel.ciudad_codigo
        data.StartDate = datosBusqueda_hotel.llegada.ToString(CapaPresentacion.Common.DateDataFormat)
        data.EndDate = datosBusqueda_hotel.salida.ToString(CapaPresentacion.Common.DateDataFormat)
        data.Nights = datosBusqueda_hotel.noches
        data.edades_Children = datosBusqueda_hotel.ninosEdades
        data.AltCurrency = datosBusqueda_hotel.monedaBusqueda
        data.HotelName = datosBusqueda_hotel.hotel_nombre.Trim

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_HOI(data, datosBusqueda_hotel)
        Return ds
    End Function

    Public Shared Function obten_general_HOR(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
        If dataOperador.Hoteles.Count > 0 And datosBusqueda_hotel.Segmento = "" Then
            Return Hor_Pasivas(datosBusqueda_hotel)
            Exit Function
        End If
        Dim data As New datos_HOR
        data.ServiceProvider = datosBusqueda_hotel.ServiceProvider
        data.CheckinDate = Format(datosBusqueda_hotel.llegada, CapaPresentacion.Common.DateDataFormat)
        data.CheckoutDate = Format(datosBusqueda_hotel.salida, CapaPresentacion.Common.DateDataFormat)
        data.AccessCode = datosBusqueda_hotel.codProm
        data.ReferenceContract = datosBusqueda_hotel.Convenio
        data.RateCode = datosBusqueda_hotel.RateCode
        data.PropertyNumber = CInt(datosBusqueda_hotel.PropertyNumber)
        data.Nights = datosBusqueda_hotel.noches
        data.ChainCode = datosBusqueda_hotel.ChainCode
        data.AltCurrency = datosBusqueda_hotel.monedaBusqueda

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_HOR(data, datosBusqueda_hotel)
        Return ds
    End Function

    Private Shared Function Hor_Pasivas(ByVal datos As dataBusqueda_hotel_test) As DataSet
        Dim ds As New Hotel_HOR_RS
        Dim dr1 As Hotel_HOR_RS.HotelRulesRow = ds.HotelRules.AddHotelRulesRow
        Dim dr2 As Hotel_HOR_RS._PropertyRow = ds._Property.New_PropertyRow
        dr2.HotelRulesRow = dr1
        dr2.PropertyNumber = datos.PropertyNumber
        dr2.HotelName = datos.PropertyName
        dr2.AllowBankDeposit = "Y"
        dr2.AllowBankDepositCCT = "Y"
        dr2.AllowSpecificPayment = "Y"
        dr2.GuarantyPoliticies = ""
        dr2.DepositInfo = ""
        dr2.AltDepositAmount = datos.TotalAvgRate
        dr2.AltExtraCharges = 0
        dr2.AltMoney = datos.AltCurrency
        dr2.AltTaxes = datos.AltTax '0
        dr2.AltTotalExtras = 0
        dr2.AltTotalRate = datos.AltTotalStay
        dr2.DepositAmount = datos.TotalAvgRate
        dr2.ExtraCharges = 0
        dr2.Money = datos.AltCurrency
        dr2.Taxes = 0
        dr2.TotalExtras = 0
        dr2.TotalRate = datos.AltTotalStay
        dr2.PlusTax = datos.PlusTax 'False
        ds._Property.Rows.Add(dr2)
        Dim dr3 As Hotel_HOR_RS.RoomsRow = ds.Rooms.NewRoomsRow
        dr3.HotelRulesRow = dr1
        ds.Rooms.AddRoomsRow(dr3)
        For Each h As uc_habitaciones.T_Habitacion In datos.habitaciones_.ListaHabitaciones
            Dim dr4 As Hotel_HOR_RS.RoomRow = ds.Room.NewRow
            dr4.RoomsRow = dr3
            dr4.NameRoom = datos.nombreHabitacion
            dr4.OnRequest = True
            dr4.DaysFree = 0
            dr4.isUvNetRate = False
            ds.Room.AddRoomRow(dr4)

            Dim dr5 As Hotel_HOR_RS.RTRoomsRow = ds.RTRooms.NewRTRoomsRow
            dr5.RoomRow = dr4
            ds.RTRooms.AddRTRoomsRow(dr5)

            Dim dr6 As Hotel_HOR_RS.RTRoomRow = ds.RTRoom.NewRTRoomRow
            dr6.RTRoomsRow = dr5
            dr6.Adults = h.NumAdultos
            dr6.Children = h.NumNinos
            ds.RTRoom.AddRTRoomRow(dr6)
        Next
        ds.Tables.Remove("Error")
        Return ds

    End Function

    Public Shared Function obten_general_modify(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
        'validaciones
        If datosBusqueda_hotel.llegada >= datosBusqueda_hotel.salida Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0156_errFechas"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return Nothing
        End If
        'validaciones
        Dim arr_cua_adul() As Integer = herramientas.get_arrCuartos(datosBusqueda_hotel.habitaciones_.TotalHabitaciones, datosBusqueda_hotel.habitaciones_.TotalAdultos)
        Dim arr_cua_chil() As Integer = herramientas.get_arrCuartos(datosBusqueda_hotel.habitaciones_.TotalHabitaciones, datosBusqueda_hotel.habitaciones_.TotalNinos)
        Dim arr_Cua_chAg() As String = herramientas.get_arrEdades(datosBusqueda_hotel.habitaciones_.TotalHabitaciones, datosBusqueda_hotel.habitaciones_.EdadesNinos, arr_cua_chil, datosBusqueda_hotel.habitaciones_.TotalNinos)

        Dim data As New datos_modify
        data.ServiceProvider = datosBusqueda_hotel.ServiceProvider
        data.Source = datosBusqueda_hotel.Source
        'Siempre que se hace una modificacion desde callcenter el source debe de establecerse como "WI"
        'data.Source = "WI"
        data.CheckInDate = datosBusqueda_hotel.llegada.ToString(CapaPresentacion.Common.DateDataFormat)
        data.CheckOutDate = datosBusqueda_hotel.salida.ToString(CapaPresentacion.Common.DateDataFormat)
        data.AccessCode = datosBusqueda_hotel.codProm
        data.ReferenceContract = datosBusqueda_hotel.Convenio
        data.PropertyNumber = datosBusqueda_hotel.PropertyNumber
        data.ChainCode = datosBusqueda_hotel.ChainCode
        data.ConfirmNumber = datosBusqueda_hotel.ConfirmNumber
        data.RateCode = misForms.reservar.txt_codigoTarifa.Text
        data.GuarDep = datosBusqueda_hotel.GuarDep
        data.Preferences = misForms.reservar.txt_peticion.Text
        data.OnRequest = "false"
        If datosBusqueda_hotel.OnRequest Then
            data.OnRequest = "true"
            data.Total = CDec(datosBusqueda_hotel.AltTotalStay) + CDec(datosBusqueda_hotel.AltTax)
            data.Currency = datosBusqueda_hotel.AltCurrency
            data.Tax = datosBusqueda_hotel.Tax
        End If

        data.PaymentInformation = ""
        'data.frecuentCode = datosBusqueda_hotel.frecuentCode

        Dim dep As enumDepositType
        If misForms.reservar.rbt_tarjetaCredito.Checked Then
            Dep = enumDepositType.CreditCard
        ElseIf misForms.reservar.rbt_depositoBancario.Checked Then
            dep = enumDepositType.BankDeposit
        ElseIf misForms.reservar.rbt_ninguno.Checked Then
            dep = enumDepositType.None
        ElseIf misForms.reservar.rbt_otra.Checked Then
            dep = enumDepositType.Other
            data.PaymentInformation = misForms.reservar.txtOtra.Text
        End If
        data.DepositType = dep


        data.CommentByVendor = misForms.reservar.txt_comentario.Text

        data.habitaciones = datosBusqueda_hotel.habitaciones_.TotalHabitaciones
        data.Adults = arr_cua_adul
        data.Children = arr_cua_chil
        data.ChildAges = arr_Cua_chAg
        With misForms.newCall.datos_call.reservacion_paquete
            data.FirstName = .cli_nombre
            data.LastName = .cli_apellido
            data.Address = .cli_direccion
            data.Country = .cli_pais
            data.City = .cli_ciudad
            data.PostalCode = .cli_cp
            data.Email = .cli_email
            data.City = .cli_ciudad
            data.Estate = .cli_estado
						data.PhoneHome = .cli_telefono_AreaCityCode + "+" + .cli_telefono_PhoneNumber
						data.PhoneWork = .cli_telefono_adicional_AreaCityCode + "+" + .cli_telefono_adicional_PhoneNumber
            'TODO:?? Bloqueado por reglas de operacion, no se permite cambiar datos del pago
            data.CodigoReferenciaCCT = .codigo_referencia

            If datosBusqueda_hotel.DepositType = enumDepositType.CreditCard Then
                data.CreditCardExpiration = .cc_fechaExpira
                data.CreditCardHolder = .cc_nombre_ & " " & .cc_apellido
                data.CreditCardNumber = .cc_numero
                data.CreditCardNumberVerify = .cc_numSeguridad
                data.CreditCardType = .cc_tipo
            End If
            If datosBusqueda_hotel.DepositType = enumDepositType.BankDeposit Then

                data.ReserveByBankDeposit = "Y"
                data.DepositLimitDate = misForms.reservar.dtp_DepFecha.Value.ToString("yyyyMMdd")
                data.DepositLimitTime = misForms.reservar.dtp_DepHora.Value.ToString("HH:mm")
                data.DepositAmountCCT = misForms.reservar.txtDeposito.Text
                data.DepositInfoCCT = misForms.reservar.txtInfoDeposito.Text
            End If
        End With


        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_modify(data, True)
        Return ds
    End Function

    Public Shared Function obten_general_cancel(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test, ByVal motivo As String) As DataSet
        Dim data As New datos_cancel
        data.ChainCode = datosBusqueda_hotel.ChainCode
        data.ConfirmNumber = datosBusqueda_hotel.ConfirmNumber
        data.PropertyNumber = datosBusqueda_hotel.PropertyNumber
        data.ServiceProvider = datosBusqueda_hotel.ServiceProvider

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_cancel(data, motivo)
        Return ds
    End Function

    Public Shared Function obten_general_HOC(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
        Dim data As New datos_HOC
        data.AltCurrency = datosBusqueda_hotel.monedaBusqueda
        data.ServiceProvider = datosBusqueda_hotel.ServiceProvider
        data.CheckinDate = Format(datosBusqueda_hotel.llegada, CapaPresentacion.Common.DateDataFormat)
        data.CheckoutDate = Format(datosBusqueda_hotel.salida, CapaPresentacion.Common.DateDataFormat)
        data.AccessCode = datosBusqueda_hotel.codProm
        data.ReferenceContract = datosBusqueda_hotel.Convenio
        data.Segment.Add("P")
        data.Segment.Add("R")
        data.Segment.Add("C")
        data.Segment.Add("K")
        data.Segment.Add("O") 'Convenios
        data.PropertyNumber = datosBusqueda_hotel.PropertyNumber

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_HOC(data, datosBusqueda_hotel)
        Return ds
    End Function

    Public Shared Function obten_general_HOD(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test) As DataSet
        Dim data As New datos_HOD
        data.ServiceProvider = datosBusqueda_hotel.ServiceProvider
        data.PropertyNumber = datosBusqueda_hotel.PropertyNumber
        data.HotelFilter.Add("DESC")
        data.HotelFilter.Add("LOCA")
        'data.HotelFilter.add("SERV")
        data.HotelFilter.Add("ROOM")
        'data.HotelFilter.add("GUAR")
        'data.HotelFilter.add("CRED")
        'data.HotelFilter.add("CANC")
        data.HotelFilter.Add("HIMG")
        'data.HotelFilter.add("RECR")
        'data.HotelFilter.add("MEAL")
        'data.HotelFilter.add("IMGH")
        'data.HotelFilter.add("TRVL")
        'data.HotelFilter.add("MEET")

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_HOD(data)
        Return ds
    End Function

    Public Shared Function obten_general_ConfirmReservation(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test, ByVal paymentAuthorization As String) As DataSet

        Dim total As Decimal
        If datosBusqueda_hotel.PlusTax.ToLower = "true" Then
            total = Convert.ToDecimal(datosBusqueda_hotel.AltTotalStay)
        Else
            total = (Convert.ToDecimal(datosBusqueda_hotel.AltTotalStay) + Convert.ToDecimal(datosBusqueda_hotel.AltTax))
        End If
        Dim res As DataSet
        If datosBusqueda_hotel.isOnlinePayment.ToLower = "true" Or datosBusqueda_hotel.isPayUPayment.ToLower = "true" Then
            res = CapaLogicaNegocios.GetWS.obten_hotel_ConfirmReservation(datosBusqueda_hotel.ConfirmNumber, CapaPresentacion.Idiomas.cultura, datosBusqueda_hotel.AltCurrency, paymentAuthorization, total)
        Else
            res = CapaLogicaNegocios.GetWS.obten_hotel_ConfirmPayPalReservation(datosBusqueda_hotel.ConfirmNumber, CapaPresentacion.Idiomas.cultura, datosBusqueda_hotel.AltCurrency, paymentAuthorization, total)
        End If
        Return res
    End Function

End Class

Public Class obtenGenerales_vuelos

    Public Shared Function obten_general_AirAvail(ByVal datosBusqueda_vuelo As dataBusqueda_vuelo) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_vuelo_AirAvail(datosBusqueda_vuelo)
        Return ds
    End Function

End Class

Public Class obtenGenerales_actividades

    Public Shared Function obten_general_IDX2(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_IDX2(datosBusqueda_actividad)
        Return ds
    End Function

    Public Shared Function obten_general_AVL2(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_AVL2(datosBusqueda_actividad)
        Return ds
    End Function

    Public Shared Function obten_general_POE(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_POE(datosBusqueda_actividad)
        Return ds
    End Function

End Class

Public Class obtenGenerales_autos

    Public Shared Function obten_general_CarAvail(ByVal datosBusqueda_auto As dataBusqueda_auto_test) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarAvail(datosBusqueda_auto)
        Return ds
    End Function

    Public Shared Function obten_general_CarRules(ByVal datosBusqueda_auto As dataBusqueda_auto_test) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarRules(datosBusqueda_auto)
        Return ds
    End Function

End Class

Public Class obtenGenerales_paquetes

    Public Shared Function obten_general_Paquete_Book(ByVal datosBusqueda_paquete As dataBusqueda_paquete, Optional IgnorarDuplicada As Boolean = False) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_paquete_Book(datosBusqueda_paquete, IgnorarDuplicada)
        Return ds
    End Function

    Public Shared Function obten_general_Paquete_Cancel(ByVal datosBusqueda_paquete As dataBusqueda_paquete) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_paquete_Cancel(datosBusqueda_paquete)
        Return ds
    End Function

    Public Shared Function obten_general_Paquete_Description(ByVal datosBusqueda_paquete As dataBusqueda_paquete) As DataSet
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_paquete_Description(datosBusqueda_paquete)
        Return ds
    End Function

End Class