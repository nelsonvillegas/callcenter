
Public Class dataBusqueda_vuelo
    Implements ICloneable

    Public rubro As enumServices
    Public confirmada As Boolean
    Public t_id As Integer

    'busqueda
    Public aerolinea As String
    Public clase As String
    Public moneda As String
    Public tipoViaje_Redondo As Boolean
    Public partidaCiudad As String
    Public partidaFecha As DateTime
    Public partidaHora As String
    Public destinoCiudad As String
    Public destinoFecha As DateTime
    Public destinoHora As String
    Public adultos As String
    Public mayores As String
    Public ninos As String
    'vuelo
    Public vuelo As dataVuelosItinerario.vuelo
    'reservar
    Public codViajero As String
    Public entrega_tipo As Boolean
    Public entrega_nombre As String
    Public entrega_apellido As String
    Public entrega_direccion As String
    Public entrega_ciudad As String
    Public entrega_estado As String
    Public entrega_cp As String
    Public entrega_pais As String
    Public entrega_email As String
    'display
    Public ConfirmNumber As String
    Public ReservationId As String
    Public status As String
    Public RecLoc As String

    Public Function Clone() As Object Implements ICloneable.Clone

        Dim temp As New dataBusqueda_vuelo

        temp.rubro = rubro
        temp.confirmada = confirmada
        temp.t_id = t_id

        temp.aerolinea = aerolinea
        temp.clase = clase
        temp.moneda = moneda
        temp.tipoViaje_Redondo = tipoViaje_Redondo
        temp.partidaCiudad = partidaCiudad
        temp.partidaFecha = partidaFecha
        temp.partidaHora = partidaHora
        temp.destinoCiudad = destinoCiudad
        temp.destinoFecha = destinoFecha
        temp.destinoHora = destinoHora
        temp.adultos = adultos
        temp.mayores = mayores
        temp.ninos = ninos
        temp.vuelo = vuelo 'falta .clone
        temp.codViajero = codViajero
        temp.entrega_tipo = entrega_tipo
        temp.entrega_nombre = entrega_nombre
        temp.entrega_apellido = entrega_apellido
        temp.entrega_direccion = entrega_direccion
        temp.entrega_ciudad = entrega_ciudad
        temp.entrega_estado = entrega_estado
        temp.entrega_cp = entrega_cp
        temp.entrega_pais = entrega_pais
        temp.entrega_email = entrega_email
        temp.ConfirmNumber = ConfirmNumber
        temp.ReservationId = ReservationId
        temp.status = status
        temp.RecLoc = RecLoc

        Return temp
    End Function

    Public Function Compara(ByVal temp As dataBusqueda_vuelo) As Boolean
        Dim iguales As Boolean = True

        If temp.rubro <> rubro Then iguales = False
        If temp.confirmada <> confirmada Then iguales = False
        If temp.t_id <> t_id Then iguales = False

        If temp.aerolinea <> aerolinea Then iguales = False
        If temp.clase <> clase Then iguales = False
        If temp.moneda <> moneda Then iguales = False
        If temp.tipoViaje_Redondo <> tipoViaje_Redondo Then iguales = False
        If temp.partidaCiudad <> partidaCiudad Then iguales = False
        If temp.partidaFecha <> partidaFecha Then iguales = False
        If temp.partidaHora <> partidaHora Then iguales = False
        If temp.destinoCiudad <> destinoCiudad Then iguales = False
        If temp.destinoFecha <> destinoFecha Then iguales = False
        If temp.destinoHora <> destinoHora Then iguales = False
        If temp.adultos <> adultos Then iguales = False
        If temp.mayores <> mayores Then iguales = False
        If temp.ninos <> ninos Then iguales = False
        'If temp.vuelo <> vuelo Then iguales = False
        If temp.codViajero <> codViajero Then iguales = False
        If temp.entrega_tipo <> entrega_tipo Then iguales = False
        If temp.entrega_nombre <> entrega_nombre Then iguales = False
        If temp.entrega_apellido <> entrega_apellido Then iguales = False
        If temp.entrega_direccion <> entrega_direccion Then iguales = False
        If temp.entrega_ciudad <> entrega_ciudad Then iguales = False
        If temp.entrega_estado <> entrega_estado Then iguales = False
        If temp.entrega_cp <> entrega_cp Then iguales = False
        If temp.entrega_pais <> entrega_pais Then iguales = False
        If temp.entrega_email <> entrega_email Then iguales = False
        If temp.ConfirmNumber <> ConfirmNumber Then iguales = False
        If temp.ReservationId <> ReservationId Then iguales = False
        If temp.status <> status Then iguales = False
        If temp.RecLoc <> RecLoc Then iguales = False

        Return iguales
    End Function

End Class

Public Class dataBusqueda_paquete
    Implements ICloneable

    Public confirmada As Boolean

    'datos reservaciones
    Public lista_Hoteles As New List(Of dataBusqueda_hotel_test)
    Public lista_Actividades As New List(Of dataBusqueda_actividad_test)
    Public lista_Autos As New List(Of dataBusqueda_auto_test)
    Public lista_Vuelos As New List(Of dataBusqueda_vuelo)
    'busqueda
    Public fechaDesde As DateTime
    Public fechaHasta As DateTime
    'reservar
    Public cli_id As String
    Public cli_nombre As String
    Public cli_apellido As String
    Public cli_direccion As String
    Public cli_ciudad As String
    Public cli_estado As String
    Public cli_cp As String
    Public cli_pais As String
    Public cli_telefono_AreaCityCode As String
    Public cli_telefono_PhoneNumber As String
    Public cli_telefono_PhoneUseType As String

    Public cli_telefono_adicional_AreaCityCode As String
    Public cli_telefono_adicional_PhoneNumber As String

    Public cli_email As String
    Public cli_emailAdicional1 As String
    Public cc_tipo As String
    Public cc_nombre_ As String
    Public cc_apellido As String
    Public cc_numero As String
    Public cc_numSeguridad As String
    Public cc_fechaExpira As String
    Public nota As String
    Public codigo_referencia As String ' Puede que este cambo vaya a nivel de data_Hotel_test
    Public homoclave As String
    Public Canal As String
    ''
    Public Rubro As enumRubrosPassport
    ''
    'display
    Public itinerario As String
    'cancel
    Public AllItinerary As String

    Public Function Clone() As Object Implements ICloneable.Clone
        Dim temp As New dataBusqueda_paquete

        'temp.rubro = rubro
        temp.confirmada = confirmada
        'temp.t = t

        For Each item As dataBusqueda_hotel_test In lista_Hoteles
            temp.lista_Hoteles.Add(item.Clone)
        Next
        For Each item As dataBusqueda_actividad_test In lista_Actividades
            temp.lista_Actividades.Add(item.Clone)
        Next
        For Each item As dataBusqueda_auto_test In lista_Autos
            temp.lista_Autos.Add(item.Clone)
        Next
        For Each item As dataBusqueda_vuelo In lista_Vuelos
            temp.lista_Vuelos.Add(item.Clone)
        Next
        temp.fechaDesde = fechaDesde
        temp.fechaHasta = fechaHasta
        temp.cli_id = cli_id
        temp.cli_nombre = cli_nombre
        temp.cli_apellido = cli_apellido
        temp.cli_direccion = cli_direccion
        temp.cli_ciudad = cli_ciudad
        temp.cli_estado = cli_estado
        temp.cli_cp = cli_cp
        temp.cli_pais = cli_pais
        temp.cli_telefono_AreaCityCode = cli_telefono_AreaCityCode
        temp.cli_telefono_PhoneNumber = cli_telefono_PhoneNumber
        temp.cli_telefono_PhoneUseType = cli_telefono_PhoneUseType
        temp.cli_telefono_adicional_AreaCityCode = cli_telefono_adicional_AreaCityCode
        temp.cli_telefono_adicional_PhoneNumber = cli_telefono_adicional_PhoneNumber
        temp.cli_email = cli_email
        temp.cli_emailAdicional1 = cli_emailAdicional1
        temp.cc_tipo = cc_tipo
        temp.cc_nombre_ = cc_nombre_
        temp.cc_apellido = cc_apellido
        temp.cc_numero = cc_numero
        temp.cc_numSeguridad = cc_numSeguridad
        temp.cc_fechaExpira = cc_fechaExpira
        temp.itinerario = itinerario
        temp.Rubro = Rubro
        temp.nota = nota
        temp.codigo_referencia = codigo_referencia
        temp.homoclave = homoclave
        temp.Canal = Canal
        Return temp
    End Function

    Public ReadOnly Property GetHotelNoConfirm() As dataBusqueda_hotel_test
        Get
            Dim datosBusqueda_hotel As dataBusqueda_hotel_test = Nothing
            For Each db_h As dataBusqueda_hotel_test In lista_Hoteles
                If Not db_h.confirmada Then
                    datosBusqueda_hotel = db_h
                    Exit For
                End If
            Next

            Return datosBusqueda_hotel
        End Get
    End Property

    Public ReadOnly Property GetHeaderNumHotelNoConfirm() As String
        Get
            Dim temp As String = ""
            For Each db_h As dataBusqueda_hotel_test In lista_Hoteles
                If Not db_h.confirmada Then
                    temp &= "H"
                End If
            Next

            Return temp
        End Get
    End Property

    Public ReadOnly Property GetAutoNoConfirm() As dataBusqueda_auto_test
        Get
            Dim datosBusqueda_auto As dataBusqueda_auto_test = Nothing
            For Each db_a As dataBusqueda_auto_test In lista_Autos
                If Not db_a.confirmada Then
                    datosBusqueda_auto = db_a
                    Exit For
                End If
            Next

            Return datosBusqueda_auto
        End Get
    End Property

    Public ReadOnly Property GetVueloNoConfirm() As dataBusqueda_vuelo
        Get
            Dim datosBusqueda_vuelo As dataBusqueda_vuelo = Nothing
            For Each db_v As dataBusqueda_vuelo In lista_Vuelos
                If Not db_v.confirmada Then
                    datosBusqueda_vuelo = db_v
                    Exit For
                End If
            Next

            Return datosBusqueda_vuelo
        End Get
    End Property

    Public ReadOnly Property TieneActividadesSinConfirmar() As Boolean
        Get
            Dim datosBusqueda_actividad As dataBusqueda_actividad_test = Nothing
            For Each db_a As dataBusqueda_actividad_test In lista_Actividades
                If Not db_a.confirmada Then Return True
            Next

            Return False
        End Get
    End Property

End Class

Public Class dataBusqueda_actividad_test
    Implements ICloneable

    Public rubro As enumServices
    Public confirmada As Boolean
    Public t_id As Integer

    'busqueda
    Public monedaBusqueda As String
    Public ciudadActividad As String
    Public ciudadActividadNombre As String
    Public fechaDesde As DateTime
    Public fechaHasta As DateTime
    Public codigoPromocion As String
    Public codigoAcceso As String
    'reservar
    Public entrega_direccion As String
    Public entrega_cp As String
    Public peticionEspecial As String
    Public cantidad As String
    'display
    Public ConfirmNumber As String
    'buscado
    Public ActivityID As String
    Public EventID As String
    Public EventTickets As String
    Public EventName As String
    Public PropertyID As String
    Public PropertyName As String
    Public AltCurrency As String
    Public PercentTax As String

    Public RateID As String
    Public RateTypeID As String
    Public RateTypeName As String
    Public DayReservar As String
    Public MinTickets As String
    Public MaxTickets As String
    Public AltRateTypePrice As String
    Public costoTotalFormato As String

    'Public ListRateID As List(Of String)
    'Public ListRateTypeID As List(Of String)
    'Public ListRateTypeName As List(Of String)
    'Public ListDayReservar As List(Of String)
    'Public ListMinTickets As List(Of String)
    'Public ListMaxTickets As List(Of String)
    'Public ListAltRateTypePrice As List(Of String)
    'Public ListcostoTotalFormato As List(Of String)
    'Public ListCantidad As List(Of Integer)

    Public ListRateType As List(Of RateType) = New List(Of RateType)

    Public status As String
    Public ReservationID As String

    Public Function Clone() As Object Implements ICloneable.Clone

        Dim temp As New dataBusqueda_actividad_test

        temp.rubro = rubro
        temp.confirmada = confirmada
        temp.t_id = t_id

        temp.monedaBusqueda = monedaBusqueda
        temp.ciudadActividad = ciudadActividad
        temp.ciudadActividadNombre = ciudadActividadNombre
        temp.fechaDesde = fechaDesde
        temp.fechaHasta = fechaHasta
        temp.entrega_direccion = entrega_direccion
        temp.entrega_cp = entrega_cp
        temp.peticionEspecial = peticionEspecial
        temp.cantidad = cantidad
        temp.ConfirmNumber = ConfirmNumber
        temp.ActivityID = ActivityID
        temp.EventID = EventID
        temp.RateID = RateID
        temp.RateTypeID = RateTypeID
        temp.PropertyID = PropertyID
        temp.PropertyName = PropertyName
        temp.RateTypeName = RateTypeName
        temp.DayReservar = DayReservar
        temp.ReservationID = ReservationID
        temp.MinTickets = MinTickets
        temp.MaxTickets = MaxTickets
        temp.PercentTax = PercentTax
        temp.AltCurrency = AltCurrency
        temp.AltRateTypePrice = AltRateTypePrice
        temp.costoTotalFormato = costoTotalFormato
        temp.status = status
        temp.EventTickets = EventTickets
        temp.EventName = EventName
        temp.codigoPromocion = codigoPromocion
        temp.codigoAcceso = codigoAcceso


        For Each rate As RateType In ListRateType
            temp.ListRateType.Add(rate)
        Next


        Return temp
    End Function

    Public Function Compara(ByVal temp As dataBusqueda_actividad_test) As Boolean
        Dim iguales As Boolean = True

        If temp.rubro <> rubro Then iguales = False
        If temp.confirmada <> confirmada Then iguales = False
        If temp.t_id <> t_id Then iguales = False

        If temp.monedaBusqueda <> monedaBusqueda Then iguales = False
        If temp.ciudadActividad <> ciudadActividad Then iguales = False
        If temp.ciudadActividadNombre <> ciudadActividadNombre Then iguales = False
        If temp.fechaDesde <> fechaDesde Then iguales = False
        If temp.fechaHasta <> fechaHasta Then iguales = False
        If temp.entrega_direccion <> entrega_direccion Then iguales = False
        If temp.entrega_cp <> entrega_cp Then iguales = False
        If temp.ConfirmNumber <> ConfirmNumber Then iguales = False
        If temp.peticionEspecial <> peticionEspecial Then iguales = False
        If temp.cantidad <> cantidad Then iguales = False
        If temp.ActivityID <> ActivityID Then iguales = False
        If temp.EventID <> EventID Then iguales = False
        If temp.RateID <> RateID Then iguales = False
        If temp.RateTypeID <> RateTypeID Then iguales = False
        If temp.PropertyID <> PropertyID Then iguales = False
        If temp.RateTypeName <> RateTypeName Then iguales = False
        If temp.DayReservar <> DayReservar Then iguales = False
        If temp.ReservationID <> ReservationID Then iguales = False
        If temp.MinTickets <> MinTickets Then iguales = False
        If temp.MaxTickets <> MaxTickets Then iguales = False
        If temp.PercentTax <> PercentTax Then iguales = False
        If temp.AltCurrency <> AltCurrency Then iguales = False
        If temp.AltRateTypePrice <> AltRateTypePrice Then iguales = False
        If temp.costoTotalFormato <> costoTotalFormato Then iguales = False
        If temp.status <> status Then iguales = False
        If temp.EventTickets <> EventTickets Then iguales = False
        If temp.EventName <> EventName Then iguales = False

        Return iguales
    End Function

End Class

Public Class dataBusqueda_auto_test
    Implements ICloneable

    Public rubro As enumServices
    Public confirmada As Boolean
    Public t_id As Integer

    'busqueda
    Public fechaRecoger As DateTime
    Public horaRecoger As String
    Public ciudadRecoger As String
    Public fechaDevolver As DateTime
    Public horaDevolver As String
    Public ciudadDevolver As String
    Public clase As String
    Public tipo As String
    Public compania As String
    Public monedaBusqueda As String
    Public RefPoint As String
    Public MoreCarsInd As String
    Public DBKey As String
    Public indexCar As String
    'agencia arrendadora
    Public idVendor As String
    Public idOffice As String
    Public idOfficeEnd As String
    Public LocnCat As String
    Public LocnNum As String
    'tarifa
    Public ServiceProvider As String
    Public Rate As String
    Public RateCat As String
    Public RateType As String
    Public RateDBKey As String
    Public idRate As String
    Public GDSType As String
    Public Tax As String
    Public SIPPClassText As String
    Public SIPPTypeText As String
    Public RateAmtCONV As String
    Public CUR As String
    Public idTypeCar As String
    Public RateSource As String
    Public FeeName As String
    Public Percent As String
    Public Amount As String
    Public RateAmtTotAproxCONV As String
    'display
    Public ConfirmNumber As String
    Public IdReservation As String
    Public empresa As String
    Public TotalTotal As String
    Public Status As String

    Public Function Clone() As Object Implements ICloneable.Clone
        Dim temp As New dataBusqueda_auto_test

        temp.rubro = rubro
        temp.confirmada = confirmada
        temp.t_id = t_id

        temp.fechaRecoger = fechaRecoger
        temp.horaRecoger = horaRecoger
        temp.ciudadRecoger = ciudadRecoger
        temp.fechaDevolver = fechaDevolver
        temp.horaDevolver = horaDevolver
        temp.ciudadDevolver = ciudadDevolver
        temp.clase = clase
        temp.tipo = tipo
        temp.compania = compania
        temp.monedaBusqueda = monedaBusqueda
        temp.RefPoint = RefPoint
        temp.MoreCarsInd = MoreCarsInd
        temp.DBKey = DBKey
        temp.indexCar = indexCar
        temp.idVendor = idVendor
        temp.idOffice = idOffice
        temp.idOfficeEnd = idOfficeEnd
        temp.LocnCat = LocnCat
        temp.LocnNum = LocnNum
        temp.ServiceProvider = ServiceProvider
        temp.Rate = Rate
        temp.RateCat = RateCat
        temp.RateType = RateType
        temp.RateDBKey = RateDBKey
        temp.idRate = idRate
        temp.GDSType = GDSType
        temp.Tax = Tax
        temp.SIPPClassText = SIPPClassText
        temp.SIPPTypeText = SIPPTypeText
        temp.RateAmtCONV = RateAmtCONV
        temp.CUR = CUR
        temp.idTypeCar = idTypeCar
        temp.RateSource = RateSource
        temp.FeeName = FeeName
        temp.Percent = Percent
        temp.Amount = Amount
        temp.RateSource = RateSource
        temp.ConfirmNumber = ConfirmNumber
        temp.IdReservation = IdReservation
        temp.empresa = empresa
        temp.RateAmtTotAproxCONV = RateAmtTotAproxCONV
        temp.TotalTotal = TotalTotal
        temp.Status = Status

        Return temp
    End Function

    Public Function Compara(ByVal temp As dataBusqueda_auto_test) As Boolean
        Dim iguales As Boolean = True

        If temp.rubro <> rubro Then iguales = False
        If temp.confirmada <> confirmada Then iguales = False
        If temp.t_id <> t_id Then iguales = False

        If temp.fechaRecoger <> fechaRecoger Then iguales = False
        If temp.horaRecoger <> horaRecoger Then iguales = False
        If temp.ciudadRecoger <> ciudadRecoger Then iguales = False
        If temp.fechaDevolver <> fechaDevolver Then iguales = False
        If temp.horaDevolver <> horaDevolver Then iguales = False
        If temp.ciudadDevolver <> ciudadDevolver Then iguales = False
        If temp.clase <> clase Then iguales = False
        If temp.tipo <> tipo Then iguales = False
        If temp.compania <> compania Then iguales = False
        If temp.monedaBusqueda <> monedaBusqueda Then iguales = False
        If temp.RefPoint <> RefPoint Then iguales = False
        If temp.MoreCarsInd <> MoreCarsInd Then iguales = False
        If temp.DBKey <> DBKey Then iguales = False
        If temp.indexCar <> indexCar Then iguales = False
        If temp.idVendor <> idVendor Then iguales = False
        If temp.idOffice <> idOffice Then iguales = False
        If temp.idOfficeEnd <> idOfficeEnd Then iguales = False
        If temp.LocnCat <> LocnCat Then iguales = False
        If temp.LocnNum <> LocnNum Then iguales = False
        If temp.ServiceProvider <> ServiceProvider Then iguales = False
        If temp.Rate <> Rate Then iguales = False
        If temp.RateCat <> RateCat Then iguales = False
        If temp.RateType <> RateType Then iguales = False
        If temp.RateDBKey <> RateDBKey Then iguales = False
        If temp.idRate <> idRate Then iguales = False
        If temp.GDSType <> GDSType Then iguales = False
        If temp.Tax <> Tax Then iguales = False
        If temp.SIPPClassText <> SIPPClassText Then iguales = False
        If temp.SIPPTypeText <> SIPPTypeText Then iguales = False
        If temp.RateAmtCONV <> RateAmtCONV Then iguales = False
        If temp.CUR <> CUR Then iguales = False
        If temp.idTypeCar <> idTypeCar Then iguales = False
        If temp.RateSource <> RateSource Then iguales = False
        If temp.FeeName <> FeeName Then iguales = False
        If temp.Percent <> Percent Then iguales = False
        If temp.Amount <> Amount Then iguales = False
        If temp.RateSource <> RateSource Then iguales = False
        If temp.ConfirmNumber <> ConfirmNumber Then iguales = False
        If temp.IdReservation <> IdReservation Then iguales = False
        If temp.empresa <> empresa Then iguales = False
        If temp.RateAmtTotAproxCONV <> RateAmtTotAproxCONV Then iguales = False
        If temp.TotalTotal <> TotalTotal Then iguales = False
        If temp.Status <> Status Then iguales = False

        Return iguales
    End Function

End Class

Public Class dataBusqueda_hotel_test
    Implements ICloneable

    Public rubro As enumServices
    Public confirmada As Boolean
    Public t_id As Integer
    Public DepositType As enumDepositType
    Public PaymentOther As String
    Public Comments As String
    Public DepositLimitDate As DateTime
    Public DepositInfoCCT As String
    Public DepositAmountCCT As String

    Public id_Empresa As String
    Public id_Segmento As String
    Public id_Medio As String
    Public frecuentCode As String

    'busqueda
    Public llegada As DateTime
    Public salida As DateTime
    Public noches As String
    Public buscaPor As String
    Public todosHoteles As Boolean
    Public ciudad_codigo As String
    Public ciudad_nombre As String
    Public hotel_nombre As String
    Public codProm As String
    Public Convenio As String
    Public habitaciones_ As uc_habitaciones.T_Habitaciones
    Public ninosEdades As String
    Public monedaBusqueda As String
    Public distancia As String
    Public RefPoint As String
    Public idArea As String

    'hotel
    Public ServiceProvider As String
    Public PropertyNumber As String
    Public PropertyName As String
    Public Address As String
    Public CityName As String
    Public CountryCode As String
    Public ChainCode As String
    Public AllowBankDeposit As Boolean
    Public AllowBankDepositCCT As Boolean
    Public AllowSpecificPayment As Boolean
    Public OnRequest As Boolean
    'tarifa
    Public RateCode As String
    Public GuarDep As String
    Public TotalAvgRate As String
    Public PlusTax As String
    Public Tax As String
    Public AltTax As String
    Public AltCurrency As String
    Public AltTotalExtras As String
    Public AvgRate As String
    Public AltTotalStay As String
    Public TotalNR As String
    Public nombreHabitacion As String
    Public PlanName As String
    Public PlanDescription As String
    Public Segmento As String
    Public isNetRate As Boolean
    Public PrecioTarifa() As String
    Public precioNR() As String
    Public adultsExtra() As String
    Public childrenExtra() As String

    'display
    Public cust_Preferences As String
    Public ConfirmNumber As String
    Public ReservationId As String
    Public Status As String
    Public Source As String

    Public MoneyExchange As String

    Public IsCopy As Boolean

    Public isOnlinePayment As String
    Public isPayPalPayment As String
    Public isPayUPayment As String
    Public isAmericanExpress As String
    Public isConektaPayment
    Public monthBanorte As String
    Public PaymentSource As Integer
    Public NoAutorizacion As String

    Public Idioma As Integer
    Public DepositTarget As String
    Public DepositReference As String




    Public Function Clone() As Object Implements ICloneable.Clone
        Dim temp As New dataBusqueda_hotel_test

        temp.rubro = rubro
        temp.confirmada = confirmada
        temp.t_id = t_id

        temp.llegada = llegada
        temp.salida = salida
        temp.noches = noches
        temp.buscaPor = buscaPor
        temp.todosHoteles = todosHoteles
        temp.ciudad_codigo = ciudad_codigo
        temp.ciudad_nombre = ciudad_nombre
        temp.codProm = codProm
        temp.Convenio = Convenio
        If habitaciones_ IsNot Nothing Then temp.habitaciones_ = habitaciones_.copy
        temp.ninosEdades = ninosEdades
        temp.monedaBusqueda = monedaBusqueda
        temp.distancia = distancia
        temp.ServiceProvider = ServiceProvider
        temp.PropertyNumber = PropertyNumber
        temp.PropertyName = PropertyName
        temp.Address = Address
        temp.CityName = CityName
        temp.CountryCode = CountryCode
        temp.ChainCode = ChainCode
        temp.RateCode = RateCode
        temp.GuarDep = GuarDep
        temp.AllowBankDeposit = AllowBankDeposit
        temp.AllowBankDepositCCT = AllowBankDepositCCT
        temp.AllowSpecificPayment = AllowSpecificPayment
        temp.TotalAvgRate = TotalAvgRate
        temp.PlusTax = PlusTax
        temp.Tax = Tax
        temp.AltTax = AltTax
        temp.AltCurrency = AltCurrency
        temp.AltTotalExtras = AltTotalExtras
        temp.AvgRate = AvgRate
        temp.AltTotalStay = AltTotalStay
        temp.nombreHabitacion = nombreHabitacion
        temp.PlanName = PlanName
        temp.PlanDescription = PlanDescription
        temp.Segmento = Segmento

        temp.cust_Preferences = cust_Preferences
        temp.ConfirmNumber = ConfirmNumber
        temp.ReservationId = ReservationId
        temp.Status = Status

        temp.DepositType = DepositType
        temp.PaymentOther = PaymentOther

        temp.DepositLimitDate = DepositLimitDate
        temp.Comments = Comments
        temp.Source = Source

        temp.DepositAmountCCT = Me.DepositAmountCCT
        temp.DepositInfoCCT = Me.DepositInfoCCT

        temp.id_Empresa = id_Empresa
        temp.id_Segmento = id_Segmento
        temp.id_Medio = id_Medio

        temp.frecuentCode = frecuentCode

        temp.idArea = idArea
        temp.MoneyExchange = MoneyExchange
        temp.OnRequest = OnRequest

        temp.isOnlinePayment = isOnlinePayment
        temp.isPayPalPayment = isPayPalPayment
        temp.isPayUPayment = isPayUPayment

        temp.IsCopy = IsCopy

        Return temp
    End Function

    Public Function Compara(ByVal temp As dataBusqueda_hotel_test) As Boolean
        Dim iguales As Boolean = True

        If temp.rubro <> rubro Then iguales = False
        If temp.confirmada <> confirmada Then iguales = False
        If temp.t_id <> t_id Then iguales = False '                               ?????

        If temp.llegada <> llegada Then iguales = False
        If temp.salida <> salida Then iguales = False
        If temp.noches <> noches Then iguales = False
        If temp.buscaPor <> buscaPor Then iguales = False
        If temp.todosHoteles <> todosHoteles Then iguales = False
        If temp.ciudad_codigo <> ciudad_codigo Then iguales = False
        If temp.ciudad_nombre <> ciudad_nombre Then iguales = False
        If temp.codProm <> codProm Then iguales = False
        If temp.Convenio <> Convenio Then iguales = False
        If temp.habitaciones_.Equals(habitaciones_) Then iguales = False
        If temp.ninosEdades <> ninosEdades Then iguales = False
        If temp.monedaBusqueda <> monedaBusqueda Then iguales = False
        If temp.distancia <> distancia Then iguales = False
        If temp.ServiceProvider <> ServiceProvider Then iguales = False
        If temp.PropertyNumber <> PropertyNumber Then iguales = False
        If temp.PropertyName <> PropertyName Then iguales = False
        If temp.Address <> Address Then iguales = False
        If temp.CityName <> CityName Then iguales = False
        If temp.CountryCode <> CountryCode Then iguales = False
        If temp.ChainCode <> ChainCode Then iguales = False
        If temp.RateCode <> RateCode Then iguales = False
        If temp.GuarDep <> GuarDep Then iguales = False
        If temp.AllowBankDeposit <> AllowBankDeposit Then iguales = False
        If temp.AllowBankDepositCCT <> AllowBankDepositCCT Then iguales = False
        If temp.AllowSpecificPayment <> AllowSpecificPayment Then iguales = False
        If temp.TotalAvgRate <> TotalAvgRate Then iguales = False
        If temp.PlusTax <> PlusTax Then iguales = False
        If temp.Tax <> Tax Then iguales = False
        If temp.AltTax <> AltTax Then iguales = False
        If temp.AltCurrency <> AltCurrency Then iguales = False
        If temp.AltTotalExtras <> AltTotalExtras Then iguales = False
        If temp.AvgRate <> AvgRate Then iguales = False
        If temp.AltTotalStay <> AltTotalStay Then iguales = False
        If temp.nombreHabitacion <> nombreHabitacion Then iguales = False
        If temp.PlanName <> PlanName Then iguales = False
        If temp.PlanDescription <> PlanDescription Then iguales = False
        If temp.Segmento <> Segmento Then iguales = False
        If temp.cust_Preferences <> cust_Preferences Then iguales = False
        If temp.ConfirmNumber <> ConfirmNumber Then iguales = False
        If temp.ReservationId <> ReservationId Then iguales = False
        If temp.Status <> Status Then iguales = False

        If temp.DepositType <> DepositType Then iguales = False
        If temp.PaymentOther <> PaymentOther Then iguales = False

        If temp.DepositLimitDate <> DepositLimitDate Then iguales = False
        If temp.Comments <> Comments Then iguales = False
        If temp.Source <> Source Then iguales = False

        If temp.DepositInfoCCT <> Me.DepositInfoCCT Then iguales = False
        If temp.DepositAmountCCT <> Me.DepositAmountCCT Then iguales = False

        If temp.id_Empresa <> Me.id_Empresa Then iguales = False
        If temp.id_Segmento <> Me.id_Segmento Then iguales = False
        If temp.id_Medio <> Me.id_Medio Then iguales = False

        If temp.frecuentCode <> Me.frecuentCode Then iguales = False

        If temp.idArea <> Me.idArea Then iguales = False
        If temp.MoneyExchange <> Me.MoneyExchange Then iguales = False
        If temp.OnRequest <> Me.OnRequest Then iguales = False

        If temp.isPayPalPayment <> Me.isPayPalPayment Then iguales = False
        If temp.isOnlinePayment <> Me.isOnlinePayment Then iguales = False
        If temp.isPayUPayment <> Me.isPayUPayment Then iguales = False

        If temp.IsCopy <> Me.IsCopy Then iguales = False

        Return iguales
    End Function

    Public Function calcula_noches() As Integer
        Return salida.Subtract(llegada).TotalDays()
    End Function

End Class

Public Class dataBusqueda_passport
    Public idUsuario As String
    Public idAgencia As String
    Public Fecha1 As DateTime
    Public Fecha2 As DateTime
    Public Rubro As String
    Public Type As String
    Public Origen As String
    Public SourceList As String
    Public idCorporativo As String
    Public NombreClie As String
    Public NumeroConf As String
    Public idSegmento As String
    Public NombreEmpresa As String
    Public TransaccionesPMS As String
    Public IdUsuarioCallCenter As String
    Public idEmpresa As String
End Class
