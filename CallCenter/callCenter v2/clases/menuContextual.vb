Public Class menuContextual

    Public Shared Sub inicializar_items(ByVal elMenu As ContextMenuStrip, ByVal llaves() As String)
        Try
            Dim tool As Infragistics.Win.UltraWinToolbars.ToolBase
            Dim item As ToolStripItem

            elMenu.Items.Clear()

            If llaves Is Nothing Then
                Return
            End If

            For Each str_key As String In llaves
                tool = misForms.form1.UltraToolbarsManager1.Tools(str_key)
                item = elMenu.Items.Add(tool.SharedProps.Caption, tool.SharedProps.AppearancesSmall.Appearance.Image)
                item.Enabled = tool.SharedProps.Enabled
                item.Name = tool.Key
            Next
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0073", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

End Class
