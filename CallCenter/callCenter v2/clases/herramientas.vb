Public Class herramientas

    Public Shared Sub cambiar_idioma(ByVal btn As Infragistics.Win.UltraWinToolbars.ToolBase)
        Dim cultura As String = "es"

        If btn Is Nothing Then
            cultura = "es"
        Else
            Dim stateB As Infragistics.Win.UltraWinToolbars.StateButtonTool = CType(btn, Infragistics.Win.UltraWinToolbars.StateButtonTool)
            Select Case stateB.Key
                Case "btn_espa�ol"
                    cultura = "es"
                Case "btn_ingles"
                    cultura = "en"
            End Select
        End If

        CapaPresentacion.Idiomas.cultura = cultura
        CapaPresentacion.Idiomas.cambiar_systema()
    End Sub

    Public Shared Sub selRow(ByVal UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim row As Infragistics.Win.UltraWinGrid.UltraGridRow = Nothing
        Dim element As Infragistics.Win.UIElement = Nothing

        element = UltraGrid1.DisplayLayout.UIElement.ElementFromPoint(e.Location)
        row = element.GetContext(GetType(Infragistics.Win.UltraWinGrid.UltraGridRow))

        If (row IsNot Nothing) AndAlso TypeOf element.SelectableItem Is Infragistics.Win.UltraWinGrid.UltraGridRow AndAlso (row.IsDataRow) Then
            UltraGrid1.ActiveRow = row
        End If
    End Sub

    Public Shared Function get_mdiTabGroup_de(ByVal nombre As String, ByVal key As String) As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup
        For Each group As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In misForms.form1.UltraTabbedMdiManager1.TabGroups
            For Each tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab In group.Tabs
                If tab.Form.Name = nombre AndAlso CObj(tab.Form).datosBusqueda_hotel.PropertyNumber = key Then
                    Return group
                End If
            Next
        Next

        Return Nothing
    End Function

    Public Shared Function get_tabForm(ByVal nombre As String, ByVal key As String) As Infragistics.Win.UltraWinTabbedMdi.MdiTab
        For Each group As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In misForms.form1.UltraTabbedMdiManager1.TabGroups
            For Each tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab In group.Tabs
                If tab.Form.Name = nombre AndAlso CObj(tab.Form).key = key Then
                    Return tab
                End If
            Next
        Next

        Return Nothing
    End Function

    Public Shared Function get_arrCuartos(ByVal num_cua As Integer, ByVal num_per As Integer)
        If num_cua < 1 Or num_per < 0 Then
            Return New Integer() {}
        End If

        Dim arr_cua(num_cua - 1) As Integer

        'asigna el maximo posible de personas a cada cuarto PERO que todos tengan lo mismo
        Dim res As Integer = num_per \ num_cua
        For i As Integer = 0 To arr_cua.Length - 1
            arr_cua(i) = res
        Next

        'asigna de las personas restantes, una en cada cuarto (las personas restantes nunca van 
        'a ser mayor que la numero de cuartos) por la divicion de arriba
        Dim num_restant As Integer = num_per - (res * num_cua)
        For i As Integer = 0 To num_restant - 1
            arr_cua(i) += 1
        Next

        Return arr_cua
    End Function

    Public Shared Function get_arrEdades(ByVal num_cua As Integer, ByVal edades As String, ByVal arr_cua_chil() As Integer, ByVal num_ch As Integer)
        If num_cua < 1 Or edades = "" Or num_ch = 0 Then
            Return New String() {}
        End If
        Dim arr_Ages(num_cua - 1) As String
        'Dim ages() As String = edades.Split(",")
        Dim ages As New List(Of String) '= edades.Split(",").t
        Dim WIndex As Integer = 0
        Dim aux As String = String.Empty
        For Each age As String In edades.Split(",")
            ages.Add(age)
        Next
        For Each childT As Integer In arr_cua_chil
            aux = String.Empty
            For i As Integer = 0 To childT - 1
                aux += ages(i).ToString + ","
            Next
            ages.RemoveRange(0, childT)
            arr_Ages(WIndex) = aux.Substring(0, aux.Length - 1)
            WIndex += 1
        Next
        Return arr_Ages
    End Function

    Public Shared Function Load_Web_Image(ByVal URL_Image As String, ByVal def_img As Bitmap) As Image
        Dim objImage As IO.MemoryStream
        Dim objwebClient As System.Net.WebClient
        URL_Image = Trim(URL_Image)
        Dim img As Image = Nothing

        'no es img
        If URL_Image.EndsWith("/") Then
            Return def_img
        End If

        Try
            'If Not URL_Image.ToLower().StartsWith("http://") Then URL_Image = "http://" + URL_Image - algunos son https
            objwebClient = New Net.WebClient()
            objImage = New IO.MemoryStream(objwebClient.DownloadData(URL_Image))
            img = Image.FromStream(objImage)
        Catch ex As Exception
            img = def_img
        End Try

        Return img
    End Function

    Public Shared Function obten_TitleValue(ByVal ds As DataSet, ByVal tabla As String, ByVal keyword As String) As String()
        Dim title As String = ""
        Dim value As String = ""

        If ds.Tables.IndexOf(tabla) <> -1 Then
            For Each r_desc As DataRow In ds.Tables(tabla).Rows
                If r_desc.Item("Keyword").ToString.ToUpper = keyword Then
                    title = r_desc.Item("Title")
                    value = CapaPresentacion.Common.HTMLDecode(r_desc.Item("Value").ToString)
                End If
            Next
        End If

        While Not value Is Nothing AndAlso value.Contains("  ")
            value = value.Replace("  ", " ")
        End While

        Return New String() {title, value}
    End Function

    Public Shared Function color_disponible() As Color
        For Each miColor As Color In lista_colores
            Dim usado As Boolean = False
            For Each group As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In misForms.form1.UltraTabbedMdiManager1.TabGroups
                For Each tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab In group.Tabs
                    If tab.Settings.TabAppearance.BackColor = miColor Then usado = True
                Next
            Next

            If Not usado Then Return miColor
        Next

        Return Color.Gray
    End Function

    Public Shared Sub fechas_inteligentes(ByVal dtp_in As DateTimePicker, ByVal dtp_out As DateTimePicker, ByVal sender As DateTimePicker, ByVal dias_Add As Integer, ByVal txt_noches As Infragistics.Win.UltraWinEditors.UltraTextEditor)
        Try
            ', ByVal use_fecha_menor As Boolean, ByVal fecha_menor As DateTime

            'If use_fecha_menor Then
            '    fecha_menor = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
            '    'If dtp_in.Value < fecha_menor Then dtp_in.Value = fecha_menor
            '    dtp_in.MinDate = fecha_menor
            '    'dtp_out.MinDate = fecha_menor
            'End If

            Dim d_o As Date = dtp_out.Value
            Dim d_i As Date = dtp_in.Value

            d_o = New Date(DatePart(DateInterval.Year, d_o), DatePart(DateInterval.Month, d_o), DatePart(DateInterval.Day, d_o))
            d_i = New Date(DatePart(DateInterval.Year, d_i), DatePart(DateInterval.Month, d_i), DatePart(DateInterval.Day, d_i))

            If txt_noches IsNot Nothing Then txt_noches.Text = (d_o - d_i).Days.ToString

            If d_o <= d_i Then dtp_out.Value = dtp_in.Value.AddDays(dias_Add)

            If dtp_in.Name = sender.Name AndAlso dias_Add = 0 AndAlso dtp_in.Value = dtp_out.Value Then
                dtp_out.Value = dtp_out.Value.AddDays(1)
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0071", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Public Structure lugares
        Dim idCiudad As String
        Dim ciudad As String
        Dim pais As String
        Dim codigo As String
        Dim RefPoint As String
        Dim distancia As String
        '---------------------
        Dim nombre_aero As String
        Dim IATA As String
    End Structure

    'Public Shared Sub llenar_combo(ByVal cmb As ComboBox, ByVal lista_lugares As List(Of lugares), ByVal rbt_ciudad As RadioButton)
    '    Dim ds As DataSet = get_lugares(rbt_ciudad, cmb)

    '    cmb.Items.Clear()

    '    If ds Is Nothing Then
    '        cmb.Items.Add(CapaPresentacion.Idiomas.get_str("str_0193_errSQLconn"))
    '        lista_lugares.Clear()
    '        Return
    '    End If

    '    lista_lugares.Clear()
    '    If ds.Tables(0).Rows.Count = 0 Then
    '        cmb.Items.Add(CapaPresentacion.Idiomas.get_str("str_0194_noDestinos"))
    '    Else
    '        For Each r As DataRow In ds.Tables(0).Rows
    '            If rbt_ciudad.Checked Then
    '                cmb.Items.Add(r.Item("ciudad").ToString + ", " + r.Item("pais").ToString)
    '                Dim un_lugar As New lugares

    '                If Not r.Item("idCiudad") Is System.DBNull.Value Then un_lugar.idCiudad = r.Item("idCiudad")
    '                If Not r.Item("ciudad") Is System.DBNull.Value Then un_lugar.ciudad = r.Item("ciudad")
    '                If Not r.Item("pais") Is System.DBNull.Value Then un_lugar.pais = r.Item("pais")
    '                If Not r.Item("codigo") Is System.DBNull.Value Then un_lugar.codigo = r.Item("codigo")
    '                lista_lugares.Add(un_lugar)
    '            Else
    '                cmb.Items.Add(r.Item("nombre").ToString)
    '                Dim un_lugar As New lugares

    '                un_lugar.nombre_aero = r.Item("nombre")
    '                un_lugar.codigo = r.Item("iata")
    '                lista_lugares.Add(un_lugar)
    '            End If
    '        Next
    '    End If
    'End Sub

    Public Shared Sub llenar(ByVal es_ciudad As Boolean, ByVal str_lugar As String, ByVal cs1 As ComboSearch)
        Dim ds As DataSet = get_lugares_3(es_ciudad, str_lugar)

        cs1.ListBox1.Items.Clear()

        If ds Is Nothing Then
            cs1.ListBox1.Items.Add(CapaPresentacion.Idiomas.get_str("str_0193_errSQLconn"))
            cs1.lista_lugares.Clear()
            Return
        End If

        cs1.lista_lugares.Clear()
        If ds.Tables(0).Rows.Count = 0 Then
            cs1.ListBox1.Items.Add(CapaPresentacion.Idiomas.get_str("str_0194_noDestinos"))
        Else
            For Each r As DataRow In ds.Tables(0).Rows
                If es_ciudad Then
                    Dim str_ciudad As String
                    If Not r.IsNull("ciudad_idioma") Then str_ciudad = r.Item("ciudad_idioma") Else str_ciudad = r.Item("ciudad")

                    cs1.ListBox1.Items.Add(str_ciudad + ", " + r.Item("estado").ToString + ", " + r.Item("pais").ToString)
                    Dim un_lugar As New lugares

                    If Not r.Item("idCiudad") Is System.DBNull.Value Then un_lugar.idCiudad = r.Item("idCiudad")
                    'If Not r.Item("ciudad") Is System.DBNull.Value Then un_lugar.ciudad = str_ciudad
                    un_lugar.ciudad = str_ciudad
                    un_lugar.RefPoint = r.Item("ciudad")
                    If Not r.Item("pais") Is System.DBNull.Value Then un_lugar.pais = r.Item("pais")
                    If Not r.Item("codigo") Is System.DBNull.Value Then un_lugar.codigo = r.Item("codigo")
                    If Not r.Item("distancia") Is System.DBNull.Value Then un_lugar.distancia = r.Item("distancia")
                    cs1.lista_lugares.Add(un_lugar)
                Else
                    Dim str_aeropuerto As String
                    If Not r.IsNull("aeropuerto_idioma") Then str_aeropuerto = r.Item("aeropuerto_idioma") Else str_aeropuerto = r.Item("nombre")

                    cs1.ListBox1.Items.Add(str_aeropuerto)
                    Dim un_lugar As New lugares

                    un_lugar.nombre_aero = str_aeropuerto
                    un_lugar.codigo = r.Item("iata")
                    cs1.lista_lugares.Add(un_lugar)
                End If
            Next
        End If
    End Sub

    'Private Shared Function get_lugares(ByVal rbt_ciudad As RadioButton, ByVal cmb_lugar As ComboBox) As DataSet
    '    Try
    '        Dim es_ciudad As Boolean
    '        If rbt_ciudad.Checked Then es_ciudad = True Else es_ciudad = False
    '        Dim lugar As String = cmb_lugar.Text
    '        Dim pais As String = ""
    '        If lugar.Contains(",") Then
    '            Dim arr() As String = lugar.Split(",")
    '            pais = arr(arr.Length - 1).Trim()
    '            lugar = lugar.Substring(0, lugar.IndexOf(","))
    '        End If

    '        Dim conn As New System.Data.SqlClient.SqlConnection()
    '        conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL 'tools_db.str_conn("ventanaCallCenter")

    '        Dim cmd As New System.Data.SqlClient.SqlCommand
    '        cmd.Connection = conn
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.CommandText = "consulta_lugares"

    '        cmd.Parameters.Add("@es_ciudades", SqlDbType.Bit).Value = es_ciudad
    '        cmd.Parameters.Add("@nombre_lugar", SqlDbType.NVarChar, 50).Value = lugar
    '        cmd.Parameters.Add("@nombre_pais", SqlDbType.NVarChar, 50).Value = pais

    '        Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
    '        Dim ds As New DataSet
    '        adapter.Fill(ds)

    '        Return ds
    '    Catch ex As Exception
    '        CapaLogicaNegocios.LogManager.agregar_registro("get_lugares - Form1_callCenter", ex.Message)
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function get_lugares_2(ByVal es_ciudad As Boolean, ByVal str_lugar As String) As DataSet
        Try

            Dim lugar As String = str_lugar
            Dim pais As String = ""
            If lugar.Contains(",") Then
                Dim arr() As String = lugar.Split(",")
                pais = arr(arr.Length - 1).Trim()
                lugar = lugar.Substring(0, lugar.IndexOf(","))
            End If

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "consulta_lugares"

            cmd.Parameters.Add("@es_ciudades", SqlDbType.Bit).Value = es_ciudad
            cmd.Parameters.Add("@nombre_lugar", SqlDbType.NVarChar, 50).Value = lugar
            cmd.Parameters.Add("@nombre_pais", SqlDbType.NVarChar, 50).Value = pais
            cmd.Parameters.Add("@idioma", SqlDbType.NVarChar, 50).Value = CapaPresentacion.Idiomas.idIdioma

            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_lugares_3(ByVal es_ciudad As Boolean, ByVal str_lugar As String) As DataSet
        Try
            Dim lugar As String = str_lugar
            Dim pais As String = ""
            If lugar.Contains(",") Then
                Dim arr() As String = lugar.Split(",")
                pais = arr(arr.Length - 1).Trim()
                lugar = lugar.Substring(0, lugar.IndexOf(","))
            End If

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            If es_ciudad Then
                Dim sql As String = ""
                sql &= "SELECT top 10 c.idCiudad,c.nombre ciudad,e.nombre estado,"
                sql &= "p.idpais pais,c.codigo codigo,d.texto ciudad_idioma,c.distancia "
                sql &= "FROM "
                sql &= "ciudades c "
                sql &= "inner join municipios				m on m.idMunicipio=c.idMunicipio "
                sql &= "inner join estados					e on e.idEstado=m.idEstado "
                sql &= "inner join paises					p on p.idPais=e.idPais "
                sql &= "LEFT OUTER JOIN PlacesDictionary	d ON c.iddiccionario=d.iddiccionario AND d.ididioma=@idioma "
                sql &= "WHERE ("
                sql &= "				c.nombre	COLLATE SQL_Latin1_General_CP1_CI_AI like '%' + @nombre_lugar + '%' OR"
                sql &= "				c.keywords	COLLATE SQL_Latin1_General_CP1_CI_AI like '%' + @nombre_lugar + '%'"
                sql &= ") AND "
                sql &= "(p.nombre COLLATE SQL_Latin1_General_CP1_CI_AI like '%' + @nombre_pais + '%' or p.idpais like '%' + @nombre_pais + '%' )"
                sql &= "ORDER BY c.nombre,p.nombre"
                cmd.CommandType = CommandType.Text
                cmd.CommandText = sql
            Else
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "consulta_lugares"
                cmd.Parameters.Add("@es_ciudades", SqlDbType.Bit).Value = es_ciudad
            End If
            cmd.Parameters.Add("@nombre_lugar", SqlDbType.NVarChar, 50).Value = lugar.Trim
            cmd.Parameters.Add("@nombre_pais", SqlDbType.NVarChar, 50).Value = pais.Trim
            cmd.Parameters.Add("@idioma", SqlDbType.NVarChar, 50).Value = CapaPresentacion.Idiomas.idIdioma

            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

    'TODO: empesas/medios/segmentos/mediospromocion
    Public Shared Function get_MediosPromocion(ByVal idCorporativo As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from uvcc_mediospromocion where idCorporativo=" & idCorporativo
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_MedioPromocion(ByVal id_MedioPromocion As String) As String
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select nombre from uvcc_mediospromocion where id_Mediopromocion=" & id_MedioPromocion
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                Return ds.Tables(0).Rows(0).Item("Nombre")
            Else
                Return ""
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_Empresas(ByVal id_Medio As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from uvcc_empresas where id_Medio=" & id_Medio
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_Empresas_Segmento(ByVal id_Segmento As String, ByVal filter As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from uvcc_empresas where id_Segmento=" & id_Segmento & " AND (nombre like '%" & filter & "%' or codigo like '%" & filter & "%' or codigo+ ' - '+nombre like '%" & filter & "%') order by nombre"
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_Empresas_filtro(ByVal filter As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from uvcc_empresas where (nombre like '%" & filter & "%' or codigo like '%" & filter & "%' or isnull(codigo,'')+ ' - '+nombre like '%" & filter & "') order by nombre"
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

    Public Shared Function get_Empresa(ByVal id_Empresa As String) As String
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select nombre from uvcc_empresas where id_empresa=" & id_Empresa
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                Return ds.Tables(0).Rows(0).Item("Nombre")
            Else
                Return ""
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_idMedioEmpresa(ByVal id_Empresa As String) As String
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select id_medio from uvcc_empresas where id_empresa=" & id_Empresa
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                Return ds.Tables(0).Rows(0).Item("id_medio")
            Else
                Return ""
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_idSegmentoEmpresa(ByVal id_Empresa As String) As String
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select id_Segmento from uvcc_empresas where id_empresa=" & id_Empresa
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                Return ds.Tables(0).Rows(0).Item("id_Segmento")
            Else
                Return ""
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_emailRequiredSegmentoEmpresa(ByVal id_Empresa As String) As String
        Dim isRequired As Boolean = False
        Try
            Using conn As New System.Data.SqlClient.SqlConnection(CapaAccesoDatos.XML.localData.ServidorSQL)
                Using cmd As New System.Data.SqlClient.SqlCommand
                    cmd.Connection = conn
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = "Select isnull(correorequerido,0) emailRequerido from uvcc_empresas e inner join uvcc_segmentos s on s.id_segmento = e.id_segmento where id_empresa=" & id_Empresa
                    conn.Open()
                    isRequired = Convert.ToBoolean(cmd.ExecuteScalar())
                End Using
            End Using
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return isRequired
    End Function
    Public Shared Function get_emailRequiredSegmento(ByVal id_Segmento As String) As String
        Dim isRequired As Boolean = False
        Try
            Using conn As New System.Data.SqlClient.SqlConnection(CapaAccesoDatos.XML.localData.ServidorSQL)
                Using cmd As New System.Data.SqlClient.SqlCommand
                    cmd.Connection = conn
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = "Select isnull(correorequerido,0) emailRequerido from uvcc_segmentos WHERE id_segmento=" & id_Segmento
                    conn.Open()
                    isRequired = Convert.ToBoolean(cmd.ExecuteScalar())
                End Using
            End Using
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return isRequired
    End Function
    Public Shared Function get_Medios(ByVal idCorporativo As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from uvcc_medios where idCorporativo=" & idCorporativo
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_Medio(ByVal id_Medio As String) As String
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select nombre from uvcc_medios where id_Medio=" & id_Medio
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                Return ds.Tables(0).Rows(0).Item("Nombre")
            Else
                Return ""
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_Segmentos(ByVal idCorporativo As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from uvcc_segmentos where idCorporativo=" & idCorporativo
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function
    Public Shared Function get_Segmento(ByVal id_segmento As String) As String
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select nombre from uvcc_segmentos where id_segmento=" & id_segmento
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)
            If Not IsNothing(ds) AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count = 1 Then
                Return ds.Tables(0).Rows(0).Item("Nombre")
            Else
                Return ""
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

    Public Shared Function EmpresaSegmentoInsertar(id_Empresa As String, Codigo As String, Nombre As String, ByVal id_Segmento As String, idCorporativo As String) As Boolean
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL
            conn.Open()
            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "INSERT INTO uvcc_empresas(id_empresa,Codigo,nombre,id_segmento,idCorporativo) values (" & id_Empresa & ",'" & Codigo & "','" & Nombre & "'," & id_Segmento & "," & idCorporativo & ")"
            Dim res As Boolean = False
            If cmd.ExecuteNonQuery > 0 Then res = True
            conn.Close()
            Return res
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return False
        End Try
    End Function

    Public Shared Function EmpresaSegmentoActualizar(id_Empresa As String, Nombre As String, ByVal id_Segmento As String) As Boolean
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL
            conn.Open()
            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "UPDATE uvcc_empresas set nombre='" & Nombre & "',id_segmento=" & id_Segmento & " WHERE id_Empresa=" & id_Empresa
            Dim res As Boolean = False
            If cmd.ExecuteNonQuery > 0 Then res = True
            conn.Close()
            Return res
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return False
        End Try
    End Function


    Public Shared Function EmpresaSegmentoEliminar(id_Empresa As String) As Boolean
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL
            conn.Open()
            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "DELETE FROM uvcc_empresas  WHERE id_Empresa=" & id_Empresa
            Dim res As Boolean = False
            If cmd.ExecuteNonQuery > 0 Then res = True
            conn.Close()
            Return res
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return False
        End Try
    End Function

    Public Shared Function get_Areas(ByVal idCiudad As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "select a.idarea,isnull(d.texto,a.nombre) area from areasciudad ac left join Areas a on ac.idarea=a.IdArea left join Diccionario d on a.iddiccionarionombre=d.iddiccionario AND d.ididioma=" & CapaPresentacion.Idiomas.idIdioma & " where ac.idCiudad=" & idCiudad & "  order by isnull(d.texto,a.nombre)"
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

    Public Shared Sub formatea_campoNumerico(ByVal txt As Infragistics.Win.UltraWinEditors.UltraTextEditor)
        Dim regEx As New System.Text.RegularExpressions.Regex("[^0-9]")
        txt.Text = regEx.Replace(txt.Text, "")
    End Sub

    Public Shared Function llena_ds(ByVal xml_data As String, ByVal tipo As String) As DataSet
        Dim ds As Object = Nothing

        Select Case tipo

            Case "Call_New_RQ"
                ds = New Call_new_RQ
            Case "Call_New_RS"
                ds = New Call_New_RS

            Case "Call_Finish_RQ"
                ds = New Call_Finish_RQ
            Case "Call_Finish_RS"
                ds = New Call_Finish_RS

            Case "Call_AddReservations_RQ"
                ds = New Call_AddReservations_RQ
            Case "Call_AddReservations_RS"
                ds = New Call_AddReservations_RS

            Case "Call_getCalls_RQ"
                ds = New Call_getCalls_RQ
            Case "Call_getCalls_RS"
                ds = New Call_getCalls_RS

            Case "Call_GetOne_RQ"
                ds = New Call_GetOne_RQ
            Case "Call_GetOne_RS"
                ds = New Call_GetOne_RS
            Case "Call_Log_Insert_RQ"
                ds = New Call_Log_Insert_RQ
            Case "Call_Log_Insert_RS"
                ds = New Call_Log_Insert_RS
            Case "Call_Log_Get_RQ"
                ds = New Call_Log_Get_RQ
            Case "Call_Log_Get_RS"
                ds = New Call_Log_Get_RS
            Case "Call_Log_Detail_RQ"
                ds = New Call_Log_Detail_RQ
            Case "Call_Log_Detail_RS"
                ds = New Call_Log_Detail_RS
            Case "Call_Hotel_GetAvailability_RQ"
                ds = New Call_Hotel_GetAvailability_RQ
            Case "Call_Hotel_GetAvailability_RS"
                ds = New Call_Hotel_GetAvailability_RS
            Case "Call_PMS_Retry_RQ"
                ds = New Call_PMS_Retry_RQ
            Case "Call_PMS_Retry_RS"
                ds = New Call_PMS_Retry_RS
            Case "Call_ReportProduction_RQ"
                ds = New Call_ReportProduction_RQ
            Case "Call_ReportProduction_RS"
                ds = New Call_ReportProduction_RS
            Case "Call_SegmentsCompany_Insert_RQ"
                ds = New Call_SegmentsCompany_Insert_RQ
            Case "Call_SegmentsCompany_Insert_RS"
                ds = New Call_SegmentsCompany_Insert_RS
            Case "Call_SegmentsCompany_List_RQ"
                ds = New Call_SegmentsCompany_List_RQ
            Case "Call_SegmentsCompany_List_RS"
                ds = New Call_SegmentsCompany_List_RS
            Case "Call_SegmentsCompany_Update_RQ"
                ds = New Call_SegmentsCompany_Update_RQ
            Case "Call_SegmentsCompany_Update_RS"
                ds = New Call_SegmentsCompany_Update_RS
            Case "Call_SegmentsCompany_Delete_RQ"
                ds = New Call_SegmentsCompany_Delete_RQ
            Case "Call_SegmentsCompany_Delete_RS"
                ds = New Call_SegmentsCompany_Delete_RS


        End Select

        Dim buf() As Byte = System.Text.Encoding.UTF8.GetBytes(xml_data)
        Dim ms As New System.IO.MemoryStream(buf, 0, buf.Length)
        ds.ReadXml(ms)

        Return ds
    End Function

    Public Shared Function verifica_campos(ByVal ds As DataSet, ByVal table_name As String, ByVal indice As Integer, ByVal campos() As String) As Boolean
        'si el dataset es nulo o no contiene la tabla o la tabla no tiene renglones
        'o alguno de los campos es nulo, regresa falso. de lo contrario true (por consiguiente
        'todos los campos son validos)

        If ds Is Nothing OrElse Not ds.Tables.Contains(table_name) OrElse ds.Tables(table_name).Rows.Count = 0 Then Return False

        For Each campo As String In campos
            If ds.Tables(table_name).Rows(indice).IsNull(campo) Then Return False
        Next

        Return True
    End Function
    Public Shared Function FechaWindows(ByVal fecha As String) As String
        Dim fechaArreglo As String() = fecha.Split(" ")
        Dim fechaBarras As String() = fechaArreglo(0).Split("/")

        Dim fechaFormateada As String = fechaBarras(2) & "/" & fechaBarras(0) & "/" & fechaBarras(1) & " " & fechaArreglo(1) & " " & fechaArreglo(2)
        Dim fechaVB As Date = CDate(fechaFormateada)

        Return fechaVB.ToString("dd/MMM/yyyy HH:mm")
    End Function
    Public Shared Function SinAcentos(ByVal textoOriginal As String) As String
        Dim reg As System.Text.RegularExpressions.Regex
        Dim textoNormalizado As String = textoOriginal.Normalize(System.Text.NormalizationForm.FormD)

        reg = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9 ]")
        Dim textoSinAcentos As String = reg.Replace(textoNormalizado, "")

        Return textoSinAcentos
    End Function

    Public Shared Sub capitaliza(ByVal txt As Infragistics.Win.UltraWinEditors.UltraTextEditor)
        Dim pos_1 As Integer = txt.SelectionStart
        Dim lon_1 As Integer = txt.SelectionLength

        txt.Text = StrConv(txt.Text, vbProperCase)

        txt.SelectionStart = pos_1
        txt.SelectionLength = lon_1
    End Sub

    Public Shared Function GetSatatusText(ByVal status_dode As String) As String
        Select Case status_dode
            Case "3"
                Return CapaPresentacion.Idiomas.get_str("str_333_cancelado") ' "-Cancelado"
            Case "1"
                Return CapaPresentacion.Idiomas.get_str("str_332_reservado") ' "Reservado"
            Case "2"
                Return CapaPresentacion.Idiomas.get_str("str_334_noConfirm") ' "No confirmada"
            Case "4"
                Return CapaPresentacion.Idiomas.get_str("str_414_InProcess") ' "No confirmada"
            Case Else
                Return ""
        End Select
    End Function

    Public Shared Function GetOperationText(ByVal operationCode As String) As String
        Dim strActions() As String = CapaPresentacion.Idiomas.get_str("str_480_logActions").Split(",")
        'Reservado,Modificado,Cancelado,Reactivado
        Select Case operationCode
            Case "1"
                Return strActions(0)
            Case "2"
                Return strActions(1)
            Case "3"
                Return strActions(2)
            Case "4"
                Return strActions(3)
            Case Else
                Return ""
        End Select
    End Function

    Public Shared Sub ConfirmNumberToDetailsForm(ByVal noConfirm As String, ByVal FechaInicio As String, Optional ByVal rubro As String = Nothing)
        'busca
        Dim datosBusqueda_passport As New dataBusqueda_passport
        datosBusqueda_passport.idUsuario = dataOperador.user_ID_agent_ID
        datosBusqueda_passport.Fecha1 = DateTime.ParseExact(CDate(FechaInicio).ToString("dd/MM/yyyy"), "dd/MM/yyyy", Nothing)
        datosBusqueda_passport.Fecha2 = DateTime.Today
        datosBusqueda_passport.Type = enumFilterReservationDates.RegisterDate
        datosBusqueda_passport.Rubro = rubro
        datosBusqueda_passport.NombreClie = "@univisit@master" 'Nothing
        datosBusqueda_passport.NumeroConf = noConfirm

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_passport_GetReservations(datosBusqueda_passport)
        If IsNothing(ds) OrElse ds.Tables.Count = 0 Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_448"), MsgBoxStyle.Critical, noConfirm)
            Exit Sub
        End If
        ds.Tables("Response").Columns.Add("ServiceProvider")

        'datos
        Dim datosBusqueda_paquete As dataBusqueda_paquete = Nothing
        Dim datosBusqueda_vuelo As dataBusqueda_vuelo = Nothing
        Dim datosBusqueda_auto As dataBusqueda_auto_test = Nothing
        Dim datosBusqueda_hotel As dataBusqueda_hotel_test = Nothing
        Dim datosBusqueda_actividad As dataBusqueda_actividad_test = Nothing
        If ds IsNot Nothing AndAlso ds.Tables.Contains("Response") AndAlso ds.Tables("Response").Rows.Count > 0 Then
            Dim r As DataRow = ds.Tables("Response").Rows(0)
            'paquete
            If r.Item("idRubro") = enumRubrosPassport.Paquetes Then
                datosBusqueda_paquete = New dataBusqueda_paquete
                datosBusqueda_paquete.itinerario = r.Item("NoReservacion")
            End If
            'vuelo
            If r.Item("idRubro") = enumRubrosPassport.Vuelos Then
                datosBusqueda_vuelo = New dataBusqueda_vuelo
                datosBusqueda_vuelo.ReservationId = r.Item("idReservacion")
                datosBusqueda_vuelo.ConfirmNumber = r.Item("NoReservacion")
                datosBusqueda_vuelo.status = r.Item("Status")
            End If
            'auto
            If r.Item("idRubro") = enumRubrosPassport.Autos Then
                datosBusqueda_auto = New dataBusqueda_auto_test
                datosBusqueda_auto.IdReservation = r.Item("idReservacion")
                datosBusqueda_auto.ConfirmNumber = r.Item("NoReservacion")
                datosBusqueda_auto.Status = r.Item("Status")
            End If
            'hotel
            If r.Item("idRubro") = enumRubrosPassport.Hotel Then
                datosBusqueda_hotel = New dataBusqueda_hotel_test
                datosBusqueda_hotel.ReservationId = r.Item("idReservacion")
                datosBusqueda_hotel.ConfirmNumber = r.Item("NoReservacion")
                datosBusqueda_hotel.Status = r.Item("Status")
                If r.Item("ServiceProvider") IsNot System.DBNull.Value Then datosBusqueda_hotel.ServiceProvider = r.Item("ServiceProvider")
            End If
            'actividad
            If r.Item("idRubro") = enumRubrosPassport.Actividades Then
                datosBusqueda_actividad = New dataBusqueda_actividad_test
                datosBusqueda_actividad.ReservationID = r.Item("idReservacion")
                datosBusqueda_actividad.ConfirmNumber = r.Item("NoReservacion")
                datosBusqueda_actividad.status = r.Item("Status")
            End If
        End If

        'muestra
        Dim frm As New detalles()
        frm.MdiParent = misForms.form1
        frm.datosBusqueda_paquete = datosBusqueda_paquete
        frm.datosBusqueda_hotel = datosBusqueda_hotel
        frm.datosBusqueda_vuelo = datosBusqueda_vuelo
        frm.datosBusqueda_auto = datosBusqueda_auto
        frm.datosBusqueda_actividad = datosBusqueda_actividad
        frm.load_data(False)
        frm.Show()
    End Sub

    Public Shared Function get_Paises() As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from paises order by nombre"
            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

    Public Shared Function get_Estados(ByVal idPais As String) As DataSet
        Try

            Dim conn As New System.Data.SqlClient.SqlConnection()
            conn.ConnectionString = CapaAccesoDatos.XML.localData.ServidorSQL

            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "Select * from estados where idPais='" & idPais & "' order by nombre"

            Dim adapter As New System.Data.SqlClient.SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0072", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

End Class
