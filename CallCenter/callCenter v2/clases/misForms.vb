Public Class misForms

    Public Shared form1 As Form1_callCenter = Nothing
    Public Shared reservaciones As Reservaciones = Nothing
    Public Shared reservar As reservar = Nothing
    Public Shared habitaciones As habitaciones = Nothing
    Public Shared busquedaHotel_ultimoActivo As busquedaHotel = Nothing
    Public Shared tarifas As tarifas = Nothing
    Public Shared newCall As newCall = Nothing
    Public Shared listaLlamadas As listaLlamadas = Nothing
    Public Shared busquedaVuelo_ultimoActivo As busquedaVuelo = Nothing
    Public Shared busquedaActividad_ultimoActivo As busquedaActividad = Nothing
    Public Shared eventosActividades_ultimoActivo As eventosActividades = Nothing
    Public Shared busquedaAutos_ultimoActivo As busquedaAutos = Nothing
    Public Shared tarifasAutos_ultimoActivo As tarifasAuto = Nothing

    '------------------------------------------------------------------------------------------------

    Public Shared Sub exe_accion(ByVal elMenu As ContextMenuStrip, ByVal key As String)
        Try
            'misForms.form1.Cursor = Cursors.WaitCursor
            'misForms.form1.UltraToolbarsManager1.Cursor = Cursors.WaitCursor

            Dim tool As Infragistics.Win.UltraWinToolbars.ToolBase = misForms.form1.UltraToolbarsManager1.Tools(key)
            Dim item As ToolStripItem
            If elMenu Is Nothing Then
                item = Nothing
            Else
                item = elMenu.Items(key)
            End If

            Select Case key
                Case "btn_copiar", "btn_cambios", "btn_nuevaLlamada", "btn_listaLlamadas", "btn_reservaciones", "btn_detalles", "btn_configurarAgente", "btn_configurarConexion", "btn_editarLlamada", "btn_modificar", "btn_espa�ol", "btn_ingles", "btn_cerrarSesion", "btn_salir", "btn_cancelar", "btn_cambiarLogo", "btn_reactivar", "btn_ProfessionalsAlta", "btn_professionals_reporte", "btn_ProfessionalsConfiguracion", "btn_reporte_produccion", "btn_SegmentCompany"
                    exe_sistema(key, elMenu, item, tool)
                Case "btn_buscar", "btn_tarifas", "bya tn_informacionGeneral", "btn_habitaciones", "btn_galeria", "btn_reservar", "btn_reglas", "btn_mapa", "btn_hotel_disponibilidad", "btn_ver_detalles"
                    exe_hoteles(key, tool)
                Case "btn_buscar_vuelo", "btn_reservar_vuelo", "btn_reglas_vuelo"
                    exe_vuelos(key)
                Case "btn_buscarActividad", "btn_eventosActividad", "btn_reservarActividad", "btn_reglasActividad"
                    exe_actividades(key)
                Case "btn_buscarAuto", "btn_tarifasAuto", "btn_reservarAuto", "btn_reglasAuto"
                    exe_autos(key)

            End Select

            If Not misForms.form1 Is Nothing Then
                misForms.form1.Cursor = Cursors.Arrow
                misForms.form1.UltraToolbarsManager1.Cursor = Cursors.Arrow
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0074", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Dim exeptionDialog As New Form()

            exeptionDialog.Dispose()
        End Try
    End Sub

    Private Shared Sub exe_sistema(ByVal key As String, ByVal elMenu As ContextMenuStrip, ByVal item As ToolStripItem, ByVal tool As Infragistics.Win.UltraWinToolbars.ToolBase)
        Select Case key
            '   ' los MDI
            Case "btn_nuevaLlamada"
                Dim frm As New newCall
                frm.MdiParent = misForms.form1
                frm.Show()
            Case "btn_listaLlamadas"
                Dim frm As New listaLlamadas
                frm.MdiParent = misForms.form1
                frm.Show()
            Case "btn_reservaciones"
                Dim frm As New reservaciones()
                frm.MdiParent = misForms.form1
                CapaLogicaNegocios.Reservations.llena_combos(frm)
                'frm.load_data()
                frm.Show()
                tool.SharedProps.Enabled = False
                If Not item Is Nothing Then item.Enabled = False
            Case "btn_reporte_produccion"
                Dim frm As New ProductionReport()
                frm.MdiParent = misForms.form1

                frm.Show()
                tool.SharedProps.Enabled = False
                If Not item Is Nothing Then item.Enabled = False
            Case "btn_detalles"
                Dim frm As New detalles()
                frm.MdiParent = misForms.form1
                frm.datosBusqueda_paquete = misForms.reservaciones.datosBusqueda_paquete
                frm.datosBusqueda_hotel = misForms.reservaciones.datosBusqueda_hotel
                frm.datosBusqueda_vuelo = misForms.reservaciones.datosBusqueda_vuelo
                frm.datosBusqueda_auto = misForms.reservaciones.datosBusqueda_auto
                frm.datosBusqueda_actividad = misForms.reservaciones.datosBusqueda_actividad
                frm.load_data(True)
                frm.Show()
                'tool.SharedProps.Enabled = False
                'If Not item Is Nothing Then item.Enabled = False
                '''


                ' los UC
                'Case "btn_detalles"
                'show_UC("detallesReservacion", "Detalles de reservaci�n", GetType(uc_detallesReservacion).FullName)


                ' los POP UP
            Case "btn_configurarAgente"
                Dim frm As New configurarAgente
                frm.ShowDialog()
            Case "btn_configurarConexion"
                Dim frm As New configurarConexion
                frm.ShowDialog()
            Case "btn_editarLlamada"
                'Dim frm As New newCall
                'frm.modificar = True
                'frm.inicializa_controles()
                'frm.ShowDialog()
                Dim idCall As Integer = misForms.listaLlamadas.UltraGrid1.ActiveRow.Cells("idCall").Value
                Dim wsCall As New webService_call()
                Dim dc As dataCall_ = wsCall.get_call(idCall, Nothing, Nothing, Nothing, Nothing, Nothing)
                '
                For Each datosBusqueda As dataBusqueda_actividad_test In dc.reservacion_paquete.lista_Actividades
                    herramientas.ConfirmNumberToDetailsForm(datosBusqueda.ConfirmNumber, misForms.listaLlamadas.UltraGrid1.ActiveRow.Cells("inicio").Value, 27)
                Next
                For Each datosBusqueda As dataBusqueda_auto_test In dc.reservacion_paquete.lista_Autos
                    herramientas.ConfirmNumberToDetailsForm(datosBusqueda.ConfirmNumber, misForms.listaLlamadas.UltraGrid1.ActiveRow.Cells("inicio").Value, 11)
                Next
                For Each datosBusqueda As dataBusqueda_hotel_test In dc.reservacion_paquete.lista_Hoteles
                    herramientas.ConfirmNumberToDetailsForm(datosBusqueda.ConfirmNumber, misForms.listaLlamadas.UltraGrid1.ActiveRow.Cells("inicio").Value, 10)
                Next
                For Each datosBusqueda As dataBusqueda_vuelo In dc.reservacion_paquete.lista_Vuelos
                    herramientas.ConfirmNumberToDetailsForm(datosBusqueda.ConfirmNumber, misForms.listaLlamadas.UltraGrid1.ActiveRow.Cells("inicio").Value, 23)
                Next
                'Dim frm As New detalles()
                'frm.MdiParent = misForms.form1
                'frm.datosBusqueda_paquete = misForms.reservaciones.datosBusqueda_paquete
                'frm.datosBusqueda_hotel = misForms.reservaciones.datosBusqueda_hotel
                'frm.datosBusqueda_vuelo = misForms.reservaciones.datosBusqueda_vuelo
                'frm.datosBusqueda_auto = misForms.reservaciones.datosBusqueda_auto
                'frm.datosBusqueda_actividad = misForms.reservaciones.datosBusqueda_actividad
                'frm.load_data(True)
                'frm.Show()
            Case "btn_copiar"

                If MsgBox(CapaPresentacion.Idiomas.get_str("str_520"), MsgBoxStyle.Question + MsgBoxStyle.YesNo, CapaPresentacion.Idiomas.get_str("str_521")) = MsgBoxResult.Yes Then
                    Dim res As callCenter.CallOption.CallResult = CapaLogicaNegocios.Calls.SelectOption()
                    If res <> CallOption.CallResult.Cancel Then

                        Dim arr_data As dataBusqueda_hotel_test
                        If misForms.reservaciones Is Nothing AndAlso misForms.reservaciones.UltraGrid1.ActiveRow Is Nothing Then
                        Else
                            arr_data = get_datosBusqueda_fromReservaciones(misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idReservacion").Value.ToString, misForms.reservaciones.UltraGrid1.ActiveRow.Cells("Source").Value.ToString)
                            misForms.newCall.datos_call.reservacion_paquete.cc_numero = String.Empty
                            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = String.Empty
                            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = String.Empty
                            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = String.Empty

                            If arr_data Is Nothing Then
                                MsgBox(CapaPresentacion.Idiomas.get_str("str_448"))
                                Exit Sub
                            Else
                                'Establecer informacion de reservacion a copiar

                                arr_data.IsCopy = True
                                arr_data.ConfirmNumber = ""
                                arr_data.ReservationId = ""
                                arr_data.Status = ""

                                Dim frm As New busquedaHotel() 'dc
                                frm.MdiParent = misForms.form1
                                frm.datosBusqueda_hotel = arr_data.Clone()

                                frm.Show()
                            End If
                        End If
                    End If
                End If


            Case "btn_modificar"
                Dim res As callCenter.CallOption.CallResult = CapaLogicaNegocios.Calls.SelectOption()
                Dim ds_display As DataSet = New DataSet()

                If res <> CallOption.CallResult.Cancel Then
                    Dim arr_data As dataBusqueda_hotel_test
                    If misForms.reservaciones Is Nothing AndAlso misForms.reservaciones.UltraGrid1.ActiveRow Is Nothing Then
                    Else
                        arr_data = get_datosBusqueda_fromReservaciones(misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idReservacion").Value.ToString, misForms.reservaciones.UltraGrid1.ActiveRow.Cells("Source").Value.ToString)
                        If arr_data Is Nothing Then
                            MsgBox(CapaPresentacion.Idiomas.get_str("str_448"))
                            Exit Sub
                        Else
                            If IsNothing(arr_data.DepositAmountCCT) Then arr_data.DepositAmountCCT = "0"
                            If arr_data.Source <> "CCT" And arr_data.DepositAmountCCT <> "0" Then
                                MsgBox(CapaPresentacion.Idiomas.get_str("str_453"))
                                Exit Sub
                            End If
                        End If

                        Dim datosDisplay As New datos_display
                        datosDisplay.ReservationId = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idReservacion").Value.ToString
                        datosDisplay.AltCurrency = moneda_selected
                        ds_display = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)

                    End If
                    Dim frm As New reservar()
                    frm.datosBusqueda_hotel = arr_data
                    frm.tipoAccion = enumReservar_TipoAccion.modificar
                    CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.modificar

                    'checar eso del nothing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    If frm.cargar(Nothing) Then
                        frm.ShowDialog()
                        If frm.Ok Then
                            Dim frm2 As New detalles()
                            frm2.MdiParent = misForms.form1
                            frm2.registraBitacora = detalles.BitacoraAccion.Editar
                            frm2.DisplayRsXML_Antes = ds_display.GetXml()
                            frm2.datosBusqueda_hotel = frm.datosBusqueda_hotel
                            frm2.load_data(True)
                            frm2.Show()

                        End If
                    Else
                        MsgBox(CapaPresentacion.Idiomas.get_str("str_448"))
                    End If
                End If

            Case "btn_cambios"
                Dim res As callCenter.CallOption.CallResult = CapaLogicaNegocios.Calls.SelectOption()
                Dim ds_display As DataSet = New DataSet()

                If res <> CallOption.CallResult.Cancel Then
                    Dim arr_data As dataBusqueda_hotel_test
                    If misForms.reservaciones Is Nothing AndAlso misForms.reservaciones.UltraGrid1.ActiveRow Is Nothing Then
                    Else
                        arr_data = get_datosBusqueda_fromReservaciones(misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idReservacion").Value.ToString, misForms.reservaciones.UltraGrid1.ActiveRow.Cells("Source").Value.ToString)
                        If arr_data Is Nothing Then
                            MsgBox(CapaPresentacion.Idiomas.get_str("str_448"))
                            Exit Sub
                        Else
                            If IsNothing(arr_data.DepositAmountCCT) Then arr_data.DepositAmountCCT = "0"
                            If arr_data.Source <> "CCT" And arr_data.DepositAmountCCT <> "0" Then
                                MsgBox(CapaPresentacion.Idiomas.get_str("str_453"))
                                Exit Sub
                            End If
                        End If

                        Dim datosDisplay As New datos_display
                        datosDisplay.ReservationId = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idReservacion").Value.ToString
                        datosDisplay.AltCurrency = moneda_selected
                        ds_display = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)

                    End If
                    CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.cambiar
                    Dim frm_cambios As New cambiar_reservacion()

                    frm_cambios.datosBusqueda_hotel = arr_data
                    frm_cambios.ShowIcon = True
                    frm_cambios.ShowDialog()

                    'Dim frm As New reservar()



                    'frm.tipoAccion = enumReservar_TipoAccion.cambiar
                    'CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.cambiar
                    'checar eso del nothing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    'If frm.cargar(Nothing) Then
                    'frm.ShowDialog()
                    'If frm.Ok Then
                    'Dim frm2 As New detalles()
                    'frm2.MdiParent = misForms.form1
                    'frm2.registraBitacora = detalles.BitacoraAccion.Editar
                    'frm2.DisplayRsXML_Antes = ds_display.GetXml()
                    'frm2.datosBusqueda_hotel = frm.datosBusqueda_hotel
                    'frm2.load_data(True)
                    'frm2.Show()

                    'End If
                    'Else
                    'MsgBox(CapaPresentacion.Idiomas.get_str("str_448"))
                    'End If
                End If
            Case "btn_reactivar"
                Dim arr_data As dataBusqueda_hotel_test
                arr_data = get_datosBusqueda_fromReservaciones(misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idReservacion").Value.ToString, misForms.reservaciones.UltraGrid1.ActiveRow.Cells("Source").Value.ToString)
                If arr_data Is Nothing Then
                    MsgBox(CapaPresentacion.Idiomas.get_str("str_448"))
                    Exit Sub
                End If
                Dim detalle As String
                detalle = vbCrLf & CapaPresentacion.Idiomas.get_str("str_0149_numConfirmacion") & ": " & arr_data.ConfirmNumber & vbCrLf & CapaPresentacion.Idiomas.get_str("str_0061_cliente") & " :" & misForms.reservaciones.UltraGrid1.ActiveRow.Cells("Cliente").Value.ToString

                If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_431_confirmreactive") & detalle, CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    If ReactiveReservation(arr_data.ConfirmNumber, arr_data.PropertyNumber, dataOperador.idCorporativo) Then
                        'recargar
                        MsgBox(CapaPresentacion.Idiomas.get_str("str_430_reactivemsg"), MsgBoxStyle.Information)
                        misForms.reservaciones.load_data()
                        'Registramos el la bitacora de movimientos
                        Dim logEntry As New webService_call
                        logEntry.LogInsert(detalles.BitacoraAccion.Reactivar, arr_data.ConfirmNumber, "", "", "")
                    End If
                End If

            Case "btn_cambiarLogo"
                Dim frm As New ConfigureImage
                frm.ShowDialog()


                'state btns
            Case "btn_espa�ol", "btn_ingles"
                herramientas.cambiar_idioma(tool)


                'sin form
            Case "btn_cerrarSesion"
                CapaAccesoDatos.XML.localData.Moneda = moneda_selected
                If Not misForms.form1.configura Then ws_login.UserLogOut()
                misForms.form1.Visible = False
                Dim frm As New login
                frm.Show()
                misForms.form1.PreguntaAlCerrar = False
                misForms.form1.Close()
            Case "btn_salir"
                misForms.form1.Close()
            Case "btn_cancelar"
                If CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = True Then
                    If Not elMenu Is Nothing Then elMenu.Hide()

                    Dim dat_paq As dataBusqueda_paquete
                    Dim dat_hot As dataBusqueda_hotel_test
                    Dim dat_vue As dataBusqueda_vuelo
                    Dim dat_aut As dataBusqueda_auto_test
                    Dim dat_act As dataBusqueda_actividad_test
                    Dim idRubro As String = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("idRubro").Value
                    If misForms.reservaciones.datosBusqueda_paquete IsNot Nothing Then dat_paq = misForms.reservaciones.datosBusqueda_paquete.Clone
                    If misForms.reservaciones.datosBusqueda_hotel IsNot Nothing Then dat_hot = misForms.reservaciones.datosBusqueda_hotel.Clone
                    If misForms.reservaciones.datosBusqueda_vuelo IsNot Nothing Then dat_vue = misForms.reservaciones.datosBusqueda_vuelo.Clone
                    If misForms.reservaciones.datosBusqueda_auto IsNot Nothing Then dat_aut = misForms.reservaciones.datosBusqueda_auto.Clone
                    If misForms.reservaciones.datosBusqueda_actividad IsNot Nothing Then dat_act = misForms.reservaciones.datosBusqueda_actividad.Clone


                    If cancelar_reservacion(idRubro, dat_vue, dat_act, dat_hot, dat_aut, dat_paq, misForms.reservaciones.Cliente) Then
                        'Recargamos la forma
                        misForms.reservaciones.load_data()
                    End If
                End If
            Case "btn_ProfessionalsAlta"
                Dim frm As New AltaSociosRewards
                frm.ShowDialog()

                'Dim frm As New AltaSocios
                'frm.ShowDialog()
            Case "btn_professionals_reporte"
                Dim frm As New SociosRewards
                frm.MdiParent = misForms.form1
                frm.Show()
                'Dim frm As New Socios
                'frm.MdiParent = misForms.form1
                'frm.Show()
            Case "btn_ProfessionalsConfiguracion"
                'Dim frm As New ConfiguracionProfessionals
                'frm.ShowDialog()
            Case "btn_SegmentCompany"
                Dim frm As New SegmentCompanyList
                frm.ShowDialog()
        End Select
    End Sub

    Private Shared Sub exe_hoteles(ByVal key As String, ByVal tool As Infragistics.Win.UltraWinToolbars.ToolBase)
        Select Case key
            '   ' los MDI
            Case "btn_buscar"
                Dim frm As New busquedaHotel() 'dc
                frm.MdiParent = misForms.form1
                frm.Show()
            Case "btn_tarifas"
                mostrar_tabForm_tar()


                ' los UC
            Case "btn_informacionGeneral"
                show_UC("informacionGeneral", CapaPresentacion.Idiomas.get_str("str_0112_infoGeneralHotel"), GetType(uc_informacionGeneral).FullName)


                ' los POP UP
            Case "btn_habitaciones"
                Dim frm As New habitaciones
                frm.datosBusqueda_hotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.Clone
                frm.ShowDialog()
            Case "btn_galeria"
                Dim frm As New galeria
                frm.datosBusqueda_hotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.Clone
                frm.ShowDialog()
            Case "btn_reservar"
                If CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Enabled = True Then
                    Dim arr_data As dataBusqueda_hotel_test = get_datosBusqueda_fromTarifa()
                    Dim dat As dataBusqueda_hotel_test = arr_data

                    Dim frm1 As New reglas
                    frm1.datosBusqueda_hotel = dat
                    frm1.cargar(Nothing)
                    frm1.ShowDialog()

                    If frm1.ds IsNot Nothing AndAlso frm1.ds.Tables.Contains("error") AndAlso frm1.ds.Tables("error").Rows.Count > 0 Then
                        '
                    Else
                        Dim frm2 As New reservar()
                        frm2.datosBusqueda_hotel = dat
                        If frm2.cargar(frm1.ds) Then
                            frm2.ShowDialog()
                            CapaPresentacion.Calls.ActivaNewCall()
                        Else
                            'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_378_errServ"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                    End If
                End If
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_378_errServ"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Case "btn_reglas"
                Dim frm As New reglas
                frm.datosBusqueda_hotel = get_datosBusqueda_fromTarifa()
                frm.cargar(Nothing)
                frm.ShowDialog()
            Case "btn_mapa"
                Dim frm As New mapa
                frm.datosBusqueda_hotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.Clone
                frm.ShowDialog()


                'sin form
                'xxxxx
            Case "btn_hotel_disponibilidad"
                Dim frm As New frm_disponibilidad
                frm.idHotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.PropertyNumber
                frm.NombreHotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.PropertyName
                frm.ShowDialog()
            Case "btn_ver_detalles"
                Dim frm As New tarifas_ver_detalles
                Dim ratecode As String = get_datosBusqueda_fromTarifa().RateCode
                frm.Cargar(ratecode, tarifas.UltraGrid1.DataSource)
                Dim result As DialogResult = frm.ShowDialog()
        End Select
    End Sub

    Private Shared Sub exe_vuelos(ByVal key As String)
        Select Case key
            '   ' los MDI
            Case "btn_buscar_vuelo"
                Dim frm As New busquedaVuelo()
                frm.MdiParent = misForms.form1
                frm.Show()
            Case "btn_tarifas"
                'xxxxx

                ' los UC
                'xxxxx


                ' los POP UP
            Case "btn_reservar_vuelo"
                Dim f As busquedaVuelo = misForms.busquedaVuelo_ultimoActivo

                For Each ctl As Control In f.Uc_itinerario1.lista_de_vuelos
                    If CType(ctl, uc_vuelo).IS_SELECTED Then f.datosBusqueda_vuelo.vuelo = CType(ctl, uc_vuelo).data_source
                Next

                Dim frm2 As New reglasVuelo(misForms.busquedaVuelo_ultimoActivo.datosBusqueda_vuelo)
                frm2.ShowDialog()

                Dim frm3 As New ReservarVuelo
                frm3.datosBusqueda_vuelo = f.datosBusqueda_vuelo.Clone
                frm3.tipoAccion = enumReservar_TipoAccion.reservar
                CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.reservar
                If frm3.cargar() Then frm3.ShowDialog()
                CapaPresentacion.Calls.ActivaNewCall()
            Case "btn_reglas_vuelo"
                Dim frm As New reglasVuelo(misForms.busquedaVuelo_ultimoActivo.datosBusqueda_vuelo)
                frm.ShowDialog()


                'sin form
                'xxxx
        End Select
    End Sub

    Private Shared Sub exe_actividades(ByVal key As String)
        Select Case key
            '   ' los MDI
            Case "btn_buscarActividad"
                Dim frm As New busquedaActividad()
                frm.MdiParent = misForms.form1
                frm.Show()
            Case "btn_eventosActividad"
                Dim frm As New eventosActividades(misForms.busquedaActividad_ultimoActivo.datosBusqueda_actividad.Clone) 'dc,
                frm.MdiParent = misForms.form1
                frm.Show()


                ' los UC
                'xxxxx


                ' los POP UP
            Case "btn_reservarActividad"
                Dim frm1 As New reglasActividad(misForms.eventosActividades_ultimoActivo.datosBusqueda_actividad.Clone) 'dc, 
                frm1.ShowDialog()

                Dim frm2 As New reservarActividad
                frm2.datosBusqueda_actividad = misForms.eventosActividades_ultimoActivo.datosBusqueda_actividad.Clone
                frm2.tipoAccion = enumReservar_TipoAccion.reservar
                CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.reservar
                If frm2.cargar() Then frm2.ShowDialog()
                CapaPresentacion.Calls.ActivaNewCall()
            Case "btn_reglasActividad"
                Dim frm As New reglasActividad(misForms.eventosActividades_ultimoActivo.datosBusqueda_actividad.Clone) 'dc,
                frm.ShowDialog()


                'sin form
                'xxxx
        End Select
    End Sub

    Private Shared Sub exe_autos(ByVal key As String)
        Select Case key
            '   ' los MDI
            Case "btn_buscarAuto"
                Dim frm As New busquedaAutos()
                frm.MdiParent = misForms.form1
                frm.Show()
            Case "btn_tarifasAuto"
                Dim frm As New tarifasAuto(misForms.busquedaAutos_ultimoActivo.datosBusqueda_auto.Clone) 'dc,
                frm.MdiParent = misForms.form1
                frm.Show()


                ' los UC
                'xxxxx


                ' los POP UP
            Case "btn_reservarAuto"
                Dim frm1 As New reglasAuto(misForms.tarifasAutos_ultimoActivo.datosBusqueda_auto.Clone) 'dc,
                frm1.ShowDialog()

                Dim frm2 As New reservarAuto
                frm2.datosBusqueda_auto = misForms.tarifasAutos_ultimoActivo.datosBusqueda_auto.Clone
                frm2.tipoAccion = enumReservar_TipoAccion.reservar
                CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.reservar
                If frm2.cargar() Then frm2.ShowDialog()
                CapaPresentacion.Calls.ActivaNewCall()
            Case "btn_reglasAuto"
                Dim frm As New reglasAuto(misForms.tarifasAutos_ultimoActivo.datosBusqueda_auto.Clone) 'dc,
                frm.ShowDialog()


                'sin form
                'xxxx
        End Select
    End Sub

    '---------------------------------------------------------------------

    Private Shared Sub mostrar_tabForm_tar()
        Try
            If misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow Is Nothing Then Return
            misForms.form1.UltraToolbarsManager1.Ribbon.SelectedTab = misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon2")
            Dim frm As New tarifas()
            Dim tem_settings As Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings = misForms.form1.UltraTabbedMdiManager1.TabFromForm(misForms.busquedaHotel_ultimoActivo).Settings

            If buscar_frmTar_repetido() Is Nothing Then
                frm.MdiParent = misForms.form1

                frm.datosBusqueda_hotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.Clone

                frm.cargar()
                frm.Show()
                'If  Then  Else misForms.busquedaHotel_ultimoActivo.Cursor = Cursors.Arrow

                misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.TabAppearance = tem_settings.TabAppearance
                misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm).Settings.SelectedTabAppearance = tem_settings.SelectedTabAppearance
            Else
                frm = buscar_frmTar_repetido()
                frm.cargar()
            End If

            Dim indice As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup = herramientas.get_mdiTabGroup_de(frm.Name, frm.datosBusqueda_hotel.PropertyNumber)
            If Not indice Is Nothing Then misForms.form1.UltraTabbedMdiManager1.TabGroups(indice.Index).SelectedTab = misForms.form1.UltraTabbedMdiManager1.TabFromForm(frm) '"tarifas"
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0075", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Shared Function buscar_frmTar_repetido() As tarifas
        For Each g As Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup In misForms.form1.UltraTabbedMdiManager1.TabGroups
            For Each t As Infragistics.Win.UltraWinTabbedMdi.MdiTab In g.Tabs
                If t.Form.Name = "tarifas" Then
                    Dim frm As tarifas = t.Form
                    Dim iguales As Boolean = True

                    iguales = frm.datosBusqueda_hotel.Compara(misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel)

                    If iguales Then Return frm
                End If
            Next
        Next

        Return Nothing
    End Function

    Private Shared Function get_datosBusqueda_fromTarifa() As dataBusqueda_hotel_test
        Dim dat_bus As dataBusqueda_hotel_test = misForms.tarifas.datosBusqueda_hotel.Clone
        If misForms.tarifas.UltraGrid1.ActiveRow.Cells.Exists("PlanCode") Then
            dat_bus.RateCode = misForms.tarifas.UltraGrid1.ActiveRow.Cells("PlanCode").Value
        Else
            dat_bus.RateCode = misForms.tarifas.UltraGrid1.ActiveRow.ParentRow.Cells("PlanCode").Value
        End If
        dat_bus.GuarDep = misForms.tarifas.UltraGrid1.ActiveRow.Cells("GuarDep").Value
        dat_bus.TotalAvgRate = misForms.tarifas.UltraGrid1.ActiveRow.Cells("TotalAvgRate").Value
        dat_bus.AvgRate = misForms.tarifas.UltraGrid1.ActiveRow.Cells("AvgRate").Value
        dat_bus.AltTotalStay = misForms.tarifas.UltraGrid1.ActiveRow.Cells("AltTotalStay").Value
        dat_bus.nombreHabitacion = misForms.tarifas.UltraGrid1.ActiveRow.Cells("nombreHabitacion").Value
        dat_bus.PlanName = misForms.tarifas.UltraGrid1.ActiveRow.Cells("PlanName").Value
        dat_bus.PlanDescription = misForms.tarifas.UltraGrid1.ActiveRow.Cells("PlanDescription").Value
        dat_bus.Segmento = misForms.tarifas.UltraGrid1.ActiveRow.Cells("Segment").Value
        If dat_bus.Segmento = "K" AndAlso misForms.tarifas.UltraGrid1.ActiveRow.Cells.Exists("Nights") Then
            dat_bus.salida = dat_bus.llegada.AddDays(CInt(misForms.tarifas.UltraGrid1.ActiveRow.Cells("Nights").Value))
            dat_bus.noches = (dat_bus.salida - dat_bus.llegada).Days.ToString
        End If

        Return dat_bus
    End Function

    Public Shared Function get_datosBusqueda_fromReservaciones(ByVal ReservationId As String, ByVal Source As String) As dataBusqueda_hotel_test
        'If misForms.reservaciones Is Nothing AndAlso misForms.reservaciones.UltraGrid1.ActiveRow Is Nothing Then Return Nothing

        Dim dat_bus As New dataBusqueda_hotel_test
        dat_bus.ServiceProvider = 0
        'dat_bus.PropertyNumber = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("PropertyNumber").Value.ToString
        'dat_bus.ConfirmNumber = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("noConfirm").Value.ToString
        'dat_bus.ChainCode = misForms.reservaciones.UltraGrid1.ActiveRow.Cells("ChainCode").Value.ToString
        'Dim ds As DataSet = obtenGenerales_hoteles.obten_general_display(dat_bus)
        Dim datosDisplay As New datos_display
        datosDisplay.ReservationId = ReservationId
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)
        If ds Is Nothing Then Return Nothing

        dat_bus.Source = Source
        With ds.Tables("Reservation").Rows(0)
            dat_bus.PropertyNumber = .Item("PropertyNumber")
            dat_bus.PropertyName = .Item("HotelName")
            dat_bus.ConfirmNumber = .Item("ConfirmNumber")
            dat_bus.ChainCode = .Item("ChainCode")
            dat_bus.llegada = .Item("CheckInDate")
            dat_bus.salida = .Item("CheckOutDate")
            dat_bus.codProm = .Item("AccessCode")
            dat_bus.noches = (dat_bus.salida - dat_bus.llegada).Days.ToString
            dat_bus.Status = .Item("Status")
            dat_bus.isOnlinePayment = .Item("OnlinePayment")
            dat_bus.isPayPalPayment = .Item("ReservedByPayPal")
            dat_bus.isPayUPayment = .Item("ReservedByPayU")
            dat_bus.isAmericanExpress = .Item("ReservedByAmericanExpress")
            dat_bus.isConektaPayment = .Item("ReservedByConekta")

            dat_bus.Idioma = Integer.Parse(.Item("IdIdiomaReservation"))

            If dat_bus.isOnlinePayment Then
                dat_bus.DepositType = enumDepositType.Online
            End If
            If dat_bus.isPayPalPayment Then
                dat_bus.DepositType = enumDepositType.paypal
            End If
            If dat_bus.isPayUPayment Then
                dat_bus.DepositType = enumDepositType.payU
            End If
            If dat_bus.isAmericanExpress Then
                dat_bus.DepositType = enumDepositType.americanExpress
            End If
            If dat_bus.isConektaPayment Then
                dat_bus.DepositType = enumDepositType.conekta
            End If


            dat_bus.NoAutorizacion = IIf(Not (.Item("NoAutorizacion") Is Nothing), .Item("NoAutorizacion"), String.Empty)

            If ds.Tables("Reservation").Columns.Contains("PaymentSource") AndAlso Not ds.Tables("Reservation").Rows(0).IsNull("PaymentSource") Then
                dat_bus.PaymentSource = IIf(Not (.Item("PaymentSource") Is Nothing), Integer.Parse(.Item("PaymentSource")), 0)
            Else
                dat_bus.PaymentSource = 0
            End If
            If ds.Tables("Reservation").Columns.Contains("MonthInterestBanorte") AndAlso Not ds.Tables("Reservation").Rows(0).IsNull("MonthInterestBanorte") Then
                dat_bus.monthBanorte = ds.Tables("Reservation").Rows(0).Item("MonthInterestBanorte")
            End If
        End With



        'dat_bus.habitaciones_.ListaHabitaciones.Clear()
        dat_bus.habitaciones_ = New uc_habitaciones.T_Habitaciones
        Dim peticionEsp As String = ""
        For Each r_room As DataRow In ds.Tables("Room").Rows
            Dim hab As New uc_habitaciones.T_Habitacion
            hab.NumAdultos = CInt(r_room("Adults"))
            hab.NumNinos = CInt(r_room("Children"))
            Dim childresAges As New List(Of Integer)
            If (hab.NumNinos > 0) Then
                Dim arrayChildrenAges() As String = Split(r_room("ChildrenAges"), ",")
                For counterArray As Byte = 0 To (arrayChildrenAges.Length - 1)
                    childresAges.Add(CInt(arrayChildrenAges(counterArray)))
                Next
                hab.EdadesNinos = childresAges
            End If
            dat_bus.habitaciones_.ListaHabitaciones.Add(hab)
            If peticionEsp = "" Then
                peticionEsp = r_room("Preferences")
            Else
                'If r_room("Preferences") <> "" Then
                '    peticionEsp += ", " + r_room("Preferences")
                'End If
            End If
        Next
        dat_bus.RateCode = ds.Tables("Reservation").Rows(0).Item("RateCode")
        dat_bus.GuarDep = ds.Tables("Reservation").Rows(0).Item("GuarDep")
        dat_bus.PlusTax = ds.Tables("Reservation").Rows(0).Item("PlusTax")
        dat_bus.AltTax = ds.Tables("Reservation").Rows(0).Item("AltTaxes")
        dat_bus.AltCurrency = ds.Tables("Reservation").Rows(0).Item("AltMoney")
        dat_bus.isNetRate = ds.Tables("Reservation").Rows(0).Item("IsNetRateUV")
        Dim subtotal As Double = CDbl(ds.Tables("Reservation").Rows(0).Item("Total"))
        Dim taxes As Double = CDbl(ds.Tables("Reservation").Rows(0).Item("Taxes"))
        If (dat_bus.isNetRate) Then
            dat_bus.TotalNR = ds.Tables("Reservation").Rows(0).Item("TotalNR")
        End If
        If Not CBool(dat_bus.PlusTax) Then dat_bus.AltTotalStay = subtotal - taxes Else dat_bus.AltTotalStay = subtotal
        ReDim dat_bus.PrecioTarifa(ds.Tables("Rate").Rows.Count - 1)
        ReDim dat_bus.precioNR(ds.Tables("Rate").Rows.Count - 1)
        ReDim dat_bus.adultsExtra(ds.Tables("Rate").Rows.Count - 1)
        ReDim dat_bus.childrenExtra(ds.Tables("Rate").Rows.Count - 1)
        For counter As Byte = 0 To ds.Tables("Rate").Rows.Count - 1
            dat_bus.PrecioTarifa(counter) = ds.Tables("Rate").Rows(counter).Item("AdultRate")
            If (dat_bus.isNetRate) Then
                dat_bus.precioNR(counter) = ds.Tables("Rate").Rows(counter).Item("AdultRateNR")
            End If
            dat_bus.adultsExtra(counter) = ds.Tables("Rate").Rows(counter).Item("ExtraRateAdult")
            dat_bus.childrenExtra(counter) = ds.Tables("Rate").Rows(counter).Item("ExtraRateChild")
        Next


        dat_bus.cust_Preferences = peticionEsp

        dat_bus.Comments = ""
        dat_bus.frecuentCode = ds.Tables("Reservation").Rows(0).Item("frecuentcode")

        Try
            dat_bus.DepositTarget = ds.Tables("Reservation").Rows(0).Item("DepositTarget")
            dat_bus.DepositReference = ds.Tables("Reservation").Rows(0).Item("DepositReference")
            dat_bus.Comments = ds.Tables("Reservation").Rows(0).Item("CommentByVendor")
            dat_bus.DepositAmountCCT = FormatCurrency(ds.Tables("Reservation").Rows(0).Item("DepositAmount"), 2).Replace("$", "").Replace(",", "")
            dat_bus.DepositInfoCCT = ds.Tables("Reservation").Rows(0).Item("DepositInfo")
            dat_bus.DepositLimitDate = DateTime.ParseExact(ds.Tables("Reservation").Rows(0).Item("DepositLimitDate"), "yyyyMMdd", Nothing).ToString("MM\/dd\/yyyy")
        Catch ex As Exception

        End Try

        Dim depositAmount As Decimal = 0
        If dat_bus.DepositType = enumDepositType.None Then


            If CBool(ds.Tables("Reservation").Rows(0).Item("ReservedBySpecificPayment")) Then
                dat_bus.DepositType = enumDepositType.Other
                dat_bus.PaymentOther = ds.Tables("Reservation").Rows(0).Item("PaymentInformation")
            ElseIf ((Decimal.TryParse(ds.Tables("Reservation").Rows(0).Item("DepositAmount"), depositAmount) AndAlso depositAmount <> 0) _
                OrElse Not String.IsNullOrEmpty(ds.Tables("Reservation").Rows(0).Item("DepositInfo")) _
                OrElse (ds.Tables("Reservation").Columns.Contains("DepositLimitTime") AndAlso Not String.IsNullOrEmpty(ds.Tables("Reservation").Rows(0).Item("DepositLimitTime")))) _
                AndAlso Not ds.Tables("Reservation").Rows(0).Item("CreditCardExpiration") <> "" Then

                'ds.Tables("Reservation").Rows(0).Item("DepositReference") <> "" Then
                dat_bus.DepositType = enumDepositType.BankDeposit


            ElseIf ds.Tables("Reservation").Rows(0).Item("CreditCardExpiration") <> "" Then
                dat_bus.DepositType = enumDepositType.CreditCard
            End If
        End If
        dat_bus.OnRequest = False
        If Not IsDBNull(ds.Tables("Reservation").Rows(0).Item("RatePlan")) AndAlso ds.Tables("Reservation").Rows(0).Item("RatePlan") = "" Then dat_bus.OnRequest = True


        If dat_bus.OnRequest Then
            Dim subtotal2 As Double = CDbl(ds.Tables("Reservation").Rows(0).Item("Total"))
            Dim taxes2 As Double = CDbl(ds.Tables("Reservation").Rows(0).Item("Taxes"))
            dat_bus.Tax = 0
            If subtotal2 - taxes > 0 Then dat_bus.Tax = (taxes2 / (subtotal2 - taxes)) * 100
        End If

        With ds.Tables("Room").Rows(0)
            misForms.newCall.datos_call.reservacion_paquete.cli_nombre = IIf(.IsNull("TravelerFirstName"), String.Empty, .Item("TravelerFirstName"))
            misForms.newCall.datos_call.reservacion_paquete.cli_apellido = IIf(.IsNull("TravelerLastName"), String.Empty, .Item("TravelerLastName"))
            misForms.newCall.datos_call.reservacion_paquete.cli_direccion = .Item("Address")
            misForms.newCall.datos_call.reservacion_paquete.cli_ciudad = .Item("TravelerCity")
            misForms.newCall.datos_call.reservacion_paquete.cli_estado = .Item("TravelerState")
            misForms.newCall.datos_call.reservacion_paquete.cli_cp = .Item("TravelerZipCode")
            misForms.newCall.datos_call.reservacion_paquete.cli_pais = .Item("TravelerCountry")

            If .Item("HomePhone").ToString.IndexOf("+") > 0 Then
                misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = .Item("HomePhone").ToString.Split("+")(0)
                misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = .Item("HomePhone").ToString.Split("+")(1)
            Else
                If .Item("HomePhone").ToString.Trim <> "" Then
                    If .Item("HomePhone").ToString.Length > 3 Then
                        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = .Item("HomePhone").ToString.Substring(0, 3)
                        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = .Item("HomePhone").ToString.Substring(3)
                    Else
                        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = .Item("HomePhone").ToString
                    End If

                End If
            End If

            If .Item("WorkHome").ToString.IndexOf("+") > 0 Then
                misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = .Item("WorkHome").ToString.Split("+")(0)
                misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = .Item("WorkHome").ToString.Split("+")(1)
            Else
                If .Item("WorkHome").ToString.Trim <> "" Then
                    If .Item("WorkHome").ToString.Length > 3 Then
                        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = .Item("WorkHome").ToString.Substring(0, 3)
                        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = .Item("WorkHome").ToString.Substring(3)
                    Else
                        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = .Item("WorkHome").ToString
                    End If
                End If
            End If
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = "R"
            misForms.newCall.datos_call.reservacion_paquete.cli_email = .Item("EmailCliente")
        End With
        With ds.Tables("Reservation").Rows(0)
            If dat_bus.DepositType = enumDepositType.CreditCard Then
                misForms.newCall.datos_call.reservacion_paquete.cc_tipo = .Item("CreditCardType")
                Dim indexSep As Integer
                If Not .IsNull("CreditCardHolder") AndAlso .Item("CreditCardHolder").ToString().IndexOf("/") >= 0 Then
                    indexSep = .Item("CreditCardHolder").ToString().IndexOf("/")
                    misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = .Item("CreditCardHolder").ToString.Substring(0, indexSep)
                    misForms.newCall.datos_call.reservacion_paquete.cc_apellido = .Item("CreditCardHolder").ToString.Substring(indexSep + 1)
                Else
                    indexSep = .Item("CreditCardHolder").ToString.IndexOf(" ")
                    misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = .Item("CreditCardHolder").ToString.Substring(0, indexSep)
                    misForms.newCall.datos_call.reservacion_paquete.cc_apellido = .Item("CreditCardHolder").ToString.Substring(indexSep + 1)
                End If

                misForms.newCall.datos_call.reservacion_paquete.cc_numero = .Item("CreditCardNumber")
                misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = .Item("CreditCardNumberVerify")
                misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = .Item("CreditCardExpiration")
            Else
                misForms.newCall.datos_call.reservacion_paquete.cc_tipo = Nothing
                misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = String.Empty
                misForms.newCall.datos_call.reservacion_paquete.cc_apellido = String.Empty
                misForms.newCall.datos_call.reservacion_paquete.cc_numero = String.Empty
                misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = String.Empty
                misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = String.Empty
            End If
            misForms.newCall.datos_call.reservacion_paquete.codigo_referencia = IIf(.IsNull("CodigoReferenciaCCT"), String.Empty, .Item("CodigoReferenciaCCT"))
        End With
        With ds.Tables("HotelHeader").Rows(0)
            If Not .IsNull("IdPortal") AndAlso CInt(.Item("IdPortal").ToString) > 0 Then
                misForms.newCall.datos_call.reservacion_paquete.Canal = .Item("IdPortal").ToString
            Else
                misForms.newCall.datos_call.reservacion_paquete.Canal = String.Empty
            End If
        End With
        Dim wsCall As New webService_call()
        Dim dc As Call_GetOne_RS = wsCall.get_CallReservation(dat_bus.ConfirmNumber)
        If Not IsNothing(dc) AndAlso dc.Reservation.Rows.Count > 0 Then
            If Not IsDBNull(dc.Reservation.Rows(0).Item("id_empresa")) AndAlso dc.Reservation.Rows(0).Item("id_empresa") <> "" Then dat_bus.id_Empresa = dc.Reservation.Rows(0).Item("id_empresa")
            If Not IsDBNull(dc.Reservation.Rows(0).Item("id_segmento")) AndAlso dc.Reservation.Rows(0).Item("id_segmento") <> "" Then dat_bus.id_Segmento = dc.Reservation.Rows(0).Item("id_segmento")
            If Not IsDBNull(dc.Reservation.Rows(0).Item("id_medio")) AndAlso dc.Reservation.Rows(0).Item("id_medio") <> "" Then dat_bus.id_Medio = dc.Reservation.Rows(0).Item("id_medio")
        End If
        Return dat_bus
    End Function

    Public Shared Function ReactiveReservation(ByVal ConfirmNumber As String, ByVal PropertyNumber As String, ByVal Corporativo As String) As Boolean
        'If misForms.reservaciones Is Nothing AndAlso misForms.reservaciones.UltraGrid1.ActiveRow Is Nothing Then Return Nothing

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_reactive(ConfirmNumber, PropertyNumber, Corporativo)
        If ds.Tables.Contains("Error") Then
            MsgBox(ds.Tables("Error").Rows(0).Item("Message"), MsgBoxStyle.Critical, "Error " & ds.Tables("Error").Rows(0).Item("ErrorCode"))
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Sub show_UC(ByVal key As String, ByVal title As String, ByVal tipo As String)
        Dim paneBase As Infragistics.Win.UltraWinDock.DockablePaneBase = misForms.form1.UltraDockManager1.PaneFromKey(key)
        misForms.form1.UltraDockManager1.Visible = True
        If paneBase Is Nothing Then
            Dim UC As Object = Nothing

            Select Case tipo
                'Case "callCenter.uc_detallesReservacion"
                '    UC = New uc_detallesReservacion
                '    CType(UC, uc_detallesReservacion).load_data(misForms.reservaciones.datosBusqueda_paquete, misForms.reservaciones.datosBusqueda_vuelo, misForms.reservaciones.datosBusqueda_auto, misForms.reservaciones.datosBusqueda_hotel, misForms.reservaciones.datosBusqueda_actividad)
                Case "callCenter.uc_informacionGeneral"
                    UC = New uc_informacionGeneral
                    CType(UC, uc_informacionGeneral).datosBusqueda_hotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.Clone
                    CType(UC, uc_informacionGeneral).load_data()
            End Select

            Dim pane As New Infragistics.Win.UltraWinDock.DockableControlPane(key, title, UC)
            misForms.form1.UltraDockManager1.DockAreas(0).Panes.Add(pane)
        Else
            paneBase.Show()

            Select Case tipo
                'Case "callCenter.uc_detallesReservacion"
                '    CType(CType(paneBase, Infragistics.Win.UltraWinDock.DockableControlPane).Control, callCenter.uc_detallesReservacion).load_data(misForms.reservaciones.datosBusqueda_paquete, misForms.reservaciones.datosBusqueda_vuelo, misForms.reservaciones.datosBusqueda_auto, misForms.reservaciones.datosBusqueda_hotel, misForms.reservaciones.datosBusqueda_actividad)
                Case "callCenter.uc_informacionGeneral"
                    CType(CType(paneBase, Infragistics.Win.UltraWinDock.DockableControlPane).Control, callCenter.uc_informacionGeneral).datosBusqueda_hotel = misForms.busquedaHotel_ultimoActivo.datosBusqueda_hotel.Clone
                    CType(CType(paneBase, Infragistics.Win.UltraWinDock.DockableControlPane).Control, callCenter.uc_informacionGeneral).load_data()
            End Select

            paneBase.Activate()
        End If
    End Sub

    Public Shared Function cancelar_reservacion(ByVal idRubro As String, ByVal datosVuelo As dataBusqueda_vuelo, ByVal datosActividad As dataBusqueda_actividad_test, ByVal datosHotel As dataBusqueda_hotel_test, ByVal datosAuto As dataBusqueda_auto_test, ByVal datosPaquete As dataBusqueda_paquete, Optional ByVal Cliente As String = "") As Boolean
        Dim canceled As Boolean = False
        Select Case idRubro
            Case enumRubrosPassport.Vuelos
                If datosVuelo.status = "3" Then canceled = True
            Case enumRubrosPassport.Actividades
                If datosActividad.status = "3" Then canceled = True
            Case enumRubrosPassport.Hotel
                If datosHotel.Status = "3" Then canceled = True
            Case enumRubrosPassport.Autos
                If datosAuto.Status = "3" Then canceled = True
            Case enumRubrosPassport.Paquetes
                'If datosPaquete.status = "3" Then canceled = True
        End Select

        If canceled Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_361_resCancelada"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If
        Dim detalle As String = ""
        If idRubro = enumRubrosPassport.Hotel And Cliente <> "" Then
            detalle = vbCrLf & CapaPresentacion.Idiomas.get_str("str_0149_numConfirmacion") & ": " & datosHotel.ConfirmNumber & IIf(Cliente <> "", vbCrLf & CapaPresentacion.Idiomas.get_str("str_0061_cliente") & " :" & Cliente, "")
        End If
        If Not MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0163_seguroCancel") & detalle, CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Return False
        End If
        Dim motivo As String
        Dim f As New frm_Motivo
        f.ShowDialog()
        motivo = f.texto

        Dim res As Boolean
        Select Case idRubro
            Case enumRubrosPassport.Vuelos
                res = cancela_vuelo(datosVuelo)
            Case enumRubrosPassport.Actividades
                res = cancela_actividad(datosActividad)
            Case enumRubrosPassport.Hotel

                Dim arr_data As dataBusqueda_hotel_test
                arr_data = get_datosBusqueda_fromReservaciones(datosHotel.ReservationId, datosHotel.Source)

                'Se realiza la cancelacion de la reservacion de hotel
                res = cancela_hotel(datosHotel, motivo)
                If res = True Then
                    'Obtenemos la reservacion antes de recargar el grid
                    get_datosBusqueda_fromReservaciones(datosHotel.ReservationId, datosHotel.Source)
                    'Registramos el la bitacora de movimientos
                    Dim logEntry As New webService_call
                    logEntry.LogInsert(detalles.BitacoraAccion.Cancelar, arr_data.ConfirmNumber, "", "", "")
                End If

            Case enumRubrosPassport.Autos
                res = cancela_auto(datosAuto)
            Case enumRubrosPassport.Paquetes
                res = cancela_paquete(datosPaquete)
            Case Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_304_rubroDesconocido"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
        End Select


        'If misForms.reservaciones IsNot Nothing AndAlso res Then misForms.reservaciones.load_data()
        Return res
    End Function

    Private Shared Function cancela_vuelo(ByVal datos As dataBusqueda_vuelo) As Boolean
        Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_vuelo_AirReservationDetails(datos)

        If ds_display IsNot Nothing AndAlso ds_display.Tables.Contains("Reservation") AndAlso ds_display.Tables("Reservation").Rows.Count > 0 AndAlso Not ds_display.Tables("Reservation").Rows(0).IsNull("noConfirmation") Then
            datos.RecLoc = ds_display.Tables("Reservation").Rows(0).Item("noConfirmation")
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_305_reclocNotFound"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_vuelo_AirCancel(datos)
        If ds Is Nothing Then Return False

        If Not ds.Tables("error") Is Nothing Then
            Dim r As DataRow = ds.Tables("error").Rows(0)
            MessageBox.Show(r.Item("Message"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        Else
            Dim num_cancel As String = ""
            herramientas.color_disponible()
            If herramientas.verifica_campos(ds, "UniqueID", 0, New String() {"Type"}) Then num_cancel = ds.Tables("UniqueID").Rows(0).Item("Type")

            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0153_resCancel") + "." + Environment.NewLine + CapaPresentacion.Idiomas.get_str("str_306_numCan") + ": " + num_cancel, CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)

            Return True
        End If
    End Function

    Private Shared Function cancela_actividad(ByVal datos As dataBusqueda_actividad_test) As Boolean
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_Cancel(datos)
        If ds Is Nothing Then Return False

        If Not ds.Tables("error") Is Nothing Then
            Dim r As DataRow = ds.Tables("error").Rows(0)
            MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Else
            If ds IsNot Nothing AndAlso ds.Tables.Contains("Reservation") AndAlso ds.Tables("Reservation").Rows.Count > 0 AndAlso Not ds.Tables("Reservation").Rows(0).IsNull("CancelationNumber") Then
                Dim r As DataRow = ds.Tables("Reservation").Rows(0)
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_307_canExitosa") + "." + Environment.NewLine + CapaPresentacion.Idiomas.get_str("str_306_numCan") + ": " + r.Item("CancelationNumber"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            Return True
        End If
    End Function

    Private Shared Function cancela_hotel(ByVal datos As dataBusqueda_hotel_test, ByVal motivo As String) As Boolean
        Dim datos_c As New datos_cancel
        datos_c.ConfirmNumber = datos.ConfirmNumber
        datos_c.ServiceProvider = datos.ServiceProvider

        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_cancel(datos_c, motivo)
        If ds Is Nothing Then Return False

        If Not ds.Tables("error") Is Nothing Then
            Dim r As DataRow = ds.Tables("error").Rows(0)
            Dim mapeado As Boolean = False

            If r.Item("ErrorCode") = "013" AndAlso r.Item("Message") = "Cancel Prior Restricted" Then
                If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_329_errCanceRest") & vbCrLf & CapaPresentacion.Idiomas.get_str("str_424_penal"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
                    'reintentar con witherror
                    Dim ds2 As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_cancel(datos_c, motivo, True)
                    If ds2 Is Nothing Then Return False
                    If Not ds2.Tables("error") Is Nothing Then
                        r = ds2.Tables("error").Rows(0)
                        MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return False
                    Else
                        'OK witherror
                        If ds2 IsNot Nothing AndAlso ds2.Tables.Contains("HotelCancel") AndAlso ds2.Tables("HotelCancel").Rows.Count > 0 Then
                            r = ds2.Tables("HotelCancel").Rows(0)
                            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_365_resCancelada") + Environment.NewLine + CapaPresentacion.Idiomas.get_str("str_306_numCan") + ": " + r.Item("CXNumber"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                        Return True
                    End If
                Else
                    Return False
                End If

            Else
                MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            End If
        Else
            If ds IsNot Nothing AndAlso ds.Tables.Contains("HotelCancel") AndAlso ds.Tables("HotelCancel").Rows.Count > 0 Then
                Dim r As DataRow = ds.Tables("HotelCancel").Rows(0)
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_365_resCancelada") + Environment.NewLine + CapaPresentacion.Idiomas.get_str("str_306_numCan") + ": " + r.Item("CXNumber"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            Return True
        End If
    End Function

    Private Shared Function cancela_auto(ByVal datos As dataBusqueda_auto_test) As Boolean
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarCancel(datos)
        If ds Is Nothing Then Return False

        If Not ds.Tables("Error") Is Nothing Then
            Dim r As DataRow = ds.Tables("Error").Rows(0)
            MessageBox.Show(r.Item("ErrorDescription"), "Error: " + r.Item("ErrorNumber"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Else
            If herramientas.verifica_campos(ds, "Request", 0, New String() {"Cancelation_number"}) Then
                Dim r As DataRow = ds.Tables("Request").Rows(0)
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_306_numCan") + ": " + r.Item("Cancelation_number"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            Return True
        End If
    End Function

    Private Shared Function cancela_paquete(ByVal datosBusqueda_paquete As dataBusqueda_paquete) As Boolean
        Dim ds As DataSet = obtenGenerales_paquetes.obten_general_Paquete_Cancel(datosBusqueda_paquete)
        If ds Is Nothing Then Return False

        If Not ds.Tables("error") Is Nothing Then
            Dim r As DataRow = ds.Tables("error").Rows(0)
            MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Else
            If ds.Tables.Contains("HotelCancel") AndAlso ds.Tables("HotelCancel").Rows.Count > 0 Then
                Dim r As DataRow = ds.Tables("HotelCancel").Rows(0)
                MessageBox.Show(r.Item("MarketText") + ControlChars.CrLf + "CXNumber: " + r.Item("CXNumber"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                If ds.Tables.Contains("ActivityCancel") AndAlso ds.Tables("ActivityCancel").Rows.Count > 0 Then
                    Dim r As DataRow = ds.Tables("ActivityCancel").Rows(0)
                    MessageBox.Show("ServiceTranID: " + r.Item("ServiceTranID"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), CapaPresentacion.Idiomas.get_str("str_0153_resCancel"), MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If

            Return True
        End If
    End Function

End Class
