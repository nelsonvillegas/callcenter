Imports System.Data.SqlClient

Public Class webService_call

    Dim ws As New WS_CALL.adminCalls
    Dim AuthHe As New WS_CALL.SoapAuthHeader

    'metodos

    Public Sub New()
        AuthHe.usr = username
        AuthHe.pass = password
        ws.SoapAuthHeaderValue = AuthHe
        ws.Url = CapaAccesoDatos.XML.wsData.UrlWsCalls
    End Sub

    Private Function call_WS(ByVal datos As String) As DataSet
        Try
            Dim res As New System.Xml.XmlDocument
            res.LoadXml(ws.SubmitXML(datos))
            If res Is Nothing Then Return Nothing
            Dim ds As New DataSet
            Dim respuesta() As Byte = System.Text.Encoding.UTF8.GetBytes(res.DocumentElement.OuterXml)
            Dim mem_stream As New System.IO.MemoryStream(respuesta, 0, respuesta.Length)
            ds.ReadXml(mem_stream)

            'CapaLogicaNegocios.LogManager.agregar_XML("RQ", datos)
            'CapaLogicaNegocios.LogManager.agregar_XML("RS", ds.GetXml)

            Return ds
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0076", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return Nothing
        End Try
    End Function

    Public Function es_ok() As Boolean
        Dim res As Boolean = False
        Try
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(ws.Url)
            Dim response As System.Net.HttpWebResponse = request.GetResponse()
            If response.StatusCode = System.Net.HttpStatusCode.OK Then
                res = True
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0077", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try

        Return res
    End Function

    'metodos generales

    Public Function iniciar()
        Try
            Dim RQ As New Call_new_RQ
            Dim r As Call_new_RQ.CallRow = RQ.Tables("Call").NewRow
            r.Create = True
            r.idOperador = dataOperador.user_ID
            RQ.Tables("Call").Rows.Add(r)
            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_New_RS = herramientas.llena_ds(xml, "Call_New_RS")

            Dim r2 As Call_New_RS.CallRow = RS.Tables("Call").Rows(0)
            Return r2.idCall
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Function finalizar(ByVal datos_cliente As dataCall_) As Boolean
        Try
            Dim RQ As New Call_Finish_RQ

            Dim r As Call_Finish_RQ.CallRow = RQ.Tables("Call").NewRow
            r.idCall = datos_cliente.idCall
            r.idOperador = dataOperador.user_ID
            r.nombreCliente = datos_cliente.nombre
            If datos_cliente.masculino Then r.sexo = "M" Else r.sexo = "F"
            r.ayuda = datos_cliente.desea
            r.telefono = datos_cliente.telefono
            r.correo = datos_cliente.correo
            r.comentarios = datos_cliente.comentarios
            r.nombreHotel = datos_cliente.nombreHotel
            r.paquete = False
            r.inicio = datos_cliente.fechaInicio
            r.fin = datos_cliente.fechaTermina
            RQ.Tables("Call").Rows.Add(r)

            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_Finish_RS = herramientas.llena_ds(xml, "Call_Finish_RS")

            Dim r2 As Call_Finish_RS.CallRow = RS.Tables("Call").Rows(0)
            Return r2.Correcto
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function addReservations(ByVal datos_call As dataCall_) As String
        'TODO: Agregar aqui los campos de mision empresa,segmento
        Dim RQ As New Call_AddReservations_RQ

        Dim tiene_otro As Boolean = False

        'hoteles
        For Each datos_busqueda As dataBusqueda_hotel_test In datos_call.reservacion_paquete.lista_Hoteles
            If datos_busqueda.confirmada Then Continue For

            Dim r3 As Call_AddReservations_RQ.ReservationRow = RQ.Tables("Reservation").NewRow
            r3.idCall = datos_call.idCall
            r3.noReservacion = "" 'noComfirm
            r3.rubro = enumServices.Hotel
            If datos_busqueda.id_Empresa <> "" Then r3.id_Empresa = datos_busqueda.id_Empresa
            If datos_busqueda.id_Segmento <> "" Then r3.id_Segmento = datos_busqueda.id_Segmento
            If datos_busqueda.id_Medio <> "" Then r3.id_Medio = datos_busqueda.id_Medio
            If datos_call.id_MedioPromocion <> "" And datos_call.id_MedioPromocion <> "0" Then r3.id_MedioPromocion = datos_call.id_MedioPromocion
            RQ.Tables("Reservation").Rows.Add(r3)

            tiene_otro = True
        Next

        'vuelos
        For Each datos_busqueda As dataBusqueda_vuelo In datos_call.reservacion_paquete.lista_Vuelos
            If datos_busqueda.confirmada Then Continue For

            Dim r3 As Call_AddReservations_RQ.ReservationRow = RQ.Tables("Reservation").NewRow
            r3.idCall = datos_call.idCall
            r3.noReservacion = "" 'noComfirm
            r3.rubro = enumServices.Vuelo
            RQ.Tables("Reservation").Rows.Add(r3)

            tiene_otro = True
        Next

        'actividades
        For Each datos_busqueda As dataBusqueda_actividad_test In datos_call.reservacion_paquete.lista_Actividades
            If datos_busqueda.confirmada Then Continue For

            Dim r3 As Call_AddReservations_RQ.ReservationRow = RQ.Tables("Reservation").NewRow
            r3.idCall = datos_call.idCall
            r3.noReservacion = "" 'noComfirm
            r3.rubro = enumServices.Actividad
            RQ.Tables("Reservation").Rows.Add(r3)

            tiene_otro = True
        Next

        'autos
        For Each datos_busqueda As dataBusqueda_auto_test In datos_call.reservacion_paquete.lista_Autos
            If datos_busqueda.confirmada Then Continue For

            Dim r3 As Call_AddReservations_RQ.ReservationRow = RQ.Tables("Reservation").NewRow
            r3.idCall = datos_call.idCall
            r3.noReservacion = "" 'noComfirm
            r3.rubro = enumServices.Auto
            RQ.Tables("Reservation").Rows.Add(r3)

            tiene_otro = True
        Next

        'paquetes
        Dim noComfirm As String = Nothing
        If tiene_otro Then
            'Dim r3 As Call_AddReservations_RQ.ReservationRow = RQ.Tables("Reservation").NewRow

            noComfirm = call_res_paquete(datos_call.reservacion_paquete)
            If noComfirm Is Nothing Then Return Nothing

            If noComfirm.StartsWith("duplicated") Then
                Dim msg As String
                msg = noComfirm.Split("-")(1)
                'Reintentar
                If MessageBox.Show(String.Format(CapaPresentacion.Idiomas.get_str("str_480"), msg), "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    noComfirm = call_res_paquete(datos_call.reservacion_paquete, True)

                    If noComfirm = "duplicated" Then 'mensaje de duplicado
                        String.Format(CapaPresentacion.Idiomas.get_str("str_455"), msg)
                        MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        noComfirm = Nothing
                    End If
                Else
                    noComfirm = Nothing
                End If
            End If
            If noComfirm Is Nothing Then Return Nothing

            If datos_call.reservacion_paquete.lista_Hoteles.Count > 1 Then
                datos_call.reservacion_paquete.itinerario = noComfirm
                Dim ds As DataSet = obtenGenerales_paquetes.obten_general_Paquete_Description(datos_call.reservacion_paquete)
                '------
                Dim idx As Integer = 0
                For Each r As Call_AddReservations_RQ.ReservationRow In RQ.Reservation
                    If r.rubro = enumServices.Hotel Then
                        If ds.Tables.Contains("Hotel") AndAlso ds.Tables("Hotel").Rows.Count >= idx + 1 Then
                            r.noReservacion = ds.Tables("Hotel").Rows(idx).Item("idReservacion")
                            idx += 1
                        Else
                            r.noReservacion = noComfirm
                        End If
                    Else
                        r.noReservacion = noComfirm
                    End If

                Next
            Else
                '------
                For Each r As Call_AddReservations_RQ.ReservationRow In RQ.Reservation
                    r.noReservacion = noComfirm
                Next
            End If
        End If

        noComfirm = ""
        For Each r As Call_AddReservations_RQ.ReservationRow In RQ.Reservation
            noComfirm &= r.noReservacion & ","
        Next
        noComfirm = noComfirm.Substring(0, noComfirm.Length - 1)
        Dim xml As String = ws.SubmitXML(RQ.GetXml)
        Dim RS As Call_AddReservations_RS = herramientas.llena_ds(xml, "Call_AddReservations_RS")

        Return noComfirm
    End Function
    Public Function modReservations(ByVal datos_call As dataBusqueda_hotel_test, ByVal idCall As Integer, ByVal noconfirm As String) As Call_AddReservations_RS
        'TODO: Agregar aqui los campos de mision empresa,segmento
        Dim RQ As New Call_AddReservations_RQ

        Dim tiene_otro As Boolean = False

        'hoteles

        Dim r3 As Call_AddReservations_RQ.ReservationRow = RQ.Tables("Reservation").NewRow
        r3.idCall = idCall
        r3.noReservacion = noconfirm
        r3.rubro = enumServices.Hotel

        If datos_call.id_Empresa <> "" Then r3.id_Empresa = datos_call.id_Empresa
        If datos_call.id_Segmento <> "" Then r3.id_Segmento = datos_call.id_Segmento
        If datos_call.id_Medio <> "" Then r3.id_Medio = datos_call.id_Medio
        'If datos_call.id_MedioPromocion <> "" And datos_call.id_MedioPromocion <> "0" Then r3.id_MedioPromocion = datos_call.id_MedioPromocion
        RQ.Tables("Reservation").Rows.Add(r3)


        Dim xml As String = ws.SubmitXML(RQ.GetXml)
        Dim RS As Call_AddReservations_RS = herramientas.llena_ds(xml, "Call_AddReservations_RS")

        Return RS
    End Function

    Public Sub modReservationsNoRestriction(ByVal dsReservation As reqHotelModifyNoRestrictions)

        Dim xml As String = ws.SubmitXML(dsReservation.GetXml)

    End Sub


    Private Function call_res_paquete(ByVal datos_busqueda As dataBusqueda_paquete, Optional IgnorarDuplicada As Boolean = False) As String
        Dim ConfirmNumber As String = Nothing
        Dim ds As DataSet = obtenGenerales_paquetes.obten_general_Paquete_Book(datos_busqueda, IgnorarDuplicada)

        datos_busqueda.cli_id = ""

        If ds Is Nothing OrElse ds.Tables.Count = 0 Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_308_noConfRes"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return Nothing
        End If

        If Not ds.Tables("error") Is Nothing Then
            Dim r As DataRow = ds.Tables("error").Rows(0)

            Dim msg As String = ""
            Select Case r.Item("Message")
                Case "Invalid Credit Card"
                    msg = CapaPresentacion.Idiomas.get_str("str_309_invalidCC")
                Case "No Accept Credit Card"
                    msg = CapaPresentacion.Idiomas.get_str("str_310_notAcceptedCC")
                Case Else
                    msg = r.Item("Message")
            End Select

            If r.Item("ErrorCode") = "042" Then
                Return "duplicated-" & msg 'String.Format(CapaPresentacion.Idiomas.get_str("str_455"), msg)

            Else
                MessageBox.Show(msg, "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If



            Return Nothing
        Else
            Dim r As DataRow = ds.Tables("response").Rows(0)

            Try
                ConfirmNumber = r.Item("ItineraryNumber")
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0267_gracias") + Environment.NewLine + CapaPresentacion.Idiomas.get_str("str_0270_numItin") + " " + ConfirmNumber, CapaPresentacion.Idiomas.get_str("str_0269_operacionSatif"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0078", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
            End Try
        End If

        Return ConfirmNumber
    End Function

    'Private Sub guardar_reservacion(ByVal datos_busqueda As dataBusqueda_hotel_test, ByVal campos_str() As String)
    '    'validaciones
    '    Dim prop_number As Integer = CInt(datos_busqueda.PropertyNumber)
    '    Dim r_res As DataRow = get_ws.obten_hotel_display(New datos_display(datos_busqueda.ChainCode, campos_str(0), prop_number)).Tables("Reservation").Rows(0)
    '    'validaciones

    '    Dim conn As New SqlConnection()
    '    conn.ConnectionString = herramientas.wsData.get_servidorSQL

    '    Dim cmd As New SqlCommand()
    '    cmd.Connection = conn
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandText = "insertar_reservacion"

    '    ' Create a SqlParameter for each parameter in the stored procedure.
    '    cmd.Parameters.Add("@fecha", SqlDbType.SmallDateTime).Value = DateTime.Now
    '    cmd.Parameters.Add("@noConfirm", SqlDbType.NVarChar, 15).Value = campos_str(0)
    '    cmd.Parameters.Add("@monto", SqlDbType.Money).Value = campos_str(3)
    '    cmd.Parameters.Add("@hotelName", SqlDbType.NVarChar, 100).Value = campos_str(4)
    '    cmd.Parameters.Add("@fecha_in", SqlDbType.SmallDateTime).Value = DateTime.ParseExact(campos_str(1), CapaPresentacion.Common.DateDataFormat, Nothing)
    '    cmd.Parameters.Add("@fecha_out", SqlDbType.SmallDateTime).Value = DateTime.ParseExact(campos_str(2), CapaPresentacion.Common.DateDataFormat, Nothing)
    '    cmd.Parameters.Add("@ChainCode", SqlDbType.NVarChar, 2).Value = datos_busqueda.ChainCode
    '    cmd.Parameters.Add("@PropertyNumber", SqlDbType.Int).Value = prop_number
    '    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = r_res.Item("Status")
    '    cmd.Parameters.Add("@currency", SqlDbType.NVarChar, 3).Value = r_res.Item("Money")

    '    Dim rowCount As Integer
    '    Dim previousConnectionState As ConnectionState
    '    previousConnectionState = conn.State
    '    Try
    '        If conn.State = ConnectionState.Closed Then
    '            conn.Open()
    '        End If
    '        rowCount = cmd.ExecuteNonQuery()
    '    Finally
    '        If previousConnectionState = ConnectionState.Closed Then
    '            conn.Close()
    '        End If
    '    End Try
    'End Sub

    Public Function get_calls(ByVal t_ini As String, ByVal t_fin As String) As DataTable
        Dim RQ As New Call_getCalls_RQ

        Dim r As Call_getCalls_RQ.CallsRow = RQ.Tables("Calls").NewRow
        r.idOperador = dataOperador.user_ID
        r.ini = t_ini
        r.fin = t_fin
        RQ.Tables("Calls").Rows.Add(r)

        Dim xml As String = ws.SubmitXML(RQ.GetXml)
        Dim RS As Call_getCalls_RS = herramientas.llena_ds(xml, "Call_getCalls_RS")

        RS.Tables("Call").Columns.Add("inicio2")
        RS.Tables("Call").Columns.Add("duracion2")
        RS.Tables("Call").Columns.Add("ayuda2")
        RS.Tables("Call").Columns.Add("sexo2")
        For i As Integer = 0 To RS.Tables("Call").Rows.Count - 1
            Dim ini As DateTime = CType(RS.Tables("Call").Rows(i).Item("inicio"), DateTime)
            RS.Tables("Call").Rows(i).Item("inicio2") = ini.ToString("dd/MMM/yyyy hh:mm:00") 'Format(ini.Hour, "00") + ":" + Format(ini.Minute, "00") + ":" + Format(ini.Second, "00")

            If RS.Tables("Call").Rows(i).Item("duracion") IsNot System.DBNull.Value Then
                'Dim dur As DateTime = CType(RS.Tables("Call").Rows(i).Item("duracion"), DateTime)

                Dim t_inicio2 As DateTime = CDate(RS.Tables("Call").Rows(i).Item("inicio").ToString)
                Dim t_fin2 As DateTime = CDate(RS.Tables("Call").Rows(i).Item("fin").ToString)

                'Dim hora As String = RS.Tables("Call").Rows(i).Item("duracion").ToString.Split("T")(1) '1900-01-01T00:02:00.07-07:00
                Dim t_dur As TimeSpan = t_fin2 - t_inicio2
                'Dim t_dur As New TimeSpan(CInt(hora.Split(":")(0)), CInt(hora.Split(":")(1)), CInt(hora.Split(":")(2).Split(".")(0)))

                RS.Tables("Call").Rows(i).Item("duracion2") = Format(t_dur.Hours, "00") + ":" + Format(t_dur.Minutes, "00") + ":" + Format(t_dur.Seconds, "00")
            End If
        Next

        RS.Tables("Call").Columns("nombreCliente").SetOrdinal(1)
        RS.Tables("Call").Columns("inicio2").SetOrdinal(2)
        RS.Tables("Call").Columns("duracion2").SetOrdinal(3)
        RS.Tables("Call").Columns("reservaciones").SetOrdinal(4)
        RS.Tables("Call").Columns("comentarios").SetOrdinal(5)

        Return RS.Tables("Call")
    End Function

    Public Function get_CallReservation(ByVal noReservacion As String, Optional ByVal idCall As Integer = 0) As Call_GetOne_RS
        Dim RQ As New Call_GetOne_RQ

        Dim r As Call_GetOne_RQ.CallRow = RQ.Tables("Call").NewRow
        r.idCall = idCall
        r.noReservacion = noReservacion
        RQ.Tables("Call").Rows.Add(r)

        Dim xml As String = ws.SubmitXML(RQ.GetXml)
        Dim RS As Call_GetOne_RS = herramientas.llena_ds(xml, "Call_GetOne_RS")
        Return RS
    End Function

    Public Function get_call(ByVal idCall As Integer, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal btnHotel As Infragistics.Win.Misc.UltraButton, ByVal btnVuelo As Infragistics.Win.Misc.UltraButton, ByVal btnActividad As Infragistics.Win.Misc.UltraButton, ByVal btnAuto As Infragistics.Win.Misc.UltraButton) As dataCall_
        Dim RQ As New Call_GetOne_RQ

        Dim r As Call_GetOne_RQ.CallRow = RQ.Tables("Call").NewRow
        r.idCall = idCall
        RQ.Tables("Call").Rows.Add(r)

        Dim xml As String = ws.SubmitXML(RQ.GetXml)
        Dim RS As Call_GetOne_RS = herramientas.llena_ds(xml, "Call_GetOne_RS")

        Dim r2 As Call_GetOne_RS.CallRow = RS.Tables("Call").Rows(0)
        Dim dc As New dataCall_

        dc.idCall = r2.idCall
        dc.nombre = r2.nombreCliente
        If r2.sexo = "M" Then dc.masculino = True Else dc.masculino = False
        dc.desea = r2.ayuda
        dc.telefono = r2.telefono
        dc.correo = r2.correo
        dc.comentarios = r2.comentarios
        dc.deseaPaquete = r2.paquete
        dc.fechaInicio = r2.inicio
        dc.fechaTermina = r2.fin

        If grid Is Nothing Then grid = New Infragistics.Win.UltraWinGrid.UltraGrid
        If btnHotel Is Nothing Then btnHotel = New Infragistics.Win.Misc.UltraButton
        If btnVuelo Is Nothing Then btnVuelo = New Infragistics.Win.Misc.UltraButton
        If btnActividad Is Nothing Then btnActividad = New Infragistics.Win.Misc.UltraButton
        If btnAuto Is Nothing Then btnAuto = New Infragistics.Win.Misc.UltraButton

        dc.grid = grid
        dc.btnHot = btnHotel
        dc.btnVue = btnVuelo
        dc.btnAct = btnActividad
        dc.btnAut = btnAuto
        For Each r3 As Call_GetOne_RS.ReservationRow In RS.Tables("Reservation").Rows
            Select Case r3.rubro
                Case 1 'hotel
                    Dim reser As New dataBusqueda_hotel_test
                    reser.ConfirmNumber = r3.noReservacion
                    reser.rubro = r3.rubro
                    'reser = misForms.get_datosBusqueda_fromReservaciones(r3.idReservo)
                    dc.add_rvaHotel(reser, True)
                Case 2 'vuelo
                    Dim reser As New dataBusqueda_vuelo
                    reser.ConfirmNumber = r3.noReservacion
                    reser.rubro = r3.rubro
                    dc.add_rvaVuelo(reser, True)
                Case 3 'paqute
                    '
                Case 4 'actividad
                    Dim reser As New dataBusqueda_actividad_test
                    reser.ConfirmNumber = r3.noReservacion
                    reser.rubro = r3.rubro
                    dc.add_rvaActividad(reser)
                Case 5 'auto
                    Dim reser As New dataBusqueda_auto_test
                    reser.ConfirmNumber = r3.noReservacion
                    reser.rubro = r3.rubro
                    dc.add_rvaAuto(reser, True)
            End Select
        Next

        Return dc
    End Function

    Public Function LogInsert(ByVal Operation As Integer, ByVal noReservation As String, ByVal Comments As String, ByVal DataBefore As String, ByVal DataAfter As String) As Boolean
        Try
            Dim RQ As New Call_Log_Insert_RQ
            Dim r As Call_Log_Insert_RQ.LogRow = RQ.Log.NewLogRow
            r.idOperador = IIf(dataOperador.user_ID Is Nothing, 0, dataOperador.user_ID)
            r.Operation = Operation
            r.NoReservacion = noReservation
            r.Comments = Comments
            r.Data_Before = DataBefore
            r.Data_After = DataAfter
            RQ.Log.AddLogRow(r)

            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_Log_Insert_RS = herramientas.llena_ds(xml, "Call_Log_Insert_RS")
            If RS.Response.Count > 0 Then
                Return RS.Response(0).Success
            End If
        Catch ex As Exception

        End Try
        Return False
    End Function

    Public Function LogGet(Optional ByVal Operation As Integer = -1, Optional ByVal noReservation As String = "", Optional ByVal DateBegin As Date = Nothing, Optional ByVal DateEnd As Date = Nothing, Optional ByVal idOperador As Integer = -1) As Call_Log_Get_RS
        Try
            Dim RQ As New Call_Log_Get_RQ
            Dim r As Call_Log_Get_RQ.LogRow = RQ.Log.NewLogRow
            If idOperador <> -1 Then r.idOperador = idOperador
            If Operation <> -1 Then r.Operation = Operation
            If noReservation <> "" Then r.noReservacion = noReservation
            If DateBegin.Year > 2000 Then r.DateBegin = DateBegin
            If DateEnd.Year > 2000 Then r.DateEnd = DateEnd
            RQ.Log.AddLogRow(r)

            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_Log_Get_RS = herramientas.llena_ds(xml, "Call_Log_Get_RS")
            Return RS
        Catch ex As Exception

        End Try
        Return Nothing
    End Function

    Public Function LogDetail(idLog As Integer) As Call_Log_Detail_RS
        Try
            Dim RQ As New Call_Log_Detail_RQ
            Dim r As Call_Log_Detail_RQ.LogRow = RQ.Log.NewLogRow
            r.idLog = idLog
            RQ.Log.AddLogRow(r)

            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_Log_Detail_RS = herramientas.llena_ds(xml, "Call_Log_Detail_RS")
            Return RS
        Catch ex As Exception

        End Try
        Return Nothing
    End Function

    Public Function PMSRetry(ByVal noReservation As String) As Boolean
        Try
            Dim RQ As New Call_PMS_Retry_RQ
            Dim r As Call_PMS_Retry_RQ.RequestRow = RQ.Request.NewRequestRow
            r.noReservacion = noReservation
            RQ.Request.AddRequestRow(r)
            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_PMS_Retry_RS = herramientas.llena_ds(xml, "Call_PMS_Retry_RS")
            If RS.Response.Count > 0 Then
                Return RS.Response(0).Success
            End If
        Catch ex As Exception

        End Try
        Return False
    End Function

    Public Function HotelGetAvailability(idHotel As Integer, DateBegin As Date, DateEnd As Date) As Call_Hotel_GetAvailability_RS
        Try
            Dim RQ As New Call_Hotel_GetAvailability_RQ
            Dim r As Call_Hotel_GetAvailability_RQ.HotelRow = RQ.Hotel.NewHotelRow
            If idHotel <> -1 Then r.idHotel = idHotel
            If DateBegin.Year > 2000 Then r.DateBegin = DateBegin.ToString("yyyy/MM/dd")
            If DateEnd.Year > 2000 Then r.DateEnd = DateEnd.ToString("yyyy/MM/dd")
            RQ.Hotel.AddHotelRow(r)

            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_Hotel_GetAvailability_RS = herramientas.llena_ds(xml, "Call_Hotel_GetAvailability_RS")
            Return RS
        Catch ex As Exception

        End Try
        Return Nothing

    End Function

    Public Function ReportProduction(DateBegin As Date, DateEnd As Date, idSegment As Integer, SegmentCompany As String, idCorporate As Integer, SourceList As String, SourceCodeList As String, AgentName As String, HotelName As String, GroupedByAgent As Boolean, ByVal id_Empresa As Integer) As Call_ReportProduction_RS
        Try
            Dim RQ As New Call_ReportProduction_RQ
            Dim r As Call_ReportProduction_RQ.RequestRow = RQ.Request.NewRequestRow
            If DateBegin.Year > 2000 Then r.DateBegin = DateBegin.ToString("yyyyMMdd")
            If DateEnd.Year > 2000 Then r.DateEnd = DateEnd.ToString("yyyyMMdd")
            If idSegment <> -1 Then r.idSegment = idSegment
            r.SegmentCompany = SegmentCompany
            If idCorporate <> -1 Then r.idCorporate = idCorporate
            If SourceList.Trim <> "" Then r.SourceList = SourceList.Trim
            If SourceCodeList.Trim <> "" Then r.SourceCodeList = SourceCodeList.Trim
            If AgentName.Trim <> "" Then r.AgentName = AgentName.Trim
            If HotelName.Trim <> "" Then r.HotelName = HotelName.Trim
            r.ByAgent = 0
            If GroupedByAgent Then r.ByAgent = 1
            If id_Empresa <> -1 Then r.id_Empresa = id_Empresa
            RQ.Request.AddRequestRow(r)

            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            Dim RS As Call_ReportProduction_RS = herramientas.llena_ds(xml, "Call_ReportProduction_RS")
            Return RS
        Catch ex As Exception

        End Try
        Return Nothing

    End Function

    Public Function SegmentCompanyInsert(Name As String, idMedium As Integer, idSegment As Integer, idCorporate As Integer) As Call_SegmentsCompany_Insert_RS
        Dim RS As Call_SegmentsCompany_Insert_RS
        Try
            Dim RQ As New Call_SegmentsCompany_Insert_RQ
            Dim r As Call_SegmentsCompany_Insert_RQ.RequestRow = RQ.Request.NewRequestRow
            If idMedium > 0 Then r.idMedium = idMedium

            r.Name = Name
            r.idSegment = idSegment
            r.idCorporate = idCorporate
            RQ.Request.AddRequestRow(r)
            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            RS = herramientas.llena_ds(xml, "Call_SegmentsCompany_Insert_RS")
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E00--", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return RS
    End Function

    Public Function SegmentCompanyUpdate(idEmpresa As String, Name As String, idMedium As Integer, idSegment As Integer, idCorporate As Integer) As Call_SegmentsCompany_Update_RS
        Dim RS As Call_SegmentsCompany_Update_RS
        Try
            Dim RQ As New Call_SegmentsCompany_Update_RQ
            Dim r As Call_SegmentsCompany_Update_RQ.RequestRow = RQ.Request.NewRequestRow
            If idMedium > 0 Then r.idMedium = idMedium
            r.idCompany = idEmpresa
            r.Name = Name
            r.idSegment = idSegment
            r.idCorporate = idCorporate
            RQ.Request.AddRequestRow(r)
            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            RS = herramientas.llena_ds(xml, "Call_SegmentsCompany_Update_RS")
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E00--", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return RS
    End Function

    Public Function SegmentCompanyDelete(idEmpresa As String) As Call_SegmentsCompany_Delete_RS
        Dim RS As Call_SegmentsCompany_Delete_RS
        Try
            Dim RQ As New Call_SegmentsCompany_Delete_RQ
            Dim r As Call_SegmentsCompany_Delete_RQ.RequestRow = RQ.Request.NewRequestRow
            r.idCompany = idEmpresa
            RQ.Request.AddRequestRow(r)
            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            RS = herramientas.llena_ds(xml, "Call_SegmentsCompany_Delete_RS")
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E00--", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return RS
    End Function

    Public Function SegmentCompanyList(Name As String, idSegment As Integer, idCorporate As Integer) As Call_SegmentsCompany_List_RS
        Dim RS As Call_SegmentsCompany_List_RS
        Try
            Dim RQ As New Call_SegmentsCompany_List_RQ
            Dim r As Call_SegmentsCompany_List_RQ.RequestRow = RQ.Request.NewRequestRow
            r.Searchtext = Name
            If idSegment <> "0" Then r.idSegment = idSegment
            r.idCorporate = idCorporate
            RQ.Request.AddRequestRow(r)
            Dim xml As String = ws.SubmitXML(RQ.GetXml)
            RS = herramientas.llena_ds(xml, "Call_SegmentsCompany_List_RS")
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E00--", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
        Return RS
    End Function

End Class