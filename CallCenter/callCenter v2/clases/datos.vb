Public Class datos_display
    Public ChainCode As String
    Public ConfirmNumber As String
    Public PropertyNumber As String
    Public ReservationId As String
    Public AltCurrency As String

    Public Sub New()
    End Sub

    Public Sub New(ByVal ch As String, ByVal co As String, ByVal pr As String)
        ChainCode = ch
        ConfirmNumber = co
        PropertyNumber = pr
    End Sub
End Class

Public Class datos_HOI
    'header
    Public Code As String
    Public Source As String
    'Public SearchBy As String

    'request
    Public StartDate As String
    Public EndDate As String
    Public Nights As String
    'Public Adults As String
    'Public Children As String
    'Public arr_Adults() As Integer
    'Public arr_Children() As Integer
    'Public NumRooms As String
    Public edades_Children As String
    Public AltCurrency As String
    Public HotelName As String
End Class

Public Class datos_HOR
    'header
    Public CheckinDate As String
    Public CheckoutDate As String
    Public AccessCode As String
    Public Language As String
    Public RateCode As String
    Public PropertyNumber As String
    Public Source As String
    Public Nights As String
    Public ChainCode As String
    Public ServiceProvider As String
    'rooms
    'Public habitaciones As String
    'room
    'Public Adults() As Integer
    'Public Children() As Integer
    Public AltCurrency As String
    Public ReferenceContract As String
End Class

'Public Class datos_sell
'    'header
'    Public ServiceProvider As String
'    'reservation
'    Public CheckinDate As String
'    Public CheckoutDate As String
'    Public AccessCode As String
'    Public PropertyNumber As String
'    Public ChainCode As String
'    Public CreditCardExpiration As String
'    Public CreditCardHolder As String
'    Public CreditCardNumber As String
'    Public CreditCardNumberVerify As String
'    Public CreditCardType As String
'    Public RateCode As String
'    Public RAwayAdult As String
'    Public RAwayChild As String
'    Public RAwayCrib As String
'    Public GuarDep As String
'    'customer
'    Public PhoneHome As String
'    Public Email As String
'    'Public PostalCode As String
'    Public Address As String
'    Public LastName As String
'    Public FirstName As String
'    Public Country As String
'    Public Estate As String
'    Public City As String
'    'rooms
'    Public hatitaciones As String
'    'room
'    'Public Adults() As Integer
'    'Public Children() As Integer
'    Public Preferences As String
'End Class

Public Class datos_modify
    'reservation
    Public ServiceProvider As String
    Public Source As String
    Public CheckInDate As String
    Public CheckOutDate As String
    Public AccessCode As String
    Public PropertyNumber As String
    Public ChainCode As String
    Public ConfirmNumber As String
    Public CreditCardExpiration As String
    Public CreditCardHolder As String
    Public CreditCardNumber As String
    Public CreditCardNumberVerify As String
    Public CreditCardType As String
    Public RateCode As String
    Public GuarDep As String
    Public ReserveByBankDeposit As String
    Public OnRequest As String
    Public Tax As String
    Public Total As String
    Public Currency As String

    Public DepositType As enumDepositType
    Public PaymentInformation As String

    Public DepositLimitDate As String
    Public DepositLimitTime As String
    Public CommentByVendor As String

    Public DepositInfoCCT As String
    Public DepositAmountCCT As String

    'customer
		Public PhoneHome As String
		Public PhoneWork As String
    Public Email As String
    Public PostalCode As String
    Public Address As String
    Public LastName As String
    Public FirstName As String
    Public Country As String
    Public Estate As String
    Public City As String
    'rooms
    Public habitaciones As String
    'romm
    Public Adults() As Integer
    Public Children() As Integer
    Public Preferences As String
    Public ChildAges() As String
    ' Public frecuentCode As String
    Public ReferenceContract As String
    Public CodigoReferenciaCCT As String
End Class

Public Class datos_cancel
    Public ChainCode As String
    Public ConfirmNumber As String
    Public PropertyNumber As String
    Public ServiceProvider As String
End Class

Public Class datos_HOC
    'header
    Public CheckinDate As String
    Public CheckoutDate As String
    Public AccessCode As String
    Public ServiceProvider As String
    Public AltCurrency As String
    'hotelFilterRow
    Public Segment As New List(Of String)
    'HotelRequest
    Public PropertyNumber As String
    'rooms
    'Public hatitaciones As String
    'romm
    'Public Adults() As Integer
    'Public Children() As Integer
    Public ReferenceContract As String

End Class

Public Class datos_viajero
    Public FName As String
    Public LName As String
    Public Email As String
    Public Phone As String
    Public postalcode As String
    Public address As String
    Public city As String
    Public state As String
    Public IdCountry As String
    Public emailtype As String = "1"
    Public idcliente As Integer
End Class

Public Class datos_HOD
    'Public ChainCode As String
    Public PropertyNumber As String
    Public HotelFilter As New List(Of String)
    Public ServiceProvider As String
End Class