Imports System.Net
Imports System.Net.NetworkInformation

Public Class webService_services

    Private ws As New WS_SERVICES.Service1
    Private auth As New WS_SERVICES.SoapAuthHeader

    Public Sub New(ByVal User As String, ByVal password As String, Optional ByVal url As String = "")
        auth.usr = User
        auth.pass = password
        ws.SoapAuthHeaderValue = auth
        If url <> "" Then ws.Url = url Else ws.Url = CapaAccesoDatos.XML.wsData.UrlWsServices
    End Sub

    Public Function call_WS(ByVal datos As String) As DataSet
        Try
            ws.Timeout = (1000 * 60)                                       ' 1 min
            CapaLogicaNegocios.LogManager.agregar_XML("RQ", datos)

            Dim res As System.Xml.XmlElement = ws.SubmitXML(datos)
            If res Is Nothing Then
                CapaLogicaNegocios.LogManager.agregar_XML("RS", "NOTHING!!!!")
                Return Nothing
            End If

            Dim ds As New DataSet
            Dim respuesta() As Byte = System.Text.Encoding.UTF8.GetBytes(res.OuterXml)
            Dim mem_stream As New System.IO.MemoryStream(respuesta, 0, respuesta.Length)
            ds.ReadXml(mem_stream)

            CapaLogicaNegocios.LogManager.agregar_XML("RS", ds.GetXml)

            Return ds
        Catch ex As Exception
            Dim msg As String = String.Empty
            If NetworkInterface.GetIsNetworkAvailable Then
                msg = "Esta conectado a una red"
                If CheckForInternetConnection() Then
                    msg = msg + "\nEsta conectado a internet"
                Else
                    msg = msg + "\nNo est conectado a internet"
                End If
            Else
                msg = "No esta conectado a la red"
            End If
            msg = msg + "\n" + ex.Message
            If ex.GetType IsNot GetType(System.Threading.ThreadAbortException) Then
                CapaLogicaNegocios.ErrorManager.Manage("E0088", msg, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Else
                ws.Abort()
            End If
            Return Nothing
        End Try
    End Function
    Public Function CheckForInternetConnection() As Boolean

        Try
            Using client As New WebClient
                Using stream As IO.Stream = client.OpenRead("http://www.google.com")
                    Return True
                End Using
            End Using
        Catch
            Return False
        End Try

    End Function
    Public Function es_ok() As Boolean
        Dim res As Boolean = False
        Try
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(ws.Url)
            Dim response As System.Net.HttpWebResponse = request.GetResponse()
            If response.StatusCode = System.Net.HttpStatusCode.OK Then
                res = True
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0089", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try

        Return res
    End Function

End Class
