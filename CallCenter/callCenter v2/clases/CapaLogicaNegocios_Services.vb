Partial Public Class CapaLogicaNegocios

    'Public Shared Function GetTravelersByEmail(ByVal Email As String) As DataSet
    '    Dim param As New SqlClient.SqlParameter("@emailCliente", SqlDbType.NVarChar, 80)
    '    param.Value = Email
    '    Dim ds As DataSet = tools_db.getSP_toDS(System.Configuration.ConfigurationSettings.AppSettings.Item("OzUnivisit_CS").ToString, "spGetTravelerByEmail", New SqlClient.SqlParameter() {param})

    '    Return ds
    'End Function

    Public Class Hotels

        Protected Shared MaxDaysAllowed As Integer = 90

        Public Class SearchHotel

            Public Shared Sub load_data(ByVal frm As busquedaHotel, ByVal ds As DataSet)
                Try
                    Dim ok As Boolean = False

                    If Not ds Is Nothing AndAlso ds.Tables.Contains("Property") AndAlso ds.Tables("Property").Rows.Count > 0 Then
                        If ds.Tables.Count > 0 Then
                            'frm.avance = Int(100 / ds.Tables("Property").Rows.Count)

                            ok = True
                        End If
                    End If

                    If Not ok Then
                        'frm.total = 100
                        'CapaPresentacion.Common.FijaProgressBar(frm, frm.total, False)

                        frm.data_lbl_totalHoteles.Text = "0"
                        frm.UltraGrid1.DataSource = Nothing
                        frm.PictureBox2.Visible = False
                        'CapaPresentacion.Common.Fija_CTLVISIBLE(CType(frm, busquedaHotel).PictureBox2, False)
                    Else
                        ds = CapaPresentacion.Hotels.SearchHotel.FormatDS_Search(ds)
                        CapaPresentacion.Hotels.SearchHotel.FormatUltraGrid(frm, ds)

                        frm.data_lbl_totalHoteles.Text = frm.UltraGrid1.Rows.Count

                        'busca rangos de tarifas
                        frm.Thread_AgregaTarifas = New Threading.Thread(AddressOf agrega_tarifas_async)
                        frm.Thread_AgregaTarifas.Start(New Object() {frm, ds.Tables("Property")})

                        'espera y colorea los rows
                        If Not frm.Thread_ColoresTarifas Is Nothing AndAlso frm.Thread_ColoresTarifas.IsAlive Then frm.Thread_ColoresTarifas.Suspend()
                        frm.Thread_ColoresTarifas = New System.Threading.Thread(AddressOf colores_tarfas)
                        frm.Thread_ColoresTarifas.Start(frm)

                        CapaPresentacion.Hotels.SearchHotel.TipForm(frm)
                    End If

                    CapaPresentacion.Idiomas.cambiar_busquedaHotel(frm)

                    frm.UltraGrid1.Focus()
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0112", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                Finally
                    'CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)
                End Try
            End Sub

            Private Shared Sub colores_tarfas(ByVal obj As Object)
                Try
                    Dim frm As busquedaHotel = CType(obj, busquedaHotel)
                    frm.Thread_AgregaTarifas.Join()

                    'frm.total = 100
                    'CapaPresentacion.Common.FijaProgressBar(frm, frm.total, False)

                    CapaPresentacion.Hotels.SearchHotel.FIJA_GRID(frm, frm.UltraGrid1, frm.datosBusqueda_hotel.todosHoteles)
                    'CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0113", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End Try
            End Sub

            Public Shared Sub agrega_tarifas_async(ByVal params As Object)
                Dim frm As busquedaHotel = CType(params(0), busquedaHotel)
                Dim t As DataTable = CType(params(1), DataTable)

                Try
                    For Each ServiceProvider As String In GetProviders(t)
                        'Dim PropertyNumbers As String = GetPropertyNumbers(ServiceProvider, t)
                        Dim ds As New DataSet
                        Dim PropertyNumbers As String = ""
                        Dim cont As Integer = 0
                        For Each r As DataRow In t.Rows
                            If r.Item("ServiceProvider") <> ServiceProvider Then Continue For

                            If PropertyNumbers <> "" Then PropertyNumbers += ","
                            PropertyNumbers += r.Item("PropertyNumber")
                            cont += 1
                            If cont >= 50 Then
                                Dim dsTemp As DataSet = obten_NOgeneral_HOC(PropertyNumbers, ServiceProvider, frm.datosBusqueda_hotel)
                                If ds.Tables.Count = 0 Then
                                    ds.Merge(dsTemp)
                                Else

                                    For x As Integer = 1 To dsTemp.Tables.Count - 1 'omitir la 0 del completeavailability
                                        Dim campo As String = ""
                                        Select Case dsTemp.Tables(x).TableName.ToLower
                                            Case "property"
                                                campo = "property_id"
                                            Case "roomtype"
                                                campo = "roomtype_id"
                                            Case "rateplan"
                                                campo = "rateplan_id"
                                        End Select
                                        Dim m As Long = 0
                                        If campo <> "" Then
                                            m = ds.Tables(x).Compute("MAX(" & campo & ")", "")
                                            For Each dr As DataRow In dsTemp.Tables(x).Rows
                                                dr(campo) = dr(campo) * (-1)
                                            Next
                                        End If

                                        For Each dr As DataRow In dsTemp.Tables(x).Rows
                                            If campo <> "" Then
                                                m += 1
                                                dr(campo) = m
                                            End If
                                            ds.Tables(x).ImportRow(dr)
                                        Next
                                    Next
                                End If

                                PropertyNumbers = ""
                                cont = 0
                            End If
                        Next
                        If PropertyNumbers <> "" Then
                            Dim dsTemp As DataSet = obten_NOgeneral_HOC(PropertyNumbers, ServiceProvider, frm.datosBusqueda_hotel)
                            If ds.Tables.Count = 0 Then
                                ds.Merge(dsTemp)
                            Else
                                For x As Integer = 1 To dsTemp.Tables.Count - 1
                                    Dim campo As String = ""
                                    Select Case dsTemp.Tables(x).TableName.ToLower
                                        Case "property"
                                            campo = "property_id"
                                        Case "roomtype"
                                            campo = "roomtype_id"
                                        Case "rateplan"
                                            campo = "rateplan_id"
                                    End Select
                                    Dim m As Long = 0
                                    If campo <> "" Then
                                        m = ds.Tables(x).Compute("MAX(" & campo & ")", "")
                                        For Each dr As DataRow In dsTemp.Tables(x).Rows
                                            dr(campo) = dr(campo) * (-1)
                                        Next
                                    End If

                                    For Each dr As DataRow In dsTemp.Tables(x).Rows
                                        If campo <> "" Then
                                            m += 1
                                            dr(campo) = m
                                        End If
                                        ds.Tables(x).ImportRow(dr)
                                    Next
                                Next
                            End If
                        End If

                        If ds Is Nothing Then Continue For

                        For Each r As DataRow In t.Rows
                            If r.Item("ServiceProvider") <> ServiceProvider Then Continue For

                            Dim tarMenor, tarMayor As Double
                            Dim Message As String = String.Empty
                            r.Item("Tarifas") = "..." + CapaPresentacion.Idiomas.get_str("str_384_buscando")
                            r.Item("Tarifas") = GetLowerRate(r.Item("PropertyNumber"), ds, tarMenor, tarMayor, Message)
                            r.Item("TarifaMenorOrdenar") = tarMenor
                            r.Item("TarifaMayorOrdenar") = tarMayor

                            If Not String.IsNullOrEmpty(Message) AndAlso Message.Contains("007") AndAlso String.IsNullOrEmpty(r.Item("Tarifas").ToString) Then
                                If Not t.Columns.Contains("Message") Then
                                    t.Columns.Add("Message")
                                    r.Item("Message") = CapaPresentacion.Idiomas.get_str("str_533")
                                Else
                                    r.Item("Message") = CapaPresentacion.Idiomas.get_str("str_533")
                                End If
                            End If

                            'If frm.total + frm.avance <= 100 Then frm.total += frm.avance Else frm.total = 99
                            'CapaPresentacion.Common.FijaProgressBar(frm, frm.total, True)
                        Next
                    Next
                Catch ex As Threading.ThreadAbortException
                    CapaLogicaNegocios.ErrorManager.Manage("E0114", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0115", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)

                    'If frm.total + frm.avance <= 100 Then frm.total += frm.avance Else frm.total = 99
                    'CapaPresentacion.Common.FijaProgressBar(frm, frm.total, True)
                End Try
            End Sub

            Private Shared Function GetLowerRate(ByVal PropertyNumber As String, ByVal ds As DataSet, ByRef tarMenor As Double, ByRef tarMayor As Double, Optional ByRef Message As String = "") As String
                tarMenor = 0

                Dim minima As Double = Nothing
                Dim maxima As Double = Nothing
                For Each r_Property As DataRow In ds.Tables("Property").Rows
                    If r_Property.Item("PropertyNumber") <> PropertyNumber Then Continue For
                    If r_Property.GetChildRows("Property_RoomType").Length = 0 Then Continue For
                    If r_Property.GetChildRows("Property_RoomType")(0).GetChildRows("RoomType_RatePlan").Length = 0 Then Continue For

                    For Each r_RoomType As DataRow In r_Property.GetChildRows("Property_RoomType")
                        For Each r_RatePlan As DataRow In r_RoomType.GetChildRows("RoomType_RatePlan")
                            'If r_RatePlan.Item("Available").ToString.ToUpper = "N" AndAlso r_RatePlan.Item("Segment").ToString.ToUpper <> "K" Then Continue For
                            If r_RatePlan.Item("Available").ToString.ToUpper = "N" Then
                                Message = r_RatePlan.Item("Message").ToString
                                Continue For
                            End If

                            'AltPackPrice
                            'If minima = Nothing Then minima = CDbl(r.Item("AltPackPrice"))
                            'If maxima = Nothing Then maxima = CDbl(r.Item("AltPackPrice"))
                            'If CDbl(r.Item("AltPackPrice")) < minima Then minima = CDbl(r.Item("AltPackPrice"))
                            'If CDbl(r.Item("AltPackPrice")) > maxima Then maxima = CDbl(r.Item("AltPackPrice"))

                            '

                            Dim cant As Double
                            If r_RatePlan.Item("Segment").ToString.ToUpper = "K" Then ' AndAlso r_RatePlan.Table.Columns.Contains("Nights") 
                                cant = CDbl(r_RatePlan.Item("AltPackPrice"))
                            Else
                                cant = CDbl(r_RatePlan.Item("AltAvgRate"))
                            End If

                            If minima = Nothing Then minima = cant
                            If maxima = Nothing Then maxima = cant
                            If cant < minima Then minima = cant
                            If cant > maxima Then maxima = cant

                            tarMenor = minima
                            tarMayor = maxima
                        Next
                    Next

                    Exit For
                Next

                If minima = Nothing OrElse maxima = Nothing Then Return ""

                'Return Format(minima, "c") + " " + ds.Tables("Property").Rows(0).Item("AltCurrency") + " - " + Format(maxima, "c") + " " + ds.Tables("Property").Rows(0).Item("AltCurrency")
                Return CapaPresentacion.Common.moneda.FormateaMoneda(minima, ds.Tables("Property").Rows(0).Item("AltCurrency")) + " - " + CapaPresentacion.Common.moneda.FormateaMoneda(maxima, ds.Tables("Property").Rows(0).Item("AltCurrency"))
            End Function

            Private Shared Function GetPropertyNumbers(ByVal ServiceProvider As String, ByVal t As DataTable) As String
                Dim PropertyNumbers As String = ""
                For Each r As DataRow In t.Rows
                    If r.Item("ServiceProvider") <> ServiceProvider Then Continue For

                    If PropertyNumbers <> "" Then PropertyNumbers += ","
                    PropertyNumbers += r.Item("PropertyNumber")
                Next

                Return PropertyNumbers
            End Function

            Private Shared Function GetProviders(ByVal t As DataTable) As String()
                Dim Providers As New List(Of String)
                For Each r As DataRow In t.Rows
                    If Providers.Contains(r.Item("ServiceProvider")) Then Continue For

                    'If Providers <> "" Then Providers += ","
                    Providers.Add(r.Item("ServiceProvider"))
                Next

                Return Providers.ToArray
            End Function

            Public Shared Function obten_NOgeneral_HOC(ByVal prop_numbers As String, ByVal ServiceProvider As String, ByVal datos_b As dataBusqueda_hotel_test) As DataSet
                Try
                    Dim data As New datos_HOC
                    data.CheckinDate = Format(datos_b.llegada, CapaPresentacion.Common.DateDataFormat)
                    data.CheckoutDate = Format(datos_b.salida, CapaPresentacion.Common.DateDataFormat)
                    data.AccessCode = datos_b.codProm
                    data.ReferenceContract = datos_b.Convenio
                    data.Segment.Add("P")
                    data.Segment.Add("R")
                    data.Segment.Add("C")
                    data.Segment.Add("K")
                    data.Segment.Add("O") 'Convenios
                    data.PropertyNumber = prop_numbers
                    data.ServiceProvider = ServiceProvider
                    data.AltCurrency = datos_b.monedaBusqueda

                    Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_HOC(data, datos_b)
                    If ds Is Nothing OrElse Not ds.Tables.Contains("RatePlan") OrElse ds.Tables("RatePlan").Rows.Count = 0 Then Return Nothing

                    Return ds
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0111", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End Try

                Return Nothing
            End Function

            Public Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal todos As Boolean)
                Try
                    If grid.DataSource Is Nothing Then Exit Sub

                    For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In grid.DisplayLayout.Bands(0).Columns
                        col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
                    Next

                    Dim dv As DataView = grid.DataSource
                    dv.Sort = "ServiceProvider ASC, TarifaMenorOrdenar DESC, PropertyName ASC"

                    If todos Then
                        dv.RowFilter = ""
                    Else
                        dv.RowFilter = "Tarifas <> ''"
                    End If

                    grid.DataSource = CapaPresentacion.Hotels.SearchHotel.NumberDV(dv)
                    If grid.DisplayLayout.Bands.Count > 1 Then grid.DisplayLayout.Bands(1).Hidden = True

                    For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In grid.Rows
                        If r.Cells("Tarifas").Value Is System.DBNull.Value OrElse r.Cells("Tarifas").Value = "" Then r.Appearance.ForeColor = Color.Gray
                    Next
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0116", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                End Try
            End Sub

            Public Shared Sub SearchDates(ByVal frm As busquedaHotel)
                Dim fecha_menor As DateTime = DateTime.Now
                frm.dtp_in.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
                frm.dtp_out.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day).AddDays(1)
                frm.dtp_in.Value = DateTime.Now.AddDays(1)
                frm.dtp_out.Value = frm.dtp_in.Value.AddDays(1)
            End Sub

            Public Shared Function AllowNumberDays(ByVal frm As busquedaHotel) As Boolean
                If (frm.dtp_out.Value - frm.dtp_in.Value).TotalDays > MaxDaysAllowed Then Return False Else Return True
            End Function

            Public Shared Sub StopSearch(ByVal frm As busquedaHotel)
                If Not frm.Thread_ColoresTarifas Is Nothing Then frm.Thread_ColoresTarifas.Abort()
                If Not frm.Thread_AgregaTarifas Is Nothing Then frm.Thread_AgregaTarifas.Abort()
                If Not frm.Thread_BuscaHoteles Is Nothing Then frm.Thread_BuscaHoteles.Abort()
                CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)
                'CapaPresentacion.Common.FijaProgressBar(frm, 0, False)

                For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In frm.UltraGrid1.Rows
                    If r.Cells("Tarifas").Value.ToString.Contains("...") Then r.Cells("Tarifas").Value = ""
                Next
            End Sub

            Public Shared Sub cargar_convenios(ByVal frm As busquedaHotel, ByVal ds As DataSet, ByVal ultima As String)
                Try
                    If Not ds Is Nothing AndAlso ds.Tables.Contains("Agreement") AndAlso ds.Tables("Agreement").Rows.Count > 0 Then
                        CapaPresentacion.Hotels.SearchHotel.autocompletarConveios(frm, ds.Tables(0), ultima)
                    End If

                Catch ex As Exception
                    'CapaLogicaNegocios.ErrorManager.Manage("E0112", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                Finally
                    'CapaPresentacion.Common.Fija_CTLVISIBLE(frm.PictureBox2, False)
                    '   Mostrar apagar indicador de tarea asincrona
                End Try
            End Sub
            Public Shared Sub cargar_ratesplan(ByVal frm As busquedaHotel, ByVal ds As DataSet)
                Try
                    If ds Is Nothing Then
                        CapaPresentacion.Hotels.SearchHotel.autocompletarRatesplan(frm, Nothing)
                    ElseIf ds.Tables.Contains("Ratesplan") AndAlso ds.Tables("Ratesplan").Rows.Count > 0 Then
                        CapaPresentacion.Hotels.SearchHotel.autocompletarRatesplan(frm, ds.Tables(0))
                    End If

                Catch ex As Exception
                    'CapaLogicaNegocios.ErrorManager.Manage("E0112", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                End Try
            End Sub
        End Class

        Public Class HotelBook

            Public Shared Sub consulta(ByVal frm As reservar)
                If frm.cambiaron Then
                    CapaPresentacion.Hotels.HotelBook.refresh_datosBusqueda(frm)
                    CapaPresentacion.Hotels.HotelBook.display_controles(frm)
                    If frm.lbl_mensaje.Text = "" Then
                        MsgBox(CapaPresentacion.Idiomas.get_str("str_419_Updated"), MsgBoxStyle.Information)
                    End If
                    frm.cambiaron = False
                Else
                    MsgBox(CapaPresentacion.Idiomas.get_str("str_419_Updated"), MsgBoxStyle.Information)
                End If
            End Sub

            Public Shared Sub cambio_valor(ByVal frm As reservar)
                Dim d_o As Date = frm.dtp_out.Value
                Dim d_i As Date = frm.dtp_in.Value

                d_o = New Date(DatePart(DateInterval.Year, d_o), DatePart(DateInterval.Month, d_o), DatePart(DateInterval.Day, d_o))
                d_i = New Date(DatePart(DateInterval.Year, d_i), DatePart(DateInterval.Month, d_i), DatePart(DateInterval.Day, d_i))

                If d_o <= d_i Then frm.dtp_out.Value = frm.dtp_in.Value.AddDays(1)
            End Sub

            Public Shared Sub make_reservar(ByVal frm As reservar)
                'If Not verifica_datos_call() Then Return

                If frm.tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado Then
                    misForms.newCall.datos_call.rem_rvaHotel(frm.datosBusqueda_hotel.t_id)
                End If

                'If frm.tipoAccion = enumReservar_TipoAccion.reservar Then CapaPresentacion.Hotels.HotelBook.refresh_datosBusqueda(frm)
                CapaPresentacion.Hotels.HotelBook.refresh_datosBusqueda(frm)
                If misForms.newCall.datos_call.add_rvaHotel(frm.datosBusqueda_hotel.Clone, True) Then 'TODO permitir multiples 
                    CapaPresentacion.Calls.ActivaNewCall()
                    frm.Close()
                Else
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0297_soloUno"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End Sub

            Public Shared Function make_modify(ByVal frm As reservar) As Boolean
                CapaPresentacion.Hotels.HotelBook.refresh_datosBusqueda(frm)
                Dim ds As DataSet = obtenGenerales_hoteles.obten_general_modify(frm.datosBusqueda_hotel)

                If ds Is Nothing OrElse ds.Tables.Count = 0 Then
                    MsgBox(CapaPresentacion.Idiomas.get_str("str_408_ErrConnServ"), MsgBoxStyle.Exclamation)
                    Return False
                End If

                If Not ds.Tables("error") Is Nothing Then
                    Dim r As DataRow = ds.Tables("error").Rows(0)
                    MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'Me.Cursor = Cursors.Arrow
                Else

                    Dim wsCall As New webService_call()
                    wsCall.modReservations(frm.datosBusqueda_hotel, misForms.newCall.datos_call.idCall, frm.datosBusqueda_hotel.ConfirmNumber)

                    Dim r As DataRow = ds.Tables("Reservation").Rows(0)
                    'MessageBox.Show(r.Item("MarketText"), CapaPresentacion.Idiomas.get_str("str_0157_resModi"), MessageBoxButtons.OK, MessageBoxIcon.Information)

                    misForms.reservaciones.load_data()
                    Return True
                End If
            End Function
            Public Shared Function make_changes(ByVal frm As cambiar_reservacion) As Boolean
                Dim wsCall As New webService_call()

                wsCall.modReservationsNoRestriction(frm.dsHotelChanges)

                Dim flag As Boolean = CapaLogicaNegocios.GetWS.obten_hotel_changes(frm.dsHotelChanges)
                Return flag

            End Function


        End Class

    End Class

    Public Class Activities

        Public Class SearchActivity

            Delegate Sub FIJA_GRID_Callback(ByVal frm As Form, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
            Private Shared Sub FIJA_GRID(ByVal frm As Form, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
                Try
                    If grid.InvokeRequired Then
                        Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                        frm.Invoke(d, New Object() {frm, grid, ds})
                    Else
                        formatea_grid(grid, ds)
                        CapaPresentacion.Idiomas.cambiar_busquedaActividad(frm)
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0117", "", CapaLogicaNegocios.ErrorManager.ErrorAction.Show, ErrorManager.ErrorSeverity.Atention)
                End Try
            End Sub

            Delegate Sub FIJA_PICT_Callback(ByVal frm As Form, ByVal pic As PictureBox, ByVal vis As Boolean)
            Private Shared Sub FIJA_PICT(ByVal frm As Form, ByVal pic As PictureBox, ByVal vis As Boolean)
                Try
                    If pic.InvokeRequired Then
                        Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
                        frm.Invoke(d, New Object() {frm, pic, vis})
                    Else
                        pic.Visible = vis
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0118", "", CapaLogicaNegocios.ErrorManager.ErrorAction.Show, ErrorManager.ErrorSeverity.Atention)
                End Try
            End Sub

            Delegate Sub Fija_LBL_Callback(ByVal frm As Form, ByVal lbl As Label, ByVal str As String)
            Private Shared Sub Fija_LBL(ByVal frm As Form, ByVal lbl As Label, ByVal str As String)
                Try
                    If lbl.InvokeRequired Then
                        Dim d As New Fija_LBL_Callback(AddressOf Fija_LBL)
                        frm.Invoke(d, New Object() {frm, lbl, str})
                    Else
                        lbl.Text = str
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0119", "", CapaLogicaNegocios.ErrorManager.ErrorAction.Show, ErrorManager.ErrorSeverity.Atention)
                End Try
            End Sub

            Public Shared Sub load_data(ByVal frm As busquedaActividad)
                Try
                    'Me.Cursor = Cursors.WaitCursor

                    If frm.cs_ciudad.ListBox1.SelectedIndex = -1 OrElse frm.cs_ciudad.lista_lugares.Count = 0 Then
                        MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Else
                        frm.datosBusqueda_actividad.rubro = enumServices.Actividad
                        frm.datosBusqueda_actividad.confirmada = False
                        '
                        frm.datosBusqueda_actividad.ciudadActividad = frm.cs_ciudad.selected_valor_Codigo
                        frm.datosBusqueda_actividad.ciudadActividadNombre = frm.cs_ciudad.selected_valor_Ciudad
                        frm.datosBusqueda_actividad.fechaDesde = frm.dtp_fechaDesde.Value
                        frm.datosBusqueda_actividad.fechaHasta = frm.dtp_fechaHasta.Value
                        frm.datosBusqueda_actividad.monedaBusqueda = frm.uce_moneda.SelectedItem.DataValue
                        frm.datosBusqueda_actividad.codigoAcceso = frm.txt_CodigoAcceso.Text
                        frm.datosBusqueda_actividad.codigoPromocion = frm.txt_CodigoPromocion.Text
                        'datosBusqueda_actividad.adultos = "" 'txt_adultos.Text

                        frm.PictureBox2.Visible = True
                        frm.MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                        frm.MI_Thread.Start(frm)
                    End If
                Catch ex As Exception
                    'CapaLogicaNegocios.LogManager.agregar_registro("", )
                    'MessageBox.Show(ex.Message, CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)

                    CapaLogicaNegocios.ErrorManager.Manage("E0003", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                Finally
                    'Me.Cursor = Cursors.Arrow
                End Try
            End Sub

            Public Shared Sub hacer_busqueda_async(ByVal objFrm As Object)
                Dim frm As busquedaActividad = CType(objFrm, busquedaActividad)

                Dim ds As New DataSet
                ds = obtenGenerales_actividades.obten_general_IDX2(frm.datosBusqueda_actividad.Clone)
                Dim str_resp As String
                If ds Is Nothing OrElse Not ds.Tables.Contains("Event") Then
                    str_resp = "Total de actividades: 0"
                Else
                    str_resp = "Total de actividades: " + CStr(ds.Tables("Event").Rows.Count)
                End If

                FIJA_GRID(frm, frm.UltraGrid1, ds)
                FIJA_PICT(frm, frm.PictureBox2, False)
                Fija_LBL(frm, frm.data_lbl_act, str_resp)
            End Sub

            Private Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
                If ds Is Nothing Then
                    grid.DataSource = Nothing
                    Return
                End If
                If ds.Tables.Contains("Error") Then
                    MessageBox.Show(ds.Tables("Error").Rows(0).Item("Message"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    grid.DataSource = Nothing
                    Return
                End If

                If Not ds.Tables.Contains("Event") Then
                    grid.DataSource = Nothing
                    Return
                End If

                Dim miDT As New DataTable("miDT")
                miDT.Columns.Add("Numero", GetType(Integer))
                miDT.Columns.Add("PropertyName")
                miDT.Columns.Add("PropertyID")
                miDT.Columns.Add("ActivityName")
                miDT.Columns.Add("ActivityID")
                miDT.Columns.Add("ActivityDescription")
                miDT.Columns.Add("ActivityLocation")
                miDT.Columns.Add("CategoryGroupName")
                miDT.Columns.Add("CategoryName")
                miDT.Columns.Add("EventName")
                miDT.Columns.Add("EventID")
                miDT.Columns.Add("AltFromPrice")
                miDT.Columns.Add("AltCurrency")
                miDT.Columns.Add("TotalOrdenar", GetType(Double))

                For Each r1 As DataRow In ds.Tables("Event").Rows
                    Dim PropertyName As String = r1.GetParentRow("Activity_Event").GetParentRow("Property_Activity").Item("PropertyName")
                    Dim PropertyID As String = r1.GetParentRow("Activity_Event").GetParentRow("Property_Activity").Item("PropertyID")
                    Dim ActivityName As String = r1.GetParentRow("Activity_Event").Item("ActivityName")
                    Dim ActivityID As String = r1.GetParentRow("Activity_Event").Item("ActivityID")
                    Dim ActivityDescription As String = r1.GetParentRow("Activity_Event").Item("ActivityDescription")
                    Dim ActivityLocation As String = r1.GetParentRow("Activity_Event").Item("ActivityLocation")
                    Dim CategoryGroupName As String = r1.GetParentRow("Activity_Event").Item("CategoryGroupName")
                    Dim CategoryName As String = r1.GetParentRow("Activity_Event").Item("CategoryName")
                    Dim EventName As String = r1.Item("EventName")
                    Dim EventID As String = r1.Item("EventID")
                    Dim AltFromPrice As String = r1.Item("AltFromPrice")
                    Dim AltCurrency As String = r1.Item("AltCurrency")
                    Dim TotalOrdenar As Double = CDbl(r1.Item("AltFromPrice"))
                    ActivityDescription = CapaPresentacion.Common.HTMLDecode(ActivityDescription).Trim
                    AltFromPrice = CapaPresentacion.Common.moneda.FormateaMoneda(AltFromPrice, AltCurrency)

                    miDT.Rows.Add(New String() {"0", PropertyName, PropertyID, ActivityName, ActivityID, ActivityDescription, ActivityLocation, CategoryGroupName, CategoryName, EventName, EventID, AltFromPrice, AltCurrency, TotalOrdenar})
                Next

                miDT.Columns("Numero").SetOrdinal(0)

                Dim dv As DataView = miDT.DefaultView
                dv.Sort = "TotalOrdenar asc"
                grid.DataSource = dv
                'grid.DisplayLayout.Bands(1).Hidden = True

                CapaPresentacion.Common.NumberGrid(grid)
            End Sub

        End Class

        Public Class ActivityEvents

            Delegate Sub FIJA_PANEL_Callback(ByVal frm As eventosActividades, ByVal panel As Panel, ByVal ds As DataSet)
            Private Shared Sub FIJA_PANEL(ByVal frm As eventosActividades, ByVal panel As Panel, ByVal ds As DataSet)
                Try
                    If panel.InvokeRequired Then
                        Dim d As New FIJA_PANEL_Callback(AddressOf FIJA_PANEL)
                        frm.Invoke(d, New Object() {frm, panel, ds})
                    Else
                        formatea_panel(panel, ds, New actFareAvail.actFareDelegate(AddressOf frm.Total_Changed))


                        'CapaPresentacion.Idiomas.cambiar_eventosActividades(frm)
                        'CapaPresentacion.Common.NumberGrid(grid)
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0120", ex.Message, ErrorManager.ErrorAction.LogAndShow, ErrorManager.ErrorSeverity.Atention)
                End Try
            End Sub

            Delegate Sub FIJA_GRID_Callback(ByVal frm As eventosActividades, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
            Private Shared Sub FIJA_GRID(ByVal frm As eventosActividades, ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
                Try
                    If grid.InvokeRequired Then
                        Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                        frm.Invoke(d, New Object() {frm, grid, ds})
                    Else
                        formatea_grid(grid, ds)


                        CapaPresentacion.Idiomas.cambiar_eventosActividades(frm)
                        CapaPresentacion.Common.NumberGrid(grid)
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0120", ex.Message, ErrorManager.ErrorAction.LogAndShow, ErrorManager.ErrorSeverity.Atention)
                End Try
            End Sub

            Delegate Sub FIJA_PICT_Callback(ByVal frm As eventosActividades, ByVal pic As PictureBox, ByVal vis As Boolean)
            Private Shared Sub FIJA_PICT(ByVal frm As eventosActividades, ByVal pic As PictureBox, ByVal vis As Boolean)
                Try
                    If pic.InvokeRequired Then
                        Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
                        frm.Invoke(d, New Object() {frm, pic, vis})
                    Else
                        pic.Visible = vis
                    End If
                Catch ex As Exception
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub

            Public Shared Sub load_data(ByVal frm As eventosActividades)
                Try
                    'frm.Cursor = Cursors.WaitCursor
                    inicializar_controles(frm)

                    If False Then
                        'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_ str("str_0155_ atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Else
                        frm.PictureBox2.Visible = True
                        frm.MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                        frm.MI_Thread.Start(frm)
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0006", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                Finally
                    'Me.Cursor = Cursors.Arrow
                End Try
            End Sub

            Private Shared Sub formatea_panel(ByVal panel As Panel, ByVal ds As DataSet, ByVal delegado As actFareAvail.actFareDelegate)
                If Not ds.Tables.Contains("RateType") Or ds.Tables.Contains("Error") Then
                    Return
                End If

                Dim TicketsAvailable As Integer = 0, EventTickets As Integer = 0
                Dim AltCurrency As String = "", PercentTax As String = ""
                Dim UCs As List(Of actFareAvail) = New List(Of actFareAvail)

                If ds.Tables("Event").Rows.Count > 0 Then
                    TicketsAvailable = CInt(ds.Tables("Event").Rows(0).Item("TicketsAvailableSeason"))
                    EventTickets = CInt(ds.Tables("Event").Rows(0).Item("EventTickets"))
                    PercentTax = ds.Tables("Event").Rows(0).Item("PercentTax")
                End If

                If ds.Tables("Property").Rows.Count > 0 Then AltCurrency = ds.Tables("Property").Rows(0).Item("AltCurrency")

                Dim posX As Integer = 0, fecha As String, hora As String
                For Each rate As DataRow In ds.Tables("Rate").Rows

                    Dim uc As actFareAvail = Nothing
                    fecha = rate.Item("Day").ToString().Substring(0, 8)
                    hora = rate.Item("Day").ToString().Substring(8)
                    If (hora.Length > 0) Then
                        hora = hora.Insert(2, ":")

                    Else
                        hora = "Cualquier Hora"
                    End If

                    For i As Integer = 0 To UCs.Count - 1
                        If (UCs(i).Fecha = fecha) Then
                            uc = UCs(i)
                            Exit For
                        End If

                    Next

                    If uc Is Nothing Then
                        ' si no encontro crea uno nuevo
                        uc = New actFareAvail(fecha, TicketsAvailable, EventTickets, AltCurrency, PercentTax)
                        UCs.Add(uc)
                        AddHandler uc.TotalChanged, delegado

                        Dim rateTypes As DataRow() = rate.GetChildRows("Rate_RateType")
                        For Each rateType As DataRow In rateTypes

                            uc.AddRate(rate.Item("RateID"), _
                                rateType.Item("RateTypeID"), _
                                rateType.Item("RateTypeName"), _
                                rate.Item("Day"), _
                                rateType.Item("MinTickets").ToString(), _
                                rateType.Item("MaxTickets"), _
                                rateType.Item("AltRateTypePrice"), _
                                CapaPresentacion.Common.moneda.FormateaMoneda(rateType.Item("AltRateTypePrice"), AltCurrency), _
                                hora)
                        Next


                        panel.Controls.Add(uc)
                        uc.Location = New Point(posX, 3)
                        uc.AutoSize = True
                        posX = uc.Width + uc.Left + 3
                    Else

                        Dim rateTypes As DataRow() = rate.GetChildRows("Rate_RateType")
                        For Each rateType As DataRow In rateTypes

                            uc.AddRate(rate.Item("RateID"), _
                                rateType.Item("RateTypeID"), _
                                rateType.Item("RateTypeName"), _
                                rate.Item("Day"), _
                                rateType.Item("MinTickets").ToString(), _
                                rateType.Item("MaxTickets"), _
                                rateType.Item("AltRateTypePrice"), _
                                CapaPresentacion.Common.moneda.FormateaMoneda(rateType.Item("AltRateTypePrice"), AltCurrency), _
                                hora)
                        Next
                    End If
                Next

                'Dim AltCurrency As String = ""
                'Dim TicketsAvailable As Integer = 0
                'Dim posX As Integer = 0

                'If ds.Tables("Property").Rows.Count > 0 Then AltCurrency = ds.Tables("Property").Rows(0).Item("AltCurrency")
                'If ds.Tables("Event").Rows.Count > 0 Then TicketsAvailable = CInt(ds.Tables("Event").Rows(0).Item("TicketsAvailableSeason"))

                'For Each rate As DataRow In ds.Tables("Rate").Rows
                '    Dim p As Panel = New Panel()
                '    p.Location = New Point(posX, 0)

                '    Dim hr As String = rate.Item("Day")
                '    '******* lbl fecha ********
                '    Dim lblFec As Label = New Label
                '    With lblFec
                '        .Text = DateTime.ParseExact(hr.Substring(0, 8), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("dd/MMM/yyyy")
                '        .Location = New Point(3, 0)
                '        .Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                '    End With
                '    p.Controls.Add(lblFec)

                '    Dim chkHr As CheckBox = New CheckBox()
                '    With chkHr
                '        .Location = New Point(3, 22)
                '        If (CInt(rate.Item("TicketsAvailableHourly")) < TicketsAvailable) Then
                '            TicketsAvailable = CInt(rate.Item("TicketsAvailableHourly"))
                '        End If

                '        If hr.Length > 8 Then
                '            'Revisar si disponibles estan bien
                '            .Text = hr.Substring(8).Insert(2, ":") + " (Disponibles:" + TicketsAvailable + ")"
                '        Else
                '            .Text = "Cualquier hora (Disponibles:" + TicketsAvailable.ToString() + ")"
                '        End If
                '        .AutoSize = True
                '    End With
                '    p.Controls.Add(chkHr)

                '    '******* rateTypes ********
                '    Dim PosY As Integer = 44
                '    Dim rateTypes As DataRow() = rate.GetChildRows("Rate_RateType")
                '    For Each rateType As DataRow In rateTypes

                '        '*********************************
                '        Dim lblPrice As Label = New Label()
                '        With lblPrice
                '            .Text = CapaPresentacion.Common.moneda.FormateaMoneda(rateType.Item("AltRateTypePrice"), AltCurrency)
                '            .Location = New Point(3, PosY)
                '            .AutoSize = True
                '        End With
                '        p.Controls.Add(lblPrice)


                '        '*********************************
                '        Dim ddlRate As ComboBox = New ComboBox()
                '        With ddlRate
                '            .Location = New Point(88, PosY)
                '            .Width = 40
                '            .Tag = rateType.Item("RateTypeID")
                '        End With
                '        p.Controls.Add(ddlRate)

                '        For i As Integer = CInt(rateType.Item("MinTickets")) To CInt(rateType.Item("MaxTickets"))
                '            ddlRate.Items.Add(i)
                '        Next

                '        '*********************************
                '        Dim lblRate As Label = New Label()
                '        With lblRate
                '            .Location = ddlRate.Location + New Point(3 + ddlRate.Width, 0)
                '            .Text = rateType.Item("RateTypeName")
                '            .AutoSize = True
                '        End With
                '        p.Controls.Add(lblRate)

                '        PosY = lblPrice.Location.Y + lblPrice.Height + 8
                '    Next

                '    p.BorderStyle = BorderStyle.FixedSingle
                '    panel.Controls.Add(p)
                '    p.AutoSize = True
                '    posX = p.Location.X + p.Width + 5

                'Next
            End Sub

            Private Shared Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)

                If ds.Tables.Contains("Error") Then
                    MessageBox.Show(ds.Tables("Error").Rows(0).Item("Message"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'grid.DataSource = Nothing
                    Return
                End If

                If Not ds.Tables.Contains("RateType") Then
                    'grid.DataSource = Nothing
                    Return
                End If

                Dim miDT As New DataTable("miDT")
                miDT.Columns.Add("Numero", GetType(Integer))
                miDT.Columns.Add("PropertyName")
                miDT.Columns.Add("AltCurrency")
                miDT.Columns.Add("EventName")
                miDT.Columns.Add("Duration")
                miDT.Columns.Add("Weekdays")
                miDT.Columns.Add("Available")
                miDT.Columns.Add("TicketType")
                miDT.Columns.Add("EventTickets")
                miDT.Columns.Add("PercentTax")
                miDT.Columns.Add("RateID")
                miDT.Columns.Add("Day")
                miDT.Columns.Add("HourlyTickets")
                miDT.Columns.Add("TicketsAvailableHourly")
                miDT.Columns.Add("RateTypeName")
                miDT.Columns.Add("RateTypeID")
                miDT.Columns.Add("AltRateTypePrice", GetType(Double))
                miDT.Columns.Add("MinTickets")
                miDT.Columns.Add("MaxTickets")
                miDT.Columns.Add("DayReservar")
                miDT.Columns.Add("costoTotalFormato")
                miDT.Columns.Add("TotalOrdenar", GetType(Double))

                For Each r1 As DataRow In ds.Tables("RateType").Rows

                    Dim PropertyName As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").GetParentRow("Property_Event").Item("PropertyName")
                    Dim AltCurrency_ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").GetParentRow("Property_Event").Item("AltCurrency")
                    Dim EventName___ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("EventName")
                    Dim Duration____ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("Duration")
                    Dim Weekdays____ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("Weekdays")
                    Dim Available___ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("Available")
                    Dim TicketType__ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("TicketType")
                    Dim EventTickets As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("EventTickets")
                    Dim PercentTax__ As String = r1.GetParentRow("Rate_RateType").GetParentRow("Event_Rate").Item("PercentTax")
                    Dim RateID______ As String = r1.GetParentRow("Rate_RateType").Item("RateID")
                    Dim Day_________ As String = r1.GetParentRow("Rate_RateType").Item("Day")
                    Dim HourlyTicket As String = r1.GetParentRow("Rate_RateType").Item("HourlyTickets")
                    Dim TicketsAvail As String = r1.GetParentRow("Rate_RateType").Item("TicketsAvailableHourly")
                    Dim RateTypeName As String = r1.Item("RateTypeName")
                    Dim RateTypeID__ As String = r1.Item("RateTypeID")
                    Dim AltRateTypeP As String = r1.Item("AltRateTypePrice")
                    Dim MinTickets__ As String = r1.Item("MinTickets")
                    Dim MaxTickets__ As String = r1.Item("MaxTickets")
                    Dim DayReservar_ As String = Day_________
                    Dim costoTotalFo As String = ""







                    AltRateTypeP = CStr(CDbl(AltRateTypeP) * (CDbl(PercentTax__) / 100 + 1))
                    costoTotalFo = CapaPresentacion.Common.moneda.FormateaMoneda(AltRateTypeP, AltCurrency_) ' Format(CDbl(AltRateTypeP), "c") + " " + AltCurrency_
                    Dim dias As String = ""
                    For i As Integer = 0 To Weekdays____.Length - 1
                        If Weekdays____.Substring(i, 1) = "1" Then
                            Dim letra As String = ""

                            Select Case i
                                Case 0
                                    letra = "Lunes"
                                Case 1
                                    letra = "Martes"
                                Case 2
                                    letra = "Miercoles"
                                Case 3
                                    letra = "Jueves"
                                Case 4
                                    letra = "Viernes"
                                Case 5
                                    letra = "Sabado"
                                Case 6
                                    letra = "Domingo"
                            End Select

                            dias += letra + ", "
                        End If

                        'grid.Rows.Add(New String() {"0",})
                    Next
                    Weekdays____ = dias.Trim(" ").Trim(",")
                    If Available___ = "Y" Then Available___ = "Si" Else Available___ = "No"
                    Day_________ = DateTime.ParseExact(Day_________.Substring(0, 8), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("dd/MMM/yyyy")

                    miDT.Rows.Add(New String() {"0", PropertyName, AltCurrency_, EventName___, Duration____, Weekdays____, Available___, TicketType__, EventTickets, PercentTax__, RateID______, Day_________, HourlyTicket, TicketsAvail, RateTypeName, RateTypeID__, AltRateTypeP, MinTickets__, MaxTickets__, DayReservar_, costoTotalFo, CDbl(AltRateTypeP)})
                Next



                Dim dv As DataView = miDT.DefaultView
                dv.Sort = "AltRateTypePrice asc"
                grid.DataSource = dv


            End Sub

            Private Shared Sub hacer_busqueda_async(ByVal objFrm As Object)
                Dim frm As eventosActividades = CType(objFrm, eventosActividades)

                Dim ds As New DataSet
                ds = obtenGenerales_actividades.obten_general_AVL2(frm.datosBusqueda_actividad.Clone)
                If (frm.UltraGrid1.Visible) Then
                    FIJA_GRID(frm, frm.UltraGrid1, ds)
                Else
                    FIJA_PANEL(frm, frm.panel_Actividades, ds)
                End If
                FIJA_PICT(frm, frm.PictureBox2, False)
            End Sub

            Private Shared Sub inicializar_controles(ByVal frm As eventosActividades)
                'data_lbl_adultos.Text = datosBusqueda_actividad.adultos
                frm.data_lbl_ciudad.Text = frm.datosBusqueda_actividad.ciudadActividadNombre
                frm.data_lbl_fechaDesde.Text = frm.datosBusqueda_actividad.fechaDesde.ToString("dd/MM/yyyy")
                frm.data_lbl_fechaHasta.Text = frm.datosBusqueda_actividad.fechaHasta.ToString("dd/MM/yyyy")
                frm.data_lbl_moneda.Text = frm.datosBusqueda_actividad.monedaBusqueda
                frm.data_lbl_empresa.Text = frm.datosBusqueda_actividad.PropertyName
                frm.data_lbl_evento.Text = frm.datosBusqueda_actividad.EventName

                'frm.lbl_Empresa.Text = frm.datosBusqueda_actividad.pro

            End Sub

        End Class

        Public Class ActivityRules

            Delegate Sub FIJA_WEB_Callback(ByVal frm As reglasActividad, ByVal web As WebBrowser, ByVal ds As DataSet)
            Private Shared Sub FIJA_WEB(ByVal frm As reglasActividad, ByVal web As WebBrowser, ByVal ds As DataSet)
                Try
                    If web.InvokeRequired Then
                        Dim d As New FIJA_WEB_Callback(AddressOf FIJA_WEB)
                        frm.Invoke(d, New Object() {frm, web, ds})
                    Else
                        web.DocumentText = CONST_WB_TOP + GetWBContent(ds) + CONST_WB_BOTTOM
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0121", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow, CapaLogicaNegocios.ErrorManager.ErrorSeverity.Atention)
                End Try
            End Sub

            Delegate Sub FIJA_PICT_Callback(ByVal frm As reglasActividad, ByVal pic As PictureBox, ByVal vis As Boolean)
            Private Shared Sub FIJA_PICT(ByVal frm As reglasActividad, ByVal pic As PictureBox, ByVal vis As Boolean)
                Try
                    If pic.InvokeRequired Then
                        Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
                        frm.Invoke(d, New Object() {frm, pic, vis})
                    Else
                        pic.Visible = vis
                    End If
                Catch ex As Exception
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Sub

            Private Shared Sub inicializar_controles(ByVal frm As reglasActividad)
                'data_lbl_adultos.Text = datosBusqueda_actividad.adultos
                frm.data_lbl_ciudad.Text = frm.datosBusqueda_actividad.ciudadActividadNombre
                frm.data_lbl_fecha.Text = DateTime.ParseExact(frm.datosBusqueda_actividad.DayReservar.Substring(0, 8), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("dd/MMM/yyyy") 'datosBusqueda_actividad.fechaDesde.ToString("dd/MMM/yyyy") + " - " + datosBusqueda_actividad.fechaHasta.ToString("dd/MMM/yyyy")
                frm.data_lbl_moneda.Text = frm.datosBusqueda_actividad.monedaBusqueda
                frm.data_lbl_actividad.Text = frm.datosBusqueda_actividad.EventName
            End Sub

            Public Shared Sub load_data(ByVal frm As reglasActividad)
                Try
                    'Me.Cursor = Cursors.WaitCursor
                    inicializar_controles(frm)

                    If False Then
                        'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_ str("str_0155_ atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Else
                        'datosBusqueda_actividad.ciudadActividad = cs_ciudad.selected_valor_Codigo
                        'datosBusqueda_actividad.fecha = dtp_fecha.Value
                        'datosBusqueda_actividad.moneda = uce_moneda.SelectedItem.DataValue
                        'datosBusqueda_actividad.adultos = txt_adultos.Text

                        frm.PictureBox2.Visible = True
                        frm.MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                        frm.MI_Thread.Start(frm)
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0007", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
                Finally
                    'Me.Cursor = Cursors.Arrow
                End Try
            End Sub

            Public Shared Sub hacer_busqueda_async(ByVal objFrm As Object)
                Dim frm As reglasActividad = CType(objFrm, reglasActividad)

                Dim ds As New DataSet
                ds = obtenGenerales_actividades.obten_general_POE(frm.datosBusqueda_actividad.Clone)

                FIJA_WEB(frm, frm.WebBrowser1, ds)
                FIJA_PICT(frm, frm.PictureBox2, False)
            End Sub

            Private Shared Function GetWBContent(ByVal ds As DataSet) As String
                Dim str As String = "<p><b>" + CapaPresentacion.Idiomas.get_str("str_411_Policies") + "</b></p>"

                If ds Is Nothing Then Return str

                If ds.Tables.Contains("Error") AndAlso ds.Tables("Error").Rows.Count > 0 Then
                    Select Case ds.Tables("Error").Rows(0).Item("ErrorCode")
                        Case "14"
                            Return str + "<p>" + CapaPresentacion.Idiomas.get_str("str_410_NoPol") + "</p>"
                    End Select

                    Return str + "<p>" + ds.Tables("Error").Rows(0).Item("Message") + "</p>"
                End If

                If Not ds.Tables.Contains("Policies") Then Return str

                For Each r As DataRow In ds.Tables("Policies").Rows
                    str += "<p><b>" + r.Item("Title") + "</b></p>"
                    str += "<p>" + r.Item("Descriptions") + "</p>"
                Next

                'str += "<p><b>POLTICAS DE RESERVACIÓN</b></p>"
                'str += "<p>" + r.Item("Reser") + "</p>"
                'str += "<p><b>POLÍTICAS DE PAGO</b></p>"
                'str += "<p>" + r.Item("Pago") + "</p>"

                Return str
            End Function

        End Class

    End Class

    Public Class Vuelos

        Public Shared Function GetAirlines(ByVal AirlinesCode As String) As String
            Select Case AirlinesCode
                Case "AA"
                    Return "American Airlines"
                Case "AC"
                    Return "Air Canada"
                Case "AF"
                    Return "Air France"
                Case "AM"
                    Return "Aeromexico"
                Case "AS"
                    Return "Alaska Airlines"
                Case "AT"
                    Return "Royal Air Maroc"
                Case "AY"
                    Return "Finnair"
                Case "AZ"
                    Return "Alitalia"
                Case "BA"
                    Return "British Airways"
                Case "CO"
                    Return "Continential Airlines"
                Case "DL"
                    Return "Delta Airlines"
                Case "EI"
                    Return "Aer Lingus"
                Case "F9"
                    Return "Frontier Airlines"
                Case "HP"
                    Return "America West"
                Case "IB"
                    Return "Iberia"
                Case "LA"
                    Return "Lan Chile"
                Case "LH"
                    Return "Lufthansa"
                Case "MA"
                    Return "Malev Hungarian"
                Case "MK"
                    Return "Air Mauritius"
                Case "MX"
                    Return "Mexicana"
                Case "NW"
                    Return "Northwest Airlines"
                Case "OS"
                    Return "Austria Air"
                Case "QF"
                    Return "Qantas Airways"
                Case "RG"
                    Return "Varig"
                Case "UA"
                    Return "United Airlines"
                Case "US"
                    Return "US Airways"
            End Select

            Return AirlinesCode
        End Function

    End Class

End Class