Partial Public Class CapaPresentacion

    Public Class Idiomas

        Public Shared _cultura As String = "es"
        Public Shared Property cultura()
            Get
                Return _cultura
            End Get
            Set(ByVal value)
                _cultura = value

                'Dim xmlD As System.Xml.XmlDocument = CapaAccesoDatos.XML.localData.ws_doc()

                'For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
                '    Select Case ws_nodo.Attributes("name").Value
                '        Case "idioma"
                '            ws_nodo.InnerText = _cultura
                '            Exit For
                '    End Select
                'Next

                'xmlD.Save(CapaAccesoDatos.XML.localData.ws_XMLfile_name)

                CapaAccesoDatos.XML.localData.Idioma = _cultura
            End Set
        End Property

        Public Shared Sub cambiar_systema()
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                'operador
                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Exists("ribbon1") Then
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon1").Caption = rm.GetString("str_0200_operador")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon1").Groups("ribbonGroup1").Caption = rm.GetString("str_0201_opcionesOperador")
                End If
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_estadisticas").SharedProps.Caption = rm.GetString("str_0202_estadisticas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_nuevaLlamada").SharedProps.Caption = rm.GetString("str_0203_nuevaLlamada")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_listaLlamadas").SharedProps.Caption = rm.GetString("str_0235_listaLlamadas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_editarLlamada").SharedProps.Caption = rm.GetString("str_0243_editCall")

                'agencia
                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Exists("ribbon3") Then
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon3").Caption = rm.GetString("str_0016_agencia")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon3").Groups("ribbonGroup1").Caption = rm.GetString("str_0017_opcionesRes")
                End If
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservaciones").SharedProps.Caption = rm.GetString("str_0018_reservaciones")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_detalles").SharedProps.Caption = rm.GetString("str_0019_detalles")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Caption = rm.GetString("str_0020_modificar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Caption = rm.GetString("str_0021_cancelar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reactivar").SharedProps.Caption = rm.GetString("str_429_reactive")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Caption = rm.GetString("str_567")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambios").SharedProps.Caption = rm.GetString("str_560")


                'configuracion
                misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon4").Caption = rm.GetString("str_0025_configuracion")
                misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon4").Groups("ribbonGroup1").Caption = rm.GetString("str_0026_accesoSistema")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_configurarAgente").SharedProps.Caption = rm.GetString("str_0027_configurarAgente")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_configurarConexion").SharedProps.Caption = rm.GetString("str_0028_configurarConexion")
                misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon4").Groups("ribbonGroup2").Caption = rm.GetString("str_0029_idioma")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_espa�ol").SharedProps.Caption = rm.GetString("str_0030_espa�ol")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ingles").SharedProps.Caption = rm.GetString("str_0031_ingles")
                misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon4").Groups("ribbonGroup3").Caption = rm.GetString("str_0014_opciones")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambiarLogo").SharedProps.Caption = rm.GetString("str_385_configImage")

                'hoteles
                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Exists("ribbon2") Then
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon2").Caption = rm.GetString("str_0032_hotel")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon2").Groups("ribbonGroup1").Caption = rm.GetString("str_0014_opciones")
                End If
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_informacionGeneral").SharedProps.Caption = rm.GetString("str_0033_informacionGeneral")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_habitaciones").SharedProps.Caption = rm.GetString("str_0008_habitaciones")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_galeria").SharedProps.Caption = rm.GetString("str_0034_galeria")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_mapa").SharedProps.Caption = rm.GetString("str_0035_mapa")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Caption = rm.GetString("str_0036_tarifas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar").SharedProps.Caption = rm.GetString("str_0037_reservar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas").SharedProps.Caption = rm.GetString("str_0038_reglas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_buscar").SharedProps.Caption = rm.GetString("str_0001_buscar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ver_detalles").SharedProps.Caption = rm.GetString("str_529_ver_detalles")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_hotel_disponibilidad").SharedProps.Caption = rm.GetString("str_483")

                'vuelos
                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Exists("ribbon5") Then
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon5").Caption = rm.GetString("str_0279_vuelos")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon5").Groups("ribbonGroup1").Caption = rm.GetString("str_0014_opciones")
                End If
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar_vuelo").SharedProps.Caption = rm.GetString("str_0037_reservar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas_vuelo").SharedProps.Caption = rm.GetString("str_0038_reglas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_buscar_vuelo").SharedProps.Caption = rm.GetString("str_0001_buscar")

                'actividades
                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Exists("ribbon6") Then
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon6").Caption = rm.GetString("str_0280_actividades")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon6").Groups("ribbonGroup1").Caption = rm.GetString("str_0014_opciones")
                End If
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_eventosActividad").SharedProps.Caption = rm.GetString("str_0036_tarifas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarActividad").SharedProps.Caption = rm.GetString("str_0037_reservar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasActividad").SharedProps.Caption = rm.GetString("str_0038_reglas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_buscarActividad").SharedProps.Caption = rm.GetString("str_0001_buscar")

                'autos
                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Exists("ribbon7") Then
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon7").Caption = rm.GetString("str_0281_autos")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon7").Groups("ribbonGroup1").Caption = rm.GetString("str_0014_opciones")
                End If
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifasAuto").SharedProps.Caption = rm.GetString("str_0036_tarifas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarAuto").SharedProps.Caption = rm.GetString("str_0037_reservar")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasAuto").SharedProps.Caption = rm.GetString("str_0038_reglas")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_buscarAuto").SharedProps.Caption = rm.GetString("str_0001_buscar")
                'mision
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_SegmentCompany").SharedProps.Caption = rm.GetString("str_514")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reporte_produccion").SharedProps.Caption = rm.GetString("str_515")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_ProfessionalsAlta").SharedProps.Caption = rm.GetString("str_518")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_professionals_reporte").SharedProps.Caption = rm.GetString("str_519")

                If misForms.form1.UltraToolbarsManager1.Ribbon.Tabs.Contains("ribbon8") Then
                    'misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon8").Groups("ribbonGroup1").Caption = rm.GetString("str_0014_opciones")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon8").Groups("ribbonGroup2").Caption = rm.GetString("str_517")
                    misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon8").Groups("ribbonGroup3").Caption = rm.GetString("str_516")
                End If

                'main menu
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_salir").SharedProps.Caption = rm.GetString("str_0039_salir")
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cerrarSesion").SharedProps.Caption = rm.GetString("str_0115_cerrarSesion")

                'cambiar controles de cada Hijo
                For Each frm As Form In misForms.form1.MdiChildren
                    Select Case frm.Name
                        Case "newCall"
                            cambiar_newCall(frm)
                        Case "datosCliente"
                            cambiar_datosCliente(frm)
                        Case "listaLlamadas"
                            cambiar_listaLlamadas(frm)
                        Case "reservaciones"
                            cambiar_reservaciones(frm)
                        Case "detalles"
                            cambiar_detalles(frm)
                        Case "busquedaHotel"
                            cambiar_busquedaHotel(frm)
                        Case "tarifas"
                            cambiar_tarifas(frm)
                        Case "busquedaVuelo"
                            cambiar_busquedaVuelo(frm)
                        Case "busquedaActividad"
                            cambiar_busquedaActividad(frm)
                        Case "eventosActividades"
                            cambiar_eventosActividades(frm)
                        Case "reglasActividad"
                            MsgBox("verificar si llega aqui")
                            cambiar_reglasActividad(frm)
                        Case "busquedaAutos"
                            cambiar_busquedaAuto(frm)
                        Case "tarifasAuto"
                            cambiar_tarifasAuto(frm)
                    End Select
                Next

            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0040", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
            End Try
        End Sub

        Public Shared Function get_str(ByVal key As String) As String
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                Return rm.GetString(key)
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0041", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try

            Return "Error de idioma"
        End Function

        Public Shared Function get_str(ByVal key As String, ByVal idioma As String) As String
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(idioma)

                Return rm.GetString(key)
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0042", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try

            Return "Error de idioma"
        End Function

        Public Shared ReadOnly Property IsCulturaEspanol()
            Get
                If Idiomas.cultura = "es" Then Return True Else Return False
            End Get
        End Property

        Public Shared ReadOnly Property IsCulturaIngles()
            Get
                If Idiomas.cultura = "en" Then Return True Else Return False
            End Get
        End Property

        Public Shared ReadOnly Property idIdioma()
            Get
                If Idiomas.cultura = "es" Then Return "1"
                If Idiomas.cultura = "en" Then Return "2"

                Return "--"
            End Get
        End Property

        'child - tambien en el LOAD
        Public Shared Sub cambiar_newCall(ByVal frm As newCall)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.ugb_contacto.Text = rm.GetString("str_0205_infoContacto")
                frm.lbl_nombre.Text = rm.GetString("str_0206_cualNombre")
                frm.lbl_Apellidos.Text = rm.GetString("str_0169_apellido") & "?"
                frm.rbt_masculino.Text = rm.GetString("str_0208_masculino")
                frm.rbt_femenino.Text = rm.GetString("str_0209_femenino")
                frm.lbl_saludos.Text = rm.GetString("str_311_tipoAyuda")
                frm.rbt_reservar.Text = rm.GetString("str_0216_reservar")
                frm.rbt_disponibilidad.Text = rm.GetString("str_0217_verDisponibilidad")
                frm.rbt_info.Text = rm.GetString("str_0218_solicitarInfo")
                frm.rbt_otro.Text = rm.GetString("str_0219_otro")
                frm.lbl_telefono.Text = rm.GetString("str_0118_telefono")
                frm.lbl_email.Text = rm.GetString("str_0148_email")
                frm.UltraButton1.Text = rm.GetString("str_0221_iniciar")
                frm.UltraButton2.Text = rm.GetString("str_0234_finalizar")
                frm.btn_hotel.Text = rm.GetString("str_0222_buscHoteles")
                frm.btn_vuelos.Text = rm.GetString("str_0223_buscVuelos")
                frm.btn_actividades.Text = rm.GetString("str_0224_buscActividades")
                frm.btn_autos.Text = rm.GetString("str_0225_buscAutos")
                frm.lbl_comentarios.Text = rm.GetString("str_0229_comentarios")
                frm.lbl_revasActuales.Text = rm.GetString("str_0230_revasActuales")
                frm.btn_confirmar.Text = rm.GetString("str_0241_confirmar")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                frm.btn_modificar.Text = rm.GetString("str_0020_modificar")

                'If frm.modificar Then frm.Text = CapaPresentacion.Idiomas.get_str("str_0243_editCall") Else frm.Text = rm.GetString("str_0203_nuevaLlamada")
                frm.Text = rm.GetString("str_0203_nuevaLlamada")
                frm.lblMedioPromocion.Text = rm.GetString("str_441_promo") & ":"
                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Tipo").Header.Caption = rm.GetString("str_0231_tipo")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Fecha").Header.Caption = rm.GetString("str_0146_fecha")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Lugar").Header.Caption = rm.GetString("str_0003_lugar")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Total").Header.Caption = rm.GetString("str_0129_total")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Confirmada").Header.Caption = rm.GetString("str_382_confirmada")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("id").Hidden = True
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0043", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub

        'child - tambien en el LOAD
        Public Shared Sub cambiar_listaLlamadas(ByVal frm As listaLlamadas)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_ini.Text = rm.GetString("str_0244_entre")
                frm.lbl_fin.Text = rm.GetString("str_0245_y")
                frm.lbl_nombre.Text = rm.GetString("str_0168_nombre")
                frm.lbl_duracion.Text = rm.GetString("str_0236_duracion")
                frm.lbl_reservaciones.Text = rm.GetString("str_0018_reservaciones")
                frm.lbl_comentarios.Text = rm.GetString("str_0229_comentarios")
                frm.lblOperador.Text = rm.GetString("str_0200_operador")
                frm.btn_buscar.Text = rm.GetString("str_0187_actualizar")
                frm.btn_limpiar.Text = rm.GetString("str_0199_limpiar")
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                frm.UltraGroupBox1.Text = rm.GetString("str_353_filters")

                frm.Text = rm.GetString("str_0235_listaLlamadas")

                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 0 AndAlso frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("nombreCliente").Header.Caption = rm.GetString("str_0168_nombre")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("inicio2").Header.Caption = rm.GetString("str_0237_inicio")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("duracion2").Header.Caption = rm.GetString("str_0236_duracion")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("reservaciones").Header.Caption = rm.GetString("str_0018_reservaciones")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("comentarios").Header.Caption = rm.GetString("str_0229_comentarios")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ayuda2").Header.Caption = rm.GetString("str_379_tipoAyuda")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("correo").Header.Caption = rm.GetString("str_0148_email")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("sexo2").Header.Caption = rm.GetString("str_380_genero")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("telefono").Header.Caption = rm.GetString("str_0118_telefono")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("operador").Header.Caption = rm.GetString("str_0200_operador")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("sexo").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ayuda").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Calls_Id").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idCall").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idOperador").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("paquete").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("inicio").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("fin").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("duracion").Hidden = True
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0044", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_configurarAgente(ByVal frm As configurarAgente)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_correo.Text = rm.GetString("str_0050_correo")
                frm.lbl_nombreAgencia.Text = rm.GetString("str_0071_nombreAgencia")
                frm.lbl_nombreAgente.Text = rm.GetString("str_0072_nombreAgente")
                frm.UltraGroupBox1.Text = rm.GetString("str_0073_cambiarPass")
                frm.lbl_passActual.Text = rm.GetString("str_0074_passActual")
                frm.lbl_nuevoPass.Text = rm.GetString("str_0075_nuevoPass")
                frm.lbl_repetirPass.Text = rm.GetString("str_0076_repetirPass")
                frm.btn_cambiar.Text = rm.GetString("str_0077_cambiar")
                frm.btn_guardar.Text = rm.GetString("str_0078_guardarCambios")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                frm.UltraTabControl1.Tabs(0).Text = rm.GetString("str_0027_configurarAgente")

                frm.Text = rm.GetString("str_0027_configurarAgente")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0045", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_configurarConexion(ByVal frm As configurarConexion)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.UltraTabControl1.Tabs(0).Text = rm.GetString("str_0079_sw")
                frm.lbl_tiempoEspera.Text = rm.GetString("str_0080_tiempoEspera")
                frm.btn_guardar.Text = rm.GetString("str_0078_guardarCambios")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                frm.lbl_notaSQL.Text = rm.GetString("str_369_nombreDB") + " " + CONST_DB_NAME
                frm.lbl_notaPassport.Text = rm.GetString("str_368_reqReiniciar")

                frm.Text = rm.GetString("str_0028_configurarConexion")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0046", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        ''pop up
        'Public Shared Sub cambiar_detallesReservacion(ByVal frm As _detallesReservacion)
        '    Try
        '        Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
        '        System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

        '        frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")

        '        frm.Text = rm.GetString("str_0082_detallesReservacion")
        '    Catch ex As System.Resources.MissingManifestResourceException
        '        CapaLogicaNegocios.LogManager.agregar_registro("cambiar_detallesReservacion - idiomas", ex.Message)
        '    End Try
        'End Sub

        'child - tambien en el LOAD
        Public Shared Sub cambiar_reservaciones(ByVal frm As Reservaciones)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_llegada.Text = rm.GetString("str_0282_desde") + ":"
                frm.lbl_salida.Text = rm.GetString("str_0283_hasta") + ":"
                frm.lbl_fechaBusqueda.Text = rm.GetString("str_0121_buscarPor") + ":"
                frm.lbl_cliente.Text = rm.GetString("str_0147_nombreCliente") + ":"
                frm.lbl_numConfirm.Text = rm.GetString("str_0149_numConfirmacion") + ":"
                ''frm.lbl_rubro.Text = rm.GetString("str_352_rubro") + ":"
                frm.lbl_compania.Text = rm.GetString("str_0167_empresa") + ":"
                frm.lbl_ciudad.Text = rm.GetString("str_0043_ciudad")
                frm.btn_actualizar.Text = rm.GetString("str_0187_actualizar")
                frm.btn_limpiar.Text = rm.GetString("str_0199_limpiar")
                frm.btn_excelExport.Text = rm.GetString("str_473_excel")
                frm.grp_filtros.Text = rm.GetString("str_353_filters")
                frm.grp_transacciones.Text = rm.GetString("str_492_Transacction")

                frm.lbl_empresa.Text = rm.GetString("str_0167_empresa") + ":"
                frm.lbl_segmento.Text = rm.GetString("str_490") + ":"
                frm.lbl_transaccionespms.Text = rm.GetString("str_491_TransacctionPMS") + ":"

                frm.opt_transacciones.Items(0).DisplayText = rm.GetString("str_354_todos")
                frm.opt_transacciones.Items(1).DisplayText = rm.GetString("str_493_ConTransacction")
                frm.opt_transacciones.Items(2).DisplayText = rm.GetString("str_494_SinTransacction")


                CapaLogicaNegocios.Reservations.llena_combos(frm)
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                frm.Text = rm.GetString("str_0018_reservaciones")

                 menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 0 AndAlso frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 AndAlso frm.UltraGrid1.Rows.Count > 0 Then
                    With frm.UltraGrid1.DisplayLayout.Bands(0)
                        .Columns("Ciudad").Header.Caption = rm.GetString("str_0248_destino")
                        .Columns("Pais").Header.Caption = rm.GetString("str_0120_pais")
                        .Columns("RubroTXT").Header.Caption = rm.GetString("str_352_rubro")
                        .Columns("CheckInTXT").Header.Caption = rm.GetString("str_336_arrDate")
                        .Columns("CheckOutTXT").Header.Caption = rm.GetString("str_337_depDate")
                        .Columns("NoReservacion").Header.Caption = rm.GetString("str_0149_numConfirmacion")
                        .Columns("StatusTXT").Header.Caption = rm.GetString("str_470_status")
                        .Columns("nombre").Header.Caption = rm.GetString("str_0032_hotel")
                        .Columns("PortalName").Header.Caption = rm.GetString("str_356_portal")
                        .Columns("Source").Header.Caption = rm.GetString("str_357_origen")
                        .Columns("Cliente").Header.Caption = rm.GetString("str_0061_cliente")
                        .Columns("EmailCliente").Header.Caption = rm.GetString("str_0148_email")
                        If .Columns.Exists("Noches") Then .Columns("Noches").Header.Caption = rm.GetString("str_0140_noches")
                        If .Columns.Exists("FechaRegistroTXT") Then .Columns("FechaRegistroTXT").Header.Caption = rm.GetString("str_335_resDate")
                        If .Columns.Exists("UsuarioReservo") Then .Columns("UsuarioReservo").Header.Caption = rm.GetString("str_423_User")
                        If .Columns.Exists("Habitaciones") Then .Columns("Habitaciones").Header.Caption = rm.GetString("str_0008_habitaciones")
                        If .Columns.Exists("EmpresaConvenio") Then .Columns("EmpresaConvenio").Header.Caption = rm.GetString("str_0167_empresa")
                        If .Columns.Exists("CiudadCliente") Then .Columns("CiudadCliente").Header.Caption = rm.GetString("str_528_ciudad")
                        If .Columns.Exists("EstadoCliente") Then .Columns("EstadoCliente").Header.Caption = rm.GetString("str_527_estado")
                        If .Columns.Exists("Impuesto") Then .Columns("Impuesto").Header.Caption = rm.GetString("str_464")


                        .Columns("PendientesPMSTXT").Header.Caption = rm.GetString("str_491_TransacctionPMS")
                        .Columns("Segmento").Header.Caption = rm.GetString("str_490")

                        .Columns("PortalId").Hidden = True
                        .Columns("CheckIn").Hidden = True
                        .Columns("idReservacion").Hidden = True
                        .Columns("idRubro").Hidden = True
                        .Columns("status").Hidden = True
                        .Columns("ServiceProvider").Hidden = True
                        .Columns("Pais").Hidden = True
                        .Columns("RubroTXT").Hidden = True
                        If .Columns.Exists("CheckOut") Then .Columns("CheckOut").Hidden = True
                        If .Columns.Exists("FechaRegistro") Then .Columns("FechaRegistro").Hidden = True
                        .Columns("PendientesPMS").Hidden = True

                    End With
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0047", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'uc
        Public Shared Sub cambiar_detallesReservacion(ByVal ctl As _uc_detallesReservacion)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                'ctl.lbl_llegada.Text = rm.GetString("str_0092_llegada")
                'ctl.lbl_salida.Text = rm.GetString("str_0093_salida")
                'ctl.lbl_cadena.Text = rm.GetString("str_0087_cadena")
                'ctl.lbl_adultos.Text = rm.GetString("str_0065_adultos")
                'ctl.lbl_ninos.Text = rm.GetString("str_0113_ni�os")
                'ctl.lbl_numHabitaciones.Text = rm.GetString("str_0094_numHabitaciones")
                'ctl.lbl_codTarifa.Text = rm.GetString("str_0096_codigoTarifa")
                'ctl.GroupBox1.Text = rm.GetString("str_0099_garantiaTarifa")
                'ctl.lbl_tipo.Text = rm.GetString("str_0100_tipo")
                'ctl.GroupBox2.Text = rm.GetString("str_0104_usarComo")
                'ctl.rbt_tarjetaCredito.Text = rm.GetString("str_0052_tarjetaCredito")
                'ctl.rbt_depositoBancario.Text = rm.GetString("str_0107_depositoBanc")
                'ctl.rbt_ninguno.Text = rm.GetString("str_0108_ninguno")
                'ctl.rbt_garantia.Text = rm.GetString("str_0105_garantia")
                'ctl.rbt_deposito.Text = rm.GetString("str_0106_deposito")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0048", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'uc
        Public Shared Sub cambiar_UC_habitaciones(ByVal ctl As uc_habitaciones)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                ctl.lbl_adultos.Text = rm.GetString("str_0065_adultos")
                ctl.lbl_habitaciones.Text = rm.GetString("str_0008_habitaciones") + ":"
                ctl.lbl_ninos.Text = rm.GetString("str_0113_ni�os")
                ctl.btn_especificar.Text = rm.GetString("str_342_especificar")
                'ctl.lbl_ninos.Text = rm.GetString("str_0113_ni�os")
                'ctl.lbl_numHabitaciones.Text = rm.GetString("str_0094_numHabitaciones")
                'ctl.lbl_codTarifa.Text = rm.GetString("str_0096_codigoTarifa")
                'ctl.GroupBox1.Text = rm.GetString("str_0099_garantiaTarifa")
                'ctl.lbl_tipo.Text = rm.GetString("str_0100_tipo")
                'ctl.GroupBox2.Text = rm.GetString("str_0104_usarComo")
                'ctl.rbt_tarjetaCredito.Text = rm.GetString("str_0052_tarjetaCredito")
                'ctl.rbt_depositoBancario.Text = rm.GetString("str_0107_depositoBanc")
                'ctl.rbt_ninguno.Text = rm.GetString("str_0108_ninguno")
                'ctl.rbt_garantia.Text = rm.GetString("str_0105_garantia")
                'ctl.rbt_deposito.Text = rm.GetString("str_0106_deposito")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0049", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_login(ByVal frm As login)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_usr.Text = rm.GetString("str_345_nombreUsuario") + ":"
                frm.lbl_pass.Text = rm.GetString("str_346_password") + ":"
                frm.btn_configurar.Text = rm.GetString("str_347_configurar")
                frm.btn_entrar.Text = rm.GetString("str_348_entrar")
                frm.btn_cerrar.Text = rm.GetString("str_349_cerrar")
                frm.UltraGroupBox1.Text = rm.GetString("str_371_login")
                frm.lbl_conacyt.Text = rm.GetString("str_372_conacyt")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0050", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'child - tambien en el LOAD
        Public Shared Sub cambiar_detalles(ByVal frm As detalles)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_estado.Text = rm.GetString("str_0044_estado")
                frm.lbl_origen.Text = rm.GetString("str_357_origen") + ":"
                frm.lbl_portal.Text = rm.GetString("str_356_portal")
                frm.lbl_reservacion.Text = rm.GetString("str_0018_reservaciones")
                frm.lbl_rubro.Text = rm.GetString("str_352_rubro")
                frm.btn_finalizar.Text = rm.GetString("str_0234_finalizar")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                frm.lbl_moneda.Text = rm.GetString("str_0251_moneda")
                frm.btn_cambiar.Text = rm.GetString("str_0077_cambiar")
                frm.btn_Imprimir.Text = rm.GetString("str_449")
                frm.chkPorDia.Text = rm.GetString("str_451")
                frm.Text = rm.GetString("str_0019_detalles")
                frm.btn_cancelar.Visible = CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled


                frm.btn_ocultar_bitacora.Text = rm.GetString("str_349_cerrar") + " [X]"
                frm.pnl_bitacora.Text = rm.GetString("str_482_logTitle")

                If (frm.btn_mostrar_bitacora.Enabled = True) Then
                    frm.btn_mostrar_bitacora.Text = CapaPresentacion.Idiomas.get_str("str_481_logLabelShow")
                Else
                    frm.btn_mostrar_bitacora.Text = CapaPresentacion.Idiomas.get_str("str_482_logSin")
                End If

                If (frm.btn_solicita_transacciones.Enabled = True) Then
                    frm.btn_solicita_transacciones.Text = CapaPresentacion.Idiomas.get_str("str_487_TransacctionCon")
                Else
                    frm.btn_solicita_transacciones.Text = CapaPresentacion.Idiomas.get_str("str_487_TransacctionSin")
                End If


                If frm.UltraGrid1.DisplayLayout.Bands.Count > 0 AndAlso frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 AndAlso frm.UltraGrid1.Rows.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    With frm.UltraGrid1.DisplayLayout.Bands(0)
                        .Columns("idLog").Hidden = True
                        .Columns("idOperador").Hidden = True
                        .Columns("noReservacion").Hidden = True
                        .Columns("Comments").Hidden = True
                        .Columns("DateOperation").Hidden = True
                        .Columns("Operation").Hidden = True

                        .Columns("Operator").Header.Caption = rm.GetString("str_0200_operador")
                        .Columns("DateOperationTXT").Header.Caption = rm.GetString("str_0146_fecha")
                        .Columns("OperationTXT").Header.Caption = rm.GetString("str_0285_actividad")
                        .Columns("DetailsTXT").Header.Caption = ""
                    End With
                End If


            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0051", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_datosCliente(ByVal frm As datosCliente)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_nombre.Text = rm.GetString("str_0168_nombre") + ":"
                frm.lbl_apellido.Text = rm.GetString("str_0169_apellido") + ":"
                frm.lbl_direccion.Text = rm.GetString("str_0117_direccion") + ":"
                frm.lbl_ciudad.Text = rm.GetString("str_0004_ciudad") + ":"
                frm.lbl_estado.Text = rm.GetString("str_0152_estado") + ":"
                frm.lbl_cp.Text = rm.GetString("str_0045_cp")
                frm.lbl_pais.Text = rm.GetString("str_0120_pais") + ":"
                frm.lbl_telefono.Text = rm.GetString("str_0118_telefono") + ":"
                frm.lbl_telefono_adicional1.Text = rm.GetString("str_0118_telefono_adicional") + ":"
                frm.lbl_email.Text = rm.GetString("str_0148_email") + ":"
                frm.lbl_tipoTarjeta.Text = rm.GetString("str_0053_tipoTarjeta")
                frm.lbl_nombre2.Text = rm.GetString("str_0168_nombre") + ":"
                frm.lbl_numTarjeta.Text = rm.GetString("str_0054_numeroTarjeta")
                frm.lbl_codigoSeguridad.Text = rm.GetString("str_0055_codigoSeguridad")
                frm.lbl_fechaExpiracion.Text = rm.GetString("str_0056_fechaExpira")
                frm.lbl_MM2.Text = rm.GetString("str_0057_mm")
                frm.lbl_AAAA2.Text = rm.GetString("str_0058_aaaa")
                frm.UltraGroupBox1.Text = rm.GetString("str_0052_tarjetaCredito")
                frm.ckb_modificar.Text = rm.GetString("str_0020_modificar")
                frm.btn_reservar.Text = rm.GetString("str_0060_aceptar")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                frm.btn_importar.Text = rm.GetString("str_366_usarDatos")
                frm.uce_tipoTarjeta.Items(0).DisplayText = rm.GetString("str_0062_tipoTarjeta")

                frm.Text = rm.GetString("str_367_datosCliente")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0052", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_ConfigureImage(ByVal frm As ConfigureImage)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_nombre.Text = rm.GetString("str_387_SysName")
                frm.lbl_imagen.Text = rm.GetString("str_388_Select")
                frm.btn_selImagen.Text = rm.GetString("str_389_Image")
                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")

                frm.Text = rm.GetString("str_385_configImage")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0053", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_CallOption(ByVal frm As CallOption)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_WhatToDo.Text = rm.GetString("str_407_WhatToDo")
                frm.btn_BookWithNoCall.Text = rm.GetString("str_407_BookNoCall")
                frm.btn_NewCall.Text = rm.GetString("str_0203_nuevaLlamada")
                frm.btn_cancel.Text = rm.GetString("str_0021_cancelar")

                frm.Text = rm.GetString("str_0203_nuevaLlamada")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0054", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        '-------------------------------HOTELES--------------------------------------
        '''

        'child - tambien en el LOAD
        Public Shared Sub cambiar_busquedaHotel(ByVal frm As busquedaHotel)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_llegada.Text = rm.GetString("str_0092_llegada")
                frm.lbl_salida.Text = rm.GetString("str_0093_salida")
                frm.lbl_noches.Text = rm.GetString("str_0140_noches") + ":"
                frm.ugb_buscarPor.Text = rm.GetString("str_0121_buscarPor")
                frm.rbt_ciudad.Text = rm.GetString("str_0004_ciudad")
                frm.rbt_aeropuerto.Text = rm.GetString("str_0006_aeropuerto")
                frm.ugb_desplegar.Text = rm.GetString("str_343_desplegar")
                frm.rbt_todosHoteles.Text = rm.GetString("str_0186_hotelesTodos")
                frm.rbt_hotelesDisponibles.Text = rm.GetString("str_0185_hotelesDisp")
                frm.lbl_lugar.Text = rm.GetString("str_0003_lugar") + ":"
                If frm.rbt_Hotel.Checked Then frm.lbl_lugar.Text = "Hotel:"
                frm.lbl_codProm.Text = rm.GetString("str_0007_codigoPromocion") + ":"
                frm.lblConvenio.Text = rm.GetString("str_443_conv") + ":"   'str_443_conv
                frm.lbl_moneda.Text = rm.GetString("str_0251_moneda") + ":"
                frm.lbl_distancia.Text = rm.GetString("str_0068_distancia") + ":"
                frm.lbl_millas.Text = rm.GetString("str_344_millasCerca")
                frm.btn_buscar.Text = rm.GetString("str_0001_buscar")
                frm.btn_detener.Text = rm.GetString("str_383_detener")
                frm.lbl_totalHoteles.Text = rm.GetString("str_0067_totalHoteles")
                cambiar_UC_habitaciones(frm.Uc_habitaciones1)
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                frm.btnHotel.Text = rm.GetString("str_450")

                frm.Text = rm.GetString("str_0070_resultadosBusqueda") + ": " + frm.datosBusqueda_hotel.ciudad_nombre
                frm.rbt_tarifas.Text = rm.GetString("str_0036_tarifas")
                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PropertyName").Header.Caption = rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Address").Header.Caption = rm.GetString("str_0117_direccion")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("CityName").Header.Caption = rm.GetString("str_0004_ciudad")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("CountryCode").Header.Caption = rm.GetString("str_0120_pais")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("laCategoria").Header.Caption = rm.GetString("str_0119_categoria")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Tarifas").Header.Caption = rm.GetString("str_0036_tarifas")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Numero").Header.Caption = "#"
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Provider") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Provider").Header.Caption = rm.GetString("str_532")
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Message") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Message").Header.Caption = rm.GetString("str_534")
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Phone") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Phone").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Fax") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Fax").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("StateCode") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("StateCode").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("ShortDescription") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ShortDescription").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("ImageURL") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ImageURL").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Amenities") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Amenities").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AvailPortal") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AvailPortal").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("HasRatesPackage") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("HasRatesPackage").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Longitude") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Longitude").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("ParseUrl") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ParseUrl").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AreasId") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AreasId").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AreasDescription") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AreasDescription").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Distance") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Distance").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Currency") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Currency").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Blocked") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Blocked").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AltCurrency") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltCurrency").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Direction") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Direction").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("laCategoriaNumero").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ChainCode").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ServiceProvider").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Category").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PropertyNumber").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("CityCode").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Emprhotur").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AmhmRes").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Latitude").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("StateName").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Logitude").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("LowRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Decimals").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("LogoURL").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RichContent").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltLowRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltDecimals").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TarifaMenorOrdenar").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TarifaMayorOrdenar").Hidden = True
                End If
                frm.btnPasiva.Text = rm.GetString("str_460")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0055", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_galeria(ByVal frm As galeria)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")

                frm.Text = rm.GetString("str_0034_galeria")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0056", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_habitaciones(ByVal frm As habitaciones)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")

                frm.Text = rm.GetString("str_0008_habitaciones")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0057", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_reglas(ByVal frm As reglas)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.GroupBox1.Text = rm.GetString("str_0083_informacion")
                frm.lbl_fecha.Text = rm.GetString("str_0063_fecha")
                frm.lbl_numNoches.Text = rm.GetString("str_0085_numNoches")
                frm.lbl_habitaciones.Text = rm.GetString("str_0008_habitaciones") + ":"
                frm.lbl_adultos.Text = rm.GetString("str_0065_adultos")
                frm.lbl_ninos.Text = rm.GetString("str_0113_ni�os")
                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")

                frm.Text = rm.GetString("str_0038_reglas")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0058", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_reservar(ByVal frm As reservar)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_llegada.Text = rm.GetString("str_0092_llegada")
                frm.lbl_salida.Text = rm.GetString("str_0093_salida")
                frm.lbl_cadena.Text = rm.GetString("str_0087_cadena")
                cambiar_UC_habitaciones(frm.Uc_habitaciones1)
                frm.lbl_codigoTarifa.Text = rm.GetString("str_0096_codigoTarifa")
                frm.lbl_codProm.Text = rm.GetString("str_0007_codigoPromocion")
                frm.lblConvenio.Text = rm.GetString("str_443_conv") + ":"
                frm.GroupBox1.Text = rm.GetString("str_0099_garantiaTarifa")
                frm.GroupBox2.Text = rm.GetString("str_0104_usarComo")
                frm.rbt_garantia.Text = rm.GetString("str_0105_garantia")
                frm.rbt_deposito.Text = rm.GetString("str_0106_deposito")
                frm.rbt_tarjetaCredito.Text = rm.GetString("str_0052_tarjetaCredito")
                frm.rbt_depositoBancario.Text = rm.GetString("str_0107_depositoBanc")
                frm.rbt_ninguno.Text = rm.GetString("str_0108_ninguno")
                frm.btn_reglas.Text = rm.GetString("str_0038_reglas")
                frm.btn_cambiar.Text = rm.GetString("str_0077_cambiar")
                frm.lbl_peticion.Text = rm.GetString("str_0051_pericion")
                frm.UltraTabControl1.Tabs(0).Text = rm.GetString("str_0110_general")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                'frm.btn_reservar.Text = rm.GetString("str_0232_addReservation")
                If frm.tipoAccion = enumReservar_TipoAccion.reservar Then
                    frm.btn_reservar.Text = rm.GetString("str_0232_addReservation")
                    frm.Text = rm.GetString("str_0037_reservar")
                Else
                    frm.btn_reservar.Text = rm.GetString("str_0158_modiRes")
                    frm.Text = rm.GetString("str_0158_modiRes")
                    If frm.tipoAccion = enumReservar_TipoAccion.modificar Then frm.Text &= " (" & frm.datosBusqueda_hotel.ConfirmNumber & ", " & misForms.newCall.datos_call.reservacion_paquete.cli_nombre & " " & misForms.newCall.datos_call.reservacion_paquete.cli_apellido & ")"
                End If
                frm.lbl_llegada2.Text = rm.GetString("str_0092_llegada")
                frm.lbl_salida2.Text = rm.GetString("str_0093_salida")
                frm.lbl_habitaciones.Text = rm.GetString("str_0008_habitaciones") + ":"
                frm.lbl_adultos.Text = rm.GetString("str_0065_adultos")
                frm.lbl_ninos.Text = rm.GetString("str_0113_ni�os")
                frm.gpb_nuevaBusqueda.Text = rm.GetString("str_0001_buscar")
                frm.btn_actualizar.Text = rm.GetString("str_0187_actualizar")
                frm.rbt_otra.Text = rm.GetString("str_416_Other")

                frm.lblAmount.Text = rm.GetString("str_426_amountinfo") & ":"
                frm.lblComentariodep.Text = rm.GetString("str_326_infoDep") & ":"
                frm.lblDep.Text = rm.GetString("str_427_deadline") & ":"

                frm.lblComentario.Text = rm.GetString("str_0229_comentarios")

                frm.lblEmpresa.Text = rm.GetString("str_434_comp") & ":"
                frm.lblMedio.Text = rm.GetString("str_435_med") & ":"
                frm.lblSegmento.Text = rm.GetString("str_436_seg") & ":"

                frm.lblFrecuent.Text = rm.GetString("str_439_frecuent") & ":"
                frm.chkDeposito.Text = rm.GetString("str_440_reg")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0059", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'child - tambien en el LOAD
        Public Shared Sub cambiar_tarifas(ByVal frm As tarifas)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_fecha.Text = rm.GetString("str_0063_fecha")
                frm.lbl_nombreHotel.Text = rm.GetString("str_0086_nombreHotel")
                frm.lbl_cadena.Text = rm.GetString("str_0087_cadena")
                If frm.taxInc Then frm.lbl_impuestos.Text = rm.GetString("str_0184_impuestosIncluidos") Else frm.lbl_impuestos.Text = rm.GetString("str_0088_impuestosNoIncluidos")
                cambiar_UC_habitaciones(frm.Uc_habitaciones1)
                frm.lbl_tarifasEn.Text = rm.GetString("str_0089_tarifasEn")
                frm.lbl_totalTarifas.Text = rm.GetString("str_0090_totalTarifas")
                frm.lbl_codProm.Text = rm.GetString("str_0007_codigoPromocion")
                frm.lblConvenio.Text = rm.GetString("str_443_conv") + ":"
                frm.btn_actualizar.Text = rm.GetString("str_0187_actualizar")
                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")
                frm.UltraGroupBox1.Text = rm.GetString("str_0001_buscar")
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                frm.Text = rm.GetString("str_0091_tarifasHotel") + ": " + frm.data_lbl_hotel.Text
                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)



                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 0 AndAlso frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PlanCode").Header.Caption = rm.GetString("str_0123_codigoTarifa")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PlanName").Header.Caption = rm.GetString("str_0124_nombreTarifa")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("nombreHabitacion").Header.Caption = rm.GetString("str_0132_nombreHabitacion")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("GuarDep2").Header.Caption = rm.GetString("str_0105_garantia")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalTotal").Header.Caption = rm.GetString("str_360_prom")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalTotalSinDesc").Header.Caption = rm.GetString("str_526_sin_prom")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Currency").Header.Caption = rm.GetString("str_456")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltCurrency").Header.Caption = rm.GetString("str_457")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Numero").Header.Caption = "#"
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AccessCode") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AccessCode").Header.Caption = rm.GetString("str_0007_codigoPromocion")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltAvgRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltTotalStay").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PlanDescription").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AdvBooking").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MinDays").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MaxDays").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RatesVariation").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("OnlyFirstDay").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("GuarDep").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Available").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Message").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("DaysFree").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("DescPromotion").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MaxAdults").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MaxChildren").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalAvgRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalStayDay").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Segment").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("RowType") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RowType").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("PackPrice") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PackPrice").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AccessCode") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AccessCode").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PackTypePrice").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AgencyPercent").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("isUvNetRate") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("isUvNetRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalStay").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AvgRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltPackPrice").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltTotalAvgRate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltTotalStayDay").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalOrdenar").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("StartDate") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("StartDate").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("EndDate") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("EndDate").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Nights") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Nights").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("DinamicPackageQuery") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("DinamicPackageQuery").Hidden = True

                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("Currency") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Currency").Hidden = True
                    If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Exists("AltCurrency") Then frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltCurrency").Hidden = True
                    'ya no se usa por que se oculta esta BAND
                    'If frm.UltraGrid1.DisplayLayout.Bands.Count > 1 AndAlso frm.UltraGrid1.DisplayLayout.Bands(1).Columns.Count > 0 Then
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("Day").Header.Caption = rm.GetString("str_0128_dia")
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("Total").Header.Caption = rm.GetString("str_0129_total")
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("Available").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("Total2").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("TotalExt").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("TotalDay").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("AltTotal").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("AltTotal2").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("AltTotalExt").Hidden = True
                    '    frm.UltraGrid1.DisplayLayout.Bands(1).Columns("AltTotalDay").Hidden = True
                    'End If
                End If

                ''agrega un tooltip con la descripcion de CancelPrior
                'For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In frm.UltraGrid1.Rows
                '    Dim str As String = r.Cells("CancelPrior").Value

                '    If str.Contains("-") Then
                '        Dim arr() As String = str.Split("-")

                '        If arr.Length = 2 Then
                '            Select Case arr(1).ToUpper
                '                Case "D"
                '                    If arr(0) = "0" Then
                '                        str = rm.GetString("str_0159_noCancel")
                '                    Else
                '                        str = rm.GetString("str_0160_cancelXXdias").Replace("@@", arr(0))
                '                    End If
                '                Case "H"
                '                    str = rm.GetString("str_0161_cancelXXhoras").Replace("@@", arr(0))
                '                Case "T"
                '                    If arr(0).Length = 4 Then
                '                        str = rm.GetString("str_0162_cancelXXin").Replace("@@", arr(0).Substring(0, 2) + ":" + arr(0).Substring(2, 2))
                '                    End If
                '            End Select
                '        End If
                '    End If

                '    r.Cells("CancelPrior").ToolTipText = str
                'Next
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0060", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'uc
        Public Shared Sub cambiar_informacionGeneral(ByVal ctl As uc_informacionGeneral)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0061", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_mapa(ByVal frm As mapa)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")

                frm.Text = rm.GetString("str_0177_mapa")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0062", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_frmHabitaciones(ByVal frm As frm_Habitaciones_dinamico)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_msg.Text = rm.GetString("str_316_espHab")
                frm.lbl_adultos.Text = rm.GetString("str_0011_adultos")
                frm.lbl_ninos.Text = rm.GetString("str_0012_ninosHabit")
                frm.chk_habitacion_1.Text = rm.GetString("str_318_habitacion") + " 1"
                frm.chk_habitacion_2.Text = rm.GetString("str_318_habitacion") + " 2"
                frm.chk_habitacion_3.Text = rm.GetString("str_318_habitacion") + " 3"
                frm.lbl_msg2.Text = rm.GetString("str_317_espEdades")
                frm.lbl_nino_1.Text = rm.GetString("str_319_nino") + " 1"
                frm.lbl_nino_2.Text = rm.GetString("str_319_nino") + " 2"
                frm.lbl_nino_3.Text = rm.GetString("str_319_nino") + " 3"
                frm.lbl_habitacion_1.Text = rm.GetString("str_318_habitacion") + " 1"
                frm.lbl_habitacion_2.Text = rm.GetString("str_318_habitacion") + " 2"
                frm.lbl_habitacion_3.Text = rm.GetString("str_318_habitacion") + " 3"
                frm.btn_Aceptar.Text = rm.GetString("str_0060_aceptar")
                frm.btn_Cancelar.Text = rm.GetString("str_0021_cancelar")

                frm.Text = rm.GetString("str_0008_habitaciones")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0063", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        '-------------------------------VUELOS---------------------------------------
        '''

        'child - tambien en el LOAD
        Public Shared Sub cambiar_busquedaVuelo(ByVal frm As busquedaVuelo)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.ugb_partida.Text = rm.GetString("str_0246_partida")
                frm.lbl_ciudadPartida.Text = rm.GetString("str_0043_ciudad")
                frm.lbl_salidaPartida.Text = rm.GetString("str_0246_partida") + ":"
                frm.lbl_horaPartida.Text = rm.GetString("str_0247_hora") + ":"
                frm.ugb_destino.Text = rm.GetString("str_0248_destino")
                frm.lbl_ciudadDestino.Text = rm.GetString("str_0043_ciudad")
                frm.lbl_regrsoDestino.Text = rm.GetString("str_0249_regreso") + ":"
                frm.lbl_horaDestino.Text = rm.GetString("str_0247_hora") + ":"
                frm.lbl_aerolinea.Text = rm.GetString("str_0250_aerolinea") + ":"
                frm.lbl_clase.Text = rm.GetString("str_0253_clase") + ":"
                frm.lbl_moneda.Text = rm.GetString("str_0251_moneda") + ":"
                frm.rbt_redondo.Text = rm.GetString("str_0252_redondo")
                frm.rbt_sencillo.Text = rm.GetString("str_0253_sencillo")
                frm.lbl_adultos.Text = rm.GetString("str_0065_adultos")
                frm.lbl_mayores.Text = rm.GetString("str_0254_mayores") + ":"
                frm.lbl_ninos.Text = rm.GetString("str_0113_ni�os")
                frm.btn_buscar.Text = rm.GetString("str_0001_buscar")
                'frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")
                'frm.llenarCombos()

                frm.Text = rm.GetString("str_0070_resultadosBusqueda") + " de vuelos"   '+ ": " + frm.datosBusqueda_hotel.ciudad_ref

                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0064", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub

        '-----------------------------ACTIVIDADES--------------------------------------
        '''

        'child - tambien en el LOAD
        Public Shared Sub cambiar_busquedaActividad(ByVal frm As busquedaActividad)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_fechaDesde.Text = rm.GetString("str_0282_desde") + ":"
                frm.lbl_fechaHasta.Text = rm.GetString("str_0283_hasta") + ":"
                frm.lbl_ciudad.Text = rm.GetString("str_0004_ciudad") + ":"
                'frm.lbl_adultos.Text = rm.GetString("str_0011_adultos") + ":"
                frm.lbl_moneda.Text = rm.GetString("str_0251_moneda") + ":"
                frm.btn_buscar.Text = rm.GetString("str_0001_buscar")
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                frm.Text = rm.GetString("str_0070_resultadosBusqueda") + ": " + rm.GetString("str_0280_actividades")
                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PropertyName").Header.Caption = rm.GetString("str_0284_empresa")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ActivityName").Header.Caption = rm.GetString("str_0285_actividad")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ActivityDescription").Header.Caption = rm.GetString("str_0286_descripcion")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ActivityLocation").Header.Caption = rm.GetString("str_0003_lugar")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("CategoryName").Header.Caption = rm.GetString("str_0287_categoria")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("EventName").Header.Caption = rm.GetString("str_0288_evento")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltFromPrice").Header.Caption = rm.GetString("str_0289_precioDesde")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Numero").Header.Caption = "#"
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PropertyID").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ActivityID").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("CategoryGroupName").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("EventID").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltCurrency").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalOrdenar").Hidden = True
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0065", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub

        'child - tambien en el LOAD
        Public Shared Sub cambiar_eventosActividades(ByVal frm As eventosActividades)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_FechaDesde.Text = rm.GetString("str_0282_desde") + ":"
                frm.lbl_fechaHasta.Text = rm.GetString("str_0283_hasta") + ":"
                frm.lbl_ciudad.Text = rm.GetString("str_0004_ciudad") + ":"
                'frm.lbl_adultos.Text = rm.GetString("str_0011_adultos") + ":"
                frm.lbl_moneda.Text = rm.GetString("str_0251_moneda") + ":"
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                frm.Text = rm.GetString("str_0290_tarifasActividad")
                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PropertyName").Header.Caption = rm.GetString("str_0284_empresa")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("EventName").Header.Caption = rm.GetString("str_0291_evento")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Duration").Header.Caption = rm.GetString("str_0236_duracion")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateTypeName").Header.Caption = rm.GetString("str_0293_tipoTarifa")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Available").Header.Caption = rm.GetString("str_0294_disponible")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Day").Header.Caption = rm.GetString("str_0128_dia")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("costoTotalFormato").Header.Caption = rm.GetString("str_0295_costo")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Numero").Header.Caption = "#"
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Weekdays").Hidden = True 'Header.Caption = rm.GetString("str_0292_dias")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltCurrency").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TicketType").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("EventTickets").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateID").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("HourlyTickets").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TicketsAvailableHourly").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateTypeID").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MinTickets").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MinTickets").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("MaxTickets").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("DayReservar").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("AltRateTypePrice").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("PercentTax").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("TotalOrdenar").Hidden = True
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0066", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_reglasActividad(ByVal frm As reglasActividad)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.GroupBox1.Text = rm.GetString("str_0083_informacion")
                frm.lbl_fecha.Text = rm.GetString("str_0146_fecha") + ":"
                frm.lbl_ciudad.Text = rm.GetString("str_0004_ciudad") + ":"
                frm.lbl_moneda.Text = rm.GetString("str_0251_moneda") + ":"
                'frm.lbl_adultos.Text = rm.GetString("str_0065_adultos") + ":"
                frm.lbl_actividad.Text = "Evento:"           'rm.GetString("str_0285_actividad") + ":"
                frm.btn_aceptar.Text = rm.GetString("str_0060_aceptar")

                frm.Text = rm.GetString("str_0038_reglas")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0067", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        'pop up
        Public Shared Sub cambiar_reservarActividad(ByVal frm As reservarActividad)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.lbl_entrega_direccion.Text = rm.GetString("str_0296_dirFactura") + ":"
                frm.lbl_entrega_cp.Text = rm.GetString("str_0045_cp")
                frm.lbl_peticion.Text = rm.GetString("str_0051_pericion")
                frm.lbl_cantidad.Text = rm.GetString("str_0266_Cantidad")
                frm.btn_cancelar.Text = rm.GetString("str_0021_cancelar")
                frm.btn_reservar.Text = rm.GetString("str_0232_addReservation")

                If frm.tipoAccion = enumReservar_TipoAccion.reservar Then frm.Text = rm.GetString("str_0037_reservar") Else frm.Text = rm.GetString("str_0158_modiRes")
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0068", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        '--------------------------------AUTOS--------------------------------------
        '''

        'child - tambien en el LOAD
        Public Shared Sub cambiar_busquedaAuto(ByVal frm As busquedaAutos)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                'frm.Text = rm.GetString("str_0070_resultadosBusqueda") + ": " + frm.datosBusqueda_hotel.ciudad_ref

                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("empresa").Header.Caption = "Empresa" 'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("oficina").Header.Caption = "Oficina" 'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("tarifas").Header.Caption = "Tarifas desde"   'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idVendor").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idOffice").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idOfficeEnd").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("tarifasNumero").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("currency").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("LocnCat").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("LocnNum").Hidden = True
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0069", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub

        'child - tambien en el LOAD
        Public Shared Sub cambiar_tarifasAuto(ByVal frm As tarifasAuto)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                'frm.lbl_ciudad.Text = rm.GetString("str_0003_lugar") + ":"
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                'frm.Text = rm.GetString("str_0070_resultadosBusqueda") + ": " + frm.datosBusqueda_hotel.ciudad_ref

                menuContextual.inicializar_items(frm.ContextMenuStrip1, frm.botones)

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("clase").Header.Caption = "Clase"                         'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("tipo").Header.Caption = "Tipo"                               'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ac").Header.Caption = "A/C"                                  'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("descripcion").Header.Caption = "Descripci�n" 'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("tarifas").Header.Caption = "Total"                   'rm.GetString("str_0032_hotel")
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateAmtTotAproxCONV").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("currency").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("GDSType").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("ServiceProvider").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateDBKey").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateCat").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateType").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Rate").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Tax").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("SIPPClassText").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("SIPPTypeText").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateAmtCONV").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("CUR").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idTypeCar").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("RateSource").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("FeeName").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Percent").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("Amount").Hidden = True
                    frm.UltraGrid1.DisplayLayout.Bands(0).Columns("idRate").Hidden = True
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0070", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Finally
            End Try
        End Sub


        Public Shared Sub cambiar_reporteProduccion(ByVal frm As ProductionReport)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.Text = rm.GetString("str_0018_reporte_produccion")

                frm.lbl_llegada.Text = rm.GetString("str_0282_desde") + ":"
                frm.lbl_salida.Text = rm.GetString("str_0283_hasta") + ":"
                frm.lbl_agente.Text = rm.GetString("str_495") + ":"
                frm.lbl_hotel.Text = rm.GetString("str_0032_hotel") + ":"
                frm.lbl_segmento.Text = rm.GetString("str_490") + ":"

                frm.btn_actualizar.Text = rm.GetString("str_0187_actualizar")
                frm.btn_excelExport.Text = rm.GetString("str_473_excel")

                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 0 AndAlso frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    With frm.UltraGrid1.DisplayLayout.Bands(0)
                        .Columns("Hotel").Header.Caption = rm.GetString("str_0032_hotel")
                        .Columns("Agent").Header.Caption = rm.GetString("str_495")
                        .Columns("AgentEmail").Header.Caption = rm.GetString("str_0148_email")
                        .Columns("SegmentCompany").Header.Caption = rm.GetString("str_0167_empresa")
                        .Columns("Segment").Header.Caption = rm.GetString("str_490")
                        .Columns("Reservations").Header.Caption = rm.GetString("str_0018_reservaciones")
                        .Columns("Rooms").Header.Caption = rm.GetString("str_0008_habitaciones")
                        .Columns("Nights").Header.Caption = rm.GetString("str_0140_noches")
                        .Columns("RoomsByNight").Header.Caption = rm.GetString("str_496")
                        .Columns("Persons").Header.Caption = rm.GetString("str_0010_personas")
                        .Columns("Adults").Header.Caption = rm.GetString("str_0011_adultos")
                        .Columns("Childs").Header.Caption = rm.GetString("str_0012_ninosHabit")

                        .Columns("idHotel").Hidden = True
                        If frm.chkGroupByAgent.Checked = False Then
                            .Columns("Agent").Hidden = True
                            .Columns("AgentEmail").Hidden = True
                        End If

                    End With
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0047", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

        Public Shared Sub cambiar_SegmentCompanyList(ByVal frm As SegmentCompanyList)
            Try
                Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)

                frm.Text = rm.GetString("str_434_comp")
                frm.lbl_segmento.Text = rm.GetString("str_490") + ":"
                frm.lbl_nombre.Text = rm.GetString("str_0168_nombre") & ":"
                frm.btn_actualizar.Text = rm.GetString("str_0187_actualizar")
                frm.btn_add.Text = rm.GetString("str_498")
                frm.btn_Delete.Text = rm.GetString("str_499")
                frm.btn_Modify.Text = rm.GetString("str_0020_modificar")
                frm.btn_cancel.Text = rm.GetString("str_0021_cancelar")
                frm.UltraGroupBox1.Text = rm.GetString("str_353_filters")
                frm.UltraGrid1.DisplayLayout.GroupByBox.Prompt = rm.GetString("str_0081_prompt")

                '-------------------------formatea grid
                If frm.UltraGrid1.DisplayLayout.Bands.Count > 0 AndAlso frm.UltraGrid1.DisplayLayout.Bands(0).Columns.Count > 0 Then
                    frm.UltraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    With frm.UltraGrid1.DisplayLayout.Bands(0)
                        .Columns("Code").Header.Caption = rm.GetString("str_497")
                        .Columns("Name").Header.Caption = rm.GetString("str_0168_nombre")
                        .Columns("Segment").Header.Caption = rm.GetString("str_490")

                        .Columns("idCompany").Hidden = True
                        .Columns("idSegment").Hidden = True
                    End With
                End If
            Catch ex As System.Resources.MissingManifestResourceException
                CapaLogicaNegocios.ErrorManager.Manage("E0047", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
        End Sub

    End Class

End Class