Imports callCenter.CapaPresentacion

Partial Public Class CapaLogicaNegocios

    Public Class ErrorManager

        Public Enum ErrorAction
            Log
            Show
            LogAndShow
        End Enum

        Public Enum ErrorSeverity
            CriticalError
            Atention
        End Enum

        Private Shared ReadOnly Property ErrorsFilePath() As String
            Get
                Const FileName As String = "Errors.xml"
                Dim Path As String = CapaAccesoDatos.XML.DataAplicationPath + "\" + FileName
                Dim Path_InFolder As String = CapaAccesoDatos.XML.DataAplicationPath + "\..\..\.." + "\data\" + FileName

                If IO.File.Exists(Path) Then
                    Return Path
                Else
                    If IO.File.Exists(Path_InFolder) Then
                        Return Path_InFolder
                    Else
                        Return Nothing
                    End If
                End If
            End Get
        End Property

        Public Shared Sub Manage(ByVal ErrorNumber As String, ByVal ErrorDetails As String, ByVal Action As ErrorAction, ByVal Severity As ErrorSeverity)
            Dim Message As String = GetString(ErrorNumber)
            If Message = Idiomas.get_str("str_405_ErrorNotDefined") Then Message = ErrorDetails

            Select Case Action
                Case ErrorAction.Log
                    Log(ErrorNumber & " :" & Message, ErrorDetails)

                Case ErrorAction.Show
                    Show(Message, Severity)

                Case ErrorAction.LogAndShow
                    Log(ErrorNumber & " :" & Message, ErrorDetails)
                    If ErrorNumber.Contains("*") Then
                        Show(ErrorDetails, Severity)
                    Else
                        Show(Message, Severity)
                    End If


            End Select
        End Sub

        Public Shared Sub Manage(ByVal ErrorNumber As String, ByVal ErrorDetails As String, ByVal Action As ErrorAction)
            Manage(ErrorNumber, ErrorDetails, Action, ErrorSeverity.CriticalError)
        End Sub

        Private Shared Sub Log(ByVal Message As String, ByVal ErrorDetails As String)
            LogManager.agregar_registro("ERROR", Message + Environment.NewLine + ErrorDetails)
        End Sub

        Private Shared Sub Show(ByVal Message As String, ByVal Severity As ErrorSeverity)
            Dim Title As String = ""
            Dim Icon As MessageBoxIcon

            Select Case Severity
                Case ErrorSeverity.CriticalError
                    Title = Idiomas.get_str("str_0154_error")
                    Icon = MessageBoxIcon.Error
                Case ErrorSeverity.Atention
                    Title = Idiomas.get_str("str_0155_atencion")
                    Icon = MessageBoxIcon.Exclamation
            End Select

            MessageBox.Show(Message, Title, MessageBoxButtons.OK, Icon)
        End Sub

        Private Shared Function GetString(ByVal ErrorNumber As String)
            If ErrorsFilePath Is Nothing Then Return Idiomas.get_str("str_405_ErrorNotDefined")

            Dim ds As DataSet = Common.XMLFileToDS(ErrorsFilePath)
            If ds Is Nothing Then Return Idiomas.get_str("str_405_ErrorNotDefined")
            If ds.Tables.Count = 0 Then Return Idiomas.get_str("str_405_ErrorNotDefined")
            If Not ds.Tables.Contains("Error") Then Return Idiomas.get_str("str_405_ErrorNotDefined")
            If ds.Tables("Error").Rows.Count = 0 Then Return Idiomas.get_str("str_405_ErrorNotDefined")
            If Not ds.Tables("Error").Columns.Contains("ErrorNumber") Then Return Idiomas.get_str("str_405_ErrorNotDefined")

            For Each r As DataRow In ds.Tables("Error").Rows
                If ErrorNumber = r.Item("ErrorNumber") Then
                    Dim message As String = ""
                    Dim LanguageKey As String = ""
                    Dim Description As String = ""
                    Dim LanguajeDescription As String = ""

                    If ds.Tables("Error").Columns.Contains("LanguageKey") Then LanguageKey = r.Item("LanguageKey")
                    If ds.Tables("Error").Columns.Contains("Description") Then Description = r.Item("Description")

                    If LanguageKey <> "" Then LanguajeDescription = Idiomas.get_str(LanguageKey)

                    Return ErrorNumber + ". " + IIf(Description <> "", Description + ". ", "") + LanguajeDescription
                End If
            Next

            Return Idiomas.get_str("str_405_ErrorNotDefined")
        End Function

    End Class

    Public Class LogManager

        Public Shared Sub agregar_registro(ByVal tit As String, ByVal msg As String)
            Try
                Dim fecha As DateTime = DateTime.Now

                Dim ruta As String = Environment.CurrentDirectory + "\log"
                Dim file As String = ruta + "\logERR_" + Str(fecha.Year).Trim + "-" + Str(fecha.Month).Trim + "-" + Str(fecha.Day).Trim + ".txt"

                If Not System.IO.Directory.Exists(ruta) Then System.IO.Directory.CreateDirectory(ruta)

                Dim f As New System.IO.FileStream(file, IO.FileMode.Append)
                Dim arr() As Byte = System.Text.Encoding.UTF8.GetBytes(vbCrLf + vbCrLf + vbCrLf + "-------------------------------------------" + vbCrLf + tit + vbCrLf + fecha.ToString + vbCrLf + "Version: " + My.Application.Info.Version.ToString + vbCrLf + "-------------------------------------------" + vbCrLf + msg)
                f.Write(arr, 0, arr.Length)
                f.Close()
            Catch ex As Exception
            End Try
        End Sub

        Public Shared Sub agregar_XML(ByVal tit As String, ByVal XML As String)
            Try
                Dim fecha As DateTime = DateTime.Now

                Dim ruta As String = Environment.CurrentDirectory + "\log"
                Dim file As String = ruta + "\logXML_" + Str(fecha.Year).Trim + "-" + Str(fecha.Month).Trim + "-" + Str(fecha.Day).Trim + "_" + Str(fecha.Hour).Trim + ".txt"

                If Not System.IO.Directory.Exists(ruta) Then System.IO.Directory.CreateDirectory(ruta)

                Dim f As New System.IO.FileStream(file, IO.FileMode.Append)
                Dim arr() As Byte = System.Text.Encoding.UTF8.GetBytes(vbCrLf + vbCrLf + vbCrLf + vbCrLf + "-------------------------------------------" + vbCrLf + tit + vbCrLf + fecha.ToString + vbCrLf + "Version: " + My.Application.Info.Version.ToString + vbCrLf + "-------------------------------------------" + vbCrLf + XML)
                f.Write(arr, 0, arr.Length)
                f.Close()
            Catch ex As Exception
            End Try
        End Sub

    End Class

End Class