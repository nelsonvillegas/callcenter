
Public Enum enumServices
    ninguno = 0

    Hotel = 1
    Vuelo = 2
    Paquete = 3
    Actividad = 4
    Auto = 5
End Enum

Public Enum enumRubrosPassport
    Portal = 0
    Hotel = 10
    Autos = 11
    Actividades = 27
    Vuelos = 23
    Paquetes = 9
End Enum

Public Enum enumReservar_TipoAccion
    reservar
    modificar
    cambiar
    modificarNoConfirmado
End Enum

Public Enum enumFilterReservationDates
    Checkin
    RegisterDate
    CheckOut
End Enum

Public Enum enumCreditCardType
    Undefined = -1
    DinerClub = 0
    AmericanExpress = 1
    JCB = 2
    CarteBlanche = 3
    Visa = 4
    MasterCard = 5
    AustralianBankCard = 6
    Discover = 7
End Enum

Public Enum enumCreditCardCode
    XX = -1
    DC = 0
    AX = 1
    JC = 2
    CB = 3
    VI = 4
    CA = 5
    AB = 6
    DS = 7
    MC = 50
    IK = 500
End Enum

Public Enum enumDepositType
    None = 0
    CreditCard
    BankDeposit
    Other
    Online
    paypal
    payU
    americanExpress
    conekta
End Enum

Public Enum enumReservationStatus
    Reserved = 1
    Confirmed
    Cancelled
    InProcess
    NoShow
    AwaitingDeposit
End Enum