Partial Public Class CapaLogicaNegocios

    Public Class GetWS

        Private Const CONST_SOURCE As String = "CCT"
        Private Const CONST_SOURCECODE As String = "WI"
        Private Const CONST_CHAINCODE As String = "UV"
        'Private Const CONST_IDPORTAL As String = "2"                '"19"

        'HOTEL--------------------------------------

        Public Shared Function obten_hotel_display(ByVal data As datos_display) As DataSet
            Dim RQ As New Hotel_Display_RQ

            Dim dr_Dis As Hotel_Display_RQ.HotelDisplayRow = RQ.HotelDisplay.NewRow
            dr_Dis.ChainCode = data.ChainCode
            dr_Dis.ConfirmNumber = data.ConfirmNumber
            dr_Dis.PropertyNumber = data.PropertyNumber
            dr_Dis.Source = CONST_SOURCE
            dr_Dis.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            'If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Dis.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            If dataOperador.idCorporativo <> "-1" Then dr_Dis.Corporativo = dataOperador.idCorporativo
            RQ.HotelDisplay.Rows.Add(dr_Dis)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(CapaLogicaNegocios.Common.transformar(Global.callCenter.My.Resources.UVHotelDisplay_RQ, RQ.GetXml)) '"UVHotelDisplay_RQ.xsl"

            Return ds
        End Function

        Public Shared Function obten_hotel_display_resID(ByVal data As datos_display) As DataSet
            Dim RQ As New Hotel_Display_RQ

            Dim dr_Dis As Hotel_Display_RQ.HotelDisplayRow = RQ.HotelDisplay.NewRow
            'dr_Dis.ChainCode = data.ChainCode
            dr_Dis.ConfirmNumber = ""
            'dr_Dis.PropertyNumber = data.PropertyNumber
            dr_Dis.ReservationId = data.ReservationId
            dr_Dis.Source = CONST_SOURCE
            dr_Dis.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dr_Dis.AltCurrency = data.AltCurrency
            'If Not IsNothing(ws_login.getUserAccessInfo) AndAlso Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Dis.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            If dataOperador.idCorporativo <> "-1" Then dr_Dis.Corporativo = dataOperador.idCorporativo
            RQ.HotelDisplay.Rows.Add(dr_Dis)

            Dim ws As New webService_services(username, password)
            Dim s As String = CapaLogicaNegocios.Common.transformar(Global.callCenter.My.Resources.UVHotelDisplay_RQ, RQ.GetXml)
            Dim ds As DataSet = ws.call_WS(s) '"UVHotelDisplay_RQ.xsl"

            Return ds
        End Function

        Public Shared Function obten_hotel_HOI(ByVal data As datos_HOI, ByVal dataBusqueda As dataBusqueda_hotel_test) As DataSet
            Dim RQ As New Hotel_HOI_RQ

            Dim drI As Hotel_HOI_RQ.IndexRow = RQ.Index.NewRow
            RQ.Index.Rows.Add(drI)

            Dim drH As Hotel_HOI_RQ.HotelHeaderRow = RQ.HotelHeader.NewRow
            drH.DistanceRatio = dataBusqueda.distancia
            drH.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            drH.Code = data.Code
            drH.RefPoint = dataBusqueda.RefPoint '''''''''''''''''''''''''''''''''''''''''''''
            drH.Source = CONST_SOURCE
            drH.ChainCode = CONST_CHAINCODE
            drH.SearchBy = dataBusqueda.buscaPor '''''''''''''''''''''''''''''''''''''''''''''
            drH.AltCurrency = data.AltCurrency
            If ws_login.IdAsociacion <> Nothing AndAlso ws_login.IdAsociacion <> "-1" Then
                drH.IdAsociacion = ws_login.IdAsociacion
            Else
                drH.IdAsociacion = "0"
            End If

            If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then drH.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            If dataBusqueda.idArea <> "" Then
                drH.idArea = dataBusqueda.idArea
            Else
                drH.idArea = "0"
            End If
            If data.HotelName <> "" Then drH.HotelName = data.HotelName
            drH.SetParentRow(drI)
            RQ.HotelHeader.Rows.Add(drH)

            Dim drR As Hotel_HOI_RQ.HotelRequestRow = RQ.HotelRequest.NewRow
            drR.StartDate = data.StartDate
            drR.EndDate = data.EndDate
            drR.Nights = data.Nights
            drR.Adults = dataBusqueda.habitaciones_.TotalAdultos ' data.Adults
            drR.Children = dataBusqueda.habitaciones_.TotalNinos ' data.Children
            drR.NumRooms = dataBusqueda.habitaciones_.TotalHabitaciones ' data.NumRooms
            drR.SetParentRow(drI)
            RQ.HotelRequest.Rows.Add(drR)

            Dim dr_Rooms As Hotel_HOI_RQ.RoomsRow = RQ.Rooms.NewRow
            dr_Rooms.SetParentRow(drR)
            RQ.Rooms.Rows.Add(dr_Rooms)


            'Dim arr_1() As String
            'If dataBusqueda.ninosEdades <> Nothing AndAlso dataBusqueda.ninosEdades <> "" Then
            '    arr_1 = dataBusqueda.ninosEdades.Split("|")
            'Else
            '    arr_1 = New String() {}
            'End If
            'Dim arr_2(CInt(dataBusqueda.habitaciones) - 1)() As String
            'For i As Integer = 0 To CInt(dataBusqueda.habitaciones) - 1
            '    Dim arr_temp() As String
            '    If i < arr_1.Length Then
            '        arr_temp = arr_1(i).Split(",")
            '        If arr_temp.Length < CInt(dataBusqueda.ninos) Then
            '            ReDim Preserve arr_temp(CInt(dataBusqueda.ninos) - 1)
            '            For j As Integer = 0 To arr_temp.Length - 1
            '                If arr_temp(j) = Nothing Then arr_temp(j) = "1"
            '            Next
            '        End If
            '    Else
            '        ReDim arr_temp(CInt(dataBusqueda.ninos) - 1)
            '        For j As Integer = 0 To arr_temp.Length - 1
            '            arr_temp(j) = "1"
            '        Next
            '    End If
            '
            '    arr_2(i) = arr_temp
            'Next

            For Each hab As uc_habitaciones.T_Habitacion In dataBusqueda.habitaciones_.ListaHabitaciones
                Dim dr_Room As Hotel_HOI_RQ.RoomRow = RQ.Room.NewRow
                dr_Room.Adults = hab.NumAdultos 'data.arr_Adults(i)
                dr_Room.Children = hab.NumNinos 'arr_2(i).Length 'data.arr_Adults(i)
                dr_Room.SetParentRow(dr_Rooms)
                RQ.Room.Rows.Add(dr_Room)

                'Dim str_edades As String = ""
                'For j As Integer = 0 To CInt(dataBusqueda.ninos) - 1 'arr_2.Length - 1
                '    If str_edades <> "" Then str_edades += ","

                '    If j < CInt(dataBusqueda.ninos) Then
                '        str_edades += arr_2(i)(j)
                '    Else
                '        str_edades += "1"
                '    End If
                'Next

                If hab.EdadesNinosSTR <> "" Then
                    Dim dr_ChildAry As Hotel_HOI_RQ.ChildAryRow = RQ.ChildAry.NewRow
                    dr_ChildAry.Age = hab.EdadesNinosSTR
                    dr_ChildAry.SetParentRow(dr_Room)
                    RQ.ChildAry.Rows.Add(dr_ChildAry)
                End If
            Next

            Dim ws As New webService_services(username, password)
            'Dim s As String = herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml)
            Dim s As String = RQ.GetXml.Replace("<Hotel_HOI_RQ xmlns=""http://tempuri.org/Hotel_HOI_RQ.xsd"">", "<reqHotelIndex>").Replace("</Hotel_HOI_RQ>", "</reqHotelIndex>")
            Dim ds As DataSet = ws.call_WS(s)

            Return copia_hotel_HOI(ds)
        End Function

        Public Shared Function obten_hotel_HOR(ByVal data As datos_HOR, ByVal dataBusqueda As dataBusqueda_hotel_test) As DataSet
            Dim RQ As New Hotel_HOR_RQ

            Dim dr_Rul As Hotel_HOR_RQ.HotelRulesRow = RQ.HotelRules.NewRow
            RQ.HotelRules.Rows.Add(dr_Rul)

            Dim dr_Hea As Hotel_HOR_RQ.HotelHeaderRow = RQ.HotelHeader.NewRow
            dr_Hea.ServiceProvider = data.ServiceProvider
            dr_Hea.CheckinDate = data.CheckinDate
            dr_Hea.CheckoutDate = data.CheckoutDate
            dr_Hea.AccessCode = data.AccessCode                 'If data.AccessCode = "" Then dr_Hea.AccessCode = "0" Else 
            dr_Hea.ReferenceContract = data.ReferenceContract
            dr_Hea.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dr_Hea.RateCode = data.RateCode
            dr_Hea.PropertyNumber = data.PropertyNumber
            dr_Hea.Source = CONST_SOURCE
            dr_Hea.Nights = data.Nights
            dr_Hea.ChainCode = data.ChainCode
            dr_Hea.AltCurrency = data.AltCurrency
            If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Hea.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            dr_Hea.SetParentRow(dr_Rul)
            RQ.HotelHeader.Rows.Add(dr_Hea)

            ''Dim arr_1() As String = dataBusqueda.ninosEdades.Split("|")
            ''Dim arr_2(arr_1.Length - 1)() As String
            ''For i As Integer = 0 To arr_1.Length - 1
            ''    arr_2(i) = arr_1(i).Split(",")
            ''Next
            'Dim arr_1() As String = dataBusqueda.ninosEdades.Split("|")
            'Dim arr_2(CInt(dataBusqueda.habitaciones) - 1)() As String
            'For i As Integer = 0 To CInt(dataBusqueda.habitaciones) - 1
            '    Dim arr_temp() As String
            '    If i < arr_1.Length Then
            '        arr_temp = arr_1(i).Split(",")
            '        If arr_temp.Length < CInt(dataBusqueda.ninos) Then
            '            ReDim Preserve arr_temp(CInt(dataBusqueda.ninos) - 1)
            '            For j As Integer = 0 To arr_temp.Length - 1
            '                If arr_temp(j) = Nothing Then arr_temp(j) = "1"
            '            Next
            '        End If
            '    Else
            '        ReDim arr_temp(CInt(dataBusqueda.ninos) - 1)
            '        For j As Integer = 0 To arr_temp.Length - 1
            '            arr_temp(j) = "1"
            '        Next
            '    End If

            '    arr_2(i) = arr_temp
            'Next

            Dim dr_Roo As Hotel_HOR_RQ.HotelRoomsRow = RQ.HotelRooms.NewRow
            dr_Roo.SetParentRow(dr_Rul)
            RQ.HotelRooms.Rows.Add(dr_Roo)
            ''''''''''''
            For Each hab As uc_habitaciones.T_Habitacion In dataBusqueda.habitaciones_.ListaHabitaciones
                Dim dr_Roo_1 As Hotel_HOR_RQ.RoomRow = RQ.Room.NewRow
                dr_Roo_1.Adults = hab.NumAdultos ' data.Adults(i)
                dr_Roo_1.Children = hab.NumNinos ' data.Children(i)
                dr_Roo_1.SetParentRow(dr_Roo)
                RQ.Room.Rows.Add(dr_Roo_1)

                If hab.EdadesNinosSTR <> "" Then 'For j As Integer = 0 To CInt(dataBusqueda.ninos) - 1 'arr_2.Length - 1
                    Dim dr_ChildAry As Hotel_HOR_RQ.ChildAryRow = RQ.ChildAry.NewRow
                    dr_ChildAry.Age = hab.EdadesNinosSTR 'arr_2(i)(j)
                    dr_ChildAry.SetParentRow(dr_Roo_1)
                    RQ.ChildAry.Rows.Add(dr_ChildAry)
                End If
            Next

            Dim ws As New webService_services(username, password)
            'Dim s As String = herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOR_RQ, RQ.GetXml) '"UVHotelHOR_RQ.xsl"
            Dim s As String = RQ.GetXml.Replace("<Hotel_HOR_RQ>", "<reqHotelRules>").Replace("</Hotel_HOR_RQ>", "</reqHotelRules>")
            Dim ds As DataSet = ws.call_WS(s)

            Return ds
        End Function

        'Public Shared Function obten_hotel_sell(ByVal data As datos_sell) As DataSet
        '    Dim RQ As New Hotel_Sell_RQ

        '    Dim dr_HSe As Hotel_Sell_RQ.HotelSellRow = RQ.HotelSell.NewRow
        '    RQ.HotelSell.Rows.Add(dr_HSe)

        '    Dim dr_Hea As Hotel_Sell_RQ.HotelHeaderRow = RQ.HotelHeader.NewRow
        '    dr_Hea.ServiceProvider = data.ServiceProvider
        '    dr_Hea.Source = CONST_SOURCE
        '    dr_Hea.SourceCode = CONST_SOURCECODE
        '    dr_Hea.Language = idiomas.cultura.ToUpper
        '    'dr_Hea.IdPortal = "" CONST_IDPORTAL
        '    dr_Hea.AccessCode = data.AccessCode
        '    If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Hea.Corporativo = ws_login.getUserAccessInfo.idCorporativo
        '    dr_Hea.SetParentRow(dr_HSe)
        '    RQ.HotelHeader.Rows.Add(dr_Hea)

        '    Dim dr_Res As Hotel_Sell_RQ.ReservationRow = RQ.Reservation.NewRow
        '    dr_Res.CheckInDate = data.CheckinDate
        '    dr_Res.CheckOutDate = data.CheckoutDate
        '    dr_Res.PropertyNumber = data.PropertyNumber
        '    dr_Res.ChainCode = data.ChainCode
        '    dr_Res.CreditCardExpiration = data.CreditCardExpiration
        '    dr_Res.CreditCardHolder = data.CreditCardHolder
        '    dr_Res.CreditCardNumber = data.CreditCardNumber
        '    dr_Res.CreditCardNumberVerify = data.CreditCardNumberVerify
        '    dr_Res.CreditCardType = data.CreditCardType
        '    dr_Res.RateCode = data.RateCode
        '    dr_Res.RAwayAdult = "0"
        '    dr_Res.RAwayChild = "0"
        '    dr_Res.RAwayCrib = "0"
        '    dr_Res.GuarDep = data.GuarDep
        '    dr_Res.SetParentRow(dr_HSe)
        '    RQ.Reservation.Rows.Add(dr_Res)

        '    Dim dr_Cus As Hotel_Sell_RQ.CustomerRow = RQ.Customer.NewRow
        '    dr_Cus.PhoneHome = data.PhoneHome
        '    dr_Cus.Email = data.Email
        '    dr_Cus.Address = data.Address
        '    dr_Cus.LastName = data.LastName
        '    dr_Cus.FirstName = data.FirstName
        '    dr_Cus.Country = data.Country
        '    dr_Cus.Estate = data.Estate
        '    dr_Cus.City = data.City
        '    dr_Cus.SetParentRow(dr_HSe)
        '    RQ.Customer.Rows.Add(dr_Cus)

        '    Dim dr_Roo As Hotel_Sell_RQ.RoomsRow = RQ.Rooms.NewRow
        '    dr_Roo.SetParentRow(dr_HSe)
        '    RQ.Rooms.Rows.Add(dr_Roo)
        '    ''''''''''''
        '    For i As Integer = 0 To CInt(data.hatitaciones) - 1
        '        Dim dr_Roo_1 As Hotel_Sell_RQ.RoomRow = RQ.Room.NewRow
        '        dr_Roo_1.Adults = data.Adults(i)
        '        dr_Roo_1.Children = data.Children(i)
        '        dr_Roo_1.Preferences = data.Preferences
        '        dr_Roo_1.SetParentRow(dr_Roo)
        '        RQ.Room.Rows.Add(dr_Roo_1)
        '    Next

        '    Dim ws As New webService_services(username, password)
        '    Dim ds As DataSet
        '    If data.CreditCardNumber Is Nothing OrElse data.CreditCardNumber = "" Then
        '        ds = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelSELL_RQ_noCC, RQ.GetXml)) '"UVHotelSELL_RQ_noCC.xsl"
        '    Else
        '        ds = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelSELL_RQ, RQ.GetXml)) '"UVHotelSELL_RQ.xsl"
        '    End If

        '    Return ds
        'End Function

        Public Shared Function obten_hotel_modify(ByVal data As datos_modify, ByVal conCC As Boolean) As DataSet
            Dim RQ As New Hotel_Modify_rq

            Dim dr_Mod As Hotel_Modify_rq.HotelModifyRow = RQ.HotelModify.NewRow
            RQ.HotelModify.Rows.Add(dr_Mod)

            Dim dr_Hea As Hotel_Modify_rq.HotelHeaderRow = RQ.HotelHeader.NewRow
            dr_Hea.ServiceProvider = data.ServiceProvider
            dr_Hea.Source = data.Source
            'dr_Hea.SourceCode = "WI"
            dr_Hea.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dr_Hea.AccessCode = data.AccessCode
            dr_Hea.ReferenceContract = data.ReferenceContract
            If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Hea.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            dr_Hea.SetParentRow(dr_Mod)
            RQ.HotelHeader.Rows.Add(dr_Hea)

            Dim dr_res As Hotel_Modify_rq.ReservationRow = RQ.Reservation.NewRow
            dr_res.CheckInDate = data.CheckInDate
            dr_res.CheckOutDate = data.CheckOutDate
            dr_res.PropertyNumber = data.PropertyNumber
            dr_res.ChainCode = data.ChainCode
            dr_res.ConfirmNumber = data.ConfirmNumber
            dr_res.CreditCardExpiration = data.CreditCardExpiration
            dr_res.CreditCardHolder = data.CreditCardHolder
            dr_res.CreditCardNumber = data.CreditCardNumber
            dr_res.CreditCardNumberVerify = data.CreditCardNumberVerify
            dr_res.CreditCardType = data.CreditCardType
            dr_res.CodigoReferenciaCCT = data.CodigoReferenciaCCT
            dr_res.RateCode = data.RateCode
            dr_res.RAwayAdult = "0"
            dr_res.RAwayChild = "0"
            dr_res.RAwayCrib = "0"
            dr_res.GuarDep = data.GuarDep
            If data.OnRequest = "true" Then
                dr_res.RecLoc = "x"
                dr_res.Total = data.Total
                dr_res.Money = data.Currency
            End If

            conCC = False
            dr_res.PaymentInformation = ""
            If data.DepositType = enumDepositType.BankDeposit Then
                If data.DepositAmountCCT = "" Then data.DepositAmountCCT = 0
                If CDec(data.DepositAmountCCT) > 0 Then
                    dr_res.ReserveByBankDepositCCT = "Y"
                    dr_res.DepositAmountCCT = data.DepositAmountCCT
                    dr_res.DepositInfoCCT = data.DepositInfoCCT
                Else
                    dr_res.ReserveByBankDeposit = "Y"
                    dr_res.DepositLimitDate = data.DepositLimitDate
                    dr_res.DepositLimitTime = data.DepositLimitTime
                End If


            ElseIf data.DepositType = enumDepositType.CreditCard Then
                conCC = True
            ElseIf data.DepositType = enumDepositType.Other Then
                dr_res.ReservedBySpecificPayment = "Y"
                dr_res.PaymentInformation = data.PaymentInformation
            Else
                'ninguna
            End If
            dr_res.CommentByVendor = data.CommentByVendor

            If data.Tax <> "" Then dr_res.TaxPercentage = data.Tax

            dr_res.SetParentRow(dr_Mod)
            RQ.Reservation.Rows.Add(dr_res)

            Dim dr_Cus As Hotel_Modify_rq.CustomerRow = RQ.Customer.NewRow
            dr_Cus.PhoneHome = data.PhoneHome
            dr_Cus.Email = data.Email
            dr_Cus.Address = data.Address
            dr_Cus.LastName = data.LastName
            dr_Cus.FirstName = data.FirstName
            dr_Cus.Country = data.Country
            dr_Cus.Estate = data.Estate
            dr_Cus.City = data.City
            dr_Cus.PhoneHome = data.PhoneHome
            dr_Cus.PostalCode = data.PostalCode
            dr_Cus.UserId = dataOperador.user_ID
            dr_Cus.SetParentRow(dr_Mod)
            RQ.Customer.Rows.Add(dr_Cus)

            Dim dr_Roo As Hotel_Modify_rq.RoomsRow = RQ.Rooms.NewRow
            dr_Roo.SetParentRow(dr_Mod)
            RQ.Rooms.Rows.Add(dr_Roo)
            ''''''''''''
            'MessageBox.Show("faltan num de hab")
            For i As Integer = 0 To (data.habitaciones) - 1
                Dim dr_Roo_1 As Hotel_Modify_rq.RoomRow = RQ.Room.NewRow
                'MessageBox.Show("faltan los adultos y ni�os")
                dr_Roo_1.Adults = data.Adults(i)
                dr_Roo_1.Children = data.Children(i)
                dr_Roo_1.Preferences = data.Preferences
                dr_Roo_1.idtraveler = 0
                If data.ChildAges.Length > 0 Then
                    dr_Roo_1.ChildrenAges = data.ChildAges(i)
                End If
                dr_Roo_1.SetParentRow(dr_Roo)
                RQ.Room.Rows.Add(dr_Roo_1)
            Next

            Dim ws As New webService_services(username, password)
            Dim xml_t As String
            If conCC Then
                xml_t = Global.callCenter.My.Resources.UVHotelModify_RQ
            Else
                xml_t = Global.callCenter.My.Resources.UVHotelModify_RQ_noCC
            End If
            Dim ds As DataSet = ws.call_WS(CapaLogicaNegocios.Common.transformar(xml_t, RQ.GetXml))

            Return ds
        End Function

        Public Shared Function obten_hotel_changes(ByVal data As reqHotelModifyNoRestrictions) As Boolean
            Dim ws As New webService_services(username, password)


            Dim ds As DataSet = ws.call_WS(data.GetXml)



            Return True
        End Function

        ''' <summary>
        ''' Si no se envia ChainCode y/o PropertyNumber (AUNQUE SEA COMO CADENA VACIA - que no sea nothing) el ws de hoteles
        ''' regresa nothing y el Cancel no se hace
        ''' </summary>
        Public Shared Function obten_hotel_cancel(ByVal data As datos_cancel, ByVal motivo As String, Optional ByVal withError As Boolean = False) As DataSet
            Dim RQ As New Hotel_Cancel_RQ

            Dim dr_Can As Hotel_Cancel_RQ.HotelCancelRow = RQ.HotelCancel.NewRow
            dr_Can.ChainCode = data.ChainCode
            dr_Can.ConfirmNumber = data.ConfirmNumber
            dr_Can.PropertyNumber = data.PropertyNumber
            dr_Can.Source = CONST_SOURCE
            dr_Can.SourceCode = CONST_SOURCECODE
            dr_Can.ServiceProvider = data.ServiceProvider
            If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Can.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            dr_Can.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dr_Can.CxWithError = IIf(withError, "True", "False")
            dr_Can.CXCause = motivo '"Prueba comentario de cancelacion"
            RQ.HotelCancel.Rows.Add(dr_Can)

            Dim dr_Cus As Hotel_Cancel_RQ.CustomerRow = RQ.Customer.NewRow
            dr_Cus.UserId = dataOperador.user_ID_agent_ID
            RQ.Customer.Rows.Add(dr_Cus)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(CapaLogicaNegocios.Common.transformar(Global.callCenter.My.Resources.UVHotelCancel_RQ, RQ.GetXml)) '"UVHotelCancel_RQ.xsl"

            Return ds
        End Function

        Public Shared Function obten_hotel_HOC(ByVal data As datos_HOC, ByVal dataBusqueda As dataBusqueda_hotel_test) As DataSet
            Dim RQ As New Hotel_HOC_RQ

            Dim dr_Com As Hotel_HOC_RQ.CompleteAvailabilityRow = RQ.CompleteAvailability.NewRow
            RQ.CompleteAvailability.Rows.Add(dr_Com)

            Dim dr_Hea As Hotel_HOC_RQ.HotelHeaderRow = RQ.HotelHeader.NewRow
            dr_Hea.AltCurrency = data.AltCurrency
            dr_Hea.ServiceProvider = data.ServiceProvider
            dr_Hea.CheckinDate = data.CheckinDate
            dr_Hea.CheckoutDate = data.CheckoutDate
            dr_Hea.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dr_Hea.AccessCode = data.AccessCode
            dr_Hea.ReferenceContract = data.ReferenceContract
            dr_Hea.IdPortal = dataOperador.idPortal 'CONST_IDPORTAL
            'If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Hea.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            'If dataOperador.idCorporativo <> "-1" Then
            '    Select Case dataOperador.idCorporativo
            '        Case "1"
            '            'mision
            '            dr_Hea.IdPortal = "102"
            '        Case "4"
            '            'believe
            '            dr_Hea.IdPortal = "104"
            '    End Select
            '    dr_Hea.Corporativo = dataOperador.idCorporativo
            'End If
            'If ws_login.IdAsociacion <> Nothing AndAlso ws_login.IdAsociacion = "1" Then
            '    dr_Hea.IdPortal = "103"
            'End If

            dr_Hea.SetParentRow(dr_Com)
            RQ.HotelHeader.Rows.Add(dr_Hea)

            For Each seg As String In data.Segment
                Dim dr_Fil_1 As Hotel_HOC_RQ.HotelFilterRow = RQ.HotelFilter.NewRow
                dr_Fil_1.Segment = seg
                dr_Fil_1.SetParentRow(dr_Com)
                RQ.HotelFilter.Rows.Add(dr_Fil_1)
            Next

            Dim dr_Reqs As Hotel_HOC_RQ.HotelRequestsRow = RQ.HotelRequests.NewRow
            dr_Reqs.SetParentRow(dr_Com)
            RQ.HotelRequests.Rows.Add(dr_Reqs)
            ''''''''''''
            For Each PropertyNumber As String In data.PropertyNumber.Split(",")
                Dim dr_Req As Hotel_HOC_RQ.HotelRequestRow = RQ.HotelRequest.NewRow
                dr_Req.PropertyNumber = PropertyNumber ' data.PropertyNumber
                dr_Req.SetParentRow(dr_Reqs)
                RQ.HotelRequest.Rows.Add(dr_Req)
            Next

            Dim dr_Roo As Hotel_HOC_RQ.HotelRoomsRow = RQ.HotelRooms.NewRow
            dr_Roo.SetParentRow(dr_Com)
            RQ.HotelRooms.Rows.Add(dr_Roo)
            ''''''''''''
            For Each hab As uc_habitaciones.T_Habitacion In dataBusqueda.habitaciones_.ListaHabitaciones
                Dim dr_Roo_1 As Hotel_HOC_RQ.RoomRow = RQ.Room.NewRow
                dr_Roo_1.Adults = hab.NumAdultos
                dr_Roo_1.Children = hab.NumNinos
                dr_Roo_1.SetParentRow(dr_Roo)
                RQ.Room.Rows.Add(dr_Roo_1)

                If hab.EdadesNinosSTR <> "" Then
                    Dim dr_ChildAry As Hotel_HOC_RQ.ChildAryRow = RQ.ChildAry.NewRow
                    dr_ChildAry.Age = hab.EdadesNinosSTR
                    dr_ChildAry.SetParentRow(dr_Roo_1)
                    RQ.ChildAry.Rows.Add(dr_ChildAry)
                End If
            Next

            Dim ws As New webService_services(username, password)
            'Dim s As String = herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOC_RQ, RQ.GetXml) '"UVHotelHOC_RQ.xsl"
            Dim s As String = RQ.GetXml.Replace("<Hotel_HOC_RQ>", "<reqHotelComplete>").Replace("</Hotel_HOC_RQ>", "</reqHotelComplete>")
            Dim ds As DataSet = ws.call_WS(s)
            If CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.cambiar Then
                Return ds
            Else
                If Not String.IsNullOrEmpty(data.ReferenceContract.Trim()) AndAlso ds.Tables.Contains("RatePlan") Then
                    Dim dt As DataTable = ds.Tables("RatePlan")
                    For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                        If Not dt.Rows(i).IsNull("Segment") AndAlso dt.Rows(i)("Segment") <> "O" Then
                            dt.Rows(i).Delete()
                        End If
                    Next
                    ds.Tables("RatePlan").AcceptChanges()
                End If
                If Not String.IsNullOrEmpty(data.AccessCode) AndAlso ds.Tables.Contains("RatePlan") Then
                    Dim dt As DataTable = ds.Tables("RatePlan")
                    For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                        If Not dt.Rows(i).IsNull("Segment") AndAlso dt.Rows(i)("Segment") <> "C" AndAlso dt.Rows(i)("Segment") <> "N" Then
                            dt.Rows(i).Delete()
                        End If
                    Next
                    ds.Tables("RatePlan").AcceptChanges()
                ElseIf Not String.IsNullOrEmpty(dataBusqueda.RateCode) AndAlso ds.Tables.Contains("RatePlan") Then
                    Dim dt As DataTable = ds.Tables("RatePlan")
                    For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                        If Not dt.Rows(i).IsNull("PlanCode") AndAlso dt.Rows(i)("PlanCode").ToString().Trim() <> dataBusqueda.RateCode Then
                            dt.Rows(i).Delete()
                        End If
                    Next
                    ds.Tables("RatePlan").AcceptChanges()
                End If
                Return ds
            End If
        End Function

        Public Shared Function obten_hotel_HOD(ByVal data As datos_HOD) As DataSet
            Dim RQ As New Hotel_HOD_RQ

            Dim dr_Des As Hotel_HOD_RQ.DescriptionRow = RQ.Description.NewRow
            RQ.Description.Rows.Add(dr_Des)

            Dim dr_Hea As Hotel_HOD_RQ.HotelHeaderRow = RQ.HotelHeader.NewRow
            dr_Hea.ServiceProvider = data.ServiceProvider
            dr_Hea.Language = CapaPresentacion.Idiomas.cultura.ToUpper
            dr_Hea.PropertyNumber = data.PropertyNumber
            If Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1 Then dr_Hea.Corporativo = ws_login.getUserAccessInfo.idCorporativo
            dr_Hea.SetParentRow(dr_Des)
            RQ.HotelHeader.Rows.Add(dr_Hea)

            Dim dr_KWA As Hotel_HOD_RQ.KeywordArrayRow = RQ.KeywordArray.NewRow
            dr_Hea.SetParentRow(dr_Des)
            RQ.KeywordArray.Rows.Add(dr_KWA)
            '
            For Each filter As String In data.HotelFilter
                Dim dr_Key As Hotel_HOD_RQ.KeywordsRow = RQ.Keywords.NewRow
                dr_Key.Keyword = filter
                dr_Hea.SetParentRow(dr_KWA)
                RQ.Keywords.Rows.Add(dr_Key)
            Next

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(CapaLogicaNegocios.Common.transformar(Global.callCenter.My.Resources.UVHotelHOD_RQ, RQ.GetXml)) '"UVHotelHOD_RQ.xsl"

            Return ds
        End Function

        Public Shared Function obten_hotel_reactive(ByVal ConfirmNumber As String, ByVal PropertyNumber As String, ByVal Corporativo As String) As DataSet
            Dim RQ As New Hotel_Reactive_RQ

            Dim dr_Dis As Hotel_Reactive_RQ.ReactiveReservationRow = RQ.ReactiveReservation.NewRow
            dr_Dis.ConfirmNumber = ConfirmNumber
            dr_Dis.PropertyNumber = PropertyNumber
            dr_Dis.Corporativo = "" 'Corporativo
            RQ.ReactiveReservation.Rows.Add(dr_Dis)

            Dim ws As New webService_services(username, password)
            Dim s As String = CapaLogicaNegocios.Common.transformar(Global.callCenter.My.Resources.UVHotelReactive_RQ, RQ.GetXml)
            Dim ds As DataSet = ws.call_WS(s)

            Return ds
        End Function


        Private Shared Function copia_hotel_HOI(ByVal ds_original As DataSet) As DataSet
            If ds_original Is Nothing Then Return Nothing

            Dim ds_nuevo As DataSet
            ds_nuevo = ds_original.Clone

            'If ds_nuevo.Tables.Contains("Property") AndAlso ds_nuevo.Tables("Property").Columns.Contains("CompanyId") Then
            '    ds_nuevo.Tables("Property").Columns("CompanyId").DataType = GetType(Integer)
            'End If

            For Each dt_n As DataTable In ds_original.Tables
                For Each dr_n As DataRow In dt_n.Rows
                    ds_nuevo.Tables(dt_n.TableName).Rows.Add(dr_n.ItemArray)
                Next
            Next

            Return ds_nuevo
        End Function

        Public Shared Function obten_hotel_portals(ByVal PropertyId As String) As DataSet
            
            Dim RqGetPortals As String = "<RqGetPortals><HotelId>" + PropertyId + "</HotelId></RqGetPortals>"

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RqGetPortals)

            Return ds
        End Function

        Public Shared Function obten_hotel_ConfirmReservation(ByVal ConfirmNumber As String, ByVal lang As String, ByVal currency As String, ByVal paymentAuthorization As String, ByVal amount As Double) As DataSet
            'Dim language As String = IIf(lang = "es", "es-MX", "en-US")

            'Dim RqConfirm As String = "<ConfirmReservation><idReservacion>" + ConfirmNumber + "</idReservacion><language>" + language + "</language><Currency>" + currency + "</Currency></ConfirmReservation>"

            'Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(RqConfirm)

            Dim update As New ReqUpdateReservationStatus
            Dim status As ReqUpdateReservationStatus.HotelStatusRow
            'Dim resUpdate As ResUpdateReservationStatus

            status = update.HotelStatus.NewHotelStatusRow
            status.ChainCode = "UV"
            status.ConfirmNumber = ConfirmNumber
            status.Language = lang.ToUpper
            status.PropertyNumber = String.Empty
            status.Source = "POR"
            status.Status = "1"
            status.PaymentAuthorization = paymentAuthorization

            If amount > 0 Then status.Amount = amount

            update.HotelStatus.AddHotelStatusRow(status)


            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(update.GetXml())

            Return ds
        End Function

        Public Shared Function obten_hotel_ConfirmPayPalReservation(ByVal ConfirmNumber As String, ByVal lang As String, ByVal currency As String, ByVal paymentAuthorization As String, ByVal amount As Double) As DataSet
            Dim RQ As New ReqUpdatePayPalStatus

            Dim rStatus As ReqUpdatePayPalStatus.HotelStatusRow = RQ.HotelStatus.NewHotelStatusRow
            rStatus.ChainCode = "UV"
            rStatus.ConfirmNumber = ConfirmNumber
            rStatus.Language = lang.ToUpper
            rStatus.PropertyNumber = String.Empty
            rStatus.Source = "POR"
            rStatus.Status = "1"
            rStatus.PayPalTx = paymentAuthorization

            If amount > 0 Then
                rStatus.Amount = amount
                rStatus.Currency = currency
            End If

            rStatus.PayPalReceiptNo = "CallCenterConfirm"
            RQ.HotelStatus.AddHotelStatusRow(rStatus)


            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml())

            Return ds
        End Function

        'VUELO--------------------------------------

        Public Shared Function obten_vuelo_AirAvail(ByVal datosBusqueda_vuelo As dataBusqueda_vuelo) As DataSet
            Dim RQ As New OTA_AirAvailRQ

            Dim drP As OTA_AirAvailRQ.POSRow = RQ.POS.NewRow
            RQ.POS.Rows.Add(drP)

            Dim drS As OTA_AirAvailRQ.SourceRow = RQ.Source.NewRow
            drS.ISOCurrency = datosBusqueda_vuelo.moneda
            drS.AgentSine = "BSIA1234PM"
            drS.SetParentRow(drP)
            RQ.Source.Rows.Add(drS)

            Dim drR As OTA_AirAvailRQ.RequestorIDRow = RQ.RequestorID.NewRow
            drR.URL = "http://www.//provider1.org"
            drR.Type = "5"
            drR.ID = "12345"
            drR.SetParentRow(drS)
            RQ.RequestorID.Rows.Add(drR)

            Dim drPI As OTA_AirAvailRQ.ProcessingInfoRow = RQ.ProcessingInfo.NewRow
            drPI.AvailabilityIndicator = "true"
            RQ.ProcessingInfo.Rows.Add(drPI)

            ''''----''''

            Dim drO_1 As OTA_AirAvailRQ.OriginDestinationInformationRow = RQ.OriginDestinationInformation.NewRow
            drO_1.CrossDateAllowedIndicator = "False"
            drO_1.WindowAfter = " " '"0000"
            drO_1.WindowBefore = " " '2359
            drO_1.DepartureDateTime = datosBusqueda_vuelo.partidaFecha.ToString("yyyy-MM-dd")
            RQ.OriginDestinationInformation.Rows.Add(drO_1)
            ''''
            Dim drOL_1 As OTA_AirAvailRQ.OriginLocationRow = RQ.OriginLocation.NewRow
            drOL_1.LocationCode = datosBusqueda_vuelo.partidaCiudad
            drOL_1.SetParentRow(drO_1)
            RQ.OriginLocation.Rows.Add(drOL_1)
            ''''
            Dim drOD_1 As OTA_AirAvailRQ.DestinationLocationRow = RQ.DestinationLocation.NewRow
            drOD_1.LocationCode = datosBusqueda_vuelo.destinoCiudad
            drOD_1.SetParentRow(drO_1)
            RQ.DestinationLocation.Rows.Add(drOD_1)

            If datosBusqueda_vuelo.tipoViaje_Redondo Then
                Dim drO_2 As OTA_AirAvailRQ.OriginDestinationInformationRow = RQ.OriginDestinationInformation.NewRow
                drO_2.CrossDateAllowedIndicator = "False"
                drO_2.WindowAfter = " " '"0000"
                drO_2.WindowBefore = " " '2359
                drO_2.DepartureDateTime = datosBusqueda_vuelo.destinoFecha.ToString("yyyy-MM-dd")
                RQ.OriginDestinationInformation.Rows.Add(drO_2)
                ''''
                Dim drOL_2 As OTA_AirAvailRQ.OriginLocationRow = RQ.OriginLocation.NewRow
                drOL_2.LocationCode = datosBusqueda_vuelo.destinoCiudad
                drOL_2.SetParentRow(drO_2)
                RQ.OriginLocation.Rows.Add(drOL_2)
                ''''
                Dim drOD_2 As OTA_AirAvailRQ.DestinationLocationRow = RQ.DestinationLocation.NewRow
                drOD_2.LocationCode = datosBusqueda_vuelo.partidaCiudad
                drOD_2.SetParentRow(drO_2)
                RQ.DestinationLocation.Rows.Add(drOD_2)
            End If

            If Not datosBusqueda_vuelo.aerolinea = "" Then
                Dim drSF As OTA_AirAvailRQ.SpecificFlightInfoRow = RQ.SpecificFlightInfo.NewRow
                RQ.SpecificFlightInfo.Rows.Add(drSF)

                Dim drA As OTA_AirAvailRQ.AirlineRow = RQ.Airline.NewRow
                drA.Code = datosBusqueda_vuelo.aerolinea
                drA.SetParentRow(drSF)
                RQ.Airline.Rows.Add(drA)
            End If

            Dim drTP As OTA_AirAvailRQ.TravelPreferencesRow = RQ.TravelPreferences.NewRow
            RQ.TravelPreferences.Rows.Add(drTP)

            Dim drCP As OTA_AirAvailRQ.CabinPrefRow = RQ.CabinPref.NewRow
            drCP.PreferLevel = "Preferred"
            drCP.Cabin = datosBusqueda_vuelo.clase
            drCP.SetParentRow(drTP)
            RQ.CabinPref.Rows.Add(drCP)

            Dim drTIS As OTA_AirAvailRQ.TravelerInfoSummaryRow = RQ.TravelerInfoSummary.NewRow
            RQ.TravelerInfoSummary.Rows.Add(drTIS)

            Dim drATA As OTA_AirAvailRQ.AirTravelerAvailRow = RQ.AirTravelerAvail.NewRow
            drATA.SetParentRow(drTIS)
            RQ.AirTravelerAvail.Rows.Add(drATA)

            Dim drPTC As OTA_AirAvailRQ.PassengerTypeQuantityRow = RQ.PassengerTypeQuantity.NewRow
            drPTC.Code = "ADT"
            drPTC.Quantity = datosBusqueda_vuelo.adultos
            drPTC.SetParentRow(drATA)
            RQ.PassengerTypeQuantity.Rows.Add(drPTC)

            Dim drSuF As OTA_AirAvailRQ.SummaryFLightRow = RQ.SummaryFLight.NewRow
            'If datosBusqueda_vuelo.tipoViaje_Redondo Then drSuF.Journy = "RT" Else drSuF.Journy = "OW"
            drSuF.Journy = "OW"
            drSuF.FLightType = "0"
            RQ.SummaryFLight.Rows.Add(drSuF)

            Dim drC As OTA_AirAvailRQ.CedadesRow = RQ.Cedades.NewRow
            drC.cod = ""
            RQ.Cedades.Rows.Add(drC)

            Dim drPa As OTA_AirAvailRQ.PasajerosRow = RQ.Pasajeros.NewRow
            drPa.Adutos = datosBusqueda_vuelo.adultos
            drPa.Seniors = datosBusqueda_vuelo.mayores
            drPa.Boy = datosBusqueda_vuelo.ninos
            drPa.Babies = "0"
            If datosBusqueda_vuelo.tipoViaje_Redondo Then drPa.Viaje = "RT" Else drPa.Viaje = "OW"
            RQ.Pasajeros.Rows.Add(drPa)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_vuelo_AirReservationDetails(ByVal datosBusqueda_vuelo As dataBusqueda_vuelo) As DataSet
            Dim RQ As New AirReservationDetails_RQ

            Dim dr_Request As AirReservationDetails_RQ.RequestRow = RQ.Request.NewRow
            dr_Request.id = datosBusqueda_vuelo.ReservationId
            dr_Request.isPackage = False
            RQ.Request.Rows.Add(dr_Request)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_vuelo_AirCancel(ByVal datosBusqueda_vuelo As dataBusqueda_vuelo) As DataSet
            Dim RQ As New OTA_AirCancelRQ

            'Dim drP As OTA_AirCancelRQ.POSRow = RQ.POS.NewRow
            'RQ.POS.Rows.Add(drP)

            'Dim drS As OTA_AirCancelRQ.SourceRow = RQ.Source.NewRow
            'drS.ISOCurrency = datosBusqueda_vuelo.moneda
            'drS.AgentSine = "BSIA1234PM"
            'drS.SetParentRow(drP)
            'RQ.Source.Rows.Add(drS)

            'Dim drR As OTA_AirCancelRQ.RequestorIDRow = RQ.RequestorID.NewRow
            'drR.URL = "http://www.//provider1.org"
            'drR.Type = "5"
            'drR.ID = "12345"
            'drR.SetParentRow(drS)
            'RQ.RequestorID.Rows.Add(drR)

            'Dim drPI As OTA_AirCancelRQ.ProcessingInfoRow = RQ.ProcessingInfo.NewRow
            'drPI.AvailabilityIndicator = "true"
            'RQ.ProcessingInfo.Rows.Add(drPI)

            ''''----''''

            Dim dr_UniqueID As OTA_AirCancelRQ.UniqueIDRow = RQ.UniqueID.NewRow
            dr_UniqueID.Type = "14"
            dr_UniqueID.ID = datosBusqueda_vuelo.RecLoc
            RQ.UniqueID.Rows.Add(dr_UniqueID)

            Dim dr_CompanyName As OTA_AirCancelRQ.CompanyNameRow = RQ.CompanyName.NewRow
            dr_CompanyName.CompanyShortName = "Agent"
            dr_CompanyName.TravelSector = "1"
            dr_CompanyName.SetParentRow(dr_UniqueID)
            RQ.CompanyName.Rows.Add(dr_CompanyName)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        'ACTIVIDAD--------------------------------------

        Public Shared Function obten_actividad_IDX2(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
            Dim RQ As New Activities_IDX2_RQ

            Dim dr_ActivityHeader As Activities_IDX2_RQ.ActivityHeaderRow = RQ.ActivityHeader.NewRow
            If CapaPresentacion.Idiomas.cultura = "es" Then dr_ActivityHeader.Language = "1"
            If CapaPresentacion.Idiomas.cultura = "en" Then dr_ActivityHeader.Language = "2"
            dr_ActivityHeader.Iata = datosBusqueda_actividad.ciudadActividad
            dr_ActivityHeader.RefPoint = datosBusqueda_actividad.ciudadActividadNombre
            dr_ActivityHeader.StartDate = datosBusqueda_actividad.fechaDesde.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_ActivityHeader.EndDate = datosBusqueda_actividad.fechaHasta.ToString(CapaPresentacion.Common.DateDataFormat)
            'dr_ActivityHeader.Segment = "RACK"
            dr_ActivityHeader.AltCurrency = datosBusqueda_actividad.monedaBusqueda
            dr_ActivityHeader.AccessCode = datosBusqueda_actividad.codigoAcceso
            RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_actividad_AVL2(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
            Dim RQ As New Activities_AVL2_RQ

            Dim dr_ActivityHeader As Activities_AVL2_RQ.ActivityHeaderRow = RQ.ActivityHeader.NewRow
            If CapaPresentacion.Idiomas.cultura = "es" Then dr_ActivityHeader.Language = "1"
            If CapaPresentacion.Idiomas.cultura = "en" Then dr_ActivityHeader.Language = "2"
            dr_ActivityHeader.Iata = datosBusqueda_actividad.ciudadActividad
            dr_ActivityHeader.RefPoint = datosBusqueda_actividad.ciudadActividadNombre
            dr_ActivityHeader.StartDate = datosBusqueda_actividad.fechaDesde.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_ActivityHeader.EndDate = datosBusqueda_actividad.fechaHasta.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_ActivityHeader.Segment = "RACK"
            dr_ActivityHeader.EventID = datosBusqueda_actividad.EventID
            dr_ActivityHeader.AltCurrency = datosBusqueda_actividad.monedaBusqueda
            dr_ActivityHeader.PromotionCode = datosBusqueda_actividad.codigoPromocion
            RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_actividad_POE(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
            Dim RQ As New Activities_POE2_RQ

            'Dim dr_ActivityHeader As Activities_IDX2_RQ.ActivityHeaderRow = RQ.ActivityHeader.NewRow
            'If idiomas.cultura = "es" Then dr_ActivityHeader.Language = 1
            'If idiomas.cultura = "en" Then dr_ActivityHeader.Language = 0
            'dr_ActivityHeader.Iata = datosBusqueda_actividad.ciudadActividad
            'dr_ActivityHeader.RefPoint = "la paz" 'datosBusqueda_actividad.ciudadActividad
            'dr_ActivityHeader.StartDate = datosBusqueda_actividad.fechaDesde.ToString(CapaPresentacion.Common.DateDataFormat)
            'dr_ActivityHeader.EndDate = datosBusqueda_actividad.fechaHasta.ToString(CapaPresentacion.Common.DateDataFormat)
            'dr_ActivityHeader.Segment = "RACK"
            'RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)

            Dim dr_ActivityHeader As Activities_POE2_RQ.ActivityHeaderRow = RQ.ActivityHeader.NewRow
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_ActivityHeader.Language = "1"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_ActivityHeader.Language = "2"
            'dr_ActivityHeader.SetParentRow(dr_ActivityHeader)
            RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)

            Dim dr_Events As Activities_POE2_RQ.EventsRow = RQ.Events.NewRow
            dr_Events.EventID = datosBusqueda_actividad.EventID
            dr_Events.SetParentRow(dr_ActivityHeader)
            RQ.Events.Rows.Add(dr_Events)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_actividad_ITR2(ByVal datosBusqueda_paquete As dataBusqueda_paquete) As DataSet
            Dim RQ As New Activities_ITR2_RQ

            Dim dr_ActivityHeader As Activities_ITR2_RQ.ActivityHeaderRow = RQ.ActivityHeader.NewRow
            dr_ActivityHeader.PackageItinerary = datosBusqueda_paquete.itinerario
            RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_actividad_DSR2(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
            Dim RQ As New Activities_DSR2_RQ

            Dim dr_ActivityHeader As Activities_DSR2_RQ.ActivityHeaderRow = RQ.ActivityHeader.NewRow
            dr_ActivityHeader.ReservationID = datosBusqueda_actividad.ReservationID
            If CapaPresentacion.Idiomas.cultura = "es" Then dr_ActivityHeader.language = "1"
            If CapaPresentacion.Idiomas.cultura = "en" Then dr_ActivityHeader.language = "2"
            RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_actividad_Cancel(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test) As DataSet
            Dim RQ As New Activities_Cancel_RQ

            Dim dr_Reservation As Activities_Cancel_RQ.ReservationRow = RQ.Reservation.NewRow
            dr_Reservation.ReservationNumber = datosBusqueda_actividad.ConfirmNumber
            RQ.Reservation.Rows.Add(dr_Reservation)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        'AUTO--------------------------------------

        Public Shared Function obten_auto_CarAvail(ByVal datosBusqueda_auto As dataBusqueda_auto_test) As DataSet
            Dim RQ As New CarAvailRQ

            Dim dr_Request As CarAvailRQ.RequestRow = RQ.Request.NewRow
            dr_Request.CidudadO = datosBusqueda_auto.ciudadRecoger
            dr_Request.CiudadD = datosBusqueda_auto.ciudadDevolver
            dr_Request.Chekin = datosBusqueda_auto.fechaRecoger.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_Request.Chekout = datosBusqueda_auto.fechaDevolver.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_Request.ChekinHour = datosBusqueda_auto.horaRecoger
            dr_Request.CheoutHour = datosBusqueda_auto.horaDevolver
            dr_Request.Aeropuerto = "0" 'datosBusqueda_auto.OfficeStartxxx
            dr_Request.Clase = datosBusqueda_auto.clase
            dr_Request.Tipo = datosBusqueda_auto.tipo
            dr_Request.Compania = datosBusqueda_auto.compania
            dr_Request.IdChekin = "-1"
            dr_Request.IdChekout = "-1"
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_Request.idLanguage = "1"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_Request.idLanguage = "2"
            dr_Request.DBKey = datosBusqueda_auto.DBKey
            dr_Request.IndexCar = datosBusqueda_auto.indexCar
            dr_Request.RefPoint = herramientas.SinAcentos(datosBusqueda_auto.RefPoint)
            dr_Request.CurrencyAlt = datosBusqueda_auto.monedaBusqueda
            dr_Request.Todos = True
            RQ.Request.Rows.Add(dr_Request)

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_auto_CarRules(ByVal datosBusqueda_auto As dataBusqueda_auto_test) As DataSet
            Dim RQ As New CarRulesRQ

            Dim dr_Request As CarRulesRQ.RequestRow = RQ.Request.NewRow
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_Request.idLanguage = "1"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_Request.idLanguage = "2"
            dr_Request.idTypeCar = datosBusqueda_auto.idTypeCar
            dr_Request.IdOffice = datosBusqueda_auto.idOffice
            dr_Request.IdOfficeEnd = datosBusqueda_auto.idOfficeEnd
            If datosBusqueda_auto.RateDBKey <> "" Then dr_Request.IdRate = datosBusqueda_auto.RateDBKey Else dr_Request.IdRate = datosBusqueda_auto.idRate
            dr_Request.GalCity = datosBusqueda_auto.ciudadRecoger
            dr_Request.CheckInDate = datosBusqueda_auto.fechaRecoger.ToString(CapaPresentacion.Common.DateDataFormat) + " " + datosBusqueda_auto.horaRecoger
            dr_Request.CheckOutDate = datosBusqueda_auto.fechaDevolver.ToString(CapaPresentacion.Common.DateDataFormat) + " " + datosBusqueda_auto.horaDevolver
            dr_Request.CheckInTime = "0000"
            dr_Request.LocnCat = datosBusqueda_auto.LocnCat
            dr_Request.LocnNum = datosBusqueda_auto.LocnNum
            dr_Request.idVendor = datosBusqueda_auto.idVendor
            dr_Request.GalCityDropOff = datosBusqueda_auto.ciudadDevolver
            dr_Request._CarType_TypeGWS = datosBusqueda_auto.GDSType
            dr_Request.GalRateType = datosBusqueda_auto.RateType
            dr_Request.GalRateCat = datosBusqueda_auto.RateCat
            dr_Request.GalRate = datosBusqueda_auto.Rate
            If datosBusqueda_auto.RateDBKey <> "" Then dr_Request.GalRateDBKey = datosBusqueda_auto.RateDBKey Else dr_Request.GalRateDBKey = datosBusqueda_auto.idRate
            dr_Request.ServiceProvider = datosBusqueda_auto.ServiceProvider
            dr_Request.AltCurrency = datosBusqueda_auto.monedaBusqueda
            RQ.Request.Rows.Add(dr_Request)

            'se paso en iddate el ratedbKey pero en UV(aga) el ratedbKey enrealidad es idRate
            'poner de forma elegante que se pasen los 2 antes de invocar este metodo y un if
            'en este metodo que determine si a dr_Request.IdRate le asigna datosBusqueda_auto.RateDBKey
            'o le asigna datosBusqueda_auto.idRate

            'pedirle al polo un esquema de rules de auto de uv(AGA) y de GALILEO

            'para el caso de autos de uv (aga), pasar LocnCat y LocnNum 
            'tambien pasar un formato de fechas correctar

            Dim ws As New webService_services(username, password)
            'Dim ds As DataSet = ws.call_WS(herramientas.transformar(Global.callCenter.My.Resources.UVHotelHOI_RQ, RQ.GetXml))
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_auto_CarReservation(ByVal datosBusqueda_auto As dataBusqueda_auto_test) As DataSet
            Dim RQ As New CarReservationRQ

            Dim dr_Request As CarReservationRQ.RequestRow = RQ.Request.NewRow
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_Request.idLanguage = "1"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_Request.idLanguage = "2"
            dr_Request.IdReservation = datosBusqueda_auto.IdReservation
            dr_Request.Email_cl = ""
            dr_Request.Currency = datosBusqueda_auto.CUR
            RQ.Request.Rows.Add(dr_Request)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_auto_CarCancel(ByVal datosBusqueda_auto As dataBusqueda_auto_test) As DataSet
            Dim RQ As New CarCancelRQ

            Dim dr_Request As CarCancelRQ.RequestRow = RQ.Request.NewRow
            dr_Request.IdReservation = datosBusqueda_auto.IdReservation
            dr_Request.idUser = "0"
            dr_Request.CarVendor = ""
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_Request.Idioma = "1"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_Request.Idioma = "2"
            dr_Request.ServiceProvider = "0"
            RQ.Request.Rows.Add(dr_Request)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        'PAQUETES--------------------------------------

        Public Shared Function obten_paquete_Book(ByVal datosBusqueda_paquete As dataBusqueda_paquete, Optional IgnorarDuplicada As Boolean = False) As DataSet
            Dim RQ As New Package_BookingRq

            Dim dr_PackageRequest As Package_BookingRq.PackageRequestRow = RQ.PackageRequest.NewRow
            RQ.PackageRequest.Rows.Add(dr_PackageRequest)

            Dim dr_DateRange As Package_BookingRq.DateRangeRow = RQ.DateRange.NewRow
            If datosBusqueda_paquete.lista_Hoteles.Count > 0 Then
                dr_DateRange.Start = datosBusqueda_paquete.lista_Hoteles(0).llegada.ToString(CapaPresentacion.Common.DateDataFormat)
                dr_DateRange._End = datosBusqueda_paquete.lista_Hoteles(0).salida.ToString(CapaPresentacion.Common.DateDataFormat)
            Else
                'If datosBusqueda_paquete.listaVuelos.Count > 0 Then 

                If datosBusqueda_paquete.lista_Actividades.Count > 0 Then
                    dr_DateRange.Start = datosBusqueda_paquete.lista_Actividades(0).fechaDesde.ToString(CapaPresentacion.Common.DateDataFormat)
                    dr_DateRange._End = datosBusqueda_paquete.lista_Actividades(0).fechaHasta.ToString(CapaPresentacion.Common.DateDataFormat)
                Else
                    If datosBusqueda_paquete.lista_Autos.Count > 0 Then
                        dr_DateRange.Start = datosBusqueda_paquete.lista_Autos(0).fechaRecoger.ToString(CapaPresentacion.Common.DateDataFormat)
                        dr_DateRange._End = datosBusqueda_paquete.lista_Autos(0).fechaDevolver.ToString(CapaPresentacion.Common.DateDataFormat)
                    Else

                    End If
                End If
            End If
            dr_DateRange.SetParentRow(dr_PackageRequest)
            RQ.DateRange.Rows.Add(dr_DateRange)

            Dim dr_Parametros As Package_BookingRq.ParametrosRow = RQ.Parametros.NewRow

            If datosBusqueda_paquete.cc_fechaExpira.Length = 6 Then dr_Parametros.CreditCardExpiration_mes = datosBusqueda_paquete.cc_fechaExpira.Substring(0, 2)
            If datosBusqueda_paquete.cc_fechaExpira.Length = 6 Then dr_Parametros.CreditCardExpiration_ano = datosBusqueda_paquete.cc_fechaExpira.Substring(2, 4)
            dr_Parametros.CreditCardHolder_firstName = datosBusqueda_paquete.cc_nombre_
            dr_Parametros.CreditCardHolder_lastName = datosBusqueda_paquete.cc_apellido
            dr_Parametros.cliente_apellidos = datosBusqueda_paquete.cli_apellido
            dr_Parametros.cliente_ciudad = datosBusqueda_paquete.cli_ciudad
            dr_Parametros.cliente_cp = datosBusqueda_paquete.cli_cp
            dr_Parametros.cliente_direccion = datosBusqueda_paquete.cli_direccion
            dr_Parametros.cliente_email = datosBusqueda_paquete.cli_email
            dr_Parametros.cliente_estado = datosBusqueda_paquete.cli_estado
            dr_Parametros.cliente_nombre = datosBusqueda_paquete.cli_nombre
            dr_Parametros.cliente_pais = datosBusqueda_paquete.cli_pais
            dr_Parametros.cliente_telefono = datosBusqueda_paquete.cli_telefono_AreaCityCode + "+" + datosBusqueda_paquete.cli_telefono_PhoneNumber
            dr_Parametros.cliente_telefono_adicional = datosBusqueda_paquete.cli_telefono_adicional_AreaCityCode + "+" + datosBusqueda_paquete.cli_telefono_adicional_PhoneNumber

            dr_Parametros.idAgent = dataOperador.agency_ID
            dr_Parametros.SourceCode = CONST_SOURCECODE
            'datos especificos de autos
            If datosBusqueda_paquete.lista_Autos.Count > 0 Then
                dr_Parametros.RQ_Avail_Start = datosBusqueda_paquete.lista_Autos(0).fechaRecoger.ToString("yyyy-MM-dd")
                dr_Parametros.RQ_Avail_End = datosBusqueda_paquete.lista_Autos(0).fechaDevolver.ToString("yyyy-MM-dd")
                dr_Parametros.RQ_Avail_LocationCode = datosBusqueda_paquete.lista_Autos(0).ciudadRecoger
            End If
            'datos especificos de vuelos
            If datosBusqueda_paquete.lista_Vuelos.Count > 0 Then
                dr_Parametros.ListadoEdades = ""
                dr_Parametros.CreditCardHolder_firstName = datosBusqueda_paquete.cc_nombre_
                dr_Parametros.CreditCardHolder_lastName = datosBusqueda_paquete.cc_apellido
            End If

            If IgnorarDuplicada Then dr_Parametros.SellWithDuplicate = "true"

            dr_Parametros.SetParentRow(dr_PackageRequest)
            RQ.Parametros.Rows.Add(dr_Parametros)

            Dim dr_CreditCard As Package_BookingRq.CreditCardRow = RQ.CreditCard.NewRow
            dr_CreditCard.CreditCardExpiration = datosBusqueda_paquete.cc_fechaExpira
            dr_CreditCard.CreditCardHolder = datosBusqueda_paquete.cc_nombre_ + " " + datosBusqueda_paquete.cc_apellido
            dr_CreditCard.CreditCardNumber = datosBusqueda_paquete.cc_numero
            dr_CreditCard.CreditCardNumberVerify = datosBusqueda_paquete.cc_numSeguridad
            dr_CreditCard.CreditCardType = datosBusqueda_paquete.cc_tipo
            dr_CreditCard.SetParentRow(dr_PackageRequest)
            RQ.CreditCard.Rows.Add(dr_CreditCard)

            Dim dr_PackageHeader As Package_BookingRq.PackageHeaderRow = RQ.PackageHeader.NewRow
            dr_PackageHeader.Source = CONST_SOURCE
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_PackageHeader.Language = "es-MX"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_PackageHeader.Language = "en-US"
            If dataOperador.idCorporativo = "-1" AndAlso Not String.IsNullOrEmpty(datosBusqueda_paquete.Canal) Then
                dr_PackageHeader.IdPortal = datosBusqueda_paquete.Canal
            Else
                dr_PackageHeader.IdPortal = dataOperador.idPortal 'CONST_IDPORTAL          ' no debe ser dr_PackageHeader.IdPortal="2" ?
            End If
            'Select Case dataOperador.idCorporativo
            '    Case "1"
            '        'mision
            '        dr_PackageHeader.IdPortal = "102"
            '    Case "4"
            '        'believe
            '        dr_PackageHeader.IdPortal = "104"
            'End Select
            'If ws_login.IdAsociacion <> Nothing AndAlso ws_login.IdAsociacion = "1" Then
            '    dr_PackageHeader.IdPortal = "103"
            'End If
            dr_PackageHeader.IdAgency = dataOperador.agency_ID
            dr_PackageHeader.SessionID = "" 'ws_login.get_p_ticket
            dr_PackageHeader.Type = ""
            If datosBusqueda_paquete.GetHotelNoConfirm IsNot Nothing Then dr_PackageHeader.Type += datosBusqueda_paquete.GetHeaderNumHotelNoConfirm '"H"
            If datosBusqueda_paquete.GetVueloNoConfirm IsNot Nothing Then dr_PackageHeader.Type += "F"
            If datosBusqueda_paquete.TieneActividadesSinConfirmar Then dr_PackageHeader.Type += "A"
            If datosBusqueda_paquete.GetAutoNoConfirm IsNot Nothing Then dr_PackageHeader.Type += "C"
            dr_PackageHeader.SetParentRow(dr_PackageRequest)
            RQ.PackageHeader.Rows.Add(dr_PackageHeader)

            Dim dr_Customer As Package_BookingRq.CustomerRow = RQ.Customer.NewRow

            Dim travelers As ResTraveler.TravelerDataTable = ws_login.getUserTravelers
            If travelers Is Nothing Then
                dr_Customer.PhoneHome = datosBusqueda_paquete.cli_telefono_AreaCityCode + "+" + datosBusqueda_paquete.cli_telefono_PhoneNumber
                dr_Customer.PhoneWork = datosBusqueda_paquete.cli_telefono_adicional_AreaCityCode + "+" + datosBusqueda_paquete.cli_telefono_adicional_PhoneNumber
                dr_Customer.Email = datosBusqueda_paquete.cli_email
                dr_Customer.PostalCode = datosBusqueda_paquete.cli_cp
                dr_Customer.Address = datosBusqueda_paquete.cli_direccion
                dr_Customer.LastName = datosBusqueda_paquete.cli_apellido
                dr_Customer.FirstName = datosBusqueda_paquete.cli_nombre
                dr_Customer.UserId = dataOperador.user_ID
            Else
                For Each persona As ResTraveler.TravelerRow In travelers.Rows
                    If Not persona.IsMainTraveler Then Continue For

                    dr_Customer.PhoneHome = persona.Phone 'datosBusqueda_paquete.telefono_AreaCityCode + "+" + datosBusqueda_paquete.telefono_PhoneNumber
                    dr_Customer.PhoneWork = persona.WorkPhone   'datosBusqueda_paquete.telefono_AreaCityCode + "+" + datosBusqueda_paquete.telefono_PhoneNumber
                    dr_Customer.Email = ws_login.getUserAccessInfo.Email 'datosBusqueda_paquete.email
                    dr_Customer.PostalCode = persona.PostalCode 'datosBusqueda_paquete.cp
                    dr_Customer.Address = persona.Address 'datosBusqueda_paquete.direccion
                    dr_Customer.LastName = persona.LastName 'datosBusqueda_paquete.apellido
                    dr_Customer.FirstName = persona.FirstName 'datosBusqueda_paquete.nombre
                    dr_Customer.UserId = dataOperador.user_ID_agent_ID '"0"  ' o debe ser 8388?

                    Exit For
                Next
            End If
            dr_Customer.SetParentRow(dr_PackageRequest)
            RQ.Customer.Rows.Add(dr_Customer)

            RQ = rq_hotel(RQ, dr_PackageRequest, datosBusqueda_paquete)
            RQ = rq_auto(RQ, dr_PackageRequest, datosBusqueda_paquete)
            RQ = rq_actividad(RQ, dr_PackageRequest, datosBusqueda_paquete)
            RQ = rq_vuelo(RQ, dr_PackageRequest, datosBusqueda_paquete)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)


            Return ds
        End Function

        Private Shared Function rq_hotel(ByVal RQ As Package_BookingRq, ByVal dr_PackageRequest As Package_BookingRq.PackageRequestRow, ByVal datosBusqueda_paquete As dataBusqueda_paquete) As Package_BookingRq

            For Each datosBusqueda_hotel As dataBusqueda_hotel_test In datosBusqueda_paquete.lista_Hoteles
                If Not datosBusqueda_hotel.confirmada Then
                    Dim dr_Hotel_Sell_Rq As Package_BookingRq.Hotel_Sell_RqRow = RQ.Hotel_Sell_Rq.NewRow
                    dr_Hotel_Sell_Rq.SetParentRow(dr_PackageRequest)
                    RQ.Hotel_Sell_Rq.Rows.Add(dr_Hotel_Sell_Rq)

                    Dim dr_Reservation As Package_BookingRq.ReservationRow = RQ.Reservation.NewRow
                    dr_Reservation.CheckInDate = datosBusqueda_hotel.llegada.ToString(CapaPresentacion.Common.DateDataFormat)
                    dr_Reservation.CheckOutDate = datosBusqueda_hotel.salida.ToString(CapaPresentacion.Common.DateDataFormat)
                    dr_Reservation.PropertyNumber = datosBusqueda_hotel.PropertyNumber
                    dr_Reservation.ChainCode = datosBusqueda_hotel.ChainCode
                    dr_Reservation.RateCode = datosBusqueda_hotel.RateCode
                    dr_Reservation.RAwayAdult = "0"
                    dr_Reservation.RAwayChild = "0"
                    dr_Reservation.RAwayCrib = "0"
                    dr_Reservation.Provider = datosBusqueda_hotel.ServiceProvider
                    dr_Reservation.GuarDep = datosBusqueda_hotel.GuarDep
                    dr_Reservation.Nights = datosBusqueda_hotel.noches
                    dr_Reservation.Money = datosBusqueda_hotel.AltCurrency
                    dr_Reservation.Total = datosBusqueda_hotel.AltTotalStay
                    dr_Reservation.MoneyChange = datosBusqueda_hotel.MoneyExchange 'Nuevo

                    dr_Reservation.HotelName = datosBusqueda_hotel.PropertyName
                    dr_Reservation.CityName = datosBusqueda_hotel.CityName
                    dr_Reservation.StateName = ""
                    dr_Reservation.CountryName = ""
                    dr_Reservation.HotelPhoneNumber = ""
                    dr_Reservation.HotelAddress = datosBusqueda_hotel.Address
                    dr_Reservation.CountryCode = datosBusqueda_hotel.CountryCode
                    dr_Reservation.RoomsDescription = datosBusqueda_hotel.nombreHabitacion
                    dr_Reservation.AltTotal = "0"
                    dr_Reservation.AltMoney = datosBusqueda_hotel.AltCurrency '"0"
                    dr_Reservation.AltTaxes = "0"
                    dr_Reservation.Taxes = datosBusqueda_hotel.AltTax
                    dr_Reservation.TaxIncluded = datosBusqueda_hotel.PlusTax
                    dr_Reservation.AltTotalExtras = "0"
                    dr_Reservation.TotalExtras = "0"
                    dr_Reservation.CityCode = datosBusqueda_hotel.ciudad_codigo
                    dr_Reservation.AccessCode = datosBusqueda_hotel.codProm
                    dr_Reservation.ReferenceContract = datosBusqueda_hotel.Convenio
                    dr_Reservation.FrecuentCode = datosBusqueda_hotel.frecuentCode
                    dr_Reservation.HomoClave = datosBusqueda_paquete.homoclave

                    dr_Reservation.DepositType = datosBusqueda_hotel.DepositType
                    dr_Reservation.PaymentInformation = datosBusqueda_hotel.PaymentOther

                    dr_Reservation.DepositLimitDate = datosBusqueda_hotel.DepositLimitDate.ToString("yyyyMMdd")
                    dr_Reservation.DepositLimitTime = datosBusqueda_hotel.DepositLimitDate.ToString("HH:mm")
                    dr_Reservation.CommentByVendor = datosBusqueda_hotel.Comments

                    dr_Reservation.DepositAmountCCT = datosBusqueda_hotel.DepositAmountCCT
                    dr_Reservation.DepositInfoCCT = datosBusqueda_hotel.DepositInfoCCT

                    dr_Reservation.MoneyChange = datosBusqueda_hotel.MoneyExchange

                    If datosBusqueda_hotel.Segmento = "" And dataOperador.Hoteles.Count > 0 Then dr_Reservation.OnRequest = True

                    If datosBusqueda_hotel.OnRequest Then
                        If datosBusqueda_hotel.Tax <> "" Then dr_Reservation.TaxPercentage = datosBusqueda_hotel.Tax
                        dr_Reservation.Total = CDec(datosBusqueda_hotel.AltTotalStay) + CDec(datosBusqueda_hotel.AltTax)
                    End If

                    dr_Reservation.CodigoReferenciaCCT = datosBusqueda_paquete.codigo_referencia

                    dr_Reservation.SetParentRow(dr_Hotel_Sell_Rq)
                    RQ.Reservation.Rows.Add(dr_Reservation)

                    Dim dr_Rooms As Package_BookingRq.RoomsRow = RQ.Rooms.NewRow
                    dr_Rooms.SetParentRow(dr_Hotel_Sell_Rq)
                    RQ.Rooms.Rows.Add(dr_Rooms)
                    '---
                    'Dim arr_cua_adul() As Integer = herramientas.get_arrCuartos(CInt(datosBusqueda_hotel.habitaciones), CInt(datosBusqueda_hotel.adultos))
                    'Dim arr_cua_chil() As Integer = herramientas.get_arrCuartos(CInt(datosBusqueda_hotel.habitaciones), CInt(datosBusqueda_hotel.ninos))

                    For Each hab As uc_habitaciones.T_Habitacion In datosBusqueda_hotel.habitaciones_.ListaHabitaciones
                        Dim dr_Room As Package_BookingRq.RoomRow = RQ.Room.NewRow
                        dr_Room.Children = hab.NumNinos ' arr_cua_chil(i) 'datosBusqueda_hotel.ninos
                        dr_Room.Adults = hab.NumAdultos '  arr_cua_adul(i) 'datosBusqueda_hotel.adultos
                        dr_Room.Ages = hab.EdadesNinosSTR
                        dr_Room.Preferences = datosBusqueda_hotel.cust_Preferences
                        dr_Room.idtraveler = "0"
                        dr_Room.NameRoom = datosBusqueda_hotel.nombreHabitacion
                        dr_Room.TravelerFirstName = datosBusqueda_paquete.cli_nombre
                        dr_Room.TravelerLastName = datosBusqueda_paquete.cli_apellido
												dr_Room.PhoneNumber = datosBusqueda_paquete.cli_telefono_AreaCityCode + "+" + datosBusqueda_paquete.cli_telefono_PhoneNumber
                        dr_Room.TravelerEmail = datosBusqueda_paquete.cli_email

                        dr_Room.TravelerCity = datosBusqueda_paquete.cli_ciudad
                        dr_Room.TravelerCountry = datosBusqueda_paquete.cli_pais
                        dr_Room.TravelerEstate = datosBusqueda_paquete.cli_estado
                        dr_Room.TravelerPostalCode = datosBusqueda_paquete.cli_cp
                        dr_Room.TravelerAddress = datosBusqueda_paquete.cli_direccion

                        dr_Room.SetParentRow(dr_Rooms)
                        RQ.Room.Rows.Add(dr_Room)
                    Next
                End If
            Next
            Return RQ
        End Function

        Private Shared Function rq_auto(ByVal RQ As Package_BookingRq, ByVal dr_PackageRequest As Package_BookingRq.PackageRequestRow, ByVal datosBusqueda_paquete As dataBusqueda_paquete) As Package_BookingRq
            Dim datosBusqueda_auto As dataBusqueda_auto_test = datosBusqueda_paquete.GetAutoNoConfirm
            If datosBusqueda_auto Is Nothing Then Return RQ

            'If datosBusqueda_paquete.lista_Autos.Count > 0 Then
            Dim dr_Car_Sell_Rq As Package_BookingRq.Car_Sell_RqRow = RQ.Car_Sell_Rq.NewRow
            dr_Car_Sell_Rq.idUser = dataOperador.user_ID_agent_ID '"0"
            dr_Car_Sell_Rq.Partner = "2"
            dr_Car_Sell_Rq.Provider = datosBusqueda_auto.ServiceProvider
            dr_Car_Sell_Rq.MoneyExchange = "0"
            dr_Car_Sell_Rq.CarVendor = datosBusqueda_auto.idVendor
            dr_Car_Sell_Rq.CarIdVendor = ""
            dr_Car_Sell_Rq.VendorName = ""
            dr_Car_Sell_Rq.StartDate = datosBusqueda_auto.fechaRecoger.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_Car_Sell_Rq.StartTime = datosBusqueda_auto.horaRecoger
            dr_Car_Sell_Rq.EndDate = datosBusqueda_auto.fechaDevolver.ToString(CapaPresentacion.Common.DateDataFormat)
            dr_Car_Sell_Rq.EndTime = datosBusqueda_auto.horaDevolver
            dr_Car_Sell_Rq.idAgency = "0"
            dr_Car_Sell_Rq.idAgent = "0"
            dr_Car_Sell_Rq.AirLine = "0"
            dr_Car_Sell_Rq.FlightNumber = "0"
            dr_Car_Sell_Rq.Purpose = "3"
            dr_Car_Sell_Rq.DriverName = datosBusqueda_paquete.cli_nombre + " " + datosBusqueda_paquete.cli_apellido         'checar
            If CapaPresentacion.Idiomas.IsCulturaEspanol Then dr_Car_Sell_Rq.Idioma = "1"
            If CapaPresentacion.Idiomas.IsCulturaIngles Then dr_Car_Sell_Rq.Idioma = "2"
            dr_Car_Sell_Rq.Session = "0"
            dr_Car_Sell_Rq.Source = CONST_SOURCE
            dr_Car_Sell_Rq.Currency = datosBusqueda_auto.CUR
            dr_Car_Sell_Rq.TotalCar = datosBusqueda_auto.RateAmtCONV
            dr_Car_Sell_Rq.TaxCar = datosBusqueda_auto.Tax
            dr_Car_Sell_Rq.RealTotalCar = datosBusqueda_auto.RateAmtTotAproxCONV
            dr_Car_Sell_Rq.RealCurrencyCar = datosBusqueda_auto.CUR
            dr_Car_Sell_Rq.RealTaxCar = datosBusqueda_auto.Tax
            dr_Car_Sell_Rq.DesCar = datosBusqueda_auto.SIPPClassText
            dr_Car_Sell_Rq.DetailCar = datosBusqueda_auto.SIPPTypeText
            dr_Car_Sell_Rq.CarEmail = datosBusqueda_paquete.cli_email
            dr_Car_Sell_Rq.CarFirstName = datosBusqueda_paquete.cli_nombre                                              'checar
            dr_Car_Sell_Rq.CarLastName = datosBusqueda_paquete.cli_apellido
            dr_Car_Sell_Rq.CarPhone = datosBusqueda_paquete.cli_telefono_AreaCityCode + "+" + datosBusqueda_paquete.cli_telefono_PhoneNumber
            dr_Car_Sell_Rq.SetParentRow(dr_PackageRequest)
            RQ.Car_Sell_Rq.Rows.Add(dr_Car_Sell_Rq)
            '---
            Dim dr_OfficeStart As Package_BookingRq.OfficeStartRow = RQ.OfficeStart.NewRow
            dr_OfficeStart.idOfficeStart = datosBusqueda_auto.idOffice
            dr_OfficeStart.CityStart = datosBusqueda_auto.ciudadRecoger
            dr_OfficeStart.CityCodeStart = datosBusqueda_auto.ciudadRecoger
            dr_OfficeStart.OfficeAddressStart = ""
            dr_OfficeStart.OfficeAddressPickupStart = ""
            dr_OfficeStart.LocnCatStart = datosBusqueda_auto.LocnCat
            dr_OfficeStart.LocnNumStart = datosBusqueda_auto.LocnNum
            dr_OfficeStart.Tax = datosBusqueda_auto.Tax
            If dr_OfficeStart.Tax = "" Then dr_OfficeStart.Tax = "0"
            dr_OfficeStart.SetParentRow(dr_Car_Sell_Rq)
            RQ.OfficeStart.Rows.Add(dr_OfficeStart)
            '---
            Dim dr_OfficeEnd As Package_BookingRq.OfficeEndRow = RQ.OfficeEnd.NewRow
            dr_OfficeEnd.idOfficeEnd = datosBusqueda_auto.idOfficeEnd
            dr_OfficeEnd.CityEnd = datosBusqueda_auto.ciudadDevolver
            dr_OfficeEnd.CityCodeEnd = datosBusqueda_auto.ciudadDevolver
            dr_OfficeEnd.LocnCatEnd = "0"
            dr_OfficeEnd.LocnNumEnd = "0"
            dr_OfficeEnd.SetParentRow(dr_Car_Sell_Rq)
            RQ.OfficeEnd.Rows.Add(dr_OfficeEnd)
            '---
            Dim dr_Car As Package_BookingRq.CarRow = RQ.Car.NewRow
            dr_Car.idCar = datosBusqueda_auto.idTypeCar
            dr_Car.CarTypeGDS = datosBusqueda_auto.GDSType
            dr_Car.SetParentRow(dr_Car_Sell_Rq)
            RQ.Car.Rows.Add(dr_Car)
            '---'---
            Dim dr_Rate As Package_BookingRq.RateRow = RQ.Rate.NewRow
            dr_Rate.idRate = "0"
            dr_Rate.AmountDay = datosBusqueda_auto.RateAmtCONV
            dr_Rate.AmountWeek = "0"
            dr_Rate.AmountExtraDay = "0"
            dr_Rate.AmountExtraHour = "0"
            dr_Rate.AmountDayQty = (datosBusqueda_auto.fechaDevolver - datosBusqueda_auto.fechaRecoger).TotalDays
            dr_Rate.AmountWeekQty = "0"
            dr_Rate.AmountExtraDayQty = "0"
            dr_Rate.AmountExtraHourQty = "0"
            dr_Rate.DropOff = "0"
            dr_Rate.AmountCarInsure = "0"
            dr_Rate.IdCarInsure = "0"
            dr_Rate.DesCarInsure = "0"
            dr_Rate.SetParentRow(dr_Car)
            RQ.Rate.Rows.Add(dr_Rate)
            '---'---
            Dim dr_RateGDS As Package_BookingRq.RateGDSRow = RQ.RateGDS.NewRow
            dr_RateGDS.RateAmt = datosBusqueda_auto.RateAmtCONV
            dr_RateGDS.RateG = datosBusqueda_auto.Rate
            dr_RateGDS.RateType = datosBusqueda_auto.RateType
            dr_RateGDS.RateCat = datosBusqueda_auto.RateCat
            dr_RateGDS.RateDBKey = datosBusqueda_auto.RateDBKey
            dr_RateGDS.RateSource = datosBusqueda_auto.RateSource
            dr_RateGDS.SetParentRow(dr_Car)
            RQ.RateGDS.Rows.Add(dr_RateGDS)
            '---'---
            Dim dr_AdditionalFees As Package_BookingRq.AdditionalFeesRow = RQ.AdditionalFees.NewRow
            dr_AdditionalFees.SetParentRow(dr_Car)
            RQ.AdditionalFees.Rows.Add(dr_AdditionalFees)
            '---'---'---
            Dim dr_AdditionalFee As Package_BookingRq.AdditionalFeeRow = RQ.AdditionalFee.NewRow
            dr_AdditionalFee.FeeName = datosBusqueda_auto.FeeName
            If dr_AdditionalFee.FeeName = "" Then dr_AdditionalFee.FeeName = "0"
            '
            dr_AdditionalFee.Percent = datosBusqueda_auto.Percent
            If dr_AdditionalFee.Percent = "" Then dr_AdditionalFee.Percent = "0"
            '
            dr_AdditionalFee.Amount = datosBusqueda_auto.Amount
            If dr_AdditionalFee.Amount = "" Then dr_AdditionalFee.Amount = "0"
            '
            dr_AdditionalFee.SetParentRow(dr_AdditionalFees)
            RQ.AdditionalFee.Rows.Add(dr_AdditionalFee)
            'End If

            Return RQ
        End Function

        Private Shared Function rq_actividad(ByVal RQ As Package_BookingRq, ByVal dr_PackageRequest As Package_BookingRq.PackageRequestRow, ByVal datosBusqueda_paquete As dataBusqueda_paquete) As Package_BookingRq
            If datosBusqueda_paquete.lista_Actividades.Count > 0 Then
                Dim dr_ActivitySell As Package_BookingRq.ActivitySellRow = RQ.ActivitySell.NewRow
                dr_ActivitySell.UserID = "0"
                dr_ActivitySell.AgencyID = "0"
                dr_ActivitySell.ActivityAltCurrencyCode = datosBusqueda_paquete.lista_Actividades(0).monedaBusqueda
                dr_ActivitySell.MoneyExchangeRate = "0"
                dr_ActivitySell.ActivityAltTotal = "0"
                dr_ActivitySell.ActivityCurrencyCode = datosBusqueda_paquete.lista_Actividades(0).monedaBusqueda
                dr_ActivitySell.ActivityTax = "0"
                dr_ActivitySell.ActivityAltTax = "0"
                dr_ActivitySell.ActivityTotal = "0"
                dr_ActivitySell.SetParentRow(dr_PackageRequest)
                RQ.ActivitySell.Rows.Add(dr_ActivitySell)
                '---
                Dim dr_ActivityHeader As Package_BookingRq.ActivityHeaderRow = RQ.ActivityHeader.NewRow
                dr_ActivityHeader.Source = CONST_SOURCE
                'dr_ActivityHeader.PortalID = dataOperador.idPortal 'CONST_IDPORTAL

                dr_ActivityHeader.ServiciTranID = "1"
                dr_ActivityHeader.PackageItinerary = "0"
                dr_ActivityHeader.SetParentRow(dr_ActivitySell)
                RQ.ActivityHeader.Rows.Add(dr_ActivityHeader)
                '---'---

                For Each datos_actividad As dataBusqueda_actividad_test In datosBusqueda_paquete.lista_Actividades
                    If datos_actividad.confirmada Then Continue For

                    For Each rate As RateType In datos_actividad.ListRateType

                        'Dim drs_Event() As Package_BookingRq.EventRow = CType(RQ._Event.Select("RateID=" + rate.RateID.ToString()), Package_BookingRq.EventRow())


                        'If drs_Event.Length = 0 Then
                        Dim drs_Event As Package_BookingRq.EventRow = RQ._Event.NewEventRow
                        drs_Event = RQ._Event.NewRow
                        drs_Event.EventID = datos_actividad.EventID
                        drs_Event.RateID = rate.RateID
                        If datosBusqueda_paquete.cli_id = "" Then drs_Event.ClientID = "0" Else drs_Event.ClientID = datosBusqueda_paquete.cli_id
                        drs_Event.ChargeFullAmount = "0"
                        'drs_Event(0).Description = ""
                        drs_Event.PropertyID = datos_actividad.PropertyID
                        'drs_Event(0).PropertyName = ""
                        drs_Event.SetParentRow(dr_ActivitySell)

                        RQ._Event.Rows.Add(drs_Event)
                        'End If
                        '---'---'---
                        Dim dr_PriceType As Package_BookingRq.PriceTypeRow = RQ.PriceType.NewRow
                        dr_PriceType.PriceTypeID = rate.RateTypeID
                        dr_PriceType.Quantity = rate.Cantidad
                        dr_PriceType.Day = rate.DayReservar
                        'dr_PriceType.ActivityTotalPrice = ""
                        ' dr_PriceType.ActivityAltTotalPrice = ""
                        dr_PriceType.TicketDescription = rate.RateTypeName
                        dr_PriceType.SetParentRow(drs_Event)
                        RQ.PriceType.Rows.Add(dr_PriceType)
                    Next
                Next

                'For Each datos_actividad As dataBusqueda_actividad_test In datosBusqueda_paquete.lista_Actividades
                '    If datos_actividad.confirmada Then Continue For

                '    Dim dr_Event As Package_BookingRq.EventRow = RQ._Event.NewRow
                '    dr_Event.EventID = datos_actividad.EventID
                '    dr_Event.RateID = datos_actividad.RateID
                '    If datosBusqueda_paquete.cli_id = "" Then dr_Event.ClientID = "0" Else dr_Event.ClientID = datosBusqueda_paquete.cli_id
                '    dr_Event.ChargeFullAmount = "0"
                '    dr_Event.Description = ""
                '    dr_Event.PropertyID = datos_actividad.PropertyID
                '    dr_Event.PropertyName = ""
                '    dr_Event.SetParentRow(dr_ActivitySell)
                '    RQ._Event.Rows.Add(dr_Event)
                '    '---'---'---
                '    Dim dr_PriceType As Package_BookingRq.PriceTypeRow = RQ.PriceType.NewRow
                '    dr_PriceType.PriceTypeID = datos_actividad.RateTypeID
                '    dr_PriceType.Quantity = datos_actividad.cantidad
                '    dr_PriceType.Day = datos_actividad.DayReservar
                '    dr_PriceType.ActivityTotalPrice = ""
                '    dr_PriceType.ActivityAltTotalPrice = ""
                '    dr_PriceType.TicketDescription = datos_actividad.RateTypeName
                '    dr_PriceType.SetParentRow(dr_Event)
                '    RQ.PriceType.Rows.Add(dr_PriceType)
                'Next
            End If

            Return RQ
        End Function

        Private Shared Function rq_vuelo(ByVal RQ As Package_BookingRq, ByVal dr_PackageRequest As Package_BookingRq.PackageRequestRow, ByVal datosBusqueda_paquete As dataBusqueda_paquete) As Package_BookingRq
            Dim datosBusqueda_vuelo As dataBusqueda_vuelo = datosBusqueda_paquete.GetVueloNoConfirm
            If datosBusqueda_vuelo Is Nothing Then Return RQ

            'If datosBusqueda_paquete.lista_Vuelos.Count > 0 Then
            Dim base As Double = CDbl(datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.BaseFare_Amount)
            Dim taxes As Double = 0
            For Each tax As dataVuelosItinerario.vuelo.ItinTotalFare.Tax In datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Taxes
                taxes += CDbl(tax.Tax_Amount)
            Next
            'Dim total As Double = base + taxes
            Dim fees As Double = 0
            For Each fee As dataVuelosItinerario.vuelo.ItinTotalFare.Fee In datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Fees
                fees += CDbl(fee.Fee_Amount)
            Next
            Dim TicketTimeLimit As DateTime = DateTime.ParseExact(datosBusqueda_vuelo.vuelo.TicketingInfo_TicketTimeLimit, CapaPresentacion.Common.DateDataFormat, Nothing)
            Dim meses() As String = New String() {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"}

            'Dim RQ As New OTA_AirBookRQ

            Dim dr_Flight_Sell_Rq As Package_BookingRq.Flight_Sell_RqRow = RQ.Flight_Sell_Rq.NewRow
            dr_Flight_Sell_Rq.TotalFlight = ""
            dr_Flight_Sell_Rq.TaxFlight = ""
            dr_Flight_Sell_Rq.RealTotalFlight = ""
            dr_Flight_Sell_Rq.RealTaxFlight = ""
            dr_Flight_Sell_Rq.RealCurrencyFlight = ""
            dr_Flight_Sell_Rq.SetParentRow(dr_PackageRequest)
            RQ.Flight_Sell_Rq.Rows.Add(dr_Flight_Sell_Rq)

            '///// POS \\\\\
            Dim dr_POS As Package_BookingRq.POSRow = RQ.POS.NewRow
            dr_POS.SetParentRow(dr_Flight_Sell_Rq)
            RQ.POS.Rows.Add(dr_POS)
            ''''
            Dim dr_Source As Package_BookingRq.SourceRow = RQ.Source.NewRow
            dr_Source.AgentSine = "1"
            dr_Source.PseudoCityCode = "0"
            dr_Source.ISOCountry = "1"
            dr_Source.ISOCurrency = "MXN"
            dr_Source.AirlineVendorID = "0"
            dr_Source.AirportCode = ""
            dr_Source.SetParentRow(dr_POS)
            RQ.Source.Rows.Add(dr_Source)
            ''''''''
            Dim dr_RequestorID As Package_BookingRq.RequestorIDRow = RQ.RequestorID.NewRow
            dr_RequestorID.URL = ""
            dr_RequestorID.Type = ""
            dr_RequestorID.ID = "-1"
            dr_RequestorID.status = ""
            dr_RequestorID.RvaNumber = ""
            dr_RequestorID.ID_Agency = "1"
            dr_RequestorID.ID_Portal = "2"
            dr_RequestorID.Prov = "1"
            dr_RequestorID.CodViajero = dataOperador.user_ID_agent_ID
            'dr_RequestorID.IsPackage = "0"
            dr_RequestorID.PackageItinerary = "0"
            dr_RequestorID.Source = "POR"
            dr_RequestorID.idAgent = "0"
            dr_RequestorID.SetParentRow(dr_Source)
            RQ.RequestorID.Rows.Add(dr_RequestorID)
            ''''''''
            Dim dr_BookingChannel As Package_BookingRq.BookingChannelRow = RQ.BookingChannel.NewRow
            dr_BookingChannel.Type = ""
            dr_BookingChannel.Primary = ""
            dr_BookingChannel.SetParentRow(dr_Source)
            RQ.BookingChannel.Rows.Add(dr_BookingChannel)

            '///// AirItinerary \\\\\
            Dim dr_AirItinerary As Package_BookingRq.AirItineraryRow = RQ.AirItinerary.NewRow
            dr_AirItinerary.SetParentRow(dr_Flight_Sell_Rq)
            RQ.AirItinerary.Rows.Add(dr_AirItinerary)
            ''''
            Dim dr_OriginDestinationOptions As Package_BookingRq.OriginDestinationOptionsRow = RQ.OriginDestinationOptions.NewRow
            dr_OriginDestinationOptions.SetParentRow(dr_AirItinerary)
            RQ.OriginDestinationOptions.Rows.Add(dr_OriginDestinationOptions)

            Dim dr_OriginDestinationOption As Package_BookingRq.OriginDestinationOptionRow = RQ.OriginDestinationOption.NewRow
            dr_OriginDestinationOption.SetParentRow(dr_OriginDestinationOptions)
            RQ.OriginDestinationOption.Rows.Add(dr_OriginDestinationOption)
            ''''
            Dim escalas As New List(Of dataVuelosItinerario.vuelo.unVuelo.escala)
            escalas.AddRange(datosBusqueda_vuelo.vuelo.vuelo_ow.lista_escala)
            escalas.AddRange(datosBusqueda_vuelo.vuelo.vuelo_rt.lista_escala)
            For Each esc As dataVuelosItinerario.vuelo.unVuelo.escala In escalas
                Dim dr_FlightSegment_1 As Package_BookingRq.FlightSegmentRow = RQ.FlightSegment.NewRow
                dr_FlightSegment_1.Journy = esc.Journy
                dr_FlightSegment_1.ArrivalDateTime = esc.ArrivalDateTime
                dr_FlightSegment_1.DepartureDateTime = esc.DepartureDateTime
                dr_FlightSegment_1.StopQuantity = esc.StopQuantity
                dr_FlightSegment_1.FlightNumber = esc.FlightNumber
                dr_FlightSegment_1.JourneyDuration = esc.JourneyDuration
                dr_FlightSegment_1.OnTimeRate = esc.OnTimeRate
                dr_FlightSegment_1.Ticket = esc.Ticket
                dr_FlightSegment_1.SmokingAllowed = esc.SmokingAllowed
                dr_FlightSegment_1.GroundDuration = esc.GroundDuration
                dr_FlightSegment_1.AccumulatedDuration = esc.AccumulatedDuration
                dr_FlightSegment_1.SetParentRow(dr_OriginDestinationOption)
                RQ.FlightSegment.Rows.Add(dr_FlightSegment_1)

                Dim dr_DepartureAirport As Package_BookingRq.DepartureAirportRow = RQ.DepartureAirport.NewRow
                dr_DepartureAirport.LocationCode = esc.dep_LocationCode
                dr_DepartureAirport.Terminal = esc.dep_Terminal
                dr_DepartureAirport.Text = esc.dep_str
                dr_DepartureAirport.SetParentRow(dr_FlightSegment_1)
                RQ.DepartureAirport.Rows.Add(dr_DepartureAirport)

                Dim dr_ArrivalAirport As Package_BookingRq.ArrivalAirportRow = RQ.ArrivalAirport.NewRow
                dr_ArrivalAirport.LocationCode = esc.arr_LocationCode
                dr_ArrivalAirport.Terminal = esc.arr_Terminal
                dr_ArrivalAirport.Text = esc.arr_str
                dr_ArrivalAirport.SetParentRow(dr_FlightSegment_1)
                RQ.ArrivalAirport.Rows.Add(dr_ArrivalAirport)

                Dim dr_Equipment As Package_BookingRq.EquipmentRow = RQ.Equipment.NewRow
                dr_Equipment.AirEquipType = esc.equ_AirEquipType
                dr_Equipment.ChangeofGauge = esc.equ_ChangeofGauge
                dr_Equipment.SetParentRow(dr_FlightSegment_1)
                RQ.Equipment.Rows.Add(dr_Equipment)

                Dim dr_MarketingAirline As Package_BookingRq.MarketingAirlineRow = RQ.MarketingAirline.NewRow
                dr_MarketingAirline.CompanyShortName = esc.m_air_CompanyShortName
                dr_MarketingAirline.OpAir = esc.m_air_OpAir
                dr_MarketingAirline.Text = esc.m_air_str
                dr_MarketingAirline.SetParentRow(dr_FlightSegment_1)
                RQ.MarketingAirline.Rows.Add(dr_MarketingAirline)

                Dim dr_MarketingCabin As Package_BookingRq.MarketingCabinRow = RQ.MarketingCabin.NewRow
                dr_MarketingCabin.CabinType = esc.m_cab_RPH '"Y" 'esc.m_cab_CabinType
                dr_MarketingCabin.RPH = "" 'con cadena vacia OK, con otra cosa, error           'esc.m_cab_RPH
                dr_MarketingCabin.SetParentRow(dr_FlightSegment_1)
                RQ.MarketingCabin.Rows.Add(dr_MarketingCabin)
            Next

            Dim dr_FareInfo As Package_BookingRq.FareInfoRow = RQ.FareInfo.NewRow
            dr_FareInfo.SetParentRow(dr_OriginDestinationOptions)
            RQ.FareInfo.Rows.Add(dr_FareInfo)
            ''''
            Dim dr_PTC As Package_BookingRq.PTCRow = RQ.PTC.NewRow
            dr_PTC.PassengerTypeCode = datosBusqueda_vuelo.vuelo.datos_FareInfo.PTC_PassengerTypeCode
            dr_PTC.Quantity = datosBusqueda_vuelo.adultos 'Data.AAAA        <- queda pendiente de checar
            dr_PTC.SetParentRow(dr_FareInfo)
            RQ.PTC.Rows.Add(dr_PTC)
            ''''''''
            For Each FICInfo As dataVuelosItinerario.vuelo.FareInfo.FICInfo In datosBusqueda_vuelo.vuelo.datos_FareInfo.PTC_lista_FICInfo
                Dim dr_FICInfo As Package_BookingRq.FICInfoRow = RQ.FICInfo.NewRow
                dr_FICInfo.Seg = Format(CInt(FICInfo.FICInfo_Seg), "00")
                dr_FICInfo.Fic = FICInfo.FICInfo_Fic
                dr_FICInfo.SetParentRow(dr_PTC)
                RQ.FICInfo.Rows.Add(dr_FICInfo)
            Next
            ''''
            Dim dr_BaseFare As Package_BookingRq.BaseFareRow = RQ.BaseFare.NewRow
            dr_BaseFare.Amount = base 'datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.BaseFare_Amount
            dr_BaseFare.CurrencyCode = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.BaseFare_CurrencyCode
            dr_BaseFare.DecimalPlaces = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.BaseFare_DecimalPlaces
            dr_BaseFare.SetParentRow(dr_FareInfo)
            RQ.BaseFare.Rows.Add(dr_BaseFare)
            ''''
            Dim dr_Taxes As Package_BookingRq.TaxesRow = RQ.Taxes.NewRow
            dr_Taxes.Amount = taxes 'datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Taxes(-1).Tax_Amount
            dr_Taxes.CurrencyCode = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.BaseFare_CurrencyCode 'datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Taxes(-1).Tax_CurrencyCode
            dr_Taxes.DecimalPlaces = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.BaseFare_DecimalPlaces '= datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Taxes(-1).Tax_TaxCode
            dr_Taxes.SetParentRow(dr_FareInfo)
            RQ.Taxes.Rows.Add(dr_Taxes)
            ''''
            Dim dr_TotalFare As Package_BookingRq.TotalFareRow = RQ.TotalFare.NewRow
            dr_TotalFare.Amount = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.TotalFare_Amount
            dr_TotalFare.CurrencyCode = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.TotalFare_CurrencyCode
            dr_TotalFare.DecimalPlaces = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.TotalFare_DecimalPlaces
            dr_TotalFare.SetParentRow(dr_FareInfo)
            RQ.TotalFare.Rows.Add(dr_TotalFare)
            ''''
            Dim dr_Fee As Package_BookingRq.FeeRow = RQ.Fee.NewRow
            dr_Fee.Amount = fees 'datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Fees(-1).Fee_Amount
            dr_Fee.CurrencyCode = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Fees(0).Fee_CurrencyCode
            dr_Fee.DecimalPlaces = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.Fees(0).Fee_DecimalPlaces
            dr_Fee.SetParentRow(dr_FareInfo)
            RQ.Fee.Rows.Add(dr_Fee)

            '///// TravelerInfo \\\\\
            Dim dr_TravelerInfo As Package_BookingRq.TravelerInfoRow = RQ.TravelerInfo.NewRow
            dr_TravelerInfo.SetParentRow(dr_Flight_Sell_Rq)
            RQ.TravelerInfo.Rows.Add(dr_TravelerInfo)

            Dim dr_AirTraveler As Package_BookingRq.AirTravelerRow = RQ.AirTraveler.NewRow
            dr_AirTraveler.PassengerTypeCode = datosBusqueda_vuelo.vuelo.datos_FareInfo.PTC_PassengerTypeCode
            dr_AirTraveler.Age = ""
            dr_AirTraveler.SetParentRow(dr_TravelerInfo)
            RQ.AirTraveler.Rows.Add(dr_AirTraveler)

            Dim dr_ProfileRef As Package_BookingRq.ProfileRefRow = RQ.ProfileRef.NewRow
            dr_ProfileRef.SetParentRow(dr_AirTraveler)
            RQ.ProfileRef.Rows.Add(dr_ProfileRef)
            ''''''''
            Dim dr_UniqueID As Package_BookingRq.UniqueIDRow = RQ.UniqueID.NewRow
            dr_UniqueID.URL = ""
            dr_UniqueID.Type = "1"
            dr_UniqueID.Instance = "0"    '11708
            dr_UniqueID.ID = "01"
            dr_UniqueID.SetParentRow(dr_ProfileRef)
            RQ.UniqueID.Rows.Add(dr_UniqueID)
            ''''
            Dim dr_PersonName As Package_BookingRq.PersonNameRow = RQ.PersonName.NewRow
            dr_PersonName.NamePrefix = "Mr." 'datosBusqueda_vuelo.nombre_prefijo
            dr_PersonName.GivenName = datosBusqueda_paquete.cli_nombre '.lista_Vuelos(0).nombre.ToUpper
            dr_PersonName.Surname = datosBusqueda_paquete.cli_apellido '.lista_Vuelos(0).apellido.ToUpper
            dr_PersonName.NameTitle = ""
            dr_PersonName.SurnamePrefix = "01" 'datosBusqueda_vuelo.apellido_prefijo
            dr_PersonName.SetParentRow(dr_AirTraveler)
            RQ.PersonName.Rows.Add(dr_PersonName)
            ''''
            Dim dr_Telephone As Package_BookingRq.TelephoneRow = RQ.Telephone.NewRow
            dr_Telephone.AreaCityCode = datosBusqueda_paquete.cli_telefono_AreaCityCode '.lista_Vuelos(0).telefono_AreaCityCode
						dr_Telephone.PhoneNumber = datosBusqueda_paquete.cli_telefono_PhoneNumber	'.lista_Vuelos(0).telefono_PhoneNumber
            dr_Telephone.PhoneUseType = datosBusqueda_paquete.cli_telefono_PhoneUseType '.lista_Vuelos(0).telefono_PhoneUseType
            dr_Telephone.SetParentRow(dr_AirTraveler)
            RQ.Telephone.Rows.Add(dr_Telephone)
            ''''
            Dim dr_Email As Package_BookingRq.EmailRow = RQ.Email.NewRow
            dr_Email.EmailType = "1"
            dr_Email.Text = datosBusqueda_paquete.cli_email '.lista_Vuelos(0).email
            dr_Email.SetParentRow(dr_AirTraveler)
            RQ.Email.Rows.Add(dr_Email)
            ''''
            Dim dr_Address As Package_BookingRq.AddressRow = RQ.Address.NewRow
            dr_Address.AddressLine = datosBusqueda_paquete.cli_direccion '.lista_Vuelos(0).direccion
            dr_Address.CityName = datosBusqueda_paquete.cli_ciudad '.lista_Vuelos(0).ciudad
            dr_Address.PostalCode = datosBusqueda_paquete.cli_cp '.lista_Vuelos(0).cp
            dr_Address.SetParentRow(dr_AirTraveler)
            RQ.Address.Rows.Add(dr_Address)
            ''''''''
            Dim dr_StateProv As Package_BookingRq.StateProvRow = RQ.StateProv.NewRow
            dr_StateProv.StateCode = datosBusqueda_paquete.cli_estado '.lista_Vuelos(0).estado
            dr_StateProv.Text = datosBusqueda_paquete.cli_ciudad '.lista_Vuelos(0).ciudad
            dr_StateProv.SetParentRow(dr_Address)
            RQ.StateProv.Rows.Add(dr_StateProv)
            ''''''''
            Dim dr_CountryName As Package_BookingRq.CountryNameRow = RQ.CountryName.NewRow
            dr_CountryName.Code = datosBusqueda_paquete.cli_pais '.lista_Vuelos(0).pais                              '
            dr_CountryName.SetParentRow(dr_Address)
            RQ.CountryName.Rows.Add(dr_CountryName)
            ''''
            Dim dr_TravelerRefNumber As Package_BookingRq.TravelerRefNumberRow = RQ.TravelerRefNumber.NewRow
            dr_TravelerRefNumber.RPH = datosBusqueda_vuelo.vuelo.RPH
            dr_TravelerRefNumber.SetParentRow(dr_AirTraveler)
            RQ.TravelerRefNumber.Rows.Add(dr_TravelerRefNumber)
            ''''
            'Dim dr_ CustLoyalty As Package_BookingRq.CustLoyaltyRow = RQ.CustLoyalty.NewRow
            'dr_CustLoyalty.ProgramID = ""
            'dr_CustLoyalty.MembershipID = ""
            'dr_CustLoyalty.SetParentRow(dr_AirTraveler)
            'RQ.Address.Rows.Add(dr_ CustLoyalty)
            '''''
            'Dim dr_ document As Package_BookingRq.CustLoyaltyRow = RQ.CustLoyalty.NewRow
            'dr_(document.ProgramID = "")
            'dr_(document.MembershipID = "")
            'dr_(document.SetParentRow(dr_AirTraveler))
            'RQ.Address.Rows.Add(dr_ document)
            '''''
            'Dim dr_ TravelerInfo As Package_BookingRq.CustLoyaltyRow = RQ.CustLoyalty.NewRow
            'dr_(TravelerInfo.ProgramID = "")
            'dr_(TravelerInfo.MembershipID = "")
            'dr_(TravelerInfo.SetParentRow(dr_AirTraveler))
            'RQ.Address.Rows.Add(dr_ document)
            ''''
            'SpecialReqDetails
            ''''
            'SeatRequests
            ''''
            'SeatRequest
            ''''
            'SpecialServiceRequests
            ''''
            'SpecialServiceRequest

            '///// Ticketing \\\\\

            Dim dr_Ticketing As Package_BookingRq.TicketingRow = RQ.Ticketing.NewRow
            dr_Ticketing.TicketType = datosBusqueda_vuelo.vuelo.vuelo_ow.lista_escala(0).Ticket
            dr_Ticketing.TicketTimeLimit = TicketTimeLimit.ToString("dd") + meses(TicketTimeLimit.Month - 1)
            If datosBusqueda_vuelo.clase = CapaPresentacion.Idiomas.get_str("str_0256_economica", "EN") Then dr_Ticketing.Clase = "Y"
            If datosBusqueda_vuelo.clase = CapaPresentacion.Idiomas.get_str("str_0257_negocios", "EN") Then dr_Ticketing.Clase = "C"
            If datosBusqueda_vuelo.clase = CapaPresentacion.Idiomas.get_str("str_0258_primeraClase", "EN") Then dr_Ticketing.Clase = "F"
            dr_Ticketing.TicketTimeLimitDate = TicketTimeLimit.ToString("yyyy/MM/dd")
            dr_Ticketing.SetParentRow(dr_Flight_Sell_Rq)
            RQ.Ticketing.Rows.Add(dr_Ticketing)

            '///// Fulfillment \\\\\

            Dim dr_Fulfillment As Package_BookingRq.FulfillmentRow = RQ.Fulfillment.NewRow
            dr_Fulfillment.SetParentRow(dr_Flight_Sell_Rq)
            RQ.Fulfillment.Rows.Add(dr_Fulfillment)

            Dim dr_PaymentDetails As Package_BookingRq.PaymentDetailsRow = RQ.PaymentDetails.NewRow
            dr_PaymentDetails.SetParentRow(dr_Fulfillment)
            RQ.PaymentDetails.Rows.Add(dr_PaymentDetails)
            ''''''''
            Dim dr_PaymentCard As Package_BookingRq.PaymentCardRow = RQ.PaymentCard.NewRow
            If datosBusqueda_paquete.cc_tipo = "VI" Then dr_PaymentCard.CardType = "4"
            If datosBusqueda_paquete.cc_tipo = "AX" Then dr_PaymentCard.CardType = "1"
            If datosBusqueda_paquete.cc_tipo = "CA" Then dr_PaymentCard.CardType = "5"
            dr_PaymentCard.CardCode = datosBusqueda_paquete.cc_numSeguridad
            dr_PaymentCard.CardNumber = datosBusqueda_paquete.cc_numero
            If datosBusqueda_paquete.cc_fechaExpira.Length = 6 Then dr_PaymentCard.EffectiveDate = datosBusqueda_paquete.cc_fechaExpira.Substring(0, 2)
            If datosBusqueda_paquete.cc_fechaExpira.Length = 6 Then dr_PaymentCard.ExpireDate = datosBusqueda_paquete.cc_fechaExpira.Substring(2, 4)
            dr_PaymentCard.CardHolderName = datosBusqueda_paquete.cc_apellido + ", " + datosBusqueda_paquete.cc_nombre_
            dr_PaymentCard.SetParentRow(dr_PaymentDetails)
            RQ.PaymentCard.Rows.Add(dr_PaymentCard)
            ''''''''
            Dim dr_PaymentAmount As Package_BookingRq.PaymentAmountRow = RQ.PaymentAmount.NewRow
            dr_PaymentAmount.Amount = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.TotalFare_Amount
            dr_PaymentAmount.CurrencyCode = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.TotalFare_CurrencyCode
            dr_PaymentAmount.DecimalPlaces = datosBusqueda_vuelo.vuelo.datos_ItinTotalFare.TotalFare_DecimalPlaces
            dr_PaymentAmount.SetParentRow(dr_PaymentDetails)
            RQ.PaymentAmount.Rows.Add(dr_PaymentAmount)
            ''''
            If Not datosBusqueda_vuelo.entrega_tipo Then
                Dim dr_DeliveryAddress As Package_BookingRq.DeliveryAddressRow = RQ.DeliveryAddress.NewRow
                dr_DeliveryAddress.DeliveryStreetNmbr = "" 'datosBusqueda_vuelo.entrega_direccion
                dr_DeliveryAddress.DeliveryAddressLine = datosBusqueda_vuelo.entrega_direccion
                dr_DeliveryAddress.DeliveryCityName = datosBusqueda_vuelo.entrega_ciudad
                dr_DeliveryAddress.DeliveryPostalCode = datosBusqueda_vuelo.entrega_cp
                dr_DeliveryAddress.DeliveryCounty = datosBusqueda_vuelo.entrega_pais
                dr_DeliveryAddress.DeliveryCountryName = "" 'datosBusqueda_vuelo.entrega_
                dr_DeliveryAddress.DeliveryStateProv_STA = datosBusqueda_vuelo.entrega_estado
                dr_DeliveryAddress.SetParentRow(dr_Fulfillment)
                RQ.DeliveryAddress.Rows.Add(dr_DeliveryAddress)
            End If
            'End If

            Return RQ
        End Function

        Public Shared Function obten_paquete_Cancel(ByVal datosBusqueda_paquete As dataBusqueda_paquete) As DataSet
            Dim RQ As New Package_Cancel_RQ

            Dim dr_PackageCancelRQ As Package_Cancel_RQ.PackageCancelRQRow = RQ.PackageCancelRQ.NewRow
            dr_PackageCancelRQ.CancelAllPackage = True
            dr_PackageCancelRQ.NoItinerary = datosBusqueda_paquete.itinerario
            RQ.PackageCancelRQ.Rows.Add(dr_PackageCancelRQ)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        Public Shared Function obten_paquete_Description(ByVal datosBusqueda_paquete As dataBusqueda_paquete) As DataSet
            Dim RQ As New Package_Description_RQ

            Dim dr_PackageDescription As Package_Description_RQ.PackageDescriptionRow = RQ.PackageDescription.NewRow
            dr_PackageDescription.Itinerario = datosBusqueda_paquete.itinerario
            RQ.PackageDescription.Rows.Add(dr_PackageDescription)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function

        'PASSPORT--------------------------------------

        Public Shared Function obten_passport_GetReservations(ByVal datosBusqueda_passport As dataBusqueda_passport) As DataSet
            Dim RQ As New Passport_GetReservations_RQ

            Dim dr_Request As Passport_GetReservations_RQ.RequestRow = RQ.Request.NewRow
            If Not datosBusqueda_passport.idCorporativo Is Nothing AndAlso datosBusqueda_passport.idCorporativo <> -1 Then
                dr_Request.idCorporativo = datosBusqueda_passport.idCorporativo
            Else
                dr_Request.idUsuario = datosBusqueda_passport.idUsuario
            End If



            dr_Request.idAgencia = Nothing
            dr_Request.Fecha1 = datosBusqueda_passport.Fecha1.ToString("yyyy/MM/dd")
            dr_Request.Fecha2 = datosBusqueda_passport.Fecha2.ToString("yyyy/MM/dd")
            dr_Request.Rubro = datosBusqueda_passport.Rubro
            If datosBusqueda_passport.Origen IsNot Nothing Then
                dr_Request.SourceList = "," + datosBusqueda_passport.Origen + ","
            Else
                dr_Request.SourceList = ""
            End If

            dr_Request.Type = datosBusqueda_passport.Type

            dr_Request.NombreCorp = Nothing
            dr_Request.NombreClie = datosBusqueda_passport.NombreClie
            dr_Request.NumeroConf = datosBusqueda_passport.NumeroConf

            dr_Request.idSegmento = datosBusqueda_passport.idSegmento
            dr_Request.SegmentoEmpresa = datosBusqueda_passport.NombreEmpresa
            dr_Request.PendientesPMS = datosBusqueda_passport.TransaccionesPMS
            dr_Request.idUsuarioCallCenter = datosBusqueda_passport.IdUsuarioCallCenter
            dr_Request.idempresa = datosBusqueda_passport.idEmpresa

            RQ.Request.Rows.Add(dr_Request)

            Dim ws As New webService_services(username, password)
            Dim ds As DataSet = ws.call_WS(RQ.GetXml)

            Return ds
        End Function


        Public Shared Function obten_convenios(ByVal key As String) As DataSet

            Dim ws As New webService_services(username, password)

            Dim idcorporativo = -1
            If Not ws_login.getUserAccessInfo.IsidCorporativoNull Then
                idcorporativo = ws_login.getUserAccessInfo.idCorporativo
            End If
            'No necesite de un dataset por que la peticion es muy sencilla
            Dim xml As String = String.Format("<Agreement_Search_RQ><Key>{0}</Key><IdCorporativo>{1}</IdCorporativo></Agreement_Search_RQ>", key, idcorporativo)
            Dim ds As DataSet = ws.call_WS(xml)

            Return ds
        End Function

        Public Shared Function obten_ratesplans(ByVal idHotel As Integer) As DataSet

            Dim ws As New webService_services(username, password)

            'No necesite de un dataset por que la peticion es muy sencilla
            Dim xml As String = String.Format("<Ratesplan_Search_RQ><IdHotel>{0}</IdHotel><IdIdioma>{1}</IdIdioma></Ratesplan_Search_RQ>", idHotel, CapaPresentacion.Idiomas.idIdioma)
            Dim ds As DataSet = ws.call_WS(xml)

            Return ds
        End Function
    End Class

End Class