



-------------------------------------------
RQ
14/03/2017 02:40:49 p. m.
Version: 1.0.4.11
-------------------------------------------
<?xml version="1.0" encoding="utf-16"?><reqHotelDisplay><HotelDisplay><ChainCode></ChainCode><ConfirmNumber></ConfirmNumber><Source>CCT</Source><Language>ES</Language><AltCurrency>MXN</AltCurrency><PropertyNumber></PropertyNumber><ReservationId>305711</ReservationId><Corporativo></Corporativo></HotelDisplay></reqHotelDisplay>



-------------------------------------------
RS
14/03/2017 02:40:53 p. m.
Version: 1.0.4.11
-------------------------------------------
<Hotel_Display_RS>
  <HotelDisplay>
    <Customer>
      <PhoneHome />
      <LastName />
      <FirstName />
      <UserId>23055</UserId>
      <Estate>Baja California Sur</Estate>
      <City>La Paz</City>
    </Customer>
    <Reservation>
      <CheckInDate>2017/10/10</CheckInDate>
      <CheckOutDate>2017/10/12</CheckOutDate>
      <PropertyNumber>1978</PropertyNumber>
      <ChainCode>UV</ChainCode>
      <CreditCardExpiration>102018</CreditCardExpiration>
      <CreditCardHolder>Luis/Cota</CreditCardHolder>
      <CreditCardNumber />
      <CreditCardNumberVerify>****</CreditCardNumberVerify>
      <RateCode>D1KRAC</RateCode>
      <CreditCardType>VI</CreditCardType>
      <RAwayAdultPrice>0</RAwayAdultPrice>
      <RAwayChildPrice>0</RAwayChildPrice>
      <RAwayCribPrice>0</RAwayCribPrice>
      <Total>2204</Total>
      <Taxes>304</Taxes>
      <ConfirmNumber>305711</ConfirmNumber>
      <CXNumber />
      <Status>4</Status>
      <GuarDep>G</GuarDep>
      <RatePlan>RAC</RatePlan>
      <PlusTax>False</PlusTax>
      <StatusDate>2017/03/13 14:27</StatusDate>
      <ReservationDate>2017/03/13</ReservationDate>
      <RAwayAdult>0</RAwayAdult>
      <RAwayChild>0</RAwayChild>
      <RAwayCrib>0</RAwayCrib>
      <HotelName>Hotel Misión Aguascalientes Sur </HotelName>
      <CityName>Aguascalientes</CityName>
      <CountryName>MX</CountryName>
      <RecLoc />
      <Provider>0</Provider>
      <EmailReservations>josealberto@internetpowerhotel.com,josealberto@univisit.com</EmailReservations>
      <HotelAddress>Blvd. José María Chávez # 2100. Ciudad Industrial</HotelAddress>
      <Money>MXN</Money>
      <ServiceFee>0</ServiceFee>
      <CompanyId>6972</CompanyId>
      <HotelCheckin>1500</HotelCheckin>
      <HotelCheckout>1300</HotelCheckout>
      <AltMoney>MXN</AltMoney>
      <AltTotal>2204</AltTotal>
      <AltTaxes>304</AltTaxes>
      <ImageLogo>https://crs.univisit.com/global/ImagesSystem/Logos/LogoCompany_6972</ImageLogo>
      <StatusConf>Y</StatusConf>
      <AgeNombre>UNIVISIT CALL CENTER</AgeNombre>
      <AgeDom>5 DE MAYO</AgeDom>
      <AgeCiudad>La Paz</AgeCiudad>
      <AgeEstado>Baja California Sur</AgeEstado>
      <AgePais>MEXICO</AgePais>
      <AgeIATA />
      <AgeCodigo>UV01</AgeCodigo>
      <DepositAmount>2204.0000</DepositAmount>
      <AltDepositAmount>2204.0000</AltDepositAmount>
      <HotelPhoneNumber>449-971-08-45</HotelPhoneNumber>
      <IdIdiomaReservation>1</IdIdiomaReservation>
      <DepositInfo>     &lt;p style="text-align: justify"&gt;Contacte al hotel para datos del dep&amp;oacute;sito bancario. La Reservaci&amp;oacute;n ser&amp;aacute; garantizada hasta que se realice el dep&amp;oacute;sito y se notifique al hotel.&lt;br /&gt; Tel&amp;eacute;fono: 01 + 449-971-0845 &amp;oacute; &lt;a href="mailto:ventasags@hotelesmision.com.mx"&gt;ventasags@hotelesmision.com.mx&lt;/a&gt;&lt;/p&gt; </DepositInfo>
      <DepositTarget>HTL</DepositTarget>
      <DepositReference>3057114</DepositReference>
      <NoConfGalileo />
      <PackageItinerary />
      <AccessCode />
      <IdAgency>1494</IdAgency>
      <FrecuentCode />
      <RatePlanName>Tarifa Estándar Prueba</RatePlanName>
      <RatePlanDescription>Solo Habitación</RatePlanDescription>
      <CancelationPolicies>En temporada regular cxl hasta 24 horas antes de la fecha de llegada del huésped al hotel para evitar cargos.
En temporada de Feria de San Marcos 8 días antes de la fecha de llegada.</CancelationPolicies>
      <CancelPrior>24-H</CancelPrior>
      <RuleMinStay>1</RuleMinStay>
      <RuleMaxStay>254</RuleMaxStay>
      <RuleNoarrival />
      <RuleAdvBook>1</RuleAdvBook>
      <EstateName>Aguascalientes</EstateName>
      <ZipCode>20329</ZipCode>
      <IsNetRateUV>false</IsNetRateUV>
      <CreditCardPolicies>Se aceptan AX, CA y VI.</CreditCardPolicies>
      <GuaranteePolicies>Tarjeta de crédito o una noche de depósito se requiere para garantizar la reservación.</GuaranteePolicies>
      <ExtraChargesPolicies xml:space="preserve"> </ExtraChargesPolicies>
      <ReservedBySpecificPayment>False</ReservedBySpecificPayment>
      <PaymentInformation />
      <DepositLimitDate>20171009</DepositLimitDate>
      <DepositLimitTime>12:00</DepositLimitTime>
      <CommentByVendor>Vendor reservación de prueba</CommentByVendor>
      <CxWithError>False</CxWithError>
      <CancellationReason />
      <CodigoReferenciaCCT />
      <NoAutorizacion>42424242 </NoAutorizacion>
      <OnlinePayment>False</OnlinePayment>
      <ReservedByPayPal>False</ReservedByPayPal>
      <PayPalReceiptNo />
      <ReservedByPayU>False</ReservedByPayU>
      <ReservedByAmericanExpress>False</ReservedByAmericanExpress>
      <PaymentSource>0</PaymentSource>
    </Reservation>
    <Rooms>
      <Room>
        <Children>0</Children>
        <Adults>1</Adults>
        <Preferences>Special reservación de prueba</Preferences>
        <idtraveler>0</idtraveler>
        <ExtraAdults>0</ExtraAdults>
        <ExtraChildren>0</ExtraChildren>
        <ChildrenAges />
        <idRoomType>6118</idRoomType>
        <NameRoom>Habitación con 1 Cama King</NameRoom>
        <RatePlanName>Tarifa Estándar Prueba</RatePlanName>
        <TravelerName>LUIS COTA</TravelerName>
        <EmailCliente>luis@internetpowerhotel.com</EmailCliente>
        <HomePhone>612+1999280</HomePhone>
        <WorkHome />
        <Address>Nicolás Bravo 125</Address>
        <TravelerCountry>MX</TravelerCountry>
        <TravelerZipCode>23000</TravelerZipCode>
        <TravelerState>Baja California Sur</TravelerState>
        <TravelerCity>La Paz</TravelerCity>
        <TravelerFirstName>LUIS</TravelerFirstName>
        <TravelerLastName>COTA</TravelerLastName>
        <Rates>
          <Rate>
            <Date>2017/10/10</Date>
            <AdultRate>950</AdultRate>
            <ChildRate>0</ChildRate>
            <ExtraRateAdult>0</ExtraRateAdult>
            <ExtraRateChild>0</ExtraRateChild>
            <AltRateAdult>950</AltRateAdult>
            <AltRateChild>0</AltRateChild>
            <AltExtraRateAdult>0</AltExtraRateAdult>
            <AltExtraRateChild>0</AltExtraRateChild>
            <idSeasson>0</idSeasson>
          </Rate>
          <Rate>
            <Date>2017/10/11</Date>
            <AdultRate>950</AdultRate>
            <ChildRate>0</ChildRate>
            <ExtraRateAdult>0</ExtraRateAdult>
            <ExtraRateChild>0</ExtraRateChild>
            <AltRateAdult>950</AltRateAdult>
            <AltRateChild>0</AltRateChild>
            <AltExtraRateAdult>0</AltExtraRateAdult>
            <AltExtraRateChild>0</AltExtraRateChild>
            <idSeasson>0</idSeasson>
          </Rate>
        </Rates>
      </Room>
    </Rooms>
    <HotelHeader>
      <Source>CCT</Source>
      <IdPortal>102</IdPortal>
    </HotelHeader>
    <WizcomData>
      <TypeMessage>A</TypeMessage>
    </WizcomData>
  </HotelDisplay>
</Hotel_Display_RS>



-------------------------------------------
RQ
14/03/2017 02:40:53 p. m.
Version: 1.0.4.11
-------------------------------------------
<reqHotelComplete>
  <CompleteAvailability>
    <HotelHeader>
      <CheckinDate>20171010</CheckinDate>
      <CheckoutDate>20171012</CheckoutDate>
      <Language>ES</Language>
      <AccessCode />
      <IdPortal />
      <ServiceProvider>0</ServiceProvider>
    </HotelHeader>
    <HotelFilter>
      <Segment>P</Segment>
    </HotelFilter>
    <HotelFilter>
      <Segment>R</Segment>
    </HotelFilter>
    <HotelFilter>
      <Segment>C</Segment>
    </HotelFilter>
    <HotelFilter>
      <Segment>K</Segment>
    </HotelFilter>
    <HotelFilter>
      <Segment>O</Segment>
    </HotelFilter>
    <HotelRequests>
      <HotelRequest>
        <PropertyNumber>1978</PropertyNumber>
      </HotelRequest>
    </HotelRequests>
    <HotelRooms>
      <Room>
        <Adults>1</Adults>
        <Children>0</Children>
      </Room>
    </HotelRooms>
  </CompleteAvailability>
</reqHotelComplete>



-------------------------------------------
RS
14/03/2017 02:40:58 p. m.
Version: 1.0.4.11
-------------------------------------------
<Hotel_HOC_RS>
  <CompleteAvailability>
    <Property>
      <PropertyNumber>1978</PropertyNumber>
      <Currency>MXN</Currency>
      <TaxIncluded>Y</TaxIncluded>
      <Tax>16.0000</Tax>
      <ServiceProvider>0</ServiceProvider>
      <CityCode>AGU</CityCode>
      <ChainCode>UV</ChainCode>
      <AltCurrency>MXN</AltCurrency>
      <RoomType>
        <RoomCode>D2D</RoomCode>
        <RoomName>Habitación Doble</RoomName>
        <RoomDescription>     &lt;p&gt;&amp;quot;Room only                                                                                            &lt;/p&gt; &lt;p&gt;&amp;quot;,&amp;quot;currency&amp;quot;:&amp;quot;&lt;/p&gt; </RoomDescription>
        <RatePlan>
          <PlanCode>D2DRAC</PlanCode>
          <PlanName>Tarifa Estándar Prueba</PlanName>
          <PlanDescription>Solo Habitación</PlanDescription>
          <AdvBooking>1</AdvBooking>
          <MinDays>1</MinDays>
          <MaxDays>254</MaxDays>
          <RatesVariation>0</RatesVariation>
          <OnlyFirstDay>0</OnlyFirstDay>
          <Available>Y</Available>
          <Message />
          <DaysFree>3</DaysFree>
          <DescPromotion>5.0000</DescPromotion>
          <MaxAdults />
          <MaxChildren />
          <TotalStay>2204</TotalStay>
          <AvgRate>1102</AvgRate>
          <Segment>R</Segment>
          <GuarDep>G</GuarDep>
          <PackPrice>1160</PackPrice>
          <PackTypePrice>N</PackTypePrice>
          <TotalAvgRate>1160</TotalAvgRate>
          <TotalStayDay>2320</TotalStayDay>
          <AltTotalStay>2204</AltTotalStay>
          <AltAvgRate>1102</AltAvgRate>
          <AltPackPrice>1160</AltPackPrice>
          <AltTotalAvgRate>1160</AltTotalAvgRate>
          <AltTotalStayDay>2320</AltTotalStayDay>
          <AgencyPercent>0</AgencyPercent>
          <AccessCode />
          <isUvNetRate>false</isUvNetRate>
          <Currency>MXN</Currency>
          <AltCurrency>MXN</AltCurrency>
          <CompanyContract />
          <ReferenceContract />
          <TypeContract />
          <Price>
            <Day>2017/10/10</Day>
            <Total>1160</Total>
            <Total2>1102</Total2>
            <TotalExt>0</TotalExt>
            <TotalDay>1160</TotalDay>
            <AltTotal>1160</AltTotal>
            <AltTotal2>1102</AltTotal2>
            <AltTotalExt>0</AltTotalExt>
            <AltTotalDay>1160</AltTotalDay>
            <Available>Y</Available>
          </Price>
          <Price>
            <Day>2017/10/11</Day>
            <Total>1160</Total>
            <Total2>1102</Total2>
            <TotalExt>0</TotalExt>
            <TotalDay>1160</TotalDay>
            <AltTotal>1160</AltTotal>
            <AltTotal2>1102</AltTotal2>
            <AltTotalExt>0</AltTotalExt>
            <AltTotalDay>1160</AltTotalDay>
            <Available>Y</Available>
          </Price>
        </RatePlan>
        <Room>
          <Adults>1</Adults>
          <Children>0</Children>
          <ExtAdults>0</ExtAdults>
          <ExtChildren>0</ExtChildren>
        </Room>
      </RoomType>
      <RoomType>
        <RoomCode>D1K</RoomCode>
        <RoomName>Habitación con 1 Cama King</RoomName>
        <RoomDescription>     &lt;p&gt; Contamos con Habitaciones aencillas, con 1 cama king y capacidad máxima para 2 personas. Las habitaciones cuentan con aire acondicionado, TV con 26 canales, secadora de cabello. La ocupación puede ser sencilla, doble, triple y cuádruple. Los siguientes servicios son sin costo solicitándolos a recepción: llamadas locales, periódico, internet, tabla de planchado y plancha a solicitud.&lt;br /&gt;  &lt;/p&gt; </RoomDescription>
        <RatePlan>
          <PlanCode>D1KRAC</PlanCode>
          <PlanName>Tarifa Estándar Prueba</PlanName>
          <PlanDescription>Solo Habitación</PlanDescription>
          <AdvBooking>1</AdvBooking>
          <MinDays>1</MinDays>
          <MaxDays>254</MaxDays>
          <RatesVariation>0</RatesVariation>
          <OnlyFirstDay>0</OnlyFirstDay>
          <Available>Y</Available>
          <Message />
          <DaysFree>3</DaysFree>
          <DescPromotion>5.0000</DescPromotion>
          <MaxAdults />
          <MaxChildren />
          <TotalStay>2204</TotalStay>
          <AvgRate>1102</AvgRate>
          <Segment>R</Segment>
          <GuarDep>G</GuarDep>
          <PackPrice>1160</PackPrice>
          <PackTypePrice>N</PackTypePrice>
          <TotalAvgRate>1160</TotalAvgRate>
          <TotalStayDay>2320</TotalStayDay>
          <AltTotalStay>2204</AltTotalStay>
          <AltAvgRate>1102</AltAvgRate>
          <AltPackPrice>1160</AltPackPrice>
          <AltTotalAvgRate>1160</AltTotalAvgRate>
          <AltTotalStayDay>2320</AltTotalStayDay>
          <AgencyPercent>0</AgencyPercent>
          <AccessCode />
          <isUvNetRate>false</isUvNetRate>
          <Currency>MXN</Currency>
          <AltCurrency>MXN</AltCurrency>
          <CompanyContract />
          <ReferenceContract />
          <TypeContract />
          <Price>
            <Day>2017/10/10</Day>
            <Total>1160</Total>
            <Total2>1102</Total2>
            <TotalExt>0</TotalExt>
            <TotalDay>1160</TotalDay>
            <AltTotal>1160</AltTotal>
            <AltTotal2>1102</AltTotal2>
            <AltTotalExt>0</AltTotalExt>
            <AltTotalDay>1160</AltTotalDay>
            <Available>Y</Available>
          </Price>
          <Price>
            <Day>2017/10/11</Day>
            <Total>1160</Total>
            <Total2>1102</Total2>
            <TotalExt>0</TotalExt>
            <TotalDay>1160</TotalDay>
            <AltTotal>1160</AltTotal>
            <AltTotal2>1102</AltTotal2>
            <AltTotalExt>0</AltTotalExt>
            <AltTotalDay>1160</AltTotalDay>
            <Available>Y</Available>
          </Price>
        </RatePlan>
        <Room>
          <Adults>1</Adults>
          <Children>0</Children>
          <ExtAdults>0</ExtAdults>
          <ExtChildren>0</ExtChildren>
        </Room>
      </RoomType>
    </Property>
  </CompleteAvailability>
</Hotel_HOC_RS>



-------------------------------------------
RQ
14/03/2017 02:41:38 p. m.
Version: 1.0.4.11
-------------------------------------------
<reqHotelModifyNoRestrictions>
  <Client>
    <Name>LUIS</Name>
    <LastName>COTA</LastName>
    <Address>Nicolás Bravo 125</Address>
    <ZipCode>23000</ZipCode>
    <Country>MX</Country>
    <State>Baja California Sur</State>
    <City>La Paz</City>
    <Phone>612+1999280</Phone>
    <ExtraPhone />
    <Email>luis@internetpowerhotel.com</Email>
    <Code />
    <TipoCC>4</TipoCC>
    <NameCC>Luis/Cota</NameCC>
    <NumberCC>4242424242424242</NumberCC>
    <DigitCC>424</DigitCC>
    <ExpireMonthCC>10</ExpireMonthCC>
    <ExpireYearCC>2019</ExpireYearCC>
  </Client>
  <Reservation>
    <ConfirmNumber>305711</ConfirmNumber>
    <CheckInDate>10/10/2017</CheckInDate>
    <CheckOutDate>12/10/2017</CheckOutDate>
    <PropertyNumber>1978</PropertyNumber>
    <Total>2204</Total>
    <TotalNR />
    <RateCode>D1KRAC</RateCode>
    <CodeRoom>D1K</CodeRoom>
    <ExtRatePrice>0</ExtRatePrice>
    <ExtRatePriceNR>0</ExtRatePriceNR>
    <CommentByVendor>Vendor reservación de prueba</CommentByVendor>
    <BanorteMonths>n</BanorteMonths>
    <CreditCard>n</CreditCard>
    <BankDeposit>n</BankDeposit>
    <AmericanExpress>n</AmericanExpress>
    <PayPal>n</PayPal>
    <PayU>n</PayU>
    <NoAutorizacion>42424242</NoAutorizacion>
  </Reservation>
  <Rooms>
    <Room>
      <RatePrice>950</RatePrice>
      <RatePriceNR>0</RatePriceNR>
      <Children>0</Children>
      <Adults>1</Adults>
      <AdultsExtra>0</AdultsExtra>
      <ChildrenExtra>0</ChildrenExtra>
      <Preferences>Special reservación de prueba</Preferences>
      <ChildrenAges />
    </Room>
  </Rooms>
</reqHotelModifyNoRestrictions>



-------------------------------------------
RQ
14/03/2017 02:42:40 p. m.
Version: 1.0.4.11
-------------------------------------------
<Passport_GetReservations_RQ>
  <Request>
    <idUsuario>23055</idUsuario>
    <Fecha1>2017/03/13</Fecha1>
    <Fecha2>2017/03/14</Fecha2>
    <Rubro>10</Rubro>
    <Type>1</Type>
    <SourceList />
    <SegmentoEmpresa />
    <idUsuarioCallCenter>23055</idUsuarioCallCenter>
  </Request>
</Passport_GetReservations_RQ>



-------------------------------------------
RQ
14/03/2017 02:54:23 p. m.
Version: 1.0.4.11
-------------------------------------------
<Passport_GetReservations_RQ>
  <Request>
    <idUsuario>23055</idUsuario>
    <Fecha1>2017/03/13</Fecha1>
    <Fecha2>2017/03/14</Fecha2>
    <Rubro>10</Rubro>
    <Type>1</Type>
    <SourceList />
    <SegmentoEmpresa />
    <idUsuarioCallCenter>23055</idUsuarioCallCenter>
  </Request>
</Passport_GetReservations_RQ>



-------------------------------------------
RS
14/03/2017 02:54:24 p. m.
Version: 1.0.4.11
-------------------------------------------
<Passport_GetReservations_RS>
  <Response>
    <idReservacion>305711</idReservacion>
    <Ciudad>Aguascalientes</Ciudad>
    <Pais>MX</Pais>
    <idRubro>10</idRubro>
    <NoReservacion>305711</NoReservacion>
    <CheckIn>2017-10-10T00:00:00-06:00</CheckIn>
    <CheckOut>2017-10-12T00:00:00-06:00</CheckOut>
    <status>4</status>
    <nombre>Hotel Misión Aguascalientes Sur </nombre>
    <Cliente>LUIS COTA</Cliente>
    <PortalName>Hotels Mision - Callcenter</PortalName>
    <PortalId>102</PortalId>
    <Source>CCT</Source>
    <EmailCliente />
    <Noches>2</Noches>
    <FechaRegistro>2017-03-13T14:27:00-07:00</FechaRegistro>
    <Habitaciones>1 - Standard Room 1 King Bed</Habitaciones>
    <Segmento />
    <PendientesPMS>0</PendientesPMS>
    <Total>2204.0000</Total>
    <codigopostal>23000</codigopostal>
    <tarifa>1102.0000</tarifa>
    <CiudadCliente>La Paz</CiudadCliente>
    <EstadoCliente>Baja California Sur</EstadoCliente>
    <Subtotal>1900.0000</Subtotal>
    <Impuesto>304.0000</Impuesto>
  </Response>
</Passport_GetReservations_RS>



-------------------------------------------
RQ
14/03/2017 02:54:27 p. m.
Version: 1.0.4.11
-------------------------------------------
<?xml version="1.0" encoding="utf-16"?><reqHotelDisplay><HotelDisplay><ChainCode></ChainCode><ConfirmNumber></ConfirmNumber><Source>CCT</Source><Language>ES</Language><AltCurrency></AltCurrency><PropertyNumber></PropertyNumber><ReservationId>305711</ReservationId><Corporativo></Corporativo></HotelDisplay></reqHotelDisplay>



-------------------------------------------
RS
14/03/2017 02:54:28 p. m.
Version: 1.0.4.11
-------------------------------------------
<Hotel_Display_RS>
  <HotelDisplay>
    <Customer>
      <PhoneHome />
      <LastName />
      <FirstName />
      <UserId>23055</UserId>
      <Estate>Baja California Sur</Estate>
      <City>La Paz</City>
    </Customer>
    <Reservation>
      <CheckInDate>2017/10/10</CheckInDate>
      <CheckOutDate>2017/10/12</CheckOutDate>
      <PropertyNumber>1978</PropertyNumber>
      <ChainCode>UV</ChainCode>
      <CreditCardExpiration>102019</CreditCardExpiration>
      <CreditCardHolder>Luis/Cota</CreditCardHolder>
      <CreditCardNumber />
      <CreditCardNumberVerify>****</CreditCardNumberVerify>
      <RateCode>D1KRAC</RateCode>
      <CreditCardType>VI</CreditCardType>
      <RAwayAdultPrice>0</RAwayAdultPrice>
      <RAwayChildPrice>0</RAwayChildPrice>
      <RAwayCribPrice>0</RAwayCribPrice>
      <Total>2204</Total>
      <Taxes>304</Taxes>
      <ConfirmNumber>305711</ConfirmNumber>
      <CXNumber />
      <Status>4</Status>
      <GuarDep>G</GuarDep>
      <RatePlan>RAC</RatePlan>
      <PlusTax>False</PlusTax>
      <StatusDate>2017/03/13 14:27</StatusDate>
      <ReservationDate>2017/03/13</ReservationDate>
      <RAwayAdult>0</RAwayAdult>
      <RAwayChild>0</RAwayChild>
      <RAwayCrib>0</RAwayCrib>
      <HotelName>Hotel Misión Aguascalientes Sur </HotelName>
      <CityName>Aguascalientes</CityName>
      <CountryName>MX</CountryName>
      <RecLoc />
      <Provider>0</Provider>
      <EmailReservations>josealberto@internetpowerhotel.com,josealberto@univisit.com</EmailReservations>
      <HotelAddress>Blvd. José María Chávez # 2100. Ciudad Industrial</HotelAddress>
      <Money>MXN</Money>
      <ServiceFee>0</ServiceFee>
      <CompanyId>6972</CompanyId>
      <HotelCheckin>1500</HotelCheckin>
      <HotelCheckout>1300</HotelCheckout>
      <AltMoney>MXN</AltMoney>
      <AltTotal>2204</AltTotal>
      <AltTaxes>304</AltTaxes>
      <ImageLogo>https://crs.univisit.com/global/ImagesSystem/Logos/LogoCompany_6972</ImageLogo>
      <StatusConf>Y</StatusConf>
      <AgeNombre>UNIVISIT CALL CENTER</AgeNombre>
      <AgeDom>5 DE MAYO</AgeDom>
      <AgeCiudad>La Paz</AgeCiudad>
      <AgeEstado>Baja California Sur</AgeEstado>
      <AgePais>MEXICO</AgePais>
      <AgeIATA />
      <AgeCodigo>UV01</AgeCodigo>
      <DepositAmount>2204.0000</DepositAmount>
      <AltDepositAmount>2204.0000</AltDepositAmount>
      <HotelPhoneNumber>449-971-08-45</HotelPhoneNumber>
      <IdIdiomaReservation>1</IdIdiomaReservation>
      <DepositInfo>     &lt;p style="text-align: justify"&gt;Contacte al hotel para datos del dep&amp;oacute;sito bancario. La Reservaci&amp;oacute;n ser&amp;aacute; garantizada hasta que se realice el dep&amp;oacute;sito y se notifique al hotel.&lt;br /&gt; Tel&amp;eacute;fono: 01 + 449-971-0845 &amp;oacute; &lt;a href="mailto:ventasags@hotelesmision.com.mx"&gt;ventasags@hotelesmision.com.mx&lt;/a&gt;&lt;/p&gt; </DepositInfo>
      <DepositTarget>HTL</DepositTarget>
      <DepositReference>3057114</DepositReference>
      <NoConfGalileo />
      <PackageItinerary />
      <AccessCode />
      <IdAgency>1494</IdAgency>
      <FrecuentCode />
      <RatePlanName>Tarifa Estándar Prueba</RatePlanName>
      <RatePlanDescription>Solo Habitación</RatePlanDescription>
      <CancelationPolicies>En temporada regular cxl hasta 24 horas antes de la fecha de llegada del huésped al hotel para evitar cargos.
En temporada de Feria de San Marcos 8 días antes de la fecha de llegada.</CancelationPolicies>
      <CancelPrior>24-H</CancelPrior>
      <RuleMinStay>1</RuleMinStay>
      <RuleMaxStay>254</RuleMaxStay>
      <RuleNoarrival />
      <RuleAdvBook>1</RuleAdvBook>
      <EstateName>Aguascalientes</EstateName>
      <ZipCode>20329</ZipCode>
      <IsNetRateUV>false</IsNetRateUV>
      <CreditCardPolicies>Se aceptan AX, CA y VI.</CreditCardPolicies>
      <GuaranteePolicies>Tarjeta de crédito o una noche de depósito se requiere para garantizar la reservación.</GuaranteePolicies>
      <ExtraChargesPolicies xml:space="preserve"> </ExtraChargesPolicies>
      <ReservedBySpecificPayment>False</ReservedBySpecificPayment>
      <PaymentInformation />
      <DepositLimitDate>20171009</DepositLimitDate>
      <DepositLimitTime>12:00</DepositLimitTime>
      <CommentByVendor>Vendor reservación de prueba</CommentByVendor>
      <CxWithError>False</CxWithError>
      <CancellationReason />
      <CodigoReferenciaCCT />
      <NoAutorizacion>42424242 </NoAutorizacion>
      <OnlinePayment>False</OnlinePayment>
      <ReservedByPayPal>False</ReservedByPayPal>
      <PayPalReceiptNo />
      <ReservedByPayU>False</ReservedByPayU>
      <ReservedByAmericanExpress>False</ReservedByAmericanExpress>
      <PaymentSource>0</PaymentSource>
    </Reservation>
    <Rooms>
      <Room>
        <Children>0</Children>
        <Adults>1</Adults>
        <Preferences>Special reservación de prueba</Preferences>
        <idtraveler>0</idtraveler>
        <ExtraAdults>0</ExtraAdults>
        <ExtraChildren>0</ExtraChildren>
        <ChildrenAges />
        <idRoomType>6118</idRoomType>
        <NameRoom>Habitación con 1 Cama King</NameRoom>
        <RatePlanName>Tarifa Estándar Prueba</RatePlanName>
        <TravelerName>LUIS COTA</TravelerName>
        <EmailCliente>luis@internetpowerhotel.com</EmailCliente>
        <HomePhone>612+1999280</HomePhone>
        <WorkHome />
        <Address>Nicolás Bravo 125</Address>
        <TravelerCountry>MX</TravelerCountry>
        <TravelerZipCode>23000</TravelerZipCode>
        <TravelerState>Baja California Sur</TravelerState>
        <TravelerCity>La Paz</TravelerCity>
        <TravelerFirstName>LUIS</TravelerFirstName>
        <TravelerLastName>COTA</TravelerLastName>
        <Rates>
          <Rate>
            <Date>2017/10/10</Date>
            <AdultRate>950</AdultRate>
            <ChildRate>0</ChildRate>
            <ExtraRateAdult>0</ExtraRateAdult>
            <ExtraRateChild>0</ExtraRateChild>
            <AltRateAdult>950</AltRateAdult>
            <AltRateChild>0</AltRateChild>
            <AltExtraRateAdult>0</AltExtraRateAdult>
            <AltExtraRateChild>0</AltExtraRateChild>
            <idSeasson>0</idSeasson>
          </Rate>
          <Rate>
            <Date>2017/10/11</Date>
            <AdultRate>950</AdultRate>
            <ChildRate>0</ChildRate>
            <ExtraRateAdult>0</ExtraRateAdult>
            <ExtraRateChild>0</ExtraRateChild>
            <AltRateAdult>950</AltRateAdult>
            <AltRateChild>0</AltRateChild>
            <AltExtraRateAdult>0</AltExtraRateAdult>
            <AltExtraRateChild>0</AltExtraRateChild>
            <idSeasson>0</idSeasson>
          </Rate>
        </Rates>
      </Room>
    </Rooms>
    <HotelHeader>
      <Source>CCT</Source>
      <IdPortal>102</IdPortal>
    </HotelHeader>
    <WizcomData>
      <TypeMessage>A</TypeMessage>
    </WizcomData>
  </HotelDisplay>
</Hotel_Display_RS>