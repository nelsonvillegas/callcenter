Public Class configurarAgente

    Private email_tipo As Boolean

    Private Sub configurarAgente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
        CapaPresentacion.Idiomas.cambiar_configurarAgente(Me)
    End Sub

    Private Sub btn_guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_guardar.Click
        has_dataSave()
    End Sub

    Private Sub btn_cambiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cambiar.Click
        has_passUpdate()
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    '--------------

    Private Sub losTXT_pass_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_passNew2.KeyDown, txt_passNew.KeyDown, txt_passActual.KeyDown
        If e.KeyCode = Keys.Enter Then
            has_passUpdate()
        End If
    End Sub

    Private Sub losTXT_data_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_correo.KeyDown, txt_agente.KeyDown, txt_agencia.KeyDown
        If e.KeyCode = Keys.Enter Then
            has_dataSave()
        End If
    End Sub

    '--------------

    Private Sub has_passUpdate()
        If txt_passActual.Text = "" Or txt_passNew.Text = "" Or txt_passNew2.Text = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0173_campReq"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If txt_passNew.Text <> txt_passNew2.Text Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0178_errPass"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If ws_login.UserChangePassword(txt_passActual.Text, txt_passNew.Text) Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0180_okCamCont"), CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0179_errCamCont"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub has_dataSave()
        If txt_correo.Text = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0173_campReq"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If ws_login.UserChangeEmail(txt_correo.Text, email_tipo) Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0181_infoActualizada"), CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        Else
            MessageBox.Show(ws_login.LastMessage, CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    '--------------

    Private Sub load_data()
        Try
            Dim r_usr As ResUserLogin.UserAccountRow = ws_login.getUserAccessInfo()
            txt_correo.Text = r_usr.Item("Email")
            email_tipo = CBool(r_usr.Item("EmailType"))
        Catch ex As Exception
            MessageBox.Show(ex.Message, CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

End Class