Imports System.Data.SqlClient
Imports callCenter.CapaLogicaNegocios
Imports callCenter.CapaLogicaNegocios.ErrorManager
Imports System.Management
Imports System.Security.Principal

Public Class login

    '----------------move windows
    Private Const WM_SYSCOMMAND As Integer = &H112&
    Private Const MOUSE_MOVE As Integer = &HF012&
    Dim instanciaUsuario As Boolean = True
    '
    ' Declaraciones del API al estilo .NET
    <System.Runtime.InteropServices.DllImport("user32.DLL", EntryPoint:="ReleaseCapture")> _
    Private Shared Sub ReleaseCapture()
    End Sub
    '
    <System.Runtime.InteropServices.DllImport("user32.DLL", EntryPoint:="SendMessage")> _
    Private Shared Sub SendMessage(ByVal hWnd As System.IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As Integer)
    End Sub

    Private Sub moverForm()
        ReleaseCapture()
        SendMessage(Me.Handle, WM_SYSCOMMAND, MOUSE_MOVE, 0)
    End Sub

    Private Function IsAdmin() As Boolean
        Dim id As WindowsIdentity = WindowsIdentity.GetCurrent()
        Dim principal As WindowsPrincipal = New WindowsPrincipal(id)
        Return principal.IsInRole(WindowsBuiltInRole.Administrator)
    End Function

    Private Function getWindowsName() As String
        Dim os_query As String = "SELECT * FROM " & _
             "Win32_OperatingSystem"
        Dim os_searcher As New ManagementObjectSearcher(os_query)
        For Each info As ManagementObject In os_searcher.Get()
            Return info.Properties("Caption").Value.ToString().Trim
        Next info
        Return ""
    End Function


    Private Sub RunAsAdministrator()
        If getWindowsName() = "Windows XP" Then

        Else
            If Not IsAdmin() Then
                Try
                    Dim procInfo As ProcessStartInfo = New ProcessStartInfo()
                    procInfo.UseShellExecute = True
                    procInfo.WorkingDirectory = Environment.CurrentDirectory
                    procInfo.FileName = Application.ExecutablePath
                    procInfo.Verb = "runas"

                    Try
                        Process.Start(procInfo)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "Process")
                    End Try
                    Application.Exit()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        End If
    End Sub


    '----------------destinos update

    Private Sub login_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Dim l As New webService_call
        ' Dim rs As Call_SegmentsCompany_Insert_RS = l.InsertSegmentCompany("test3", Nothing, 1, 0)
        'l.LogInsert(0, "", "test", "", "")
        'Dim d As Call_Log_Detail_RS = l.LogDetail(1)
        'Dim r As Call_Log_Get_RS = (New webService_call).LogGet(Nothing, Nothing, Nothing, Nothing, 1)
        'Dim ds As DataSet = l.ReportProduction(Now.AddDays(-10), Now, 1, "", 1, "", "", "", "", True)
        'If ds.Tables.Count = 0 Then

        'End If
        'If Not instanciaUsuario Then RunAsAdministrator()
        CapaPresentacion.Idiomas.cambiar_login(Me)
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER = "https://crs.univisit.com/rewards/wsRewards_1_0.asmx"
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER = "UvRewards"
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD = "0000"
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB = ""

        txt_usuario.Text = CapaAccesoDatos.XML.localData.Email

        CapaPresentacion.Idiomas.cultura = CapaAccesoDatos.XML.localData.Idioma
        moneda_selected = CapaAccesoDatos.XML.localData.Moneda
        If CapaAccesoDatos.XML.localData.Imagen <> "" And IO.File.Exists(CapaAccesoDatos.XML.localData.Imagen) Then
            'PictureBox1.Image = Image.FromFile(herramientas.localData.Imagen)

            Using ms As New IO.MemoryStream(IO.File.ReadAllBytes(CapaAccesoDatos.XML.localData.Imagen))
                PictureBox1.Image = Image.FromStream(ms)
            End Using

            UltraGroupBox1.Text += " - " + CapaAccesoDatos.XML.localData.NombreSistema

            Me.Height = 283
        End If

        lbl_mesnsage.Text = ""
        lbl_version.Text = "v" + My.Application.Info.Version.ToString

        If Not version_programadores Then
            stop_controls()
            lbl_mesnsage.Visible = True
            lbl_mesnsage.Text = CapaPresentacion.Idiomas.get_str("str_350_buscActu")
            miThread_updates = New System.Threading.Thread(AddressOf obten_updates)
            miThread_updates.Start()
        Else
            MsgBox("Version para programadores")
        End If
    End Sub

    'Private Sub lee_correo()
    '    Try
    '        If Not System.IO.File.Exists("correo.txt") Then Return
    '        Dim f As System.IO.FileStream = System.IO.File.OpenRead("correo.txt")
    '        Dim b(1024) As Byte
    '        Dim temp As System.Text.UTF8Encoding = New System.Text.UTF8Encoding(True)
    '        Dim txt As String = ""
    '        Do While f.Read(b, 0, b.Length) > 0
    '            txt = temp.GetString(b)
    '        Loop
    '        f.Close()

    '        txt_usuario.Text = ""
    '        For Each c As Char In txt.ToCharArray
    '            If c = Nothing Then Exit For

    '            txt_usuario.Text += CStr(c)
    '        Next
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub stop_controls()
        Fija_Enabled(PictureBox2, True, True)
        Fija_Enabled(txt_usuario, False, True)
        Fija_Enabled(txt_password, False, True)
        Fija_Enabled(btn_entrar, False, True)
        Fija_Enabled(btn_cerrar, False, True)
        Fija_Enabled(btn_configurar, False, True)
    End Sub

    Private Sub obten_updates()
        If busca_updates Then
            busca_updates = False

            Dim res As Boolean
            If instanciausuario Then
                res = True
            Else
                res = CapaAccesoDatos.DB.try_conn("master")
            End If


            If Not res Then
                'CapaLogicaNegocios.ErrorManager.Manage("E0093", "", CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
            Else
                'If Now.DayOfWeek = DayOfWeek.Saturday Then

                'Dim t As DateTime = Now

                'If Not es_ws_du_ok() Then
                '    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0192_confirmWSDU"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Else

                'MsgBox((Now - t).TotalMilliseconds)

                Try
                    If Not instanciaUsuario Then verifica_bd()

                    Dim info_db As info = consultar_info()
                    If info_db Is Nothing Then
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_401_ProcessCompleted"), False)
                        start_controls(True)

                        MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_402_CantVerifyDB") + vbCrLf + CapaPresentacion.Idiomas.get_str("str_403_CallCenterAgain"))
                        Return
                    End If

                    Dim WS_DU As New WS_DESTINOSUPDATE.destinosUpdate_service
                    WS_DU.Url = CapaAccesoDatos.XML.wsData.UrlWsUpdate

                    'Dim re As Boolean = WS_DU.IsClient_Outdated(info_db.fecha.ToString("ddMMyyyy"))
                    Dim fe As String = WS_DU.GetServer_Date()

                    Dim fecha_server As DateTime = DateTime.ParseExact(fe, CapaPresentacion.Common.DateDataFormat, Nothing)

                    Dim desactualizado As Boolean
                    If info_db Is Nothing Then
                        desactualizado = True
                    Else
                        desactualizado = info_db.fecha < fecha_server
                    End If

                    If info_db.fecha.Year <> 1 AndAlso desactualizado AndAlso MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_391_NuevosDestinos"), CapaPresentacion.Idiomas.get_str("str_392_DonwloadDestinations"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_393_DownloadingUpdates"), True)

                        'Inicializaciones y crear directorios
                        Dim zip_ruta_data As String = "data"
                        Dim zip_ruta_file As String = zip_ruta_data + "/data_update.rar"
                        Dim zip_ruta_fold As String = zip_ruta_file.Substring(0, zip_ruta_file.LastIndexOf("."))

                        If Not System.IO.Directory.Exists(zip_ruta_data) Then
                            System.IO.Directory.CreateDirectory(zip_ruta_data)
                        End If

                        If Not System.IO.Directory.Exists(zip_ruta_fold) Then
                            System.IO.Directory.CreateDirectory(zip_ruta_fold)
                        End If

                        'crea archivo zip
                        Dim zip_file As New System.IO.FileStream(zip_ruta_file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite)
                        Dim mybytearray() As Byte = WS_DU.obten_actualizaciones()
                        zip_file.Write(mybytearray, 0, mybytearray.Length)
                        zip_file.Close()
                        zip_file.Dispose()

                        'extrae zip
                        Dim zip As New Ionic.Utils.Zip.ZipFile(zip_ruta_file)
                        zip.ExtractAll(zip_ruta_fold, True)

                        'update datos
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations"), True)

                        If Not CapaAccesoDatos.DB.exe_db_nonQ("ventanaCallCenter", "exec borra_destinos", False) Then Throw New Exception("err del db dest")

                        update_tabla(zip_ruta_fold + "/Aeropuertos.xml", "Aeropuertos")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": " + CapaPresentacion.Idiomas.get_str("str_395_Aeropuertos"), True)

                        update_tabla(zip_ruta_fold + "/Ciudades.xml", "Ciudades")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": " + CapaPresentacion.Idiomas.get_str("str_396_Ciudades"), True)

                        update_tabla(zip_ruta_fold + "/Estados.xml", "Estados")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": " + CapaPresentacion.Idiomas.get_str("str_397_Estados"), True)

                        update_tabla(zip_ruta_fold + "/Municipios.xml", "Municipios")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": " + CapaPresentacion.Idiomas.get_str("str_398_Municiopios"), True)

                        update_tabla(zip_ruta_fold + "/Paises.xml", "Paises")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": " + CapaPresentacion.Idiomas.get_str("str_399_Paises"), True)

                        update_tabla(zip_ruta_fold + "/Zonas.xml", "Zonas")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": " + CapaPresentacion.Idiomas.get_str("str_400_Zonas"), True)

                        update_tabla(zip_ruta_fold + "/PlacesDictionary.xml", "PlacesDictionary")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": PlacesDictionary", True)

                        update_tabla(zip_ruta_fold + "/Areas.xml", "Areas")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": Areas", True)
                        update_tabla(zip_ruta_fold + "/AreasCiudad.xml", "AreasCiudad")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": Areas-Dic", True)
                        update_tabla(zip_ruta_fold + "/Diccionario.xml", "Diccionario")
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations") + ": Dic", True)

                        'borra temp db files
                        Try
                            System.IO.Directory.Delete(zip_ruta_fold, True)
                            System.IO.File.Delete(zip_ruta_file)
                        Catch ex As Exception
                            CapaLogicaNegocios.ErrorManager.Manage("E0094", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                        End Try

                        'actualiza fecha update
                        If Not CapaAccesoDatos.DB.exe_db_nonQ("ventanaCallCenter", "EXEC actualizar_info", False) Then Throw New Exception("err upD db date")
                    End If
                Catch ex As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0095", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                End Try
                'End If
                'End If
            End If
        End If

        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_401_ProcessCompleted"), False)
        start_controls(False)
    End Sub

    'Public Function es_ws_du_ok() As Boolean
    '    Dim res As Boolean = False
    '    Try
    '        Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(CapaAccesoDatos.XML.wsData.UrlWsUpdate)
    '        Dim response As System.Net.HttpWebResponse = request.GetResponse()
    '        If response.StatusCode = System.Net.HttpStatusCode.OK Then
    '            res = True
    '        End If
    '    Catch ex As Exception
    '        CapaLogicaNegocios.LogManager.agregar_registro("es_ws_du_ok - login", ex.Message)
    ' =>       CapaLogicaNegocios.ErrorManager.Manage("E0095", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
    '    End Try

    '    Return res
    'End Function

    Private Sub verifica_bd()
        Try
            Dim existe As Boolean = CBool(CapaAccesoDatos.DB.exe_db_scal("master", "IF EXISTS (select * from sys.databases where name = 'ventanaCallCenter') select 1 ELSE select 0"))
            Dim crear As Boolean = Not existe

            If existe Then
                Dim info_db As info = consultar_info()

                If info_db Is Nothing OrElse info_db.db_version <> CONST_BD_VERSION Then
                    Dim s As String = Environment.CurrentDirectory
                    'If Not s.EndsWith("\Debug") AndAlso Not s.EndsWith("\Release") Then
                    crear = CBool(CapaAccesoDatos.DB.exe_db_nonQ("master", "ALTER DATABASE ventanaCallCenter SET SINGLE_USER WITH ROLLBACK IMMEDIATE; drop database ventanaCallCenter", False))
                    'End If
                Else
                    crear = False
                End If
            End If

            If crear Then
                CapaAccesoDatos.DB.exe_db_nonQ("master", "create database [ventanaCallCenter]", False)

                'While Not existe
                '    existe = CBool(tools_db.exe_db_scal("master", "IF EXISTS (select * from sys.databases where name = 'ventanaCallCenter') select 1 ELSE select 0"))
                '    System.Threading.Thread.Sleep(100)
                'End While

                Dim res As Boolean = CapaAccesoDatos.DB.exe_db_nonQ("master", "data\crea_db.sql", True)  'Global.callCenter.My.Resources.crea_db.Replace(vbCrLf, " ")

                'Threading.Thread.Sleep(8000)
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0096", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub




    Private Function consultar_info() As info
        Try
            Dim ds As DataSet = CapaAccesoDatos.DB.exe_db_fill("ventanaCallCenter", "exec consultar_info")
            Dim info_db As New info
            info_db.db_version = ds.Tables(0).Rows(0).Item("db_version")
            info_db.fecha = ds.Tables(0).Rows(0).Item("fecha")
            If dataOperador.idCorporativo <> "-1" And dataOperador.idCorporativo <> "" And info_db.db_version > 8 Then
                Dim ds2 As DataSet = CapaAccesoDatos.DB.exe_db_fill("ventanaCallCenter", "exec consultar_infocorp @idCorporativo=" & dataOperador.idCorporativo)
                info_db.fecha_catalogos = ds2.Tables(0).Rows(0).Item("fecha")
            End If
            Return info_db
        Catch ex As Exception
            Dim i As Integer = 0

            While i < 30
                Try
                    Dim ds As DataSet = CapaAccesoDatos.DB.exe_db_fill("ventanaCallCenter", "exec consultar_info")
                    Dim info_db As New info
                    info_db.db_version = ds.Tables(0).Rows(0).Item("db_version")
                    info_db.fecha = ds.Tables(0).Rows(0).Item("fecha")
                    If dataOperador.idCorporativo <> "-1" And dataOperador.idCorporativo <> "" And info_db.db_version > 8 Then
                        Dim ds2 As DataSet = CapaAccesoDatos.DB.exe_db_fill("ventanaCallCenter", "exec consultar_infocorp @idCorporativo=" & dataOperador.idCorporativo)
                        info_db.fecha_catalogos = ds2.Tables(0).Rows(0).Item("fecha")
                    End If
                    Return info_db
                Catch ex2 As Exception
                    CapaLogicaNegocios.ErrorManager.Manage("E0097", " While No: " + i.ToString + vbCrLf + ex2.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                    Threading.Thread.Sleep(1000)
                End Try

                i += 1
            End While

            Return Nothing
        End Try
    End Function

    Private Sub update_tabla(ByVal xml_file As String, ByVal tbl As String)

        'carga el xml con los nuevos datos
        Dim xmlD As New System.Xml.XmlDocument
        xmlD.Load(xml_file)

        Dim ds As New DataSet

        Dim datos() As Byte = System.Text.Encoding.UTF8.GetBytes(xmlD.OuterXml)
        Dim mem_stream_dat As New System.IO.MemoryStream(datos, 0, datos.Length)
        ds.ReadXml(mem_stream_dat)
        If ds.Tables.Count > 0 Then
            'actualiza las tablas locales
            Using connection As New System.Data.SqlClient.SqlConnection(CapaAccesoDatos.XML.localData.ServidorSQL) 'tools_db.str_conn("ventanaCallCenter")
                Dim adapter As New System.Data.SqlClient.SqlDataAdapter()
                adapter.SelectCommand = New System.Data.SqlClient.SqlCommand("select * from " + tbl, connection)
                Dim builder As System.Data.SqlClient.SqlCommandBuilder = New System.Data.SqlClient.SqlCommandBuilder(adapter)

                connection.Open()

                Dim new_ds As New DataSet
                adapter.Fill(new_ds)
                new_ds = ds.Copy

                adapter.Update(new_ds)
            End Using
        End If
    End Sub

    Private Sub start_controls(ByVal HalfControls As Boolean)
        Fija_Enabled(PictureBox2, True, False)
        If Not HalfControls Then Fija_Enabled(txt_usuario, True, True)
        If Not HalfControls Then Fija_Enabled(txt_password, True, True)
        If Not HalfControls Then Fija_Enabled(btn_entrar, True, True)
        Fija_Enabled(btn_cerrar, True, True)
        Fija_Enabled(btn_configurar, True, True)
        If Not HalfControls Then Fija_Focus(txt_usuario, txt_password)
    End Sub



    Private Sub obten_updates2(ByVal idCorporativo As String)
        Dim res As Boolean = False
        If instanciaUsuario Then
            res = True
        Else
            res = CapaAccesoDatos.DB.try_conn("master")
        End If


        If Not res Then
            CapaLogicaNegocios.ErrorManager.Manage("E0093", "", CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        Else
            Try
                If Not instanciaUsuario Then verifica_bd()
                Dim info_db As info = consultar_info()
                If info_db Is Nothing Then
                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_401_ProcessCompleted"), False)
                    start_controls(True)

                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_402_CantVerifyDB") + vbCrLf + CapaPresentacion.Idiomas.get_str("str_403_CallCenterAgain"))
                    Return
                End If

                Dim WS_DU As New WS_DESTINOSUPDATE.destinosUpdate_service
                WS_DU.Url = CapaAccesoDatos.XML.wsData.UrlWsUpdate

                Dim fe As String = WS_DU.GetServer_Date_Catalogos(idCorporativo)
                If fe Is Nothing Then fe = Format(Now.AddDays(-1), "yyyyMMddHHmm")
                Dim fecha_server As DateTime
                fecha_server = DateTime.ParseExact(fe, CapaPresentacion.Common.DateDataFormat + IIf(fe.Length = 8, "", "HHmm"), Nothing)



                Dim desactualizado As Boolean
                If info_db Is Nothing Then
                    desactualizado = True
                Else
                    desactualizado = info_db.fecha_catalogos < fecha_server
                End If

                If info_db.fecha.Year <> 1 AndAlso desactualizado AndAlso MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_438_newcatalogs"), CapaPresentacion.Idiomas.get_str("str_392_DonwloadDestinations"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_393_DownloadingUpdates"), True)

                    'Inicializaciones y crear directorios
                    Dim zip_ruta_data As String = "data"
                    Dim zip_ruta_file As String = zip_ruta_data + "/data2_update.rar"
                    Dim zip_ruta_fold As String = zip_ruta_file.Substring(0, zip_ruta_file.LastIndexOf("."))

                    If Not System.IO.Directory.Exists(zip_ruta_data) Then
                        System.IO.Directory.CreateDirectory(zip_ruta_data)
                    End If

                    If Not System.IO.Directory.Exists(zip_ruta_fold) Then
                        System.IO.Directory.CreateDirectory(zip_ruta_fold)
                    End If

                    'crea archivo zip
                    Dim zip_file As New System.IO.FileStream(zip_ruta_file, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite)
                    Dim mybytearray() As Byte = WS_DU.obten_actualizaciones_catalogos(idCorporativo)
                    zip_file.Write(mybytearray, 0, mybytearray.Length)
                    zip_file.Close()
                    zip_file.Dispose()

                    'extrae zip
                    Dim zip As New Ionic.Utils.Zip.ZipFile(zip_ruta_file)
                    zip.ExtractAll(zip_ruta_fold, True)

                    'update datos
                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_394_UpdatingDestinations"), True)

                    If Not CapaAccesoDatos.DB.exe_db_nonQ("ventanaCallCenter", "exec borra_catalogos " & idCorporativo, False) Then Throw New Exception("err del db dest")
                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_433_updcat") + ": " + CapaPresentacion.Idiomas.get_str("str_435_med") & " promo.", True)
                    update_tabla(zip_ruta_fold + "/uvcc_mediospromocion.xml", "uvcc_mediospromocion")

                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_433_updcat") + ": " + CapaPresentacion.Idiomas.get_str("str_435_med"), True)
                    update_tabla(zip_ruta_fold + "/uvcc_medios.xml", "uvcc_medios")

                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_433_updcat") + ": " + CapaPresentacion.Idiomas.get_str("str_434_comp"), True)
                    update_tabla(zip_ruta_fold + "/uvcc_empresas.xml", "uvcc_empresas")

                    Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_433_updcat") + ": " + CapaPresentacion.Idiomas.get_str("str_436_seg"), True)
                    update_tabla(zip_ruta_fold + "/uvcc_segmentos.xml", "uvcc_segmentos")


                    'borra temp db files
                    Try
                        System.IO.Directory.Delete(zip_ruta_fold, True)
                        System.IO.File.Delete(zip_ruta_file)
                    Catch ex As Exception
                        CapaLogicaNegocios.ErrorManager.Manage("E0094", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
                    End Try

                    'actualiza fecha update
                    If Not CapaAccesoDatos.DB.exe_db_nonQ("ventanaCallCenter", "EXEC actualizar_info_catalogos " & dataOperador.idCorporativo, False) Then Throw New Exception("err upD db date")
                End If

            Catch ex As Exception
                CapaLogicaNegocios.ErrorManager.Manage("E0095", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            End Try
            'End If
        End If
        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_401_ProcessCompleted"), False)
        start_controls(False)
    End Sub




    '----------------login

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_entrar.Click
        CapaAccesoDatos.XML.localData.Email = txt_usuario.Text

        Me.Cursor = Cursors.WaitCursor
        stop_controls()

        miThread_login = New Threading.Thread(AddressOf iniciando_sesion)
        miThread_login.Start()
    End Sub

    'Private Sub guarda_correo()
    '    Try
    '        If System.IO.File.Exists("correo.txt") Then System.IO.File.Delete("correo.txt")
    '        Dim f As System.IO.FileStream = System.IO.File.Create("correo.txt")
    '        Dim info As Byte() = New System.Text.UTF8Encoding(True).GetBytes(txt_usuario.Text)
    '        f.Write(info, 0, info.Length)
    '        f.Close()
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub iniciando_sesion()
        Dim entra As Boolean = False

        Try
            Dim ok As Boolean
            ws_login = New webService_passport(ok)
            If Not ok Then
                CapaLogicaNegocios.ErrorManager.Manage("E0098", "ws_login no OK", CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Else
                'FIX:faltaba limpiar datos
                dataOperador.agency_ID = Nothing
                dataOperador.idCorporativo = "-1"
                'dataOperador.idCallcenter = "-1"
                dataOperador.MostrarCatalogos = False
                dataOperador.user = ""
                dataOperador.user_ID = ""
                dataOperador.user_ID_agent_ID = ""
                dataOperador.Hoteles.Clear()
                dataOperador.idPortal = "" 'Se quito el id 2 para que al obtenter tarifas se obtengan todas
                If ws_login.UserLogin(txt_usuario.Text, txt_password.Text) Then
                    If ws_login.IsAgencyCompany OrElse ws_login.IsAgentCorporateCCT OrElse ws_login.IsUserChain OrElse ws_login.IsUserAssociation OrElse version_programadores Then
                        entra = True

                        dataOperador.user_ID = ws_login.UserId
                        dataOperador.user_ID_agent_ID = ws_login.UserId
                        dataOperador.user = txt_usuario.Text
                        dataOperador.pass = txt_password.Text
                        'TODO: pruebas mision
                        'dataOperador.idCorporativo = 1
                        Try
                            Dim res As ResAgencies.AgencyDataTable = ws_login.GetAgencies()
                            If res IsNot Nothing Then dataOperador.agency_ID = res(0).AgencyId
                            If dataOperador.idCorporativo > "0" Then
                                Dim ws As New WS_DESTINOSUPDATE.destinosUpdate_service
                                ws.Url = CapaAccesoDatos.XML.wsData.UrlWsUpdate
                                Dim ds As DataSet
                                ds = ws.obten_empresas_corporativo("10", "", "", "", "", "2", dataOperador.idCorporativo, ws_login.UserId)
                                For Each dr As DataRow In ds.Tables(0).Rows
                                    Dim h As New dataHotel
                                    h.Nombre = dr("NombreEmpresa")
                                    h.Ciudad = dr("Ciudad") & ", " & dr("Estado") & ", " & dr("idPais")
                                    h.Refpoint = dr("Ciudad")
                                    h.IATA = dr("IATA")
                                    h.idHotel = dr("ID")
                                    h.idEmpresa = dr("IdEmpresa")
                                    h.index = dataOperador.Hoteles.Count
                                    dataOperador.Hoteles.Add(h)
                                Next

                            End If
                        Catch ex As Exception
                        End Try
                    Else
                        Fija_TextVisible(lbl_mesnsage, CapaPresentacion.Idiomas.get_str("str_0166_tipoUsuarioIncorrecto"), True)
                    End If
                Else
                    Fija_TextVisible(lbl_mesnsage, ws_login.LastMessage, True)
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
            ' no hacer nada
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0099", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try

        ' If Not entra Then
        Fija_Frm(Me, 2)

        If entra Then
            If dataOperador.idCorporativo = "1" Then Me.obten_updates2(dataOperador.idCorporativo)
            'If dataOperador.idCorporativo <> "11" Then

            'End If
            Select Case dataOperador.idCorporativo
                Case "1"
                    'mision
                    dataOperador.idPortal = "102"
                Case "4"
                    'believe
                    dataOperador.idPortal = "104"
                Case "43"
                    'ZAR
                    dataOperador.idPortal = "188"
                Case "95"
                    dataOperador.idPortal = "418"
            End Select
            If ws_login.IdAsociacion <> Nothing AndAlso ws_login.IdAsociacion = "1" Then
                dataOperador.idPortal = "103"
            End If

            If dataOperador.idCorporativo = "1" Then dataOperador.MostrarCatalogos = True

            'Fija_Frm(frm1, 3)
            Dim d As New Fija_Frm_Callback(AddressOf Fija_Frm)
            Me.Invoke(d, New Object() {frm1, 3})

            Fija_Frm(Me, 1)
        Else
            start_controls(False)
        End If
    End Sub








    '----------------otros
    Dim miThread_login As Threading.Thread
    Dim miThread_updates As Threading.Thread
    Private frm1 As New Form1_callCenter

    Delegate Sub FijaEnabledCallback(ByVal ctl As Control, ByVal enab As Boolean, ByVal visib As Boolean)

    Delegate Sub Fija_TextVisible_Callback(ByVal ctl As Control, ByVal text As String, ByVal visi As Boolean)

    Delegate Sub Fija_Focus_Callback(ByVal ctl1 As Control, ByVal ctl2 As Control)

    Delegate Sub Fija_Frm_Callback(ByVal frm As Form, ByVal action As Integer)

    Private Sub Fija_Enabled(ByVal ctl As Control, ByVal enab As Boolean, ByVal visib As Boolean)
        Try
            If ctl.InvokeRequired Then
                Dim d As New FijaEnabledCallback(AddressOf Fija_Enabled)
                Me.Invoke(d, New Object() {ctl, enab, visib})
            Else
                ctl.Enabled = enab
                ctl.Visible = visib
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0100", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub Fija_TextVisible(ByVal ctl As Control, ByVal text As String, ByVal visi As Boolean)
        Try
            If ctl.InvokeRequired Then
                Dim d As New Fija_TextVisible_Callback(AddressOf Fija_TextVisible)
                Me.Invoke(d, New Object() {ctl, text, visi})
            Else
                ctl.Text = text
                ctl.Visible = visi
                ctl.Refresh()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0101", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub Fija_Focus(ByVal ctl1 As Control, ByVal ctl2 As Control)
        Try
            If ctl1.InvokeRequired Then
                Dim d As New Fija_Focus_Callback(AddressOf Fija_Focus)
                Me.Invoke(d, New Object() {ctl1, ctl2})
            Else
                If ctl1.Text = "" Then ctl1.Focus() Else ctl2.Focus()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0102", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub Fija_Frm(ByVal frm As Form, ByVal action As Integer)
        Try
            If frm.InvokeRequired Then
                Dim d As New Fija_Frm_Callback(AddressOf Fija_Frm)
                Me.Invoke(d, New Object() {frm, action})
            Else
                Select Case action
                    Case 1                      ' cerrar
                        frm.Close()
                    Case 2                      ' cambia cursor a arrow
                        frm.Cursor = Cursors.Arrow
                    Case 3                      ' entrar
                        frm.Show()
                End Select
            End If
        Catch ex As System.Threading.ThreadAbortException
            ' no hacer nada
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0103", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Class info
        Public fecha As DateTime
        Public fecha_catalogos As DateTime
        Public db_version As Integer
    End Class

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cerrar.Click
        Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_configurar.Click
        Dim frm As New Form1_callCenter
        frm.configura = True
        frm.Show()

        Close()
    End Sub

    Private Sub UltraGroupBox1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGroupBox1.MouseMove
        If e.Button = MouseButtons.Left Then
            moverForm()
        End If
    End Sub

    Private Sub PictureBox1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseMove
        If e.Button = MouseButtons.Left Then
            moverForm()
        End If
    End Sub

    Private Sub PictureBox2_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseMove
        If e.Button = MouseButtons.Left Then
            moverForm()
        End If
    End Sub

    Private Sub login_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        PictureBox1.Image = Nothing

        If Not miThread_login Is Nothing Then miThread_login.Abort()
        If Not miThread_updates Is Nothing Then miThread_updates.Abort()
    End Sub

    Private Sub PictureBox3_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox3.MouseMove, lbl_conacyt.MouseMove
        If e.Button = MouseButtons.Left Then
            moverForm()
        End If
    End Sub

End Class