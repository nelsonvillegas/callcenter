Public Class cmbSearch

    Public lista As New List(Of Object)
    Public ListBox1 As ListBox = Nothing
    Private X_Offset As Integer = 0
    Private Y_Offset As Integer = 0

    Public Event llena()
    Public Event ValueChanged()

    Public ReadOnly Property InputText() As String
        Get
            Return TextBox1.Text
        End Get
    End Property

    Private Sub fija_pos()
        ListBox1.Location = New Point(X_Offset, Me.Location.Y + Me.TextBox1.Height + 1 + Y_Offset)
    End Sub

    Private Sub desp(ByVal s As Object, ByVal e As EventArgs) Handles TextBox1.Enter
        If TextBox1.Text.Length >= 3 Then ListBox1.Visible = True
    End Sub

    Private Sub ocul(ByVal s As Object, ByVal e As EventArgs) Handles TextBox1.Leave
        If Not ListBox1.Focused Then
            ListBox1.Visible = False
            RaiseEvent ValueChanged()
        End If

    End Sub

    Private Sub ocul_lb(ByVal s As Object, ByVal e As EventArgs)
        ListBox1.Visible = False
        RaiseEvent ValueChanged()
    End Sub

    Private Sub lb_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ListBox1.Visible Then TextBox1.Text = ListBox1.Text
    End Sub

    Private Sub lb_Keyup(ByVal s As Object, ByVal e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            TextBox1.Focus()
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub lb_MouseDown(ByVal s As Object, ByVal e As MouseEventArgs)
        TextBox1.Focus()
        SendKeys.Send("{TAB}")
    End Sub

    Private Sub set_ListBox(ByVal lb1 As ListBox)
        ListBox1 = lb1
        ListBox1.Visible = False
        ListBox1.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        fija_pos()
        AddHandler ListBox1.LostFocus, AddressOf ocul_lb
        AddHandler ListBox1.KeyUp, AddressOf lb_Keyup
        AddHandler ListBox1.VisibleChanged, AddressOf lb_VisibleChanged
        AddHandler ListBox1.MouseDown, AddressOf lb_MouseDown
    End Sub

    Public Sub crea_lista(ByVal ctl_to_add As Control, ByVal X_Offset As Integer, ByVal Y_Offset As Integer)
        Me.X_Offset = X_Offset
        Me.Y_Offset = Y_Offset

        Dim lb As New ListBox
        lb.DisplayMember = "Name"
				lb.ValueMember = "_Value"
				lb.Size = New Size(Me.Width, 80)
        ctl_to_add.Controls.Add(lb)
        set_ListBox(lb)
        lb.BringToFront()
        Me.TextBox1.Width = Me.Width
    End Sub

    Public Function selected_valor() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim dato As ListData = Me.lista(Me.ListBox1.SelectedIndex)
        Return (dato._Value)
    End Function

    Public Function selected_text() As String
        If Me.ListBox1.SelectedIndex = -1 Then Return Nothing

        Dim dato As ListData = Me.lista(Me.ListBox1.SelectedIndex)
        Return dato.Name
    End Function

    Public Function getText() As String
        Return TextBox1.Text
    End Function


    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Up AndAlso ListBox1.SelectedIndex <> 0 AndAlso ListBox1.SelectedIndex <> -1 Then ListBox1.SelectedIndex -= 1
        ''''If e.KeyCode = Keys.Up Then TextBox1.SelectionStart += 1

        If e.KeyCode = Keys.Down AndAlso ListBox1.SelectedIndex <> ListBox1.Items.Count - 1 Then ListBox1.SelectedIndex += 1
        ''''If e.KeyCode = Keys.Down AndAlso TextBox1.SelectionStart <> 0 Then TextBox1.SelectionStart -= 1
    End Sub

    Private Sub TextBox1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyUp
        desp(Nothing, Nothing)
        'If TextBox1.Text.Length < 3 Then ocul(Nothing, Nothing)

        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        'If afecta Then
        If TextBox1.Text.Length >= 3 Then
            ''''Dim ini As Integer = TextBox1.SelectionStart

            RaiseEvent llena()
            If ListBox1.SelectedIndex = -1 Then ListBox1.SelectedIndex = 0

            'TextBox1.SelectionLength = 0
            ''''TextBox1.SelectionStart = ini
        Else
            'afecta = False
            ''''Dim ini As Integer = TextBox1.SelectionStart

            ListBox1.Items.Clear()

            ''''TextBox1.SelectionStart = ini
        End If
        'End If
    End Sub

    Private Sub ComboSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'AddHandler TextBox1.GotFocus, AddressOf desp
        'AddHandler TextBox1.LostFocus, AddressOf ocul
        'AddHandler Me.Parent.MouseDown, AddressOf ocul_lb_mouse
    End Sub

    Private Sub ComboSearch_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Move
        If Not ListBox1 Is Nothing Then fija_pos()
    End Sub

End Class
