<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1_callCenter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel3 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim OptionSet1 As Infragistics.Win.UltraWinToolbars.OptionSet = New Infragistics.Win.UltraWinToolbars.OptionSet("los_BTN_idioma")
        Dim ButtonTool21 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_salir")
        Dim ButtonTool37 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cerrarSesion")
        Dim RibbonTab1 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon1")
        Dim RibbonGroup1 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool15 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_nuevaLlamada")
        Dim ButtonTool30 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_listaLlamadas")
        Dim ButtonTool1 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_editarLlamada")
        Dim RibbonTab2 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon2")
        Dim RibbonGroup2 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool3 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_informacionGeneral")
        Dim ButtonTool4 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_habitaciones")
        Dim ButtonTool5 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_galeria")
        Dim ButtonTool6 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_mapa")
        Dim ButtonTool11 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_tarifas")
        Dim ButtonTool13 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservar")
        Dim ButtonTool19 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglas")
        Dim ButtonTool71 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_hotel_disponibilidad")
        Dim ButtonTool80 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_ver_detalles")
        Dim ButtonTool27 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscar")
        Dim RibbonTab3 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon5")
        Dim RibbonGroup3 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool42 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservar_vuelo")
        Dim ButtonTool43 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglas_vuelo")
        Dim ButtonTool41 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscar_vuelo")
        Dim RibbonTab4 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon6")
        Dim RibbonGroup4 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool53 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_eventosActividad")
        Dim ButtonTool47 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservarActividad")
        Dim ButtonTool49 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglasActividad")
        Dim ButtonTool50 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscarActividad")
        Dim RibbonTab5 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon7")
        Dim RibbonGroup5 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool58 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_tarifasAuto")
        Dim ButtonTool57 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservarAuto")
        Dim ButtonTool56 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglasAuto")
        Dim ButtonTool55 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscarAuto")
        Dim RibbonTab6 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon3")
        Dim RibbonGroup6 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool17 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservaciones")
        Dim ButtonTool31 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_detalles")
        Dim ButtonTool64 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_modificar")
        Dim ButtonTool34 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cancelar")
        Dim ButtonTool68 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reactivar")
        Dim ButtonTool74 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_copiar")
        Dim ButtonTool82 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cambios")
        Dim RibbonTab7 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon4")
        Dim RibbonGroup7 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool22 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_configurarAgente")
        Dim ButtonTool23 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_configurarConexion")
        Dim RibbonGroup8 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup2")
        Dim StateButtonTool1 As Infragistics.Win.UltraWinToolbars.StateButtonTool = New Infragistics.Win.UltraWinToolbars.StateButtonTool("btn_espa�ol", "los_BTN_idioma")
        Dim StateButtonTool3 As Infragistics.Win.UltraWinToolbars.StateButtonTool = New Infragistics.Win.UltraWinToolbars.StateButtonTool("btn_ingles", "los_BTN_idioma")
        Dim RibbonGroup9 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup3")
        Dim ButtonTool33 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cambiarLogo")
        Dim RibbonTab8 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("ribbon8")
        Dim RibbonGroup10 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup1")
        Dim ButtonTool65 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_ProfessionalsAlta")
        Dim ButtonTool69 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_professionals_reporte")
        Dim RibbonGroup11 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup2")
        Dim ButtonTool77 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reporte_produccion")
        Dim RibbonGroup12 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("ribbonGroup3")
        Dim ButtonTool78 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_SegmentCompany")
        Dim ButtonTool2 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscar")
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1_callCenter))
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool7 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_informacionGeneral")
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool8 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_habitaciones")
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool9 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_galeria")
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool10 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_mapa")
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool12 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_tarifas")
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool14 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservar")
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool16 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_salir")
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool18 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_mas")
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool20 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglas")
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance40 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool24 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservaciones")
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance41 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool25 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_modificar")
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance42 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool26 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cancelar")
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance43 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool32 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_detalles")
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance46 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool35 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_configurarAgente")
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance47 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool36 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_configurarConexion")
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance48 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim StateButtonTool2 As Infragistics.Win.UltraWinToolbars.StateButtonTool = New Infragistics.Win.UltraWinToolbars.StateButtonTool("btn_espa�ol", "los_BTN_idioma")
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance49 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim StateButtonTool4 As Infragistics.Win.UltraWinToolbars.StateButtonTool = New Infragistics.Win.UltraWinToolbars.StateButtonTool("btn_ingles", "los_BTN_idioma")
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance50 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool38 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cerrarSesion")
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance51 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool28 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_estadisticas")
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool29 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_nuevaLlamada")
        Dim Appearance45 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance44 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool39 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_listaLlamadas")
        Dim Appearance53 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance52 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool40 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_editarLlamada")
        Dim Appearance55 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance54 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool44 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscar_vuelo")
        Dim Appearance57 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance56 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool45 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservar_vuelo")
        Dim Appearance59 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance58 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool46 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglas_vuelo")
        Dim Appearance61 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance60 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool48 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservarActividad")
        Dim Appearance63 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance62 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool51 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglasActividad")
        Dim Appearance64 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance65 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool52 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscarActividad")
        Dim Appearance67 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance66 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool54 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_eventosActividad")
        Dim Appearance69 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance68 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool59 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_buscarAuto")
        Dim Appearance71 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance70 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool60 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reglasAuto")
        Dim Appearance73 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance72 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool61 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reservarAuto")
        Dim Appearance75 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance74 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool62 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_tarifasAuto")
        Dim Appearance77 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance76 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool63 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cambiarLogo")
        Dim Appearance78 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance79 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool67 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reactivar")
        Dim Appearance80 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance81 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool66 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_ProfessionalsAlta")
        Dim Appearance82 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance83 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool70 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_professionals_reporte")
        Dim Appearance84 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance85 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool72 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_ProfessionalsConfiguracion")
        Dim Appearance86 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool73 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_hotel_disponibilidad")
        Dim Appearance87 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance90 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool75 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_copiar")
        Dim Appearance89 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance88 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool76 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_reporte_produccion")
        Dim Appearance91 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance92 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool79 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_SegmentCompany")
        Dim Appearance93 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance94 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool81 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_ver_detalles")
        Dim Appearance96 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance95 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ButtonTool83 As Infragistics.Win.UltraWinToolbars.ButtonTool = New Infragistics.Win.UltraWinToolbars.ButtonTool("btn_cambios")
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UltraTabbedMdiManager1 = New Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager(Me.components)
        Me.UltraDockManager1 = New Infragistics.Win.UltraWinDock.UltraDockManager(Me.components)
        Me._Form1_callCenterUnpinnedTabAreaLeft = New Infragistics.Win.UltraWinDock.UnpinnedTabArea()
        Me._Form1_callCenterUnpinnedTabAreaRight = New Infragistics.Win.UltraWinDock.UnpinnedTabArea()
        Me._Form1_callCenterUnpinnedTabAreaTop = New Infragistics.Win.UltraWinDock.UnpinnedTabArea()
        Me._Form1_callCenterUnpinnedTabAreaBottom = New Infragistics.Win.UltraWinDock.UnpinnedTabArea()
        Me._Form1_callCenterAutoHideControl = New Infragistics.Win.UltraWinDock.AutoHideControl()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me._callCenter_Toolbars_Dock_Area_Right = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me.UltraToolbarsManager1 = New Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(Me.components)
        Me._callCenter_Toolbars_Dock_Area_Left = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me._callCenter_Toolbars_Dock_Area_Bottom = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me._callCenter_Toolbars_Dock_Area_Top = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        CType(Me.UltraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraDockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraToolbarsManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraTabbedMdiManager1
        '
        Me.UltraTabbedMdiManager1.MdiParent = Me
        Me.UltraTabbedMdiManager1.TabSettings.DisplayFormIcon = Infragistics.Win.DefaultableBoolean.[True]
        Me.UltraTabbedMdiManager1.ViewStyle = Infragistics.Win.UltraWinTabbedMdi.ViewStyle.Office2007
        '
        'UltraDockManager1
        '
        Me.UltraDockManager1.CaptionStyle = Infragistics.Win.UltraWinDock.CaptionStyle.Office2003
        Me.UltraDockManager1.HostControl = Me
        Me.UltraDockManager1.Visible = False
        '
        '_Form1_callCenterUnpinnedTabAreaLeft
        '
        Me._Form1_callCenterUnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me._Form1_callCenterUnpinnedTabAreaLeft.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Form1_callCenterUnpinnedTabAreaLeft.Location = New System.Drawing.Point(4, 158)
        Me._Form1_callCenterUnpinnedTabAreaLeft.Name = "_Form1_callCenterUnpinnedTabAreaLeft"
        Me._Form1_callCenterUnpinnedTabAreaLeft.Owner = Me.UltraDockManager1
        Me._Form1_callCenterUnpinnedTabAreaLeft.Size = New System.Drawing.Size(0, 398)
        Me._Form1_callCenterUnpinnedTabAreaLeft.TabIndex = 19
        '
        '_Form1_callCenterUnpinnedTabAreaRight
        '
        Me._Form1_callCenterUnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right
        Me._Form1_callCenterUnpinnedTabAreaRight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Form1_callCenterUnpinnedTabAreaRight.Location = New System.Drawing.Point(1054, 158)
        Me._Form1_callCenterUnpinnedTabAreaRight.Name = "_Form1_callCenterUnpinnedTabAreaRight"
        Me._Form1_callCenterUnpinnedTabAreaRight.Owner = Me.UltraDockManager1
        Me._Form1_callCenterUnpinnedTabAreaRight.Size = New System.Drawing.Size(0, 398)
        Me._Form1_callCenterUnpinnedTabAreaRight.TabIndex = 20
        '
        '_Form1_callCenterUnpinnedTabAreaTop
        '
        Me._Form1_callCenterUnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top
        Me._Form1_callCenterUnpinnedTabAreaTop.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Form1_callCenterUnpinnedTabAreaTop.Location = New System.Drawing.Point(4, 158)
        Me._Form1_callCenterUnpinnedTabAreaTop.Name = "_Form1_callCenterUnpinnedTabAreaTop"
        Me._Form1_callCenterUnpinnedTabAreaTop.Owner = Me.UltraDockManager1
        Me._Form1_callCenterUnpinnedTabAreaTop.Size = New System.Drawing.Size(1050, 0)
        Me._Form1_callCenterUnpinnedTabAreaTop.TabIndex = 21
        '
        '_Form1_callCenterUnpinnedTabAreaBottom
        '
        Me._Form1_callCenterUnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._Form1_callCenterUnpinnedTabAreaBottom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Form1_callCenterUnpinnedTabAreaBottom.Location = New System.Drawing.Point(4, 556)
        Me._Form1_callCenterUnpinnedTabAreaBottom.Name = "_Form1_callCenterUnpinnedTabAreaBottom"
        Me._Form1_callCenterUnpinnedTabAreaBottom.Owner = Me.UltraDockManager1
        Me._Form1_callCenterUnpinnedTabAreaBottom.Size = New System.Drawing.Size(1050, 0)
        Me._Form1_callCenterUnpinnedTabAreaBottom.TabIndex = 22
        '
        '_Form1_callCenterAutoHideControl
        '
        Me._Form1_callCenterAutoHideControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Form1_callCenterAutoHideControl.Location = New System.Drawing.Point(0, 0)
        Me._Form1_callCenterAutoHideControl.Name = "_Form1_callCenterAutoHideControl"
        Me._Form1_callCenterAutoHideControl.Owner = Me.UltraDockManager1
        Me._Form1_callCenterAutoHideControl.Size = New System.Drawing.Size(0, 0)
        Me._Form1_callCenterAutoHideControl.TabIndex = 23
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 556)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel1.Key = "pnl_1"
        UltraStatusPanel1.Text = "Univisit CallCenter 2009"
        UltraStatusPanel1.Width = 200
        UltraStatusPanel2.Key = "pnl_2"
        UltraStatusPanel2.Width = 700
        UltraStatusPanel3.Key = "pnl_3"
        UltraStatusPanel3.ProgressBarInfo.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Office2007Continuous
        UltraStatusPanel3.ProgressBarInfo.Value = 100
        UltraStatusPanel3.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Progress
        UltraStatusPanel3.Visible = False
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2, UltraStatusPanel3})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(1058, 23)
        Me.UltraStatusBar1.TabIndex = 41
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        '_callCenter_Toolbars_Dock_Area_Right
        '
        Me._callCenter_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._callCenter_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._callCenter_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right
        Me._callCenter_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText
        Me._callCenter_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 4
        Me._callCenter_Toolbars_Dock_Area_Right.Location = New System.Drawing.Point(1054, 158)
        Me._callCenter_Toolbars_Dock_Area_Right.Name = "_callCenter_Toolbars_Dock_Area_Right"
        Me._callCenter_Toolbars_Dock_Area_Right.Size = New System.Drawing.Size(4, 398)
        Me._callCenter_Toolbars_Dock_Area_Right.ToolbarsManager = Me.UltraToolbarsManager1
        '
        'UltraToolbarsManager1
        '
        Me.UltraToolbarsManager1.DesignerFlags = 1
        Me.UltraToolbarsManager1.DockWithinContainer = Me
        Me.UltraToolbarsManager1.DockWithinContainerBaseType = GetType(System.Windows.Forms.Form)
        OptionSet1.AllowAllUp = False
        Me.UltraToolbarsManager1.OptionSets.Add(OptionSet1)
        Me.UltraToolbarsManager1.Ribbon.ApplicationMenu.FooterToolbar.NonInheritedTools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool21})
        Me.UltraToolbarsManager1.Ribbon.ApplicationMenu.ToolAreaLeft.NonInheritedTools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool37})
        RibbonTab1.Caption = "Operador"
        RibbonGroup1.Caption = "Opciones del operador"
        RibbonGroup1.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup1.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool15, ButtonTool30, ButtonTool1})
        RibbonTab1.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup1})
        RibbonTab2.Caption = "Hotel"
        RibbonGroup2.Caption = "Opciones"
        RibbonGroup2.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup2.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool3, ButtonTool4, ButtonTool5, ButtonTool6, ButtonTool11, ButtonTool13, ButtonTool19, ButtonTool71, ButtonTool80, ButtonTool27})
        RibbonTab2.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup2})
        RibbonTab3.Caption = "Vuelos"
        RibbonGroup3.Caption = "Opciones"
        RibbonGroup3.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup3.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool42, ButtonTool43, ButtonTool41})
        RibbonTab3.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup3})
        RibbonTab4.Caption = "Actividades"
        RibbonGroup4.Caption = "Opciones"
        RibbonGroup4.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup4.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool53, ButtonTool47, ButtonTool49, ButtonTool50})
        RibbonTab4.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup4})
        RibbonTab5.Caption = "Autos"
        RibbonGroup5.Caption = "Opciones"
        RibbonGroup5.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup5.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool58, ButtonTool57, ButtonTool56, ButtonTool55})
        RibbonTab5.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup5})
        RibbonTab6.Caption = "Agencia"
        RibbonGroup6.Caption = "Opciones de reservaciones"
        RibbonGroup6.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup6.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool17, ButtonTool31, ButtonTool64, ButtonTool34, ButtonTool68, ButtonTool74, ButtonTool82})
        RibbonTab6.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup6})
        RibbonTab7.Caption = "Configuraci�n"
        RibbonGroup7.Caption = "Acceso al sistema"
        RibbonGroup7.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup7.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool22, ButtonTool23})
        RibbonGroup8.Caption = "Idioma"
        RibbonGroup8.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        StateButtonTool1.Checked = True
        RibbonGroup8.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {StateButtonTool1, StateButtonTool3})
        RibbonGroup9.Caption = "Opciones"
        RibbonGroup9.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup9.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool33})
        RibbonTab7.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup7, RibbonGroup8, RibbonGroup9})
        RibbonTab8.Caption = "Mision"
        RibbonGroup10.Caption = "Distinciones"
        ButtonTool65.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        ButtonTool69.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup10.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool65, ButtonTool69})
        RibbonGroup11.Caption = "Reportes"
        ButtonTool77.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup11.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool77})
        RibbonGroup12.Caption = "Catalogos"
        ButtonTool78.InstanceProps.PreferredSizeOnRibbon = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup12.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool78})
        RibbonTab8.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup10, RibbonGroup11, RibbonGroup12})
        Me.UltraToolbarsManager1.Ribbon.NonInheritedRibbonTabs.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonTab() {RibbonTab1, RibbonTab2, RibbonTab3, RibbonTab4, RibbonTab5, RibbonTab6, RibbonTab7, RibbonTab8})
        Me.UltraToolbarsManager1.Ribbon.Visible = True
        Me.UltraToolbarsManager1.ShowFullMenusDelay = 500
        Appearance11.Image = CType(resources.GetObject("Appearance11.Image"), Object)
        ButtonTool2.SharedProps.AppearancesLarge.Appearance = Appearance11
        Appearance32.Image = CType(resources.GetObject("Appearance32.Image"), Object)
        ButtonTool2.SharedProps.AppearancesSmall.Appearance = Appearance32
        ButtonTool2.SharedProps.Caption = "Buscar"
        ButtonTool2.SharedProps.CustomizerCaption = "Buscar"
        Appearance12.Image = CType(resources.GetObject("Appearance12.Image"), Object)
        ButtonTool7.SharedProps.AppearancesLarge.Appearance = Appearance12
        Appearance33.Image = CType(resources.GetObject("Appearance33.Image"), Object)
        ButtonTool7.SharedProps.AppearancesSmall.Appearance = Appearance33
        ButtonTool7.SharedProps.Caption = "Informaci�n general"
        ButtonTool7.SharedProps.Enabled = False
        Appearance17.Image = CType(resources.GetObject("Appearance17.Image"), Object)
        ButtonTool8.SharedProps.AppearancesLarge.Appearance = Appearance17
        Appearance34.Image = CType(resources.GetObject("Appearance34.Image"), Object)
        ButtonTool8.SharedProps.AppearancesSmall.Appearance = Appearance34
        ButtonTool8.SharedProps.Caption = "Habitaciones"
        ButtonTool8.SharedProps.Enabled = False
        Appearance13.Image = CType(resources.GetObject("Appearance13.Image"), Object)
        ButtonTool9.SharedProps.AppearancesLarge.Appearance = Appearance13
        Appearance35.Image = CType(resources.GetObject("Appearance35.Image"), Object)
        ButtonTool9.SharedProps.AppearancesSmall.Appearance = Appearance35
        ButtonTool9.SharedProps.Caption = "Galer�a"
        ButtonTool9.SharedProps.Enabled = False
        Appearance14.Image = CType(resources.GetObject("Appearance14.Image"), Object)
        ButtonTool10.SharedProps.AppearancesLarge.Appearance = Appearance14
        Appearance36.Image = CType(resources.GetObject("Appearance36.Image"), Object)
        ButtonTool10.SharedProps.AppearancesSmall.Appearance = Appearance36
        ButtonTool10.SharedProps.Caption = "Mapa"
        ButtonTool10.SharedProps.Enabled = False
        Appearance16.Image = CType(resources.GetObject("Appearance16.Image"), Object)
        ButtonTool12.SharedProps.AppearancesLarge.Appearance = Appearance16
        Appearance37.Image = CType(resources.GetObject("Appearance37.Image"), Object)
        ButtonTool12.SharedProps.AppearancesSmall.Appearance = Appearance37
        ButtonTool12.SharedProps.Caption = "Tarifas"
        ButtonTool12.SharedProps.Enabled = False
        Appearance15.Image = CType(resources.GetObject("Appearance15.Image"), Object)
        ButtonTool14.SharedProps.AppearancesLarge.Appearance = Appearance15
        Appearance38.Image = CType(resources.GetObject("Appearance38.Image"), Object)
        ButtonTool14.SharedProps.AppearancesSmall.Appearance = Appearance38
        ButtonTool14.SharedProps.Caption = "Reservar"
        ButtonTool14.SharedProps.Enabled = False
        Appearance21.Image = CType(resources.GetObject("Appearance21.Image"), Object)
        ButtonTool16.SharedProps.AppearancesSmall.Appearance = Appearance21
        ButtonTool16.SharedProps.Caption = "Salir"
        ButtonTool16.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText
        Appearance18.Image = CType(resources.GetObject("Appearance18.Image"), Object)
        ButtonTool18.SharedProps.AppearancesLarge.Appearance = Appearance18
        Appearance39.Image = CType(resources.GetObject("Appearance39.Image"), Object)
        ButtonTool18.SharedProps.AppearancesSmall.Appearance = Appearance39
        ButtonTool18.SharedProps.Caption = "M�s"
        ButtonTool18.SharedProps.Enabled = False
        Appearance19.Image = CType(resources.GetObject("Appearance19.Image"), Object)
        ButtonTool20.SharedProps.AppearancesLarge.Appearance = Appearance19
        Appearance40.Image = CType(resources.GetObject("Appearance40.Image"), Object)
        ButtonTool20.SharedProps.AppearancesSmall.Appearance = Appearance40
        ButtonTool20.SharedProps.Caption = "Reglas"
        ButtonTool20.SharedProps.Enabled = False
        Appearance23.Image = CType(resources.GetObject("Appearance23.Image"), Object)
        ButtonTool24.SharedProps.AppearancesLarge.Appearance = Appearance23
        Appearance41.Image = CType(resources.GetObject("Appearance41.Image"), Object)
        ButtonTool24.SharedProps.AppearancesSmall.Appearance = Appearance41
        ButtonTool24.SharedProps.Caption = "Reservaciones"
        Appearance22.Image = CType(resources.GetObject("Appearance22.Image"), Object)
        ButtonTool25.SharedProps.AppearancesLarge.Appearance = Appearance22
        Appearance42.Image = CType(resources.GetObject("Appearance42.Image"), Object)
        ButtonTool25.SharedProps.AppearancesSmall.Appearance = Appearance42
        ButtonTool25.SharedProps.Caption = "Modificar"
        ButtonTool25.SharedProps.Enabled = False
        Appearance20.Image = CType(resources.GetObject("Appearance20.Image"), Object)
        ButtonTool26.SharedProps.AppearancesLarge.Appearance = Appearance20
        Appearance43.Image = CType(resources.GetObject("Appearance43.Image"), Object)
        ButtonTool26.SharedProps.AppearancesSmall.Appearance = Appearance43
        ButtonTool26.SharedProps.Caption = "Cancelar"
        ButtonTool26.SharedProps.Enabled = False
        Appearance26.Image = CType(resources.GetObject("Appearance26.Image"), Object)
        ButtonTool32.SharedProps.AppearancesLarge.Appearance = Appearance26
        Appearance46.Image = CType(resources.GetObject("Appearance46.Image"), Object)
        ButtonTool32.SharedProps.AppearancesSmall.Appearance = Appearance46
        ButtonTool32.SharedProps.Caption = "Detalles"
        ButtonTool32.SharedProps.Enabled = False
        Appearance29.Image = CType(resources.GetObject("Appearance29.Image"), Object)
        ButtonTool35.SharedProps.AppearancesLarge.Appearance = Appearance29
        Appearance47.Image = CType(resources.GetObject("Appearance47.Image"), Object)
        ButtonTool35.SharedProps.AppearancesSmall.Appearance = Appearance47
        ButtonTool35.SharedProps.Caption = "Configurar agente"
        Appearance30.Image = CType(resources.GetObject("Appearance30.Image"), Object)
        ButtonTool36.SharedProps.AppearancesLarge.Appearance = Appearance30
        Appearance48.Image = CType(resources.GetObject("Appearance48.Image"), Object)
        ButtonTool36.SharedProps.AppearancesSmall.Appearance = Appearance48
        ButtonTool36.SharedProps.Caption = "Configurar conexi�n"
        StateButtonTool2.Checked = True
        StateButtonTool2.OptionSetKey = "los_BTN_idioma"
        Appearance27.Image = CType(resources.GetObject("Appearance27.Image"), Object)
        StateButtonTool2.SharedProps.AppearancesLarge.Appearance = Appearance27
        Appearance49.Image = CType(resources.GetObject("Appearance49.Image"), Object)
        StateButtonTool2.SharedProps.AppearancesSmall.Appearance = Appearance49
        StateButtonTool2.SharedProps.Caption = "Espa�ol"
        StateButtonTool4.OptionSetKey = "los_BTN_idioma"
        Appearance28.Image = CType(resources.GetObject("Appearance28.Image"), Object)
        StateButtonTool4.SharedProps.AppearancesLarge.Appearance = Appearance28
        Appearance50.Image = CType(resources.GetObject("Appearance50.Image"), Object)
        StateButtonTool4.SharedProps.AppearancesSmall.Appearance = Appearance50
        StateButtonTool4.SharedProps.Caption = "Ingles"
        Appearance31.Image = CType(resources.GetObject("Appearance31.Image"), Object)
        ButtonTool38.SharedProps.AppearancesLarge.Appearance = Appearance31
        Appearance51.Image = CType(resources.GetObject("Appearance51.Image"), Object)
        ButtonTool38.SharedProps.AppearancesSmall.Appearance = Appearance51
        ButtonTool38.SharedProps.Caption = "Cerrar sesi�n"
        Appearance25.Image = CType(resources.GetObject("Appearance25.Image"), Object)
        ButtonTool28.SharedProps.AppearancesLarge.Appearance = Appearance25
        Appearance24.Image = CType(resources.GetObject("Appearance24.Image"), Object)
        ButtonTool28.SharedProps.AppearancesSmall.Appearance = Appearance24
        ButtonTool28.SharedProps.Caption = "Estad�sticas"
        ButtonTool28.SharedProps.Enabled = False
        Appearance45.Image = CType(resources.GetObject("Appearance45.Image"), Object)
        ButtonTool29.SharedProps.AppearancesLarge.Appearance = Appearance45
        Appearance44.Image = CType(resources.GetObject("Appearance44.Image"), Object)
        ButtonTool29.SharedProps.AppearancesSmall.Appearance = Appearance44
        ButtonTool29.SharedProps.Caption = "Nueva llamada"
        Appearance53.Image = CType(resources.GetObject("Appearance53.Image"), Object)
        ButtonTool39.SharedProps.AppearancesLarge.Appearance = Appearance53
        Appearance52.Image = CType(resources.GetObject("Appearance52.Image"), Object)
        ButtonTool39.SharedProps.AppearancesSmall.Appearance = Appearance52
        ButtonTool39.SharedProps.Caption = "Lista de llamadas"
        Appearance55.Image = CType(resources.GetObject("Appearance55.Image"), Object)
        ButtonTool40.SharedProps.AppearancesLarge.Appearance = Appearance55
        Appearance54.Image = CType(resources.GetObject("Appearance54.Image"), Object)
        ButtonTool40.SharedProps.AppearancesSmall.Appearance = Appearance54
        ButtonTool40.SharedProps.Caption = "Ver reservaciones"
        ButtonTool40.SharedProps.Enabled = False
        Appearance57.Image = CType(resources.GetObject("Appearance57.Image"), Object)
        ButtonTool44.SharedProps.AppearancesLarge.Appearance = Appearance57
        Appearance56.Image = CType(resources.GetObject("Appearance56.Image"), Object)
        ButtonTool44.SharedProps.AppearancesSmall.Appearance = Appearance56
        ButtonTool44.SharedProps.Caption = "Buscar"
        Appearance59.Image = CType(resources.GetObject("Appearance59.Image"), Object)
        ButtonTool45.SharedProps.AppearancesLarge.Appearance = Appearance59
        Appearance58.Image = CType(resources.GetObject("Appearance58.Image"), Object)
        ButtonTool45.SharedProps.AppearancesSmall.Appearance = Appearance58
        ButtonTool45.SharedProps.Caption = "Reservar"
        ButtonTool45.SharedProps.Enabled = False
        Appearance61.Image = CType(resources.GetObject("Appearance61.Image"), Object)
        ButtonTool46.SharedProps.AppearancesLarge.Appearance = Appearance61
        Appearance60.Image = CType(resources.GetObject("Appearance60.Image"), Object)
        ButtonTool46.SharedProps.AppearancesSmall.Appearance = Appearance60
        ButtonTool46.SharedProps.Caption = "Reglas"
        ButtonTool46.SharedProps.Enabled = False
        Appearance63.Image = CType(resources.GetObject("Appearance63.Image"), Object)
        ButtonTool48.SharedProps.AppearancesLarge.Appearance = Appearance63
        Appearance62.Image = CType(resources.GetObject("Appearance62.Image"), Object)
        ButtonTool48.SharedProps.AppearancesSmall.Appearance = Appearance62
        ButtonTool48.SharedProps.Caption = "Reservar"
        ButtonTool48.SharedProps.Enabled = False
        Appearance64.Image = CType(resources.GetObject("Appearance64.Image"), Object)
        ButtonTool51.SharedProps.AppearancesLarge.Appearance = Appearance64
        Appearance65.Image = CType(resources.GetObject("Appearance65.Image"), Object)
        ButtonTool51.SharedProps.AppearancesSmall.Appearance = Appearance65
        ButtonTool51.SharedProps.Caption = "Reglas"
        ButtonTool51.SharedProps.Enabled = False
        Appearance67.Image = CType(resources.GetObject("Appearance67.Image"), Object)
        ButtonTool52.SharedProps.AppearancesLarge.Appearance = Appearance67
        Appearance66.Image = CType(resources.GetObject("Appearance66.Image"), Object)
        ButtonTool52.SharedProps.AppearancesSmall.Appearance = Appearance66
        ButtonTool52.SharedProps.Caption = "Buscar"
        Appearance69.Image = CType(resources.GetObject("Appearance69.Image"), Object)
        ButtonTool54.SharedProps.AppearancesLarge.Appearance = Appearance69
        Appearance68.Image = CType(resources.GetObject("Appearance68.Image"), Object)
        ButtonTool54.SharedProps.AppearancesSmall.Appearance = Appearance68
        ButtonTool54.SharedProps.Caption = "Tarifas"
        ButtonTool54.SharedProps.Enabled = False
        Appearance71.Image = CType(resources.GetObject("Appearance71.Image"), Object)
        ButtonTool59.SharedProps.AppearancesLarge.Appearance = Appearance71
        Appearance70.Image = CType(resources.GetObject("Appearance70.Image"), Object)
        ButtonTool59.SharedProps.AppearancesSmall.Appearance = Appearance70
        ButtonTool59.SharedProps.Caption = "Buscar"
        Appearance73.Image = CType(resources.GetObject("Appearance73.Image"), Object)
        ButtonTool60.SharedProps.AppearancesLarge.Appearance = Appearance73
        Appearance72.Image = CType(resources.GetObject("Appearance72.Image"), Object)
        ButtonTool60.SharedProps.AppearancesSmall.Appearance = Appearance72
        ButtonTool60.SharedProps.Caption = "Reglas"
        ButtonTool60.SharedProps.Enabled = False
        Appearance75.Image = CType(resources.GetObject("Appearance75.Image"), Object)
        ButtonTool61.SharedProps.AppearancesLarge.Appearance = Appearance75
        Appearance74.Image = CType(resources.GetObject("Appearance74.Image"), Object)
        ButtonTool61.SharedProps.AppearancesSmall.Appearance = Appearance74
        ButtonTool61.SharedProps.Caption = "Reservar"
        ButtonTool61.SharedProps.Enabled = False
        Appearance77.Image = CType(resources.GetObject("Appearance77.Image"), Object)
        ButtonTool62.SharedProps.AppearancesLarge.Appearance = Appearance77
        Appearance76.Image = CType(resources.GetObject("Appearance76.Image"), Object)
        ButtonTool62.SharedProps.AppearancesSmall.Appearance = Appearance76
        ButtonTool62.SharedProps.Caption = "Tarifas"
        ButtonTool62.SharedProps.Enabled = False
        Appearance78.Image = CType(resources.GetObject("Appearance78.Image"), Object)
        ButtonTool63.SharedProps.AppearancesLarge.Appearance = Appearance78
        Appearance79.Image = CType(resources.GetObject("Appearance79.Image"), Object)
        ButtonTool63.SharedProps.AppearancesSmall.Appearance = Appearance79
        ButtonTool63.SharedProps.Caption = "Cambiar logo"
        Appearance80.Image = CType(resources.GetObject("Appearance80.Image"), Object)
        ButtonTool67.SharedProps.AppearancesLarge.Appearance = Appearance80
        Appearance81.Image = CType(resources.GetObject("Appearance81.Image"), Object)
        ButtonTool67.SharedProps.AppearancesSmall.Appearance = Appearance81
        ButtonTool67.SharedProps.Caption = "Reactivar"
        ButtonTool67.SharedProps.Enabled = False
        Appearance82.Image = CType(resources.GetObject("Appearance82.Image"), Object)
        ButtonTool66.SharedProps.AppearancesLarge.Appearance = Appearance82
        Appearance83.Image = CType(resources.GetObject("Appearance83.Image"), Object)
        ButtonTool66.SharedProps.AppearancesSmall.Appearance = Appearance83
        ButtonTool66.SharedProps.Caption = "Alta de Socios"
        Appearance84.Image = CType(resources.GetObject("Appearance84.Image"), Object)
        ButtonTool70.SharedProps.AppearancesLarge.Appearance = Appearance84
        Appearance85.Image = CType(resources.GetObject("Appearance85.Image"), Object)
        ButtonTool70.SharedProps.AppearancesSmall.Appearance = Appearance85
        ButtonTool70.SharedProps.Caption = "Lista de Socios"
        Appearance86.Image = CType(resources.GetObject("Appearance86.Image"), Object)
        ButtonTool72.SharedProps.AppearancesLarge.Appearance = Appearance86
        ButtonTool72.SharedProps.Caption = "Configuraci�n"
        ButtonTool72.SharedProps.Visible = False
        Appearance87.Image = CType(resources.GetObject("Appearance87.Image"), Object)
        ButtonTool73.SharedProps.AppearancesLarge.Appearance = Appearance87
        Appearance90.Image = CType(resources.GetObject("Appearance90.Image"), Object)
        ButtonTool73.SharedProps.AppearancesSmall.Appearance = Appearance90
        ButtonTool73.SharedProps.Caption = "Disponibilidad"
        ButtonTool73.SharedProps.Enabled = False
        Appearance89.Image = CType(resources.GetObject("Appearance89.Image"), Object)
        ButtonTool75.SharedProps.AppearancesLarge.Appearance = Appearance89
        Appearance88.Image = CType(resources.GetObject("Appearance88.Image"), Object)
        ButtonTool75.SharedProps.AppearancesSmall.Appearance = Appearance88
        ButtonTool75.SharedProps.Caption = "Copiar"
        ButtonTool75.SharedProps.Enabled = False
        Appearance91.Image = CType(resources.GetObject("Appearance91.Image"), Object)
        ButtonTool76.SharedProps.AppearancesLarge.Appearance = Appearance91
        Appearance92.Image = CType(resources.GetObject("Appearance92.Image"), Object)
        ButtonTool76.SharedProps.AppearancesSmall.Appearance = Appearance92
        ButtonTool76.SharedProps.Caption = "Producci�n"
        ButtonTool76.SharedProps.ToolTipText = "Producci�n Agente / Empresa"
        Appearance93.Image = CType(resources.GetObject("Appearance93.Image"), Object)
        ButtonTool79.SharedProps.AppearancesLarge.Appearance = Appearance93
        Appearance94.Image = CType(resources.GetObject("Appearance94.Image"), Object)
        ButtonTool79.SharedProps.AppearancesSmall.Appearance = Appearance94
        ButtonTool79.SharedProps.Caption = "Empresas Convenios"
        Appearance96.Image = CType(resources.GetObject("Appearance96.Image"), Object)
        ButtonTool81.SharedProps.AppearancesLarge.Appearance = Appearance96
        Appearance95.Image = CType(resources.GetObject("Appearance95.Image"), Object)
        ButtonTool81.SharedProps.AppearancesSmall.Appearance = Appearance95
        ButtonTool81.SharedProps.Caption = "Ver detalles"
        ButtonTool81.SharedProps.Enabled = False
        Appearance1.Image = CType(resources.GetObject("Appearance1.Image"), Object)
        ButtonTool83.SharedProps.AppearancesSmall.Appearance = Appearance1
        ButtonTool83.SharedProps.Caption = "Cambios"
        ButtonTool83.SharedProps.Enabled = False
        Me.UltraToolbarsManager1.Tools.AddRange(New Infragistics.Win.UltraWinToolbars.ToolBase() {ButtonTool2, ButtonTool7, ButtonTool8, ButtonTool9, ButtonTool10, ButtonTool12, ButtonTool14, ButtonTool16, ButtonTool18, ButtonTool20, ButtonTool24, ButtonTool25, ButtonTool26, ButtonTool32, ButtonTool35, ButtonTool36, StateButtonTool2, StateButtonTool4, ButtonTool38, ButtonTool28, ButtonTool29, ButtonTool39, ButtonTool40, ButtonTool44, ButtonTool45, ButtonTool46, ButtonTool48, ButtonTool51, ButtonTool52, ButtonTool54, ButtonTool59, ButtonTool60, ButtonTool61, ButtonTool62, ButtonTool63, ButtonTool67, ButtonTool66, ButtonTool70, ButtonTool72, ButtonTool73, ButtonTool75, ButtonTool76, ButtonTool79, ButtonTool81, ButtonTool83})
        '
        '_callCenter_Toolbars_Dock_Area_Left
        '
        Me._callCenter_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._callCenter_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._callCenter_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left
        Me._callCenter_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText
        Me._callCenter_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 4
        Me._callCenter_Toolbars_Dock_Area_Left.Location = New System.Drawing.Point(0, 158)
        Me._callCenter_Toolbars_Dock_Area_Left.Name = "_callCenter_Toolbars_Dock_Area_Left"
        Me._callCenter_Toolbars_Dock_Area_Left.Size = New System.Drawing.Size(4, 398)
        Me._callCenter_Toolbars_Dock_Area_Left.ToolbarsManager = Me.UltraToolbarsManager1
        '
        '_callCenter_Toolbars_Dock_Area_Bottom
        '
        Me._callCenter_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._callCenter_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._callCenter_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom
        Me._callCenter_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText
        Me._callCenter_Toolbars_Dock_Area_Bottom.Location = New System.Drawing.Point(0, 556)
        Me._callCenter_Toolbars_Dock_Area_Bottom.Name = "_callCenter_Toolbars_Dock_Area_Bottom"
        Me._callCenter_Toolbars_Dock_Area_Bottom.Size = New System.Drawing.Size(1058, 0)
        Me._callCenter_Toolbars_Dock_Area_Bottom.ToolbarsManager = Me.UltraToolbarsManager1
        '
        '_callCenter_Toolbars_Dock_Area_Top
        '
        Me._callCenter_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._callCenter_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._callCenter_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top
        Me._callCenter_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText
        Me._callCenter_Toolbars_Dock_Area_Top.Location = New System.Drawing.Point(0, 0)
        Me._callCenter_Toolbars_Dock_Area_Top.Name = "_callCenter_Toolbars_Dock_Area_Top"
        Me._callCenter_Toolbars_Dock_Area_Top.Size = New System.Drawing.Size(1058, 158)
        Me._callCenter_Toolbars_Dock_Area_Top.ToolbarsManager = Me.UltraToolbarsManager1
        '
        'Form1_callCenter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1058, 579)
        Me.Controls.Add(Me._Form1_callCenterAutoHideControl)
        Me.Controls.Add(Me._Form1_callCenterUnpinnedTabAreaBottom)
        Me.Controls.Add(Me._Form1_callCenterUnpinnedTabAreaTop)
        Me.Controls.Add(Me._Form1_callCenterUnpinnedTabAreaRight)
        Me.Controls.Add(Me._Form1_callCenterUnpinnedTabAreaLeft)
        Me.Controls.Add(Me._callCenter_Toolbars_Dock_Area_Left)
        Me.Controls.Add(Me._callCenter_Toolbars_Dock_Area_Right)
        Me.Controls.Add(Me._callCenter_Toolbars_Dock_Area_Top)
        Me.Controls.Add(Me._callCenter_Toolbars_Dock_Area_Bottom)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "Form1_callCenter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Univisit Call Center"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.UltraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraDockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraToolbarsManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraTabbedMdiManager1 As Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager
    Friend WithEvents _Form1_callCenterAutoHideControl As Infragistics.Win.UltraWinDock.AutoHideControl
    Friend WithEvents UltraDockManager1 As Infragistics.Win.UltraWinDock.UltraDockManager
    Friend WithEvents _Form1_callCenterUnpinnedTabAreaTop As Infragistics.Win.UltraWinDock.UnpinnedTabArea
    Friend WithEvents _Form1_callCenterUnpinnedTabAreaBottom As Infragistics.Win.UltraWinDock.UnpinnedTabArea
    Friend WithEvents _Form1_callCenterUnpinnedTabAreaLeft As Infragistics.Win.UltraWinDock.UnpinnedTabArea
    Friend WithEvents _Form1_callCenterUnpinnedTabAreaRight As Infragistics.Win.UltraWinDock.UnpinnedTabArea
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents _callCenter_Toolbars_Dock_Area_Left As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents UltraToolbarsManager1 As Infragistics.Win.UltraWinToolbars.UltraToolbarsManager
    Friend WithEvents _callCenter_Toolbars_Dock_Area_Right As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents _callCenter_Toolbars_Dock_Area_Top As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents _callCenter_Toolbars_Dock_Area_Bottom As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea

End Class
