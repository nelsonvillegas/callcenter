<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class configurarAgente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim UltraTab3 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab
        Me.UltraTabPageControl3 = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.btn_cambiar = New System.Windows.Forms.Button
        Me.txt_passNew2 = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_repetirPass = New System.Windows.Forms.Label
        Me.txt_passNew = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_nuevoPass = New System.Windows.Forms.Label
        Me.txt_passActual = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_passActual = New System.Windows.Forms.Label
        Me.btn_guardar = New System.Windows.Forms.Button
        Me.lbl_correo = New System.Windows.Forms.Label
        Me.txt_correo = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_nombreAgencia = New System.Windows.Forms.Label
        Me.txt_agencia = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_agente = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_nombreAgente = New System.Windows.Forms.Label
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.UltraTabControl1 = New Infragistics.Win.UltraWinTabControl.UltraTabControl
        Me.UltraTabSharedControlsPage1 = New Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
        Me.UltraTabPageControl3.SuspendLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.txt_passNew2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_passNew, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_passActual, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_correo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_agencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_agente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraTabPageControl3
        '
        Me.UltraTabPageControl3.Controls.Add(Me.UltraGroupBox1)
        Me.UltraTabPageControl3.Controls.Add(Me.btn_guardar)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_correo)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_correo)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_nombreAgencia)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_agencia)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_agente)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_nombreAgente)
        Me.UltraTabPageControl3.Location = New System.Drawing.Point(1, 22)
        Me.UltraTabPageControl3.Name = "UltraTabPageControl3"
        Me.UltraTabPageControl3.Size = New System.Drawing.Size(685, 171)
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UltraGroupBox1.Controls.Add(Me.Label3)
        Me.UltraGroupBox1.Controls.Add(Me.Label2)
        Me.UltraGroupBox1.Controls.Add(Me.Label1)
        Me.UltraGroupBox1.Controls.Add(Me.btn_cambiar)
        Me.UltraGroupBox1.Controls.Add(Me.txt_passNew2)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_repetirPass)
        Me.UltraGroupBox1.Controls.Add(Me.txt_passNew)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_nuevoPass)
        Me.UltraGroupBox1.Controls.Add(Me.txt_passActual)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_passActual)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(389, 3)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(293, 162)
        Me.UltraGroupBox1.TabIndex = 5
        Me.UltraGroupBox1.Text = "Cambiar contraseña"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(274, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "*"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(274, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(11, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "*"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(274, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(11, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "*"
        '
        'btn_cambiar
        '
        Me.btn_cambiar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cambiar.Location = New System.Drawing.Point(198, 127)
        Me.btn_cambiar.Name = "btn_cambiar"
        Me.btn_cambiar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cambiar.TabIndex = 4
        Me.btn_cambiar.Text = "Cambiar"
        Me.btn_cambiar.UseVisualStyleBackColor = False
        '
        'txt_passNew2
        '
        Me.txt_passNew2.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_passNew2.Location = New System.Drawing.Point(112, 100)
        Me.txt_passNew2.Name = "txt_passNew2"
        Me.txt_passNew2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_passNew2.Size = New System.Drawing.Size(161, 19)
        Me.txt_passNew2.TabIndex = 3
        '
        'lbl_repetirPass
        '
        Me.lbl_repetirPass.AutoSize = True
        Me.lbl_repetirPass.BackColor = System.Drawing.Color.Transparent
        Me.lbl_repetirPass.Location = New System.Drawing.Point(6, 104)
        Me.lbl_repetirPass.Name = "lbl_repetirPass"
        Me.lbl_repetirPass.Size = New System.Drawing.Size(100, 13)
        Me.lbl_repetirPass.TabIndex = 0
        Me.lbl_repetirPass.Text = "Repetir contraseña:"
        '
        'txt_passNew
        '
        Me.txt_passNew.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_passNew.Location = New System.Drawing.Point(112, 73)
        Me.txt_passNew.Name = "txt_passNew"
        Me.txt_passNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_passNew.Size = New System.Drawing.Size(161, 19)
        Me.txt_passNew.TabIndex = 2
        '
        'lbl_nuevoPass
        '
        Me.lbl_nuevoPass.AutoSize = True
        Me.lbl_nuevoPass.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nuevoPass.Location = New System.Drawing.Point(6, 77)
        Me.lbl_nuevoPass.Name = "lbl_nuevoPass"
        Me.lbl_nuevoPass.Size = New System.Drawing.Size(98, 13)
        Me.lbl_nuevoPass.TabIndex = 0
        Me.lbl_nuevoPass.Text = "Nueva contraseña:"
        '
        'txt_passActual
        '
        Me.txt_passActual.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_passActual.Location = New System.Drawing.Point(112, 19)
        Me.txt_passActual.Name = "txt_passActual"
        Me.txt_passActual.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_passActual.Size = New System.Drawing.Size(161, 19)
        Me.txt_passActual.TabIndex = 1
        '
        'lbl_passActual
        '
        Me.lbl_passActual.AutoSize = True
        Me.lbl_passActual.BackColor = System.Drawing.Color.Transparent
        Me.lbl_passActual.Location = New System.Drawing.Point(6, 23)
        Me.lbl_passActual.Name = "lbl_passActual"
        Me.lbl_passActual.Size = New System.Drawing.Size(96, 13)
        Me.lbl_passActual.TabIndex = 0
        Me.lbl_passActual.Text = "Contraseña actual:"
        '
        'btn_guardar
        '
        Me.btn_guardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_guardar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_guardar.Location = New System.Drawing.Point(244, 130)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(129, 23)
        Me.btn_guardar.TabIndex = 4
        Me.btn_guardar.Text = "Guardar cambios"
        Me.btn_guardar.UseVisualStyleBackColor = False
        '
        'lbl_correo
        '
        Me.lbl_correo.AutoSize = True
        Me.lbl_correo.Location = New System.Drawing.Point(3, 25)
        Me.lbl_correo.Name = "lbl_correo"
        Me.lbl_correo.Size = New System.Drawing.Size(96, 13)
        Me.lbl_correo.TabIndex = 0
        Me.lbl_correo.Text = "Correo electrónico:"
        '
        'txt_correo
        '
        Me.txt_correo.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_correo.Location = New System.Drawing.Point(123, 21)
        Me.txt_correo.Name = "txt_correo"
        Me.txt_correo.Size = New System.Drawing.Size(250, 19)
        Me.txt_correo.TabIndex = 1
        '
        'lbl_nombreAgencia
        '
        Me.lbl_nombreAgencia.AutoSize = True
        Me.lbl_nombreAgencia.Enabled = False
        Me.lbl_nombreAgencia.Location = New System.Drawing.Point(3, 52)
        Me.lbl_nombreAgencia.Name = "lbl_nombreAgencia"
        Me.lbl_nombreAgencia.Size = New System.Drawing.Size(114, 13)
        Me.lbl_nombreAgencia.TabIndex = 0
        Me.lbl_nombreAgencia.Text = "Nombre de la agencia:"
        '
        'txt_agencia
        '
        Me.txt_agencia.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_agencia.Enabled = False
        Me.txt_agencia.Location = New System.Drawing.Point(123, 48)
        Me.txt_agencia.Name = "txt_agencia"
        Me.txt_agencia.Size = New System.Drawing.Size(202, 19)
        Me.txt_agencia.TabIndex = 2
        '
        'txt_agente
        '
        Me.txt_agente.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_agente.Enabled = False
        Me.txt_agente.Location = New System.Drawing.Point(123, 75)
        Me.txt_agente.Name = "txt_agente"
        Me.txt_agente.Size = New System.Drawing.Size(202, 19)
        Me.txt_agente.TabIndex = 3
        '
        'lbl_nombreAgente
        '
        Me.lbl_nombreAgente.AutoSize = True
        Me.lbl_nombreAgente.Enabled = False
        Me.lbl_nombreAgente.Location = New System.Drawing.Point(3, 79)
        Me.lbl_nombreAgente.Name = "lbl_nombreAgente"
        Me.lbl_nombreAgente.Size = New System.Drawing.Size(100, 13)
        Me.lbl_nombreAgente.TabIndex = 0
        Me.lbl_nombreAgente.Text = "Nombre del agente:"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(600, 206)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancelar.TabIndex = 1
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'UltraTabControl1
        '
        Me.UltraTabControl1.Controls.Add(Me.UltraTabSharedControlsPage1)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControl3)
        Me.UltraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UltraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.UltraTabControl1.Name = "UltraTabControl1"
        Me.UltraTabControl1.SharedControlsPage = Me.UltraTabSharedControlsPage1
        Me.UltraTabControl1.Size = New System.Drawing.Size(687, 194)
        Me.UltraTabControl1.TabIndex = 0
        UltraTab3.TabPage = Me.UltraTabPageControl3
        UltraTab3.Text = "Configurar agente"
        Me.UltraTabControl1.Tabs.AddRange(New Infragistics.Win.UltraWinTabControl.UltraTab() {UltraTab3})
        Me.UltraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007
        '
        'UltraTabSharedControlsPage1
        '
        Me.UltraTabSharedControlsPage1.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabSharedControlsPage1.Name = "UltraTabSharedControlsPage1"
        Me.UltraTabSharedControlsPage1.Size = New System.Drawing.Size(685, 171)
        '
        'configurarAgente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(687, 241)
        Me.Controls.Add(Me.UltraTabControl1)
        Me.Controls.Add(Me.btn_cancelar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "configurarAgente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configurar agente"
        Me.UltraTabPageControl3.ResumeLayout(False)
        Me.UltraTabPageControl3.PerformLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.txt_passNew2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_passNew, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_passActual, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_correo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_agencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_agente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbl_correo As System.Windows.Forms.Label
    Friend WithEvents txt_correo As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_agencia As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nombreAgencia As System.Windows.Forms.Label
    Friend WithEvents txt_agente As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nombreAgente As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents btn_cambiar As System.Windows.Forms.Button
    Friend WithEvents txt_passNew2 As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_repetirPass As System.Windows.Forms.Label
    Friend WithEvents txt_passNew As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nuevoPass As System.Windows.Forms.Label
    Friend WithEvents txt_passActual As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_passActual As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_guardar As System.Windows.Forms.Button
    Friend WithEvents UltraTabControl1 As Infragistics.Win.UltraWinTabControl.UltraTabControl
    Friend WithEvents UltraTabSharedControlsPage1 As Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
    Friend WithEvents UltraTabPageControl3 As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
