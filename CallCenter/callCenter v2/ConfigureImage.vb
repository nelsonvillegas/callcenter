Public Class ConfigureImage

    Private SeleccionoImagen As Boolean = False
    Private BorroImagen As Boolean = False
    Public OriginalFileName As String

    Private Sub ConfigureImage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CapaPresentacion.Idiomas.cambiar_ConfigureImage(Me)

        txt_nombre.Text = CapaAccesoDatos.XML.localData.NombreSistema
        If CapaAccesoDatos.XML.localData.Imagen = "" OrElse Not IO.File.Exists(CapaAccesoDatos.XML.localData.Imagen_name) Then
            DefaultImage()
        Else
            'pic_imagen.Image = System.Drawing.Image.FromFile(imgName_Imagen)

            Using ms As New IO.MemoryStream(IO.File.ReadAllBytes(CapaAccesoDatos.XML.localData.Imagen_name))
                pic_imagen.Image = Image.FromStream(ms)
            End Using
        End If
        'If herramientas.localData.Imagen = "" Then pic_logo.Image = frm.PictureBox3.Image Else pic_logo.Image = System.Drawing.Image.FromFile(imgName_Logo)
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        Try
            CapaAccesoDatos.XML.localData.NombreSistema = txt_nombre.Text
            misForms.form1.Text = txt_nombre.Text
            'herramientas.localData.Leyenda = txt_leyenda.Text
            If SeleccionoImagen Then
                CapaAccesoDatos.XML.localData.Imagen = CapaAccesoDatos.XML.localData.Imagen_name

                Dim img As Image = pic_imagen.Image.Clone
                pic_imagen.Image = Nothing
                pic_imagen = Nothing

                If IO.File.Exists(CapaAccesoDatos.XML.localData.Imagen_name) Then IO.File.Delete(CapaAccesoDatos.XML.localData.Imagen_name)
                'img.Save(imgName_Imagen, System.Drawing.Imaging.ImageFormat.Png)
                If IO.File.Exists(OriginalFileName) Then IO.File.Copy(OriginalFileName, CapaAccesoDatos.XML.localData.Imagen_name)
            End If

            If BorroImagen Then
                DefaultImage()

                Try
                    If IO.File.Exists(CapaAccesoDatos.XML.localData.Imagen) Then IO.File.Delete(CapaAccesoDatos.XML.localData.Imagen)
                Catch ex As Exception
                End Try

                CapaAccesoDatos.XML.localData.Imagen = ""
            End If

            'herramientas.localData.Logo = 
            Close()
        Catch ex As Exception
            'herramientas.localData.Imagen = imgName_Imagen2

            'If IO.File.Exists(imgName_Imagen2) Then IO.File.Delete(imgName_Imagen2)
            'IO.File.Copy(imgName_Imagen, imgName_Imagen2)

            'Dim img As Image = Image.FromFile(imgName_Imagen2)
            'pic_imagen.Image = Nothing

            'If IO.File.Exists(imgName_Imagen2) Then IO.File.Delete(imgName_Imagen2)
            'img.Save(imgName_Imagen2)
        End Try
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub btn_selImagen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selImagen.Click
        OpenFileDialog1.FileName = ""
        Dim res As DialogResult = OpenFileDialog1.ShowDialog()
        If res = Windows.Forms.DialogResult.OK Then
            OriginalFileName = OpenFileDialog1.FileName

            'Dim img As System.Drawing.Image = System.Drawing.Image.FromFile(OriginalFileName)
            'pic_imagen.Image = img.Clone

            Using ms As New IO.MemoryStream(IO.File.ReadAllBytes(OriginalFileName))
                pic_imagen.Image = Image.FromStream(ms)
            End Using

            SeleccionoImagen = True
            BorroImagen = False
        End If
    End Sub

    Private Sub btn_eliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_eliminar.Click
        BorroImagen = True
        SeleccionoImagen = False
        DefaultImage()
    End Sub

    Private Sub DefaultImage()
        Dim frm As New login
        pic_imagen.Image = frm.PictureBox1.Image
    End Sub

End Class
