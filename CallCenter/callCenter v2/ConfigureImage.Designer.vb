<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigureImage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ConfigureImage))
        Me.txt_nombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_nombre = New System.Windows.Forms.Label
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.lbl_imagen = New System.Windows.Forms.Label
        Me.btn_selImagen = New System.Windows.Forms.Button
        Me.pic_imagen = New System.Windows.Forms.PictureBox
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.Label1 = New System.Windows.Forms.Label
        Me.btn_eliminar = New System.Windows.Forms.Button
        CType(Me.txt_nombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_nombre
        '
        Me.txt_nombre.AlwaysInEditMode = True
        Me.txt_nombre.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_nombre.Location = New System.Drawing.Point(137, 12)
        Me.txt_nombre.MaxLength = 50
        Me.txt_nombre.Name = "txt_nombre"
        Me.txt_nombre.Size = New System.Drawing.Size(343, 19)
        Me.txt_nombre.TabIndex = 1
        '
        'lbl_nombre
        '
        Me.lbl_nombre.AutoSize = True
        Me.lbl_nombre.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nombre.Location = New System.Drawing.Point(15, 15)
        Me.lbl_nombre.Name = "lbl_nombre"
        Me.lbl_nombre.Size = New System.Drawing.Size(102, 13)
        Me.lbl_nombre.TabIndex = 0
        Me.lbl_nombre.Text = "Nombre del sistema:"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(396, 186)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(90, 23)
        Me.btn_cancelar.TabIndex = 6
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.Location = New System.Drawing.Point(261, 186)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(129, 23)
        Me.btn_aceptar.TabIndex = 5
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = True
        '
        'lbl_imagen
        '
        Me.lbl_imagen.AutoSize = True
        Me.lbl_imagen.BackColor = System.Drawing.Color.Transparent
        Me.lbl_imagen.Location = New System.Drawing.Point(15, 42)
        Me.lbl_imagen.Name = "lbl_imagen"
        Me.lbl_imagen.Size = New System.Drawing.Size(45, 13)
        Me.lbl_imagen.TabIndex = 0
        Me.lbl_imagen.Text = "Imagen:"
        '
        'btn_selImagen
        '
        Me.btn_selImagen.Location = New System.Drawing.Point(137, 37)
        Me.btn_selImagen.Name = "btn_selImagen"
        Me.btn_selImagen.Size = New System.Drawing.Size(75, 23)
        Me.btn_selImagen.TabIndex = 3
        Me.btn_selImagen.Text = "Seleccionar"
        Me.btn_selImagen.UseVisualStyleBackColor = True
        '
        'pic_imagen
        '
        Me.pic_imagen.BackColor = System.Drawing.Color.Transparent
        Me.pic_imagen.Image = CType(resources.GetObject("pic_imagen.Image"), System.Drawing.Image)
        Me.pic_imagen.Location = New System.Drawing.Point(137, 66)
        Me.pic_imagen.Name = "pic_imagen"
        Me.pic_imagen.Size = New System.Drawing.Size(343, 109)
        Me.pic_imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pic_imagen.TabIndex = 48
        Me.pic_imagen.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(417, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "343, 109 px"
        '
        'btn_eliminar
        '
        Me.btn_eliminar.Location = New System.Drawing.Point(218, 37)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(75, 23)
        Me.btn_eliminar.TabIndex = 50
        Me.btn_eliminar.Text = "Eliminar"
        Me.btn_eliminar.UseVisualStyleBackColor = True
        '
        'ConfigureImage
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(498, 221)
        Me.Controls.Add(Me.btn_eliminar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pic_imagen)
        Me.Controls.Add(Me.btn_selImagen)
        Me.Controls.Add(Me.lbl_imagen)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Controls.Add(Me.txt_nombre)
        Me.Controls.Add(Me.lbl_nombre)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ConfigureImage"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configurar imagen"
        CType(Me.txt_nombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_imagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_nombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nombre As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents lbl_imagen As System.Windows.Forms.Label
    Friend WithEvents btn_selImagen As System.Windows.Forms.Button
    Friend WithEvents pic_imagen As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_eliminar As System.Windows.Forms.Button
End Class
