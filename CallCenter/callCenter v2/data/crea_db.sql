use ventanaCallCenter
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consulta_paises]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create PROCEDURE [dbo].[consulta_paises]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT idPais,nombre FROM paises ORDER BY nombre
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[info]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[info](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [smalldatetime] NULL,
	[fecha_catalogos] [smalldatetime] NULL,
	[db_version] [int] NULL,
 CONSTRAINT [PK_info] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[infocorp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[infocorp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [smalldatetime] NULL,
	[idCorporativo] [int] NULL
 CONSTRAINT [PK_infocorp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[actualizar_cancelada]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[actualizar_cancelada]
	@noConfirm as nvarchar(15)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE reservaciones SET status=3 WHERE noConfirm=@noConfirm
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Aeropuertos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Aeropuertos](
	[IdAeropuerto] [int] NOT NULL,
	[IdCiudad] [int] NULL,
	[IATA] [char](3) NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[IdDiccionario] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ciudades]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Ciudades](
	[IdCiudad] [int] NOT NULL,
	[IdMunicipio] [int] NOT NULL,
	[Nombre] [nvarchar](30) NOT NULL,
	[codigo] [char](3) NULL,
	[latitud] [float] NULL,
	[longitud] [float] NULL,
	[IdDiccionario] [int] NULL,
	[keywords] [nvarchar](200) NULL,
	[Distancia] [decimal](18, 2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Estados]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Estados](
	[IdEstado] [int] NOT NULL,
	[IdPais] [char](2) NOT NULL,
	[Nombre] [nvarchar](30) NOT NULL,
	[codigo] [char](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Municipios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Municipios](
	[IdMunicipio] [int] NOT NULL,
	[IdEstado] [int] NOT NULL,
	[Nombre] [nvarchar](30) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Paises]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Paises](
	[IdPais] [char](2) NOT NULL,
	[IdZona] [int] NULL,
	[Nombre] [nvarchar](30) NOT NULL,
	[LDCode] [int] NULL,
	[IdDiccionario] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Zonas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Zonas](
	[IdZona] [int] NOT NULL,
	[nombre] [nvarchar](30) NOT NULL,
	[idDiccionario] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PlacesDictionary]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[PlacesDictionary](
	[IdDiccionario] [int] NOT NULL,
	[IdIdioma] [int] NOT NULL,
	[Texto] [nvarchar](100) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Areas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Areas](
	[IdArea] [int] NOT NULL,
	[nombre] [nvarchar](40) NOT NULL,
	[idDiccionarioNombre] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Diccionario]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Diccionario](
	[IdDiccionario] [int] NOT NULL,
	[IdIdioma] [int] NOT NULL,
	[Texto] [nvarchar](4000) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AreasCiudad]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AreasCiudad](
	[idCiudad] [int] NOT NULL,
	[IdArea] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[insertar_reservacion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[insertar_reservacion]
	@fecha as smalldatetime,
	@nombres nvarchar(100),
	@telefono nvarchar(20),
	@correo nvarchar(50),
	@noConfirm as nvarchar(15),
	@monto as money,
	@hotelName as nvarchar(100),
	@fecha_in as smalldatetime,
	@fecha_out as smalldatetime,
	@ChainCode as nvarchar(2),
	@PropertyNumber as int,
	@status as int,
	@currency as nvarchar(3)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO reservaciones values(@fecha, @nombres, @telefono, @correo, @noConfirm, @monto, @hotelName, @fecha_in, @fecha_out, @ChainCode, @PropertyNumber, @status, @currency)
END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consulta_reservaciones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[consulta_reservaciones]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		convert(nvarchar,fecha,112) fecha,
		nombres,
		telefono,
		correo,
		noConfirm,
		''$''+cast(monto as nvarchar)+'' ''+currency as monto,
		hotelName,
		convert(nvarchar,fecha_in,112) fecha_in,
		convert(nvarchar,fecha_out,112) fecha_out, 
		ChainCode, 
		PropertyNumber, 
		(case 
			when status=1 then ''Reservada'' 
			when status=3 then ''Cancelada''
			when status=4 then ''En proceso''
		end) cancelado
	FROM 
		reservaciones 
	ORDER BY fecha DESC
END



' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[actualizar_info]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[actualizar_info]
AS
BEGIN
	SET NOCOUNT ON;

	if not exists(select * from info)
		insert into info values(getdate(),getdate(),11)
	else
		update info set fecha=getdate()
END



' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consultar_info]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[consultar_info]
AS
BEGIN
	SET NOCOUNT ON;

	if not exists(select * from info)
	begin
			declare @fecha as smalldatetime
			set @fecha = YEAR(0)
			insert into info values(@fecha,@fecha,11)
			select @fecha fecha,11 db_version
	end
	else
		select fecha,db_version from info
END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[borra_destinos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[borra_destinos]
AS
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID (''dbo.Aeropuertos'', ''U'')		IS NOT NULL delete from dbo.Aeropuertos;
	IF OBJECT_ID (''dbo.Ciudades'', ''U'')			IS NOT NULL delete from dbo.Ciudades;
	IF OBJECT_ID (''dbo.Estados'', ''U'')			IS NOT NULL delete from dbo.Estados;
	IF OBJECT_ID (''dbo.Municipios'', ''U'')		IS NOT NULL delete from dbo.Municipios;
	IF OBJECT_ID (''dbo.Paises'', ''U'')			IS NOT NULL delete from dbo.Paises;
	IF OBJECT_ID (''dbo.Zonas'', ''U'')				IS NOT NULL delete from dbo.Zonas;
	IF OBJECT_ID (''dbo.PlacesDictionary'', ''U'')	IS NOT NULL delete from dbo.PlacesDictionary;
	IF OBJECT_ID (''dbo.Areas'', ''U'')				IS NOT NULL delete from dbo.Areas;
	IF OBJECT_ID (''dbo.Diccionario'', ''U'')				IS NOT NULL delete from dbo.Diccionario;
	IF OBJECT_ID (''dbo.AreasCiudad'', ''U'')				IS NOT NULL delete from dbo.AreasCiudad;
END


' 
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consulta_lugares]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[consulta_lugares]
	@es_ciudades as bit,
	@nombre_lugar as nvarchar(50),
	@nombre_pais  as nvarchar(50),
	@idioma as int
AS
BEGIN
	SET NOCOUNT ON;

	IF @es_ciudades=1
	BEGIN
		SELECT 
			top 10 c.idCiudad,
			c.nombre ciudad,
			p.nombre pais,
			c.codigo codigo,
			d.texto ciudad_idioma,
			c.distancia distancia
		FROM 
			ciudades							c
			inner join municipios				m on m.idMunicipio=c.idMunicipio
			inner join estados					e on e.idEstado=m.idEstado
			inner join paises					p on p.idPais=e.idPais
			LEFT OUTER JOIN PlacesDictionary	d ON c.iddiccionario=d.iddiccionario AND d.ididioma=@idioma
		WHERE (
				c.nombre	COLLATE SQL_Latin1_General_CP1_CI_AI like ''%'' + @nombre_lugar + ''%'' OR
				--d.texto		COLLATE SQL_Latin1_General_CP1_CI_AI like ''%'' + @nombre_lugar + ''%'' OR
				c.keywords	COLLATE SQL_Latin1_General_CP1_CI_AI like ''%'' + @nombre_lugar + ''%''
			  ) AND
		      p.nombre COLLATE SQL_Latin1_General_CP1_CI_AI like ''%'' + @nombre_pais + ''%'' 
		ORDER BY c.nombre,p.nombre
	END
	ELSE
	BEGIN
		SELECT 
			top 10 a.idAeropuerto,
			a.Nombre,
			a.IATA,
			d.texto aeropuerto_idioma
		FROM 
			aeropuertos							a
			LEFT OUTER JOIN PlacesDictionary	d ON a.iddiccionario=d.iddiccionario AND d.ididioma=@idioma
		WHERE 
			( 
				a.Nombre COLLATE SQL_Latin1_General_CP1_CI_AI like ''%'' + @nombre_lugar + ''%'' OR
				d.texto  COLLATE SQL_Latin1_General_CP1_CI_AI like ''%'' + @nombre_lugar + ''%''
			)
	END
END
' 
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[uvcc_mediospromocion]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[uvcc_mediospromocion](
	[id_MedioPromocion] [int] NOT NULL,
	[idCorporativo] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[uvcc_medios]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[uvcc_medios](
	[id_Medio] [int] NOT NULL,
	[idCorporativo] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[uvcc_empresas]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[uvcc_empresas](
	[id_Medio] [int] NULL,
	[id_Empresa] [int] NULL,
	[idCorporativo] [int] NULL,
	[Nombre] [nvarchar](50) NULL,
	[id_Segmento] [int] NULL,
	[Codigo] [nvarchar](20) NULL,
	[Domicilio] [nvarchar](80) NULL,
	[Ciudad] [nvarchar](50) NULL,
	[CP] [nvarchar](10) NULL,
	[Telefono] [nvarchar](20) NULL,
	[RFC] [nvarchar](15) NULL,
	[Tipo] [nvarchar](2) NULL,
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[uvcc_segmentos]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[uvcc_segmentos](
	[id_Segmento] [int] NULL,
	[idCorporativo] [int] NULL,
	[Nombre] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[actualizar_info_catalogos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[actualizar_info_catalogos]
@idCorporativo as int
AS
BEGIN
	SET NOCOUNT ON;

	if not exists(select * from infocorp where idCorporativo=@idCorporativo)
		insert into infocorp values(getdate(),@idCorporativo)
	else
		update infocorp set fecha=getdate() where idCorporativo=@idCorporativo
END



' 
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[borra_catalogos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[borra_catalogos] 
@idCorporativo as int
AS
BEGIN
	SET NOCOUNT ON;
	IF OBJECT_ID (''dbo.uvcc_mediospromocion'', ''U'')		IS NOT NULL delete from dbo.uvcc_mediospromocion where idCorporativo=@idCorporativo;
	IF OBJECT_ID (''dbo.uvcc_medios'', ''U'')		IS NOT NULL delete from dbo.uvcc_medios where idCorporativo=@idCorporativo;
	IF OBJECT_ID (''dbo.uvcc_empresas'', ''U'')			IS NOT NULL delete from dbo.uvcc_empresas where idCorporativo=@idCorporativo;
	IF OBJECT_ID (''dbo.uvcc_segmentos'', ''U'')			IS NOT NULL delete from dbo.uvcc_segmentos where idCorporativo=@idCorporativo;
END


' 
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[consultar_infocorp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[consultar_infocorp]
@idCorporativo as int
AS
BEGIN
	SET NOCOUNT ON;

	if not exists(select * from infocorp where idCorporativo=@idCorporativo)
	begin
			declare @fecha as smalldatetime
			set @fecha = YEAR(0)
			insert into infocorp values(@fecha,@idCorporativo)
			select @fecha fecha
	end
	else
		select fecha from infocorp  where idCorporativo=@idCorporativo
END
' 
END
GO
