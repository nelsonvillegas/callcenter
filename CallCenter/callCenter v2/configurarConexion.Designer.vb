<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class configurarConexion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim UltraTab3 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab
        Me.UltraTabPageControl3 = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl
        Me.lbl_notaPassport = New System.Windows.Forms.Label
        Me.lbl_notaSQL = New System.Windows.Forms.Label
        Me.txt_servidorSQL = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label5 = New System.Windows.Forms.Label
        Me.txt_teUpdate = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_wsDestinosUpdate = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label4 = New System.Windows.Forms.Label
        Me.txt_teGalileo = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label1 = New System.Windows.Forms.Label
        Me.txt_wsAdminCalls = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_tePassport = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label2 = New System.Windows.Forms.Label
        Me.txt_wsPassport = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_wsServices = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_tiempoEspera = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txt_teHotels = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.btn_guardar = New System.Windows.Forms.Button
        Me.UltraTabControl1 = New Infragistics.Win.UltraWinTabControl.UltraTabControl
        Me.UltraTabSharedControlsPage1 = New Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
        Me.UltraTabPageControl3.SuspendLayout()
        CType(Me.txt_servidorSQL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_teUpdate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_wsDestinosUpdate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_teGalileo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_wsAdminCalls, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_tePassport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_wsPassport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_wsServices, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_teHotels, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraTabPageControl3
        '
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_notaPassport)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_notaSQL)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_servidorSQL)
        Me.UltraTabPageControl3.Controls.Add(Me.Label5)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_teUpdate)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_wsDestinosUpdate)
        Me.UltraTabPageControl3.Controls.Add(Me.Label4)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_teGalileo)
        Me.UltraTabPageControl3.Controls.Add(Me.Label1)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_wsAdminCalls)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_tePassport)
        Me.UltraTabPageControl3.Controls.Add(Me.Label2)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_wsPassport)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_wsServices)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_tiempoEspera)
        Me.UltraTabPageControl3.Controls.Add(Me.Label3)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_teHotels)
        Me.UltraTabPageControl3.Location = New System.Drawing.Point(1, 22)
        Me.UltraTabPageControl3.Name = "UltraTabPageControl3"
        Me.UltraTabPageControl3.Size = New System.Drawing.Size(701, 210)
        '
        'lbl_notaPassport
        '
        Me.lbl_notaPassport.AutoSize = True
        Me.lbl_notaPassport.BackColor = System.Drawing.Color.Transparent
        Me.lbl_notaPassport.Location = New System.Drawing.Point(100, 179)
        Me.lbl_notaPassport.Name = "lbl_notaPassport"
        Me.lbl_notaPassport.Size = New System.Drawing.Size(187, 13)
        Me.lbl_notaPassport.TabIndex = 13
        Me.lbl_notaPassport.Text = "* Requiere reiniciar Univisit Call Center"
        '
        'lbl_notaSQL
        '
        Me.lbl_notaSQL.AutoSize = True
        Me.lbl_notaSQL.BackColor = System.Drawing.Color.Transparent
        Me.lbl_notaSQL.Location = New System.Drawing.Point(100, 192)
        Me.lbl_notaSQL.Name = "lbl_notaSQL"
        Me.lbl_notaSQL.Size = New System.Drawing.Size(208, 13)
        Me.lbl_notaSQL.TabIndex = 12
        Me.lbl_notaSQL.Text = "** El nombre de la base de datos debe ser:"
        '
        'txt_servidorSQL
        '
        Me.txt_servidorSQL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_servidorSQL.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_servidorSQL.Location = New System.Drawing.Point(103, 157)
        Me.txt_servidorSQL.Name = "txt_servidorSQL"
        Me.txt_servidorSQL.Size = New System.Drawing.Size(508, 19)
        Me.txt_servidorSQL.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(11, 161)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Servidor SQL**:"
        '
        'txt_teUpdate
        '
        Me.txt_teUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_teUpdate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_teUpdate.Location = New System.Drawing.Point(617, 104)
        Me.txt_teUpdate.Name = "txt_teUpdate"
        Me.txt_teUpdate.Size = New System.Drawing.Size(74, 19)
        Me.txt_teUpdate.TabIndex = 9
        Me.txt_teUpdate.Visible = False
        '
        'txt_wsDestinosUpdate
        '
        Me.txt_wsDestinosUpdate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_wsDestinosUpdate.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_wsDestinosUpdate.Location = New System.Drawing.Point(103, 104)
        Me.txt_wsDestinosUpdate.Name = "txt_wsDestinosUpdate"
        Me.txt_wsDestinosUpdate.Size = New System.Drawing.Size(508, 19)
        Me.txt_wsDestinosUpdate.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(11, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "DestinosUpdate:"
        '
        'txt_teGalileo
        '
        Me.txt_teGalileo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_teGalileo.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_teGalileo.Location = New System.Drawing.Point(617, 79)
        Me.txt_teGalileo.Name = "txt_teGalileo"
        Me.txt_teGalileo.Size = New System.Drawing.Size(74, 19)
        Me.txt_teGalileo.TabIndex = 6
        Me.txt_teGalileo.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(11, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "WsServices:"
        '
        'txt_wsAdminCalls
        '
        Me.txt_wsAdminCalls.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_wsAdminCalls.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_wsAdminCalls.Location = New System.Drawing.Point(103, 79)
        Me.txt_wsAdminCalls.Name = "txt_wsAdminCalls"
        Me.txt_wsAdminCalls.Size = New System.Drawing.Size(508, 19)
        Me.txt_wsAdminCalls.TabIndex = 5
        '
        'txt_tePassport
        '
        Me.txt_tePassport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_tePassport.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_tePassport.Location = New System.Drawing.Point(617, 52)
        Me.txt_tePassport.Name = "txt_tePassport"
        Me.txt_tePassport.Size = New System.Drawing.Size(74, 19)
        Me.txt_tePassport.TabIndex = 4
        Me.txt_tePassport.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(11, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "WsPassport*:"
        '
        'txt_wsPassport
        '
        Me.txt_wsPassport.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_wsPassport.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_wsPassport.Location = New System.Drawing.Point(103, 52)
        Me.txt_wsPassport.Name = "txt_wsPassport"
        Me.txt_wsPassport.Size = New System.Drawing.Size(508, 19)
        Me.txt_wsPassport.TabIndex = 3
        '
        'txt_wsServices
        '
        Me.txt_wsServices.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_wsServices.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_wsServices.Location = New System.Drawing.Point(103, 25)
        Me.txt_wsServices.Name = "txt_wsServices"
        Me.txt_wsServices.Size = New System.Drawing.Size(508, 19)
        Me.txt_wsServices.TabIndex = 1
        '
        'lbl_tiempoEspera
        '
        Me.lbl_tiempoEspera.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_tiempoEspera.AutoSize = True
        Me.lbl_tiempoEspera.BackColor = System.Drawing.Color.Transparent
        Me.lbl_tiempoEspera.Location = New System.Drawing.Point(614, 9)
        Me.lbl_tiempoEspera.Name = "lbl_tiempoEspera"
        Me.lbl_tiempoEspera.Size = New System.Drawing.Size(77, 13)
        Me.lbl_tiempoEspera.TabIndex = 0
        Me.lbl_tiempoEspera.Text = "Tiempo espera"
        Me.lbl_tiempoEspera.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(11, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "AdminCalls:"
        '
        'txt_teHotels
        '
        Me.txt_teHotels.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_teHotels.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_teHotels.Location = New System.Drawing.Point(617, 25)
        Me.txt_teHotels.Name = "txt_teHotels"
        Me.txt_teHotels.Size = New System.Drawing.Size(74, 19)
        Me.txt_teHotels.TabIndex = 2
        Me.txt_teHotels.Visible = False
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(617, 239)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancelar.TabIndex = 2
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'btn_guardar
        '
        Me.btn_guardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_guardar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_guardar.Location = New System.Drawing.Point(499, 239)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(112, 23)
        Me.btn_guardar.TabIndex = 1
        Me.btn_guardar.Text = "Guardar cambios"
        Me.btn_guardar.UseVisualStyleBackColor = False
        '
        'UltraTabControl1
        '
        Me.UltraTabControl1.Controls.Add(Me.UltraTabSharedControlsPage1)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControl3)
        Me.UltraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UltraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.UltraTabControl1.Name = "UltraTabControl1"
        Me.UltraTabControl1.SharedControlsPage = Me.UltraTabSharedControlsPage1
        Me.UltraTabControl1.Size = New System.Drawing.Size(703, 233)
        Me.UltraTabControl1.TabIndex = 0
        UltraTab3.TabPage = Me.UltraTabPageControl3
        UltraTab3.Text = "Servicios web"
        Me.UltraTabControl1.Tabs.AddRange(New Infragistics.Win.UltraWinTabControl.UltraTab() {UltraTab3})
        Me.UltraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007
        '
        'UltraTabSharedControlsPage1
        '
        Me.UltraTabSharedControlsPage1.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabSharedControlsPage1.Name = "UltraTabSharedControlsPage1"
        Me.UltraTabSharedControlsPage1.Size = New System.Drawing.Size(701, 210)
        '
        'configurarConexion
        '
        Me.AcceptButton = Me.btn_guardar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(703, 274)
        Me.Controls.Add(Me.UltraTabControl1)
        Me.Controls.Add(Me.btn_guardar)
        Me.Controls.Add(Me.btn_cancelar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "configurarConexion"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configurar conexi�n"
        Me.UltraTabPageControl3.ResumeLayout(False)
        Me.UltraTabPageControl3.PerformLayout()
        CType(Me.txt_servidorSQL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_teUpdate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_wsDestinosUpdate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_teGalileo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_wsAdminCalls, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_tePassport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_wsPassport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_wsServices, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_teHotels, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_wsAdminCalls As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_wsPassport As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_wsServices As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_teHotels As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_tiempoEspera As System.Windows.Forms.Label
    Friend WithEvents txt_teGalileo As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_tePassport As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_guardar As System.Windows.Forms.Button
    Friend WithEvents UltraTabControl1 As Infragistics.Win.UltraWinTabControl.UltraTabControl
    Friend WithEvents UltraTabSharedControlsPage1 As Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
    Friend WithEvents UltraTabPageControl3 As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Friend WithEvents txt_teUpdate As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_wsDestinosUpdate As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_servidorSQL As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lbl_notaSQL As System.Windows.Forms.Label
    Friend WithEvents lbl_notaPassport As System.Windows.Forms.Label
End Class
