Imports System.Net.Mail
Imports System.Text

Public Class datosCliente

    Private afecta As Boolean
    Public ModificandoReservaHotel As Boolean
    Public Preguntar As Boolean
    Private credit As String
    Private creditCard As String
    Public creditCardFrom As Boolean = True

    ''' <summary>
    ''' Se utiliza cuando se muestran u ocultan la solicitud de datos de la tarjeta de credito
    ''' </summary>
    ''' <remarks>MinWindowHeight, MaxWindowHeight</remarks>
    Const MinWindowHeight As Integer = 369
    Const MaxWindowHeight As Integer = 581 '557

    Private Sub datosCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        clear_controls(Me)
        CapaPresentacion.Common.cargar_paises(uce_paises)
        CapaPresentacion.Common.cargar_estados(uce_estados, uce_paises.Value)

        inicializa_controles()
        If misForms.newCall.datos_call.reservacion_paquete.cc_tipo = Nothing Then uce_tipoTarjeta.SelectedIndex = 0
        Me.txt_estado.Visible = False
        Me.uce_estados.Visible = True

        btn_importar.Enabled = False
        For Each item As dataBusqueda_vuelo In misForms.newCall.datos_call.reservacion_paquete.lista_Vuelos
            If Not item.confirmada Then
                If Not item.entrega_tipo Then btn_importar.Enabled = True
            End If
        Next

        If txt_ccNombre.Text = "" AndAlso txt_ccApellido.Text = "" Then
            afecta = True
        Else
            afecta = False
        End If

        If creditCardFrom = True Then
            UltraGroupBox1.Visible = True
        Else
            UltraGroupBox1.Visible = False
        End If


        CapaPresentacion.Idiomas.cambiar_datosCliente(Me)
        If Not misForms.form1.datoscliente Is Nothing Then
            misForms.newCall.datos_call.reservacion_paquete.cli_nombre = misForms.form1.datoscliente.cli_nombre
            misForms.newCall.datos_call.reservacion_paquete.cli_apellido = misForms.form1.datoscliente.cli_apellido
            misForms.newCall.datos_call.reservacion_paquete.cli_direccion = misForms.form1.datoscliente.cli_direccion
            misForms.newCall.datos_call.reservacion_paquete.cli_ciudad = misForms.form1.datoscliente.cli_ciudad
            misForms.newCall.datos_call.reservacion_paquete.cli_estado = misForms.form1.datoscliente.cli_estado
            misForms.newCall.datos_call.reservacion_paquete.cli_cp = misForms.form1.datoscliente.cli_cp
            misForms.newCall.datos_call.reservacion_paquete.cli_pais = misForms.form1.datoscliente.cli_pais

            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = misForms.form1.datoscliente.cli_telefono_AreaCityCode
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = misForms.form1.datoscliente.cli_telefono_PhoneNumber
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = misForms.form1.datoscliente.cli_telefono_PhoneUseType

            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = misForms.form1.datoscliente.cli_telefono_adicional_AreaCityCode
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = misForms.form1.datoscliente.cli_telefono_adicional_PhoneNumber

            misForms.newCall.datos_call.reservacion_paquete.cli_email = misForms.form1.datoscliente.cli_email

            '            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = uce_tipoTarjeta.SelectedItem.DataValue
            '            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = txt_ccNombre.Text
            '            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = txt_ccApellido.Text
            '            misForms.newCall.datos_call.reservacion_paquete.cc_numero = txt_ccNumero.Text
            '            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = txt_ccNumeroSeguridad.Text
        End If
    End Sub

    Private Sub btn_reservar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_reservar.Click

        If txt_nombreTraveler.Text.Trim = "" OrElse txt_apellido.Text.Trim = "" Then 'OrElse txt_email.Text = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0299_faltaNombre"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If txt_area.Text.Trim = "" OrElse txt_telefono.Text.Trim = "" Then 'OrElse txt_email.Text = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_467"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If
        'If txt_cp.Text = "" Then
        '    MessageBox.Show("Debe especificar un c�digo postal.", CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    Return
        'End If
        If lbl_val_email.Visible Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_447"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If Me.UltraGroupBox1.Visible Then
            If Not IsNumeric(txt_MM.Text) OrElse Not IsNumeric(txt_AAAA.Text) OrElse CInt(txt_MM.Text) > 12 OrElse txt_MM.Text.Length <> 2 OrElse txt_AAAA.Text.Length <> 4 Then
                CapaLogicaNegocios.ErrorManager.Manage("E0122", "", CapaLogicaNegocios.ErrorManager.ErrorAction.Show, CapaLogicaNegocios.ErrorManager.ErrorSeverity.Atention)
                Return
            End If
            If uce_tipoTarjeta.SelectedIndex = 0 Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0133_errTipoTarjeta"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            If txt_ccNombre.Text = "" OrElse txt_ccNumero.Text = "" OrElse txt_ccNumeroSeguridad.Text = "" OrElse txt_MM.Text = "" OrElse txt_AAAA.Text = "" Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0298_faltanDatosCC"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If
        End If


        Dim emailRequiredBySelectedSegment As Boolean = misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Exists(AddressOf ReserveEmailRequired)
        If (emailRequiredBySelectedSegment = True And txt_email.Text.Trim = "") Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_469_faltaCorreo"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If



        If txt_email.Text.Trim = "" Then txt_email.Text = "sincorreo@mail.com"
        misForms.newCall.datos_call.reservacion_paquete.cli_nombre = txt_nombreTraveler.Text.ToUpper
        misForms.newCall.datos_call.reservacion_paquete.cli_apellido = txt_apellido.Text.ToUpper
        misForms.newCall.datos_call.reservacion_paquete.cli_direccion = txt_direccion.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_ciudad = txt_ciudad.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_estado = txt_estado.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_cp = txt_cp.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_pais = uce_paises.SelectedItem.DataValue
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = txt_area.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = txt_telefono.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = "R"
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = txt_area_adicional1.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = txt_telefono_adicional1.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_email = txt_email.Text
        misForms.newCall.datos_call.reservacion_paquete.homoclave = txt_homoclave.Text
        misForms.newCall.datos_call.reservacion_paquete.codigo_referencia = txt_codigo.Text
        misForms.newCall.datos_call.reservacion_paquete.Canal = txt_Canal.Text

        If Me.UltraGroupBox1.Visible Then
            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = uce_tipoTarjeta.SelectedItem.DataValue
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = txt_ccNombre.Text & "/"
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = txt_ccApellido.Text
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = IIf(txt_ccNumero.Text.Contains("*"), credit, creditCard)
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = txt_ccNumeroSeguridad.Text
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = txt_MM.Text + txt_AAAA.Text
        Else
            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = ""
        End If
        Preguntar = True
        If Preguntar Then
            If MsgBox(CapaPresentacion.Idiomas.get_str("str_446"), MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.Yes Then
                If misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count > 0 Then
                    misForms.newCall.datos_call.reservacion_paquete.nota = misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(0).cust_Preferences
                End If
                misForms.form1.datoscliente = misForms.newCall.datos_call.reservacion_paquete.Clone
                misForms.form1.datoscliente.lista_Actividades.Clear()
                misForms.form1.datoscliente.lista_Autos.Clear()
                misForms.form1.datoscliente.lista_Hoteles.Clear()
                misForms.form1.datoscliente.lista_Vuelos.Clear()
            Else
                misForms.form1.datoscliente = Nothing
            End If
        Else
            misForms.form1.datoscliente = Nothing
        End If
        'If busca_travelers() Then
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Close()
        'End If
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        If txt_email.Text.Trim = "" Then txt_email.Text = "sincorreo@mail.com"
        misForms.newCall.datos_call.reservacion_paquete.cli_nombre = txt_nombreTraveler.Text.ToUpper
        misForms.newCall.datos_call.reservacion_paquete.cli_apellido = txt_apellido.Text.ToUpper
        misForms.newCall.datos_call.reservacion_paquete.cli_direccion = txt_direccion.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_ciudad = txt_ciudad.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_estado = txt_estado.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_cp = txt_cp.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_pais = uce_paises.SelectedItem.DataValue
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = txt_area.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = txt_telefono.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = "R"
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = txt_area_adicional1.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = txt_telefono_adicional1.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_email = txt_email.Text
        misForms.newCall.datos_call.reservacion_paquete.homoclave = txt_homoclave.Text
        misForms.newCall.datos_call.reservacion_paquete.codigo_referencia = txt_codigo.Text
        misForms.newCall.datos_call.reservacion_paquete.Canal = txt_Canal.Text
        If Me.UltraGroupBox1.Visible Then
            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = uce_tipoTarjeta.SelectedItem.DataValue
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = txt_ccNombre.Text & "/"
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = txt_ccApellido.Text
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = IIf(txt_ccNumero.Text.Contains("*"), credit, creditCard)
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = txt_ccNumeroSeguridad.Text
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = txt_MM.Text + txt_AAAA.Text
        Else
            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = ""
        End If
        If misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count > 0 Then
            misForms.newCall.datos_call.reservacion_paquete.nota = misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(0).cust_Preferences
        End If
        misForms.form1.datoscliente = misForms.newCall.datos_call.reservacion_paquete.Clone
        misForms.form1.datoscliente.lista_Actividades.Clear()
        misForms.form1.datoscliente.lista_Autos.Clear()
        misForms.form1.datoscliente.lista_Hoteles.Clear()
        misForms.form1.datoscliente.lista_Vuelos.Clear()
        Close()
    End Sub
    Private Sub datosCliente_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing        
        If txt_email.Text.Trim = "" Then txt_email.Text = "sincorreo@mail.com"
        misForms.newCall.datos_call.reservacion_paquete.cli_nombre = txt_nombreTraveler.Text.ToUpper
        misForms.newCall.datos_call.reservacion_paquete.cli_apellido = txt_apellido.Text.ToUpper
        misForms.newCall.datos_call.reservacion_paquete.cli_direccion = txt_direccion.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_ciudad = txt_ciudad.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_estado = txt_estado.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_cp = txt_cp.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_pais = uce_paises.SelectedItem.DataValue
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = txt_area.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = txt_telefono.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = "R"
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = txt_area_adicional1.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = txt_telefono_adicional1.Text
        misForms.newCall.datos_call.reservacion_paquete.cli_email = txt_email.Text
        misForms.newCall.datos_call.reservacion_paquete.homoclave = txt_homoclave.Text
        misForms.newCall.datos_call.reservacion_paquete.codigo_referencia = txt_codigo.Text
        misForms.newCall.datos_call.reservacion_paquete.Canal = txt_Canal.Text
        If Me.UltraGroupBox1.Visible Then
            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = uce_tipoTarjeta.SelectedItem.DataValue
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = txt_ccNombre.Text & "/"
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = txt_ccApellido.Text
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = IIf(txt_ccNumero.Text.Contains("*"), credit, creditCard)
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = txt_ccNumeroSeguridad.Text
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = txt_MM.Text + txt_AAAA.Text
        Else
            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = ""
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = ""
        End If
        If misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count > 0 Then
            misForms.newCall.datos_call.reservacion_paquete.nota = misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(0).cust_Preferences
        End If
        misForms.form1.datoscliente = misForms.newCall.datos_call.reservacion_paquete.Clone
        misForms.form1.datoscliente.lista_Actividades.Clear()
        misForms.form1.datoscliente.lista_Autos.Clear()
        misForms.form1.datoscliente.lista_Hoteles.Clear()
        misForms.form1.datoscliente.lista_Vuelos.Clear()
        'Close()
    End Sub

    Public Sub inicializa_controles()
        If Not clear_test_controls Then Return
        'TODO: Verificar si es necesario solicitar los datos de la tarjeta
        Me.Height = MaxWindowHeight
        Me.UltraGroupBox1.Visible = True
        Dim operaciones As Integer = (misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count + misForms.newCall.datos_call.reservacion_paquete.lista_Actividades.Count + misForms.newCall.datos_call.reservacion_paquete.lista_Autos.Count + misForms.newCall.datos_call.reservacion_paquete.lista_Vuelos.Count)

        lbl_homoclave.Visible = False
        txt_homoclave.Visible = False
        lbl_Canales.Visible = False
        uce_Canales.Visible = False
        txt_Canal.Visible = False
        If misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count > 0 Then
            Me.UltraGroupBox1.Visible = False
            Me.Height = MinWindowHeight
            For x As Integer = 0 To misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count - 1
                If misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(x).confirmada = False And misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(x).DepositType = enumDepositType.CreditCard Then
                    Me.Height = MaxWindowHeight
                    Me.UltraGroupBox1.Visible = True
                    'Exit For
                End If
                If Not String.IsNullOrEmpty(misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(x).Convenio) Then
                    lbl_homoclave.Visible = True
                    txt_homoclave.Visible = True
                End If

                If dataOperador.idCorporativo = "-1" Then 'visible para los usuarios de univisit, elijan la fuente
                    Dim dsPortals As DataSet
                    With New CapaLogicaNegocios
                        dsPortals = .GetWS.obten_hotel_portals(misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles(0).PropertyNumber)
                    End With

                    If dsPortals IsNot Nothing AndAlso dsPortals.Tables.Count > 0 AndAlso dsPortals.Tables(0).Rows.Count > 0 Then
                        uce_Canales.Items.Clear()
                        Dim item As Infragistics.Win.ValueListItem
                        item = New Infragistics.Win.ValueListItem("19", "Visite Hoteles de M�xico")
                        uce_Canales.Items.Add(item)
                        item = New Infragistics.Win.ValueListItem("9", "Visit Mexico Hotels")
                        uce_Canales.Items.Add(item)

                        If dsPortals.Tables("Error") Is Nothing Then
                            For Each row As DataRow In dsPortals.Tables(0).Rows
                                item = New Infragistics.Win.ValueListItem(row("IdPortal").ToString, row("Nombre").ToString)
                                uce_Canales.Items.Add(item)
                            Next
                        End If
                        'If uce_Canales.Items.Count > 0 Then
                        '    uce_Canales.SelectedIndex = 0
                        '    txt_Canal.Text = uce_Canales.SelectedItem.DataValue
                        'End If
                    Else
                        uce_Canales.Items.Clear()
                        Dim item As Infragistics.Win.ValueListItem
                        item = New Infragistics.Win.ValueListItem("19", "Visite Hoteles de M�xico")
                        uce_Canales.Items.Add(item)
                        item = New Infragistics.Win.ValueListItem("9", "Visit Mexico Hotels")
                        uce_Canales.Items.Add(item)
                    End If

                    Me.Height = MaxWindowHeight
                    lbl_homoclave.Visible = False
                    txt_homoclave.Visible = False
                    lbl_Canales.Location = New Point(16, 289) 'cambiamos la localizacion en el form, para que sea uniforme a los demas controles
                    lbl_Canales.Visible = True
                    uce_Canales.Location = New Point(137, 286)
                    uce_Canales.Visible = True
                End If
            Next
        End If
        If Me.ModificandoReservaHotel Then
            'Se esta modificando reserva de hotel no se cambio info de pago

            If CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.modificar Then
                If misForms.reservar.datosBusqueda_hotel.DepositType <> enumDepositType.CreditCard Then
                    Me.UltraGroupBox1.Visible = False
                    Me.Height = MinWindowHeight
                Else 'Agregado el else por que cuando se modifica y se cambia a tarjeta de credito no dejaba capturar 
                    Me.UltraGroupBox1.Visible = True
                    Me.Height = MaxWindowHeight
                End If
            End If
        End If

        '   Si es nulo no se cargaran los datos guardados
        If misForms.form1.datoscliente Is Nothing Then Return


        If misForms.newCall.datos_call.reservacion_paquete.cli_nombre = Nothing Then
            txt_nombreTraveler.Text = misForms.newCall.txt_nombre.Text
        Else
            txt_nombreTraveler.Text = misForms.newCall.datos_call.reservacion_paquete.cli_nombre
        End If
        If misForms.newCall.datos_call.reservacion_paquete.cli_apellido = Nothing Then
            txt_apellido.Text = misForms.newCall.txt_apellido.Text
        Else
            txt_apellido.Text = misForms.newCall.datos_call.reservacion_paquete.cli_apellido
        End If
        'txt_apellido.Text = misForms.newCall.datos_call.reservacion_paquete.cli_apellido
        txt_direccion.Text = misForms.newCall.datos_call.reservacion_paquete.cli_direccion
        txt_ciudad.Text = misForms.newCall.datos_call.reservacion_paquete.cli_ciudad
        txt_estado.Text = misForms.newCall.datos_call.reservacion_paquete.cli_estado
        txt_cp.Text = misForms.newCall.datos_call.reservacion_paquete.cli_cp
        txt_homoclave.Text = misForms.newCall.datos_call.reservacion_paquete.homoclave
        txt_Canal.Text = misForms.newCall.datos_call.reservacion_paquete.Canal

        'If misForms.newCall.datos_call.reservacion_paquete Then

        'uce_paises.SelectedItem.DataValue = misForms.newCall.datos_call.reservacion_paquete.pais
        For i As Integer = 0 To uce_paises.Items.Count - 1
            If uce_paises.Items(i).DataValue = misForms.newCall.datos_call.reservacion_paquete.cli_pais Then
                uce_paises.SelectedIndex = i
                Exit For
            End If
        Next
        For i As Integer = 0 To uce_estados.Items.Count - 1
            If uce_estados.Items(i).DisplayText = misForms.newCall.datos_call.reservacion_paquete.cli_estado Then
                uce_estados.SelectedIndex = i
                Exit For
            End If
        Next
        If uce_estados.Text <> misForms.newCall.datos_call.reservacion_paquete.cli_estado Then
            uce_estados.Visible = False
            txt_estado.Visible = True
        Else
            uce_estados.Visible = True
            txt_estado.Visible = False
        End If
        txt_area.Text = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode
        If misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = Nothing Then
            txt_telefono.Text = misForms.newCall.txt_telefono.Text
        Else
            txt_telefono.Text = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber
        End If
        txt_area_adicional1.Text = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode
        If misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = Nothing Then
            txt_telefono_adicional1.Text = ""
        Else
            txt_telefono_adicional1.Text = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber
        End If

        ' = misForms.newCall.datos_call.reservacion_paquete.telefono_PhoneUseType 
        If misForms.newCall.datos_call.reservacion_paquete.cli_email = Nothing Then
            txt_email.Text = misForms.newCall.txt_email.Text
        Else
            txt_email.Text = misForms.newCall.datos_call.reservacion_paquete.cli_email
        End If
        'If misForms.newCall.datos_call.reservacion_paquete.cc_tipo <> Nothing Then
        'uce_tipoTarjeta.SelectedItem.DataValue = misForms.newCall.datos_call.reservacion_paquete.cc_tipo
        For i As Integer = 0 To uce_tipoTarjeta.Items.Count - 1
            If uce_tipoTarjeta.Items(i).DataValue = misForms.newCall.datos_call.reservacion_paquete.cc_tipo Then
                uce_tipoTarjeta.SelectedIndex = i
            End If
        Next
        'End If
        If misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = Nothing Then
            txt_ccNombre.Text = misForms.newCall.txt_nombre.Text
        Else
            txt_ccNombre.Text = misForms.newCall.datos_call.reservacion_paquete.cc_nombre_
            txt_ccNombre.Text = txt_ccNombre.Text.Replace("/", "")
        End If
        If misForms.newCall.datos_call.reservacion_paquete.cc_apellido = Nothing Then
            txt_ccApellido.Text = misForms.newCall.txt_apellido.Text
        Else
            txt_ccApellido.Text = misForms.newCall.datos_call.reservacion_paquete.cc_apellido
        End If

        If Not String.IsNullOrEmpty(misForms.newCall.datos_call.reservacion_paquete.codigo_referencia) Then
            txt_codigo.Text = misForms.newCall.datos_call.reservacion_paquete.codigo_referencia
        End If




        credit = IIf(misForms.newCall.datos_call.reservacion_paquete.cc_numero Is Nothing, "", misForms.newCall.datos_call.reservacion_paquete.cc_numero)

        txt_ccNumero.Text = String.Empty

        For i As Integer = 1 To credit.Length - 4
            txt_ccNumero.Text += "*"
        Next

        If credit.Length > 3 Then txt_ccNumero.Text += credit.Substring(credit.Length - 4)


        txt_ccNumeroSeguridad.Text = misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad
        txt_codigo.Text = misForms.newCall.datos_call.reservacion_paquete.codigo_referencia
        Try
            If Not misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira Is Nothing Then
                txt_MM.Text = misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira.Substring(0, 2)
                txt_AAAA.Text = misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira.Substring(2, 4)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtControls_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_ccNumeroSeguridad.KeyPress, txt_MM.KeyPress, txt_AAAA.KeyPress
        If Not IsNumeric(e.KeyChar.ToString) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ControlChars.NullChar
    End Sub

    'Private Sub txt_ccNumero_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_ccNumero.KeyDown
    '    If (e.Control AndAlso (e.KeyCode = 86 Or e.KeyCode = 118)) Or (e.Shift AndAlso e.KeyData = Keys.Insert) Then
    '        e.Handled = True
    '    End If
    'End Sub
    Private Sub txt_ccNumero_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_ccNumero.KeyPress
        If Not IsNumeric(e.KeyChar.ToString) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.KeyChar = ControlChars.NullChar
        ElseIf txt_ccNumero.Text.Contains("*") Then
            txt_ccNumero.Text = String.Empty
        ElseIf IsNumeric(e.KeyChar.ToString) Then

            Dim length As Integer = txt_ccNumero.TextLength
            'Dim back As Integer = Keys.Back
            If length < 12 Then
                creditCard &= e.KeyChar.ToString()
                e.KeyChar = Char.Parse("-")
            Else
                Dim tamCad As Integer = creditCard.Length
                If tamCad < 16 Then creditCard &= e.KeyChar
            End If
        ElseIf e.KeyChar = ControlChars.Back Then
            If creditCard.Length > 0 Then
                creditCard = creditCard.Substring(0, creditCard.Length - 1)
            End If
        End If
    End Sub


    'Private Function busca_travelers() As Boolean
    '    If Not esActividadSolo() AndAlso Not esVueloSolo() Then Return True


    '   Dim ds_travelers As DataSet = CapaDatos.GetTravelersByEmail(txt_email.Text)

    '    If ds_travelers Is Nothing OrElse ds_travelers.Tables.Count = 0 OrElse ds_travelers.Tables(0).Rows.Count = 0 Then Return True

    '    Dim frm As New TravelerSelector
    '    frm.carga(ds_travelers)
    '    frm.ShowDialog()

    '    If misForms.newCall.datos_call.reservacion_paquete.cli_id = "" Then Return False

    '    txt_nombreTraveler.Text = misForms.newCall.datos_call.reservacion_paquete.cli_nombre
    '    txt_apellido.Text = misForms.newCall.datos_call.reservacion_paquete.cli_apellido
    '    txt_direccion.Text = misForms.newCall.datos_call.reservacion_paquete.cli_direccion
    '    txt_ciudad.Text = misForms.newCall.datos_call.reservacion_paquete.cli_ciudad
    '    txt_estado.Text = misForms.newCall.datos_call.reservacion_paquete.cli_estado
    '    txt_cp.Text = misForms.newCall.datos_call.reservacion_paquete.cli_cp
    '    uce_paises.SelectedItem.DataValue = misForms.newCall.datos_call.reservacion_paquete.cli_pais
    '    txt_area.Text = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode
    '    txt_telefono.Text = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber
    '    'misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = "R"
    '    txt_email.Text = misForms.newCall.datos_call.reservacion_paquete.cli_email

    '    Return True
    'End Function

    'Private Function esActividadSolo() As Boolean
    '    If _
    '    misForms.newCall.datos_call.reservacion_paquete.cli_id = "" AndAlso _
    '    misForms.newCall.datos_call.reservacion_paquete.lista_Actividades.Count <> 0 AndAlso _
    '    misForms.newCall.datos_call.reservacion_paquete.lista_Vuelos.Count = 0 AndAlso _
    '    misForms.newCall.datos_call.reservacion_paquete.lista_Autos.Count = 0 AndAlso _
    '    misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count = 0 _
    '    Then Return True Else Return False
    'End Function

    'Private Function esVueloSolo() As Boolean
    '    'aunque no sea solo vuelo, tambien paquete

    '    'If _
    '    'misForms.newCall.datos_call.reservacion_paquete.cli_id = "" AndAlso _
    '    'misForms.newCall.datos_call.reservacion_paquete.lista_Actividades.Count = 0 AndAlso _
    '    'misForms.newCall.datos_call.reservacion_paquete.lista_Vuelos.Count <> 0 AndAlso _
    '    'misForms.newCall.datos_call.reservacion_paquete.lista_Autos.Count = 0 AndAlso _
    '    'misForms.newCall.datos_call.reservacion_paquete.lista_Hoteles.Count = 0 _
    '    'Then Return True Else Return False

    '    If misForms.newCall.datos_call.reservacion_paquete.cli_id = "" AndAlso misForms.newCall.datos_call.reservacion_paquete.lista_Vuelos.Count <> 0 Then _
    '    Return True Else Return False
    'End Function

    Private Sub btn_importar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_importar.Click
        For Each item As dataBusqueda_vuelo In misForms.newCall.datos_call.reservacion_paquete.lista_Vuelos
            If Not item.confirmada Then
                If Not item.entrega_tipo Then
                    txt_nombreTraveler.Text = item.entrega_nombre
                    txt_apellido.Text = item.entrega_apellido
                    txt_direccion.Text = item.entrega_direccion
                    txt_ciudad.Text = item.entrega_ciudad
                    txt_estado.Text = item.entrega_estado
                    txt_cp.Text = item.entrega_cp
                    For i As Integer = 0 To uce_paises.Items.Count - 1
                        If uce_paises.Items(i).DataValue = item.entrega_pais Then
                            uce_paises.SelectedIndex = i
                        End If
                    Next
                    txt_email.Text = item.entrega_email
                    Exit For
                End If
            End If
        Next
    End Sub

    Private Sub losTXT_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_nombreTraveler.TextChanged, txt_apellido.TextChanged
        If afecta Then
            txt_ccNombre.Text = txt_nombreTraveler.Text
            txt_ccApellido.Text = txt_apellido.Text
        End If
    End Sub

    Private Sub masTXT_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_ccNombre.KeyPress, txt_ccApellido.KeyPress
        afecta = False
    End Sub

    Private Sub losTXT_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_nombreTraveler.KeyUp, txt_ccNombre.KeyUp, txt_ccApellido.KeyUp, txt_apellido.KeyUp
        herramientas.capitaliza(txt_nombreTraveler)
        herramientas.capitaliza(txt_ccNombre)
        herramientas.capitaliza(txt_ccApellido)
        herramientas.capitaliza(txt_apellido)
    End Sub

    Private Shared Function ReserveEmailRequired(ByVal e As dataBusqueda_hotel_test) As Boolean
        If e.id_Segmento <> "" Then
            Return herramientas.get_emailRequiredSegmento(e.id_Segmento)
        Else
            Return herramientas.get_emailRequiredSegmentoEmpresa(e.id_Empresa)
        End If

    End Function

    Private Sub uce_estados_ValueChanged(sender As System.Object, e As System.EventArgs) Handles uce_estados.ValueChanged
        Me.txt_estado.Text = uce_estados.Text
    End Sub

    Private Sub uce_paises_ValueChanged(sender As System.Object, e As System.EventArgs) Handles uce_paises.ValueChanged
        If uce_paises.Value = "MX" Or uce_paises.Value = "US" Then
            CapaPresentacion.Common.cargar_estados(uce_estados, uce_paises.Value)
            Me.txt_estado.Visible = False
            Me.uce_estados.Visible = True
        Else
            Me.txt_estado.Text = ""
            Me.txt_estado.Visible = True
            Me.uce_estados.Visible = False
        End If
    End Sub

    'Private Sub txt_ccNumero_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles txt_ccNumero.MouseDown
    '    If e.Button = Windows.Forms.MouseButtons.Right Then
    '        Clipboard.Clear()
    '    End If
    'End Sub

    Private Sub txt_solonumeros_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_area.KeyPress, txt_area_adicional1.KeyPress, txt_telefono.KeyPress, txt_telefono_adicional1.KeyPress, txt_cp.KeyPress
        If Not Char.IsNumber(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txt_email_Validating(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles txt_email.Validating
        Dim val As Boolean = String.IsNullOrEmpty(txt_email.Text.Trim())

        val = val OrElse IsValidEmailFormat(txt_email.Text.Trim())

        lbl_val_email.Visible = Not val


    End Sub

    Private Function IsValidEmailFormat(ByVal s As String) As Boolean
        Dim result As Boolean = False

        If Not String.IsNullOrEmpty(s) Then

            '^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$
            Dim pattern As String = "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$"

            Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(s, pattern)
            result = match.Success
        End If

        Return result
    End Function


    Private Sub txt_cp_Validated(sender As System.Object, e As System.EventArgs) Handles txt_cp.Validated

        Dim Thread_BuscaCiudad As Threading.Thread = New Threading.Thread(AddressOf cargar_ciudad)
        Thread_BuscaCiudad.Start()
    End Sub
    Private Sub cargar_ciudad()
        CapaPresentacion.Common.cargar_ciudad_por_cp(txt_cp.Text, txt_ciudad, uce_estados, uce_paises)
    End Sub

    Private Sub uce_Canales_ValueChanged(sender As System.Object, e As System.EventArgs) Handles uce_Canales.ValueChanged
        Me.txt_Canal.Text = uce_Canales.Value
    End Sub
End Class