Public Class uc_vuelo

    Private Sub add_unVuelo(ByVal unVuelo As uc_unVuelo)
        pnl_body.Controls.Add(unVuelo)

        Dim ctls_heigth = 0
        For Each ctl As Control In pnl_body.Controls
            ctls_heigth += ctl.Height
        Next

        Me.Height = pnl_header.Height + ctls_heigth + pnl_footer.Height + 15
    End Sub

    Private vuelo As dataVuelosItinerario.vuelo

    Public Property data_source() As dataVuelosItinerario.vuelo
        Get
            Return vuelo
        End Get
        Set(ByVal value As dataVuelosItinerario.vuelo)
            vuelo = value

            If vuelo Is Nothing Then Return

            llena_controles()
        End Set
    End Property

    Private Sub llena_controles()
        Label2.Text = Format(vuelo.AltCantidad, "c") + " " + vuelo.AltCurrency 'vuelo.datos_ItinTotalFare.TotalFare_CurrencyCode

        Dim ctl_unVuelo_ow As New uc_unVuelo(True)
        ctl_unVuelo_ow.data_unVuelo = vuelo.vuelo_ow
        add_unVuelo(ctl_unVuelo_ow)

        If vuelo.vuelo_rt IsNot Nothing AndAlso vuelo.vuelo_rt.lista_escala.Count > 0 Then
            Dim ctl_unVuelo_rt As New uc_unVuelo(False)
            ctl_unVuelo_rt.data_unVuelo = vuelo.vuelo_rt
            add_unVuelo(ctl_unVuelo_rt)
        End If
    End Sub

    Private _is_selected As Boolean
    Private el_color As Color = Color.FromArgb(100, 226, 250, 253)

    Public Property IS_SELECTED() As Boolean
        Get
            Return _is_selected
        End Get
        Set(ByVal value As Boolean)
            _is_selected = value


            If _is_selected Then
                'Me.BackColor = el_color
                'RadioButton1.Checked = True
            Else
                Me.BackColor = Nothing
                RadioButton1.Checked = False
            End If
        End Set
    End Property

    Public Sub SELECCIONAR()
        For Each ctl_vuelo As uc_vuelo In CType(Me.Parent.Parent, uc_itinerario).lista_de_vuelos
            If ctl_vuelo Is Me Then Continue For

            ctl_vuelo.IS_SELECTED = False
        Next

        _is_selected = True
        Me.BackColor = el_color
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked Then
            SELECCIONAR()
            CType(Me.Parent.Parent, uc_itinerario).dispara_DespuesVueloActivate(RadioButton1.Checked)
        End If
    End Sub

    Public Sub sel_CKB()
        RadioButton1.Checked = True
    End Sub

End Class
