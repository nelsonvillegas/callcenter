Public Class dataVuelosItinerario

    Public lista_vuelos As New List(Of vuelo)

    'hacer metodo CLONE

    Public Class vuelo
        Public PricedItinerary_id As String
        Public cantidad As Double
        Public base As Double
        Public AltCantidad As Double
        Public AltBase As Double
        Public AltCurrency As String
        Public RPH As String
        Public vuelo_ow As unVuelo
        Public vuelo_rt As unVuelo
        Public datos_ItinTotalFare As New ItinTotalFare
        Public datos_FareInfo As New FareInfo
        Public TicketingInfo_TicketTimeLimit As String

        Public Class unVuelo
            Public lista_escala As New List(Of escala)

            Public Class escala
                Public Journy As String
                Public ArrivalDateTime As String
                Public DepartureDateTime As String
                Public StopQuantity As String
                Public FlightNumber As String
                Public JourneyDuration As String
                Public OnTimeRate As String
                Public Ticket As String
                Public SmokingAllowed As String
                Public GroundDuration As String
                Public AccumulatedDuration As String
                Public Identity As String

                '---------------

                Public dep_LocationCode As String
                Public dep_Terminal As String
                Public dep_str As String

                Public arr_LocationCode As String
                Public arr_Terminal As String
                Public arr_str As String

                Public equ_AirEquipType As String
                Public equ_ChangeofGauge As String

                Public m_air_CompanyShortName As String
                Public m_air_OpAir As String
                Public m_air_str As String

                Public m_cab_CabinType As String
                Public m_cab_RPH As String
            End Class
        End Class

        Public Class ItinTotalFare
            Public NegotiatedFare As String
            Public BaseFare_Amount As String
            Public BaseFare_CurrencyCode As String
            Public BaseFare_DecimalPlaces As String
            Public Taxes As New List(Of Tax)
            Public TotalFare_Amount As String
            Public TotalFare_CurrencyCode As String
            Public TotalFare_DecimalPlaces As String
            Public Fees As New List(Of Fee)

            Public Class Tax
                Public Tax_TaxCode As String
                Public Tax_Amount As String
                Public Tax_CurrencyCode As String
            End Class

            Public Class Fee
                Public Fee_Amount As String
                Public Fee_CurrencyCode As String
                Public Fee_DecimalPlaces As String
            End Class
        End Class

        Public Class FareInfo
            Public PTC_PassengerTypeCode As String
            Public PTC_lista_FICInfo As New List(Of FICInfo)

            Public Class FICInfo
                Public FICInfo_Seg As String
                Public FICInfo_Fic As String
            End Class
        End Class
    End Class

End Class

