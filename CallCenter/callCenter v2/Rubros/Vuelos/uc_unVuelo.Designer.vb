<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_unVuelo
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.pnl_header = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.pnl_footer = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.UltraFlowLayoutManager1 = New Infragistics.Win.Misc.UltraFlowLayoutManager(Me.components)
        Me.pnl_body = New System.Windows.Forms.Panel
        Me.pnl_header.SuspendLayout()
        Me.pnl_footer.SuspendLayout()
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnl_header
        '
        Me.pnl_header.Controls.Add(Me.Label2)
        Me.pnl_header.Controls.Add(Me.Label1)
        Me.pnl_header.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnl_header.Location = New System.Drawing.Point(0, 0)
        Me.pnl_header.Name = "pnl_header"
        Me.pnl_header.Size = New System.Drawing.Size(758, 20)
        Me.pnl_header.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(275, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Label2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(91, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Label1"
        '
        'pnl_footer
        '
        Me.pnl_footer.Controls.Add(Me.Label4)
        Me.pnl_footer.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnl_footer.Location = New System.Drawing.Point(0, 49)
        Me.pnl_footer.Name = "pnl_footer"
        Me.pnl_footer.Size = New System.Drawing.Size(758, 20)
        Me.pnl_footer.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(467, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Label4"
        '
        'UltraFlowLayoutManager1
        '
        Me.UltraFlowLayoutManager1.ContainerControl = Me.pnl_body
        Me.UltraFlowLayoutManager1.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.UltraFlowLayoutManager1.VerticalAlignment = Infragistics.Win.Layout.DefaultableFlowLayoutAlignment.Near
        '
        'pnl_body
        '
        Me.pnl_body.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl_body.Location = New System.Drawing.Point(0, 20)
        Me.pnl_body.Name = "pnl_body"
        Me.pnl_body.Size = New System.Drawing.Size(758, 49)
        Me.pnl_body.TabIndex = 1
        '
        'uc_unVuelo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnl_footer)
        Me.Controls.Add(Me.pnl_body)
        Me.Controls.Add(Me.pnl_header)
        Me.Name = "uc_unVuelo"
        Me.Size = New System.Drawing.Size(758, 69)
        Me.pnl_header.ResumeLayout(False)
        Me.pnl_header.PerformLayout()
        Me.pnl_footer.ResumeLayout(False)
        Me.pnl_footer.PerformLayout()
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnl_header As System.Windows.Forms.Panel
    Friend WithEvents pnl_footer As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UltraFlowLayoutManager1 As Infragistics.Win.Misc.UltraFlowLayoutManager
    Friend WithEvents pnl_body As System.Windows.Forms.Panel

End Class
