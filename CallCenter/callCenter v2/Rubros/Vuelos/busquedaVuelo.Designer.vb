<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class busquedaVuelo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(busquedaVuelo))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.rbt_redondo = New System.Windows.Forms.RadioButton
        Me.rbt_sencillo = New System.Windows.Forms.RadioButton
        Me.data_lbl_vuelos = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.ugb_destino = New Infragistics.Win.Misc.UltraGroupBox
        Me.uce_horaDestino = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lbl_horaDestino = New System.Windows.Forms.Label
        Me.cs_ciudadDestino = New callCenter.ComboSearch
        Me.lbl_ciudadDestino = New System.Windows.Forms.Label
        Me.dtp_regresoDestino = New System.Windows.Forms.DateTimePicker
        Me.lbl_regrsoDestino = New System.Windows.Forms.Label
        Me.lbl_moneda = New System.Windows.Forms.Label
        Me.ugb_partida = New Infragistics.Win.Misc.UltraGroupBox
        Me.uce_horaPartida = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lbl_horaPartida = New System.Windows.Forms.Label
        Me.cs_ciudadPartida = New callCenter.ComboSearch
        Me.lbl_ciudadPartida = New System.Windows.Forms.Label
        Me.dtp_salidaPartida = New System.Windows.Forms.DateTimePicker
        Me.lbl_salidaPartida = New System.Windows.Forms.Label
        Me.uce_moneda = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lbl_clase = New System.Windows.Forms.Label
        Me.uce_clase = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lbl_aerolinea = New System.Windows.Forms.Label
        Me.uce_aerolinea = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.btn_buscar = New System.Windows.Forms.Button
        Me.txt_ninos = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_adultos = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_mayores = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_mayores = New System.Windows.Forms.Label
        Me.lbl_ninos = New System.Windows.Forms.Label
        Me.lbl_adultos = New System.Windows.Forms.Label
        Me.UltraFlowLayoutManager1 = New Infragistics.Win.Misc.UltraFlowLayoutManager(Me.components)
        Me.Uc_itinerario1 = New callCenter.uc_itinerario
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ugb_destino, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugb_destino.SuspendLayout()
        CType(Me.uce_horaDestino, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ugb_partida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugb_partida.SuspendLayout()
        CType(Me.uce_horaPartida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_clase, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_aerolinea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ninos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_adultos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_mayores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.data_lbl_vuelos)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.ugb_destino)
        Me.Panel1.Controls.Add(Me.lbl_moneda)
        Me.Panel1.Controls.Add(Me.ugb_partida)
        Me.Panel1.Controls.Add(Me.uce_moneda)
        Me.Panel1.Controls.Add(Me.lbl_clase)
        Me.Panel1.Controls.Add(Me.uce_clase)
        Me.Panel1.Controls.Add(Me.lbl_aerolinea)
        Me.Panel1.Controls.Add(Me.uce_aerolinea)
        Me.Panel1.Controls.Add(Me.btn_buscar)
        Me.Panel1.Controls.Add(Me.txt_ninos)
        Me.Panel1.Controls.Add(Me.txt_adultos)
        Me.Panel1.Controls.Add(Me.txt_mayores)
        Me.Panel1.Controls.Add(Me.lbl_mayores)
        Me.Panel1.Controls.Add(Me.lbl_ninos)
        Me.Panel1.Controls.Add(Me.lbl_adultos)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1140, 144)
        Me.Panel1.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.rbt_redondo)
        Me.Panel2.Controls.Add(Me.rbt_sencillo)
        Me.Panel2.Location = New System.Drawing.Point(12, 97)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(215, 28)
        Me.Panel2.TabIndex = 4
        '
        'rbt_redondo
        '
        Me.rbt_redondo.AutoSize = True
        Me.rbt_redondo.Checked = True
        Me.rbt_redondo.Location = New System.Drawing.Point(3, 3)
        Me.rbt_redondo.Name = "rbt_redondo"
        Me.rbt_redondo.Size = New System.Drawing.Size(72, 17)
        Me.rbt_redondo.TabIndex = 4
        Me.rbt_redondo.TabStop = True
        Me.rbt_redondo.Text = "Redondo "
        Me.rbt_redondo.UseVisualStyleBackColor = True
        '
        'rbt_sencillo
        '
        Me.rbt_sencillo.AutoSize = True
        Me.rbt_sencillo.Location = New System.Drawing.Point(126, 3)
        Me.rbt_sencillo.Name = "rbt_sencillo"
        Me.rbt_sencillo.Size = New System.Drawing.Size(62, 17)
        Me.rbt_sencillo.TabIndex = 4
        Me.rbt_sencillo.Text = "Sencillo"
        Me.rbt_sencillo.UseVisualStyleBackColor = True
        '
        'data_lbl_vuelos
        '
        Me.data_lbl_vuelos.AutoSize = True
        Me.data_lbl_vuelos.Location = New System.Drawing.Point(928, 71)
        Me.data_lbl_vuelos.Name = "data_lbl_vuelos"
        Me.data_lbl_vuelos.Size = New System.Drawing.Size(16, 13)
        Me.data_lbl_vuelos.TabIndex = 12
        Me.data_lbl_vuelos.Text = "---"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(839, 105)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 11
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'ugb_destino
        '
        Me.ugb_destino.Controls.Add(Me.uce_horaDestino)
        Me.ugb_destino.Controls.Add(Me.lbl_horaDestino)
        Me.ugb_destino.Controls.Add(Me.cs_ciudadDestino)
        Me.ugb_destino.Controls.Add(Me.lbl_ciudadDestino)
        Me.ugb_destino.Controls.Add(Me.dtp_regresoDestino)
        Me.ugb_destino.Controls.Add(Me.lbl_regrsoDestino)
        Me.ugb_destino.Location = New System.Drawing.Point(502, 13)
        Me.ugb_destino.Name = "ugb_destino"
        Me.ugb_destino.Size = New System.Drawing.Size(234, 119)
        Me.ugb_destino.TabIndex = 6
        Me.ugb_destino.Text = "Destino"
        Me.ugb_destino.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'uce_horaDestino
        '
        Me.uce_horaDestino.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_horaDestino.Location = New System.Drawing.Point(68, 85)
        Me.uce_horaDestino.Name = "uce_horaDestino"
        Me.uce_horaDestino.Size = New System.Drawing.Size(150, 21)
        Me.uce_horaDestino.TabIndex = 3
        '
        'lbl_horaDestino
        '
        Me.lbl_horaDestino.AutoSize = True
        Me.lbl_horaDestino.BackColor = System.Drawing.Color.Transparent
        Me.lbl_horaDestino.Location = New System.Drawing.Point(6, 89)
        Me.lbl_horaDestino.Name = "lbl_horaDestino"
        Me.lbl_horaDestino.Size = New System.Drawing.Size(33, 13)
        Me.lbl_horaDestino.TabIndex = 0
        Me.lbl_horaDestino.Text = "Hora:"
        '
        'cs_ciudadDestino
        '
        Me.cs_ciudadDestino.BackColor = System.Drawing.Color.Transparent
        Me.cs_ciudadDestino.Location = New System.Drawing.Point(68, 30)
        Me.cs_ciudadDestino.Name = "cs_ciudadDestino"
        Me.cs_ciudadDestino.Size = New System.Drawing.Size(150, 21)
        Me.cs_ciudadDestino.TabIndex = 1
        '
        'lbl_ciudadDestino
        '
        Me.lbl_ciudadDestino.AutoSize = True
        Me.lbl_ciudadDestino.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudadDestino.Location = New System.Drawing.Point(6, 33)
        Me.lbl_ciudadDestino.Name = "lbl_ciudadDestino"
        Me.lbl_ciudadDestino.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudadDestino.TabIndex = 0
        Me.lbl_ciudadDestino.Text = "Ciudad:"
        '
        'dtp_regresoDestino
        '
        Me.dtp_regresoDestino.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_regresoDestino.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_regresoDestino.Location = New System.Drawing.Point(68, 57)
        Me.dtp_regresoDestino.Name = "dtp_regresoDestino"
        Me.dtp_regresoDestino.Size = New System.Drawing.Size(106, 20)
        Me.dtp_regresoDestino.TabIndex = 2
        '
        'lbl_regrsoDestino
        '
        Me.lbl_regrsoDestino.AutoSize = True
        Me.lbl_regrsoDestino.BackColor = System.Drawing.Color.Transparent
        Me.lbl_regrsoDestino.Location = New System.Drawing.Point(6, 63)
        Me.lbl_regrsoDestino.Name = "lbl_regrsoDestino"
        Me.lbl_regrsoDestino.Size = New System.Drawing.Size(50, 13)
        Me.lbl_regrsoDestino.TabIndex = 0
        Me.lbl_regrsoDestino.Text = "Regreso:"
        '
        'lbl_moneda
        '
        Me.lbl_moneda.AutoSize = True
        Me.lbl_moneda.Location = New System.Drawing.Point(17, 70)
        Me.lbl_moneda.Name = "lbl_moneda"
        Me.lbl_moneda.Size = New System.Drawing.Size(49, 13)
        Me.lbl_moneda.TabIndex = 0
        Me.lbl_moneda.Text = "Moneda:"
        '
        'ugb_partida
        '
        Me.ugb_partida.Controls.Add(Me.uce_horaPartida)
        Me.ugb_partida.Controls.Add(Me.lbl_horaPartida)
        Me.ugb_partida.Controls.Add(Me.cs_ciudadPartida)
        Me.ugb_partida.Controls.Add(Me.lbl_ciudadPartida)
        Me.ugb_partida.Controls.Add(Me.dtp_salidaPartida)
        Me.ugb_partida.Controls.Add(Me.lbl_salidaPartida)
        Me.ugb_partida.Location = New System.Drawing.Point(262, 12)
        Me.ugb_partida.Name = "ugb_partida"
        Me.ugb_partida.Size = New System.Drawing.Size(234, 119)
        Me.ugb_partida.TabIndex = 5
        Me.ugb_partida.Text = "Partida"
        Me.ugb_partida.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'uce_horaPartida
        '
        Me.uce_horaPartida.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_horaPartida.Location = New System.Drawing.Point(68, 85)
        Me.uce_horaPartida.Name = "uce_horaPartida"
        Me.uce_horaPartida.Size = New System.Drawing.Size(150, 21)
        Me.uce_horaPartida.TabIndex = 3
        '
        'lbl_horaPartida
        '
        Me.lbl_horaPartida.AutoSize = True
        Me.lbl_horaPartida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_horaPartida.Location = New System.Drawing.Point(6, 89)
        Me.lbl_horaPartida.Name = "lbl_horaPartida"
        Me.lbl_horaPartida.Size = New System.Drawing.Size(33, 13)
        Me.lbl_horaPartida.TabIndex = 0
        Me.lbl_horaPartida.Text = "Hora:"
        '
        'cs_ciudadPartida
        '
        Me.cs_ciudadPartida.BackColor = System.Drawing.Color.Transparent
        Me.cs_ciudadPartida.Location = New System.Drawing.Point(68, 30)
        Me.cs_ciudadPartida.Name = "cs_ciudadPartida"
        Me.cs_ciudadPartida.Size = New System.Drawing.Size(150, 21)
        Me.cs_ciudadPartida.TabIndex = 1
        '
        'lbl_ciudadPartida
        '
        Me.lbl_ciudadPartida.AutoSize = True
        Me.lbl_ciudadPartida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudadPartida.Location = New System.Drawing.Point(6, 33)
        Me.lbl_ciudadPartida.Name = "lbl_ciudadPartida"
        Me.lbl_ciudadPartida.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudadPartida.TabIndex = 0
        Me.lbl_ciudadPartida.Text = "Ciudad:"
        '
        'dtp_salidaPartida
        '
        Me.dtp_salidaPartida.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_salidaPartida.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_salidaPartida.Location = New System.Drawing.Point(68, 57)
        Me.dtp_salidaPartida.Name = "dtp_salidaPartida"
        Me.dtp_salidaPartida.Size = New System.Drawing.Size(106, 20)
        Me.dtp_salidaPartida.TabIndex = 2
        '
        'lbl_salidaPartida
        '
        Me.lbl_salidaPartida.AutoSize = True
        Me.lbl_salidaPartida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_salidaPartida.Location = New System.Drawing.Point(6, 63)
        Me.lbl_salidaPartida.Name = "lbl_salidaPartida"
        Me.lbl_salidaPartida.Size = New System.Drawing.Size(39, 13)
        Me.lbl_salidaPartida.TabIndex = 0
        Me.lbl_salidaPartida.Text = "Salida:"
        '
        'uce_moneda
        '
        Me.uce_moneda.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_moneda.Location = New System.Drawing.Point(77, 66)
        Me.uce_moneda.Name = "uce_moneda"
        Me.uce_moneda.Size = New System.Drawing.Size(150, 21)
        Me.uce_moneda.TabIndex = 3
        '
        'lbl_clase
        '
        Me.lbl_clase.AutoSize = True
        Me.lbl_clase.Location = New System.Drawing.Point(17, 43)
        Me.lbl_clase.Name = "lbl_clase"
        Me.lbl_clase.Size = New System.Drawing.Size(36, 13)
        Me.lbl_clase.TabIndex = 0
        Me.lbl_clase.Text = "Clase:"
        '
        'uce_clase
        '
        Me.uce_clase.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_clase.Location = New System.Drawing.Point(77, 39)
        Me.uce_clase.Name = "uce_clase"
        Me.uce_clase.Size = New System.Drawing.Size(150, 21)
        Me.uce_clase.TabIndex = 2
        '
        'lbl_aerolinea
        '
        Me.lbl_aerolinea.AutoSize = True
        Me.lbl_aerolinea.Location = New System.Drawing.Point(17, 16)
        Me.lbl_aerolinea.Name = "lbl_aerolinea"
        Me.lbl_aerolinea.Size = New System.Drawing.Size(54, 13)
        Me.lbl_aerolinea.TabIndex = 0
        Me.lbl_aerolinea.Text = "Aerolinea:"
        '
        'uce_aerolinea
        '
        Me.uce_aerolinea.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_aerolinea.Location = New System.Drawing.Point(77, 12)
        Me.uce_aerolinea.Name = "uce_aerolinea"
        Me.uce_aerolinea.Size = New System.Drawing.Size(150, 21)
        Me.uce_aerolinea.TabIndex = 1
        '
        'btn_buscar
        '
        Me.btn_buscar.Image = CType(resources.GetObject("btn_buscar.Image"), System.Drawing.Image)
        Me.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_buscar.Location = New System.Drawing.Point(931, 12)
        Me.btn_buscar.Name = "btn_buscar"
        Me.btn_buscar.Size = New System.Drawing.Size(75, 56)
        Me.btn_buscar.TabIndex = 10
        Me.btn_buscar.Text = "Buscar"
        Me.btn_buscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_buscar.UseVisualStyleBackColor = True
        '
        'txt_ninos
        '
        Me.txt_ninos.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ninos.Location = New System.Drawing.Point(839, 62)
        Me.txt_ninos.Name = "txt_ninos"
        Me.txt_ninos.Size = New System.Drawing.Size(45, 19)
        Me.txt_ninos.TabIndex = 9
        Me.txt_ninos.Text = "0"
        '
        'txt_adultos
        '
        Me.txt_adultos.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_adultos.Location = New System.Drawing.Point(839, 12)
        Me.txt_adultos.Name = "txt_adultos"
        Me.txt_adultos.Size = New System.Drawing.Size(45, 19)
        Me.txt_adultos.TabIndex = 7
        Me.txt_adultos.Text = "1"
        '
        'txt_mayores
        '
        Me.txt_mayores.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_mayores.Location = New System.Drawing.Point(839, 37)
        Me.txt_mayores.Name = "txt_mayores"
        Me.txt_mayores.Size = New System.Drawing.Size(45, 19)
        Me.txt_mayores.TabIndex = 8
        Me.txt_mayores.Text = "0"
        '
        'lbl_mayores
        '
        Me.lbl_mayores.AutoSize = True
        Me.lbl_mayores.Location = New System.Drawing.Point(761, 40)
        Me.lbl_mayores.Name = "lbl_mayores"
        Me.lbl_mayores.Size = New System.Drawing.Size(50, 13)
        Me.lbl_mayores.TabIndex = 0
        Me.lbl_mayores.Text = "Mayores:"
        '
        'lbl_ninos
        '
        Me.lbl_ninos.AutoSize = True
        Me.lbl_ninos.Location = New System.Drawing.Point(761, 65)
        Me.lbl_ninos.Name = "lbl_ninos"
        Me.lbl_ninos.Size = New System.Drawing.Size(37, 13)
        Me.lbl_ninos.TabIndex = 0
        Me.lbl_ninos.Text = "Ni�os:"
        '
        'lbl_adultos
        '
        Me.lbl_adultos.AutoSize = True
        Me.lbl_adultos.Location = New System.Drawing.Point(761, 15)
        Me.lbl_adultos.Name = "lbl_adultos"
        Me.lbl_adultos.Size = New System.Drawing.Size(45, 13)
        Me.lbl_adultos.TabIndex = 0
        Me.lbl_adultos.Text = "Adultos:"
        '
        'UltraFlowLayoutManager1
        '
        Me.UltraFlowLayoutManager1.HorizontalAlignment = Infragistics.Win.Layout.DefaultableFlowLayoutAlignment.Near
        Me.UltraFlowLayoutManager1.VerticalAlignment = Infragistics.Win.Layout.DefaultableFlowLayoutAlignment.Near
        '
        'Uc_itinerario1
        '
        Me.Uc_itinerario1.data_source = Nothing
        Me.Uc_itinerario1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Uc_itinerario1.Location = New System.Drawing.Point(0, 144)
        Me.Uc_itinerario1.Name = "Uc_itinerario1"
        Me.Uc_itinerario1.Size = New System.Drawing.Size(1140, 394)
        Me.Uc_itinerario1.TabIndex = 3
        '
        'busquedaVuelo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1140, 538)
        Me.Controls.Add(Me.Uc_itinerario1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "busquedaVuelo"
        Me.Text = "Resultados de la b�squeda: Vuelos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ugb_destino, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugb_destino.ResumeLayout(False)
        Me.ugb_destino.PerformLayout()
        CType(Me.uce_horaDestino, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ugb_partida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugb_partida.ResumeLayout(False)
        Me.ugb_partida.PerformLayout()
        CType(Me.uce_horaPartida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_clase, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_aerolinea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ninos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_adultos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_mayores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents lbl_adultos As System.Windows.Forms.Label
    Friend WithEvents lbl_mayores As System.Windows.Forms.Label
    Friend WithEvents lbl_ciudadPartida As System.Windows.Forms.Label
    Friend WithEvents lbl_salidaPartida As System.Windows.Forms.Label
    Friend WithEvents dtp_salidaPartida As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_ninos As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_adultos As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_mayores As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_buscar As System.Windows.Forms.Button
    Friend WithEvents cs_ciudadPartida As callCenter.ComboSearch
    Friend WithEvents ugb_partida As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents rbt_sencillo As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_redondo As System.Windows.Forms.RadioButton
    Friend WithEvents uce_horaPartida As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_horaPartida As System.Windows.Forms.Label
    Friend WithEvents ugb_destino As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents uce_horaDestino As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_horaDestino As System.Windows.Forms.Label
    Friend WithEvents cs_ciudadDestino As callCenter.ComboSearch
    Friend WithEvents lbl_ciudadDestino As System.Windows.Forms.Label
    Friend WithEvents dtp_regresoDestino As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_regrsoDestino As System.Windows.Forms.Label
    Friend WithEvents lbl_moneda As System.Windows.Forms.Label
    Friend WithEvents uce_moneda As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_clase As System.Windows.Forms.Label
    Friend WithEvents uce_clase As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_aerolinea As System.Windows.Forms.Label
    Friend WithEvents uce_aerolinea As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents UltraFlowLayoutManager1 As Infragistics.Win.Misc.UltraFlowLayoutManager
    Friend WithEvents Uc_itinerario1 As callCenter.uc_itinerario
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents data_lbl_vuelos As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    'Friend WithEvents ComboSearch1 As callCenter.ComboSearch
End Class
