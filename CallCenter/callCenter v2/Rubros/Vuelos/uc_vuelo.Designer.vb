<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_vuelo
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_vuelo))
        Me.pnl_header = New Infragistics.Win.Misc.UltraPanel
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.pnl_footer = New Infragistics.Win.Misc.UltraPanel
        Me.UltraFlowLayoutManager1 = New Infragistics.Win.Misc.UltraFlowLayoutManager(Me.components)
        Me.pnl_body = New System.Windows.Forms.Panel
        Me.pnl_header.ClientArea.SuspendLayout()
        Me.pnl_header.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_footer.SuspendLayout()
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnl_header
        '
        Appearance1.BackColor = System.Drawing.Color.SkyBlue
        Appearance1.BackColor2 = System.Drawing.Color.Azure
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.pnl_header.Appearance = Appearance1
        '
        'pnl_header.ClientArea
        '
        Me.pnl_header.ClientArea.Controls.Add(Me.RadioButton1)
        Me.pnl_header.ClientArea.Controls.Add(Me.Label2)
        Me.pnl_header.ClientArea.Controls.Add(Me.PictureBox1)
        Me.pnl_header.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnl_header.Location = New System.Drawing.Point(0, 0)
        Me.pnl_header.Name = "pnl_header"
        Me.pnl_header.Size = New System.Drawing.Size(795, 32)
        Me.pnl_header.TabIndex = 0
        Me.pnl_header.Text = "UltraPanel1"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton1.Location = New System.Drawing.Point(38, 7)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(133, 17)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Seleccionar este vuelo"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(674, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Label2"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'pnl_footer
        '
        Me.pnl_footer.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnl_footer.Location = New System.Drawing.Point(0, 372)
        Me.pnl_footer.Name = "pnl_footer"
        Me.pnl_footer.Size = New System.Drawing.Size(795, 32)
        Me.pnl_footer.TabIndex = 2
        Me.pnl_footer.Text = "UltraPanel3"
        '
        'UltraFlowLayoutManager1
        '
        Me.UltraFlowLayoutManager1.ContainerControl = Me.pnl_body
        Me.UltraFlowLayoutManager1.VerticalAlignment = Infragistics.Win.Layout.DefaultableFlowLayoutAlignment.Near
        '
        'pnl_body
        '
        Me.pnl_body.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl_body.Location = New System.Drawing.Point(0, 32)
        Me.pnl_body.Name = "pnl_body"
        Me.pnl_body.Size = New System.Drawing.Size(795, 340)
        Me.pnl_body.TabIndex = 4
        '
        'uc_vuelo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnl_body)
        Me.Controls.Add(Me.pnl_footer)
        Me.Controls.Add(Me.pnl_header)
        Me.Name = "uc_vuelo"
        Me.Size = New System.Drawing.Size(795, 404)
        Me.pnl_header.ClientArea.ResumeLayout(False)
        Me.pnl_header.ClientArea.PerformLayout()
        Me.pnl_header.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_footer.ResumeLayout(False)
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnl_header As Infragistics.Win.Misc.UltraPanel
    Friend WithEvents pnl_footer As Infragistics.Win.Misc.UltraPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents UltraFlowLayoutManager1 As Infragistics.Win.Misc.UltraFlowLayoutManager
    Friend WithEvents pnl_body As System.Windows.Forms.Panel
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton

End Class
