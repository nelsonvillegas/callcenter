Public Class busquedaVuelo

    Public botones() As String
    'Public lista_tarifas As New List(Of tarifas)
    Public datosBusqueda_vuelo As New dataBusqueda_vuelo
    'Public datos_call As dataCall

    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
    End Sub

    Public MI_Thread As Threading.Thread
    'Public UN_Thread As System.Threading.Thread
    'Public avance As Integer
    'Public total As Integer
    'Private cont As Integer = 0
    'Public datos_cliente As dataCliente

    'Delegate Sub FijaProgressBarCallback(ByVal valor As Integer, ByVal visible As Boolean)
    'Delegate Sub Fija_BTNActualizar_BarCallback()

    '' ''Private Sub Fija_BTNActualizar()
    '' ''    Try
    '' ''        If misForms.form1.UltraStatusBar1.InvokeRequired Then
    '' ''            Dim d As New Fija_BTNActualizar_BarCallback(AddressOf Fija_BTNActualizar)
    '' ''            Me.Invoke(d, New Object() {})
    '' ''        Else
    '' ''            'CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_actualizar").SharedProps.Enabled = True
    '' ''        End If
    '' ''    Catch ex As Exception
    '' ''    End Try
    '' ''End Sub

    'Private Sub FijaProgressBar(ByVal valor As Integer, ByVal visible As Boolean)
    '    Try
    '        If misForms.form1.UltraStatusBar1.InvokeRequired Then
    '            Dim d As New FijaProgressBarCallback(AddressOf FijaProgressBar)
    '            Me.Invoke(d, New Object() {valor, visible})
    '        Else
    '            If valor > 100 Then valor = 100

    '            If Me Is misForms.busquedaHotel_ultimoActivo Then
    '                misForms.form1.UltraStatusBar1.Panels("pnl_3").ProgressBarInfo.Value = valor
    '                misForms.form1.UltraStatusBar1.Refresh()
    '                misForms.form1.UltraStatusBar1.Panels("pnl_3").Visible = visible
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
    '    End Try
    'End Sub

    Public Sub load_data()
        Try
            Me.Cursor = Cursors.WaitCursor

            If cs_ciudadPartida.ListBox1.SelectedIndex = -1 OrElse cs_ciudadDestino.ListBox1.SelectedIndex = -1 OrElse cs_ciudadPartida.lista_lugares.Count = 0 OrElse cs_ciudadDestino.lista_lugares.Count = 0 Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                datosBusqueda_vuelo.rubro = enumServices.Hotel
                datosBusqueda_vuelo.confirmada = False
                '
                datosBusqueda_vuelo.aerolinea = uce_aerolinea.SelectedItem.DataValue
                'datosBusqueda_vuelo.clase = uce_clase.SelectedItem.DataValue
                If uce_clase.SelectedItem.DataValue = "Y" Then datosBusqueda_vuelo.clase = "Economy" '  CapaPresentacion.Idiomas.get_str("str_0256_economica", "EN")
                If uce_clase.SelectedItem.DataValue = "C" Then datosBusqueda_vuelo.clase = "Business" ' CapaPresentacion.Idiomas.get_str("str_0257_negocios", "EN")
                If uce_clase.SelectedItem.DataValue = "F" Then datosBusqueda_vuelo.clase = "First" '    CapaPresentacion.Idiomas.get_str("str_0258_primeraClase", "EN")
                datosBusqueda_vuelo.moneda = uce_moneda.SelectedItem.DataValue
                If rbt_redondo.Checked Then datosBusqueda_vuelo.tipoViaje_Redondo = True Else datosBusqueda_vuelo.tipoViaje_Redondo = False
                '
                datosBusqueda_vuelo.partidaCiudad = cs_ciudadPartida.selected_valor_Codigo
                datosBusqueda_vuelo.partidaFecha = dtp_salidaPartida.Value
                datosBusqueda_vuelo.partidaHora = uce_horaPartida.SelectedItem.DataValue
                '
                datosBusqueda_vuelo.destinoCiudad = cs_ciudadDestino.selected_valor_Codigo
                datosBusqueda_vuelo.destinoFecha = dtp_regresoDestino.Value
                datosBusqueda_vuelo.destinoHora = uce_horaDestino.SelectedItem.DataValue
                '
                datosBusqueda_vuelo.adultos = txt_adultos.Text
                datosBusqueda_vuelo.mayores = txt_mayores.Text
                datosBusqueda_vuelo.ninos = txt_ninos.Text

                PictureBox2.Visible = True
                MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                MI_Thread.Start(Uc_itinerario1)
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0031", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        Finally
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

    Public Sub hacer_busqueda_async(ByVal OBJs As Object)
        Dim itin As uc_itinerario = CType(OBJs, uc_itinerario)

        Dim ds As New DataSet
        ds = obtenGenerales_vuelos.obten_general_AirAvail(datosBusqueda_vuelo)
        itin.data_source = ds
        Dim str_resp As String = "Total de vuelos: " + CStr(itin.vuelos_desplegados)
        If itin.vuelos_desplegados = 0 Then MsgBox("No se encontraron vuelos disponibles.")

        Fija_FOCUS(itin)
        Fija_PIC(False)
        Fija_LBL(data_lbl_vuelos, str_resp)
    End Sub

    Delegate Sub Fija_FOCUS_Callback(ByVal itin As uc_itinerario)
    Private Sub Fija_FOCUS(ByVal itin As uc_itinerario)
        Try
            If itin.InvokeRequired Then
                Dim d As New Fija_FOCUS_Callback(AddressOf Fija_FOCUS)
                Me.Invoke(d, New Object() {itin})
            Else
                If itin.lista_de_vuelos.Count > 0 Then CType(itin.lista_de_vuelos(0), uc_vuelo).RadioButton1.Focus()
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub Fija_PIC_Callback(ByVal vis As Boolean)
    Private Sub Fija_PIC(ByVal vis As Boolean)
        Try
            If PictureBox2.InvokeRequired Then
                Dim d As New Fija_PIC_Callback(AddressOf Fija_PIC)
                Me.Invoke(d, New Object() {vis})
            Else
                PictureBox2.Visible = vis
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub Fija_LBL_Callback(ByVal lbl As Label, ByVal str As String)
    Private Sub Fija_LBL(ByVal lbl As Label, ByVal str As String)
        Try
            If lbl.InvokeRequired Then
                Dim d As New Fija_LBL_Callback(AddressOf Fija_LBL)
                Me.Invoke(d, New Object() {lbl, str})
            Else
                lbl.Text = str
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    'Private Sub agrega_tarifas_async(ByVal t_o As Object)
    '    Try
    '        Dim t As DataTable = CType(t_o, DataTable)
    '        For Each r As DataRow In t.Rows
    '            r.Item("Tarifas") = obten_NOgeneral_HOC(r.Item("PropertyNumber"), datosBusqueda_vuelo)

    '            If total + avance <= 100 Then total += avance Else total = 99
    '            FijaProgressBar(total, True)
    '        Next
    '    Catch ex As Threading.ThreadAbortException
    '        ' no va nada
    '    Catch ex As Exception
    '        CapaLogicaNegocios.LogManager.agregar_registro("agrega_tarifas_async - busquedaHotel", ex.Message)

    '        If total + avance <= 100 Then total += avance Else total = 99
    '        FijaProgressBar(total, True)
    '    End Try
    'End Sub

    'Public Shared Function obten_NOgeneral_HOC(ByVal prop_number As String, ByVal datos_b As info_data) As String
    '    Try
    '        'validaciones
    '        Dim arr_cua_adul() As Integer = herramientas.get_arrCuartos(CInt(datos_b.habitaciones), CInt(datos_b.adultos))
    '        Dim arr_cua_chil() As Integer = herramientas.get_arrCuartos(CInt(datos_b.habitaciones), CInt(datos_b.ninos))
    '        'validaciones

    '        Dim data As New datos_HOC
    '        data.CheckinDate = Format(datos_b.llegada, CapaPresentacion.Common.DateDataFormat)
    '        data.CheckoutDate = Format(datos_b.salida, CapaPresentacion.Common.DateDataFormat)
    '        data.AccessCode = datos_b.codProm
    '        data.Segment.Add("P")
    '        data.Segment.Add("R")
    '        data.PropertyNumber = prop_number
    '        data.hatitaciones = datos_b.habitaciones
    '        data.Adults = arr_cua_adul
    '        data.Children = arr_cua_chil

    '        Dim ds As DataSet = get_ws.obten_HOC(data)

    '        If ds Is Nothing OrElse Not ds.Tables.Contains("RatePlan") OrElse ds.Tables("RatePlan").Rows.Count = 0 Then Return ""

    '        Dim minima As Double = Nothing
    '        Dim maxima As Double = Nothing
    '        For Each r As DataRow In ds.Tables("RatePlan").Rows
    '            If r.Item("Available").ToString.ToUpper = "N" Then Continue For

    '            If minima = Nothing Then minima = CDbl(r.Item("TotalAvgRate"))
    '            If maxima = Nothing Then maxima = CDbl(r.Item("TotalAvgRate"))

    '            If CDbl(r.Item("TotalAvgRate")) < minima Then minima = CDbl(r.Item("TotalAvgRate"))
    '            If CDbl(r.Item("TotalAvgRate")) > maxima Then maxima = CDbl(r.Item("TotalAvgRate"))
    '        Next

    '        If minima = Nothing OrElse maxima = Nothing Then Return ""

    '        Return Format(minima, "c") + " " + ds.Tables("Property").Rows(0).Item("Currency") + " - " + Format(maxima, "c") + " " + ds.Tables("Property").Rows(0).Item("Currency")
    '    Catch ex As Exception
    '        CapaLogicaNegocios.LogManager.agregar_registro("obten_NOgeneral_HOC - busquedaHotel", ex.Message)
    '    End Try

    '    Return ""
    'End Function

    'Private Sub colores_tarfas()
    '    MI_Thread.Join()

    '    Try
    '        total = 100
    '        FijaProgressBar(total, False)

    '        For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns
    '            col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
    '        Next
    '        For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In UltraGrid1.Rows
    '            If r.Cells("Tarifas").Value Is System.DBNull.Value OrElse r.Cells("Tarifas").Value = "" Then r.Appearance.ForeColor = Color.Gray
    '        Next

    '        Dim dv As DataView = UltraGrid1.DataSource
    '        dv.Sort = "Tarifas DESC, ServiceProvider ASC, PropertyName ASC"
    '        If datosBusqueda_vuelo.todosHoteles Then
    '            dv.RowFilter = ""
    '        Else
    '            dv.RowFilter = "Tarifas <> ''"
    '        End If
    '        UltraGrid1.DataSource = dv
    '    Catch ex As Exception
    '        'messagebox---(ex.Message)
    '        'Finally
    '        '    Fija_BTNActualizar()
    '    End Try
    'End Sub

    Public Sub llena_1()
        herramientas.llenar(True, cs_ciudadPartida.TextBox1.Text, cs_ciudadPartida)
    End Sub

    Public Sub llena_2()
        herramientas.llenar(True, cs_ciudadDestino.TextBox1.Text, cs_ciudadDestino)
    End Sub

    '

    'Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_salidaPartida.ValueChanged
    '    herramientas.fechas_inteligentes(dtp_in, dtp_out, txt_noches)
    'End Sub

    'Private Sub rbt_todosHoteles_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    datosBusqueda_vuelo.todosHoteles = rbt_todosHoteles.Checked

    '    Dim dv As DataView = UltraGrid1.DataSource
    '    If dv Is Nothing Then Return

    '    dv.Sort = "Tarifas DESC, PropertyName"
    '    If datosBusqueda_vuelo.todosHoteles Then
    '        dv.RowFilter = ""
    '    Else
    '        dv.RowFilter = "Tarifas <> ''"
    '    End If
    '    UltraGrid1.DataSource = dv

    '    For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In UltraGrid1.Rows
    '        If r.Cells("Tarifas").Value Is System.DBNull.Value OrElse r.Cells("Tarifas").Value = "" Then r.Appearance.ForeColor = Color.Gray
    '    Next
    'End Sub

    'Public Sub New(ByVal dc As dataCliente)

    '    ' This call is required by the Windows Form Designer.
    '    InitializeComponent()

    '    ' Add any initialization after the InitializeComponent() call.
    '    datos_cliente = dc
    'End Sub

    Private Sub busquedaVuelo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'CapaLogicaNegocios.Calls.VerificaNewCall()

        'Dim vuelo As New uc_vuelo
        'Dim unVuelo1 As New uc_unVuelo
        'unVuelo1.BackColor = Color.Purple
        'Dim unVuelo2 As New uc_unVuelo
        'unVuelo2.BackColor = Color.Red
        'Dim escala1 As New uc_escala
        'Dim escala2 As New uc_escala
        'Dim escala3 As New uc_escala
        'Dim escala4 As New uc_escala
        'Dim escala5 As New uc_escala

        'unVuelo1.add_escala(escala1)
        'unVuelo1.add_escala(escala2)
        'unVuelo2.add_escala(escala3)
        'unVuelo2.add_escala(escala4)
        'unVuelo2.add_escala(escala5)
        'vuelo.add_unVuelo(unVuelo1)
        'vuelo.add_unVuelo(unVuelo2)
        'Panel2.Controls.Add(vuelo)

        'UltraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect

        'dtp_salidaPartida.Value = DateTime.Now.AddDays(1)
        'dtp_out.Value = dtp_in.Value.AddDays(1)

        CapaPresentacion.Common.SetTabColor(Me)
        'Dim miColor As Color = herramientas.color_disponible
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackColor = miColor
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackColor2 = Color.FromArgb(100, 191, 219, 255)
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackColor = miColor
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackColor2 = Color.White
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical

        'botones = New String() {"btn_informacionGeneral", "btn_habitaciones", "btn_galeria", "btn_mapa", "btn_tarifas", "btn_reservar", "btn_reglas"}
        CapaPresentacion.Idiomas.cambiar_busquedaVuelo(Me)
        llenarCombos()
        'data_lbl_totalHoteles.Text = ""

        'For Each ctl As Control In Panel1.Controls
        '    Select Case ctl.GetType.Name
        '        Case "Label", "PictureBox", "Button"
        '            'estos no ejecutan el performClick
        '        Case Else
        '            AddHandler ctl.KeyPress, AddressOf ctlKeyPress
        '    End Select
        'Next

        cs_ciudadPartida.crea_lista(Me, ugb_partida.Location.X, ugb_partida.Location.Y)
        AddHandler cs_ciudadPartida.llena, AddressOf llena_1
        AddHandler cs_ciudadPartida.TextBox1.KeyPress, AddressOf ctlKeyPress
        AddHandler cs_ciudadPartida.ListBox1.KeyPress, AddressOf ctlKeyPress

        cs_ciudadDestino.crea_lista(Me, ugb_destino.Location.X, ugb_destino.Location.Y)
        AddHandler cs_ciudadDestino.llena, AddressOf llena_2
        AddHandler cs_ciudadDestino.TextBox1.KeyPress, AddressOf ctlKeyPress
        AddHandler cs_ciudadDestino.ListBox1.KeyPress, AddressOf ctlKeyPress

        Dim fecha_menor As DateTime = DateTime.Now.AddDays(7)           'polo dijo 7
        dtp_salidaPartida.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_regresoDestino.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_salidaPartida.Value = fecha_menor 'Now.AddDays(1)
        dtp_regresoDestino.Value = dtp_salidaPartida.Value.AddDays(1)

        data_lbl_vuelos.Text = "Total de vuelos: "
    End Sub

    Private Sub ctlKeyPress(ByVal s As Object, ByVal e As KeyPressEventArgs)
        If Asc(e.KeyChar) = Keys.Enter Then
            btn_buscar.PerformClick()
        End If
    End Sub

    'Private Sub busquedaHotel_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
    '    If Not MI_Thread Is Nothing Then MI_Thread.Abort()
    '    If Not UN_Thread Is Nothing Then UN_Thread.Abort()

    '    Do While lista_tarifas.Count > 0
    '        lista_tarifas(0).Close()            'si se elimina esta linea, tambien quitar lavariable de lista
    '        lista_tarifas.RemoveAt(0)
    '        If misForms.form1 Is Nothing Then
    '            Application.Exit()
    '        End If
    '    Loop

    '    If misForms.form1 Is Nothing Then
    '        Return
    '    End If

    '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_buscar").SharedProps.Enabled = True
    '    If misForms.form1.UltraDockManager1.DockAreas(0).Panes.Exists("informacionGeneral") Then misForms.form1.UltraDockManager1.DockAreas(0).Panes("informacionGeneral").Close()
    '    FijaProgressBar(100, False)
    'End Sub

    'Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
    '    misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    'End Sub

    'Private Sub UltraGrid1_AfterRowActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
    '    datosBusqueda_vuelo.PropertyNumber = UltraGrid1.ActiveRow.Cells("PropertyNumber").Value.ToString

    '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_informacionGeneral").SharedProps.Enabled = True
    '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_habitaciones").SharedProps.Enabled = True
    '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_galeria").SharedProps.Enabled = True
    '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_mapa").SharedProps.Enabled = True
    '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Enabled = True

    '    datosBusqueda_vuelo.ServiceProvider = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("ServiceProvider").Value.ToString()
    '    datosBusqueda_vuelo.PropertyNumber = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("PropertyNumber").Value.ToString()
    '    datosBusqueda_vuelo.PropertyName = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("PropertyName").Value.ToString()
    '    datosBusqueda_vuelo.Address = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("Address").Value.ToString()
    '    datosBusqueda_vuelo.CityName = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("CityName").Value.ToString()
    '    datosBusqueda_vuelo.CountryCode = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("CountryCode").Value.ToString()
    '    datosBusqueda_vuelo.ChainCode = misForms.busquedaHotel_ultimoActivo.UltraGrid1.ActiveRow.Cells("ChainCode").Value.ToString()

    '    CapaPresentacion.Idiomas.cambiar_busquedaHotel(Me)
    'End Sub

    'Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow, UltraGrid1.DoubleClickRow
    '    If cont = 0 Then
    '        misForms.exe_accion(ContextMenuStrip1, "btn_tarifas")
    '        cont += 1
    '    Else
    '        cont -= 1
    '    End If
    'End Sub

    'Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown, UltraGrid1.MouseDown
    '    herramientas.selRow(UltraGrid1, e)
    'End Sub

    'Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        misForms.exe_accion(ContextMenuStrip1, "btn_tarifas")
    '    End If
    'End Sub

    Private Sub btn_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_buscar.Click
        Uc_itinerario1.data_source = Nothing

        If cs_ciudadDestino.TextBox1.Text = "" AndAlso cs_ciudadPartida.TextBox1.Text = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0274_errSelLugares"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Dim pais As String = cs_ciudadPartida.selected_valor_Pais.ToUpper
        If pais <> "Mexico".ToUpper AndAlso pais <> "Canada".ToUpper AndAlso pais <> "UNITED STATES".ToUpper Then
            MsgBox("La ciudad de partida debe de ser de M�xico, Estados Unidos o Canad�.")
            Return
        End If

        Try
            If CInt(txt_adultos.Text) + CInt(txt_mayores.Text) + CInt(txt_ninos.Text) < 1 OrElse CInt(txt_adultos.Text) + CInt(txt_mayores.Text) + CInt(txt_ninos.Text) > 7 Then
                MsgBox("Solo se puede reservar entre 1 y 7 viajeros")
                Return
            End If
        Catch ex As Exception
            MsgBox("Solo se puede reservar entre 1 y 7 viajeros")
            Return
        End Try

        Try
            load_data()
            CapaPresentacion.Idiomas.cambiar_busquedaVuelo(Me)
            'UltraGrid1.Focus()

            'If Not frm.MI_Thread Is Nothing Then frm.MI_Thread.Abort()
            'If Not frm.UN_Thread Is Nothing Then frm.UN_Thread.Abort()
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0032", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Public Sub llenarCombos()
        uce_aerolinea.Items.Clear()
        uce_aerolinea.Items.Add("", CapaPresentacion.Idiomas.get_str("str_0255_sinPref"))
        uce_aerolinea.Items.Add("AA", "American Airlines")
        uce_aerolinea.Items.Add("AC", "Air Canada")
        uce_aerolinea.Items.Add("AF", "Air France")
        uce_aerolinea.Items.Add("AM", "Aeromexico")
        uce_aerolinea.Items.Add("AS", "Alaska Airlines")
        uce_aerolinea.Items.Add("AT", "Royal Air Maroc")
        uce_aerolinea.Items.Add("AY", "Finnair")
        uce_aerolinea.Items.Add("AZ", "Alitalia")
        uce_aerolinea.Items.Add("BA", "British Airways")
        uce_aerolinea.Items.Add("CO", "Continential Airlines")
        uce_aerolinea.Items.Add("DL", "Delta Airlines")
        uce_aerolinea.Items.Add("EI", "Aer Lingus")
        uce_aerolinea.Items.Add("F9", "Frontier Airlines")
        uce_aerolinea.Items.Add("HP", "America West")
        uce_aerolinea.Items.Add("IB", "Iberia")
        uce_aerolinea.Items.Add("LA", "Lan Chile")
        uce_aerolinea.Items.Add("LH", "Lufthansa")
        uce_aerolinea.Items.Add("MA", "Malev Hungarian")
        uce_aerolinea.Items.Add("MK", "Air Mauritius")
        uce_aerolinea.Items.Add("MX", "Mexicana")
        uce_aerolinea.Items.Add("NW", "Northwest Airlines")
        uce_aerolinea.Items.Add("OS", "Austria Air")
        uce_aerolinea.Items.Add("QF", "Qantas Airways")
        uce_aerolinea.Items.Add("RG", "Varig")
        uce_aerolinea.Items.Add("UA", "United Airlines")
        uce_aerolinea.Items.Add("US", "US Airways")
        If uce_aerolinea.SelectedIndex = -1 Then uce_aerolinea.SelectedIndex = 0

        uce_clase.Items.Clear()
        uce_clase.Items.Add("Y", CapaPresentacion.Idiomas.get_str("str_0256_economica"))
        uce_clase.Items.Add("C", CapaPresentacion.Idiomas.get_str("str_0257_negocios"))
        uce_clase.Items.Add("F", CapaPresentacion.Idiomas.get_str("str_0258_primeraClase"))
        If uce_clase.SelectedIndex = -1 Then uce_clase.SelectedIndex = 0

        CapaPresentacion.Common.moneda.FillUceMoneda(uce_moneda)

        uce_horaPartida.Items.Clear()
        uce_horaPartida.Items.Add(" ", CapaPresentacion.Idiomas.get_str("str_0259_cualquiera"))
        uce_horaPartida.Items.Add("DM", CapaPresentacion.Idiomas.get_str("str_0260_ma�ana") + "(7:00 to 11:00)")
        uce_horaPartida.Items.Add("DA", CapaPresentacion.Idiomas.get_str("str_0261_tarde") + "(12:00 to 16:00)")
        uce_horaPartida.Items.Add("DE", CapaPresentacion.Idiomas.get_str("str_0262_noche") + "(17:00 to 21:00)")
        uce_horaPartida.Items.Add("0000", "12:00 " + CapaPresentacion.Idiomas.get_str("str_0263_medianoche"))
        uce_horaPartida.Items.Add("0100", "1:00 AM")
        uce_horaPartida.Items.Add("0200", "2:00 AM")
        uce_horaPartida.Items.Add("0300", "3:00 AM")
        uce_horaPartida.Items.Add("0400", "4:00 AM")
        uce_horaPartida.Items.Add("0500", "5:00 AM")
        uce_horaPartida.Items.Add("0600", "6:00 AM")
        uce_horaPartida.Items.Add("0700", "7:00 AM")
        uce_horaPartida.Items.Add("0800", "8:00 AM")
        uce_horaPartida.Items.Add("0900", "9:00 AM")
        uce_horaPartida.Items.Add("1000", "10:00 AM")
        uce_horaPartida.Items.Add("1100", "11:00 AM")
        uce_horaPartida.Items.Add("1200", "12:00 PM")
        uce_horaPartida.Items.Add("1300", "1:00 PM")
        uce_horaPartida.Items.Add("1400", "2:00 PM")
        uce_horaPartida.Items.Add("1500", "3:00 PM")
        uce_horaPartida.Items.Add("1600", "4:00 PM")
        uce_horaPartida.Items.Add("1700", "5:00 PM")
        uce_horaPartida.Items.Add("1800", "6:00 PM")
        uce_horaPartida.Items.Add("1900", "7:00 PM")
        uce_horaPartida.Items.Add("2000", "8:00 PM")
        uce_horaPartida.Items.Add("2100", "9:00 PM")
        uce_horaPartida.Items.Add("2200", "10:00 PM")
        uce_horaPartida.Items.Add("2300", "11:00 PM")
        If uce_horaPartida.SelectedIndex = -1 Then uce_horaPartida.SelectedIndex = 0

        uce_horaDestino.Items.Clear()
        uce_horaDestino.Items.Add(" ", CapaPresentacion.Idiomas.get_str("str_0259_cualquiera"))
        uce_horaDestino.Items.Add("DM", CapaPresentacion.Idiomas.get_str("str_0260_ma�ana") + "(7:00 to 11:00)")
        uce_horaDestino.Items.Add("DA", CapaPresentacion.Idiomas.get_str("str_0261_tarde") + "(12:00 to 16:00)")
        uce_horaDestino.Items.Add("DE", CapaPresentacion.Idiomas.get_str("str_0262_noche") + "(17:00 to 21:00)")
        uce_horaDestino.Items.Add("0000", "12:00 " + CapaPresentacion.Idiomas.get_str("str_0263_medianoche"))
        uce_horaDestino.Items.Add("0100", "1:00 AM")
        uce_horaDestino.Items.Add("0200", "2:00 AM")
        uce_horaDestino.Items.Add("0300", "3:00 AM")
        uce_horaDestino.Items.Add("0400", "4:00 AM")
        uce_horaDestino.Items.Add("0500", "5:00 AM")
        uce_horaDestino.Items.Add("0600", "6:00 AM")
        uce_horaDestino.Items.Add("0700", "7:00 AM")
        uce_horaDestino.Items.Add("0800", "8:00 AM")
        uce_horaDestino.Items.Add("0900", "9:00 AM")
        uce_horaDestino.Items.Add("1000", "10:00 AM")
        uce_horaDestino.Items.Add("1100", "11:00 AM")
        uce_horaDestino.Items.Add("1200", "12:00 PM")
        uce_horaDestino.Items.Add("1300", "1:00 PM")
        uce_horaDestino.Items.Add("1400", "2:00 PM")
        uce_horaDestino.Items.Add("1500", "3:00 PM")
        uce_horaDestino.Items.Add("1600", "4:00 PM")
        uce_horaDestino.Items.Add("1700", "5:00 PM")
        uce_horaDestino.Items.Add("1800", "6:00 PM")
        uce_horaDestino.Items.Add("1900", "7:00 PM")
        uce_horaDestino.Items.Add("2000", "8:00 PM")
        uce_horaDestino.Items.Add("2100", "9:00 PM")
        uce_horaDestino.Items.Add("2200", "10:00 PM")
        uce_horaDestino.Items.Add("2300", "11:00 PM")
        If uce_horaDestino.SelectedIndex = -1 Then uce_horaDestino.SelectedIndex = 0

    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_salidaPartida.ValueChanged, dtp_regresoDestino.ValueChanged
        herramientas.fechas_inteligentes(dtp_salidaPartida, dtp_regresoDestino, sender, 0, Nothing)
    End Sub

    Private Sub Uc_itinerario1_DespuesVueloActivate(ByVal rbtChecked As System.Boolean) Handles Uc_itinerario1.DespuesVueloActivate
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservar_vuelo").SharedProps.Enabled = rbtChecked
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglas_vuelo").SharedProps.Enabled = rbtChecked
    End Sub

    Private Sub losRBT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbt_sencillo.CheckedChanged, rbt_redondo.CheckedChanged
        Dim ena As Boolean = rbt_redondo.Checked

        dtp_regresoDestino.Enabled = ena
        uce_horaDestino.Enabled = ena
    End Sub

    Private Sub losTXT_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_ninos.Leave, txt_mayores.Leave, txt_adultos.Leave
        herramientas.formatea_campoNumerico(sender)

        If txt_adultos.Text = "" Then txt_adultos.Text = "1"
        If txt_mayores.Text = "" Then txt_mayores.Text = "0"
        If txt_ninos.Text = "" Then txt_ninos.Text = "0"

        If CInt(txt_adultos.Text) > 7 Then txt_adultos.Text = "7"
        If CInt(txt_mayores.Text) > 7 Then txt_mayores.Text = "7"
        If CInt(txt_ninos.Text) > 6 Then txt_ninos.Text = "6"
    End Sub

End Class