Public Class uc_escala

    Private escala As dataVuelosItinerario.vuelo.unVuelo.escala

    Public Property data_escala() As dataVuelosItinerario.vuelo.unVuelo.escala
        Get
            Return escala
        End Get
        Set(ByVal value As dataVuelosItinerario.vuelo.unVuelo.escala)
            escala = value

            If escala Is Nothing Then Return

            llena_controles()
        End Set
    End Property

    Private Sub llena_controles()
        Dim t1 As DateTime = CDate(escala.ArrivalDateTime)
        Dim t2 As DateTime = CDate(escala.DepartureDateTime)
        Dim t As TimeSpan = New TimeSpan(0, CInt(escala.GroundDuration.Split("H")(1).Trim("M")), 0)

        Label1.Text = "Partida: " + t1.ToString("hh:mm tt")
        Label2.Text = escala.dep_str + ". Terminal " + escala.dep_Terminal
        Label3.Text = "llegada: " + t2.ToString("hh:mm tt")
        Label4.Text = escala.arr_str + ". Terminal " + escala.arr_Terminal

        Label5.Text = CStr(t.Hours) + " hr." + CStr(t.Minutes) + " min. | Equip. " + escala.equ_AirEquipType
        'Label5.Text = +" min. | Equip. " + escala.equ_AirEquipType
        Label6.Text = escala.m_air_CompanyShortName + " " + escala.FlightNumber
    End Sub

End Class
