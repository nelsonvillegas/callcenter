<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReservarVuelo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReservarVuelo))
        Me.txt_codViajero = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_codigoViajero = New System.Windows.Forms.Label
        Me.rbt_deseaBoletosElec = New System.Windows.Forms.RadioButton
        Me.rbt_deseaBoletoTrad = New System.Windows.Forms.RadioButton
        Me.btn_reservar = New System.Windows.Forms.Button
        Me.uce_entrega_pais = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lbl_entrega_pais = New System.Windows.Forms.Label
        Me.txt_entrega_email = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_email = New System.Windows.Forms.Label
        Me.txt_entrega_cp = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_cp = New System.Windows.Forms.Label
        Me.txt_entrega_estado = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_estado = New System.Windows.Forms.Label
        Me.txt_entrega_ciudad = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_ciudad = New System.Windows.Forms.Label
        Me.txt_entrega_direccion = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_direccion = New System.Windows.Forms.Label
        Me.txt_entrega_apellido = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_apellido = New System.Windows.Forms.Label
        Me.txt_entrega_nombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_nombre = New System.Windows.Forms.Label
        Me.btn_cancelar = New System.Windows.Forms.Button
        CType(Me.txt_codViajero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_entrega_pais, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_email, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_cp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_estado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_ciudad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_direccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_apellido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_nombre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_codViajero
        '
        Me.txt_codViajero.AlwaysInEditMode = True
        Me.txt_codViajero.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_codViajero.Location = New System.Drawing.Point(143, 12)
        Me.txt_codViajero.Name = "txt_codViajero"
        Me.txt_codViajero.Size = New System.Drawing.Size(265, 19)
        Me.txt_codViajero.TabIndex = 14
        '
        'lbl_codigoViajero
        '
        Me.lbl_codigoViajero.AutoSize = True
        Me.lbl_codigoViajero.BackColor = System.Drawing.Color.Transparent
        Me.lbl_codigoViajero.Location = New System.Drawing.Point(12, 15)
        Me.lbl_codigoViajero.Name = "lbl_codigoViajero"
        Me.lbl_codigoViajero.Size = New System.Drawing.Size(125, 13)
        Me.lbl_codigoViajero.TabIndex = 0
        Me.lbl_codigoViajero.Text = "C�digo viajero frecuente:"
        '
        'rbt_deseaBoletosElec
        '
        Me.rbt_deseaBoletosElec.AutoSize = True
        Me.rbt_deseaBoletosElec.Checked = True
        Me.rbt_deseaBoletosElec.Location = New System.Drawing.Point(15, 54)
        Me.rbt_deseaBoletosElec.Name = "rbt_deseaBoletosElec"
        Me.rbt_deseaBoletosElec.Size = New System.Drawing.Size(160, 17)
        Me.rbt_deseaBoletosElec.TabIndex = 16
        Me.rbt_deseaBoletosElec.TabStop = True
        Me.rbt_deseaBoletosElec.Text = "Prefiero Boletos Electr�nicos"
        Me.rbt_deseaBoletosElec.UseVisualStyleBackColor = True
        '
        'rbt_deseaBoletoTrad
        '
        Me.rbt_deseaBoletoTrad.AutoSize = True
        Me.rbt_deseaBoletoTrad.Location = New System.Drawing.Point(15, 77)
        Me.rbt_deseaBoletoTrad.Name = "rbt_deseaBoletoTrad"
        Me.rbt_deseaBoletoTrad.Size = New System.Drawing.Size(194, 17)
        Me.rbt_deseaBoletoTrad.TabIndex = 16
        Me.rbt_deseaBoletoTrad.TabStop = True
        Me.rbt_deseaBoletoTrad.Text = "Entreguen mis boletos tradicionales "
        Me.rbt_deseaBoletoTrad.UseVisualStyleBackColor = True
        '
        'btn_reservar
        '
        Me.btn_reservar.Location = New System.Drawing.Point(183, 320)
        Me.btn_reservar.Name = "btn_reservar"
        Me.btn_reservar.Size = New System.Drawing.Size(129, 23)
        Me.btn_reservar.TabIndex = 25
        Me.btn_reservar.Text = "Agregar a la lista"
        Me.btn_reservar.UseVisualStyleBackColor = True
        '
        'uce_entrega_pais
        '
        Me.uce_entrega_pais.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.uce_entrega_pais.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_entrega_pais.Location = New System.Drawing.Point(183, 255)
        Me.uce_entrega_pais.Name = "uce_entrega_pais"
        Me.uce_entrega_pais.Size = New System.Drawing.Size(107, 19)
        Me.uce_entrega_pais.TabIndex = 23
        '
        'lbl_entrega_pais
        '
        Me.lbl_entrega_pais.AutoSize = True
        Me.lbl_entrega_pais.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_pais.Location = New System.Drawing.Point(52, 258)
        Me.lbl_entrega_pais.Name = "lbl_entrega_pais"
        Me.lbl_entrega_pais.Size = New System.Drawing.Size(32, 13)
        Me.lbl_entrega_pais.TabIndex = 0
        Me.lbl_entrega_pais.Text = "Pa�s:"
        '
        'txt_entrega_email
        '
        Me.txt_entrega_email.AlwaysInEditMode = True
        Me.txt_entrega_email.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_email.Location = New System.Drawing.Point(183, 280)
        Me.txt_entrega_email.Name = "txt_entrega_email"
        Me.txt_entrega_email.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_email.TabIndex = 24
        '
        'lbl_entrega_email
        '
        Me.lbl_entrega_email.AutoSize = True
        Me.lbl_entrega_email.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_email.Location = New System.Drawing.Point(52, 283)
        Me.lbl_entrega_email.Name = "lbl_entrega_email"
        Me.lbl_entrega_email.Size = New System.Drawing.Size(96, 13)
        Me.lbl_entrega_email.TabIndex = 0
        Me.lbl_entrega_email.Text = "Correo electr�nico:"
        '
        'txt_entrega_cp
        '
        Me.txt_entrega_cp.AlwaysInEditMode = True
        Me.txt_entrega_cp.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_cp.Location = New System.Drawing.Point(183, 230)
        Me.txt_entrega_cp.Name = "txt_entrega_cp"
        Me.txt_entrega_cp.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_cp.TabIndex = 22
        Me.txt_entrega_cp.Text = "29100"
        '
        'lbl_entrega_cp
        '
        Me.lbl_entrega_cp.AutoSize = True
        Me.lbl_entrega_cp.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_cp.Location = New System.Drawing.Point(52, 233)
        Me.lbl_entrega_cp.Name = "lbl_entrega_cp"
        Me.lbl_entrega_cp.Size = New System.Drawing.Size(74, 13)
        Me.lbl_entrega_cp.TabIndex = 0
        Me.lbl_entrega_cp.Text = "Codigo postal:"
        '
        'txt_entrega_estado
        '
        Me.txt_entrega_estado.AlwaysInEditMode = True
        Me.txt_entrega_estado.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_estado.Location = New System.Drawing.Point(183, 205)
        Me.txt_entrega_estado.Name = "txt_entrega_estado"
        Me.txt_entrega_estado.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_estado.TabIndex = 21
        Me.txt_entrega_estado.Text = "BCS"
        '
        'lbl_entrega_estado
        '
        Me.lbl_entrega_estado.AutoSize = True
        Me.lbl_entrega_estado.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_estado.Location = New System.Drawing.Point(52, 208)
        Me.lbl_entrega_estado.Name = "lbl_entrega_estado"
        Me.lbl_entrega_estado.Size = New System.Drawing.Size(43, 13)
        Me.lbl_entrega_estado.TabIndex = 0
        Me.lbl_entrega_estado.Text = "Estado:"
        '
        'txt_entrega_ciudad
        '
        Me.txt_entrega_ciudad.AlwaysInEditMode = True
        Me.txt_entrega_ciudad.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_ciudad.Location = New System.Drawing.Point(183, 180)
        Me.txt_entrega_ciudad.Name = "txt_entrega_ciudad"
        Me.txt_entrega_ciudad.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_ciudad.TabIndex = 20
        Me.txt_entrega_ciudad.Text = "LA PAZ"
        '
        'lbl_entrega_ciudad
        '
        Me.lbl_entrega_ciudad.AutoSize = True
        Me.lbl_entrega_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_ciudad.Location = New System.Drawing.Point(52, 183)
        Me.lbl_entrega_ciudad.Name = "lbl_entrega_ciudad"
        Me.lbl_entrega_ciudad.Size = New System.Drawing.Size(43, 13)
        Me.lbl_entrega_ciudad.TabIndex = 0
        Me.lbl_entrega_ciudad.Text = "Ciudad:"
        '
        'txt_entrega_direccion
        '
        Me.txt_entrega_direccion.AlwaysInEditMode = True
        Me.txt_entrega_direccion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_direccion.Location = New System.Drawing.Point(183, 155)
        Me.txt_entrega_direccion.Name = "txt_entrega_direccion"
        Me.txt_entrega_direccion.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_direccion.TabIndex = 19
        Me.txt_entrega_direccion.Text = "CALLE MALECON #2345"
        '
        'lbl_entrega_direccion
        '
        Me.lbl_entrega_direccion.AutoSize = True
        Me.lbl_entrega_direccion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_direccion.Location = New System.Drawing.Point(52, 158)
        Me.lbl_entrega_direccion.Name = "lbl_entrega_direccion"
        Me.lbl_entrega_direccion.Size = New System.Drawing.Size(55, 13)
        Me.lbl_entrega_direccion.TabIndex = 0
        Me.lbl_entrega_direccion.Text = "Direcci�n:"
        '
        'txt_entrega_apellido
        '
        Me.txt_entrega_apellido.AlwaysInEditMode = True
        Me.txt_entrega_apellido.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_apellido.Location = New System.Drawing.Point(183, 130)
        Me.txt_entrega_apellido.Name = "txt_entrega_apellido"
        Me.txt_entrega_apellido.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_apellido.TabIndex = 18
        '
        'lbl_entrega_apellido
        '
        Me.lbl_entrega_apellido.AutoSize = True
        Me.lbl_entrega_apellido.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_apellido.Location = New System.Drawing.Point(52, 133)
        Me.lbl_entrega_apellido.Name = "lbl_entrega_apellido"
        Me.lbl_entrega_apellido.Size = New System.Drawing.Size(47, 13)
        Me.lbl_entrega_apellido.TabIndex = 0
        Me.lbl_entrega_apellido.Text = "Apellido:"
        '
        'txt_entrega_nombre
        '
        Me.txt_entrega_nombre.AlwaysInEditMode = True
        Me.txt_entrega_nombre.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_nombre.Location = New System.Drawing.Point(183, 105)
        Me.txt_entrega_nombre.Name = "txt_entrega_nombre"
        Me.txt_entrega_nombre.Size = New System.Drawing.Size(225, 19)
        Me.txt_entrega_nombre.TabIndex = 17
        '
        'lbl_entrega_nombre
        '
        Me.lbl_entrega_nombre.AutoSize = True
        Me.lbl_entrega_nombre.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_nombre.Location = New System.Drawing.Point(52, 108)
        Me.lbl_entrega_nombre.Name = "lbl_entrega_nombre"
        Me.lbl_entrega_nombre.Size = New System.Drawing.Size(47, 13)
        Me.lbl_entrega_nombre.TabIndex = 0
        Me.lbl_entrega_nombre.Text = "Nombre:"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(318, 320)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(90, 23)
        Me.btn_cancelar.TabIndex = 26
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'ReservarVuelo
        '
        Me.AcceptButton = Me.btn_reservar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(424, 362)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.uce_entrega_pais)
        Me.Controls.Add(Me.lbl_entrega_pais)
        Me.Controls.Add(Me.txt_entrega_email)
        Me.Controls.Add(Me.lbl_entrega_email)
        Me.Controls.Add(Me.txt_entrega_cp)
        Me.Controls.Add(Me.lbl_entrega_cp)
        Me.Controls.Add(Me.txt_entrega_estado)
        Me.Controls.Add(Me.lbl_entrega_estado)
        Me.Controls.Add(Me.txt_entrega_ciudad)
        Me.Controls.Add(Me.lbl_entrega_ciudad)
        Me.Controls.Add(Me.txt_entrega_direccion)
        Me.Controls.Add(Me.lbl_entrega_direccion)
        Me.Controls.Add(Me.txt_entrega_apellido)
        Me.Controls.Add(Me.lbl_entrega_apellido)
        Me.Controls.Add(Me.txt_entrega_nombre)
        Me.Controls.Add(Me.lbl_entrega_nombre)
        Me.Controls.Add(Me.btn_reservar)
        Me.Controls.Add(Me.rbt_deseaBoletoTrad)
        Me.Controls.Add(Me.rbt_deseaBoletosElec)
        Me.Controls.Add(Me.txt_codViajero)
        Me.Controls.Add(Me.lbl_codigoViajero)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ReservarVuelo"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reservar vuelo"
        CType(Me.txt_codViajero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_entrega_pais, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_email, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_cp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_estado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_ciudad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_direccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_apellido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_nombre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_codViajero As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_codigoViajero As System.Windows.Forms.Label
    Friend WithEvents rbt_deseaBoletosElec As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_deseaBoletoTrad As System.Windows.Forms.RadioButton
    Friend WithEvents btn_reservar As System.Windows.Forms.Button
    Friend WithEvents uce_entrega_pais As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_entrega_pais As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_email As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_email As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_cp As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_cp As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_estado As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_estado As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_ciudad As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_ciudad As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_direccion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_direccion As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_apellido As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_apellido As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_nombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_nombre As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
End Class
