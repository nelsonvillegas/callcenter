Public Class uc_unVuelo

    Public Sub add_escala(ByVal escala As uc_escala)
        Try
            pnl_body.Controls.Add(escala)

            Dim ctls_heigth = 0
            For Each ctl As Control In pnl_body.Controls
                ctls_heigth += ctl.Height
            Next

            Me.Height = pnl_header.Height + ctls_heigth + pnl_footer.Height + 10
        Catch ex As Exception
            MessageBox.Show(ex.Message, CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private unVuelo As dataVuelosItinerario.vuelo.unVuelo
    Private _is_ow As Boolean

    Public Property data_unVuelo() As dataVuelosItinerario.vuelo.unVuelo
        Get
            Return unVuelo
        End Get
        Set(ByVal value As dataVuelosItinerario.vuelo.unVuelo)
            unVuelo = value

            If unVuelo Is Nothing Then Return

            llena_controles()
        End Set
    End Property

    Private Sub llena_controles()
        Try
            If unVuelo.lista_escala.Count = 0 Then
                Label1.Text = ""
                Label2.Text = ""
                Label4.Text = ""
                Return
            End If

            If _is_ow Then Label1.Text = "Salida" Else Label1.Text = "Regreso"
            Label2.Text = CDate(unVuelo.lista_escala(0).ArrivalDateTime).ToString("ddd dd MMMM")

            Dim duracion As TimeSpan
            For Each escala As dataVuelosItinerario.vuelo.unVuelo.escala In unVuelo.lista_escala
                Dim ctl_escala As New uc_escala
                ctl_escala.data_escala = escala

                If duracion.Ticks = 0 Then duracion = New TimeSpan(0, CInt(escala.JourneyDuration.Split("H")(1).Trim("M")), 0) '+= CDate(escala.DepartureDateTime) - CDate(escala.ArrivalDateTime)

                add_escala(ctl_escala)
            Next

            Label4.Text = "Duracion: " + duracion.Hours.ToString + " hr. " + duracion.Minutes.ToString + " min."
        Catch ex As Exception
            MessageBox.Show(ex.Message, CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub New(ByVal IS_OW As Boolean)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _is_ow = IS_OW
    End Sub

End Class
