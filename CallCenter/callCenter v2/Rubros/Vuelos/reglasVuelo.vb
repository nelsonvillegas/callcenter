Public Class reglasVuelo

    Public datosBusqueda_vuelo As dataBusqueda_vuelo

    Public Sub New(ByVal db As dataBusqueda_vuelo)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        datosBusqueda_vuelo = db
    End Sub

    Private Sub reglas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'CapaPresentacion.Idiomas.cambiar_reglas(Me)
        load_data()
        inicializar_controles()
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        Close()
    End Sub

    'Public Function load_data() As Boolean
    '    Try
    '        Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)

    '        If ds.Tables.Count = 0 Then
    '            Return False
    '        End If

    '        If Not ds.Tables("error") Is Nothing Then
    '            Dim r As DataRow = ds.Tables("error").Rows(0)
    '            MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            Me.Cursor = Cursors.Arrow

    '            Return False
    '        Else
    '            Dim r As DataRow = ds.Tables("Property").Rows(0)
    '            Dim str As String = ""

    '            str += "<b>Hotel</b>: " + r.Item("HotelName") + "<br /><br />"

    '            str += "<b>Pol�ticas de cancelaci�n</b> <br />"
    '            str += r.Item("CancelationPoliticies") + "<br /><br />"

    '            str += "<b>Pol�ticas de garant�a</b> <br />"
    '            str += r.Item("GuarantyPoliticies") + "<br /><br />"

    '            str += "<b>Pol�ticas de la tarjeta de cr�dito</b> <br />"
    '            str += r.Item("CreditCardPoliticies") + "<br /><br />"

    '            str += "<b>Cargos extra</b> <br />"
    '            str += r.Item("ExtraCharges") + "<br /><br />"

    '            str += "<b>Edad m�xima de los ni�os</b> <br />"
    '            str += r.Item("MaxAgeOfChildren") + " A�os <br /><br />"

    '            str += "Entrada: " + r.Item("Checkin").ToString.Substring(0, 2) + ":" + r.Item("Checkin").ToString.Substring(2, 2) + "<br />"
    '            str += "Salida: " + r.Item("Checkout").ToString.Substring(0, 2) + ":" + r.Item("Checkout").ToString.Substring(2, 2) + "<br /><br />"

    '            str += "Impuestos: " + r.Item("Taxes") + "<br /><br />"

    '            str += "<b>Ubicaci�n</b> <br />"
    '            str += r.Item("Address") + "<br />"
    '            str += "C�digo postal: " + r.Item("PostalCode") + "<br />"
    '            str += r.Item("City") + ", " + r.Item("State") + ", " + r.Item("Country") + "<br />"

    '            str += "Moneda: " + r.Item("Money") + "<br /><br />"

    '            str += "<b>Informaci�n de dep�sito</b> <br />"
    '            str += r.Item("DepositInfo") + "<br /><br />"

    '            WebBrowser1.DocumentText = CONST_WB_TOP + str + CONST_WB_BOTTOM

    '            inicializar_controles()
    '            Return True
    '        End If
    '    Catch ex As Exception
    '        CapaLogicaNegocios.LogManager.agregar_registro("load_data - reglas", ex.Message)
    '        MessageBox.Show(ex.Message, CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Me.Cursor = Cursors.Arrow

    '        Return False
    '    End Try
    'End Function

    Private Function load_data() As Boolean
        Dim str As String = ""
        str += "<p><b>Vuelos</b></p>"
        str += "<p>"
        str += "Una vez que se emita el boleto, no puede ser reasignado a un pasajero o l�nea a�rea diferente."
        str += "El boleto no es reembolsable<br />"
        str += "Los cambios del boleto pueden incurrir en sanciones y/o incrementar la tarifa."
        str += "Este precio incluye un cargo por procesamiento no reembolsable de $192.17 MXN, que puede ser cargado por separado a su tarjeta de cr�dito."
        str += "El cargo de $192.17 MXN puede variar de acuerdo al tipo de cambio vigente en su Pa�s. "
        str += "Leer la descripci�n de las reglas y restricciones aplicables a esta tarifa. "
        str += "</p>"

        'str += "<p><b>Reglas de Tarifas</b></p>"
        'str += "<p>"
        'str += "Las tarifas mostradas reflejan un plan de tarifa en periodo regular (ej. diario, fin de semana, o mensual o especificado de ah� en adelante) basado en la informaci�n disponible a la hora de hacer la reservaci�n y puede ser sujeta a cambios.<br />"
        'str += "Los d�as adicionales posteriores al plan de tarifa regular ser�n cargados con tarifa de d�a extra. "
        'str += "Los d�as especiales y d�as pico pueden resultar en tarifas m�s altas, d�as exactos y tiempos var�an dependiendo la compa��a de auto renta.<br />"
        'str += "Los impuestos y cargos de los veh�culos no est�n incluidos. "
        'str += "Cargos adicionales pueden ser pagados localmente y/o aplicados a la hora de la renta como recargo de combustible, cargos adicionales de conductor, cargo por conductor joven y cargos por entrega y colecta.<br />"
        'str += "Cambios en fechas pueden ocasionar cambio de tarifas."
        'str += "A menos que se especifique de otra manera, las tarifas se muestran en d�lares. El tipo de cambio puede variar. Se le cobrar� usando el tipo de cambio y tipo de moneda locales a la hora de la entrega."
        'str += "</p>"

        'str += "<p><b>Requisitos para reservaciones por adelantado</b></p>"
        'str += "<p>"
        'str += "La mayor�a de las compa��as de autorentas requieren una de las (principales) tarjetas de cr�dito para asegurar y pagar la renta. Si usted desea usar una tarjeta de d�bito, por favor, antes de realizar su reserva cons�ltelo directamente con el arrendador. Las tarjetas de d�bito est�n sujetas a aceptaci�n por parte de las compa��as, y puede variar dependiendo de la locaci�n.<br />"
        'str += "Deber� presentar una licencia vigente al momento de la renta. Su r�cord de manejo podr�a ser revisado al momento de la renta como condici�n para concretar la renta. "
        'str += "Las reservaciones de autos son respetadas hasta el momento que se agend� para recoger el veh�culo. La compa��a de renta podr�a otorgar un periodo de gracia, bajo reserva. Contacte a la compa��a de renta para m�s detalles.<br />"
        'str += "Algunas reservaciones requieren una garant�a. Por favor cancele con 72 horas de anticipaci�n para evitar penalizaciones por cancelaci�n. "
        'str += "Si proporciona informaci�n falsa, la responsabilidad civil de la compa��a podr�a invalidarse con lo que el cliente podr�a ser culpable por da�os a s� mismo, al veh�culo y da�os materiales, as� como por da�os a terceros.<br />"
        'str += "Quitar los asientos de cualquier veh�culo no est� permitido."
        'str += "Podr�an aplicarse restricciones geogr�ficas, incluso para kilometraje ilimitado. Algunas compa��as de renta de autos no permiten abandonar el estado o el pa�s en el carro de renta. "
        'str += "</p>"

        WebBrowser1.DocumentText = CONST_WB_TOP + str + CONST_WB_BOTTOM
        Return True
    End Function

    Private Sub inicializar_controles()
        'datosBusqueda_hotel.noches = datosBusqueda_hotel.salida.Subtract(datosBusqueda_hotel.llegada).TotalDays

        data_lbl_adultos.Text = datosBusqueda_vuelo.adultos
        data_lbl_ninos.Text = datosBusqueda_vuelo.ninos
        data_lbl_mayores.Text = datosBusqueda_vuelo.mayores
        data_lbl_fechas.Text = datosBusqueda_vuelo.partidaFecha.ToString("dd/MMM/yyyy") + " - " + datosBusqueda_vuelo.destinoFecha.ToString("dd/MMM/yyyy")
        data_lbl_destino.Text = datosBusqueda_vuelo.partidaCiudad + " - " + datosBusqueda_vuelo.destinoCiudad
    End Sub

End Class