Public Class uc_itinerario

    Private Sub uc_itinerario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel1.Dock = DockStyle.Fill
        Me.Dock = DockStyle.Fill
    End Sub

    Private ds As DataSet
    Private datosVuelo_itinerario As dataVuelosItinerario
    Private _vuelos_desplegados As Integer = 0

    Public Property data_source() As DataSet
        Get
            Return ds
        End Get
        Set(ByVal value As DataSet)
            ds = value

            If ds Is Nothing Then
                'For Each ctl1 As Control In Panel1.Controls
                'Next

                PNL_CLR(Panel1)
                Return
            End If

            llena_controles()
        End Set
    End Property

    Public ReadOnly Property vuelos_desplegados() As Integer
        Get
            Return _vuelos_desplegados
        End Get
    End Property

    Private Sub llena_controles()
        inicializa_data()

        PNL_CLR(Panel1)

        _vuelos_desplegados = 0
        For Each datos_vuelo As dataVuelosItinerario.vuelo In datosVuelo_itinerario.lista_vuelos
            If datos_vuelo.vuelo_ow Is Nothing OrElse datos_vuelo.vuelo_rt Is Nothing Then Continue For

            ADD_DATA(Panel1, datos_vuelo)
            _vuelos_desplegados += 1
        Next
    End Sub

    Private Sub inicializa_data()
        Dim data As New dataVuelosItinerario

        'Dim OTA_Text As String = herramientas.transformar(Global.callCenter.My.Resources.OTA_To_OTAText, ds.GetXml)
        'Dim ds_vuelos As OTA_AirAvailRS = herramientas.llena_ds(OTA_Text, "OTA_AirAvailRS")

        'OTA_AirAvailRS.OriginDestinationOptionRow - ds_vuelos.OriginDestinationOption.Rows 
        For Each r_OriginDestinationOption As DataRow In ds.Tables("OriginDestinationOption").Rows
            Dim vuelo As New dataVuelosItinerario.vuelo

            'OTA_AirAvailRS.PricedItineraryRow
            Dim r_PricedItinerary As DataRow = r_OriginDestinationOption.GetParentRow("PricedItinerary_OriginDestinationOption")
            vuelo.cantidad = r_PricedItinerary.Item("Cant")
            vuelo.base = r_PricedItinerary.Item("Base")
            vuelo.AltCantidad = r_PricedItinerary.Item("AltCant")
            vuelo.AltBase = r_PricedItinerary.Item("AltBase")
            vuelo.AltCurrency = r_PricedItinerary.Item("AltCurrency")
            vuelo.RPH = r_PricedItinerary.Item("RPH")
            vuelo.PricedItinerary_id = r_PricedItinerary.Item("PricedItinerary_Id")

            Dim unVuelo_ow As New dataVuelosItinerario.vuelo.unVuelo
            Dim unVuelo_rt As New dataVuelosItinerario.vuelo.unVuelo

            'OTA_AirAvailRS.FlightSegmentRow 
            For Each r_FlightSegment As DataRow In r_OriginDestinationOption.GetChildRows("OriginDestinationOption_FlightSegment")
                Dim escala As New dataVuelosItinerario.vuelo.unVuelo.escala

                escala.Journy = r_FlightSegment.Item("Journy")
                escala.ArrivalDateTime = r_FlightSegment.Item("ArrivalDateTime")
                escala.DepartureDateTime = r_FlightSegment.Item("DepartureDateTime")
                escala.StopQuantity = r_FlightSegment.Item("StopQuantity")
                escala.FlightNumber = r_FlightSegment.Item("FlightNumber").PadLeft(4, "0000")
                escala.JourneyDuration = r_FlightSegment.Item("JourneyDuration")
                escala.OnTimeRate = r_FlightSegment.Item("OnTimeRate")
                escala.Ticket = r_FlightSegment.Item("Ticket")
                escala.SmokingAllowed = r_FlightSegment.Item("SmokingAllowed")
                escala.GroundDuration = r_FlightSegment.Item("GroundDuration")
                escala.AccumulatedDuration = r_FlightSegment.Item("AccumulatedDuration")
                escala.Identity = r_FlightSegment.Item("Identity")

                'OTA_AirAvailRS.DepartureAirportRow
                For Each r_DepartureAirport As DataRow In r_FlightSegment.GetChildRows("FlightSegment_DepartureAirport")
                    escala.dep_LocationCode = r_DepartureAirport.Item("LocationCode")
                    escala.dep_Terminal = r_DepartureAirport.Item("Terminal")
                    escala.dep_str = r_DepartureAirport.Item("DepartureAirport_Text")
                Next

                'OTA_AirAvailRS.ArrivalAirportRow
                For Each r_ArrivalAirport As DataRow In r_FlightSegment.GetChildRows("FlightSegment_ArrivalAirport")
                    escala.arr_LocationCode = r_ArrivalAirport.Item("LocationCode")
                    escala.arr_Terminal = r_ArrivalAirport.Item("Terminal")
                    escala.arr_str = r_ArrivalAirport.Item("ArrivalAirport_Text")
                Next

                'OTA_AirAvailRS.EquipmentRow
                For Each r_Equipment As DataRow In r_FlightSegment.GetChildRows("FlightSegment_Equipment")
                    escala.equ_AirEquipType = r_Equipment.Item("AirEquipType")
                    escala.equ_ChangeofGauge = r_Equipment.Item("ChangeofGauge")
                Next

                'OTA_AirAvailRS.MarketingAirlineRow
                For Each r_MarketingAirline As DataRow In r_FlightSegment.GetChildRows("FlightSegment_MarketingAirline")
                    escala.m_air_CompanyShortName = r_MarketingAirline.Item("CompanyShortName")
                    escala.m_air_OpAir = r_MarketingAirline.Item("OpAir")
                    escala.m_air_str = r_MarketingAirline.Item("MarketingAirline_Text")
                Next

                'OTA_AirAvailRS.MarketingCabinRow
                For Each r_MarketingCabin As DataRow In r_FlightSegment.GetChildRows("FlightSegment_MarketingCabin")
                    escala.m_cab_CabinType = r_MarketingCabin.Item("CabinType")
                    escala.m_cab_RPH = r_MarketingCabin.Item("RPH")
                Next

                If escala.Journy = "OW" Then
                    unVuelo_ow.lista_escala.Add(escala)
                Else
                    unVuelo_rt.lista_escala.Add(escala)
                End If
            Next

            vuelo.vuelo_ow = unVuelo_ow
            vuelo.vuelo_rt = unVuelo_rt

            '/////////////////////// ItinTotalFare \\\\\\\\\\\\\\\\\\\\\\\\
            'OTA_AirAvailRS.ItinTotalFareRow
            Dim r_ItinTotalFare As DataRow = r_PricedItinerary.GetChildRows("PricedItinerary_ItinTotalFare")(0)

            'OTA_AirAvailRS.BaseFareRow
            For Each r_BaseFare As DataRow In r_ItinTotalFare.GetChildRows("ItinTotalFare_BaseFare")
                vuelo.datos_ItinTotalFare.BaseFare_Amount = r_BaseFare.Item("Amount")
                vuelo.datos_ItinTotalFare.BaseFare_CurrencyCode = r_BaseFare.Item("CurrencyCode")
                vuelo.datos_ItinTotalFare.BaseFare_DecimalPlaces = r_BaseFare.Item("DecimalPlaces")
            Next

            'OTA_AirAvailRS.TaxRow
            For Each r_Tax As DataRow In r_ItinTotalFare.GetChildRows("ItinTotalFare_Taxes")(0).GetChildRows("Taxes_Tax")
                Dim tax As New dataVuelosItinerario.vuelo.ItinTotalFare.Tax
                tax.Tax_Amount = r_Tax.Item("Amount")
                tax.Tax_CurrencyCode = r_Tax.Item("CurrencyCode")
                tax.Tax_TaxCode = r_Tax.Item("TaxCode")

                vuelo.datos_ItinTotalFare.Taxes.Add(tax)
            Next

            'OTA_AirAvailRS.TotalFareRow
            For Each r_TotalFare As DataRow In r_ItinTotalFare.GetChildRows("ItinTotalFare_TotalFare")
                vuelo.datos_ItinTotalFare.TotalFare_Amount = r_TotalFare.Item("Amount")
                vuelo.datos_ItinTotalFare.TotalFare_CurrencyCode = r_TotalFare.Item("CurrencyCode")
                vuelo.datos_ItinTotalFare.TotalFare_DecimalPlaces = r_TotalFare.Item("DecimalPlaces")
            Next

            'OTA_AirAvailRS.FeeRow
            For Each r_Fee As DataRow In r_ItinTotalFare.GetChildRows("ItinTotalFare_Fees")(0).GetChildRows("Fees_Fee")
                Dim Fee As New dataVuelosItinerario.vuelo.ItinTotalFare.Fee
                Fee.Fee_Amount = r_Fee.Item("Amount")
                Fee.Fee_CurrencyCode = r_Fee.Item("CurrencyCode")
                Fee.Fee_DecimalPlaces = r_Fee.Item("DecimalPlaces")

                vuelo.datos_ItinTotalFare.Fees.Add(Fee)
            Next

            '/////////////////////// FareInfo \\\\\\\\\\\\\\\\\\\\\\\\
            'OTA_AirAvailRS.PTCRow 
            Dim r_PTC As DataRow = r_PricedItinerary.GetChildRows("PricedItinerary_FareInfo")(0).GetChildRows("FareInfo_PTC")(0)
            vuelo.datos_FareInfo.PTC_PassengerTypeCode = r_PTC.Item("PassengerTypeCode")

            'OTA_AirAvailRS.FICInfoRow
            For Each r_FICInfo As DataRow In r_PTC.GetChildRows("PTC_FICInfo")
                Dim FICInfo As New dataVuelosItinerario.vuelo.FareInfo.FICInfo
                FICInfo.FICInfo_Seg = r_FICInfo.Item("Seg")
                FICInfo.FICInfo_Fic = r_FICInfo.Item("Fic")

                vuelo.datos_FareInfo.PTC_lista_FICInfo.Add(FICInfo)
            Next

            '/////////////////////// TicketingInfo \\\\\\\\\\\\\\\\\\\\\\\\
            'OTA_AirAvailRS.TicketingInfoRow
            Dim r_TicketingInfo As DataRow = r_PricedItinerary.GetChildRows("PricedItinerary_TicketingInfo")(0)
            vuelo.TicketingInfo_TicketTimeLimit = r_TicketingInfo.Item("TicketTimeLimit")

            data.lista_vuelos.Add(vuelo)
        Next

        datosVuelo_itinerario = data
    End Sub

    'Private Sub inicializa_data()
    '    Dim data As New dataVuelosItinerario

    '    For Each dr As DataRow In ds.Tables("PricedItinerary").Rows
    '        Dim vuelo As New dataVuelosItinerario.vuelo
    '        vuelo.cantidad = dr.Item("Cant")
    '        vuelo.base = dr.Item("Base")
    '        vuelo.AltCantidad = dr.Item("AltCant")
    '        vuelo.AltBase = dr.Item("AltBase")
    '        vuelo.AltCurrency = dr.Item("AltCurrency")
    '        vuelo.RPH = dr.Item("RPH")
    '        vuelo.PricedItinerary_id = dr.Item("PricedItinerary_id")

    '        Dim unVuelo_ow As New dataVuelosItinerario.vuelo.unVuelo
    '        Dim unVuelo_rt As New dataVuelosItinerario.vuelo.unVuelo

    '        For Each dr2 As DataRow In ds.Tables("OriginDestinationOption").Rows
    '            If vuelo.PricedItinerary_id <> dr2.Item("PricedItinerary_id") Then Continue For
    '            If dr2.Item("Index") <> "1" Then Continue For 'esto es por que ponia todos las OPCIONES de un vuelo juntas por que en la interfaz no estan separadas

    '            Dim OriginDestinationOption_id As String = dr2.Item("OriginDestinationOption_id")
    '            For Each dr3 As DataRow In ds.Tables("FlightSegment").Rows
    '                If OriginDestinationOption_id <> dr3.Item("OriginDestinationOption_id") Then Continue For

    '                Dim escala As New dataVuelosItinerario.vuelo.unVuelo.escala

    '                escala.Journy = dr3.Item("Journy")
    '                escala.ArrivalDateTime = dr3.Item("ArrivalDateTime")
    '                escala.DepartureDateTime = dr3.Item("DepartureDateTime")
    '                escala.StopQuantity = dr3.Item("StopQuantity")
    '                escala.FlightNumber = dr3.Item("FlightNumber").ToString().PadLeft(4, "0000")
    '                escala.JourneyDuration = dr3.Item("JourneyDuration")
    '                escala.OnTimeRate = dr3.Item("OnTimeRate")
    '                escala.Ticket = dr3.Item("Ticket")
    '                escala.SmokingAllowed = dr3.Item("SmokingAllowed")
    '                escala.GroundDuration = dr3.Item("GroundDuration")
    '                escala.AccumulatedDuration = dr3.Item("AccumulatedDuration")
    '                escala.Identity = dr3.Item("Identity")

    '                Dim FlightSegment_id As String = dr3.Item("FlightSegment_id")

    '                For Each dr4 As DataRow In ds.Tables("DepartureAirport").Rows
    '                    If FlightSegment_id <> dr4.Item("FlightSegment_id") Then Continue For

    '                    escala.dep_LocationCode = dr4.Item("LocationCode")
    '                    escala.dep_Terminal = dr4.Item("Terminal")
    '                    escala.dep_str = dr4.Item("DepartureAirport_Text")
    '                Next

    '                For Each dr5 As DataRow In ds.Tables("ArrivalAirport").Rows
    '                    If FlightSegment_id <> dr5.Item("FlightSegment_id") Then Continue For

    '                    escala.arr_LocationCode = dr5.Item("LocationCode")
    '                    escala.arr_Terminal = dr5.Item("Terminal")
    '                    escala.arr_str = dr5.Item("ArrivalAirport_Text")
    '                Next

    '                For Each dr6 As DataRow In ds.Tables("Equipment").Rows
    '                    If FlightSegment_id <> dr6.Item("FlightSegment_id") Then Continue For

    '                    escala.equ_AirEquipType = dr6.Item("AirEquipType")
    '                    escala.equ_ChangeofGauge = dr6.Item("ChangeofGauge")
    '                Next

    '                For Each dr7 As DataRow In ds.Tables("MarketingAirline").Rows
    '                    If FlightSegment_id <> dr7.Item("FlightSegment_id") Then Continue For

    '                    escala.m_air_CompanyShortName = dr7.Item("CompanyShortName")
    '                    escala.m_air_OpAir = dr7.Item("OpAir")
    '                    escala.m_air_str = dr7.Item("MarketingAirline_Text")
    '                Next

    '                For Each dr8 As DataRow In ds.Tables("MarketingCabin").Rows
    '                    If FlightSegment_id <> dr8.Item("FlightSegment_id") Then Continue For

    '                    escala.m_cab_CabinType = dr8.Item("CabinType")
    '                    escala.m_cab_RPH = dr8.Item("RPH")
    '                Next

    '                If escala.Journy = "OW" Then
    '                    unVuelo_ow.lista_escala.Add(escala)
    '                Else
    '                    unVuelo_rt.lista_escala.Add(escala)
    '                End If
    '            Next

    '            vuelo.vuelo_ow = unVuelo_ow
    '            vuelo.vuelo_rt = unVuelo_rt
    '        Next

    '        'ItinTotalFare
    '        For Each dr9 As DataRow In ds.Tables("ItinTotalFare").Rows
    '            If vuelo.PricedItinerary_id <> dr9.Item("PricedItinerary_id") Then Continue For

    '            Dim ItinTotalFare_id As String = dr9.Item("ItinTotalFare_id")

    '            'BaseFare
    '            For Each dr10 As DataRow In ds.Tables("BaseFare").Rows
    '                If ItinTotalFare_id <> dr10.Item("ItinTotalFare_id") Then Continue For

    '                vuelo.datos_ItinTotalFare.BaseFare_Amount = dr10.Item("Amount")
    '                vuelo.datos_ItinTotalFare.BaseFare_CurrencyCode = dr10.Item("CurrencyCode")
    '                vuelo.datos_ItinTotalFare.BaseFare_DecimalPlaces = dr10.Item("DecimalPlaces")
    '            Next

    '            'Taxes
    '            For Each dr11 As DataRow In ds.Tables("Taxes").Rows
    '                If ItinTotalFare_id <> dr11.Item("ItinTotalFare_id") Then Continue For

    '                Dim Taxes_id As String = dr11.Item("Taxes_id")
    '                For Each dr12 As DataRow In ds.Tables("Tax").Rows
    '                    If Taxes_id <> dr12.Item("Taxes_id") Then Continue For

    '                    Dim tax As New dataVuelosItinerario.vuelo.ItinTotalFare.Tax
    '                    tax.Tax_Amount = dr12.Item("Amount")
    '                    tax.Tax_CurrencyCode = dr12.Item("CurrencyCode")
    '                    tax.Tax_TaxCode = dr12.Item("TaxCode")

    '                    vuelo.datos_ItinTotalFare.Taxes.Add(tax)
    '                Next
    '            Next

    '            'TotalFare
    '            For Each dr13 As DataRow In ds.Tables("TotalFare").Rows
    '                If ItinTotalFare_id <> dr13.Item("ItinTotalFare_id") Then Continue For

    '                vuelo.datos_ItinTotalFare.TotalFare_Amount = dr13.Item("Amount")
    '                vuelo.datos_ItinTotalFare.TotalFare_CurrencyCode = dr13.Item("CurrencyCode")
    '                vuelo.datos_ItinTotalFare.TotalFare_DecimalPlaces = dr13.Item("DecimalPlaces")
    '            Next

    '            'Fees
    '            For Each dr14 As DataRow In ds.Tables("Fees").Rows
    '                If ItinTotalFare_id <> dr14.Item("ItinTotalFare_id") Then Continue For

    '                Dim Fees_id As String = dr14.Item("Fees_id")
    '                For Each dr15 As DataRow In ds.Tables("Fee").Rows
    '                    If Fees_id <> dr15.Item("Fees_id") Then Continue For

    '                    Dim Fee As New dataVuelosItinerario.vuelo.ItinTotalFare.Fee
    '                    Fee.Fee_Amount = dr15.Item("Amount")
    '                    Fee.Fee_CurrencyCode = dr15.Item("CurrencyCode")
    '                    Fee.Fee_DecimalPlaces = dr15.Item("DecimalPlaces")

    '                    vuelo.datos_ItinTotalFare.Fees.Add(Fee)
    '                Next
    '            Next
    '        Next

    '        'FareInfo
    '        For Each dr16 As DataRow In ds.Tables("FareInfo").Rows
    '            If vuelo.PricedItinerary_id <> dr16.Item("PricedItinerary_id") Then Continue For

    '            Dim FareInfo_id As String = dr16.Item("FareInfo_id")

    '            'PTC
    '            For Each dr17 As DataRow In ds.Tables("PTC").Rows
    '                If FareInfo_id <> dr17.Item("FareInfo_id") Then Continue For

    '                vuelo.datos_FareInfo.PTC_PassengerTypeCode = dr17.Item("PassengerTypeCode")

    '                Dim PTC_id As String = dr17.Item("PTC_id")
    '                For Each dr18 As DataRow In ds.Tables("FICInfo").Rows
    '                    If PTC_id <> dr18.Item("PTC_id") Then Continue For

    '                    Dim FICInfo As New dataVuelosItinerario.vuelo.FareInfo.FICInfo
    '                    FICInfo.FICInfo_Seg = dr18.Item("Seg")
    '                    FICInfo.FICInfo_Fic = dr18.Item("Fic")

    '                    vuelo.datos_FareInfo.PTC_lista_FICInfo.Add(FICInfo)
    '                Next
    '            Next
    '        Next

    '        'TicketingInfo
    '        For Each dr19 As DataRow In ds.Tables("TicketingInfo").Rows
    '            If vuelo.PricedItinerary_id <> dr19.Item("PricedItinerary_id") Then Continue For

    '            vuelo.TicketingInfo_TicketTimeLimit = dr19.Item("TicketTimeLimit")
    '        Next

    '        data.lista_vuelos.Add(vuelo)
    '    Next

    '    datosVuelo_itinerario = data
    'End Sub

    Public ReadOnly Property lista_de_vuelos() As ControlCollection
        Get
            Return Panel1.Controls
        End Get
    End Property

    Delegate Sub PNL_CLR_Callback(ByVal pnl As Panel)
    Private Sub PNL_CLR(ByVal pnl As Panel)
        Try
            If Panel1.InvokeRequired Then
                Dim d As New PNL_CLR_Callback(AddressOf PNL_CLR)
                Me.Invoke(d, New Object() {pnl})
            Else
                pnl.Controls.Clear()
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub ADD_DATA_Callback(ByVal pnl As Panel, ByVal datos_vuelo As dataVuelosItinerario.vuelo)
    Private Sub ADD_DATA(ByVal pnl As Panel, ByVal datos_vuelo As dataVuelosItinerario.vuelo)
        Try
            If Panel1.InvokeRequired Then
                Dim d As New ADD_DATA_Callback(AddressOf ADD_DATA)
                Me.Invoke(d, New Object() {pnl, datos_vuelo})
            Else
                Dim clt_vuelo As New uc_vuelo
                clt_vuelo.data_source = datos_vuelo
                ADD_PNL(pnl, clt_vuelo)
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub ADD_PNL_Callback(ByVal pnl As Panel, ByVal clt_vuelo As uc_vuelo)
    Private Sub ADD_PNL(ByVal pnl As Panel, ByVal clt_vuelo As uc_vuelo)
        Try
            If pnl.InvokeRequired Then
                Dim d As New ADD_PNL_Callback(AddressOf ADD_PNL)
                Me.Invoke(d, New Object() {pnl, clt_vuelo})
            Else
                ADD_CTL(pnl, clt_vuelo)
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub ADD_CTL_Callback(ByVal pnl As Panel, ByVal clt_vuelo As uc_vuelo)
    Private Sub ADD_CTL(ByVal pnl As Panel, ByVal clt_vuelo As uc_vuelo)
        Try
            If clt_vuelo.InvokeRequired Then
                Dim d As New ADD_CTL_Callback(AddressOf ADD_CTL)
                Me.Invoke(d, New Object() {pnl, clt_vuelo})
            Else
                pnl.Controls.Add(clt_vuelo)
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub





    Public Event DespuesVueloActivate(ByVal rbtChecked As Boolean)

    Public Sub dispara_DespuesVueloActivate(ByVal rbtChecked As Boolean)
        RaiseEvent DespuesVueloActivate(rbtChecked)
        Application.DoEvents()
    End Sub

    Public Function TieneSelected() As Boolean
        For Each ctl As Control In lista_de_vuelos
            If CType(ctl, uc_vuelo).IS_SELECTED Then Return True
        Next

        Return False
    End Function


End Class
