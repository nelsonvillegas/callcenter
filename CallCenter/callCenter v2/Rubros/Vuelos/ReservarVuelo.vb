Public Class ReservarVuelo

    Public datosBusqueda_vuelo As dataBusqueda_vuelo
    'Public datos_call As dataCall
    Public tipoAccion As enumReservar_TipoAccion = enumReservar_TipoAccion.reservar

    Private Sub ReservarVuelo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If misForms.newCall Is Nothing Then btn_reservar.Enabled = False
    End Sub

    Public Function cargar() As Boolean
        clear_controls(Me)

        If CapaPresentacion.Common.cargar_paises(uce_entrega_pais) Then
            If tipoAccion = enumReservar_TipoAccion.reservar Then
                inicializar_controles()
            Else
                display_controles()
            End If

            Return True
        Else
            Return False
        End If

        Return True
    End Function

    Private Sub btn_reservar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_reservar.Click
        If Not verifica_datos_call() Then Return

        If tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado Then
            misForms.newCall.datos_call.rem_rvaVuelo(datosBusqueda_vuelo.t_id)
        End If

        'datosBusqueda_vuelo.nombre = txt_nombreTraveler.Text
        'datosBusqueda_vuelo.apellido = txt_apellido.Text
        'datosBusqueda_vuelo.nombre_prefijo = txt_nombre_p.Text
        'datosBusqueda_vuelo.apellido_prefijo = txt_apellido_p.Text
        'datosBusqueda_vuelo.direccion = txt_direccion.Text
        'datosBusqueda_vuelo.ciudad = txt_ciudad.Text
        'datosBusqueda_vuelo.estado = txt_estado.Text
        'datosBusqueda_vuelo.cp = txt_cp.Text
        'datosBusqueda_vuelo.pais = uce_paises.SelectedItem.DataValue
        'datosBusqueda_vuelo.telefono_AreaCityCode = txt_area.Text
        'datosBusqueda_vuelo.telefono_PhoneNumber = txt_telefono.Text
        'datosBusqueda_vuelo.telefono_PhoneUseType = "R"
        'datosBusqueda_vuelo.email = txt_email.Text
        datosBusqueda_vuelo.codViajero = txt_codViajero.Text
        'datosBusqueda_vuelo.cc_tipo = uce_tipoTarjeta.SelectedItem.DataValue
        'datosBusqueda_vuelo.cc_nombre = txt_ccNombre.Text
        'datosBusqueda_vuelo.cc_numero = txt_ccNumero.Text
        'datosBusqueda_vuelo.cc_numSeguridad = txt_ccNumeroSeguridad.Text
        'datosBusqueda_vuelo.cc_fechaExpira = txt_MM.Text + txt_AAAA.Text
        datosBusqueda_vuelo.entrega_tipo = rbt_deseaBoletosElec.Checked
        If Not datosBusqueda_vuelo.entrega_tipo Then
            datosBusqueda_vuelo.entrega_nombre = txt_entrega_nombre.Text
            datosBusqueda_vuelo.entrega_apellido = txt_entrega_apellido.Text
            datosBusqueda_vuelo.entrega_direccion = txt_entrega_direccion.Text
            datosBusqueda_vuelo.entrega_ciudad = txt_entrega_ciudad.Text
            datosBusqueda_vuelo.entrega_estado = txt_entrega_estado.Text
            datosBusqueda_vuelo.entrega_cp = txt_entrega_cp.Text
            datosBusqueda_vuelo.entrega_pais = uce_entrega_pais.SelectedItem.DataValue
            datosBusqueda_vuelo.entrega_email = txt_entrega_email.Text
        End If

        If misForms.newCall.datos_call.add_rvaVuelo(datosBusqueda_vuelo.Clone, False) Then
            CapaPresentacion.Calls.ActivaNewCall()
            Close()
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0297_soloUno"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub losRBT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbt_deseaBoletosElec.CheckedChanged, rbt_deseaBoletoTrad.CheckedChanged
        Dim en As Boolean = Not rbt_deseaBoletosElec.Checked

        txt_entrega_apellido.Enabled = en
        txt_entrega_ciudad.Enabled = en
        txt_entrega_cp.Enabled = en
        txt_entrega_direccion.Enabled = en
        txt_entrega_email.Enabled = en
        txt_entrega_estado.Enabled = en
        txt_entrega_nombre.Enabled = en
        uce_entrega_pais.Enabled = en
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub inicializar_controles()

    End Sub

    Private Sub display_controles()
        'txt_nombreTraveler.Text = datosBusqueda_vuelo.nombre
        'txt_apellido.Text = datosBusqueda_vuelo.apellido
        'txt_nombre_p.Text = datosBusqueda_vuelo.nombre_prefijo
        'txt_apellido_p.Text = datosBusqueda_vuelo.apellido_prefijo
        'txt_direccion.Text = datosBusqueda_vuelo.direccion
        'txt_ciudad.Text = datosBusqueda_vuelo.ciudad
        'txt_estado.Text = datosBusqueda_vuelo.estado
        'txt_cp.Text = datosBusqueda_vuelo.cp
        ''uce_paises.selec = datosBusqueda_vuelo.cp
        'txt_area.Text = datosBusqueda_vuelo.telefono_AreaCityCode
        'txt_telefono.Text = datosBusqueda_vuelo.telefono_PhoneNumber
        'txt_tipo.Text = datosBusqueda_vuelo.telefono_PhoneUseType
        'txt_email.Text = datosBusqueda_vuelo.email
        txt_codViajero.Text = datosBusqueda_vuelo.codViajero
        'uce_tipoTarjeta.Text = datosBusqueda_vuelo.codViajero
        'txt_ccNombre.Text = datosBusqueda_vuelo.cc_nombre
        'txt_ccNumero.Text = datosBusqueda_vuelo.cc_numero
        'txt_ccNumeroSeguridad.Text = datosBusqueda_vuelo.cc_numSeguridad
        'If datosBusqueda_vuelo.cc_fechaExpira.Length = 6 Then txt_MM.Text = datosBusqueda_vuelo.cc_fechaExpira.Substring(0, 2)
        'If datosBusqueda_vuelo.cc_fechaExpira.Length = 6 Then txt_AAAA.Text = datosBusqueda_vuelo.cc_fechaExpira.Substring(2, 4)

        If Not datosBusqueda_vuelo.entrega_tipo Then
            rbt_deseaBoletosElec.Checked = datosBusqueda_vuelo.entrega_tipo
            txt_entrega_nombre.Text = datosBusqueda_vuelo.entrega_nombre
            txt_entrega_apellido.Text = datosBusqueda_vuelo.entrega_apellido
            txt_entrega_direccion.Text = datosBusqueda_vuelo.entrega_direccion
            txt_entrega_ciudad.Text = datosBusqueda_vuelo.entrega_ciudad
            txt_entrega_estado.Text = datosBusqueda_vuelo.entrega_estado
            txt_entrega_cp.Text = datosBusqueda_vuelo.entrega_cp
            uce_entrega_pais.SelectedItem.DataValue = datosBusqueda_vuelo.entrega_pais
            txt_entrega_email.Text = datosBusqueda_vuelo.entrega_email
        End If
    End Sub

    Private Sub txtControls_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar.ToString) AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ControlChars.NullChar
    End Sub
End Class