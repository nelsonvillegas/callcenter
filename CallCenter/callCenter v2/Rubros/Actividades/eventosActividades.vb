Public Class eventosActividades

    Public botones() As String
    Private cont As Integer = 0
    Private ds_actividades As DataSet
    'Public datos_call As dataCall
    Public datosBusqueda_actividad As New dataBusqueda_actividad_test
    Public MI_Thread As Threading.Thread
    '---------------------------'

    'Public grid_Actividades As New DataGridView
    Public panel_Actividades As New Panel
    '--------------------------------'

    Public Sub New(ByVal db As dataBusqueda_actividad_test) 'ByVal dc As dataCall,

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
        datosBusqueda_actividad = db.Clone
    End Sub

    Private Sub eventosActividades_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'menuContextual.inicializar_items(misForms.busquedaActividad_ultimoActivo.ContextMenuStrip1, misForms.busquedaActividad_ultimoActivo.botones)
        misForms.eventosActividades_ultimoActivo = Nothing
    End Sub

    Private Sub eventosActividades_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)

        CapaPresentacion.Common.SetTabColor(Me)
        Dim tem_settings As Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings = misForms.form1.UltraTabbedMdiManager1.TabFromForm(misForms.busquedaActividad_ultimoActivo).Settings
        misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance = tem_settings.TabAppearance
        misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance = tem_settings.SelectedTabAppearance

        botones = New String() {"btn_reservarActividad", "btn_reglasActividad"}
        CapaPresentacion.Idiomas.cambiar_eventosActividades(Me)

        '*********************************************'


        Me.UltraGrid1.Visible = False
        Me.panel_Actividades.Location = New Point(0, 69)
        Me.panel_Actividades.Width = Me.Width
        Me.panel_Actividades.Height = Me.Height - Me.panel_Actividades.Top

        Me.panel_Actividades.AutoScroll = True


        Me.Controls.Add(Me.panel_Actividades)

        '****************************************************'
        CapaLogicaNegocios.Activities.ActivityEvents.load_data(Me)
        UltraGrid1.Focus()
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        'datosBusqueda_actividad.EventID = UltraGrid1.ActiveRow.Cells("EventID").Value - se supone que ya esta desde la ventana anterior
        datosBusqueda_actividad.PercentTax = UltraGrid1.ActiveRow.Cells("PercentTax").Value
        datosBusqueda_actividad.AltCurrency = UltraGrid1.ActiveRow.Cells("AltCurrency").Value
        datosBusqueda_actividad.EventTickets = UltraGrid1.ActiveRow.Cells("EventTickets").Value
        datosBusqueda_actividad.EventName = UltraGrid1.ActiveRow.Cells("EventName").Value

        datosBusqueda_actividad.RateID = UltraGrid1.ActiveRow.Cells("RateID").Value
        datosBusqueda_actividad.RateTypeID = UltraGrid1.ActiveRow.Cells("RateTypeID").Value
        datosBusqueda_actividad.RateTypeName = UltraGrid1.ActiveRow.Cells("RateTypeName").Value
        datosBusqueda_actividad.DayReservar = UltraGrid1.ActiveRow.Cells("DayReservar").Value
        datosBusqueda_actividad.MinTickets = UltraGrid1.ActiveRow.Cells("MinTickets").Value
        datosBusqueda_actividad.MaxTickets = UltraGrid1.ActiveRow.Cells("MaxTickets").Value
        datosBusqueda_actividad.AltRateTypePrice = UltraGrid1.ActiveRow.Cells("AltRateTypePrice").Value
        datosBusqueda_actividad.costoTotalFormato = UltraGrid1.ActiveRow.Cells("costoTotalFormato").Value
        

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarActividad").SharedProps.Enabled = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasActividad").SharedProps.Enabled = True

        CapaPresentacion.Idiomas.cambiar_eventosActividades(Me)
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow, UltraGrid1.DoubleClickRow
        'por alguna extra�a razon se ejecuta 2 veces, por eso con CONT se controla para que solo entre una vez
        If cont = 0 Then
            misForms.exe_accion(ContextMenuStrip1, "btn_reservarActividad")
            cont += 1
        Else
            cont -= 1
        End If
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown, UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        If e.KeyCode = Keys.Enter Then
            misForms.exe_accion(ContextMenuStrip1, "btn_reservarActividad")
        End If
    End Sub

    Private Sub UltraGrid1_AfterSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BandEventArgs) Handles UltraGrid1.AfterSortChange
        CapaPresentacion.Common.NumberGrid(UltraGrid1)
    End Sub

    Private Sub UltraGrid1_BeforeSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeSortChangeEventArgs) Handles UltraGrid1.BeforeSortChange
        For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In e.SortedColumns
            If col.Key = "costoTotalFormato" Then
                Dim sortAsc As Boolean = IIf(UltraGrid1.DisplayLayout.Bands(0).Columns("TotalOrdenar").SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, True, False)
                e.SortedColumns.Remove("costoTotalFormato")
                e.SortedColumns.Add("TotalOrdenar", sortAsc)

                Exit For
            End If
        Next
    End Sub

    Private Sub btn_siguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_siguiente.Click
        Dim listRate As List(Of RateType) = New List(Of RateType)
        For Each control As Control In panel_Actividades.Controls
            Dim ctrl As actFareAvail = CType(control, actFareAvail)
            ctrl.GetSelected(listRate)
        Next
        datosBusqueda_actividad.ListRateType = listRate

        Dim uc1 As actFareAvail = CType(panel_Actividades.Controls(0), actFareAvail)
        datosBusqueda_actividad.PercentTax = uc1.PercentTax
        datosBusqueda_actividad.AltCurrency = uc1.AltCurrency
        datosBusqueda_actividad.EventTickets = uc1.EventTickets
        'datosBusqueda_actividad.EventName = UltraGrid1.ActiveRow.Cells("EventName").Value
        '
        datosBusqueda_actividad.DayReservar = uc1.Fecha.Substring(0, 8)


        misForms.exe_accion(ContextMenuStrip1, "btn_reservarActividad")
    End Sub

    Public Sub Total_Changed()
        Dim suma As Double = 0
        For Each ctrl As Control In panel_Actividades.Controls
            If CType(ctrl, actFareAvail).HoraSelected Then suma = suma + CType(ctrl, actFareAvail).Total
        Next
        data_lbl_total.Text = CapaPresentacion.Common.moneda.FormateaMoneda(suma, Me.datosBusqueda_actividad.monedaBusqueda)
    End Sub
End Class