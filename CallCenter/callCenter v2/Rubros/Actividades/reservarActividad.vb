Public Class reservarActividad

    Public datosBusqueda_actividad As dataBusqueda_actividad_test
    Public tipoAccion As enumReservar_TipoAccion = enumReservar_TipoAccion.reservar

    Private Sub reservarActividad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If misForms.newCall Is Nothing Then btn_reservar.Enabled = False
        CapaPresentacion.Idiomas.cambiar_reservarActividad(Me)
        If tipoAccion = enumReservar_TipoAccion.reservar Then clear_controls(Me)
    End Sub

    Public Function cargar() As Boolean
        If tipoAccion = enumReservar_TipoAccion.reservar Then
            inicializar_controles()
        Else
            display_controles()
        End If

        Return True
    End Function

    Private Sub btn_reservar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_reservar.Click
        Dim res As callCenter.CallOption.CallResult = CapaLogicaNegocios.Calls.SelectOption()

        If res <> CallOption.CallResult.Cancel Then
            If tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado Then
                misForms.newCall.datos_call.rem_rvaActividad(datosBusqueda_actividad.t_id)
            End If

            datosBusqueda_actividad.entrega_direccion = txt_entrega_direccion.Text
            datosBusqueda_actividad.entrega_cp = txt_entrega_cp.Text
            datosBusqueda_actividad.peticionEspecial = txt_peticion.Text
            datosBusqueda_actividad.cantidad = cmb_cantidad.SelectedItem 'txt_cantidad.Text

            misForms.newCall.datos_call.add_rvaActividad(datosBusqueda_actividad.Clone)
            CapaPresentacion.Calls.ActivaNewCall()
            Close()
        End If
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub UltraCalendarInfo1_AfterSelectedDateRangeChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraCalendarInfo1.AfterSelectedDateRangeChange
        ListBox1.Items.Clear()
        For Each rango As Infragistics.Win.UltraWinSchedule.DateRange In UltraCalendarInfo1.SelectedDateRanges
            For Each dia As Infragistics.Win.UltraWinSchedule.Day In rango.Days
                ListBox1.Items.Add(dia.Date.ToString("dd/MMM/yyyy"))
            Next
        Next
    End Sub

    Private Sub inicializar_controles()
        cmb_cantidad.Items.Clear()
        Dim j As Integer = datosBusqueda_actividad.MinTickets
        If j = 0 Then j = 1

        If CInt(datosBusqueda_actividad.MinTickets) > CInt(datosBusqueda_actividad.EventTickets) Then
            btn_reservar.Enabled = False
            cmb_cantidad.Enabled = False
            Return
        End If

        Dim menor_maximo As Integer = Nothing
        If CInt(datosBusqueda_actividad.MaxTickets) < CInt(datosBusqueda_actividad.EventTickets) Then menor_maximo = datosBusqueda_actividad.MaxTickets Else menor_maximo = datosBusqueda_actividad.EventTickets

        For i As Integer = j To menor_maximo
            cmb_cantidad.Items.Add(i)
        Next
        cmb_cantidad.SelectedIndex = 0

        Dim can As Double = CDbl(datosBusqueda_actividad.AltRateTypePrice) * CDbl(cmb_cantidad.Items(cmb_cantidad.SelectedIndex))
        datosBusqueda_actividad.costoTotalFormato = CapaPresentacion.Common.moneda.FormateaMoneda(can, datosBusqueda_actividad.AltCurrency) ' Format(can, "c") + " " + datosBusqueda_actividad.AltCurrency
        data_lbl_total.Text = datosBusqueda_actividad.costoTotalFormato
    End Sub

    Private Sub display_controles()
        txt_entrega_direccion.Text = datosBusqueda_actividad.entrega_direccion
        txt_entrega_cp.Text = datosBusqueda_actividad.entrega_cp
        txt_peticion.Text = datosBusqueda_actividad.peticionEspecial
        cmb_cantidad.SelectedItem = datosBusqueda_actividad.cantidad
        data_lbl_total.Text = datosBusqueda_actividad.costoTotalFormato 'RateTypePrice
    End Sub

    Private Sub cmb_cantidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_cantidad.SelectedIndexChanged
        Dim can As Double = CDbl(datosBusqueda_actividad.AltRateTypePrice) * CDbl(cmb_cantidad.Items(cmb_cantidad.SelectedIndex))
        datosBusqueda_actividad.costoTotalFormato = CapaPresentacion.Common.moneda.FormateaMoneda(can, datosBusqueda_actividad.AltCurrency) ' Format(can, "c") + " " + datosBusqueda_actividad.AltCurrency
        data_lbl_total.Text = datosBusqueda_actividad.costoTotalFormato
    End Sub

End Class