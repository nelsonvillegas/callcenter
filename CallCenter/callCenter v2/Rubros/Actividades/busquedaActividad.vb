Public Class busquedaActividad

    Public botones() As String
    Private cont As Integer = 0
    Public datosBusqueda_actividad As New dataBusqueda_actividad_test
    Public MI_Thread As Threading.Thread

    Private Sub btn_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_buscar.Click
        Try
            CapaLogicaNegocios.Activities.SearchActivity.load_data(Me)
            UltraGrid1.Focus()
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0002", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub cs_ciudad_llena() Handles cs_ciudad.llena
        herramientas.llenar(True, cs_ciudad.TextBox1.Text, cs_ciudad)
    End Sub

    Private Sub busquedaActividad_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        misForms.busquedaActividad_ultimoActivo = Nothing
    End Sub

    Private Sub busquedaActividad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'CapaLogicaNegocios.Calls.VerificaNewCall()

        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        CapaPresentacion.Common.SetTabColor(Me)

        botones = New String() {"btn_eventosActividad"}
        CapaPresentacion.Idiomas.cambiar_busquedaActividad(Me)

        cs_ciudad.crea_lista(Me, Panel1.Location.X, Panel1.Location.Y)
        For Each ctl As Control In Panel1.Controls
            Select Case ctl.GetType.Name
                Case "Label", "PictureBox", "Button"
                    'estos no ejecutan el performClick
                Case Else
                    AddHandler ctl.KeyPress, AddressOf ctlKeyPress
            End Select
        Next
        AddHandler cs_ciudad.TextBox1.KeyPress, AddressOf ctlKeyPress
        AddHandler cs_ciudad.ListBox1.KeyPress, AddressOf ctlKeyPress

        CapaPresentacion.Common.moneda.FillUceMoneda(uce_moneda)
        Dim fecha_menor As DateTime = DateTime.Now
        dtp_fechaDesde.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_fechaHasta.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_fechaHasta.Value = dtp_fechaDesde.Value.AddDays(1)

        data_lbl_act.Text = ""
    End Sub

    Private Sub ctlKeyPress(ByVal s As Object, ByVal e As KeyPressEventArgs)
        If Asc(e.KeyChar) = Keys.Enter Then
            btn_buscar.PerformClick()
        End If
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        datosBusqueda_actividad.PropertyID = UltraGrid1.ActiveRow.Cells("PropertyID").Value
        datosBusqueda_actividad.PropertyName = UltraGrid1.ActiveRow.Cells("PropertyName").Value
        datosBusqueda_actividad.ActivityID = UltraGrid1.ActiveRow.Cells("ActivityID").Value
        datosBusqueda_actividad.EventID = UltraGrid1.ActiveRow.Cells("EventID").Value
        datosBusqueda_actividad.EventName = UltraGrid1.ActiveRow.Cells("EventName").Value

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_eventosActividad").SharedProps.Enabled = True

        CapaPresentacion.Idiomas.cambiar_busquedaActividad(Me)
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow, UltraGrid1.DoubleClickRow
        'por alguna extra�a razon se ejecuta 2 veces, por eso con CONT se controla para que solo entre una vez
        If cont = 0 Then
            misForms.exe_accion(ContextMenuStrip1, "btn_eventosActividad")
            cont += 1
        Else
            cont -= 1
        End If
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown, UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        If e.KeyCode = Keys.Enter AndAlso UltraGrid1.ActiveRow IsNot Nothing Then
            misForms.exe_accion(ContextMenuStrip1, "btn_eventosActividad")
        End If
    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_fechaDesde.ValueChanged, dtp_fechaHasta.ValueChanged
        herramientas.fechas_inteligentes(dtp_fechaDesde, dtp_fechaHasta, sender, 0, Nothing)
    End Sub

    Private Sub UltraGrid1_AfterSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BandEventArgs) Handles UltraGrid1.AfterSortChange
        CapaPresentacion.Common.NumberGrid(UltraGrid1)
    End Sub

    Private Sub UltraGrid1_BeforeSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeSortChangeEventArgs) Handles UltraGrid1.BeforeSortChange
        For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In e.SortedColumns
            If col.Key = "AltFromPrice" Then
                Dim sortAsc As Boolean = IIf(UltraGrid1.DisplayLayout.Bands(0).Columns("TotalOrdenar").SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, True, False)
                e.SortedColumns.Remove("AltFromPrice")
                e.SortedColumns.Add("TotalOrdenar", sortAsc)

                Exit For
            End If
        Next
    End Sub

End Class