<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reservarActividad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reservarActividad))
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.txt_entrega_cp = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_cp = New System.Windows.Forms.Label
        Me.txt_entrega_direccion = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_entrega_direccion = New System.Windows.Forms.Label
        Me.btn_reservar = New System.Windows.Forms.Button
        Me.txt_peticion = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_peticion = New System.Windows.Forms.Label
        Me.UltraCalendarLook1 = New Infragistics.Win.UltraWinSchedule.UltraCalendarLook(Me.components)
        Me.UltraMonthViewMulti1 = New Infragistics.Win.UltraWinSchedule.UltraMonthViewMulti
        Me.UltraCalendarInfo1 = New Infragistics.Win.UltraWinSchedule.UltraCalendarInfo(Me.components)
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.lbl_cantidad = New System.Windows.Forms.Label
        Me.cmb_cantidad = New System.Windows.Forms.ComboBox
        Me.data_lbl_total = New System.Windows.Forms.Label
        Me.lbl_total = New System.Windows.Forms.Label
        CType(Me.txt_entrega_cp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_entrega_direccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_peticion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraMonthViewMulti1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_cancelar
        '
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(303, 299)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(90, 23)
        Me.btn_cancelar.TabIndex = 6
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'txt_entrega_cp
        '
        Me.txt_entrega_cp.AlwaysInEditMode = True
        Me.txt_entrega_cp.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_cp.Location = New System.Drawing.Point(142, 34)
        Me.txt_entrega_cp.Name = "txt_entrega_cp"
        Me.txt_entrega_cp.Size = New System.Drawing.Size(251, 19)
        Me.txt_entrega_cp.TabIndex = 2
        '
        'lbl_entrega_cp
        '
        Me.lbl_entrega_cp.AutoSize = True
        Me.lbl_entrega_cp.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_cp.Location = New System.Drawing.Point(12, 37)
        Me.lbl_entrega_cp.Name = "lbl_entrega_cp"
        Me.lbl_entrega_cp.Size = New System.Drawing.Size(74, 13)
        Me.lbl_entrega_cp.TabIndex = 0
        Me.lbl_entrega_cp.Text = "Codigo postal:"
        '
        'txt_entrega_direccion
        '
        Me.txt_entrega_direccion.AlwaysInEditMode = True
        Me.txt_entrega_direccion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_entrega_direccion.Location = New System.Drawing.Point(142, 12)
        Me.txt_entrega_direccion.Name = "txt_entrega_direccion"
        Me.txt_entrega_direccion.Size = New System.Drawing.Size(251, 19)
        Me.txt_entrega_direccion.TabIndex = 1
        '
        'lbl_entrega_direccion
        '
        Me.lbl_entrega_direccion.AutoSize = True
        Me.lbl_entrega_direccion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_entrega_direccion.Location = New System.Drawing.Point(12, 15)
        Me.lbl_entrega_direccion.Name = "lbl_entrega_direccion"
        Me.lbl_entrega_direccion.Size = New System.Drawing.Size(126, 13)
        Me.lbl_entrega_direccion.TabIndex = 0
        Me.lbl_entrega_direccion.Text = "Direcci�n de facturaci�n:"
        '
        'btn_reservar
        '
        Me.btn_reservar.Location = New System.Drawing.Point(168, 299)
        Me.btn_reservar.Name = "btn_reservar"
        Me.btn_reservar.Size = New System.Drawing.Size(129, 23)
        Me.btn_reservar.TabIndex = 5
        Me.btn_reservar.Text = "Agregar a la lista"
        Me.btn_reservar.UseVisualStyleBackColor = True
        '
        'txt_peticion
        '
        Me.txt_peticion.AlwaysInEditMode = True
        Me.txt_peticion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_peticion.Location = New System.Drawing.Point(142, 59)
        Me.txt_peticion.Name = "txt_peticion"
        Me.txt_peticion.Size = New System.Drawing.Size(251, 19)
        Me.txt_peticion.TabIndex = 3
        '
        'lbl_peticion
        '
        Me.lbl_peticion.AutoSize = True
        Me.lbl_peticion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_peticion.Location = New System.Drawing.Point(12, 62)
        Me.lbl_peticion.Name = "lbl_peticion"
        Me.lbl_peticion.Size = New System.Drawing.Size(90, 13)
        Me.lbl_peticion.TabIndex = 0
        Me.lbl_peticion.Text = "Petici�n especial:"
        '
        'UltraCalendarLook1
        '
        Me.UltraCalendarLook1.ViewStyle = Infragistics.Win.UltraWinSchedule.ViewStyle.Office2007
        '
        'UltraMonthViewMulti1
        '
        Me.UltraMonthViewMulti1.BackColor = System.Drawing.SystemColors.Window
        Me.UltraMonthViewMulti1.CalendarInfo = Me.UltraCalendarInfo1
        Me.UltraMonthViewMulti1.CalendarLook = Me.UltraCalendarLook1
        Me.UltraMonthViewMulti1.Location = New System.Drawing.Point(13, 156)
        Me.UltraMonthViewMulti1.MonthDimensions = New System.Drawing.Size(2, 1)
        Me.UltraMonthViewMulti1.Name = "UltraMonthViewMulti1"
        Me.UltraMonthViewMulti1.Size = New System.Drawing.Size(284, 124)
        Me.UltraMonthViewMulti1.TabIndex = 0
        Me.UltraMonthViewMulti1.Visible = False
        '
        'UltraCalendarInfo1
        '
        Me.UltraCalendarInfo1.DataBindingsForAppointments.BindingContextControl = Me
        Me.UltraCalendarInfo1.DataBindingsForOwners.BindingContextControl = Me
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(303, 156)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(91, 121)
        Me.ListBox1.TabIndex = 0
        Me.ListBox1.Visible = False
        '
        'lbl_cantidad
        '
        Me.lbl_cantidad.AutoSize = True
        Me.lbl_cantidad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_cantidad.Location = New System.Drawing.Point(12, 87)
        Me.lbl_cantidad.Name = "lbl_cantidad"
        Me.lbl_cantidad.Size = New System.Drawing.Size(52, 13)
        Me.lbl_cantidad.TabIndex = 0
        Me.lbl_cantidad.Text = "Cantidad:"
        '
        'cmb_cantidad
        '
        Me.cmb_cantidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_cantidad.FormattingEnabled = True
        Me.cmb_cantidad.Location = New System.Drawing.Point(142, 84)
        Me.cmb_cantidad.Name = "cmb_cantidad"
        Me.cmb_cantidad.Size = New System.Drawing.Size(47, 21)
        Me.cmb_cantidad.TabIndex = 4
        '
        'data_lbl_total
        '
        Me.data_lbl_total.AutoSize = True
        Me.data_lbl_total.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_total.Location = New System.Drawing.Point(139, 108)
        Me.data_lbl_total.Name = "data_lbl_total"
        Me.data_lbl_total.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_total.TabIndex = 22
        Me.data_lbl_total.Text = "----"
        '
        'lbl_total
        '
        Me.lbl_total.AutoSize = True
        Me.lbl_total.BackColor = System.Drawing.Color.Transparent
        Me.lbl_total.Location = New System.Drawing.Point(12, 108)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(34, 13)
        Me.lbl_total.TabIndex = 0
        Me.lbl_total.Text = "Total:"
        '
        'reservarActividad
        '
        Me.AcceptButton = Me.btn_reservar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(407, 334)
        Me.Controls.Add(Me.lbl_total)
        Me.Controls.Add(Me.data_lbl_total)
        Me.Controls.Add(Me.cmb_cantidad)
        Me.Controls.Add(Me.lbl_cantidad)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.UltraMonthViewMulti1)
        Me.Controls.Add(Me.txt_peticion)
        Me.Controls.Add(Me.lbl_peticion)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.txt_entrega_cp)
        Me.Controls.Add(Me.lbl_entrega_cp)
        Me.Controls.Add(Me.txt_entrega_direccion)
        Me.Controls.Add(Me.lbl_entrega_direccion)
        Me.Controls.Add(Me.btn_reservar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "reservarActividad"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reservar actividad"
        CType(Me.txt_entrega_cp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_entrega_direccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_peticion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraMonthViewMulti1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents txt_entrega_cp As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_cp As System.Windows.Forms.Label
    Friend WithEvents txt_entrega_direccion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_entrega_direccion As System.Windows.Forms.Label
    Friend WithEvents btn_reservar As System.Windows.Forms.Button
    Friend WithEvents txt_peticion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_peticion As System.Windows.Forms.Label
    Friend WithEvents UltraMonthViewMulti1 As Infragistics.Win.UltraWinSchedule.UltraMonthViewMulti
    Friend WithEvents UltraCalendarLook1 As Infragistics.Win.UltraWinSchedule.UltraCalendarLook
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents UltraCalendarInfo1 As Infragistics.Win.UltraWinSchedule.UltraCalendarInfo
    Friend WithEvents lbl_cantidad As System.Windows.Forms.Label
    Friend WithEvents cmb_cantidad As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_total As System.Windows.Forms.Label
    Friend WithEvents data_lbl_total As System.Windows.Forms.Label
End Class
