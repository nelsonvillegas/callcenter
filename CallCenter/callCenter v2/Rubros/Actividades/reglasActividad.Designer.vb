<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reglasActividad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reglasActividad))
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.data_lbl_actividad = New System.Windows.Forms.Label
        Me.lbl_actividad = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.data_lbl_ciudad = New System.Windows.Forms.Label
        Me.data_lbl_moneda = New System.Windows.Forms.Label
        Me.data_lbl_fecha = New System.Windows.Forms.Label
        Me.lbl_ciudad = New System.Windows.Forms.Label
        Me.lbl_moneda = New System.Windows.Forms.Label
        Me.lbl_fecha = New System.Windows.Forms.Label
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 96)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(520, 428)
        Me.WebBrowser1.TabIndex = 8
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.data_lbl_actividad)
        Me.GroupBox1.Controls.Add(Me.lbl_actividad)
        Me.GroupBox1.Controls.Add(Me.PictureBox2)
        Me.GroupBox1.Controls.Add(Me.data_lbl_ciudad)
        Me.GroupBox1.Controls.Add(Me.data_lbl_moneda)
        Me.GroupBox1.Controls.Add(Me.data_lbl_fecha)
        Me.GroupBox1.Controls.Add(Me.lbl_ciudad)
        Me.GroupBox1.Controls.Add(Me.lbl_moneda)
        Me.GroupBox1.Controls.Add(Me.lbl_fecha)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 78)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Informacion"
        '
        'data_lbl_actividad
        '
        Me.data_lbl_actividad.AutoSize = True
        Me.data_lbl_actividad.Location = New System.Drawing.Point(302, 23)
        Me.data_lbl_actividad.Name = "data_lbl_actividad"
        Me.data_lbl_actividad.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_actividad.TabIndex = 28
        Me.data_lbl_actividad.Text = "----"
        '
        'lbl_actividad
        '
        Me.lbl_actividad.AutoSize = True
        Me.lbl_actividad.Location = New System.Drawing.Point(237, 23)
        Me.lbl_actividad.Name = "lbl_actividad"
        Me.lbl_actividad.Size = New System.Drawing.Size(54, 13)
        Me.lbl_actividad.TabIndex = 27
        Me.lbl_actividad.Text = "Actividad:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(240, 39)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 26
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'data_lbl_ciudad
        '
        Me.data_lbl_ciudad.AutoSize = True
        Me.data_lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_ciudad.Location = New System.Drawing.Point(88, 36)
        Me.data_lbl_ciudad.Name = "data_lbl_ciudad"
        Me.data_lbl_ciudad.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_ciudad.TabIndex = 24
        Me.data_lbl_ciudad.Text = "----"
        '
        'data_lbl_moneda
        '
        Me.data_lbl_moneda.AutoSize = True
        Me.data_lbl_moneda.Location = New System.Drawing.Point(88, 49)
        Me.data_lbl_moneda.Name = "data_lbl_moneda"
        Me.data_lbl_moneda.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_moneda.TabIndex = 22
        Me.data_lbl_moneda.Text = "----"
        '
        'data_lbl_fecha
        '
        Me.data_lbl_fecha.AutoSize = True
        Me.data_lbl_fecha.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_fecha.Location = New System.Drawing.Point(88, 23)
        Me.data_lbl_fecha.Name = "data_lbl_fecha"
        Me.data_lbl_fecha.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_fecha.TabIndex = 23
        Me.data_lbl_fecha.Text = "----"
        '
        'lbl_ciudad
        '
        Me.lbl_ciudad.AutoSize = True
        Me.lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudad.Location = New System.Drawing.Point(23, 36)
        Me.lbl_ciudad.Name = "lbl_ciudad"
        Me.lbl_ciudad.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudad.TabIndex = 18
        Me.lbl_ciudad.Text = "Ciudad:"
        '
        'lbl_moneda
        '
        Me.lbl_moneda.AutoSize = True
        Me.lbl_moneda.Location = New System.Drawing.Point(23, 49)
        Me.lbl_moneda.Name = "lbl_moneda"
        Me.lbl_moneda.Size = New System.Drawing.Size(49, 13)
        Me.lbl_moneda.TabIndex = 19
        Me.lbl_moneda.Text = "Moneda:"
        '
        'lbl_fecha
        '
        Me.lbl_fecha.AutoSize = True
        Me.lbl_fecha.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fecha.Location = New System.Drawing.Point(23, 23)
        Me.lbl_fecha.Name = "lbl_fecha"
        Me.lbl_fecha.Size = New System.Drawing.Size(40, 13)
        Me.lbl_fecha.TabIndex = 20
        Me.lbl_fecha.Text = "Fecha:"
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_aceptar.Location = New System.Drawing.Point(457, 530)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_aceptar.TabIndex = 6
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = False
        '
        'reglasActividad
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_aceptar
        Me.ClientSize = New System.Drawing.Size(544, 565)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "reglasActividad"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reglas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents data_lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents data_lbl_moneda As System.Windows.Forms.Label
    Friend WithEvents data_lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents lbl_moneda As System.Windows.Forms.Label
    Friend WithEvents lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents data_lbl_actividad As System.Windows.Forms.Label
    Friend WithEvents lbl_actividad As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
