Public Class reglasActividad

    'Public datos_call As dataCall
    Public datosBusqueda_actividad As New dataBusqueda_actividad_test
    Public MI_Thread As Threading.Thread

    Public Sub New(ByVal db As dataBusqueda_actividad_test) 'ByVal dc As dataCall,

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
        datosBusqueda_actividad = db.Clone
    End Sub

    Private Sub reglasActividad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CapaLogicaNegocios.Activities.ActivityRules.load_data(Me)
        CapaPresentacion.Idiomas.cambiar_reglasActividad(Me)
    End Sub

End Class