Public Class actFareAvail
    Public Fecha As String
    Public EventTickets As String
    Public PercentTax As String
    Public AltCurrency As String
    Public Total As Double
    'Private EventDisponibles As Integer
    'Private RateDisponibles As Integer

    '    Public ListRate As List(Of RateType)
    Public ListDaysData As List(Of DaysData)

    Public Event TotalChanged As actFareDelegate
    Delegate Sub actFareDelegate()

    Private ListComboBoxes As List(Of ComboBox)
    Private TotalDisponibles As Integer = 0
    Private ddlHoras As ComboBox


    Public Sub New(ByVal fecha As String, ByVal EventTickets As Integer, ByVal TicketsAvailSeason As Integer, ByVal AltCurrency As String, ByVal PercentTax As String) 'ByVal dc As dataCall,
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        Me.Fecha = fecha
        Me.EventTickets = EventTickets
        Me.AltCurrency = AltCurrency
        Me.PercentTax = PercentTax
        Me.ListDaysData = New List(Of DaysData)

        If (EventTickets < TicketsAvailSeason And EventTickets > 0) Then
            Me.TotalDisponibles = EventTickets
        Else
            Me.TotalDisponibles = TicketsAvailSeason
        End If

        lblTitle.Text = DateTime.ParseExact(fecha, "yyyyMMdd", Nothing) + " (Disponibles:" + Me.TotalDisponibles.ToString() + ")"

        ddlHoras = New ComboBox()
        ddlHoras.Location = New Point(3, 20)
        ddlHoras.DropDownStyle = ComboBoxStyle.DropDownList
        ddlHoras.DataSource = New BindingSource(ListDaysData, Nothing)
        ddlHoras.DisplayMember = "Hora"
        ddlHoras.Width = 150
        AddHandler ddlHoras.SelectedIndexChanged, AddressOf ddlHoras_SelectedIndexChanged
        Me.Controls.Add(ddlHoras)

        ListComboBoxes = New List(Of ComboBox)

        ListDaysData.Add(New DaysData("Seleccione una opcion", -1))
        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc

    End Sub

    Public ReadOnly Property HoraSelected() As Boolean
        Get
            Return ddlHoras.SelectedIndex > 0
        End Get
    End Property

    Public Sub AddRate(ByVal RateID As String, ByVal RateTypeID As String, ByVal RateTypeName As String, ByVal DayReservar As String, ByVal MinTickets As String, ByVal MaxTickets As String, ByVal AltRateTypePrice As String, ByVal costoTotalFormato As String, ByVal Hora As String)

        Dim DaysData As DaysData
        Dim i As Integer
        For i = 0 To ListDaysData.Count - 1
            If ListDaysData(i).Hora = Hora Then
                DaysData = ListDaysData(i)
                Exit For
            End If
        Next

        If DaysData Is Nothing Then
            DaysData = New DaysData(Hora, ListDaysData.Count)
            ListDaysData.Add(DaysData)
            CType(ddlHoras.DataSource, BindingSource).ResetBindings(False)
        End If


        DaysData.AddRate(RateID, RateTypeID, RateTypeName, DayReservar, MinTickets, MaxTickets, AltRateTypePrice, costoTotalFormato)

        If i < 2 Then AddView(DaysData)
    End Sub

    Public Sub GetSelected(ByVal datos As List(Of RateType))
        Dim i As Integer
        If Not HoraSelected Then Return

        Dim ListRate As List(Of RateType) = ListDaysData(ddlHoras.SelectedIndex).Rates


        For i = 0 To ListRate.Count - 1
            If CInt(ListComboBoxes(i).SelectedItem) > 0 Then
                Dim rate As RateType = New RateType()

                rate.RateID = ListRate(i).RateID

                rate.RateTypeID = ListRate(i).RateTypeID
                rate.RateTypeName = ListRate(i).RateTypeName
                rate.DayReservar = ListRate(i).DayReservar
                rate.MinTickets = ListRate(i).MinTickets
                rate.MaxTickets = ListRate(i).MaxTickets
                rate.AltRateTypePrice = ListRate(i).AltRateTypePrice
                rate.costoTotalFormato = ListRate(i).costoTotalFormato
                rate.Cantidad = ListComboBoxes(i).SelectedItem
                datos.Add(rate)
            End If
        Next
    End Sub

    Public Sub AddHour(ByVal Hora As String)
        Dim DaysData As DaysData
        For Each dd As DaysData In ListDaysData
            If dd.Hora = Hora Then
                DaysData = dd
                Exit For
            End If
        Next

        If DaysData Is Nothing Then
            DaysData = New DaysData(Hora, ListDaysData.Count)
            ListDaysData.Add(DaysData)
            CType(ddlHoras.DataSource, BindingSource).ResetBindings(False)
            'ddlHoras.DataSource = New BindingSource(ListDaysData, Nothing)
            'ddlHoras.DisplayMember = "Hora"
        End If
    End Sub

    Private Sub actFareAvail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub ddlHoras_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles ddlHoras.SelectedIndexChanged
        ddl_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub ddl_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Total = 0
        If (HoraSelected()) Then
            For i = 0 To ListDaysData(ddlHoras.SelectedIndex).Rates.Count - 1
                Total = Total + CInt(ListComboBoxes(i).SelectedItem) * CType(ListDaysData(ddlHoras.SelectedIndex).Rates(i).AltRateTypePrice, Double)
            Next
        End If

        RaiseEvent TotalChanged()
    End Sub

    Private Sub AddView(ByVal DaysData As DaysData)
        'Desplegar hora
        Dim PosY As Integer
        Dim inRateIndex = ListComboBoxes.Count

        If inRateIndex = 0 Then
            PosY = 55
        Else
            PosY = ListComboBoxes(inRateIndex - 1).Top + ListComboBoxes(inRateIndex - 1).Height + 10
        End If

        '********* [LABEL] combo label **********************
        Dim lblFare As Label = New Label()
        With lblFare
            .Text = DaysData.Rates(inRateIndex).costoTotalFormato
            .Location = New Point(3, PosY)
            .AutoSize = True
        End With
        Me.Controls.Add(lblFare)

        '********* label [COMBOBOX] label **********************
        ListComboBoxes.Add(New ComboBox())
        With ListComboBoxes(inRateIndex)
            .Location = New Point(88, PosY)
            .Width = 40
            .DropDownStyle = ComboBoxStyle.DropDownList
        End With
        Me.Controls.Add(ListComboBoxes(inRateIndex))

        Dim fin As Integer = CInt(DaysData.Rates(inRateIndex).MaxTickets)
        If TotalDisponibles < fin Then fin = TotalDisponibles

        For i As Integer = CInt(DaysData.Rates(inRateIndex).MinTickets) To fin
            ListComboBoxes(inRateIndex).Items.Add(i)
        Next

        ListComboBoxes(inRateIndex).SelectedIndex = 0
        AddHandler ListComboBoxes(inRateIndex).SelectedIndexChanged, AddressOf ddl_SelectedIndexChanged

        '********* label combo [LABEL] **********************
        Dim lblRate As Label = New Label()
        With lblRate
            .Location = ListComboBoxes(inRateIndex).Location + New Point(3 + ListComboBoxes(inRateIndex).Width, 0)
            .Text = DaysData.Rates(inRateIndex).RateTypeName
            .AutoSize = True
        End With
        Me.Controls.Add(lblRate)
    End Sub

End Class

Public Class DaysData

    Private hr As String
    Private index As Integer
    Private ListRate As List(Of RateType)

    Public Sub New(ByVal Hora As String, ByVal kIndex As Integer) 'ByVal dc As dataCall,
        Me.hr = Hora
        Me.index = kIndex
        ListRate = New List(Of RateType)
    End Sub

    Public Sub AddRate(ByVal RateID As String, ByVal RateTypeID As String, ByVal RateTypeName As String, ByVal DayReservar As String, ByVal MinTickets As String, ByVal MaxTickets As String, ByVal AltRateTypePrice As String, ByVal costoTotalFormato As String)
        ListRate.Add(New RateType(RateID, RateTypeID, RateTypeName, DayReservar, MinTickets, MaxTickets, AltRateTypePrice, costoTotalFormato, -1))

        'Dim fin As Integer = CInt(MaxTickets)
        'If TotalDisponibles < fin Then fin = TotalDisponibles

        'Dim cmb = New ComboBox()
        'For i As Integer = CInt(MinTickets) To fin
        '    cmb.Items.Add(i)
        'Next
        'cmb.SelectedIndex = 0

        'ListCombo.Add(cmb)
    End Sub

    Public ReadOnly Property Rates() As List(Of RateType)
        Get
            Return ListRate
        End Get
    End Property

    Public Property Hora() As String
        Get
            Return Me.hr
        End Get
        Set(ByVal value As String)
            Me.hr = value
        End Set
    End Property

    Public Property kIndex() As Integer
        Get
            Return Me.index
        End Get
        Set(ByVal value As Integer)
            Me.index = value
        End Set
    End Property

End Class

Public Class RateType

    Public RateID As String
    Public RateTypeID As String
    Public RateTypeName As String
    Public DayReservar As String
    Public MinTickets As String
    Public MaxTickets As String
    Public AltRateTypePrice As String
    Public costoTotalFormato As String
    Public Cantidad As Integer
    Public Sub New()

    End Sub
    Public Sub New(ByVal RateID As String, ByVal RateTypeID As String, ByVal RateTypeName As String, ByVal DayReservar As String, ByVal MinTickets As String, ByVal MaxTickets As String, ByVal AltRateTypePrice As String, ByVal costoTotalFormato As String, ByVal Cantidad As Integer)

        Me.RateID = RateID
        Me.RateTypeID = RateTypeID
        Me.RateTypeName = RateTypeName

        Me.DayReservar = DayReservar

        Me.MinTickets = MinTickets
        Me.MaxTickets = MaxTickets
        Me.AltRateTypePrice = AltRateTypePrice

        Me.costoTotalFormato = costoTotalFormato
        Me.Cantidad = Cantidad
    End Sub
End Class
