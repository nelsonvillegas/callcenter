<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class busquedaActividad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(busquedaActividad))
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txt_CodigoPromocion = New System.Windows.Forms.TextBox
        Me.txt_CodigoAcceso = New System.Windows.Forms.TextBox
        Me.lbl_CodigoAcceso = New System.Windows.Forms.Label
        Me.lbl_CodigoPromocion = New System.Windows.Forms.Label
        Me.data_lbl_act = New System.Windows.Forms.Label
        Me.dtp_fechaHasta = New System.Windows.Forms.DateTimePicker
        Me.lbl_fechaHasta = New System.Windows.Forms.Label
        Me.cs_ciudad = New callCenter.ComboSearch
        Me.lbl_ciudad = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.dtp_fechaDesde = New System.Windows.Forms.DateTimePicker
        Me.lbl_moneda = New System.Windows.Forms.Label
        Me.lbl_fechaDesde = New System.Windows.Forms.Label
        Me.uce_moneda = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.btn_buscar = New System.Windows.Forms.Button
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txt_CodigoPromocion)
        Me.Panel1.Controls.Add(Me.txt_CodigoAcceso)
        Me.Panel1.Controls.Add(Me.lbl_CodigoAcceso)
        Me.Panel1.Controls.Add(Me.lbl_CodigoPromocion)
        Me.Panel1.Controls.Add(Me.data_lbl_act)
        Me.Panel1.Controls.Add(Me.dtp_fechaHasta)
        Me.Panel1.Controls.Add(Me.lbl_fechaHasta)
        Me.Panel1.Controls.Add(Me.cs_ciudad)
        Me.Panel1.Controls.Add(Me.lbl_ciudad)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.dtp_fechaDesde)
        Me.Panel1.Controls.Add(Me.lbl_moneda)
        Me.Panel1.Controls.Add(Me.lbl_fechaDesde)
        Me.Panel1.Controls.Add(Me.uce_moneda)
        Me.Panel1.Controls.Add(Me.btn_buscar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1086, 110)
        Me.Panel1.TabIndex = 3
        '
        'txt_CodigoPromocion
        '
        Me.txt_CodigoPromocion.Location = New System.Drawing.Point(450, 39)
        Me.txt_CodigoPromocion.Name = "txt_CodigoPromocion"
        Me.txt_CodigoPromocion.Size = New System.Drawing.Size(150, 20)
        Me.txt_CodigoPromocion.TabIndex = 4
        '
        'txt_CodigoAcceso
        '
        Me.txt_CodigoAcceso.Location = New System.Drawing.Point(450, 14)
        Me.txt_CodigoAcceso.Name = "txt_CodigoAcceso"
        Me.txt_CodigoAcceso.Size = New System.Drawing.Size(150, 20)
        Me.txt_CodigoAcceso.TabIndex = 3
        '
        'lbl_CodigoAcceso
        '
        Me.lbl_CodigoAcceso.AutoSize = True
        Me.lbl_CodigoAcceso.BackColor = System.Drawing.Color.Transparent
        Me.lbl_CodigoAcceso.Location = New System.Drawing.Point(272, 13)
        Me.lbl_CodigoAcceso.Name = "lbl_CodigoAcceso"
        Me.lbl_CodigoAcceso.Size = New System.Drawing.Size(102, 13)
        Me.lbl_CodigoAcceso.TabIndex = 16
        Me.lbl_CodigoAcceso.Text = "Coodigo de acceso:"
        '
        'lbl_CodigoPromocion
        '
        Me.lbl_CodigoPromocion.AutoSize = True
        Me.lbl_CodigoPromocion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_CodigoPromocion.Location = New System.Drawing.Point(272, 39)
        Me.lbl_CodigoPromocion.Name = "lbl_CodigoPromocion"
        Me.lbl_CodigoPromocion.Size = New System.Drawing.Size(110, 13)
        Me.lbl_CodigoPromocion.TabIndex = 14
        Me.lbl_CodigoPromocion.Text = "Codigo de promocion:"
        '
        'data_lbl_act
        '
        Me.data_lbl_act.AutoSize = True
        Me.data_lbl_act.Location = New System.Drawing.Point(721, 40)
        Me.data_lbl_act.Name = "data_lbl_act"
        Me.data_lbl_act.Size = New System.Drawing.Size(22, 13)
        Me.data_lbl_act.TabIndex = 13
        Me.data_lbl_act.Text = "-----"
        '
        'dtp_fechaHasta
        '
        Me.dtp_fechaHasta.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_fechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_fechaHasta.Location = New System.Drawing.Point(77, 37)
        Me.dtp_fechaHasta.Name = "dtp_fechaHasta"
        Me.dtp_fechaHasta.Size = New System.Drawing.Size(109, 20)
        Me.dtp_fechaHasta.TabIndex = 2
        '
        'lbl_fechaHasta
        '
        Me.lbl_fechaHasta.AutoSize = True
        Me.lbl_fechaHasta.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fechaHasta.Location = New System.Drawing.Point(15, 39)
        Me.lbl_fechaHasta.Name = "lbl_fechaHasta"
        Me.lbl_fechaHasta.Size = New System.Drawing.Size(38, 13)
        Me.lbl_fechaHasta.TabIndex = 12
        Me.lbl_fechaHasta.Text = "Hasta:"
        '
        'cs_ciudad
        '
        Me.cs_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.cs_ciudad.Location = New System.Drawing.Point(450, 63)
        Me.cs_ciudad.Name = "cs_ciudad"
        Me.cs_ciudad.Size = New System.Drawing.Size(150, 21)
        Me.cs_ciudad.TabIndex = 5
        '
        'lbl_ciudad
        '
        Me.lbl_ciudad.AutoSize = True
        Me.lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudad.Location = New System.Drawing.Point(272, 63)
        Me.lbl_ciudad.Name = "lbl_ciudad"
        Me.lbl_ciudad.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudad.TabIndex = 0
        Me.lbl_ciudad.Text = "Ciudad:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(724, 69)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 11
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'dtp_fechaDesde
        '
        Me.dtp_fechaDesde.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_fechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_fechaDesde.Location = New System.Drawing.Point(77, 11)
        Me.dtp_fechaDesde.Name = "dtp_fechaDesde"
        Me.dtp_fechaDesde.Size = New System.Drawing.Size(109, 20)
        Me.dtp_fechaDesde.TabIndex = 1
        '
        'lbl_moneda
        '
        Me.lbl_moneda.AutoSize = True
        Me.lbl_moneda.Location = New System.Drawing.Point(646, 14)
        Me.lbl_moneda.Name = "lbl_moneda"
        Me.lbl_moneda.Size = New System.Drawing.Size(49, 13)
        Me.lbl_moneda.TabIndex = 0
        Me.lbl_moneda.Text = "Moneda:"
        '
        'lbl_fechaDesde
        '
        Me.lbl_fechaDesde.AutoSize = True
        Me.lbl_fechaDesde.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fechaDesde.Location = New System.Drawing.Point(15, 13)
        Me.lbl_fechaDesde.Name = "lbl_fechaDesde"
        Me.lbl_fechaDesde.Size = New System.Drawing.Size(41, 13)
        Me.lbl_fechaDesde.TabIndex = 0
        Me.lbl_fechaDesde.Text = "Desde:"
        '
        'uce_moneda
        '
        Me.uce_moneda.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_moneda.Location = New System.Drawing.Point(724, 10)
        Me.uce_moneda.Name = "uce_moneda"
        Me.uce_moneda.Size = New System.Drawing.Size(150, 21)
        Me.uce_moneda.TabIndex = 6
        '
        'btn_buscar
        '
        Me.btn_buscar.Image = CType(resources.GetObject("btn_buscar.Image"), System.Drawing.Image)
        Me.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_buscar.Location = New System.Drawing.Point(926, 12)
        Me.btn_buscar.Name = "btn_buscar"
        Me.btn_buscar.Size = New System.Drawing.Size(75, 56)
        Me.btn_buscar.TabIndex = 7
        Me.btn_buscar.Text = "Buscar"
        Me.btn_buscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_buscar.UseVisualStyleBackColor = True
        '
        'UltraGrid1
        '
        Me.UltraGrid1.ContextMenuStrip = Me.ContextMenuStrip1
        Appearance30.BackColor = System.Drawing.Color.White
        Appearance30.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance30
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance31.ImageBackground = CType(resources.GetObject("Appearance31.ImageBackground"), System.Drawing.Image)
        Appearance31.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance31.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance31.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance31
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance28.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance28
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance29.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance29.FontData.Name = "Trebuchet MS"
        Appearance29.FontData.SizeInPoints = 9.0!
        Appearance29.ForeColor = System.Drawing.Color.White
        Appearance29.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance29
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance32.FontData.BoldAsString = "True"
        Appearance32.FontData.Name = "Trebuchet MS"
        Appearance32.FontData.SizeInPoints = 10.0!
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance32
        Appearance33.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance33
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance34.BackColor2 = System.Drawing.SystemColors.Control
        Appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance34.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance34
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance13.BackColor = System.Drawing.SystemColors.Window
        Appearance13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance13
        Appearance14.BackColor = System.Drawing.SystemColors.Highlight
        Appearance14.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance14
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance15.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance15
        Appearance16.BorderColor = System.Drawing.Color.Transparent
        Appearance16.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance16
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance17
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance21.BackColor = System.Drawing.SystemColors.Control
        Appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance21.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance21
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance22.FontData.BoldAsString = "True"
        Appearance22.FontData.Name = "Trebuchet MS"
        Appearance22.FontData.SizeInPoints = 10.0!
        Appearance22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance22.ImageBackground = CType(resources.GetObject("Appearance22.ImageBackground"), System.Drawing.Image)
        Appearance22.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance22.TextHAlignAsString = "Left"
        Appearance22.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance22
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance1
        Appearance23.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance24
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance25.BorderColor = System.Drawing.Color.Transparent
        Appearance25.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance25
        Appearance26.BorderColor = System.Drawing.Color.Transparent
        Appearance26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance26.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance26
        Appearance27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance27
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance35.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance37.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance38
        Appearance39.ImageBackground = CType(resources.GetObject("Appearance39.ImageBackground"), System.Drawing.Image)
        Appearance39.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance39.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance39
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 110)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(1086, 438)
        Me.UltraGrid1.TabIndex = 4
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'busquedaActividad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1086, 548)
        Me.Controls.Add(Me.UltraGrid1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "busquedaActividad"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Resultados de la b�squeda: Actividades"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_moneda As System.Windows.Forms.Label
    Friend WithEvents cs_ciudad As callCenter.ComboSearch
    Friend WithEvents lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents dtp_fechaDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_fechaDesde As System.Windows.Forms.Label
    Friend WithEvents uce_moneda As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents btn_buscar As System.Windows.Forms.Button
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents dtp_fechaHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_fechaHasta As System.Windows.Forms.Label
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents data_lbl_act As System.Windows.Forms.Label
    Friend WithEvents txt_CodigoPromocion As System.Windows.Forms.TextBox
    Friend WithEvents txt_CodigoAcceso As System.Windows.Forms.TextBox
    Friend WithEvents lbl_CodigoAcceso As System.Windows.Forms.Label
    Friend WithEvents lbl_CodigoPromocion As System.Windows.Forms.Label
End Class
