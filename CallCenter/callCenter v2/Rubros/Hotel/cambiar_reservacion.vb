﻿
Public Class cambiar_reservacion
    Public datosBusqueda_hotel As dataBusqueda_hotel_test
    Public dsHotelChanges As New reqHotelModifyNoRestrictions
    Public numRooms As Short
    Dim dsTypeRoom As DataSet


    Private Sub txt_edit_tarifa_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs)
        only_numbers(e)
    End Sub


    Private Sub only_numbers(e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            e.Handled = Not (Char.IsDigit(e.KeyChar) Or e.KeyChar = ".")
        End If
    End Sub

    Private Sub txt_edit_personaExtra_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs)
        only_numbers(e)
    End Sub

    Private Sub txt_edit_total_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_edit_total.KeyPress
        only_numbers(e)
    End Sub


    Private Sub cambiar_reservacion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        'carga textos de idiomas
        loadResources()

        'cargar datos
        dtp_in.Value = datosBusqueda_hotel.llegada
        dtp_out.Value = datosBusqueda_hotel.salida
        txt_edit_peticion.Text = datosBusqueda_hotel.cust_Preferences
        txt_edit_comentario.Text = datosBusqueda_hotel.Comments
        'txt_edit_tarifa.Text = CStr(CDbl(datosBusqueda_hotel.AltTotalStay) / (CDbl(datosBusqueda_hotel.noches)))
        txt_edit_total.Text = CStr(CDbl(datosBusqueda_hotel.AltTotalStay) + (CDbl(datosBusqueda_hotel.AltTax)))
        txt_edit_totalNR.Text = datosBusqueda_hotel.TotalNR



        lbl_info_currency_totalNR.Text = datosBusqueda_hotel.AltCurrency
        lbl_info_currency_total.Text = datosBusqueda_hotel.AltCurrency

        If datosBusqueda_hotel.isNetRate Then
            lbl_info_totalNR.Visible = True
            txt_edit_totalNR.Visible = True
            lbl_info_currency_totalNR.Visible = True
            chk_NR.Checked = True
        End If

        'crar filas del datagrid y cargar datos

        If datosBusqueda_hotel.Status IsNot Nothing AndAlso datosBusqueda_hotel.Status IsNot String.Empty Then
            cmb_status.SelectedIndex = Integer.Parse(datosBusqueda_hotel.Status) - 1
        Else
            cmb_status.SelectedIndex = enumReservationStatus.InProcess + 1
        End If

        'Init combos
        cmb_mount_payment.SelectedIndex = 0
        If datosBusqueda_hotel.PaymentSource <> Nothing AndAlso datosBusqueda_hotel.PaymentSource > 0 Then
            cmb_source.SelectedIndex = datosBusqueda_hotel.PaymentSource - 1
        Else
            cmb_source.SelectedIndex = 4
        End If

        'Ver si no truena así

        'rdt_tarjeta.Checked = True
        'If datosBusqueda_hotel.isPayPalPayment = True Then
        '   rbt_paypal.Checked = True
        'End If
        'If datosBusqueda_hotel.isPayUPaymnet = True Then
        '   rbt_pay_u.Checked = True
        'End If
        'If datosBusqueda_hotel.isAmericanExpress = True Then
        '   rbt_american_express.Checked = True
        'End If


        Select Case datosBusqueda_hotel.DepositType
            Case enumDepositType.CreditCard
                rdt_pay_in_hotel.Checked = True
            Case enumDepositType.BankDeposit
                rbt_deposito.Checked = True
            Case enumDepositType.americanExpress
                rbt_american_express.Checked = True
            Case enumDepositType.paypal
                rbt_paypal.Checked = True
            Case enumDepositType.payU
                rbt_pay_u.Checked = True
            Case enumDepositType.conekta
                rbt_conekta.Checked = True
            Case Else
                rdt_tarjeta.Checked = True
        End Select

        If datosBusqueda_hotel.monthBanorte <> Nothing Then
            Select Case datosBusqueda_hotel.monthBanorte
                Case "3"
                    cmb_mount_payment.SelectedIndex = 1
                Case "6"
                    cmb_mount_payment.SelectedIndex = 2
                Case "9"
                    cmb_mount_payment.SelectedIndex = 3
                Case "12"
                    cmb_mount_payment.SelectedIndex = 4
            End Select


        End If

        dtp_in.Format = DateTimePickerFormat.Custom
        dtp_in.CustomFormat = "dd/MMM/yy"

        dtp_out.Format = DateTimePickerFormat.Custom
        dtp_out.CustomFormat = "dd/MMM/yy"

        If datosBusqueda_hotel.NoAutorizacion <> Nothing Then
            AuthNum.Text = datosBusqueda_hotel.NoAutorizacion.Split(" ")(0)
        End If


        'Deposit info items init

        If datosBusqueda_hotel.DepositAmountCCT <> Nothing Then
            txt_DepositAmount.Text = datosBusqueda_hotel.DepositAmountCCT
        Else
            txt_DepositAmount.Text = "0"
        End If
        If datosBusqueda_hotel.DepositInfoCCT <> Nothing Then
            If datosBusqueda_hotel.Idioma <> Nothing Then
                If datosBusqueda_hotel.Idioma = 1 Then
                    txt_deposit_esp.Text = datosBusqueda_hotel.DepositInfoCCT
                Else
                    txt_deposit_eng.Text = datosBusqueda_hotel.DepositInfoCCT
                End If
            End If
        Else
            txt_deposit_esp.Text = ""
            txt_deposit_eng.Text = ""
        End If


        rbt_target_UV.Checked = True

        If datosBusqueda_hotel.DepositTarget <> Nothing Then
            If datosBusqueda_hotel.DepositTarget = "HTL" Then
                rbt_target_HTL.Checked = True
            End If
        End If

        If datosBusqueda_hotel.DepositReference <> Nothing Then
            txt_reference.Text = datosBusqueda_hotel.DepositReference
        End If






        'agregar tipo de habitacion
        Try
            dsTypeRoom = obtenGenerales_hoteles.obten_general_HOC(datosBusqueda_hotel)


            For countTypeRoom As Byte = 0 To (dsTypeRoom.Tables("RoomType").Rows.Count - 1)
                cmb_tipo_habitacion.Items.Add(dsTypeRoom.Tables("RoomType").Rows(countTypeRoom).Item("RoomName"))
                If datosBusqueda_hotel.RateCode.Contains(dsTypeRoom.Tables("RoomType").Rows(countTypeRoom).Item("RoomCode")) Then
                    cmb_tipo_habitacion.SelectedIndex = countTypeRoom
                End If
            Next
        Catch ex As Exception
            Dim menssage As String = ex.Message
        End Try


    End Sub

    Private Sub btn_cancelar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub
    Private Sub buildDataGrid()
        Dim colCount As Integer = 7
        Dim rowCount As Integer = 0
        'iniciar el datagrid
        If Not datosBusqueda_hotel.isNetRate Then
            colCount = colCount - 1
        End If

        dtgrid_ratesdetails.ColumnCount = colCount
        dtgrid_ratesdetails.ColumnHeadersVisible = True

        'cambiar estilo de las columnas.
        Dim columnHeaderStyle As New DataGridViewCellStyle()
        columnHeaderStyle.BackColor = Color.Black
        columnHeaderStyle.Font = New Font("Helvetica", 8, FontStyle.Regular)
        dtgrid_ratesdetails.ColumnHeadersDefaultCellStyle = columnHeaderStyle

        ' cambiar nombres de las columnas.
        dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_0097_tarifa")
        rowCount += 1
        If datosBusqueda_hotel.isNetRate Then
            dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_547")
            rowCount += 1
        End If
        dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_0011_adultos")
        rowCount += 1
        dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit")
        rowCount += 1
        dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_548")
        rowCount += 1
        dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_549")
        rowCount += 1
        dtgrid_ratesdetails.Columns(rowCount).Name = CapaPresentacion.Idiomas.get_str("str_0013_edadesMenores")
        rowCount += 1

        rowCount = 0
        numRooms = datosBusqueda_hotel.habitaciones_.ListaHabitaciones.Count


        dtgrid_ratesdetails.RowCount = numRooms

        For counterRooms As Byte = 0 To (numRooms - 1)              'Valores default si hay un error
            dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = IIf(datosBusqueda_hotel.PrecioTarifa.Length - 1 < counterRooms, "0", CStr(CDbl(datosBusqueda_hotel.PrecioTarifa(counterRooms))))
            rowCount += 1
            If datosBusqueda_hotel.isNetRate Then
                dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = IIf(datosBusqueda_hotel.precioNR.Length - 1 < counterRooms, "0", CStr(CDbl(datosBusqueda_hotel.precioNR(counterRooms))))
                rowCount += 1
            End If
            dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = datosBusqueda_hotel.habitaciones_.ListaHabitaciones(counterRooms).NumAdultos
            rowCount += 1
            dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = datosBusqueda_hotel.habitaciones_.ListaHabitaciones(counterRooms).NumNinos
            rowCount += 1
            dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = datosBusqueda_hotel.adultsExtra(counterRooms)
            rowCount += 1
            dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = datosBusqueda_hotel.childrenExtra(counterRooms)
            rowCount += 1
            dtgrid_ratesdetails.Rows(counterRooms).Cells(rowCount).Value = datosBusqueda_hotel.habitaciones_.ListaHabitaciones(counterRooms).EdadesNinosSTR
        Next


    End Sub

    Public Sub paymentCheck(datasetReservation As reqHotelModifyNoRestrictions.ReservationRow)
        datasetReservation.PayPal = "n"
        datasetReservation.PayU = "n"
        datasetReservation.CreditCard = "n"
        datasetReservation.BanorteMonths = "n"
        datasetReservation.BankDeposit = "n"
        datasetReservation.AmericanExpress = "n"
        datasetReservation.Conekta = "n"

        If rbt_paypal.Checked Then
            datasetReservation.PayPal = "y"
        End If
        If rbt_pay_u.Checked Then
            datasetReservation.PayU = "y"
        End If
        If rbt_deposito.Checked Then
            datasetReservation.BankDeposit = "y"
        End If
        If rbt_american_express.Checked Then
            datasetReservation.AmericanExpress = "y"
        End If
        If rbt_conekta.Checked Then
            datasetReservation.Conekta = "y"
        End If

        'Set Payment Source and Months
        '1-Banamex, 2-Santander, 3-DineroMail, 4-Bancomer, 5-Banorte

        If rdt_tarjeta.Checked = True Then
            datasetReservation.CreditCard = "y"
            datasetReservation.PaymentSource = cmb_source.SelectedIndex + 1
            datasetReservation.BanorteMonths = cmb_mount_payment.Text.Substring(0, 2)
            Try
                Integer.Parse(datasetReservation.BanorteMonths.Trim())
            Catch ex As Exception
                datasetReservation.BanorteMonths = "n"
            End Try
        End If


    End Sub

    Private Sub btn_modificar_Click(sender As System.Object, e As System.EventArgs) Handles btn_modificar.Click
        Try

            If validate() = True Then


                Dim frm As New datosCliente
                frm.ModificandoReservaHotel = True
                misForms.form1.datoscliente = New callCenter.dataBusqueda_paquete()

                Dim dsReservationRow As reqHotelModifyNoRestrictions.ReservationRow = dsHotelChanges.Reservation.NewRow

                dsReservationRow.ConfirmNumber = datosBusqueda_hotel.ConfirmNumber
                dsReservationRow.CheckInDate = dtp_in.Value
                dsReservationRow.CheckOutDate = dtp_out.Value
                dsReservationRow.PropertyNumber = datosBusqueda_hotel.PropertyNumber
                dsReservationRow.Total = txt_edit_total.Text
                dsReservationRow.TotalNR = txt_edit_totalNR.Text
                dsReservationRow.RateCode = datosBusqueda_hotel.RateCode
                dsReservationRow.ExtRatePrice = "0"
                dsReservationRow.ExtRatePriceNR = "0"
                dsReservationRow.CommentByVendor = txt_edit_comentario.Text



                dsReservationRow.DepositInfoEsp = txt_deposit_esp.Text
                dsReservationRow.DepositInfoEng = txt_deposit_eng.Text
                dsReservationRow.DepositAmount = txt_DepositAmount.Text
                dsReservationRow.DepositTarget = IIf(rbt_target_UV.Checked, "UV", "HTL")
                dsReservationRow.CurrencyCode = lbl_info_currency_total.Text
                dsReservationRow.DepositReference = txt_reference.Text


                dsReservationRow.Status = (cmb_status.SelectedIndex + 1).ToString()

                dsReservationRow.NoAutorizacion = AuthNum.Text
                For countTypeRoom As Byte = 0 To (dsTypeRoom.Tables("RoomType").Rows.Count - 1)
                    If cmb_tipo_habitacion.Text = dsTypeRoom.Tables("RoomType").Rows(countTypeRoom).Item("RoomName") Then
                        dsReservationRow.CodeRoom = dsTypeRoom.Tables("RoomType").Rows(countTypeRoom).Item("RoomCode")
                    End If
                Next
                paymentCheck(dsReservationRow)
                dsHotelChanges.Reservation.Rows.Add(dsReservationRow)
                If rdt_pay_in_hotel.Checked = True Then
                    frm.creditCardFrom = True
                Else
                    frm.creditCardFrom = False
                    frm.Height = 500
                End If

                If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    ' Close()
                Else
                    Dim dsClientRow As reqHotelModifyNoRestrictions.ClientRow = dsHotelChanges.Client.NewRow
                    'dsHotelChanges.Client.Rows.Add(dsClientRow)

                    dsClientRow.Name = misForms.newCall.datos_call.reservacion_paquete.cli_nombre
                    dsClientRow.LastName = misForms.newCall.datos_call.reservacion_paquete.cli_apellido
                    dsClientRow.Address = misForms.newCall.datos_call.reservacion_paquete.cli_direccion
                    dsClientRow.ZipCode = misForms.newCall.datos_call.reservacion_paquete.cli_cp
                    dsClientRow.Country = misForms.newCall.datos_call.reservacion_paquete.cli_pais
                    dsClientRow.State = misForms.newCall.datos_call.reservacion_paquete.cli_estado
                    dsClientRow.City = misForms.newCall.datos_call.reservacion_paquete.cli_ciudad
                    dsClientRow.Phone = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode + "+" + misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber
                    If misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber <> "" Then
                        dsClientRow.ExtraPhone = misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode + "+" + misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber
                    Else
                        dsClientRow.ExtraPhone = ""
                    End If
                    dsClientRow.Email = misForms.newCall.datos_call.reservacion_paquete.cli_email
                    dsClientRow.Code = misForms.newCall.datos_call.reservacion_paquete.codigo_referencia
                    'tarjeta
                    If rdt_pay_in_hotel.Checked Then

                        dsClientRow.NameCC = misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ + misForms.newCall.datos_call.reservacion_paquete.cc_apellido
                        dsClientRow.NumberCC = misForms.newCall.datos_call.reservacion_paquete.cc_numero
                        dsClientRow.DigitCC = misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad
                        dsClientRow.TipoCC = DirectCast([Enum].Parse(GetType(enumCreditCardCode), misForms.newCall.datos_call.reservacion_paquete.cc_tipo), enumCreditCardCode)
                        dsClientRow.ExpireMonthCC = misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira.Substring(0, 2)
                        dsClientRow.ExpireYearCC = misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira.Substring(2, 4)
                    End If



                    dsHotelChanges.Client.Rows.Add(dsClientRow)

                    Dim dsRooms(numRooms) As reqHotelModifyNoRestrictions.RoomsRow

                    For counterRooms As Byte = 0 To (numRooms - 1)
                        dsRooms(counterRooms) = dsHotelChanges.Rooms.NewRow
                        dsHotelChanges.Rooms.Rows.Add(dsRooms(counterRooms))
                    Next


                    Dim dsRoom(numRooms) As reqHotelModifyNoRestrictions.RoomRow

                    For counterRoom As Byte = 0 To (numRooms - 1)
                        dsRoom(counterRoom) = dsHotelChanges.Room.NewRow
                        dsRoom(counterRoom).RatePrice = CStr(CDbl(dtgrid_ratesdetails.Rows(counterRoom).Cells(0).Value()))
                        dsRoom(counterRoom).RatePriceNR = CStr(CDbl(dtgrid_ratesdetails.Rows(counterRoom).Cells(1).Value()))
                        dsRoom(counterRoom).Adults = CStr(CDbl(dtgrid_ratesdetails.Rows(counterRoom).Cells(2).Value()))
                        dsRoom(counterRoom).Children = CStr(CDbl(dtgrid_ratesdetails.Rows(counterRoom).Cells(3).Value()))
                        dsRoom(counterRoom).AdultsExtra = CStr(CDbl(dtgrid_ratesdetails.Rows(counterRoom).Cells(4).Value()))
                        dsRoom(counterRoom).ChildrenExtra = CStr(CDbl(dtgrid_ratesdetails.Rows(counterRoom).Cells(5).Value()))
                        dsRoom(counterRoom).Preferences = txt_edit_peticion.Text
                        dsRoom(counterRoom).ChildrenAges = dtgrid_ratesdetails.Rows(counterRoom).Cells(6).Value()
                        dsRoom(counterRoom).Rooms_Id = dsRooms(counterRoom).Rooms_Id
                        dsRoom(counterRoom).SetParentRow(dsRooms(counterRoom))
                        dsHotelChanges.Room.Rows.Add(dsRoom(counterRoom))
                    Next

                    If CapaLogicaNegocios.Hotels.HotelBook.make_changes(Me) Then
                        misForms.form1.datoscliente = Nothing
                        misForms.reservaciones.actualize()
                        MsgBox(CapaPresentacion.Idiomas.get_str("str_421_Modification"), MsgBoxStyle.Information)
                    End If
                End If
                Close()
            End If
        Catch ex As Exception

            MsgBox("Error: " & ex.Message, MsgBoxStyle.Information)

        End Try
    End Sub

    Function validate() As Boolean

        ' Validaciones generales
        If txt_edit_total.Text = "" Or txt_edit_total.Text = Nothing Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_556"), MsgBoxStyle.Information)
            Return False
        End If

        'Check in mayor a hoy
        If dtp_in.Value.ToString("yyyyMMdd") < DateTime.Now.ToString("yyyyMMdd") Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_558"), MsgBoxStyle.Information)
            Return False
        End If

        'Check out mayor o igual a check in
        If dtp_out.Value.ToString("yyyyMMdd") < dtp_in.Value.ToString("yyyyMMdd") Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_559"), MsgBoxStyle.Information)
            Return False
        End If

        'Validaciones Tarifa Neta
        If chk_NR.Checked Then
            If txt_edit_totalNR.Text = "" Or txt_edit_totalNR.Text = Nothing Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_557"), MsgBoxStyle.Information)
                Return False
            End If
        End If

        'Validaciones Deposito Bancario
        If rbt_deposito.Checked Then
            If CDec(txt_DepositAmount.Text) < 0 Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_428_amountinforeq"))
                Return False
            End If
            If txt_reference.Text = "" Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_554"))
                Return False
            End If
        End If
        'Validaciones Pago en Linea
        If Not rbt_deposito.Checked AndAlso Not rdt_pay_in_hotel.Checked Then
            If AuthNum.Text = "" Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_554"))
                Return False
            End If
        End If




        Return True

    End Function

    Private Sub txt_edit_totalNR_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txt_edit_totalNR.KeyPress
        only_numbers(e)
    End Sub

    Private Sub btn_add_row_Click(sender As System.Object, e As System.EventArgs) Handles btn_add_row.Click
        'dtgrid_ratesdetails.Columns(0).Name = "Tarifa"
        'dtgrid_ratesdetails.Columns(1).Name = "Tarifa Neta"
        'dtgrid_ratesdetails.Columns(2).Name = "Adultos"
        'dtgrid_ratesdetails.Columns(3).Name = "Niños"
        'dtgrid_ratesdetails.Columns(4).Name = "Adultos Extra"
        'dtgrid_ratesdetails.Columns(5).Name = "Niños Extra"
        'dtgrid_ratesdetails.Columns(6).Name = "Edades Niños"
        dtgrid_ratesdetails.Rows.Add(
            datosBusqueda_hotel.PrecioTarifa(0),
            datosBusqueda_hotel.precioNR(0), "1", "0", "0", "0", "")
        numRooms += 1

    End Sub

    Private Sub btn_remove_row_Click(sender As System.Object, e As System.EventArgs) Handles btn_remove_row.Click
        Dim RowIndex As Byte
        Dim ColIndex As Byte
        Dim gridAuxRow As DataGridViewRow
        If numRooms > 1 Then
            RowIndex = dtgrid_ratesdetails.CurrentCellAddress.Y
            ColIndex = dtgrid_ratesdetails.CurrentCellAddress.X
            gridAuxRow = dtgrid_ratesdetails.Rows(RowIndex)
            If RowIndex = 0 Then
                removeFirstRow(dtgrid_ratesdetails.Rows(RowIndex))
            Else
                removeRow(dtgrid_ratesdetails.Rows(RowIndex))
            End If
            dtgrid_ratesdetails.Rows.Remove(dtgrid_ratesdetails.Rows(RowIndex))
            numRooms -= 1
        Else
            MsgBox("No puede Dejar La Reserva Sin Habitacion", MsgBoxStyle.Information)
        End If

    End Sub

    Public Sub removeFirstRow(ByVal row As DataGridViewRow)
        dtgrid_ratesdetails.Rows(1).Cells(2).Value = CStr(CByte(dtgrid_ratesdetails.Rows(1).Cells(2).Value) + CByte(row.Cells(2).Value))
        dtgrid_ratesdetails.Rows(1).Cells(3).Value = CStr(CByte(dtgrid_ratesdetails.Rows(1).Cells(3).Value) + CByte(row.Cells(3).Value))
        dtgrid_ratesdetails.Rows(1).Cells(4).Value = CStr(CByte(dtgrid_ratesdetails.Rows(1).Cells(4).Value) + CByte(row.Cells(4).Value))
        dtgrid_ratesdetails.Rows(1).Cells(5).Value = CStr(CByte(dtgrid_ratesdetails.Rows(1).Cells(5).Value) + CByte(row.Cells(5).Value))
    End Sub
    Public Sub removeRow(ByVal row As DataGridViewRow)
        dtgrid_ratesdetails.Rows(0).Cells(2).Value = CStr(CByte(dtgrid_ratesdetails.Rows(0).Cells(2).Value) + CByte(row.Cells(2).Value))
        dtgrid_ratesdetails.Rows(0).Cells(3).Value = CStr(CByte(dtgrid_ratesdetails.Rows(0).Cells(3).Value) + CByte(row.Cells(3).Value))
        dtgrid_ratesdetails.Rows(0).Cells(4).Value = CStr(CByte(dtgrid_ratesdetails.Rows(0).Cells(4).Value) + CByte(row.Cells(4).Value))
        dtgrid_ratesdetails.Rows(0).Cells(5).Value = CStr(CByte(dtgrid_ratesdetails.Rows(0).Cells(5).Value) + CByte(row.Cells(5).Value))
    End Sub

    Private Sub rdt_pay_in_hotel_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdt_tarjeta.CheckedChanged, rdt_pay_in_hotel.CheckedChanged, rbt_paypal.CheckedChanged, rbt_pay_u.CheckedChanged, rbt_deposito.CheckedChanged, rbt_american_express.CheckedChanged, rbt_conekta.CheckedChanged
        cmb_mount_payment.Visible = False
        lbl_months.Visible = False

        cmb_source.Visible = False
        lbl_source.Visible = False

        AuthNum.Visible = False
        lbl_noAuth.Visible = False

        lbl_deposit.Visible = False
        lbl_depositAmount.Visible = False
        lbl_depositCurrency.Visible = False
        lbl_depositTarget.Visible = False
        lbl_reference.Visible = False

        txt_DepositAmount.Visible = False
        tc_depositInfo.Visible = False
        rbt_target_HTL.Visible = False
        rbt_target_UV.Visible = False

        txt_deposit_eng.Visible = False
        txt_deposit_esp.Visible = False
        txt_reference.Visible = False

        If Not (rdt_pay_in_hotel.Checked OrElse rbt_deposito.Checked) Then
            AuthNum.Visible = True
            lbl_noAuth.Visible = True

        Else


        End If
        If rdt_tarjeta.Checked Then
            cmb_source.Visible = True
            lbl_source.Visible = True

            cmb_mount_payment.Visible = True
            lbl_months.Visible = True
        End If

        If rbt_deposito.Checked Then
            lbl_deposit.Visible = True
            lbl_depositAmount.Visible = True
            lbl_depositCurrency.Visible = True
            txt_DepositAmount.Visible = True
            tc_depositInfo.Visible = True
            lbl_depositTarget.Visible = True
            lbl_reference.Visible = True

            rbt_target_HTL.Visible = True
            rbt_target_UV.Visible = True

            txt_deposit_eng.Visible = True
            txt_deposit_esp.Visible = True
            txt_reference.Visible = True

        End If




    End Sub

    Private Sub chk_NR_CheckedChanged(sender As Object, e As EventArgs) Handles chk_NR.CheckedChanged
        If chk_NR.Checked Then
            lbl_info_totalNR.Visible = True
            txt_edit_totalNR.Visible = True
            lbl_info_currency_totalNR.Visible = True
            datosBusqueda_hotel.isNetRate = True
            buildDataGrid()
        Else
            lbl_info_totalNR.Visible = False
            txt_edit_totalNR.Visible = False
            lbl_info_currency_totalNR.Visible = False
            datosBusqueda_hotel.isNetRate = False
            buildDataGrid()
        End If
    End Sub

    Private Sub rbt_target_UV_CheckedChanged(sender As Object, e As EventArgs) Handles rbt_target_UV.CheckedChanged, rbt_target_HTL.CheckedChanged
        If rbt_target_UV.Checked Then
            rbt_target_HTL.Checked = False
        Else
            rbt_target_UV.Checked = False
        End If
    End Sub

    Private Sub loadResources()

        rbt_deposito.Text = CapaPresentacion.Idiomas.get_str("str_0107_depositoBanc")
        lbl_info_totalNR.Text = CapaPresentacion.Idiomas.get_str("str_565")
        lbl_noAuth.Text = CapaPresentacion.Idiomas.get_str("str_564")
        lbl_source.Text = CapaPresentacion.Idiomas.get_str("str_357_origen")
        lbl_months.Text = CapaPresentacion.Idiomas.get_str("str_563")
        lbl_llegada.Text = CapaPresentacion.Idiomas.get_str("str_0092_llegada")
        lbl_salida.Text = CapaPresentacion.Idiomas.get_str("str_0093_salida")
        lbl_tipo_Habitacion.Text = CapaPresentacion.Idiomas.get_str("str_0141_tipoRoom")
        btn_add_row.Text = CapaPresentacion.Idiomas.get_str("str_498")
        btn_cancelar.Text = CapaPresentacion.Idiomas.get_str("str_0021_cancelar")
        btn_remove_row.Text = CapaPresentacion.Idiomas.get_str("str_499")
        btn_modificar.Text = CapaPresentacion.Idiomas.get_str("str_0158_modiRes")
        rdt_pay_in_hotel.Text = CapaPresentacion.Idiomas.get_str("str_561")
        rdt_tarjeta.Text = CapaPresentacion.Idiomas.get_str("str_562")

        lbl_peticion.Text = CapaPresentacion.Idiomas.get_str("str_0051_pericion")
        lbl_comentario.Text = CapaPresentacion.Idiomas.get_str("str_0229_comentarios")
        chk_NR.Text = CapaPresentacion.Idiomas.get_str("str_547")
        group_payment.Text = CapaPresentacion.Idiomas.get_str("str_566")


        cmb_mount_payment.Items.Add(CapaPresentacion.Idiomas.get_str("str_551"))
        cmb_mount_payment.Items.Add("3 " & CapaPresentacion.Idiomas.get_str("str_550"))
        cmb_mount_payment.Items.Add("6 " & CapaPresentacion.Idiomas.get_str("str_550"))
        cmb_mount_payment.Items.Add("9 " & CapaPresentacion.Idiomas.get_str("str_550"))
        cmb_mount_payment.Items.Add("12 " & CapaPresentacion.Idiomas.get_str("str_550"))



        cmb_source.Items.Add("Banamex")
        cmb_source.Items.Add("Santander")
        cmb_source.Items.Add("DineroMail")
        cmb_source.Items.Add("Bancomer")
        cmb_source.Items.Add("Banorte")

        cmb_status.Items.Add(CapaPresentacion.Idiomas.get_str("str_332_reservado"))
        cmb_status.Items.Add(CapaPresentacion.Idiomas.get_str("str_543"))
        cmb_status.Items.Add(CapaPresentacion.Idiomas.get_str("str_333_cancelado"))
        cmb_status.Items.Add(CapaPresentacion.Idiomas.get_str("str_544"))

        lbl_deposit.Text = CapaPresentacion.Idiomas.get_str("str_552")
        lbl_depositAmount.Text = CapaPresentacion.Idiomas.get_str("str_426_amountinfo")
        lbl_depositCurrency.Text = datosBusqueda_hotel.AltCurrency

        lbl_depositTarget.Text = CapaPresentacion.Idiomas.get_str("str_553")

        tp_eng.Text = CapaPresentacion.Idiomas.get_str("str_0031_ingles")
        tp_esp.Text = CapaPresentacion.Idiomas.get_str("str_0030_español")
        lbl_reference.Text = CapaPresentacion.Idiomas.get_str("str_0268_numeroReferencia")
    End Sub

End Class

