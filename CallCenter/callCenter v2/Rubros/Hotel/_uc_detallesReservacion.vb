Public Class _uc_detallesReservacion

    Public Sub load_data(ByVal datosBusqueda_paquete As dataBusqueda_paquete, ByVal datosBusqueda_vuelo As dataBusqueda_vuelo, ByVal datosBusqueda_auto As dataBusqueda_auto_test, ByVal datosBusqueda_hotel As dataBusqueda_hotel_test, ByVal datosBusqueda_actividad As dataBusqueda_actividad_test)
        Try
            DataGridView1.Rows.Clear()

            If datosBusqueda_paquete IsNot Nothing Then
                Dim ds As DataSet = obtenGenerales_paquetes.obten_general_Paquete_Description(datosBusqueda_paquete)
                inicializar_controles_paquetes(ds)
                Return
            End If

            If datosBusqueda_vuelo IsNot Nothing Then
                inicializar_controles_vuelos(datosBusqueda_vuelo)
                Return
            End If

            If datosBusqueda_auto IsNot Nothing Then
                inicializar_controles_autos(datosBusqueda_auto)
                Return
            End If

            If datosBusqueda_hotel IsNot Nothing Then
                inicializar_controles_hotel(datosBusqueda_hotel)
                Return
            End If

            If datosBusqueda_actividad IsNot Nothing Then
                inicializar_controles_actividad(datosBusqueda_actividad)
                Return
            End If

        Catch ex As Exception
            '
        End Try
    End Sub

    Private Sub inicializar_controles_paquetes(ByVal ds As DataSet)
        If ds Is Nothing Then
            Return
        End If

        If Not ds.Tables("error") Is Nothing Then
            PropertyGrid1.SelectedObject = Nothing
        Else
            Dim r As DataRow = ds.Tables("Package").Rows(0)

            data_lbl_itinerario.Text = r.Item("Itinerary")
            'Select Case r.Item("Status")
            '    Case "1"
            '        data_lbl_estado.Text = "Reservado"
            '    Case "3"
            '        data_lbl_estado.Text = "-Cancelado"
            '    Case "2"
            '        data_lbl_estado.Text = "No confirmada"
            '    Case Else
            '        data_lbl_estado.Text = ""
            'End Select
            data_lbl_estado.Text = herramientas.GetSatatusText(r.Item("Status"))

            agrega_item(r, "HotelReservationId", "HtlStatus", "Hotel")
            agrega_item(r, "CarReservationId", "CarStatus", "Auto")
            agrega_item(r, "FlightReservationId", "FlightStatus", "Vuelo")

            ''''''''actividades
            If r.Table.Columns.Contains("ActivityReservationId") AndAlso Not r.IsNull("ActivityReservationId") AndAlso r.Item("ActivityReservationId") = "1" Then
                Dim datos_paq As New dataBusqueda_paquete
                datos_paq.itinerario = ds.Tables("Package").Rows(0).Item("Itinerary")
                Dim ds_actividades As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_ITR2(datos_paq)

                For Each r_actividad As DataRow In ds_actividades.Tables("Reservation").Rows
                    agrega_item(r_actividad, "ReservationID", "Status", "Actividad")
                Next
            End If

            If DataGridView1.Rows.Count = 0 Then PropertyGrid1.SelectedObject = Nothing
        End If
    End Sub

    Private Sub agrega_item(ByVal r As DataRow, ByVal campo_id As String, ByVal campo_estado As String, ByVal rubro As String)
        Dim estado As String
        If r.Table.Columns.Contains(campo_id) AndAlso r.Table.Columns.Contains(campo_estado) AndAlso Not r.IsNull(campo_id) AndAlso Not r.IsNull(campo_estado) Then
            'Select Case r.Item(campo_estado)
            '    Case "1"
            '        estado = "Reservado"
            '    Case "3"
            '        estado = "-Cancelado"
            '    Case Else
            '        estado = ""
            'End Select
            estado = herramientas.GetSatatusText(r.Item(campo_estado))
            DataGridView1.Rows.Add(r.Item(campo_id), rubro, estado)
        End If
    End Sub

    Private Sub inicializar_controles_vuelos(ByVal datosBusqueda_vuelo As dataBusqueda_vuelo)
        Dim estado As String
        'Select Case datosBusqueda_vuelo.status
        '    Case "1"
        '        estado = "Reservado"
        '    Case "3"
        '        estado = "-Cancelado"
        '    Case Else
        '        estado = ""
        'End Select
        estado = herramientas.GetSatatusText(datosBusqueda_vuelo.status)

        data_lbl_itinerario.Text = datosBusqueda_vuelo.ConfirmNumber
        data_lbl_estado.Text = estado

        DataGridView1.Rows.Add(datosBusqueda_vuelo.ReservationId, "Vuelo", estado)
    End Sub

    Private Sub inicializar_controles_autos(ByVal datosBusqueda_auto As dataBusqueda_auto_test)
        Dim estado As String
        'Select Case datosBusqueda_auto.Status
        '    Case "1"
        '        estado = "Reservado"
        '    Case "3"
        '        estado = "-Cancelado"
        '    Case Else
        '        estado = ""
        'End Select
        estado = herramientas.GetSatatusText(datosBusqueda_auto.Status)

        data_lbl_itinerario.Text = datosBusqueda_auto.ConfirmNumber
        data_lbl_estado.Text = estado

        DataGridView1.Rows.Add(datosBusqueda_auto.IdReservation, "Auto", estado)
    End Sub

    Private Sub inicializar_controles_hotel(ByVal datosBusqueda_hotel As dataBusqueda_hotel_test)
        Dim estado As String
        'Select Case datosBusqueda_hotel.Status
        '    Case "1"
        '        estado = "Reservado"
        '    Case "3"
        '        estado = "-Cancelado"
        '    Case Else
        '        estado = ""
        'End Select
        estado = herramientas.GetSatatusText(datosBusqueda_hotel.Status)

        data_lbl_itinerario.Text = datosBusqueda_hotel.ConfirmNumber
        data_lbl_estado.Text = estado

        DataGridView1.Rows.Add(datosBusqueda_hotel.ReservationId, "Hotel", estado)
    End Sub

    Private Sub inicializar_controles_actividad(ByVal datosBusqueda_actividad As dataBusqueda_actividad_test)
        Dim estado As String
        'Select Case datosBusqueda_actividad.Status
        '    Case "1"
        '        estado = "Reservado"
        '    Case "3"
        '        estado = "-Cancelado"
        '    Case "2"
        '        estado = "No confirmada"
        '    Case Else
        '        estado = ""
        'End Select
        estado = herramientas.GetSatatusText(datosBusqueda_actividad.status)

        data_lbl_itinerario.Text = datosBusqueda_actividad.ConfirmNumber
        data_lbl_estado.Text = estado

        DataGridView1.Rows.Add(datosBusqueda_actividad.ReservationID, "Actividad", estado)
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count = 0 Then Return

        Dim Rubro As String = DataGridView1.SelectedRows(0).Cells("Rubro").Value
        Dim ResId As String = DataGridView1.SelectedRows(0).Cells("ResId").Value

        Select Case Rubro
            Case "Hotel"
                Dim datosDisplay As New datos_display
                datosDisplay.ReservationId = ResId
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_hotel_display_resID(datosDisplay)
                If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Room") OrElse ds_display.Tables("Room").Rows.Count = 0 Then
                    PropertyGrid1.SelectedObject = Nothing
                    Exit Select
                End If

                Dim dbl_total As Double = CDbl(ds_display.Tables("Reservation").Rows(0).Item("Total"))
                Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Reservation").Rows(0).Item("Money")

                Dim fecha_res As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("ReservationDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
                Dim fecha_in_ As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("CheckInDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")
                Dim fecha_out As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("CheckOutDate"), "yyyy/MM/dd", Nothing).ToString("dd/MMM/yyyy")

                Dim datos As New reservaciones_display(ds_display.Tables("Reservation").Rows(0).Item("ConfirmNumber"), ds_display.Tables("Reservation").Rows(0).Item("RecLoc"), fecha_res, fecha_in_, fecha_out, ds_display.Tables("Room").Rows(0).Item("TravelerName"), ds_display.Tables("Reservation").Rows(0).Item("CityName"), str_total, ds_display.Tables("Reservation").Rows(0).Item("HotelName"))
                PropertyGrid1.SelectedObject = datos

            Case "Auto"
                Dim datos_car As New dataBusqueda_auto_test
                datos_car.IdReservation = ResId
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarReservation(datos_car)
                If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("OfficeStart") OrElse ds_display.Tables("OfficeStart").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Rate") OrElse ds_display.Tables("Rate").Rows.Count = 0 Then
                    PropertyGrid1.SelectedObject = Nothing
                    Exit Select
                End If

                Dim total_day As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountDay")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountDayQty"))
                Dim total_wee As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountWeek")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountWeekQty"))
                Dim total_exD As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraDay")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraDayQty"))
                Dim total_exH As Double = CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraHour")) * CDbl(ds_display.Tables("Rate").Rows(0).Item("AmountExtraHourQty"))

                Dim dbl_total As Double = total_day + total_wee + total_exD + total_exH
                Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Reservation").Rows(0).Item("Currency")

                Dim str_fechR As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("ReserveDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MM/dd")
                Dim str_fechI As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("StartDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MM/dd")
                Dim str_fechO As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("EndDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MM/dd")

                Dim datos As New reservaciones_display(ds_display.Tables("Reservation").Rows(0).Item("ConfReservation"), ds_display.Tables("Reservation").Rows(0).Item("idReservationGDS"), str_fechR, str_fechI, str_fechO, ds_display.Tables("Reservation").Rows(0).Item("DriverName"), ds_display.Tables("OfficeStart").Rows(0).Item("CityStart"), str_total, ds_display.Tables("Reservation").Rows(0).Item("CarVendor"))
                PropertyGrid1.SelectedObject = datos

            Case "Actividad"
                Dim datos_actividad As New dataBusqueda_actividad_test
                datos_actividad.ReservationID = ResId
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_actividad_DSR2(datos_actividad)
                If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 OrElse Not ds_display.Tables.Contains("Event") OrElse ds_display.Tables("Event").Rows.Count = 0 Then
                    PropertyGrid1.SelectedObject = Nothing
                    Exit Select
                End If

                Dim dbl_total As Double = CDbl(ds_display.Tables("Reservation").Rows(0).Item("Total"))
                Dim str_total As String = Format(dbl_total, "c") + " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")

                'Dim str_fechR As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("Date"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MM/dd")
                Dim str_fechR As String = CDate(ds_display.Tables("Reservation").Rows(0).Item("Date")).ToString("yyyy/MM/dd")
                Dim str_fechI As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).GetChildRows("Reservation_Event")(0).Item("StartDate"), "MM/dd/yyyy", Nothing).ToString("yyyy/MM/dd")
                'Dim str_fechO As String = DateTime.ParseExact(ds_display.Tables("Reservation").Rows(0).Item("EndDate"), CapaPresentacion.Common.DateDataFormat, Nothing).ToString("yyyy/MM/dd")

                Dim datos As New reservaciones_display(ds_display.Tables("Reservation").Rows(0).Item("ReservationNumber"), "", str_fechR, str_fechI, "", ds_display.Tables("CreditCard").Rows(0).Item("Name"), ds_display.Tables("Reservation").Rows(0).GetChildRows("Reservation_Event")(0).Item("City"), str_total, ds_display.Tables("Property").Rows(0).Item("PropertyName"))
                PropertyGrid1.SelectedObject = datos

            Case "Vuelo"
                Dim datos_vuelo As New dataBusqueda_vuelo
                datos_vuelo.ReservationId = ResId
                Dim ds_display As DataSet = CapaLogicaNegocios.GetWS.obten_vuelo_AirReservationDetails(datos_vuelo)

                If ds_display Is Nothing OrElse Not ds_display.Tables.Contains("Reservation") OrElse ds_display.Tables("Reservation").Rows.Count = 0 Then
                    PropertyGrid1.SelectedObject = Nothing
                    Exit Select
                End If

                Dim dbl_total As Double '= CDbl(ds_display.Tables("ReservationFees").Rows(0).Item("GrandTotal"))
                Dim str_total As String '= Format(dbl_total, "c") '+ " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")
                Dim str_fechR As String '= CDate(ds_display.Tables("Reservation").Rows(0).Item("RegistrationDate")).ToString("yyyy/MM/dd")
                Dim str_fechI As String '= ds_display.Tables("Reservation").Rows(0).Item("depDate")
                Dim str_fechO As String '= ds_display.Tables("Reservation").Rows(0).Item("retDate")
                Dim str_aero As String
                Dim name As String '= ds_display.Tables("Passengers").Rows(0).Item("FName") + " " + ds_display.Tables("Passengers").Rows(0).Item("LName")

                If ds_display.Tables.Contains("ReservationFees") AndAlso ds_display.Tables("ReservationFees").Rows.Count > 0 Then
                    dbl_total = CDbl(ds_display.Tables("ReservationFees").Rows(0).Item("GrandTotal"))
                End If
                If ds_display.Tables.Contains("Passengers") AndAlso ds_display.Tables("Passengers").Rows.Count > 0 Then
                    name = ds_display.Tables("Passengers").Rows(0).Item("FName") + " " + ds_display.Tables("Passengers").Rows(0).Item("LName")
                End If
                If ds_display.Tables.Contains("ReservationDetail") AndAlso ds_display.Tables("ReservationDetail").Rows.Count > 0 Then
                    str_aero = ds_display.Tables("ReservationDetail").Rows(0).Item("airline")
                End If

                str_total = Format(dbl_total, "c") '+ " " + ds_display.Tables("Property").Rows(0).Item("CurrencyCode")
                str_fechR = CDate(ds_display.Tables("Reservation").Rows(0).Item("RegistrationDate")).ToString("yyyy/MM/dd")
                str_fechI = ds_display.Tables("Reservation").Rows(0).Item("depDate")
                If ds_display.Tables("Reservation").Columns.Contains("retDate") Then
                    str_fechO = ds_display.Tables("Reservation").Rows(0).Item("retDate")
                End If

                Dim datos As New reservaciones_display(ds_display.Tables("Reservation").Rows(0).Item("RvaNumber"), ds_display.Tables("Reservation").Rows(0).Item("noConfirmation"), str_fechR, str_fechI, str_fechO, name, ds_display.Tables("Reservation").Rows(0).Item("depCity"), str_total, str_aero)
                PropertyGrid1.SelectedObject = datos
        End Select
    End Sub

    Public Class reservaciones_display
        Private _NumeroConfirmacion As String
        Private _RecLOc As String
        Private _FechaRegistro As String
        Private _FechaArrivo As String
        Private _FechaSalida As String
        Private _NombreCliente As String
        Private _Ciudad As String
        Private _Total As String
        Private _Empresa As String

        Public Sub New(ByVal _NumeroConfirmacion, ByVal _RecLOc, ByVal _FechaRegistro, ByVal _FechaArrivo, ByVal _FechaSalida, ByVal _NombreCliente, ByVal _Ciudad, ByVal _Total, ByVal _Empresa)
            Me._NumeroConfirmacion = _NumeroConfirmacion
            Me._RecLOc = _RecLOc
            Me._FechaRegistro = _FechaRegistro
            Me._FechaArrivo = _FechaArrivo
            Me._FechaSalida = _FechaSalida
            Me._NombreCliente = _NombreCliente
            Me._Ciudad = _Ciudad
            Me._Total = _Total
            Me._Empresa = _Empresa
        End Sub

        Public ReadOnly Property NumeroConfirmacion()
            Get
                Return _NumeroConfirmacion
            End Get
        End Property

        Public ReadOnly Property RecLOc()
            Get
                Return _RecLOc
            End Get
        End Property

        Public ReadOnly Property FechaRegistro()
            Get
                Return _FechaRegistro
            End Get
        End Property

        Public ReadOnly Property FechaArrivo()
            Get
                Return _FechaArrivo
            End Get
        End Property

        Public ReadOnly Property FechaSalida()
            Get
                Return _FechaSalida
            End Get
        End Property

        Public ReadOnly Property NombreCliente()
            Get
                Return _NombreCliente
            End Get
        End Property

        Public ReadOnly Property Ciudad()
            Get
                Return _Ciudad
            End Get
        End Property

        Public ReadOnly Property Total()
            Get
                Return _Total
            End Get
        End Property

        Public ReadOnly Property Empresa()
            Get
                Return _Empresa
            End Get
        End Property

    End Class

    Private Sub DataGridView1_RowsRemoved(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        PropertyGrid1.SelectedObject = Nothing
    End Sub

End Class
