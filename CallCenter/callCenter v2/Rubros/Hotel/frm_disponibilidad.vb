﻿Public Class frm_disponibilidad
    Public idHotel As Integer
    Public NombreHotel As String
    Dim ds As Call_Hotel_GetAvailability_RS
    Dim cargando As Boolean = True
    Private Sub frm_disponibilidad_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CargaRecursos()
        CargaHoteles()
        cargando = False
        Mostrar(True)
    End Sub

    Private Sub CargaRecursos()
        Me.btn_Cancelar.Text = CapaPresentacion.Idiomas.get_str("str_0039_salir")
        Me.btnMostrar.Text = CapaPresentacion.Idiomas.get_str("str_488")
        Me.rbDisponibles.Text = CapaPresentacion.Idiomas.get_str("str_483")
        Me.rbOcupados.Text = CapaPresentacion.Idiomas.get_str("str_489")

        Me.Text = CapaPresentacion.Idiomas.get_str("str_483")
    End Sub

    Private Sub btn_Cancelar_Click(sender As System.Object, e As System.EventArgs) Handles btn_Cancelar.Click
        Close()
    End Sub


    Private Sub CargaHoteles()

        cmbHotel.Items.Clear()
        For Each dr As dataHotel In dataOperador.Hoteles
            cmbHotel.Items.Add(dr.idHotel, dr.Nombre)
            If dr.idHotel = idHotel Then cmbHotel.SelectedIndex = dr.index
        Next
        If cmbHotel.SelectedIndex = -1 Then
            cmbHotel.Visible = False
            lblHotelData.Text = NombreHotel
        End If

    End Sub

    Private Sub Mostrar(CargarDatos As Boolean)
        Dim ws As New webService_call
        Dim rs As Call_Hotel_GetAvailability_RS = Nothing
        Dim DateBegin As Date = Me.DateTimePicker1.Value
        Dim idH As Integer = idHotel
        If cmbHotel.Visible And cmbHotel.SelectedIndex <> -1 Then idH = cmbHotel.Value
        If CargarDatos Then
            rs = ws.HotelGetAvailability(idH, DateBegin, DateBegin.AddDays(14))
        Else
            If ds Is Nothing Then
                rs = ws.HotelGetAvailability(idH, DateBegin, DateBegin.AddDays(14))
            Else
                rs = ds
            End If
        End If
        ds = rs
        CargaDisponibilidad(DateBegin, rbDisponibles.Checked, rs)

    End Sub

    Private Sub CargaDisponibilidad(DateBegin As Date, Availables As Boolean, rs As Call_Hotel_GetAvailability_RS)
        Try
            If Not rs Is Nothing Then
                Dim colw_h As String = " style ='width:20px; text-align: center; font-size:9px; background-color:Grey;'"
                Dim colw_h_hab As String = " style ='width:120px; text-align: center; font-size:9px; background-color:Grey;'"
                Dim colw_hab As String = " style ='width:120px; font-size:9px;'"
                Dim colw As String = " style ='width:20px; text-align: center; font-size:9px;  border: thin solid;'"
                Dim cad As String = ""
                cad &= "<table cellpadding='0' cellspacing='0'>"
                cad &= "<tr>"
                cad &= "<td></td>"
                cad &= "<td colspan='14' " & colw & ">" & DateBegin.ToString("MMM/yyyy") & "</td>"
                cad &= "</tr>"
                cad &= "<tr>"
                cad &= "<td></td>"
                For x As Integer = 0 To 13
                    cad &= "<td " & colw_h & ">" & DateBegin.AddDays(x).ToString("ddd").Substring(0, 2) & "</td>"
                Next
                cad &= "</tr>"
                cad &= "<tr>"
                cad &= "<td " & colw_h_hab & ">Habitacion</td>"
                For x As Integer = 0 To 13
                    cad &= "<td " & colw_h & ">" & DateBegin.AddDays(x).Day & "</td>"
                Next
                cad &= "</tr>"
                Dim arr(14) As Integer
                Dim room As String = ""
                For Each r As DataRow In rs.Tables(1).Rows
                    If room <> r("idRoom") Then
                        If room <> "" Then cad &= "</tr>"
                        cad &= "<tr>"
                        Dim dr As DataRow() = rs.Tables(0).Select("idRoom='" & r("idRoom") & "'")
                        Dim roomname As String = ""
                        If dr.Length = 1 Then roomname = dr(0).Item("Name")
                        cad &= "<td " & colw_hab & ">" & roomname & "</td>"
                        room = r("idRoom")
                    End If
                    Dim dia As Long = DateDiff(DateInterval.Day, CDate(DateBegin.ToShortDateString), CDate(r("date")))
                    If Availables Then
                        cad &= "<td " & colw & ">" & r("Availables") & "</td>"
                        If r("Availables") <> "X" Then arr(dia) += r("Availables")
                    Else
                        cad &= "<td " & colw & ">" & r("Reserved") & "</td>"
                        If r("Reserved") <> "X" Then arr(dia) += r("Reserved")
                    End If
                Next
                'Totales
                cad &= "<tr>"
                cad &= "<td " & colw & "><b>Total Global:</b></td>"
                For x As Integer = 0 To 13
                    cad &= "<td " & colw & "><b>" & arr(x) & "</b></td>"
                Next
                cad &= "</tr>"

                cad &= "</table>"
                WebBrowser1.DocumentText = cad & "<br />"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub rbOcupados_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbOcupados.CheckedChanged
        Call Mostrar(False)
    End Sub

    Private Sub rbDisponibles_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbDisponibles.CheckedChanged
        If Not cargando Then Call Mostrar(False)
    End Sub

    Private Sub cmbHotel_ValueChanged(sender As System.Object, e As System.EventArgs) Handles cmbHotel.ValueChanged
        If Not cargando Then Call Mostrar(True)
    End Sub

    Private Sub btnMostrar_Click(sender As System.Object, e As System.EventArgs) Handles btnMostrar.Click
        Call Mostrar(True)
    End Sub

    
    Private Sub btnAtras_Click(sender As System.Object, e As System.EventArgs) Handles btnAtras.Click
        DateTimePicker1.Value = DateTimePicker1.Value.AddDays(-7)
        Call Mostrar(True)
    End Sub

    Private Sub btnAdelante_Click(sender As System.Object, e As System.EventArgs) Handles btnAdelante.Click
        DateTimePicker1.Value = DateTimePicker1.Value.AddDays(7)
        Call Mostrar(True)
    End Sub
End Class