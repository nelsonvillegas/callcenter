Public Class frm_HotelporNombre
    Public Ciudad As String
    Public Nombre As String

    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        If txtNombre.Text.Trim = "" Then Exit Sub

        Dim ws As New WS_DESTINOSUPDATE.destinosUpdate_service
        ws.Url = CapaAccesoDatos.XML.wsData.UrlWsUpdate

        Dim ds As DataSet
        If dataOperador.idCorporativo <> "" Then
            ds = ws.obten_empresas_corporativo("10", txtNombre.Text, "", "", "", "2", dataOperador.idCorporativo, 0)
        Else
            ds = ws.obten_empresas("10", txtNombre.Text, "", "", "", "2")
        End If

        Me.UltraGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False


        Me.UltraGrid1.DataSource = ds
        For Each c As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns
            c.Hidden = True
            c.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
        Next

        UltraGrid1.DisplayLayout.Bands(0).Columns("NombreEmpresa").Header.Caption = CapaPresentacion.Idiomas.get_str("str_0032_hotel")
        UltraGrid1.DisplayLayout.Bands(0).Columns("Domicilio").Header.Caption = CapaPresentacion.Idiomas.get_str("str_0117_direccion")
        UltraGrid1.DisplayLayout.Bands(0).Columns("Ciudad").Header.Caption = CapaPresentacion.Idiomas.get_str("str_0004_ciudad")
        UltraGrid1.DisplayLayout.Bands(0).Columns("idpais").Header.Caption = CapaPresentacion.Idiomas.get_str("str_0120_pais")

        UltraGrid1.DisplayLayout.Bands(0).Columns("NombreEmpresa").Hidden = False
        UltraGrid1.DisplayLayout.Bands(0).Columns("Domicilio").Hidden = False
        UltraGrid1.DisplayLayout.Bands(0).Columns("Ciudad").Hidden = False
        UltraGrid1.DisplayLayout.Bands(0).Columns("estado").Hidden = False
        UltraGrid1.DisplayLayout.Bands(0).Columns("idpais").Hidden = False

    End Sub

    Private Sub frm_HotelporNombre_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ciudad = ""
        Nombre = ""
        Me.UltraGroupBox1.Text = CapaPresentacion.Idiomas.get_str("str_450")
        Me.Text = CapaPresentacion.Idiomas.get_str("str_0222_buscHoteles")
        Me.lblNombre.Text = CapaPresentacion.Idiomas.get_str("str_0040_nombre")
        'str_0222_buscHoteles
        Me.btn_actualizar.Text = CapaPresentacion.Idiomas.get_str("str_0187_actualizar")
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow

        Me.Ciudad = e.Row.Cells("Ciudad").Text & "," & e.Row.Cells("Estado").Text & "," & e.Row.Cells("idpais").Text
        Me.Close()

    End Sub

    
End Class