Public Class habitaciones

    Public datosBusqueda_hotel As dataBusqueda_hotel_test

    Private Sub habitaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
        CapaPresentacion.Idiomas.cambiar_habitaciones(Me)
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        Close()
    End Sub

    Private Sub load_data()
        Try
            Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOD(datosBusqueda_hotel)
      
            If ds Is Nothing Then
                Return
            End If

            If Not ds.Tables("error") Is Nothing Then
                Dim r As DataRow = ds.Tables("error").Rows(0)
                MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Cursor = Cursors.Arrow
            Else
                Dim titVal() As String = herramientas.obten_TitleValue(ds, "Description", "ROOM")

                Dim lbl_header As New Label
                lbl_header.AutoSize = True
                lbl_header.MaximumSize = New Point(UltraPanel1.Width - 40, 0)
                lbl_header.Text = titVal(0) + ControlChars.CrLf + ControlChars.CrLf + titVal(1)
                UltraPanel1.ClientArea.Controls.Add(lbl_header)

                If ds.Tables.Contains("Room") Then
                    For Each r_roo As DataRow In ds.Tables("Room").Rows
                        Dim pnl As New Infragistics.Win.Misc.UltraPanel
                        pnl.Size = New Size(UltraPanel1.Width - 30, 120 + 10)
                        pnl.AutoSize = True

                        Dim pic As New PictureBox
                        pic.Image = herramientas.Load_Web_Image(r_roo.Item("ImageURL"), Global.callCenter.My.Resources.Resources.noRoomImage)
                        pic.Size = New Size(160, 120)

                        Dim tit As New Label
                        tit.Text = r_roo.Item("RoomName")
                        tit.AutoSize = True
                        tit.Location = New Point(pic.Left + pic.Width + 10, 5)

                        Dim tex As New Label
                        tex.AutoSize = True
                        tex.Location = New Point(pic.Left + pic.Width + 10, tit.Top + tit.Height)
                        tex.MaximumSize = New Point(pnl.Width - pic.Left - pic.Width - 215, 0)
                        tex.Text = CapaPresentacion.Common.HTMLDecode(r_roo.Item("Description")).Trim

                        pnl.ClientArea.Controls.Add(pic)
                        pnl.ClientArea.Controls.Add(tit)
                        pnl.ClientArea.Controls.Add(tex)

                        UltraPanel1.ClientArea.Controls.Add(pnl)
                    Next
                End If
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0024", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Sub

End Class