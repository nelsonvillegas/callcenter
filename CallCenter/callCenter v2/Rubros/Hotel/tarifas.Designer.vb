<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tarifas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(tarifas))
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim T_Habitaciones1 As callCenter.uc_habitaciones.T_Habitaciones = New callCenter.uc_habitaciones.T_Habitaciones
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lbl_info = New System.Windows.Forms.Label
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox
        Me.Uc_habitaciones1 = New callCenter.uc_habitaciones
        Me.btn_actualizar = New System.Windows.Forms.Button
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.data_lbl_codProm = New System.Windows.Forms.Label
        Me.lbl_codProm = New System.Windows.Forms.Label
        Me.data_lbl_totalTarifas = New System.Windows.Forms.Label
        Me.lbl_totalTarifas = New System.Windows.Forms.Label
        Me.data_lbl_tarifasEn = New System.Windows.Forms.Label
        Me.lbl_tarifasEn = New System.Windows.Forms.Label
        Me.lbl_impuestos = New System.Windows.Forms.Label
        Me.data_lbl_cadena = New System.Windows.Forms.Label
        Me.data_lbl_hotel = New System.Windows.Forms.Label
        Me.data_lbl_fechas = New System.Windows.Forms.Label
        Me.lbl_cadena = New System.Windows.Forms.Label
        Me.lbl_nombreHotel = New System.Windows.Forms.Label
        Me.lbl_fecha = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblConvenio = New System.Windows.Forms.Label
        Me.lblConvenioData = New System.Windows.Forms.Label
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraGrid1
        '
        Me.UltraGrid1.ContextMenuStrip = Me.ContextMenuStrip1
        Appearance30.BackColor = System.Drawing.Color.White
        Appearance30.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance30
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance31.ImageBackground = CType(resources.GetObject("Appearance31.ImageBackground"), System.Drawing.Image)
        Appearance31.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance31.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance31.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance31
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance28.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance28
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance29.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance29.FontData.Name = "Trebuchet MS"
        Appearance29.FontData.SizeInPoints = 9.0!
        Appearance29.ForeColor = System.Drawing.Color.White
        Appearance29.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance29
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance32.FontData.BoldAsString = "True"
        Appearance32.FontData.Name = "Trebuchet MS"
        Appearance32.FontData.SizeInPoints = 10.0!
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance32
        Appearance33.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance33
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance34.BackColor2 = System.Drawing.SystemColors.Control
        Appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance34.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance34
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance13.BackColor = System.Drawing.SystemColors.Window
        Appearance13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance13
        Appearance14.BackColor = System.Drawing.SystemColors.Highlight
        Appearance14.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance14
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance15.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance15
        Appearance16.BorderColor = System.Drawing.Color.Transparent
        Appearance16.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance16
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance17
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance21.BackColor = System.Drawing.SystemColors.Control
        Appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance21.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance21
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance22.FontData.BoldAsString = "True"
        Appearance22.FontData.Name = "Trebuchet MS"
        Appearance22.FontData.SizeInPoints = 10.0!
        Appearance22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance22.ImageBackground = CType(resources.GetObject("Appearance22.ImageBackground"), System.Drawing.Image)
        Appearance22.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance22.TextHAlignAsString = "Left"
        Appearance22.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance22
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance1
        Appearance23.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance24
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance25.BorderColor = System.Drawing.Color.Transparent
        Appearance25.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance25
        Appearance26.BorderColor = System.Drawing.Color.Transparent
        Appearance26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance26.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance26
        Appearance27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance27
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance35.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance37.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance38
        Appearance39.ImageBackground = CType(resources.GetObject("Appearance39.ImageBackground"), System.Drawing.Image)
        Appearance39.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance39.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance39
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 0)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(979, 294)
        Me.UltraGrid1.TabIndex = 0
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lbl_info)
        Me.Panel1.Controls.Add(Me.UltraGroupBox1)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.btn_aceptar)
        Me.Panel1.Controls.Add(Me.lblConvenioData)
        Me.Panel1.Controls.Add(Me.lblConvenio)
        Me.Panel1.Controls.Add(Me.data_lbl_codProm)
        Me.Panel1.Controls.Add(Me.lbl_codProm)
        Me.Panel1.Controls.Add(Me.data_lbl_totalTarifas)
        Me.Panel1.Controls.Add(Me.lbl_totalTarifas)
        Me.Panel1.Controls.Add(Me.data_lbl_tarifasEn)
        Me.Panel1.Controls.Add(Me.lbl_tarifasEn)
        Me.Panel1.Controls.Add(Me.lbl_impuestos)
        Me.Panel1.Controls.Add(Me.data_lbl_cadena)
        Me.Panel1.Controls.Add(Me.data_lbl_hotel)
        Me.Panel1.Controls.Add(Me.data_lbl_fechas)
        Me.Panel1.Controls.Add(Me.lbl_cadena)
        Me.Panel1.Controls.Add(Me.lbl_nombreHotel)
        Me.Panel1.Controls.Add(Me.lbl_fecha)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(979, 117)
        Me.Panel1.TabIndex = 1
        '
        'lbl_info
        '
        Me.lbl_info.AutoSize = True
        Me.lbl_info.Location = New System.Drawing.Point(12, 90)
        Me.lbl_info.Name = "lbl_info"
        Me.lbl_info.Size = New System.Drawing.Size(121, 13)
        Me.lbl_info.TabIndex = 13
        Me.lbl_info.Text = "--------------------------------------"
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Controls.Add(Me.Uc_habitaciones1)
        Me.UltraGroupBox1.Controls.Add(Me.btn_actualizar)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(589, 3)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(362, 105)
        Me.UltraGroupBox1.TabIndex = 1
        Me.UltraGroupBox1.Text = "Buscar"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'Uc_habitaciones1
        '
        Me.Uc_habitaciones1.BackColor = System.Drawing.Color.Transparent
        T_Habitaciones1.TotalAdultos = 1
        T_Habitaciones1.TotalHabitaciones = 1
        T_Habitaciones1.TotalNinos = 0
        Me.Uc_habitaciones1.Habitaciones = T_Habitaciones1
        Me.Uc_habitaciones1.Location = New System.Drawing.Point(0, 19)
        Me.Uc_habitaciones1.Name = "Uc_habitaciones1"
        Me.Uc_habitaciones1.Size = New System.Drawing.Size(275, 88)
        Me.Uc_habitaciones1.TabIndex = 1
        '
        'btn_actualizar
        '
        Me.btn_actualizar.Location = New System.Drawing.Point(281, 21)
        Me.btn_actualizar.Name = "btn_actualizar"
        Me.btn_actualizar.Size = New System.Drawing.Size(75, 23)
        Me.btn_actualizar.TabIndex = 2
        Me.btn_actualizar.Text = "Actualizar"
        Me.btn_actualizar.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(269, 50)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'btn_aceptar
        '
        Me.btn_aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_aceptar.Location = New System.Drawing.Point(269, 85)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_aceptar.TabIndex = 3
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = True
        Me.btn_aceptar.Visible = False
        '
        'data_lbl_codProm
        '
        Me.data_lbl_codProm.AutoSize = True
        Me.data_lbl_codProm.Location = New System.Drawing.Point(91, 48)
        Me.data_lbl_codProm.Name = "data_lbl_codProm"
        Me.data_lbl_codProm.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_codProm.TabIndex = 0
        Me.data_lbl_codProm.Text = "----"
        '
        'lbl_codProm
        '
        Me.lbl_codProm.AutoSize = True
        Me.lbl_codProm.Location = New System.Drawing.Point(12, 48)
        Me.lbl_codProm.Name = "lbl_codProm"
        Me.lbl_codProm.Size = New System.Drawing.Size(62, 13)
        Me.lbl_codProm.TabIndex = 0
        Me.lbl_codProm.Text = "Cod. Prom.:"
        '
        'data_lbl_totalTarifas
        '
        Me.data_lbl_totalTarifas.AutoSize = True
        Me.data_lbl_totalTarifas.Location = New System.Drawing.Point(374, 5)
        Me.data_lbl_totalTarifas.Name = "data_lbl_totalTarifas"
        Me.data_lbl_totalTarifas.Size = New System.Drawing.Size(37, 13)
        Me.data_lbl_totalTarifas.TabIndex = 0
        Me.data_lbl_totalTarifas.Text = "----------"
        '
        'lbl_totalTarifas
        '
        Me.lbl_totalTarifas.AutoSize = True
        Me.lbl_totalTarifas.Location = New System.Drawing.Point(266, 5)
        Me.lbl_totalTarifas.Name = "lbl_totalTarifas"
        Me.lbl_totalTarifas.Size = New System.Drawing.Size(65, 13)
        Me.lbl_totalTarifas.TabIndex = 0
        Me.lbl_totalTarifas.Text = "Total tarifas:"
        '
        'data_lbl_tarifasEn
        '
        Me.data_lbl_tarifasEn.AutoSize = True
        Me.data_lbl_tarifasEn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.data_lbl_tarifasEn.Location = New System.Drawing.Point(374, 18)
        Me.data_lbl_tarifasEn.Name = "data_lbl_tarifasEn"
        Me.data_lbl_tarifasEn.Size = New System.Drawing.Size(25, 13)
        Me.data_lbl_tarifasEn.TabIndex = 0
        Me.data_lbl_tarifasEn.Text = "------"
        '
        'lbl_tarifasEn
        '
        Me.lbl_tarifasEn.AutoSize = True
        Me.lbl_tarifasEn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_tarifasEn.Location = New System.Drawing.Point(266, 18)
        Me.lbl_tarifasEn.Name = "lbl_tarifasEn"
        Me.lbl_tarifasEn.Size = New System.Drawing.Size(102, 13)
        Me.lbl_tarifasEn.TabIndex = 0
        Me.lbl_tarifasEn.Text = "Las tarifas est�n en:"
        '
        'lbl_impuestos
        '
        Me.lbl_impuestos.AutoSize = True
        Me.lbl_impuestos.Location = New System.Drawing.Point(266, 31)
        Me.lbl_impuestos.Name = "lbl_impuestos"
        Me.lbl_impuestos.Size = New System.Drawing.Size(176, 13)
        Me.lbl_impuestos.TabIndex = 0
        Me.lbl_impuestos.Text = "Impuestos no incluidos en las tarifas"
        '
        'data_lbl_cadena
        '
        Me.data_lbl_cadena.AutoSize = True
        Me.data_lbl_cadena.Location = New System.Drawing.Point(91, 35)
        Me.data_lbl_cadena.Name = "data_lbl_cadena"
        Me.data_lbl_cadena.Size = New System.Drawing.Size(49, 13)
        Me.data_lbl_cadena.TabIndex = 0
        Me.data_lbl_cadena.Text = "--------------"
        '
        'data_lbl_hotel
        '
        Me.data_lbl_hotel.AutoSize = True
        Me.data_lbl_hotel.Location = New System.Drawing.Point(91, 22)
        Me.data_lbl_hotel.Name = "data_lbl_hotel"
        Me.data_lbl_hotel.Size = New System.Drawing.Size(55, 13)
        Me.data_lbl_hotel.TabIndex = 0
        Me.data_lbl_hotel.Text = "----------------"
        '
        'data_lbl_fechas
        '
        Me.data_lbl_fechas.AutoSize = True
        Me.data_lbl_fechas.Location = New System.Drawing.Point(91, 9)
        Me.data_lbl_fechas.Name = "data_lbl_fechas"
        Me.data_lbl_fechas.Size = New System.Drawing.Size(58, 13)
        Me.data_lbl_fechas.TabIndex = 0
        Me.data_lbl_fechas.Text = "-----------------"
        '
        'lbl_cadena
        '
        Me.lbl_cadena.AutoSize = True
        Me.lbl_cadena.Location = New System.Drawing.Point(12, 35)
        Me.lbl_cadena.Name = "lbl_cadena"
        Me.lbl_cadena.Size = New System.Drawing.Size(47, 13)
        Me.lbl_cadena.TabIndex = 0
        Me.lbl_cadena.Text = "Cadena:"
        '
        'lbl_nombreHotel
        '
        Me.lbl_nombreHotel.AutoSize = True
        Me.lbl_nombreHotel.Location = New System.Drawing.Point(12, 22)
        Me.lbl_nombreHotel.Name = "lbl_nombreHotel"
        Me.lbl_nombreHotel.Size = New System.Drawing.Size(73, 13)
        Me.lbl_nombreHotel.TabIndex = 0
        Me.lbl_nombreHotel.Text = "Nombre hotel:"
        '
        'lbl_fecha
        '
        Me.lbl_fecha.AutoSize = True
        Me.lbl_fecha.Location = New System.Drawing.Point(12, 9)
        Me.lbl_fecha.Name = "lbl_fecha"
        Me.lbl_fecha.Size = New System.Drawing.Size(40, 13)
        Me.lbl_fecha.TabIndex = 0
        Me.lbl_fecha.Text = "Fecha:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.UltraGrid1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 117)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(979, 294)
        Me.Panel2.TabIndex = 2
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(12, 61)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(55, 13)
        Me.lblConvenio.TabIndex = 0
        Me.lblConvenio.Text = "Convenio:"
        '
        'lblConvenioData
        '
        Me.lblConvenioData.AutoSize = True
        Me.lblConvenioData.Location = New System.Drawing.Point(91, 61)
        Me.lblConvenioData.Name = "lblConvenioData"
        Me.lblConvenioData.Size = New System.Drawing.Size(19, 13)
        Me.lblConvenioData.TabIndex = 0
        Me.lblConvenioData.Text = "----"
        '
        'tarifas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(979, 411)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "tarifas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas del hotel"
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents lbl_totalTarifas As System.Windows.Forms.Label
    Friend WithEvents data_lbl_tarifasEn As System.Windows.Forms.Label
    Friend WithEvents lbl_tarifasEn As System.Windows.Forms.Label
    Friend WithEvents lbl_impuestos As System.Windows.Forms.Label
    Friend WithEvents data_lbl_cadena As System.Windows.Forms.Label
    Friend WithEvents data_lbl_hotel As System.Windows.Forms.Label
    Friend WithEvents data_lbl_fechas As System.Windows.Forms.Label
    Friend WithEvents lbl_cadena As System.Windows.Forms.Label
    Friend WithEvents lbl_nombreHotel As System.Windows.Forms.Label
    Friend WithEvents data_lbl_totalTarifas As System.Windows.Forms.Label
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents data_lbl_codProm As System.Windows.Forms.Label
    Friend WithEvents lbl_codProm As System.Windows.Forms.Label
    Friend WithEvents btn_actualizar As System.Windows.Forms.Button
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Uc_habitaciones1 As callCenter.uc_habitaciones
    Friend WithEvents lbl_info As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents lblConvenioData As System.Windows.Forms.Label
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
End Class
