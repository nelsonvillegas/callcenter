<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tarifas_personalizadas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim T_Habitaciones1 As callCenter.uc_habitaciones.T_Habitaciones = New callCenter.uc_habitaciones.T_Habitaciones
        Me.lblTipo = New System.Windows.Forms.Label
        Me.cmbTipo = New System.Windows.Forms.ComboBox
        Me.txtTarifa = New System.Windows.Forms.TextBox
        Me.lblTarifa = New System.Windows.Forms.Label
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.lblHotel = New System.Windows.Forms.Label
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.lblHotelName = New System.Windows.Forms.Label
        Me.lblCheckin = New System.Windows.Forms.Label
        Me.lblLlegada = New System.Windows.Forms.Label
        Me.lblCheckout = New System.Windows.Forms.Label
        Me.lblSalida = New System.Windows.Forms.Label
        Me.uce_moneda = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.Uc_habitaciones1 = New callCenter.uc_habitaciones
        Me.lblTax = New System.Windows.Forms.Label
        Me.txtTax = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(12, 80)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(69, 13)
        Me.lblTipo.TabIndex = 1
        Me.lblTipo.Text = "Tipo de Hab."
        '
        'cmbTipo
        '
        Me.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipo.FormattingEnabled = True
        Me.cmbTipo.Location = New System.Drawing.Point(90, 77)
        Me.cmbTipo.Name = "cmbTipo"
        Me.cmbTipo.Size = New System.Drawing.Size(286, 21)
        Me.cmbTipo.TabIndex = 0
        '
        'txtTarifa
        '
        Me.txtTarifa.Location = New System.Drawing.Point(137, 189)
        Me.txtTarifa.MaxLength = 10
        Me.txtTarifa.Name = "txtTarifa"
        Me.txtTarifa.Size = New System.Drawing.Size(131, 20)
        Me.txtTarifa.TabIndex = 2
        Me.txtTarifa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTarifa
        '
        Me.lblTarifa.AutoSize = True
        Me.lblTarifa.Location = New System.Drawing.Point(12, 192)
        Me.lblTarifa.Name = "lblTarifa"
        Me.lblTarifa.Size = New System.Drawing.Size(119, 13)
        Me.lblTarifa.TabIndex = 4
        Me.lblTarifa.Text = "Tarifa Noche por Hab. :"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(220, 237)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'lblHotel
        '
        Me.lblHotel.AutoSize = True
        Me.lblHotel.Location = New System.Drawing.Point(12, 9)
        Me.lblHotel.Name = "lblHotel"
        Me.lblHotel.Size = New System.Drawing.Size(38, 13)
        Me.lblHotel.TabIndex = 4
        Me.lblHotel.Text = "Hotel :"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(301, 237)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancelar.TabIndex = 6
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'lblHotelName
        '
        Me.lblHotelName.AutoSize = True
        Me.lblHotelName.ForeColor = System.Drawing.Color.Blue
        Me.lblHotelName.Location = New System.Drawing.Point(87, 9)
        Me.lblHotelName.Name = "lblHotelName"
        Me.lblHotelName.Size = New System.Drawing.Size(38, 13)
        Me.lblHotelName.TabIndex = 4
        Me.lblHotelName.Text = "Hotel :"
        '
        'lblCheckin
        '
        Me.lblCheckin.AutoSize = True
        Me.lblCheckin.Location = New System.Drawing.Point(12, 31)
        Me.lblCheckin.Name = "lblCheckin"
        Me.lblCheckin.Size = New System.Drawing.Size(51, 13)
        Me.lblCheckin.TabIndex = 4
        Me.lblCheckin.Text = "Llegada :"
        '
        'lblLlegada
        '
        Me.lblLlegada.AutoSize = True
        Me.lblLlegada.ForeColor = System.Drawing.Color.Blue
        Me.lblLlegada.Location = New System.Drawing.Point(87, 31)
        Me.lblLlegada.Name = "lblLlegada"
        Me.lblLlegada.Size = New System.Drawing.Size(38, 13)
        Me.lblLlegada.TabIndex = 4
        Me.lblLlegada.Text = "Hotel :"
        '
        'lblCheckout
        '
        Me.lblCheckout.AutoSize = True
        Me.lblCheckout.Location = New System.Drawing.Point(12, 53)
        Me.lblCheckout.Name = "lblCheckout"
        Me.lblCheckout.Size = New System.Drawing.Size(42, 13)
        Me.lblCheckout.TabIndex = 4
        Me.lblCheckout.Text = "Salida :"
        '
        'lblSalida
        '
        Me.lblSalida.AutoSize = True
        Me.lblSalida.ForeColor = System.Drawing.Color.Blue
        Me.lblSalida.Location = New System.Drawing.Point(87, 53)
        Me.lblSalida.Name = "lblSalida"
        Me.lblSalida.Size = New System.Drawing.Size(38, 13)
        Me.lblSalida.TabIndex = 4
        Me.lblSalida.Text = "Hotel :"
        '
        'uce_moneda
        '
        Me.uce_moneda.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_moneda.Location = New System.Drawing.Point(270, 188)
        Me.uce_moneda.Name = "uce_moneda"
        Me.uce_moneda.Size = New System.Drawing.Size(106, 21)
        Me.uce_moneda.TabIndex = 3
        '
        'Uc_habitaciones1
        '
        T_Habitaciones1.TotalAdultos = 1
        T_Habitaciones1.TotalHabitaciones = 1
        T_Habitaciones1.TotalNinos = 0
        Me.Uc_habitaciones1.Habitaciones = T_Habitaciones1
        Me.Uc_habitaciones1.Location = New System.Drawing.Point(7, 101)
        Me.Uc_habitaciones1.Name = "Uc_habitaciones1"
        Me.Uc_habitaciones1.Size = New System.Drawing.Size(275, 88)
        Me.Uc_habitaciones1.TabIndex = 1
        '
        'lblTax
        '
        Me.lblTax.AutoSize = True
        Me.lblTax.Location = New System.Drawing.Point(12, 220)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(56, 13)
        Me.lblTax.TabIndex = 10
        Me.lblTax.Text = "Impuesto :"
        '
        'txtTax
        '
        Me.txtTax.Location = New System.Drawing.Point(90, 217)
        Me.txtTax.MaxLength = 10
        Me.txtTax.Name = "txtTax"
        Me.txtTax.Size = New System.Drawing.Size(58, 20)
        Me.txtTax.TabIndex = 4
        Me.txtTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(154, 220)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(15, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "%"
        '
        'tarifas_personalizadas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(385, 272)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtTax)
        Me.Controls.Add(Me.lblTax)
        Me.Controls.Add(Me.uce_moneda)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lblSalida)
        Me.Controls.Add(Me.lblCheckout)
        Me.Controls.Add(Me.lblLlegada)
        Me.Controls.Add(Me.lblCheckin)
        Me.Controls.Add(Me.lblHotelName)
        Me.Controls.Add(Me.lblHotel)
        Me.Controls.Add(Me.lblTarifa)
        Me.Controls.Add(Me.txtTarifa)
        Me.Controls.Add(Me.cmbTipo)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.Uc_habitaciones1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "tarifas_personalizadas"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Personalizada"
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Uc_habitaciones1 As callCenter.uc_habitaciones
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cmbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents txtTarifa As System.Windows.Forms.TextBox
    Friend WithEvents lblTarifa As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents lblHotel As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents lblHotelName As System.Windows.Forms.Label
    Friend WithEvents lblCheckin As System.Windows.Forms.Label
    Friend WithEvents lblLlegada As System.Windows.Forms.Label
    Friend WithEvents lblCheckout As System.Windows.Forms.Label
    Friend WithEvents lblSalida As System.Windows.Forms.Label
    Friend WithEvents uce_moneda As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents txtTax As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
