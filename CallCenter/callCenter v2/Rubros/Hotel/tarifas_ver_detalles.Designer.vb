﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class tarifas_ver_detalles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("wa")
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.lvDias = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'btn_ok
        '
        Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btn_ok.Location = New System.Drawing.Point(141, 89)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(75, 23)
        Me.btn_ok.TabIndex = 0
        Me.btn_ok.Text = "Aceptar"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'lvDias
        '
        Me.lvDias.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        ListViewItem1.ToolTipText = "w"
        Me.lvDias.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1})
        Me.lvDias.Location = New System.Drawing.Point(2, 1)
        Me.lvDias.Name = "lvDias"
        Me.lvDias.Size = New System.Drawing.Size(360, 82)
        Me.lvDias.TabIndex = 1
        Me.lvDias.UseCompatibleStateImageBehavior = False
        Me.lvDias.View = System.Windows.Forms.View.Details
        '
        'tarifas_ver_detalles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(361, 121)
        Me.Controls.Add(Me.lvDias)
        Me.Controls.Add(Me.btn_ok)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "tarifas_ver_detalles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ver detalles"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents lvDias As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
End Class
