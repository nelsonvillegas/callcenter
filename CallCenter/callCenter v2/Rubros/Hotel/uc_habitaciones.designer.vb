﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_habitaciones
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_especificar = New System.Windows.Forms.Button
        Me.txt_ninos = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_adultos = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_habitaciones = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_habitaciones = New System.Windows.Forms.Label
        Me.lbl_ninos = New System.Windows.Forms.Label
        Me.lbl_adultos = New System.Windows.Forms.Label
        CType(Me.txt_ninos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_adultos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_habitaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_especificar
        '
        Me.btn_especificar.Location = New System.Drawing.Point(189, 2)
        Me.btn_especificar.Name = "btn_especificar"
        Me.btn_especificar.Size = New System.Drawing.Size(75, 23)
        Me.btn_especificar.TabIndex = 14
        Me.btn_especificar.Text = "Especificar"
        Me.btn_especificar.UseVisualStyleBackColor = True
        '
        'txt_ninos
        '
        Me.txt_ninos.Location = New System.Drawing.Point(83, 57)
        Me.txt_ninos.MaxLength = 2
        Me.txt_ninos.Name = "txt_ninos"
        Me.txt_ninos.Size = New System.Drawing.Size(100, 21)
        Me.txt_ninos.TabIndex = 13
        '
        'txt_adultos
        '
        Me.txt_adultos.Location = New System.Drawing.Point(83, 30)
        Me.txt_adultos.MaxLength = 2
        Me.txt_adultos.Name = "txt_adultos"
        Me.txt_adultos.Size = New System.Drawing.Size(100, 21)
        Me.txt_adultos.TabIndex = 12
        '
        'txt_habitaciones
        '
        Me.txt_habitaciones.Location = New System.Drawing.Point(83, 3)
        Me.txt_habitaciones.MaxLength = 2
        Me.txt_habitaciones.Name = "txt_habitaciones"
        Me.txt_habitaciones.Size = New System.Drawing.Size(100, 21)
        Me.txt_habitaciones.TabIndex = 11
        '
        'lbl_habitaciones
        '
        Me.lbl_habitaciones.AutoSize = True
        Me.lbl_habitaciones.Location = New System.Drawing.Point(5, 7)
        Me.lbl_habitaciones.Name = "lbl_habitaciones"
        Me.lbl_habitaciones.Size = New System.Drawing.Size(72, 13)
        Me.lbl_habitaciones.TabIndex = 10
        Me.lbl_habitaciones.Text = "Habitaciones:"
        '
        'lbl_ninos
        '
        Me.lbl_ninos.AutoSize = True
        Me.lbl_ninos.Location = New System.Drawing.Point(5, 61)
        Me.lbl_ninos.Name = "lbl_ninos"
        Me.lbl_ninos.Size = New System.Drawing.Size(37, 13)
        Me.lbl_ninos.TabIndex = 9
        Me.lbl_ninos.Text = "Niños:"
        '
        'lbl_adultos
        '
        Me.lbl_adultos.AutoSize = True
        Me.lbl_adultos.Location = New System.Drawing.Point(5, 34)
        Me.lbl_adultos.Name = "lbl_adultos"
        Me.lbl_adultos.Size = New System.Drawing.Size(45, 13)
        Me.lbl_adultos.TabIndex = 8
        Me.lbl_adultos.Text = "Adultos:"
        '
        'uc_habitaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btn_especificar)
        Me.Controls.Add(Me.txt_ninos)
        Me.Controls.Add(Me.txt_adultos)
        Me.Controls.Add(Me.txt_habitaciones)
        Me.Controls.Add(Me.lbl_habitaciones)
        Me.Controls.Add(Me.lbl_ninos)
        Me.Controls.Add(Me.lbl_adultos)
        Me.Name = "uc_habitaciones"
        Me.Size = New System.Drawing.Size(275, 88)
        CType(Me.txt_ninos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_adultos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_habitaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_especificar As System.Windows.Forms.Button
    Friend WithEvents txt_ninos As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_adultos As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_habitaciones As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_habitaciones As System.Windows.Forms.Label
    Friend WithEvents lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents lbl_adultos As System.Windows.Forms.Label

End Class
