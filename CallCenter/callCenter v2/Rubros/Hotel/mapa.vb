Public Class mapa

    Public datosBusqueda_hotel As dataBusqueda_hotel_test

    Private Sub mapa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
        CapaPresentacion.Idiomas.cambiar_mapa(Me)
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        Close()
    End Sub

    Private Sub load_data()
        Try
            Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOD(datosBusqueda_hotel)

            If ds Is Nothing Then
                pic_GRANDE.Image = Nothing
                txtInfo.Text = ""
                Return
            End If

            If Not ds.Tables("error") Is Nothing Then
                Dim r As DataRow = ds.Tables("error").Rows(0)
                MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Cursor = Cursors.Arrow
            Else
                Dim titVal_mapa() As String = herramientas.obten_TitleValue(ds, "Image", "LOCA")
                Dim titVal_desc() As String = herramientas.obten_TitleValue(ds, "Description", "LOCA")

                pic_GRANDE.Image = herramientas.Load_Web_Image(titVal_mapa(1), Global.callCenter.My.Resources.Resources.hotel)

                If pic_GRANDE.Image.Height < pic_GRANDE.Height And pic_GRANDE.Image.Width < pic_GRANDE.Width Then
                    pic_GRANDE.SizeMode = PictureBoxSizeMode.CenterImage
                Else
                    pic_GRANDE.SizeMode = PictureBoxSizeMode.Zoom
                End If

                txtInfo.Text = titVal_desc(0) + vbCrLf + titVal_desc(1)
                If ds.Tables.IndexOf("GeneralInformation") <> -1 Then
                    If ds.Tables("GeneralInformation").Rows.Count > 0 Then
                        txtInfo.Text &= vbCrLf & "Tel :" & ds.Tables("GeneralInformation").Rows(0).Item("Phone")
                        txtInfo.Text &= vbCrLf & "Fax :" & ds.Tables("GeneralInformation").Rows(0).Item("Fax")
                        Dim path As String = CapaAccesoDatos.XML.DataAplicationPath + "\Mapa.htm"
                        Try
                            Dim content As String = IO.File.ReadAllText(path)
                            content = content.Replace("[LAT]", ds.Tables("GeneralInformation").Rows(0).Item("Latitude"))
                            content = content.Replace("[LON]", ds.Tables("GeneralInformation").Rows(0).Item("Longitude"))
                            content = content.Replace("[NAME]", ds.Tables("GeneralInformation").Rows(0).Item("Name"))
                            content = content.Replace("[ADDR]", "<br />" + ds.Tables("GeneralInformation").Rows(0).Item("Address") + "<br />" + ds.Tables("GeneralInformation").Rows(0).Item("CityName") + "," + ds.Tables("GeneralInformation").Rows(0).Item("CountryName"))
                            Me.WebBrowser1.DocumentText = content
                            Me.WebBrowser1.ScriptErrorsSuppressed = True
                        Catch ex As Exception

                        End Try


                    End If
                End If
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0025", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Sub

End Class