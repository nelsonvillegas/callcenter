<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class _uc_detallesReservacion
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.data_lbl_itinerario = New System.Windows.Forms.Label
        Me.lbl_itinerario = New System.Windows.Forms.Label
        Me.data_lbl_estado = New System.Windows.Forms.Label
        Me.lbl_estado = New System.Windows.Forms.Label
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.ResId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Rubro = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PropertyGrid1 = New System.Windows.Forms.PropertyGrid
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'data_lbl_itinerario
        '
        Me.data_lbl_itinerario.AutoSize = True
        Me.data_lbl_itinerario.Location = New System.Drawing.Point(107, 12)
        Me.data_lbl_itinerario.Name = "data_lbl_itinerario"
        Me.data_lbl_itinerario.Size = New System.Drawing.Size(37, 13)
        Me.data_lbl_itinerario.TabIndex = 30
        Me.data_lbl_itinerario.Text = "----------"
        '
        'lbl_itinerario
        '
        Me.lbl_itinerario.AutoSize = True
        Me.lbl_itinerario.Location = New System.Drawing.Point(3, 12)
        Me.lbl_itinerario.Name = "lbl_itinerario"
        Me.lbl_itinerario.Size = New System.Drawing.Size(50, 13)
        Me.lbl_itinerario.TabIndex = 29
        Me.lbl_itinerario.Text = "Itinerario:"
        '
        'data_lbl_estado
        '
        Me.data_lbl_estado.AutoSize = True
        Me.data_lbl_estado.Location = New System.Drawing.Point(107, 25)
        Me.data_lbl_estado.Name = "data_lbl_estado"
        Me.data_lbl_estado.Size = New System.Drawing.Size(37, 13)
        Me.data_lbl_estado.TabIndex = 32
        Me.data_lbl_estado.Text = "----------"
        '
        'lbl_estado
        '
        Me.lbl_estado.AutoSize = True
        Me.lbl_estado.Location = New System.Drawing.Point(3, 25)
        Me.lbl_estado.Name = "lbl_estado"
        Me.lbl_estado.Size = New System.Drawing.Size(43, 13)
        Me.lbl_estado.TabIndex = 31
        Me.lbl_estado.Text = "Estado:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ResId, Me.Rubro, Me.Estado})
        Me.DataGridView1.Location = New System.Drawing.Point(3, 51)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(249, 127)
        Me.DataGridView1.TabIndex = 33
        '
        'ResId
        '
        Me.ResId.HeaderText = "ResId"
        Me.ResId.Name = "ResId"
        Me.ResId.ReadOnly = True
        Me.ResId.Visible = False
        '
        'Rubro
        '
        Me.Rubro.HeaderText = "Rubro"
        Me.Rubro.Name = "Rubro"
        Me.Rubro.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'PropertyGrid1
        '
        Me.PropertyGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PropertyGrid1.Location = New System.Drawing.Point(3, 184)
        Me.PropertyGrid1.Name = "PropertyGrid1"
        Me.PropertyGrid1.Size = New System.Drawing.Size(249, 258)
        Me.PropertyGrid1.TabIndex = 34
        '
        'uc_detallesReservacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.PropertyGrid1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.data_lbl_estado)
        Me.Controls.Add(Me.lbl_estado)
        Me.Controls.Add(Me.data_lbl_itinerario)
        Me.Controls.Add(Me.lbl_itinerario)
        Me.Name = "uc_detallesReservacion"
        Me.Size = New System.Drawing.Size(255, 445)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents data_lbl_itinerario As System.Windows.Forms.Label
    Friend WithEvents lbl_itinerario As System.Windows.Forms.Label
    Friend WithEvents data_lbl_estado As System.Windows.Forms.Label
    Friend WithEvents lbl_estado As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PropertyGrid1 As System.Windows.Forms.PropertyGrid
    Friend WithEvents ResId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rubro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
