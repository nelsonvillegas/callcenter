Public Class galeria

    Public datosBusqueda_hotel As dataBusqueda_hotel_test

    Private Sub galeria_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
        CapaPresentacion.Idiomas.cambiar_galeria(Me)
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        Close()
    End Sub

    Private Sub load_data()
        Try
            Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOD(datosBusqueda_hotel)

            If ds Is Nothing Then
                Return
            End If

            If Not ds.Tables("error") Is Nothing Then
                Dim r As DataRow = ds.Tables("error").Rows(0)
                MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Cursor = Cursors.Arrow
            Else
                If ds.Tables.Contains("Image") Then
                    Dim MyThread As New System.Threading.Thread(AddressOf descarga_imagenes)
                    MyThread.Start(ds)
                End If
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0021", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Sub

    Private Sub pic_click(ByVal sender As Object, ByVal e As EventArgs)
        pic_GRANDE.Image = CType(sender, PictureBox).Image

        If pic_GRANDE.Image.Height < pic_GRANDE.Height And pic_GRANDE.Image.Width < pic_GRANDE.Width Then
            pic_GRANDE.SizeMode = PictureBoxSizeMode.CenterImage
        Else
            pic_GRANDE.SizeMode = PictureBoxSizeMode.Zoom
        End If
    End Sub

    '---------------------------------multithread

    Private Sub descarga_imagenes(ByVal ds_o As Object)
        Try
            Dim ds As DataSet = CType(ds_o, DataSet)
            For Each r_img As DataRow In ds.Tables("Image").Rows
                If r_img.Item("keyword").ToString.ToUpper <> "HIMG" Then
                    Continue For
                End If

                Dim MyThread As New System.Threading.Thread(AddressOf get_un_pic)
                MyThread.Start(r_img.Item("value"))
            Next
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0022", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub get_un_pic(ByVal dir As Object)
        Dim pic As New PictureBox
        pic.Image = herramientas.Load_Web_Image(CStr(dir), Nothing)
        pic.Size = New Point(70, 70)
        pic.SizeMode = PictureBoxSizeMode.Zoom
        pic.Cursor = Cursors.Hand
        AddHandler pic.Click, AddressOf pic_click

        If pic.Image Is Nothing Then
            Return
        End If

        FijaImagen(pic)
        FijaPrimera(pic)
    End Sub

    Delegate Sub FijaImagenCallback(ByVal pic As PictureBox)
    Delegate Sub FijaPrimeraCallback(ByVal pic As PictureBox)

    Private Sub FijaImagen(ByVal pic As PictureBox)
        Try
            If UltraPanel1.InvokeRequired Then
                Dim d As New FijaImagenCallback(AddressOf FijaImagen)
                Me.Invoke(d, New Object() {pic})
            Else
                UltraPanel1.ClientArea.Controls.Add(pic)
            End If
        Catch ex As Exception
            '
        End Try
    End Sub

    Private Sub FijaPrimera(ByVal pic As PictureBox)
        Try
            If pic_GRANDE.InvokeRequired Then
                Dim d As New FijaPrimeraCallback(AddressOf FijaPrimera)
                Me.Invoke(d, New Object() {pic})
            Else
                If pic_GRANDE.Image Is Nothing Then pic_GRANDE.Image = pic.Image
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0023", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

End Class