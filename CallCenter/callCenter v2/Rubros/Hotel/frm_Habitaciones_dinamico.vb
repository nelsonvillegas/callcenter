﻿Public Class frm_Habitaciones_dinamico

    Private _HabitacionesSettings As New uc_habitaciones.T_HabitacionSettings
    Private Temp_Habitaciones As List(Of uc_habitaciones.T_Habitacion)


    Public ReadOnly Property TotalHabitaciones() As Integer
        Get
            Dim tot As Integer = 0
            For x As Integer = 1 To Me._HabitacionesSettings.Max_Habitaciones
                If CType(Panel1.Controls("chkHab" & x), CheckBox).Checked = False Then
                    Exit For
                Else
                    tot = x
                End If
            Next
            Return tot
        End Get

    End Property
    Public ReadOnly Property TotalAdultos() As Integer
        Get
            Dim tot As Integer = 0
            For x As Integer = 1 To Me._HabitacionesSettings.Max_Habitaciones
                If CType(Panel1.Controls("chkHab" & x), CheckBox).Checked = False Then
                    Exit For
                Else
                    Dim obj As ComboBox = CType(Panel1.Controls("cmbAdultos" & x), ComboBox)
                    tot += obj.SelectedItem
                End If
            Next
            Return tot
        End Get
    End Property
    Public ReadOnly Property TotalNinos() As Integer
        Get
            Dim tot As Integer = 0
            For x As Integer = 1 To Me._HabitacionesSettings.Max_Habitaciones
                If CType(Panel1.Controls("chkHab" & x), CheckBox).Checked = False Then
                    Exit For
                Else
                    Dim obj As ComboBox = CType(Panel1.Controls("cmbNiños" & x), ComboBox)
                    tot += obj.SelectedItem
                End If
            Next
            Return tot
        End Get
    End Property

    Public Property Habitaciones() As List(Of uc_habitaciones.T_Habitacion)
        Get
            Dim lista As New List(Of uc_habitaciones.T_Habitacion)
            For x As Integer = 1 To Me._HabitacionesSettings.Max_Habitaciones
                If CType(Panel1.Controls("chkHab" & x), CheckBox).Checked Then
                    Dim hab As New uc_habitaciones.T_Habitacion
                    hab.NumAdultos = CInt(CType(Panel1.Controls("cmbAdultos" & x), ComboBox).Text)
                    hab.NumNinos = CInt(CType(Panel1.Controls("cmbniños" & x), ComboBox).Text)
                    For y As Integer = 1 To hab.NumNinos
                        hab.EdadesNinos.Add(CInt(CType(Panel2.Controls("cmbRoom" & x & "Age" & y), ComboBox).Text))
                    Next
                    lista.Add(hab)
                End If
            Next

            Return lista
        End Get
        Set(ByVal value As List(Of uc_habitaciones.T_Habitacion))
            For xx As Integer = 1 To Me._HabitacionesSettings.Max_Habitaciones
                Dim x As Integer
                x = xx - 1
                If x < value.Count Then
                    CType(Panel1.Controls("chkHab" & xx), CheckBox).Checked = True
                    CType(Panel1.Controls("cmbAdultos" & xx), ComboBox).SelectedItem = value(x).NumAdultos
                    CType(Panel1.Controls("cmbNiños" & xx), ComboBox).SelectedItem = value(x).NumNinos
                    'For y As Integer = 0 To value(x).NumNinos - 1 'se comento por que la edad de los niños nunca se asigna en el list EdadesNinos
                    '    CType(Panel2.Controls("cmbRoom" & xx & "Age" & y + 1), ComboBox).SelectedIndex = value(x).EdadesNinos(y) - 1
                    'Next
                Else
                    CType(Panel1.Controls("chkHab" & xx), CheckBox).Checked = False
                End If
            Next
        End Set
    End Property

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Temp_Habitaciones = Habitaciones
        chk_habitacion_1.Focus()
        chk_habitacion_1.Checked = True
        CapaPresentacion.Idiomas.cambiar_frmHabitaciones(Me) 'recursos

    End Sub

    Private Sub llena_cmb(ByVal cmb As ComboBox, ByVal min As Integer, ByVal max As Integer)
        cmb.Items.Clear()
        For i As Integer = min To max
            cmb.Items.Add(i)
        Next

        If cmb.Items.Count > 0 Then cmb.SelectedIndex = 0
    End Sub

    Private Sub btn_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Cancelar.Click
        Habitaciones = Temp_Habitaciones
        Close()
    End Sub

    Private Sub btn_Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Aceptar.Click
        Close()
    End Sub

    Public Sub CreaControles(ByVal adultos As Integer, ByVal niños As Integer, Optional ByVal habitaciones As Integer = -1)
        Dim info As New uc_habitaciones.T_HabitacionSettings
        Dim hab As Integer = habitaciones
        If hab = -1 Then hab = 1

        Dim arr() As Integer = herramientas.get_arrCuartos(hab, adultos)
        Dim arr2() As Integer = herramientas.get_arrCuartos(hab, niños)

        For x As Integer = 1 To info.Max_NinosHabitacion
            Dim _obj1 As New Label
            _obj1.Name = "lblEnc" & x
            _obj1.Text = CapaPresentacion.Idiomas.get_str("str_319_nino") & " " & x
            _obj1.Top = 0
            _obj1.Width = 60
            _obj1.Left = 120 + (70 * (x - 1))
            Panel2.Controls.Add(_obj1)
        Next

        For x As Integer = 1 To info.Max_Habitaciones ' habitaciones
            Dim obj1 As CheckBox = New CheckBox
            obj1.Name = "chkHab" & x
            obj1.Text = CapaPresentacion.Idiomas.get_str("str_318_habitacion") & " " & x
            obj1.Top = 25 * (x - 1)
            obj1.Left = 10
            If x > 1 Then obj1.Enabled = False
            AddHandler obj1.CheckedChanged, AddressOf CheckedChanged
            Panel1.Controls.Add(obj1)
            Dim obj2 As ComboBox = New ComboBox
            obj2.DropDownStyle = ComboBoxStyle.DropDownList
            obj2.Name = "cmbAdultos" & x
            obj2.Text = ""
            obj2.Top = 25 * (x - 1)
            obj2.Left = 174
            obj2.Width = 39
            obj2.Enabled = False
            llena_cmb(obj2, info.Min_AdultosHabitacion, info.Max_AdultosHabitacion)
            obj2.SelectedValue = 0
            If x < arr.Length Then obj2.SelectedValue = arr(x - 1)
            Panel1.Controls.Add(obj2)
            Dim obj3 As ComboBox = New ComboBox
            obj3.DropDownStyle = ComboBoxStyle.DropDownList
            obj3.Name = "cmbNiños" & x
            obj3.Text = ""
            obj3.Top = 25 * (x - 1)
            obj3.Left = 264
            obj3.Width = 39
            obj3.Enabled = False
            llena_cmb(obj3, info.Min_NinosHasbitacion, info.Max_NinosHabitacion)
            If x < arr2.Length Then obj3.SelectedValue = arr2(x - 1)
            AddHandler obj3.SelectedIndexChanged, AddressOf CMB_SelectedIndexChanged
            Panel1.Controls.Add(obj3)
            '2do panel
            Dim _obj1 As New Label
            _obj1.Name = "lblHab" & x
            _obj1.Text = CapaPresentacion.Idiomas.get_str("str_318_habitacion") & " " & x
            _obj1.Top = 25 * (x)
            _obj1.Left = 10
            Panel2.Controls.Add(_obj1)
            For y As Integer = 1 To info.Max_NinosHabitacion
                Dim _obj2 As ComboBox = New ComboBox
                _obj2.DropDownStyle = ComboBoxStyle.DropDownList
                _obj2.Name = "cmbRoom" & x & "Age" & y
                _obj2.Text = ""
                _obj2.Top = 25 * (x)
                _obj2.Left = 120 + (70 * (y - 1))
                _obj2.Width = 39
                _obj2.Enabled = False
                llena_cmb(_obj2, info.Min_EdadNinos, info.Max_EdadNinos)
                _obj2.SelectedIndex = 0
                Panel2.Controls.Add(_obj2)
            Next
        Next
    End Sub

    Private Sub CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim index As Integer = CInt(chk.Name.Replace("chkHab", ""))
        CType(Panel1.Controls("cmbAdultos" & index), ComboBox).Enabled = chk.Checked
        CType(Panel1.Controls("cmbniños" & index), ComboBox).Enabled = chk.Checked
        CType(Panel1.Controls("cmbniños" & index), ComboBox).SelectedIndex = 0
        For x As Integer = 1 To Me._HabitacionesSettings.Max_Habitaciones
            If CType(Panel1.Controls("chkHab" & index), CheckBox).Checked Then
                If x > (index + 1) Then
                    CType(Panel1.Controls("chkHab" & x), CheckBox).Checked = False
                    CType(Panel1.Controls("chkHab" & x), CheckBox).Enabled = False
                    CType(Panel1.Controls("cmbNiños" & x), ComboBox).SelectedIndex = 0
                Else
                    CType(Panel1.Controls("chkHab" & x), CheckBox).Enabled = True
                End If
            Else
                If x > (index) Then
                    CType(Panel1.Controls("chkHab" & x), CheckBox).Checked = False
                    CType(Panel1.Controls("cmbNiños" & x), ComboBox).SelectedIndex = 0
                    CType(Panel1.Controls("chkHab" & x), CheckBox).Enabled = False
                End If
            End If
        Next x
        'habilitar combos edades

        If Not CType(Panel1.Controls("chkHab1"), CheckBox).Checked Then
            CType(Panel1.Controls("chkHab1"), CheckBox).Checked = True
        Else
            If Me.TotalHabitaciones > 1 Then
                CType(Panel1.Controls("chkHab2"), CheckBox).Enabled = True
            End If
        End If
    End Sub

    Private Sub CMB_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cmb As ComboBox = CType(sender, ComboBox)
        If cmb.Text = "" Then Return
        Dim index As Integer = CInt(cmb.Name.Replace("cmbNiños", ""))
        For y As Integer = 1 To _HabitacionesSettings.Max_NinosHabitacion
            If y <= CInt(cmb.Text) Then
                CType(Panel2.Controls("cmbRoom" & index & "Age" & y), ComboBox).Enabled = True
            Else
                CType(Panel2.Controls("cmbRoom" & index & "Age" & y), ComboBox).Enabled = False
            End If

        Next
    End Sub

End Class