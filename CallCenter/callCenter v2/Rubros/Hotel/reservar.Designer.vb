<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reservar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reservar))
        Dim T_Habitaciones1 As callCenter.uc_habitaciones.T_Habitaciones = New callCenter.uc_habitaciones.T_Habitaciones()
        Dim UltraTab3 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab()
        Me.UltraTabPageControl3 = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl()
        Me.lbl_mensaje = New System.Windows.Forms.Label()
        Me.pnlFrecuentCode = New System.Windows.Forms.Panel()
        Me.btnFrecCerrar = New System.Windows.Forms.Button()
        Me.lblFrec_Name = New System.Windows.Forms.Label()
        Me.lblFrec_Code = New System.Windows.Forms.Label()
        Me.btnBuscarSocio = New System.Windows.Forms.Button()
        Me.txtFrecuentCode = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.btnAltaSocio = New System.Windows.Forms.Button()
        Me.lblFrecuent = New System.Windows.Forms.Label()
        Me.txt_comentario = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.pnlCatalogos = New System.Windows.Forms.Panel()
        Me.CmbSearch1 = New callCenter.cmbSearch()
        Me.lblSegmento = New System.Windows.Forms.Label()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.lblMedio = New System.Windows.Forms.Label()
        Me.cmbSegmento = New System.Windows.Forms.ComboBox()
        Me.cmbMedios = New System.Windows.Forms.ComboBox()
        Me.txt_peticion = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.data_lbl_habitaciones = New System.Windows.Forms.Label()
        Me.data_lbl_ninos = New System.Windows.Forms.Label()
        Me.data_lbl_adultos = New System.Windows.Forms.Label()
        Me.lbl_habitaciones = New System.Windows.Forms.Label()
        Me.lbl_ninos = New System.Windows.Forms.Label()
        Me.lbl_adultos = New System.Windows.Forms.Label()
        Me.data_lbl_in = New System.Windows.Forms.Label()
        Me.data_lbl_out = New System.Windows.Forms.Label()
        Me.lblConvenioData = New System.Windows.Forms.Label()
        Me.data_lbl_codProm = New System.Windows.Forms.Label()
        Me.data_lbl_hotel = New System.Windows.Forms.Label()
        Me.data_lbl_cadena = New System.Windows.Forms.Label()
        Me.gpb_nuevaBusqueda = New System.Windows.Forms.GroupBox()
        Me.btn_actualizar = New System.Windows.Forms.Button()
        Me.Uc_habitaciones1 = New callCenter.uc_habitaciones()
        Me.dtp_out = New System.Windows.Forms.DateTimePicker()
        Me.lbl_salida = New System.Windows.Forms.Label()
        Me.lbl_llegada = New System.Windows.Forms.Label()
        Me.dtp_in = New System.Windows.Forms.DateTimePicker()
        Me.WebBrowser2 = New System.Windows.Forms.WebBrowser()
        Me.lbl_llegada2 = New System.Windows.Forms.Label()
        Me.lbl_salida2 = New System.Windows.Forms.Label()
        Me.lbl_info_personasExtra = New System.Windows.Forms.Label()
        Me.lbl_info_total = New System.Windows.Forms.Label()
        Me.btn_cambiar = New System.Windows.Forms.Button()
        Me.lbl_info_impuestos = New System.Windows.Forms.Label()
        Me.btn_reglas = New System.Windows.Forms.Button()
        Me.lbl_info_subtotal = New System.Windows.Forms.Label()
        Me.lblConvenio = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.txtInfoDeposito = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lblComentariodep = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.txtDeposito = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.chkDeposito = New System.Windows.Forms.CheckBox()
        Me.pnlDeposito = New System.Windows.Forms.Panel()
        Me.lblDep = New System.Windows.Forms.Label()
        Me.dtp_DepHora = New System.Windows.Forms.DateTimePicker()
        Me.dtp_DepFecha = New System.Windows.Forms.DateTimePicker()
        Me.txtOtra = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbt_garantia = New System.Windows.Forms.RadioButton()
        Me.rbt_deposito = New System.Windows.Forms.RadioButton()
        Me.rbt_otra = New System.Windows.Forms.RadioButton()
        Me.rbt_ninguno = New System.Windows.Forms.RadioButton()
        Me.rbt_depositoBancario = New System.Windows.Forms.RadioButton()
        Me.rbt_tarjetaCredito = New System.Windows.Forms.RadioButton()
        Me.lbl_codProm = New System.Windows.Forms.Label()
        Me.lbl_info_tarifa = New System.Windows.Forms.Label()
        Me.txt_codigoTarifa = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lblComentario = New System.Windows.Forms.Label()
        Me.lbl_peticion = New System.Windows.Forms.Label()
        Me.lbl_Hotel = New System.Windows.Forms.Label()
        Me.lbl_codigoTarifa = New System.Windows.Forms.Label()
        Me.lbl_cadena = New System.Windows.Forms.Label()
        Me.btn_reservar = New System.Windows.Forms.Button()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.UltraTabSharedControlsPage1 = New Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage()
        Me.UltraTabControl1 = New Infragistics.Win.UltraWinTabControl.UltraTabControl()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btn_imprimir = New System.Windows.Forms.Button()
        Me.btnCotizar = New System.Windows.Forms.Button()
        Me.gbCorreo = New System.Windows.Forms.GroupBox()
        Me.txtCorreo = New System.Windows.Forms.TextBox()
        Me.btn_correo = New System.Windows.Forms.Button()
        Me.btnConfirm = New System.Windows.Forms.Button()
        Me.UltraTabPageControl3.SuspendLayout()
        Me.pnlFrecuentCode.SuspendLayout()
        CType(Me.txtFrecuentCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_comentario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCatalogos.SuspendLayout()
        CType(Me.txt_peticion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpb_nuevaBusqueda.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtInfoDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDeposito.SuspendLayout()
        CType(Me.txtOtra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txt_codigoTarifa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabControl1.SuspendLayout()
        Me.gbCorreo.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraTabPageControl3
        '
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_mensaje)
        Me.UltraTabPageControl3.Controls.Add(Me.pnlFrecuentCode)
        Me.UltraTabPageControl3.Controls.Add(Me.btnBuscarSocio)
        Me.UltraTabPageControl3.Controls.Add(Me.txtFrecuentCode)
        Me.UltraTabPageControl3.Controls.Add(Me.btnAltaSocio)
        Me.UltraTabPageControl3.Controls.Add(Me.lblFrecuent)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_comentario)
        Me.UltraTabPageControl3.Controls.Add(Me.pnlCatalogos)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_peticion)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_habitaciones)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_ninos)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_adultos)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_habitaciones)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_ninos)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_adultos)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_in)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_out)
        Me.UltraTabPageControl3.Controls.Add(Me.lblConvenioData)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_codProm)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_hotel)
        Me.UltraTabPageControl3.Controls.Add(Me.data_lbl_cadena)
        Me.UltraTabPageControl3.Controls.Add(Me.gpb_nuevaBusqueda)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_llegada2)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_salida2)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_info_personasExtra)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_info_total)
        Me.UltraTabPageControl3.Controls.Add(Me.btn_cambiar)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_info_impuestos)
        Me.UltraTabPageControl3.Controls.Add(Me.btn_reglas)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_info_subtotal)
        Me.UltraTabPageControl3.Controls.Add(Me.lblConvenio)
        Me.UltraTabPageControl3.Controls.Add(Me.GroupBox1)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_codProm)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_info_tarifa)
        Me.UltraTabPageControl3.Controls.Add(Me.txt_codigoTarifa)
        Me.UltraTabPageControl3.Controls.Add(Me.lblComentario)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_peticion)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_Hotel)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_codigoTarifa)
        Me.UltraTabPageControl3.Controls.Add(Me.lbl_cadena)
        Me.UltraTabPageControl3.Location = New System.Drawing.Point(1, 22)
        Me.UltraTabPageControl3.Name = "UltraTabPageControl3"
        Me.UltraTabPageControl3.Size = New System.Drawing.Size(923, 398)
        '
        'lbl_mensaje
        '
        Me.lbl_mensaje.AutoSize = True
        Me.lbl_mensaje.BackColor = System.Drawing.Color.Transparent
        Me.lbl_mensaje.ForeColor = System.Drawing.Color.Red
        Me.lbl_mensaje.Location = New System.Drawing.Point(586, 206)
        Me.lbl_mensaje.Name = "lbl_mensaje"
        Me.lbl_mensaje.Size = New System.Drawing.Size(10, 13)
        Me.lbl_mensaje.TabIndex = 23
        Me.lbl_mensaje.Text = "."
        '
        'pnlFrecuentCode
        '
        Me.pnlFrecuentCode.Controls.Add(Me.btnFrecCerrar)
        Me.pnlFrecuentCode.Controls.Add(Me.lblFrec_Name)
        Me.pnlFrecuentCode.Controls.Add(Me.lblFrec_Code)
        Me.pnlFrecuentCode.Location = New System.Drawing.Point(667, 242)
        Me.pnlFrecuentCode.Name = "pnlFrecuentCode"
        Me.pnlFrecuentCode.Size = New System.Drawing.Size(254, 45)
        Me.pnlFrecuentCode.TabIndex = 48
        Me.pnlFrecuentCode.Visible = False
        '
        'btnFrecCerrar
        '
        Me.btnFrecCerrar.Image = CType(resources.GetObject("btnFrecCerrar.Image"), System.Drawing.Image)
        Me.btnFrecCerrar.Location = New System.Drawing.Point(222, 1)
        Me.btnFrecCerrar.Name = "btnFrecCerrar"
        Me.btnFrecCerrar.Size = New System.Drawing.Size(28, 23)
        Me.btnFrecCerrar.TabIndex = 48
        Me.btnFrecCerrar.UseVisualStyleBackColor = True
        '
        'lblFrec_Name
        '
        Me.lblFrec_Name.AutoSize = True
        Me.lblFrec_Name.BackColor = System.Drawing.Color.Transparent
        Me.lblFrec_Name.Location = New System.Drawing.Point(3, 24)
        Me.lblFrec_Name.Name = "lblFrec_Name"
        Me.lblFrec_Name.Size = New System.Drawing.Size(77, 13)
        Me.lblFrec_Name.TabIndex = 46
        Me.lblFrec_Name.Text = "Frecuent Code"
        '
        'lblFrec_Code
        '
        Me.lblFrec_Code.AutoSize = True
        Me.lblFrec_Code.BackColor = System.Drawing.Color.Transparent
        Me.lblFrec_Code.Location = New System.Drawing.Point(3, 6)
        Me.lblFrec_Code.Name = "lblFrec_Code"
        Me.lblFrec_Code.Size = New System.Drawing.Size(77, 13)
        Me.lblFrec_Code.TabIndex = 45
        Me.lblFrec_Code.Text = "Frecuent Code"
        '
        'btnBuscarSocio
        '
        Me.btnBuscarSocio.Image = CType(resources.GetObject("btnBuscarSocio.Image"), System.Drawing.Image)
        Me.btnBuscarSocio.Location = New System.Drawing.Point(832, 293)
        Me.btnBuscarSocio.Name = "btnBuscarSocio"
        Me.btnBuscarSocio.Size = New System.Drawing.Size(38, 23)
        Me.btnBuscarSocio.TabIndex = 14
        Me.btnBuscarSocio.UseVisualStyleBackColor = True
        '
        'txtFrecuentCode
        '
        Me.txtFrecuentCode.AlwaysInEditMode = True
        Me.txtFrecuentCode.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtFrecuentCode.Location = New System.Drawing.Point(666, 241)
        Me.txtFrecuentCode.MaxLength = 30
        Me.txtFrecuentCode.Name = "txtFrecuentCode"
        Me.txtFrecuentCode.Size = New System.Drawing.Size(254, 19)
        Me.txtFrecuentCode.TabIndex = 13
        '
        'btnAltaSocio
        '
        Me.btnAltaSocio.Image = CType(resources.GetObject("btnAltaSocio.Image"), System.Drawing.Image)
        Me.btnAltaSocio.Location = New System.Drawing.Point(877, 293)
        Me.btnAltaSocio.Name = "btnAltaSocio"
        Me.btnAltaSocio.Size = New System.Drawing.Size(38, 23)
        Me.btnAltaSocio.TabIndex = 15
        Me.btnAltaSocio.UseVisualStyleBackColor = True
        '
        'lblFrecuent
        '
        Me.lblFrecuent.AutoSize = True
        Me.lblFrecuent.BackColor = System.Drawing.Color.Transparent
        Me.lblFrecuent.Location = New System.Drawing.Point(664, 224)
        Me.lblFrecuent.Name = "lblFrecuent"
        Me.lblFrecuent.Size = New System.Drawing.Size(77, 13)
        Me.lblFrecuent.TabIndex = 44
        Me.lblFrecuent.Text = "Frecuent Code"
        '
        'txt_comentario
        '
        Me.txt_comentario.AlwaysInEditMode = True
        Me.txt_comentario.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_comentario.Location = New System.Drawing.Point(392, 325)
        Me.txt_comentario.MaxLength = 200
        Me.txt_comentario.Multiline = True
        Me.txt_comentario.Name = "txt_comentario"
        Me.txt_comentario.Scrollbars = System.Windows.Forms.ScrollBars.Vertical
        Me.txt_comentario.Size = New System.Drawing.Size(261, 71)
        Me.txt_comentario.TabIndex = 12
        '
        'pnlCatalogos
        '
        Me.pnlCatalogos.BackColor = System.Drawing.Color.Transparent
        Me.pnlCatalogos.Controls.Add(Me.CmbSearch1)
        Me.pnlCatalogos.Controls.Add(Me.lblSegmento)
        Me.pnlCatalogos.Controls.Add(Me.lblEmpresa)
        Me.pnlCatalogos.Controls.Add(Me.lblMedio)
        Me.pnlCatalogos.Controls.Add(Me.cmbSegmento)
        Me.pnlCatalogos.Controls.Add(Me.cmbMedios)
        Me.pnlCatalogos.Location = New System.Drawing.Point(664, 244)
        Me.pnlCatalogos.Name = "pnlCatalogos"
        Me.pnlCatalogos.Size = New System.Drawing.Size(256, 120)
        Me.pnlCatalogos.TabIndex = 16
        '
        'CmbSearch1
        '
        Me.CmbSearch1.Location = New System.Drawing.Point(3, -54)
        Me.CmbSearch1.Name = "CmbSearch1"
        Me.CmbSearch1.Size = New System.Drawing.Size(249, 19)
        Me.CmbSearch1.TabIndex = 1
        '
        'lblSegmento
        '
        Me.lblSegmento.AutoSize = True
        Me.lblSegmento.BackColor = System.Drawing.Color.Transparent
        Me.lblSegmento.Location = New System.Drawing.Point(0, 9)
        Me.lblSegmento.Name = "lblSegmento"
        Me.lblSegmento.Size = New System.Drawing.Size(55, 13)
        Me.lblSegmento.TabIndex = 41
        Me.lblSegmento.Text = "Segmento"
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpresa.Location = New System.Drawing.Point(0, -27)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(48, 13)
        Me.lblEmpresa.TabIndex = 42
        Me.lblEmpresa.Text = "Empresa"
        '
        'lblMedio
        '
        Me.lblMedio.AutoSize = True
        Me.lblMedio.BackColor = System.Drawing.Color.Transparent
        Me.lblMedio.Location = New System.Drawing.Point(0, 76)
        Me.lblMedio.Name = "lblMedio"
        Me.lblMedio.Size = New System.Drawing.Size(36, 13)
        Me.lblMedio.TabIndex = 43
        Me.lblMedio.Text = "Medio"
        '
        'cmbSegmento
        '
        Me.cmbSegmento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSegmento.FormattingEnabled = True
        Me.cmbSegmento.Location = New System.Drawing.Point(3, 52)
        Me.cmbSegmento.Name = "cmbSegmento"
        Me.cmbSegmento.Size = New System.Drawing.Size(178, 21)
        Me.cmbSegmento.TabIndex = 2
        '
        'cmbMedios
        '
        Me.cmbMedios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMedios.FormattingEnabled = True
        Me.cmbMedios.Location = New System.Drawing.Point(3, 89)
        Me.cmbMedios.Name = "cmbMedios"
        Me.cmbMedios.Size = New System.Drawing.Size(178, 21)
        Me.cmbMedios.TabIndex = 3
        '
        'txt_peticion
        '
        Me.txt_peticion.AlwaysInEditMode = True
        Me.txt_peticion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_peticion.Location = New System.Drawing.Point(392, 239)
        Me.txt_peticion.Multiline = True
        Me.txt_peticion.Name = "txt_peticion"
        Me.txt_peticion.Scrollbars = System.Windows.Forms.ScrollBars.Vertical
        Me.txt_peticion.Size = New System.Drawing.Size(261, 67)
        Me.txt_peticion.TabIndex = 11
        '
        'data_lbl_habitaciones
        '
        Me.data_lbl_habitaciones.AutoSize = True
        Me.data_lbl_habitaciones.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_habitaciones.Location = New System.Drawing.Point(463, 95)
        Me.data_lbl_habitaciones.Name = "data_lbl_habitaciones"
        Me.data_lbl_habitaciones.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_habitaciones.TabIndex = 36
        Me.data_lbl_habitaciones.Text = "--------"
        '
        'data_lbl_ninos
        '
        Me.data_lbl_ninos.AutoSize = True
        Me.data_lbl_ninos.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_ninos.Location = New System.Drawing.Point(463, 121)
        Me.data_lbl_ninos.Name = "data_lbl_ninos"
        Me.data_lbl_ninos.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_ninos.TabIndex = 35
        Me.data_lbl_ninos.Text = "--------"
        '
        'data_lbl_adultos
        '
        Me.data_lbl_adultos.AutoSize = True
        Me.data_lbl_adultos.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_adultos.Location = New System.Drawing.Point(463, 108)
        Me.data_lbl_adultos.Name = "data_lbl_adultos"
        Me.data_lbl_adultos.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_adultos.TabIndex = 34
        Me.data_lbl_adultos.Text = "--------"
        Me.data_lbl_adultos.Visible = False
        '
        'lbl_habitaciones
        '
        Me.lbl_habitaciones.AutoSize = True
        Me.lbl_habitaciones.BackColor = System.Drawing.Color.Transparent
        Me.lbl_habitaciones.Location = New System.Drawing.Point(388, 95)
        Me.lbl_habitaciones.Name = "lbl_habitaciones"
        Me.lbl_habitaciones.Size = New System.Drawing.Size(72, 13)
        Me.lbl_habitaciones.TabIndex = 33
        Me.lbl_habitaciones.Text = "Habitaciones:"
        '
        'lbl_ninos
        '
        Me.lbl_ninos.AutoSize = True
        Me.lbl_ninos.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ninos.Location = New System.Drawing.Point(388, 121)
        Me.lbl_ninos.Name = "lbl_ninos"
        Me.lbl_ninos.Size = New System.Drawing.Size(37, 13)
        Me.lbl_ninos.TabIndex = 31
        Me.lbl_ninos.Text = "Ni�os:"
        '
        'lbl_adultos
        '
        Me.lbl_adultos.AutoSize = True
        Me.lbl_adultos.BackColor = System.Drawing.Color.Transparent
        Me.lbl_adultos.Location = New System.Drawing.Point(388, 108)
        Me.lbl_adultos.Name = "lbl_adultos"
        Me.lbl_adultos.Size = New System.Drawing.Size(45, 13)
        Me.lbl_adultos.TabIndex = 32
        Me.lbl_adultos.Text = "Adultos:"
        '
        'data_lbl_in
        '
        Me.data_lbl_in.AutoSize = True
        Me.data_lbl_in.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_in.Location = New System.Drawing.Point(463, 21)
        Me.data_lbl_in.Name = "data_lbl_in"
        Me.data_lbl_in.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_in.TabIndex = 30
        Me.data_lbl_in.Text = "--------"
        '
        'data_lbl_out
        '
        Me.data_lbl_out.AutoSize = True
        Me.data_lbl_out.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_out.Location = New System.Drawing.Point(463, 34)
        Me.data_lbl_out.Name = "data_lbl_out"
        Me.data_lbl_out.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_out.TabIndex = 29
        Me.data_lbl_out.Text = "--------"
        '
        'lblConvenioData
        '
        Me.lblConvenioData.AutoSize = True
        Me.lblConvenioData.BackColor = System.Drawing.Color.Transparent
        Me.lblConvenioData.Location = New System.Drawing.Point(463, 73)
        Me.lblConvenioData.Name = "lblConvenioData"
        Me.lblConvenioData.Size = New System.Drawing.Size(31, 13)
        Me.lblConvenioData.TabIndex = 28
        Me.lblConvenioData.Text = "--------"
        '
        'data_lbl_codProm
        '
        Me.data_lbl_codProm.AutoSize = True
        Me.data_lbl_codProm.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_codProm.Location = New System.Drawing.Point(463, 60)
        Me.data_lbl_codProm.Name = "data_lbl_codProm"
        Me.data_lbl_codProm.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_codProm.TabIndex = 28
        Me.data_lbl_codProm.Text = "--------"
        '
        'data_lbl_hotel
        '
        Me.data_lbl_hotel.AutoSize = True
        Me.data_lbl_hotel.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_hotel.Location = New System.Drawing.Point(463, 8)
        Me.data_lbl_hotel.Name = "data_lbl_hotel"
        Me.data_lbl_hotel.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_hotel.TabIndex = 27
        Me.data_lbl_hotel.Text = "--------"
        '
        'data_lbl_cadena
        '
        Me.data_lbl_cadena.AutoSize = True
        Me.data_lbl_cadena.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_cadena.Location = New System.Drawing.Point(463, 47)
        Me.data_lbl_cadena.Name = "data_lbl_cadena"
        Me.data_lbl_cadena.Size = New System.Drawing.Size(31, 13)
        Me.data_lbl_cadena.TabIndex = 27
        Me.data_lbl_cadena.Text = "--------"
        '
        'gpb_nuevaBusqueda
        '
        Me.gpb_nuevaBusqueda.Controls.Add(Me.btn_actualizar)
        Me.gpb_nuevaBusqueda.Controls.Add(Me.Uc_habitaciones1)
        Me.gpb_nuevaBusqueda.Controls.Add(Me.dtp_out)
        Me.gpb_nuevaBusqueda.Controls.Add(Me.lbl_salida)
        Me.gpb_nuevaBusqueda.Controls.Add(Me.lbl_llegada)
        Me.gpb_nuevaBusqueda.Controls.Add(Me.dtp_in)
        Me.gpb_nuevaBusqueda.Controls.Add(Me.WebBrowser2)
        Me.gpb_nuevaBusqueda.Location = New System.Drawing.Point(583, 8)
        Me.gpb_nuevaBusqueda.Name = "gpb_nuevaBusqueda"
        Me.gpb_nuevaBusqueda.Size = New System.Drawing.Size(324, 176)
        Me.gpb_nuevaBusqueda.TabIndex = 23
        Me.gpb_nuevaBusqueda.TabStop = False
        Me.gpb_nuevaBusqueda.Text = "Nueva busqueda"
        '
        'btn_actualizar
        '
        Me.btn_actualizar.Location = New System.Drawing.Point(212, 114)
        Me.btn_actualizar.Name = "btn_actualizar"
        Me.btn_actualizar.Size = New System.Drawing.Size(89, 23)
        Me.btn_actualizar.TabIndex = 24
        Me.btn_actualizar.Text = " pue"
        Me.btn_actualizar.UseVisualStyleBackColor = True
        '
        'Uc_habitaciones1
        '
        Me.Uc_habitaciones1.Enabled = False
        T_Habitaciones1.EdadesNinos = ""
        T_Habitaciones1.TotalAdultos = 1
        T_Habitaciones1.TotalHabitaciones = 1
        T_Habitaciones1.TotalNinos = 0
        Me.Uc_habitaciones1.Habitaciones = T_Habitaciones1
        Me.Uc_habitaciones1.Location = New System.Drawing.Point(6, 13)
        Me.Uc_habitaciones1.Name = "Uc_habitaciones1"
        Me.Uc_habitaciones1.Size = New System.Drawing.Size(299, 88)
        Me.Uc_habitaciones1.TabIndex = 4
        '
        'dtp_out
        '
        Me.dtp_out.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_out.Enabled = False
        Me.dtp_out.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_out.Location = New System.Drawing.Point(91, 140)
        Me.dtp_out.Name = "dtp_out"
        Me.dtp_out.Size = New System.Drawing.Size(115, 20)
        Me.dtp_out.TabIndex = 2
        Me.dtp_out.Value = New Date(2009, 4, 30, 0, 0, 0, 0)
        '
        'lbl_salida
        '
        Me.lbl_salida.AutoSize = True
        Me.lbl_salida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_salida.Location = New System.Drawing.Point(14, 146)
        Me.lbl_salida.Name = "lbl_salida"
        Me.lbl_salida.Size = New System.Drawing.Size(39, 13)
        Me.lbl_salida.TabIndex = 0
        Me.lbl_salida.Text = "Salida:"
        '
        'lbl_llegada
        '
        Me.lbl_llegada.AutoSize = True
        Me.lbl_llegada.BackColor = System.Drawing.Color.Transparent
        Me.lbl_llegada.Location = New System.Drawing.Point(14, 119)
        Me.lbl_llegada.Name = "lbl_llegada"
        Me.lbl_llegada.Size = New System.Drawing.Size(48, 13)
        Me.lbl_llegada.TabIndex = 0
        Me.lbl_llegada.Text = "Llegada:"
        '
        'dtp_in
        '
        Me.dtp_in.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_in.Enabled = False
        Me.dtp_in.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_in.Location = New System.Drawing.Point(91, 114)
        Me.dtp_in.Name = "dtp_in"
        Me.dtp_in.Size = New System.Drawing.Size(115, 20)
        Me.dtp_in.TabIndex = 1
        Me.dtp_in.Value = New Date(2009, 4, 26, 0, 0, 0, 0)
        '
        'WebBrowser2
        '
        Me.WebBrowser2.Location = New System.Drawing.Point(-585, -8)
        Me.WebBrowser2.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser2.Name = "WebBrowser2"
        Me.WebBrowser2.Size = New System.Drawing.Size(912, 421)
        Me.WebBrowser2.TabIndex = 3
        Me.WebBrowser2.Visible = False
        '
        'lbl_llegada2
        '
        Me.lbl_llegada2.AutoSize = True
        Me.lbl_llegada2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_llegada2.Location = New System.Drawing.Point(388, 21)
        Me.lbl_llegada2.Name = "lbl_llegada2"
        Me.lbl_llegada2.Size = New System.Drawing.Size(48, 13)
        Me.lbl_llegada2.TabIndex = 26
        Me.lbl_llegada2.Text = "Llegada:"
        '
        'lbl_salida2
        '
        Me.lbl_salida2.AutoSize = True
        Me.lbl_salida2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_salida2.Location = New System.Drawing.Point(388, 34)
        Me.lbl_salida2.Name = "lbl_salida2"
        Me.lbl_salida2.Size = New System.Drawing.Size(39, 13)
        Me.lbl_salida2.TabIndex = 25
        Me.lbl_salida2.Text = "Salida:"
        '
        'lbl_info_personasExtra
        '
        Me.lbl_info_personasExtra.AutoSize = True
        Me.lbl_info_personasExtra.BackColor = System.Drawing.Color.Transparent
        Me.lbl_info_personasExtra.Location = New System.Drawing.Point(388, 156)
        Me.lbl_info_personasExtra.Name = "lbl_info_personasExtra"
        Me.lbl_info_personasExtra.Size = New System.Drawing.Size(55, 13)
        Me.lbl_info_personasExtra.TabIndex = 22
        Me.lbl_info_personasExtra.Text = "xxxxxxxxx-"
        '
        'lbl_info_total
        '
        Me.lbl_info_total.AutoSize = True
        Me.lbl_info_total.BackColor = System.Drawing.Color.Transparent
        Me.lbl_info_total.Location = New System.Drawing.Point(388, 200)
        Me.lbl_info_total.Name = "lbl_info_total"
        Me.lbl_info_total.Size = New System.Drawing.Size(52, 13)
        Me.lbl_info_total.TabIndex = 0
        Me.lbl_info_total.Text = "xxxxxxxxx"
        '
        'btn_cambiar
        '
        Me.btn_cambiar.Location = New System.Drawing.Point(757, 184)
        Me.btn_cambiar.Name = "btn_cambiar"
        Me.btn_cambiar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cambiar.TabIndex = 7
        Me.btn_cambiar.Text = "Cambiar"
        Me.btn_cambiar.UseVisualStyleBackColor = True
        '
        'lbl_info_impuestos
        '
        Me.lbl_info_impuestos.AutoSize = True
        Me.lbl_info_impuestos.BackColor = System.Drawing.Color.Transparent
        Me.lbl_info_impuestos.Location = New System.Drawing.Point(388, 183)
        Me.lbl_info_impuestos.Name = "lbl_info_impuestos"
        Me.lbl_info_impuestos.Size = New System.Drawing.Size(52, 13)
        Me.lbl_info_impuestos.TabIndex = 0
        Me.lbl_info_impuestos.Text = "xxxxxxxxx"
        '
        'btn_reglas
        '
        Me.btn_reglas.Location = New System.Drawing.Point(833, 184)
        Me.btn_reglas.Name = "btn_reglas"
        Me.btn_reglas.Size = New System.Drawing.Size(75, 23)
        Me.btn_reglas.TabIndex = 8
        Me.btn_reglas.Text = "Reglas"
        Me.btn_reglas.UseVisualStyleBackColor = True
        '
        'lbl_info_subtotal
        '
        Me.lbl_info_subtotal.AutoSize = True
        Me.lbl_info_subtotal.BackColor = System.Drawing.Color.Transparent
        Me.lbl_info_subtotal.Location = New System.Drawing.Point(388, 170)
        Me.lbl_info_subtotal.Name = "lbl_info_subtotal"
        Me.lbl_info_subtotal.Size = New System.Drawing.Size(52, 13)
        Me.lbl_info_subtotal.TabIndex = 0
        Me.lbl_info_subtotal.Text = "xxxxxxxxx"
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.BackColor = System.Drawing.Color.Transparent
        Me.lblConvenio.Location = New System.Drawing.Point(388, 73)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(55, 13)
        Me.lblConvenio.TabIndex = 0
        Me.lblConvenio.Text = "Convenio:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.WebBrowser1)
        Me.GroupBox1.Controls.Add(Me.txtInfoDeposito)
        Me.GroupBox1.Controls.Add(Me.lblComentariodep)
        Me.GroupBox1.Controls.Add(Me.lblAmount)
        Me.GroupBox1.Controls.Add(Me.txtDeposito)
        Me.GroupBox1.Controls.Add(Me.chkDeposito)
        Me.GroupBox1.Controls.Add(Me.pnlDeposito)
        Me.GroupBox1.Controls.Add(Me.txtOtra)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.rbt_otra)
        Me.GroupBox1.Controls.Add(Me.rbt_ninguno)
        Me.GroupBox1.Controls.Add(Me.rbt_depositoBancario)
        Me.GroupBox1.Controls.Add(Me.rbt_tarjetaCredito)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 393)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Garant�a de tarifa"
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(8, 93)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(365, 200)
        Me.WebBrowser1.TabIndex = 5
        '
        'txtInfoDeposito
        '
        Me.txtInfoDeposito.AlwaysInEditMode = True
        Me.txtInfoDeposito.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtInfoDeposito.Location = New System.Drawing.Point(74, 128)
        Me.txtInfoDeposito.MaxLength = 200
        Me.txtInfoDeposito.Multiline = True
        Me.txtInfoDeposito.Name = "txtInfoDeposito"
        Me.txtInfoDeposito.Scrollbars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoDeposito.Size = New System.Drawing.Size(299, 147)
        Me.txtInfoDeposito.TabIndex = 7
        '
        'lblComentariodep
        '
        Me.lblComentariodep.AutoSize = True
        Me.lblComentariodep.BackColor = System.Drawing.Color.Transparent
        Me.lblComentariodep.Location = New System.Drawing.Point(6, 112)
        Me.lblComentariodep.Name = "lblComentariodep"
        Me.lblComentariodep.Size = New System.Drawing.Size(108, 13)
        Me.lblComentariodep.TabIndex = 32
        Me.lblComentariodep.Text = "Comentario Deposito:"
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.BackColor = System.Drawing.Color.Transparent
        Me.lblAmount.Location = New System.Drawing.Point(6, 96)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(40, 13)
        Me.lblAmount.TabIndex = 31
        Me.lblAmount.Text = "Monto:"
        '
        'txtDeposito
        '
        Me.txtDeposito.AlwaysInEditMode = True
        Me.txtDeposito.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtDeposito.Location = New System.Drawing.Point(109, 93)
        Me.txtDeposito.MaxLength = 10
        Me.txtDeposito.Name = "txtDeposito"
        Me.txtDeposito.Scrollbars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDeposito.Size = New System.Drawing.Size(90, 19)
        Me.txtDeposito.TabIndex = 6
        '
        'chkDeposito
        '
        Me.chkDeposito.AutoSize = True
        Me.chkDeposito.Location = New System.Drawing.Point(6, 75)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(68, 17)
        Me.chkDeposito.TabIndex = 4
        Me.chkDeposito.Text = "Registrar"
        Me.chkDeposito.UseVisualStyleBackColor = True
        '
        'pnlDeposito
        '
        Me.pnlDeposito.Controls.Add(Me.lblDep)
        Me.pnlDeposito.Controls.Add(Me.dtp_DepHora)
        Me.pnlDeposito.Controls.Add(Me.dtp_DepFecha)
        Me.pnlDeposito.Location = New System.Drawing.Point(123, 49)
        Me.pnlDeposito.Name = "pnlDeposito"
        Me.pnlDeposito.Size = New System.Drawing.Size(252, 26)
        Me.pnlDeposito.TabIndex = 3
        '
        'lblDep
        '
        Me.lblDep.AutoSize = True
        Me.lblDep.BackColor = System.Drawing.Color.Transparent
        Me.lblDep.Location = New System.Drawing.Point(4, 7)
        Me.lblDep.Name = "lblDep"
        Me.lblDep.Size = New System.Drawing.Size(37, 13)
        Me.lblDep.TabIndex = 32
        Me.lblDep.Text = "Limite:"
        '
        'dtp_DepHora
        '
        Me.dtp_DepHora.CustomFormat = "HH:mm"
        Me.dtp_DepHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtp_DepHora.Location = New System.Drawing.Point(164, 5)
        Me.dtp_DepHora.Name = "dtp_DepHora"
        Me.dtp_DepHora.ShowUpDown = True
        Me.dtp_DepHora.Size = New System.Drawing.Size(86, 20)
        Me.dtp_DepHora.TabIndex = 31
        Me.dtp_DepHora.Value = New Date(2009, 4, 26, 0, 0, 0, 0)
        '
        'dtp_DepFecha
        '
        Me.dtp_DepFecha.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_DepFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_DepFecha.Location = New System.Drawing.Point(56, 5)
        Me.dtp_DepFecha.Name = "dtp_DepFecha"
        Me.dtp_DepFecha.Size = New System.Drawing.Size(102, 20)
        Me.dtp_DepFecha.TabIndex = 30
        Me.dtp_DepFecha.Value = New Date(2009, 4, 26, 0, 0, 0, 0)
        '
        'txtOtra
        '
        Me.txtOtra.AlwaysInEditMode = True
        Me.txtOtra.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtOtra.Location = New System.Drawing.Point(76, 305)
        Me.txtOtra.MaxLength = 200
        Me.txtOtra.Multiline = True
        Me.txtOtra.Name = "txtOtra"
        Me.txtOtra.Scrollbars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOtra.Size = New System.Drawing.Size(299, 35)
        Me.txtOtra.TabIndex = 9
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbt_garantia)
        Me.GroupBox2.Controls.Add(Me.rbt_deposito)
        Me.GroupBox2.Location = New System.Drawing.Point(142, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(160, 44)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Usar como"
        Me.GroupBox2.Visible = False
        '
        'rbt_garantia
        '
        Me.rbt_garantia.AutoSize = True
        Me.rbt_garantia.BackColor = System.Drawing.Color.Transparent
        Me.rbt_garantia.Checked = True
        Me.rbt_garantia.Enabled = False
        Me.rbt_garantia.Location = New System.Drawing.Point(6, 19)
        Me.rbt_garantia.Name = "rbt_garantia"
        Me.rbt_garantia.Size = New System.Drawing.Size(67, 17)
        Me.rbt_garantia.TabIndex = 0
        Me.rbt_garantia.TabStop = True
        Me.rbt_garantia.Text = "Garant�a"
        Me.rbt_garantia.UseVisualStyleBackColor = False
        '
        'rbt_deposito
        '
        Me.rbt_deposito.AutoSize = True
        Me.rbt_deposito.BackColor = System.Drawing.Color.Transparent
        Me.rbt_deposito.Enabled = False
        Me.rbt_deposito.Location = New System.Drawing.Point(87, 19)
        Me.rbt_deposito.Name = "rbt_deposito"
        Me.rbt_deposito.Size = New System.Drawing.Size(67, 17)
        Me.rbt_deposito.TabIndex = 1
        Me.rbt_deposito.Text = "Deposito"
        Me.rbt_deposito.UseVisualStyleBackColor = False
        '
        'rbt_otra
        '
        Me.rbt_otra.AutoSize = True
        Me.rbt_otra.BackColor = System.Drawing.Color.Transparent
        Me.rbt_otra.Location = New System.Drawing.Point(6, 304)
        Me.rbt_otra.Name = "rbt_otra"
        Me.rbt_otra.Size = New System.Drawing.Size(45, 17)
        Me.rbt_otra.TabIndex = 8
        Me.rbt_otra.Text = "Otra"
        Me.rbt_otra.UseVisualStyleBackColor = False
        '
        'rbt_ninguno
        '
        Me.rbt_ninguno.AutoSize = True
        Me.rbt_ninguno.BackColor = System.Drawing.Color.Transparent
        Me.rbt_ninguno.Enabled = False
        Me.rbt_ninguno.Location = New System.Drawing.Point(6, 346)
        Me.rbt_ninguno.Name = "rbt_ninguno"
        Me.rbt_ninguno.Size = New System.Drawing.Size(65, 17)
        Me.rbt_ninguno.TabIndex = 10
        Me.rbt_ninguno.Text = "Ninguno"
        Me.rbt_ninguno.UseVisualStyleBackColor = False
        '
        'rbt_depositoBancario
        '
        Me.rbt_depositoBancario.AutoSize = True
        Me.rbt_depositoBancario.BackColor = System.Drawing.Color.Transparent
        Me.rbt_depositoBancario.Enabled = False
        Me.rbt_depositoBancario.Location = New System.Drawing.Point(6, 52)
        Me.rbt_depositoBancario.Name = "rbt_depositoBancario"
        Me.rbt_depositoBancario.Size = New System.Drawing.Size(111, 17)
        Me.rbt_depositoBancario.TabIndex = 2
        Me.rbt_depositoBancario.Text = "Dep�sito bancario"
        Me.rbt_depositoBancario.UseVisualStyleBackColor = False
        '
        'rbt_tarjetaCredito
        '
        Me.rbt_tarjetaCredito.AutoSize = True
        Me.rbt_tarjetaCredito.BackColor = System.Drawing.Color.Transparent
        Me.rbt_tarjetaCredito.Checked = True
        Me.rbt_tarjetaCredito.Enabled = False
        Me.rbt_tarjetaCredito.Location = New System.Drawing.Point(6, 18)
        Me.rbt_tarjetaCredito.Name = "rbt_tarjetaCredito"
        Me.rbt_tarjetaCredito.Size = New System.Drawing.Size(108, 17)
        Me.rbt_tarjetaCredito.TabIndex = 1
        Me.rbt_tarjetaCredito.TabStop = True
        Me.rbt_tarjetaCredito.Text = "Tarjeta de cr�dito"
        Me.rbt_tarjetaCredito.UseVisualStyleBackColor = False
        '
        'lbl_codProm
        '
        Me.lbl_codProm.AutoSize = True
        Me.lbl_codProm.BackColor = System.Drawing.Color.Transparent
        Me.lbl_codProm.Location = New System.Drawing.Point(388, 60)
        Me.lbl_codProm.Name = "lbl_codProm"
        Me.lbl_codProm.Size = New System.Drawing.Size(58, 13)
        Me.lbl_codProm.TabIndex = 0
        Me.lbl_codProm.Text = "C�d. prom:"
        '
        'lbl_info_tarifa
        '
        Me.lbl_info_tarifa.AutoSize = True
        Me.lbl_info_tarifa.BackColor = System.Drawing.Color.Transparent
        Me.lbl_info_tarifa.Location = New System.Drawing.Point(388, 142)
        Me.lbl_info_tarifa.Name = "lbl_info_tarifa"
        Me.lbl_info_tarifa.Size = New System.Drawing.Size(52, 13)
        Me.lbl_info_tarifa.TabIndex = 0
        Me.lbl_info_tarifa.Text = "xxxxxxxxx"
        '
        'txt_codigoTarifa
        '
        Me.txt_codigoTarifa.AlwaysInEditMode = True
        Me.txt_codigoTarifa.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_codigoTarifa.Enabled = False
        Me.txt_codigoTarifa.Location = New System.Drawing.Point(651, 186)
        Me.txt_codigoTarifa.Name = "txt_codigoTarifa"
        Me.txt_codigoTarifa.Size = New System.Drawing.Size(100, 19)
        Me.txt_codigoTarifa.TabIndex = 6
        '
        'lblComentario
        '
        Me.lblComentario.AutoSize = True
        Me.lblComentario.BackColor = System.Drawing.Color.Transparent
        Me.lblComentario.Location = New System.Drawing.Point(389, 309)
        Me.lblComentario.Name = "lblComentario"
        Me.lblComentario.Size = New System.Drawing.Size(112, 13)
        Me.lblComentario.TabIndex = 0
        Me.lblComentario.Text = "Comentario Vendedor:"
        '
        'lbl_peticion
        '
        Me.lbl_peticion.AutoSize = True
        Me.lbl_peticion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_peticion.Location = New System.Drawing.Point(389, 225)
        Me.lbl_peticion.Name = "lbl_peticion"
        Me.lbl_peticion.Size = New System.Drawing.Size(90, 13)
        Me.lbl_peticion.TabIndex = 0
        Me.lbl_peticion.Text = "Petici�n especial:"
        '
        'lbl_Hotel
        '
        Me.lbl_Hotel.AutoSize = True
        Me.lbl_Hotel.BackColor = System.Drawing.Color.Transparent
        Me.lbl_Hotel.Location = New System.Drawing.Point(388, 8)
        Me.lbl_Hotel.Name = "lbl_Hotel"
        Me.lbl_Hotel.Size = New System.Drawing.Size(35, 13)
        Me.lbl_Hotel.TabIndex = 0
        Me.lbl_Hotel.Text = "Hotel:"
        '
        'lbl_codigoTarifa
        '
        Me.lbl_codigoTarifa.AutoSize = True
        Me.lbl_codigoTarifa.BackColor = System.Drawing.Color.Transparent
        Me.lbl_codigoTarifa.Location = New System.Drawing.Point(580, 189)
        Me.lbl_codigoTarifa.Name = "lbl_codigoTarifa"
        Me.lbl_codigoTarifa.Size = New System.Drawing.Size(69, 13)
        Me.lbl_codigoTarifa.TabIndex = 0
        Me.lbl_codigoTarifa.Text = "C�digo tarifa:"
        '
        'lbl_cadena
        '
        Me.lbl_cadena.AutoSize = True
        Me.lbl_cadena.BackColor = System.Drawing.Color.Transparent
        Me.lbl_cadena.Location = New System.Drawing.Point(388, 47)
        Me.lbl_cadena.Name = "lbl_cadena"
        Me.lbl_cadena.Size = New System.Drawing.Size(47, 13)
        Me.lbl_cadena.TabIndex = 0
        Me.lbl_cadena.Text = "Cadena:"
        '
        'btn_reservar
        '
        Me.btn_reservar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_reservar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_reservar.Location = New System.Drawing.Point(706, 436)
        Me.btn_reservar.Name = "btn_reservar"
        Me.btn_reservar.Size = New System.Drawing.Size(127, 23)
        Me.btn_reservar.TabIndex = 1
        Me.btn_reservar.Text = "Agregar reservaci�n"
        Me.btn_reservar.UseVisualStyleBackColor = False
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(841, 436)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancelar.TabIndex = 2
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'UltraTabSharedControlsPage1
        '
        Me.UltraTabSharedControlsPage1.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabSharedControlsPage1.Name = "UltraTabSharedControlsPage1"
        Me.UltraTabSharedControlsPage1.Size = New System.Drawing.Size(923, 398)
        '
        'UltraTabControl1
        '
        Me.UltraTabControl1.Controls.Add(Me.UltraTabSharedControlsPage1)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControl3)
        Me.UltraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UltraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.UltraTabControl1.Name = "UltraTabControl1"
        Me.UltraTabControl1.SharedControlsPage = Me.UltraTabSharedControlsPage1
        Me.UltraTabControl1.Size = New System.Drawing.Size(925, 421)
        Me.UltraTabControl1.TabIndex = 0
        UltraTab3.Key = "cliente"
        UltraTab3.TabPage = Me.UltraTabPageControl3
        UltraTab3.Text = "Cliente"
        Me.UltraTabControl1.Tabs.AddRange(New Infragistics.Win.UltraWinTabControl.UltraTab() {UltraTab3})
        Me.UltraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'btn_imprimir
        '
        Me.btn_imprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_imprimir.BackColor = System.Drawing.SystemColors.Control
        Me.btn_imprimir.Location = New System.Drawing.Point(149, 436)
        Me.btn_imprimir.Name = "btn_imprimir"
        Me.btn_imprimir.Size = New System.Drawing.Size(127, 23)
        Me.btn_imprimir.TabIndex = 5
        Me.btn_imprimir.Text = "Imprimir Cotizacion"
        Me.btn_imprimir.UseVisualStyleBackColor = False
        Me.btn_imprimir.Visible = False
        '
        'btnCotizar
        '
        Me.btnCotizar.Location = New System.Drawing.Point(9, 436)
        Me.btnCotizar.Name = "btnCotizar"
        Me.btnCotizar.Size = New System.Drawing.Size(127, 23)
        Me.btnCotizar.TabIndex = 4
        Me.btnCotizar.Text = "Mostrar Cotizacion"
        Me.btnCotizar.UseVisualStyleBackColor = True
        '
        'gbCorreo
        '
        Me.gbCorreo.Controls.Add(Me.txtCorreo)
        Me.gbCorreo.Controls.Add(Me.btn_correo)
        Me.gbCorreo.Location = New System.Drawing.Point(286, 424)
        Me.gbCorreo.Name = "gbCorreo"
        Me.gbCorreo.Size = New System.Drawing.Size(321, 42)
        Me.gbCorreo.TabIndex = 9
        Me.gbCorreo.TabStop = False
        Me.gbCorreo.Text = "Enviar por Correo"
        Me.gbCorreo.Visible = False
        '
        'txtCorreo
        '
        Me.txtCorreo.Location = New System.Drawing.Point(6, 16)
        Me.txtCorreo.Name = "txtCorreo"
        Me.txtCorreo.Size = New System.Drawing.Size(175, 20)
        Me.txtCorreo.TabIndex = 8
        '
        'btn_correo
        '
        Me.btn_correo.Location = New System.Drawing.Point(188, 13)
        Me.btn_correo.Name = "btn_correo"
        Me.btn_correo.Size = New System.Drawing.Size(127, 23)
        Me.btn_correo.TabIndex = 9
        Me.btn_correo.Text = "Enviar"
        Me.btn_correo.UseVisualStyleBackColor = True
        '
        'btnConfirm
        '
        Me.btnConfirm.Location = New System.Drawing.Point(613, 436)
        Me.btnConfirm.Name = "btnConfirm"
        Me.btnConfirm.Size = New System.Drawing.Size(75, 23)
        Me.btnConfirm.TabIndex = 10
        Me.btnConfirm.Text = "Confirmar"
        Me.btnConfirm.UseVisualStyleBackColor = True
        Me.btnConfirm.Visible = False
        '
        'reservar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(925, 471)
        Me.Controls.Add(Me.btnConfirm)
        Me.Controls.Add(Me.gbCorreo)
        Me.Controls.Add(Me.btnCotizar)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_imprimir)
        Me.Controls.Add(Me.btn_reservar)
        Me.Controls.Add(Me.UltraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "reservar"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reservar"
        Me.UltraTabPageControl3.ResumeLayout(False)
        Me.UltraTabPageControl3.PerformLayout()
        Me.pnlFrecuentCode.ResumeLayout(False)
        Me.pnlFrecuentCode.PerformLayout()
        CType(Me.txtFrecuentCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_comentario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCatalogos.ResumeLayout(False)
        Me.pnlCatalogos.PerformLayout()
        CType(Me.txt_peticion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpb_nuevaBusqueda.ResumeLayout(False)
        Me.gpb_nuevaBusqueda.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtInfoDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDeposito.ResumeLayout(False)
        Me.pnlDeposito.PerformLayout()
        CType(Me.txtOtra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txt_codigoTarifa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabControl1.ResumeLayout(False)
        Me.gbCorreo.ResumeLayout(False)
        Me.gbCorreo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_reservar As System.Windows.Forms.Button
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents UltraTabPageControl3 As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Friend WithEvents UltraTabSharedControlsPage1 As Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
    Friend WithEvents UltraTabControl1 As Infragistics.Win.UltraWinTabControl.UltraTabControl
    Friend WithEvents txt_peticion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_peticion As System.Windows.Forms.Label
    Friend WithEvents lbl_codProm As System.Windows.Forms.Label
    Friend WithEvents lbl_llegada As System.Windows.Forms.Label
    Friend WithEvents lbl_salida As System.Windows.Forms.Label
    Friend WithEvents dtp_out As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_codigoTarifa As System.Windows.Forms.Label
    Friend WithEvents lbl_cadena As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbt_garantia As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_deposito As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_ninguno As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_depositoBancario As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_tarjetaCredito As System.Windows.Forms.RadioButton
    Friend WithEvents dtp_in As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn_reglas As System.Windows.Forms.Button
    Friend WithEvents btn_cambiar As System.Windows.Forms.Button
    Friend WithEvents lbl_info_total As System.Windows.Forms.Label
    Friend WithEvents lbl_info_impuestos As System.Windows.Forms.Label
    Friend WithEvents lbl_info_subtotal As System.Windows.Forms.Label
    Friend WithEvents lbl_info_tarifa As System.Windows.Forms.Label
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents lbl_info_personasExtra As System.Windows.Forms.Label
    Friend WithEvents Uc_habitaciones1 As callCenter.uc_habitaciones
    Friend WithEvents lbl_mensaje As System.Windows.Forms.Label
    Friend WithEvents lbl_llegada2 As System.Windows.Forms.Label
    Friend WithEvents lbl_salida2 As System.Windows.Forms.Label
    Friend WithEvents gpb_nuevaBusqueda As System.Windows.Forms.GroupBox
    Friend WithEvents txt_codigoTarifa As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_actualizar As System.Windows.Forms.Button
    Friend WithEvents data_lbl_cadena As System.Windows.Forms.Label
    Friend WithEvents data_lbl_codProm As System.Windows.Forms.Label
    Friend WithEvents data_lbl_in As System.Windows.Forms.Label
    Friend WithEvents data_lbl_out As System.Windows.Forms.Label
    Friend WithEvents data_lbl_habitaciones As System.Windows.Forms.Label
    Friend WithEvents data_lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents data_lbl_adultos As System.Windows.Forms.Label
    Friend WithEvents lbl_habitaciones As System.Windows.Forms.Label
    Friend WithEvents lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents lbl_adultos As System.Windows.Forms.Label
    Friend WithEvents txtOtra As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents rbt_otra As System.Windows.Forms.RadioButton
    Friend WithEvents data_lbl_hotel As System.Windows.Forms.Label
    Friend WithEvents lbl_Hotel As System.Windows.Forms.Label
    Friend WithEvents pnlDeposito As System.Windows.Forms.Panel
    Friend WithEvents lblDep As System.Windows.Forms.Label
    Friend WithEvents dtp_DepHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_DepFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_comentario As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lblComentario As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents chkDeposito As System.Windows.Forms.CheckBox
    Friend WithEvents txtDeposito As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtInfoDeposito As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lblComentariodep As System.Windows.Forms.Label
    Friend WithEvents pnlCatalogos As System.Windows.Forms.Panel
    Friend WithEvents lblSegmento As System.Windows.Forms.Label
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents lblMedio As System.Windows.Forms.Label
    Friend WithEvents cmbSegmento As System.Windows.Forms.ComboBox
    Friend WithEvents cmbMedios As System.Windows.Forms.ComboBox
    Friend WithEvents CmbSearch1 As callCenter.cmbSearch
    Friend WithEvents lblFrecuent As System.Windows.Forms.Label
    Friend WithEvents btnAltaSocio As System.Windows.Forms.Button
    Friend WithEvents txtFrecuentCode As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btnBuscarSocio As System.Windows.Forms.Button
    Friend WithEvents pnlFrecuentCode As System.Windows.Forms.Panel
    Friend WithEvents btnFrecCerrar As System.Windows.Forms.Button
    Friend WithEvents lblFrec_Name As System.Windows.Forms.Label
    Friend WithEvents lblFrec_Code As System.Windows.Forms.Label
    Friend WithEvents lblConvenioData As System.Windows.Forms.Label
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents btn_imprimir As System.Windows.Forms.Button
    Friend WithEvents WebBrowser2 As System.Windows.Forms.WebBrowser
    Friend WithEvents btnCotizar As System.Windows.Forms.Button
    Friend WithEvents gbCorreo As System.Windows.Forms.GroupBox
    Friend WithEvents txtCorreo As System.Windows.Forms.TextBox
    Friend WithEvents btn_correo As System.Windows.Forms.Button
    Friend WithEvents btnConfirm As System.Windows.Forms.Button
End Class
