﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cambiar_reservacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cambiar_reservacion))
        Me.group_payment = New System.Windows.Forms.GroupBox()
        Me.rbt_conekta = New System.Windows.Forms.RadioButton()
        Me.rdt_pay_in_hotel = New System.Windows.Forms.RadioButton()
        Me.rbt_pay_u = New System.Windows.Forms.RadioButton()
        Me.rbt_american_express = New System.Windows.Forms.RadioButton()
        Me.rbt_paypal = New System.Windows.Forms.RadioButton()
        Me.rbt_deposito = New System.Windows.Forms.RadioButton()
        Me.rdt_tarjeta = New System.Windows.Forms.RadioButton()
        Me.lbl_info_tarifa = New System.Windows.Forms.Label()
        Me.lbl_info_total = New System.Windows.Forms.Label()
        Me.txt_edit_total = New System.Windows.Forms.TextBox()
        Me.lbl_comentario = New System.Windows.Forms.Label()
        Me.lbl_peticion = New System.Windows.Forms.Label()
        Me.txt_edit_peticion = New System.Windows.Forms.TextBox()
        Me.txt_edit_comentario = New System.Windows.Forms.TextBox()
        Me.lbl_llegada = New System.Windows.Forms.Label()
        Me.dtp_in = New System.Windows.Forms.DateTimePicker()
        Me.lbl_salida = New System.Windows.Forms.Label()
        Me.dtp_out = New System.Windows.Forms.DateTimePicker()
        Me.btn_modificar = New System.Windows.Forms.Button()
        Me.lbl_info_currency_total = New System.Windows.Forms.Label()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.dtgrid_ratesdetails = New System.Windows.Forms.DataGridView()
        Me.txt_edit_totalNR = New System.Windows.Forms.TextBox()
        Me.lbl_info_totalNR = New System.Windows.Forms.Label()
        Me.lbl_info_currency_totalNR = New System.Windows.Forms.Label()
        Me.btn_add_row = New System.Windows.Forms.Button()
        Me.btn_remove_row = New System.Windows.Forms.Button()
        Me.cmb_tipo_habitacion = New System.Windows.Forms.ComboBox()
        Me.lbl_tipo_Habitacion = New System.Windows.Forms.Label()
        Me.cmb_mount_payment = New System.Windows.Forms.ComboBox()
        Me.lbl_months = New System.Windows.Forms.Label()
        Me.cmb_source = New System.Windows.Forms.ComboBox()
        Me.lbl_source = New System.Windows.Forms.Label()
        Me.lbl_noAuth = New System.Windows.Forms.Label()
        Me.AuthNum = New System.Windows.Forms.TextBox()
        Me.cmb_status = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chk_NR = New System.Windows.Forms.CheckBox()
        Me.lbl_deposit = New System.Windows.Forms.Label()
        Me.txt_DepositAmount = New System.Windows.Forms.TextBox()
        Me.lbl_depositAmount = New System.Windows.Forms.Label()
        Me.lbl_depositCurrency = New System.Windows.Forms.Label()
        Me.rbt_target_UV = New System.Windows.Forms.RadioButton()
        Me.rbt_target_HTL = New System.Windows.Forms.RadioButton()
        Me.lbl_depositTarget = New System.Windows.Forms.Label()
        Me.tc_depositInfo = New System.Windows.Forms.TabControl()
        Me.tp_esp = New System.Windows.Forms.TabPage()
        Me.txt_deposit_esp = New System.Windows.Forms.TextBox()
        Me.tp_eng = New System.Windows.Forms.TabPage()
        Me.txt_deposit_eng = New System.Windows.Forms.TextBox()
        Me.txt_reference = New System.Windows.Forms.TextBox()
        Me.lbl_reference = New System.Windows.Forms.Label()
        Me.group_payment.SuspendLayout()
        CType(Me.dtgrid_ratesdetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tc_depositInfo.SuspendLayout()
        Me.tp_esp.SuspendLayout()
        Me.tp_eng.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_payment
        '
        Me.group_payment.Controls.Add(Me.rbt_conekta)
        Me.group_payment.Controls.Add(Me.rdt_pay_in_hotel)
        Me.group_payment.Controls.Add(Me.rbt_pay_u)
        Me.group_payment.Controls.Add(Me.rbt_american_express)
        Me.group_payment.Controls.Add(Me.rbt_paypal)
        Me.group_payment.Controls.Add(Me.rbt_deposito)
        Me.group_payment.Controls.Add(Me.rdt_tarjeta)
        Me.group_payment.Location = New System.Drawing.Point(12, 20)
        Me.group_payment.Name = "group_payment"
        Me.group_payment.Size = New System.Drawing.Size(299, 130)
        Me.group_payment.TabIndex = 1
        Me.group_payment.TabStop = False
        Me.group_payment.Text = "Metodo de pago"
        '
        'rbt_conekta
        '
        Me.rbt_conekta.AutoSize = True
        Me.rbt_conekta.Location = New System.Drawing.Point(6, 109)
        Me.rbt_conekta.Name = "rbt_conekta"
        Me.rbt_conekta.Size = New System.Drawing.Size(65, 17)
        Me.rbt_conekta.TabIndex = 16
        Me.rbt_conekta.Text = "Conekta"
        Me.rbt_conekta.UseVisualStyleBackColor = True
        '
        'rdt_pay_in_hotel
        '
        Me.rdt_pay_in_hotel.AutoSize = True
        Me.rdt_pay_in_hotel.Location = New System.Drawing.Point(6, 29)
        Me.rdt_pay_in_hotel.Name = "rdt_pay_in_hotel"
        Me.rdt_pay_in_hotel.Size = New System.Drawing.Size(104, 17)
        Me.rdt_pay_in_hotel.TabIndex = 15
        Me.rdt_pay_in_hotel.Text = "Pago en el Hotel"
        Me.rdt_pay_in_hotel.UseVisualStyleBackColor = True
        '
        'rbt_pay_u
        '
        Me.rbt_pay_u.AutoSize = True
        Me.rbt_pay_u.Location = New System.Drawing.Point(149, 86)
        Me.rbt_pay_u.Name = "rbt_pay_u"
        Me.rbt_pay_u.Size = New System.Drawing.Size(83, 17)
        Me.rbt_pay_u.TabIndex = 13
        Me.rbt_pay_u.Text = "PayU Latam"
        Me.rbt_pay_u.UseVisualStyleBackColor = True
        '
        'rbt_american_express
        '
        Me.rbt_american_express.AutoSize = True
        Me.rbt_american_express.Location = New System.Drawing.Point(6, 86)
        Me.rbt_american_express.Name = "rbt_american_express"
        Me.rbt_american_express.Size = New System.Drawing.Size(109, 17)
        Me.rbt_american_express.TabIndex = 12
        Me.rbt_american_express.Text = "American Express"
        Me.rbt_american_express.UseVisualStyleBackColor = True
        '
        'rbt_paypal
        '
        Me.rbt_paypal.AutoSize = True
        Me.rbt_paypal.Location = New System.Drawing.Point(149, 63)
        Me.rbt_paypal.Name = "rbt_paypal"
        Me.rbt_paypal.Size = New System.Drawing.Size(58, 17)
        Me.rbt_paypal.TabIndex = 11
        Me.rbt_paypal.Text = "PayPal"
        Me.rbt_paypal.UseVisualStyleBackColor = True
        '
        'rbt_deposito
        '
        Me.rbt_deposito.AutoSize = True
        Me.rbt_deposito.Location = New System.Drawing.Point(149, 29)
        Me.rbt_deposito.Name = "rbt_deposito"
        Me.rbt_deposito.Size = New System.Drawing.Size(111, 17)
        Me.rbt_deposito.TabIndex = 10
        Me.rbt_deposito.Text = "Deposito bancario"
        Me.rbt_deposito.UseVisualStyleBackColor = True
        '
        'rdt_tarjeta
        '
        Me.rdt_tarjeta.AutoSize = True
        Me.rdt_tarjeta.Checked = True
        Me.rdt_tarjeta.Location = New System.Drawing.Point(6, 63)
        Me.rdt_tarjeta.Name = "rdt_tarjeta"
        Me.rdt_tarjeta.Size = New System.Drawing.Size(90, 17)
        Me.rdt_tarjeta.TabIndex = 8
        Me.rdt_tarjeta.TabStop = True
        Me.rdt_tarjeta.Text = "Pago en linea"
        Me.rdt_tarjeta.UseVisualStyleBackColor = True
        '
        'lbl_info_tarifa
        '
        Me.lbl_info_tarifa.AutoSize = True
        Me.lbl_info_tarifa.Location = New System.Drawing.Point(322, 148)
        Me.lbl_info_tarifa.Name = "lbl_info_tarifa"
        Me.lbl_info_tarifa.Size = New System.Drawing.Size(0, 13)
        Me.lbl_info_tarifa.TabIndex = 2
        '
        'lbl_info_total
        '
        Me.lbl_info_total.AutoSize = True
        Me.lbl_info_total.Location = New System.Drawing.Point(497, 321)
        Me.lbl_info_total.Name = "lbl_info_total"
        Me.lbl_info_total.Size = New System.Drawing.Size(34, 13)
        Me.lbl_info_total.TabIndex = 4
        Me.lbl_info_total.Text = "Total:"
        '
        'txt_edit_total
        '
        Me.txt_edit_total.Location = New System.Drawing.Point(541, 318)
        Me.txt_edit_total.Name = "txt_edit_total"
        Me.txt_edit_total.Size = New System.Drawing.Size(100, 20)
        Me.txt_edit_total.TabIndex = 7
        Me.txt_edit_total.Text = "0"
        '
        'lbl_comentario
        '
        Me.lbl_comentario.AutoSize = True
        Me.lbl_comentario.Location = New System.Drawing.Point(615, 100)
        Me.lbl_comentario.Name = "lbl_comentario"
        Me.lbl_comentario.Size = New System.Drawing.Size(109, 13)
        Me.lbl_comentario.TabIndex = 8
        Me.lbl_comentario.Text = "Comentario Vendedor"
        '
        'lbl_peticion
        '
        Me.lbl_peticion.AutoSize = True
        Me.lbl_peticion.Location = New System.Drawing.Point(615, 19)
        Me.lbl_peticion.Name = "lbl_peticion"
        Me.lbl_peticion.Size = New System.Drawing.Size(87, 13)
        Me.lbl_peticion.TabIndex = 9
        Me.lbl_peticion.Text = "Peticion especial"
        '
        'txt_edit_peticion
        '
        Me.txt_edit_peticion.Location = New System.Drawing.Point(612, 35)
        Me.txt_edit_peticion.Multiline = True
        Me.txt_edit_peticion.Name = "txt_edit_peticion"
        Me.txt_edit_peticion.Size = New System.Drawing.Size(220, 62)
        Me.txt_edit_peticion.TabIndex = 10
        '
        'txt_edit_comentario
        '
        Me.txt_edit_comentario.Location = New System.Drawing.Point(612, 116)
        Me.txt_edit_comentario.Multiline = True
        Me.txt_edit_comentario.Name = "txt_edit_comentario"
        Me.txt_edit_comentario.Size = New System.Drawing.Size(220, 66)
        Me.txt_edit_comentario.TabIndex = 11
        '
        'lbl_llegada
        '
        Me.lbl_llegada.AutoSize = True
        Me.lbl_llegada.Location = New System.Drawing.Point(370, 70)
        Me.lbl_llegada.Name = "lbl_llegada"
        Me.lbl_llegada.Size = New System.Drawing.Size(48, 13)
        Me.lbl_llegada.TabIndex = 12
        Me.lbl_llegada.Text = "Llegada:"
        '
        'dtp_in
        '
        Me.dtp_in.Location = New System.Drawing.Point(435, 66)
        Me.dtp_in.Name = "dtp_in"
        Me.dtp_in.Size = New System.Drawing.Size(100, 20)
        Me.dtp_in.TabIndex = 13
        '
        'lbl_salida
        '
        Me.lbl_salida.AutoSize = True
        Me.lbl_salida.Location = New System.Drawing.Point(377, 99)
        Me.lbl_salida.Name = "lbl_salida"
        Me.lbl_salida.Size = New System.Drawing.Size(39, 13)
        Me.lbl_salida.TabIndex = 14
        Me.lbl_salida.Text = "Salida:"
        '
        'dtp_out
        '
        Me.dtp_out.Location = New System.Drawing.Point(435, 96)
        Me.dtp_out.Name = "dtp_out"
        Me.dtp_out.Size = New System.Drawing.Size(100, 20)
        Me.dtp_out.TabIndex = 15
        '
        'btn_modificar
        '
        Me.btn_modificar.Location = New System.Drawing.Point(630, 392)
        Me.btn_modificar.Name = "btn_modificar"
        Me.btn_modificar.Size = New System.Drawing.Size(100, 23)
        Me.btn_modificar.TabIndex = 16
        Me.btn_modificar.Text = "Modificar"
        Me.btn_modificar.UseVisualStyleBackColor = True
        '
        'lbl_info_currency_total
        '
        Me.lbl_info_currency_total.AutoSize = True
        Me.lbl_info_currency_total.Location = New System.Drawing.Point(642, 321)
        Me.lbl_info_currency_total.Name = "lbl_info_currency_total"
        Me.lbl_info_currency_total.Size = New System.Drawing.Size(31, 13)
        Me.lbl_info_currency_total.TabIndex = 19
        Me.lbl_info_currency_total.Text = "MXN"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Location = New System.Drawing.Point(736, 392)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(100, 23)
        Me.btn_cancelar.TabIndex = 22
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'dtgrid_ratesdetails
        '
        Me.dtgrid_ratesdetails.AllowUserToAddRows = False
        Me.dtgrid_ratesdetails.AllowUserToResizeColumns = False
        Me.dtgrid_ratesdetails.AllowUserToResizeRows = False
        Me.dtgrid_ratesdetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dtgrid_ratesdetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgrid_ratesdetails.Location = New System.Drawing.Point(19, 215)
        Me.dtgrid_ratesdetails.Name = "dtgrid_ratesdetails"
        Me.dtgrid_ratesdetails.Size = New System.Drawing.Size(817, 92)
        Me.dtgrid_ratesdetails.TabIndex = 23
        '
        'txt_edit_totalNR
        '
        Me.txt_edit_totalNR.Location = New System.Drawing.Point(541, 344)
        Me.txt_edit_totalNR.Name = "txt_edit_totalNR"
        Me.txt_edit_totalNR.Size = New System.Drawing.Size(100, 20)
        Me.txt_edit_totalNR.TabIndex = 24
        Me.txt_edit_totalNR.Text = "0"
        Me.txt_edit_totalNR.Visible = False
        '
        'lbl_info_totalNR
        '
        Me.lbl_info_totalNR.AutoSize = True
        Me.lbl_info_totalNR.Location = New System.Drawing.Point(472, 347)
        Me.lbl_info_totalNR.Name = "lbl_info_totalNR"
        Me.lbl_info_totalNR.Size = New System.Drawing.Size(60, 13)
        Me.lbl_info_totalNR.TabIndex = 25
        Me.lbl_info_totalNR.Text = "Total Neto:"
        Me.lbl_info_totalNR.Visible = False
        '
        'lbl_info_currency_totalNR
        '
        Me.lbl_info_currency_totalNR.AutoSize = True
        Me.lbl_info_currency_totalNR.Location = New System.Drawing.Point(643, 347)
        Me.lbl_info_currency_totalNR.Name = "lbl_info_currency_totalNR"
        Me.lbl_info_currency_totalNR.Size = New System.Drawing.Size(31, 13)
        Me.lbl_info_currency_totalNR.TabIndex = 26
        Me.lbl_info_currency_totalNR.Text = "MXN"
        Me.lbl_info_currency_totalNR.Visible = False
        '
        'btn_add_row
        '
        Me.btn_add_row.Location = New System.Drawing.Point(702, 316)
        Me.btn_add_row.Name = "btn_add_row"
        Me.btn_add_row.Size = New System.Drawing.Size(57, 23)
        Me.btn_add_row.TabIndex = 27
        Me.btn_add_row.Text = "Agregar"
        Me.btn_add_row.UseVisualStyleBackColor = True
        '
        'btn_remove_row
        '
        Me.btn_remove_row.Location = New System.Drawing.Point(775, 316)
        Me.btn_remove_row.Name = "btn_remove_row"
        Me.btn_remove_row.Size = New System.Drawing.Size(57, 23)
        Me.btn_remove_row.TabIndex = 28
        Me.btn_remove_row.Text = "Eliminar"
        Me.btn_remove_row.UseVisualStyleBackColor = True
        '
        'cmb_tipo_habitacion
        '
        Me.cmb_tipo_habitacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_tipo_habitacion.FormattingEnabled = True
        Me.cmb_tipo_habitacion.Location = New System.Drawing.Point(435, 35)
        Me.cmb_tipo_habitacion.Name = "cmb_tipo_habitacion"
        Me.cmb_tipo_habitacion.Size = New System.Drawing.Size(167, 21)
        Me.cmb_tipo_habitacion.TabIndex = 29
        '
        'lbl_tipo_Habitacion
        '
        Me.lbl_tipo_Habitacion.AutoSize = True
        Me.lbl_tipo_Habitacion.Location = New System.Drawing.Point(319, 38)
        Me.lbl_tipo_Habitacion.Name = "lbl_tipo_Habitacion"
        Me.lbl_tipo_Habitacion.Size = New System.Drawing.Size(100, 13)
        Me.lbl_tipo_Habitacion.TabIndex = 30
        Me.lbl_tipo_Habitacion.Text = "Tipo de Habitacion:"
        '
        'cmb_mount_payment
        '
        Me.cmb_mount_payment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_mount_payment.FormattingEnabled = True
        Me.cmb_mount_payment.Location = New System.Drawing.Point(211, 156)
        Me.cmb_mount_payment.Name = "cmb_mount_payment"
        Me.cmb_mount_payment.Size = New System.Drawing.Size(90, 21)
        Me.cmb_mount_payment.TabIndex = 31
        '
        'lbl_months
        '
        Me.lbl_months.AutoSize = True
        Me.lbl_months.Location = New System.Drawing.Point(165, 159)
        Me.lbl_months.Name = "lbl_months"
        Me.lbl_months.Size = New System.Drawing.Size(40, 13)
        Me.lbl_months.TabIndex = 32
        Me.lbl_months.Text = "Pagos:"
        '
        'cmb_source
        '
        Me.cmb_source.AllowDrop = True
        Me.cmb_source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_source.FormattingEnabled = True
        Me.cmb_source.Location = New System.Drawing.Point(63, 156)
        Me.cmb_source.Name = "cmb_source"
        Me.cmb_source.Size = New System.Drawing.Size(83, 21)
        Me.cmb_source.TabIndex = 31
        '
        'lbl_source
        '
        Me.lbl_source.AutoSize = True
        Me.lbl_source.Location = New System.Drawing.Point(16, 159)
        Me.lbl_source.Name = "lbl_source"
        Me.lbl_source.Size = New System.Drawing.Size(41, 13)
        Me.lbl_source.TabIndex = 34
        Me.lbl_source.Text = "Origen:"
        '
        'lbl_noAuth
        '
        Me.lbl_noAuth.AutoSize = True
        Me.lbl_noAuth.Location = New System.Drawing.Point(15, 189)
        Me.lbl_noAuth.Name = "lbl_noAuth"
        Me.lbl_noAuth.Size = New System.Drawing.Size(103, 13)
        Me.lbl_noAuth.TabIndex = 35
        Me.lbl_noAuth.Text = "No. de Autorización:"
        '
        'AuthNum
        '
        Me.AuthNum.Location = New System.Drawing.Point(144, 186)
        Me.AuthNum.Name = "AuthNum"
        Me.AuthNum.Size = New System.Drawing.Size(100, 20)
        Me.AuthNum.TabIndex = 36
        '
        'cmb_status
        '
        Me.cmb_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_status.FormattingEnabled = True
        Me.cmb_status.Location = New System.Drawing.Point(435, 181)
        Me.cmb_status.Name = "cmb_status"
        Me.cmb_status.Size = New System.Drawing.Size(167, 21)
        Me.cmb_status.TabIndex = 37
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(370, 184)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Status:"
        '
        'chk_NR
        '
        Me.chk_NR.AutoSize = True
        Me.chk_NR.Location = New System.Drawing.Point(753, 195)
        Me.chk_NR.Name = "chk_NR"
        Me.chk_NR.Size = New System.Drawing.Size(79, 17)
        Me.chk_NR.TabIndex = 41
        Me.chk_NR.Text = "Tarifa Neta"
        Me.chk_NR.UseVisualStyleBackColor = True
        '
        'lbl_deposit
        '
        Me.lbl_deposit.AutoSize = True
        Me.lbl_deposit.Location = New System.Drawing.Point(16, 316)
        Me.lbl_deposit.Name = "lbl_deposit"
        Me.lbl_deposit.Size = New System.Drawing.Size(139, 13)
        Me.lbl_deposit.TabIndex = 42
        Me.lbl_deposit.Text = "Datos del deposito bancario"
        '
        'txt_DepositAmount
        '
        Me.txt_DepositAmount.Location = New System.Drawing.Point(350, 378)
        Me.txt_DepositAmount.Name = "txt_DepositAmount"
        Me.txt_DepositAmount.Size = New System.Drawing.Size(100, 20)
        Me.txt_DepositAmount.TabIndex = 45
        '
        'lbl_depositAmount
        '
        Me.lbl_depositAmount.AutoSize = True
        Me.lbl_depositAmount.Location = New System.Drawing.Point(228, 381)
        Me.lbl_depositAmount.Name = "lbl_depositAmount"
        Me.lbl_depositAmount.Size = New System.Drawing.Size(97, 13)
        Me.lbl_depositAmount.TabIndex = 44
        Me.lbl_depositAmount.Text = "Monto del deposito"
        '
        'lbl_depositCurrency
        '
        Me.lbl_depositCurrency.AutoSize = True
        Me.lbl_depositCurrency.Location = New System.Drawing.Point(456, 381)
        Me.lbl_depositCurrency.Name = "lbl_depositCurrency"
        Me.lbl_depositCurrency.Size = New System.Drawing.Size(31, 13)
        Me.lbl_depositCurrency.TabIndex = 48
        Me.lbl_depositCurrency.Text = "MXN"
        '
        'rbt_target_UV
        '
        Me.rbt_target_UV.AutoSize = True
        Me.rbt_target_UV.Location = New System.Drawing.Point(328, 329)
        Me.rbt_target_UV.Name = "rbt_target_UV"
        Me.rbt_target_UV.Size = New System.Drawing.Size(40, 17)
        Me.rbt_target_UV.TabIndex = 49
        Me.rbt_target_UV.TabStop = True
        Me.rbt_target_UV.Text = "UV"
        Me.rbt_target_UV.UseVisualStyleBackColor = True
        '
        'rbt_target_HTL
        '
        Me.rbt_target_HTL.AutoSize = True
        Me.rbt_target_HTL.Location = New System.Drawing.Point(387, 329)
        Me.rbt_target_HTL.Name = "rbt_target_HTL"
        Me.rbt_target_HTL.Size = New System.Drawing.Size(50, 17)
        Me.rbt_target_HTL.TabIndex = 50
        Me.rbt_target_HTL.TabStop = True
        Me.rbt_target_HTL.Text = "Hotel"
        Me.rbt_target_HTL.UseVisualStyleBackColor = True
        '
        'lbl_depositTarget
        '
        Me.lbl_depositTarget.AutoSize = True
        Me.lbl_depositTarget.Location = New System.Drawing.Point(228, 333)
        Me.lbl_depositTarget.Name = "lbl_depositTarget"
        Me.lbl_depositTarget.Size = New System.Drawing.Size(61, 13)
        Me.lbl_depositTarget.TabIndex = 51
        Me.lbl_depositTarget.Text = "Depositar a"
        '
        'tc_depositInfo
        '
        Me.tc_depositInfo.Controls.Add(Me.tp_esp)
        Me.tc_depositInfo.Controls.Add(Me.tp_eng)
        Me.tc_depositInfo.Location = New System.Drawing.Point(19, 333)
        Me.tc_depositInfo.Name = "tc_depositInfo"
        Me.tc_depositInfo.SelectedIndex = 0
        Me.tc_depositInfo.Size = New System.Drawing.Size(200, 100)
        Me.tc_depositInfo.TabIndex = 52
        '
        'tp_esp
        '
        Me.tp_esp.Controls.Add(Me.txt_deposit_esp)
        Me.tp_esp.Location = New System.Drawing.Point(4, 22)
        Me.tp_esp.Name = "tp_esp"
        Me.tp_esp.Padding = New System.Windows.Forms.Padding(3)
        Me.tp_esp.Size = New System.Drawing.Size(192, 74)
        Me.tp_esp.TabIndex = 0
        Me.tp_esp.Text = "TabPage1"
        Me.tp_esp.UseVisualStyleBackColor = True
        '
        'txt_deposit_esp
        '
        Me.txt_deposit_esp.Location = New System.Drawing.Point(0, -1)
        Me.txt_deposit_esp.Multiline = True
        Me.txt_deposit_esp.Name = "txt_deposit_esp"
        Me.txt_deposit_esp.Size = New System.Drawing.Size(192, 75)
        Me.txt_deposit_esp.TabIndex = 53
        '
        'tp_eng
        '
        Me.tp_eng.Controls.Add(Me.txt_deposit_eng)
        Me.tp_eng.Location = New System.Drawing.Point(4, 22)
        Me.tp_eng.Name = "tp_eng"
        Me.tp_eng.Padding = New System.Windows.Forms.Padding(3)
        Me.tp_eng.Size = New System.Drawing.Size(192, 74)
        Me.tp_eng.TabIndex = 1
        Me.tp_eng.Text = "TabPage2"
        Me.tp_eng.UseVisualStyleBackColor = True
        '
        'txt_deposit_eng
        '
        Me.txt_deposit_eng.Location = New System.Drawing.Point(0, -1)
        Me.txt_deposit_eng.Multiline = True
        Me.txt_deposit_eng.Name = "txt_deposit_eng"
        Me.txt_deposit_eng.Size = New System.Drawing.Size(192, 75)
        Me.txt_deposit_eng.TabIndex = 54
        '
        'txt_reference
        '
        Me.txt_reference.Location = New System.Drawing.Point(350, 407)
        Me.txt_reference.Name = "txt_reference"
        Me.txt_reference.Size = New System.Drawing.Size(100, 20)
        Me.txt_reference.TabIndex = 54
        '
        'lbl_reference
        '
        Me.lbl_reference.AutoSize = True
        Me.lbl_reference.Location = New System.Drawing.Point(228, 410)
        Me.lbl_reference.Name = "lbl_reference"
        Me.lbl_reference.Size = New System.Drawing.Size(59, 13)
        Me.lbl_reference.TabIndex = 53
        Me.lbl_reference.Text = "Referencia"
        '
        'cambiar_reservacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(851, 439)
        Me.Controls.Add(Me.txt_reference)
        Me.Controls.Add(Me.lbl_reference)
        Me.Controls.Add(Me.tc_depositInfo)
        Me.Controls.Add(Me.lbl_depositTarget)
        Me.Controls.Add(Me.rbt_target_HTL)
        Me.Controls.Add(Me.rbt_target_UV)
        Me.Controls.Add(Me.lbl_depositCurrency)
        Me.Controls.Add(Me.txt_DepositAmount)
        Me.Controls.Add(Me.lbl_depositAmount)
        Me.Controls.Add(Me.lbl_deposit)
        Me.Controls.Add(Me.chk_NR)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmb_status)
        Me.Controls.Add(Me.AuthNum)
        Me.Controls.Add(Me.lbl_noAuth)
        Me.Controls.Add(Me.lbl_source)
        Me.Controls.Add(Me.cmb_source)
        Me.Controls.Add(Me.lbl_months)
        Me.Controls.Add(Me.cmb_mount_payment)
        Me.Controls.Add(Me.lbl_tipo_Habitacion)
        Me.Controls.Add(Me.cmb_tipo_habitacion)
        Me.Controls.Add(Me.btn_remove_row)
        Me.Controls.Add(Me.btn_add_row)
        Me.Controls.Add(Me.lbl_info_currency_totalNR)
        Me.Controls.Add(Me.lbl_info_totalNR)
        Me.Controls.Add(Me.txt_edit_totalNR)
        Me.Controls.Add(Me.dtgrid_ratesdetails)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.lbl_info_currency_total)
        Me.Controls.Add(Me.btn_modificar)
        Me.Controls.Add(Me.dtp_out)
        Me.Controls.Add(Me.lbl_salida)
        Me.Controls.Add(Me.dtp_in)
        Me.Controls.Add(Me.lbl_llegada)
        Me.Controls.Add(Me.txt_edit_comentario)
        Me.Controls.Add(Me.txt_edit_peticion)
        Me.Controls.Add(Me.lbl_peticion)
        Me.Controls.Add(Me.lbl_comentario)
        Me.Controls.Add(Me.txt_edit_total)
        Me.Controls.Add(Me.lbl_info_total)
        Me.Controls.Add(Me.lbl_info_tarifa)
        Me.Controls.Add(Me.group_payment)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "cambiar_reservacion"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = CapaPresentacion.Idiomas.get_str("str_569")
        Me.group_payment.ResumeLayout(False)
        Me.group_payment.PerformLayout()
        CType(Me.dtgrid_ratesdetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tc_depositInfo.ResumeLayout(False)
        Me.tp_esp.ResumeLayout(False)
        Me.tp_esp.PerformLayout()
        Me.tp_eng.ResumeLayout(False)
        Me.tp_eng.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents group_payment As System.Windows.Forms.GroupBox
    Friend WithEvents rbt_deposito As System.Windows.Forms.RadioButton
    Friend WithEvents rdt_tarjeta As System.Windows.Forms.RadioButton
    Friend WithEvents lbl_info_tarifa As System.Windows.Forms.Label
    Friend WithEvents lbl_info_total As System.Windows.Forms.Label
    Friend WithEvents txt_edit_total As System.Windows.Forms.TextBox
    Friend WithEvents lbl_comentario As System.Windows.Forms.Label
    Friend WithEvents lbl_peticion As System.Windows.Forms.Label
    Friend WithEvents txt_edit_peticion As System.Windows.Forms.TextBox
    Friend WithEvents txt_edit_comentario As System.Windows.Forms.TextBox
    Friend WithEvents lbl_llegada As System.Windows.Forms.Label
    Friend WithEvents dtp_in As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_salida As System.Windows.Forms.Label
    Friend WithEvents dtp_out As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbt_paypal As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_pay_u As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_american_express As System.Windows.Forms.RadioButton
    Friend WithEvents btn_modificar As System.Windows.Forms.Button
    Friend WithEvents lbl_info_currency_total As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents rdt_pay_in_hotel As System.Windows.Forms.RadioButton
    Friend WithEvents dtgrid_ratesdetails As System.Windows.Forms.DataGridView
    Friend WithEvents txt_edit_totalNR As System.Windows.Forms.TextBox
    Friend WithEvents lbl_info_totalNR As System.Windows.Forms.Label
    Friend WithEvents lbl_info_currency_totalNR As System.Windows.Forms.Label
    Friend WithEvents btn_add_row As System.Windows.Forms.Button
    Friend WithEvents btn_remove_row As System.Windows.Forms.Button
    Friend WithEvents cmb_tipo_habitacion As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_tipo_Habitacion As System.Windows.Forms.Label
    Friend WithEvents cmb_mount_payment As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_months As System.Windows.Forms.Label
    Friend WithEvents cmb_source As ComboBox
    Friend WithEvents lbl_source As Label
    Friend WithEvents lbl_noAuth As Label
    Friend WithEvents AuthNum As TextBox
    Friend WithEvents rbt_conekta As RadioButton
    Friend WithEvents cmb_status As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents chk_NR As CheckBox
    Friend WithEvents lbl_deposit As Label
    Friend WithEvents txt_DepositAmount As TextBox
    Friend WithEvents lbl_depositAmount As Label
    Friend WithEvents lbl_depositCurrency As Label
    Friend WithEvents rbt_target_UV As RadioButton
    Friend WithEvents rbt_target_HTL As RadioButton
    Friend WithEvents lbl_depositTarget As Label
    Friend WithEvents tc_depositInfo As TabControl
    Friend WithEvents tp_esp As TabPage
    Friend WithEvents tp_eng As TabPage
    Friend WithEvents txt_deposit_eng As TextBox
    Friend WithEvents txt_deposit_esp As TextBox
    Friend WithEvents txt_reference As TextBox
    Friend WithEvents lbl_reference As Label
End Class
