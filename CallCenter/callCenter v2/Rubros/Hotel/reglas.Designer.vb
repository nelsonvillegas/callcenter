<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reglas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reglas))
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.data_lbl_habitaciones = New System.Windows.Forms.Label
        Me.lbl_habitaciones = New System.Windows.Forms.Label
        Me.data_lbl_ninos = New System.Windows.Forms.Label
        Me.data_lbl_noches = New System.Windows.Forms.Label
        Me.lbl_ninos = New System.Windows.Forms.Label
        Me.lbl_numNoches = New System.Windows.Forms.Label
        Me.data_lbl_adultos = New System.Windows.Forms.Label
        Me.lbl_adultos = New System.Windows.Forms.Label
        Me.data_lbl_fechas = New System.Windows.Forms.Label
        Me.lbl_fecha = New System.Windows.Forms.Label
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_aceptar.Location = New System.Drawing.Point(535, 561)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_aceptar.TabIndex = 3
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.PictureBox2)
        Me.GroupBox1.Controls.Add(Me.data_lbl_habitaciones)
        Me.GroupBox1.Controls.Add(Me.lbl_habitaciones)
        Me.GroupBox1.Controls.Add(Me.data_lbl_ninos)
        Me.GroupBox1.Controls.Add(Me.data_lbl_noches)
        Me.GroupBox1.Controls.Add(Me.lbl_ninos)
        Me.GroupBox1.Controls.Add(Me.lbl_numNoches)
        Me.GroupBox1.Controls.Add(Me.data_lbl_adultos)
        Me.GroupBox1.Controls.Add(Me.lbl_adultos)
        Me.GroupBox1.Controls.Add(Me.data_lbl_fechas)
        Me.GroupBox1.Controls.Add(Me.lbl_fecha)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(598, 78)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Informacion"
        '
        'data_lbl_habitaciones
        '
        Me.data_lbl_habitaciones.AutoSize = True
        Me.data_lbl_habitaciones.ForeColor = System.Drawing.SystemColors.ControlText
        Me.data_lbl_habitaciones.Location = New System.Drawing.Point(415, 51)
        Me.data_lbl_habitaciones.Name = "data_lbl_habitaciones"
        Me.data_lbl_habitaciones.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_habitaciones.TabIndex = 29
        Me.data_lbl_habitaciones.Text = "----"
        '
        'lbl_habitaciones
        '
        Me.lbl_habitaciones.AutoSize = True
        Me.lbl_habitaciones.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_habitaciones.Location = New System.Drawing.Point(334, 51)
        Me.lbl_habitaciones.Name = "lbl_habitaciones"
        Me.lbl_habitaciones.Size = New System.Drawing.Size(72, 13)
        Me.lbl_habitaciones.TabIndex = 28
        Me.lbl_habitaciones.Text = "Habitaciones:"
        '
        'data_lbl_ninos
        '
        Me.data_lbl_ninos.AutoSize = True
        Me.data_lbl_ninos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.data_lbl_ninos.Location = New System.Drawing.Point(415, 38)
        Me.data_lbl_ninos.Name = "data_lbl_ninos"
        Me.data_lbl_ninos.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_ninos.TabIndex = 27
        Me.data_lbl_ninos.Text = "----"
        '
        'data_lbl_noches
        '
        Me.data_lbl_noches.AutoSize = True
        Me.data_lbl_noches.ForeColor = System.Drawing.SystemColors.ControlText
        Me.data_lbl_noches.Location = New System.Drawing.Point(114, 38)
        Me.data_lbl_noches.Name = "data_lbl_noches"
        Me.data_lbl_noches.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_noches.TabIndex = 19
        Me.data_lbl_noches.Text = "----"
        '
        'lbl_ninos
        '
        Me.lbl_ninos.AutoSize = True
        Me.lbl_ninos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_ninos.Location = New System.Drawing.Point(334, 38)
        Me.lbl_ninos.Name = "lbl_ninos"
        Me.lbl_ninos.Size = New System.Drawing.Size(37, 13)
        Me.lbl_ninos.TabIndex = 26
        Me.lbl_ninos.Text = "Ni�os:"
        '
        'lbl_numNoches
        '
        Me.lbl_numNoches.AutoSize = True
        Me.lbl_numNoches.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_numNoches.Location = New System.Drawing.Point(33, 38)
        Me.lbl_numNoches.Name = "lbl_numNoches"
        Me.lbl_numNoches.Size = New System.Drawing.Size(75, 13)
        Me.lbl_numNoches.TabIndex = 18
        Me.lbl_numNoches.Text = "N�m. Noches:"
        '
        'data_lbl_adultos
        '
        Me.data_lbl_adultos.AutoSize = True
        Me.data_lbl_adultos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.data_lbl_adultos.Location = New System.Drawing.Point(415, 25)
        Me.data_lbl_adultos.Name = "data_lbl_adultos"
        Me.data_lbl_adultos.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_adultos.TabIndex = 25
        Me.data_lbl_adultos.Text = "----"
        '
        'lbl_adultos
        '
        Me.lbl_adultos.AutoSize = True
        Me.lbl_adultos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_adultos.Location = New System.Drawing.Point(334, 25)
        Me.lbl_adultos.Name = "lbl_adultos"
        Me.lbl_adultos.Size = New System.Drawing.Size(45, 13)
        Me.lbl_adultos.TabIndex = 24
        Me.lbl_adultos.Text = "Adultos:"
        '
        'data_lbl_fechas
        '
        Me.data_lbl_fechas.AutoSize = True
        Me.data_lbl_fechas.ForeColor = System.Drawing.SystemColors.ControlText
        Me.data_lbl_fechas.Location = New System.Drawing.Point(79, 25)
        Me.data_lbl_fechas.Name = "data_lbl_fechas"
        Me.data_lbl_fechas.Size = New System.Drawing.Size(73, 13)
        Me.data_lbl_fechas.TabIndex = 5
        Me.data_lbl_fechas.Text = "----------------------"
        '
        'lbl_fecha
        '
        Me.lbl_fecha.AutoSize = True
        Me.lbl_fecha.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_fecha.Location = New System.Drawing.Point(33, 25)
        Me.lbl_fecha.Name = "lbl_fecha"
        Me.lbl_fecha.Size = New System.Drawing.Size(40, 13)
        Me.lbl_fecha.TabIndex = 4
        Me.lbl_fecha.Text = "Fecha:"
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 91)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(598, 464)
        Me.WebBrowser1.TabIndex = 5
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(36, 54)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 30
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'reglas
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_aceptar
        Me.ClientSize = New System.Drawing.Size(622, 596)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "reglas"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reglas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents data_lbl_fechas As System.Windows.Forms.Label
    Friend WithEvents lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents data_lbl_noches As System.Windows.Forms.Label
    Friend WithEvents lbl_numNoches As System.Windows.Forms.Label
    Friend WithEvents data_lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents data_lbl_adultos As System.Windows.Forms.Label
    Friend WithEvents lbl_adultos As System.Windows.Forms.Label
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents data_lbl_habitaciones As System.Windows.Forms.Label
    Friend WithEvents lbl_habitaciones As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
