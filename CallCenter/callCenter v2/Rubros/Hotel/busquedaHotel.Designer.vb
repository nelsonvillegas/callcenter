<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class busquedaHotel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(busquedaHotel))
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook()
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim T_Habitaciones1 As callCenter.uc_habitaciones.T_Habitaciones = New callCenter.uc_habitaciones.T_Habitaciones()
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txt_Rateplan = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Conv = New System.Windows.Forms.TextBox()
        Me.btnPasiva = New System.Windows.Forms.Button()
        Me.cmbHotel = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.btnHotel = New System.Windows.Forms.Button()
        Me.cmbArea = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_detener = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.lbl_millas = New System.Windows.Forms.Label()
        Me.ComboSearch1 = New callCenter.ComboSearch()
        Me.lbl_distancia = New System.Windows.Forms.Label()
        Me.btn_buscar = New System.Windows.Forms.Button()
        Me.uce_moneda = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.uce_distancia = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.ugb_desplegar = New Infragistics.Win.Misc.UltraGroupBox()
        Me.txtMax = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMin = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbt_tarifas = New System.Windows.Forms.RadioButton()
        Me.rbt_hotelesDisponibles = New System.Windows.Forms.RadioButton()
        Me.rbt_todosHoteles = New System.Windows.Forms.RadioButton()
        Me.lbl_moneda = New System.Windows.Forms.Label()
        Me.ugb_buscarPor = New Infragistics.Win.Misc.UltraGroupBox()
        Me.rbt_Hotel = New System.Windows.Forms.RadioButton()
        Me.rbt_aeropuerto = New System.Windows.Forms.RadioButton()
        Me.rbt_ciudad = New System.Windows.Forms.RadioButton()
        Me.txt_noches = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_noches = New System.Windows.Forms.Label()
        Me.lbl_llegada = New System.Windows.Forms.Label()
        Me.lbl_salida = New System.Windows.Forms.Label()
        Me.dtp_out = New System.Windows.Forms.DateTimePicker()
        Me.dtp_in = New System.Windows.Forms.DateTimePicker()
        Me.txt_codProm = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_lugar = New System.Windows.Forms.Label()
        Me.lblConvenio = New System.Windows.Forms.Label()
        Me.pbx_6 = New System.Windows.Forms.PictureBox()
        Me.lbl_codProm = New System.Windows.Forms.Label()
        Me.pbx_5 = New System.Windows.Forms.PictureBox()
        Me.pbx_4 = New System.Windows.Forms.PictureBox()
        Me.pbx_3 = New System.Windows.Forms.PictureBox()
        Me.pbx_2 = New System.Windows.Forms.PictureBox()
        Me.pbx_1 = New System.Windows.Forms.PictureBox()
        Me.data_lbl_totalHoteles = New System.Windows.Forms.Label()
        Me.lbl_totalHoteles = New System.Windows.Forms.Label()
        Me.Uc_habitaciones1 = New callCenter.uc_habitaciones()
        Me.Panel2 = New System.Windows.Forms.Panel()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.cmbHotel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_distancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ugb_desplegar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugb_desplegar.SuspendLayout()
        CType(Me.txtMax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ugb_buscarPor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugb_buscarPor.SuspendLayout()
        CType(Me.txt_noches, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_codProm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbx_1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraGrid1
        '
        Me.UltraGrid1.ContextMenuStrip = Me.ContextMenuStrip1
        Appearance30.BackColor = System.Drawing.Color.White
        Appearance30.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance30
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance31.ImageBackground = CType(resources.GetObject("Appearance31.ImageBackground"), System.Drawing.Image)
        Appearance31.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance31.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance31.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance31
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance28.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance28
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance29.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance29.FontData.Name = "Trebuchet MS"
        Appearance29.FontData.SizeInPoints = 9.0!
        Appearance29.ForeColor = System.Drawing.Color.White
        Appearance29.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance29
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance32.FontData.BoldAsString = "True"
        Appearance32.FontData.Name = "Trebuchet MS"
        Appearance32.FontData.SizeInPoints = 10.0!
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance32
        Appearance33.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance33
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance34.BackColor2 = System.Drawing.SystemColors.Control
        Appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance34.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance34
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance13.BackColor = System.Drawing.SystemColors.Window
        Appearance13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance13
        Appearance14.BackColor = System.Drawing.SystemColors.Highlight
        Appearance14.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance14
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance15.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance15
        Appearance16.BorderColor = System.Drawing.Color.Transparent
        Appearance16.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance16
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance17
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance21.BackColor = System.Drawing.SystemColors.Control
        Appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance21.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance21
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance22.FontData.BoldAsString = "True"
        Appearance22.FontData.Name = "Trebuchet MS"
        Appearance22.FontData.SizeInPoints = 10.0!
        Appearance22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance22.ImageBackground = CType(resources.GetObject("Appearance22.ImageBackground"), System.Drawing.Image)
        Appearance22.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance22.TextHAlignAsString = "Left"
        Appearance22.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance22
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance1
        Appearance23.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance24
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance25.BorderColor = System.Drawing.Color.Transparent
        Appearance25.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance25
        Appearance26.BorderColor = System.Drawing.Color.Transparent
        Appearance26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance26.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance26
        Appearance27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance27
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance35.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance37.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance38
        Appearance39.ImageBackground = CType(resources.GetObject("Appearance39.ImageBackground"), System.Drawing.Image)
        Appearance39.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance39.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance39
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 0)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(1242, 323)
        Me.UltraGrid1.TabIndex = 0
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.txt_Rateplan)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_Conv)
        Me.Panel1.Controls.Add(Me.btnPasiva)
        Me.Panel1.Controls.Add(Me.cmbHotel)
        Me.Panel1.Controls.Add(Me.btnHotel)
        Me.Panel1.Controls.Add(Me.cmbArea)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btn_detener)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.lbl_millas)
        Me.Panel1.Controls.Add(Me.ComboSearch1)
        Me.Panel1.Controls.Add(Me.lbl_distancia)
        Me.Panel1.Controls.Add(Me.btn_buscar)
        Me.Panel1.Controls.Add(Me.uce_moneda)
        Me.Panel1.Controls.Add(Me.uce_distancia)
        Me.Panel1.Controls.Add(Me.ugb_desplegar)
        Me.Panel1.Controls.Add(Me.lbl_moneda)
        Me.Panel1.Controls.Add(Me.ugb_buscarPor)
        Me.Panel1.Controls.Add(Me.txt_noches)
        Me.Panel1.Controls.Add(Me.lbl_noches)
        Me.Panel1.Controls.Add(Me.lbl_llegada)
        Me.Panel1.Controls.Add(Me.lbl_salida)
        Me.Panel1.Controls.Add(Me.dtp_out)
        Me.Panel1.Controls.Add(Me.dtp_in)
        Me.Panel1.Controls.Add(Me.txt_codProm)
        Me.Panel1.Controls.Add(Me.lbl_lugar)
        Me.Panel1.Controls.Add(Me.lblConvenio)
        Me.Panel1.Controls.Add(Me.pbx_6)
        Me.Panel1.Controls.Add(Me.lbl_codProm)
        Me.Panel1.Controls.Add(Me.pbx_5)
        Me.Panel1.Controls.Add(Me.pbx_4)
        Me.Panel1.Controls.Add(Me.pbx_3)
        Me.Panel1.Controls.Add(Me.pbx_2)
        Me.Panel1.Controls.Add(Me.pbx_1)
        Me.Panel1.Controls.Add(Me.data_lbl_totalHoteles)
        Me.Panel1.Controls.Add(Me.lbl_totalHoteles)
        Me.Panel1.Controls.Add(Me.Uc_habitaciones1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1242, 143)
        Me.Panel1.TabIndex = 2
        '
        'txt_Rateplan
        '
        Me.txt_Rateplan.Location = New System.Drawing.Point(849, 118)
        Me.txt_Rateplan.Name = "txt_Rateplan"
        Me.txt_Rateplan.Size = New System.Drawing.Size(181, 20)
        Me.txt_Rateplan.TabIndex = 52
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(771, 121)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 51
        Me.Label4.Text = "Rate Plan :"
        '
        'txt_Conv
        '
        Me.txt_Conv.Location = New System.Drawing.Point(849, 92)
        Me.txt_Conv.Name = "txt_Conv"
        Me.txt_Conv.Size = New System.Drawing.Size(181, 20)
        Me.txt_Conv.TabIndex = 50
        '
        'btnPasiva
        '
        Me.btnPasiva.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnPasiva.Location = New System.Drawing.Point(955, 36)
        Me.btnPasiva.Name = "btnPasiva"
        Me.btnPasiva.Size = New System.Drawing.Size(75, 49)
        Me.btnPasiva.TabIndex = 49
        Me.btnPasiva.Text = "Reservaci�n Pasiva"
        Me.btnPasiva.UseVisualStyleBackColor = True
        '
        'cmbHotel
        '
        Me.cmbHotel.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.cmbHotel.Location = New System.Drawing.Point(545, 12)
        Me.cmbHotel.Name = "cmbHotel"
        Me.cmbHotel.Size = New System.Drawing.Size(215, 21)
        Me.cmbHotel.TabIndex = 6
        Me.cmbHotel.Visible = False
        '
        'btnHotel
        '
        Me.btnHotel.Location = New System.Drawing.Point(1140, 89)
        Me.btnHotel.Name = "btnHotel"
        Me.btnHotel.Size = New System.Drawing.Size(75, 34)
        Me.btnHotel.TabIndex = 47
        Me.btnHotel.Text = "Ubicar Hotel"
        Me.btnHotel.UseVisualStyleBackColor = True
        '
        'cmbArea
        '
        Me.cmbArea.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.cmbArea.Location = New System.Drawing.Point(545, 37)
        Me.cmbArea.Name = "cmbArea"
        Me.cmbArea.Size = New System.Drawing.Size(215, 21)
        Me.cmbArea.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(483, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 46
        Me.Label1.Text = "Area:"
        '
        'btn_detener
        '
        Me.btn_detener.Image = CType(resources.GetObject("btn_detener.Image"), System.Drawing.Image)
        Me.btn_detener.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_detener.Location = New System.Drawing.Point(1140, 12)
        Me.btn_detener.Name = "btn_detener"
        Me.btn_detener.Size = New System.Drawing.Size(75, 56)
        Me.btn_detener.TabIndex = 14
        Me.btn_detener.Text = "Detener"
        Me.btn_detener.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_detener.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(1059, 95)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 44
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'lbl_millas
        '
        Me.lbl_millas.AutoSize = True
        Me.lbl_millas.Location = New System.Drawing.Point(620, 68)
        Me.lbl_millas.Name = "lbl_millas"
        Me.lbl_millas.Size = New System.Drawing.Size(124, 13)
        Me.lbl_millas.TabIndex = 0
        Me.lbl_millas.Text = "millas cerca de la Ciudad"
        '
        'ComboSearch1
        '
        Me.ComboSearch1.Location = New System.Drawing.Point(545, 12)
        Me.ComboSearch1.Name = "ComboSearch1"
        Me.ComboSearch1.Size = New System.Drawing.Size(212, 21)
        Me.ComboSearch1.TabIndex = 6
        '
        'lbl_distancia
        '
        Me.lbl_distancia.AutoSize = True
        Me.lbl_distancia.Location = New System.Drawing.Point(483, 68)
        Me.lbl_distancia.Name = "lbl_distancia"
        Me.lbl_distancia.Size = New System.Drawing.Size(54, 13)
        Me.lbl_distancia.TabIndex = 0
        Me.lbl_distancia.Text = "Distancia:"
        '
        'btn_buscar
        '
        Me.btn_buscar.Image = CType(resources.GetObject("btn_buscar.Image"), System.Drawing.Image)
        Me.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_buscar.Location = New System.Drawing.Point(1059, 12)
        Me.btn_buscar.Name = "btn_buscar"
        Me.btn_buscar.Size = New System.Drawing.Size(75, 56)
        Me.btn_buscar.TabIndex = 13
        Me.btn_buscar.Text = "Buscar"
        Me.btn_buscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_buscar.UseVisualStyleBackColor = True
        '
        'uce_moneda
        '
        Me.uce_moneda.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_moneda.Location = New System.Drawing.Point(77, 89)
        Me.uce_moneda.Name = "uce_moneda"
        Me.uce_moneda.Size = New System.Drawing.Size(106, 21)
        Me.uce_moneda.TabIndex = 4
        '
        'uce_distancia
        '
        Me.uce_distancia.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_distancia.Location = New System.Drawing.Point(545, 64)
        Me.uce_distancia.Name = "uce_distancia"
        Me.uce_distancia.Size = New System.Drawing.Size(53, 21)
        Me.uce_distancia.TabIndex = 9
        '
        'ugb_desplegar
        '
        Me.ugb_desplegar.Controls.Add(Me.txtMax)
        Me.ugb_desplegar.Controls.Add(Me.Label3)
        Me.ugb_desplegar.Controls.Add(Me.txtMin)
        Me.ugb_desplegar.Controls.Add(Me.Label2)
        Me.ugb_desplegar.Controls.Add(Me.rbt_tarifas)
        Me.ugb_desplegar.Controls.Add(Me.rbt_hotelesDisponibles)
        Me.ugb_desplegar.Controls.Add(Me.rbt_todosHoteles)
        Me.ugb_desplegar.Location = New System.Drawing.Point(289, 12)
        Me.ugb_desplegar.Name = "ugb_desplegar"
        Me.ugb_desplegar.Size = New System.Drawing.Size(188, 111)
        Me.ugb_desplegar.TabIndex = 5
        Me.ugb_desplegar.Text = "Desplegar"
        Me.ugb_desplegar.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'txtMax
        '
        Me.txtMax.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtMax.Location = New System.Drawing.Point(127, 88)
        Me.txtMax.MaxLength = 10
        Me.txtMax.Name = "txtMax"
        Me.txtMax.Size = New System.Drawing.Size(55, 19)
        Me.txtMax.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(95, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Max.:"
        '
        'txtMin
        '
        Me.txtMin.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtMin.Location = New System.Drawing.Point(36, 89)
        Me.txtMin.MaxLength = 10
        Me.txtMin.Name = "txtMin"
        Me.txtMin.Size = New System.Drawing.Size(55, 19)
        Me.txtMin.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(6, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Min. :"
        '
        'rbt_tarifas
        '
        Me.rbt_tarifas.AutoSize = True
        Me.rbt_tarifas.BackColor = System.Drawing.Color.Transparent
        Me.rbt_tarifas.Location = New System.Drawing.Point(6, 72)
        Me.rbt_tarifas.Name = "rbt_tarifas"
        Me.rbt_tarifas.Size = New System.Drawing.Size(57, 17)
        Me.rbt_tarifas.TabIndex = 2
        Me.rbt_tarifas.Text = "Tarifas"
        Me.rbt_tarifas.UseVisualStyleBackColor = False
        '
        'rbt_hotelesDisponibles
        '
        Me.rbt_hotelesDisponibles.AutoSize = True
        Me.rbt_hotelesDisponibles.BackColor = System.Drawing.Color.Transparent
        Me.rbt_hotelesDisponibles.Location = New System.Drawing.Point(6, 49)
        Me.rbt_hotelesDisponibles.Name = "rbt_hotelesDisponibles"
        Me.rbt_hotelesDisponibles.Size = New System.Drawing.Size(138, 17)
        Me.rbt_hotelesDisponibles.TabIndex = 1
        Me.rbt_hotelesDisponibles.Text = "Solo hoteles disponibles"
        Me.rbt_hotelesDisponibles.UseVisualStyleBackColor = False
        '
        'rbt_todosHoteles
        '
        Me.rbt_todosHoteles.AutoSize = True
        Me.rbt_todosHoteles.BackColor = System.Drawing.Color.Transparent
        Me.rbt_todosHoteles.Checked = True
        Me.rbt_todosHoteles.Location = New System.Drawing.Point(6, 26)
        Me.rbt_todosHoteles.Name = "rbt_todosHoteles"
        Me.rbt_todosHoteles.Size = New System.Drawing.Size(108, 17)
        Me.rbt_todosHoteles.TabIndex = 0
        Me.rbt_todosHoteles.TabStop = True
        Me.rbt_todosHoteles.Text = "Todos los hoteles"
        Me.rbt_todosHoteles.UseVisualStyleBackColor = False
        '
        'lbl_moneda
        '
        Me.lbl_moneda.AutoSize = True
        Me.lbl_moneda.Location = New System.Drawing.Point(15, 93)
        Me.lbl_moneda.Name = "lbl_moneda"
        Me.lbl_moneda.Size = New System.Drawing.Size(49, 13)
        Me.lbl_moneda.TabIndex = 0
        Me.lbl_moneda.Text = "Moneda:"
        '
        'ugb_buscarPor
        '
        Me.ugb_buscarPor.Controls.Add(Me.rbt_Hotel)
        Me.ugb_buscarPor.Controls.Add(Me.rbt_aeropuerto)
        Me.ugb_buscarPor.Controls.Add(Me.rbt_ciudad)
        Me.ugb_buscarPor.Location = New System.Drawing.Point(189, 12)
        Me.ugb_buscarPor.Name = "ugb_buscarPor"
        Me.ugb_buscarPor.Size = New System.Drawing.Size(88, 111)
        Me.ugb_buscarPor.TabIndex = 4
        Me.ugb_buscarPor.Text = "Buscar por"
        Me.ugb_buscarPor.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'rbt_Hotel
        '
        Me.rbt_Hotel.AutoSize = True
        Me.rbt_Hotel.BackColor = System.Drawing.Color.Transparent
        Me.rbt_Hotel.Location = New System.Drawing.Point(6, 72)
        Me.rbt_Hotel.Name = "rbt_Hotel"
        Me.rbt_Hotel.Size = New System.Drawing.Size(50, 17)
        Me.rbt_Hotel.TabIndex = 2
        Me.rbt_Hotel.Text = "Hotel"
        Me.rbt_Hotel.UseVisualStyleBackColor = False
        '
        'rbt_aeropuerto
        '
        Me.rbt_aeropuerto.AutoSize = True
        Me.rbt_aeropuerto.BackColor = System.Drawing.Color.Transparent
        Me.rbt_aeropuerto.Location = New System.Drawing.Point(6, 49)
        Me.rbt_aeropuerto.Name = "rbt_aeropuerto"
        Me.rbt_aeropuerto.Size = New System.Drawing.Size(77, 17)
        Me.rbt_aeropuerto.TabIndex = 1
        Me.rbt_aeropuerto.Text = "Aeropuerto"
        Me.rbt_aeropuerto.UseVisualStyleBackColor = False
        '
        'rbt_ciudad
        '
        Me.rbt_ciudad.AutoSize = True
        Me.rbt_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.rbt_ciudad.Checked = True
        Me.rbt_ciudad.Location = New System.Drawing.Point(6, 26)
        Me.rbt_ciudad.Name = "rbt_ciudad"
        Me.rbt_ciudad.Size = New System.Drawing.Size(58, 17)
        Me.rbt_ciudad.TabIndex = 0
        Me.rbt_ciudad.TabStop = True
        Me.rbt_ciudad.Text = "Ciudad"
        Me.rbt_ciudad.UseVisualStyleBackColor = False
        '
        'txt_noches
        '
        Me.txt_noches.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_noches.Location = New System.Drawing.Point(77, 64)
        Me.txt_noches.MaxLength = 3
        Me.txt_noches.Name = "txt_noches"
        Me.txt_noches.Size = New System.Drawing.Size(37, 19)
        Me.txt_noches.TabIndex = 3
        '
        'lbl_noches
        '
        Me.lbl_noches.AutoSize = True
        Me.lbl_noches.Location = New System.Drawing.Point(14, 68)
        Me.lbl_noches.Name = "lbl_noches"
        Me.lbl_noches.Size = New System.Drawing.Size(47, 13)
        Me.lbl_noches.TabIndex = 0
        Me.lbl_noches.Text = "Noches:"
        '
        'lbl_llegada
        '
        Me.lbl_llegada.AutoSize = True
        Me.lbl_llegada.Location = New System.Drawing.Point(14, 18)
        Me.lbl_llegada.Name = "lbl_llegada"
        Me.lbl_llegada.Size = New System.Drawing.Size(48, 13)
        Me.lbl_llegada.TabIndex = 0
        Me.lbl_llegada.Text = "Llegada:"
        '
        'lbl_salida
        '
        Me.lbl_salida.AutoSize = True
        Me.lbl_salida.Location = New System.Drawing.Point(14, 44)
        Me.lbl_salida.Name = "lbl_salida"
        Me.lbl_salida.Size = New System.Drawing.Size(39, 13)
        Me.lbl_salida.TabIndex = 0
        Me.lbl_salida.Text = "Salida:"
        '
        'dtp_out
        '
        Me.dtp_out.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_out.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_out.Location = New System.Drawing.Point(77, 38)
        Me.dtp_out.Name = "dtp_out"
        Me.dtp_out.Size = New System.Drawing.Size(106, 20)
        Me.dtp_out.TabIndex = 2
        '
        'dtp_in
        '
        Me.dtp_in.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_in.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_in.Location = New System.Drawing.Point(77, 12)
        Me.dtp_in.Name = "dtp_in"
        Me.dtp_in.Size = New System.Drawing.Size(106, 20)
        Me.dtp_in.TabIndex = 1
        '
        'txt_codProm
        '
        Me.txt_codProm.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_codProm.Location = New System.Drawing.Point(559, 93)
        Me.txt_codProm.Name = "txt_codProm"
        Me.txt_codProm.Size = New System.Drawing.Size(100, 19)
        Me.txt_codProm.TabIndex = 10
        '
        'lbl_lugar
        '
        Me.lbl_lugar.AutoSize = True
        Me.lbl_lugar.BackColor = System.Drawing.Color.White
        Me.lbl_lugar.Location = New System.Drawing.Point(483, 15)
        Me.lbl_lugar.Name = "lbl_lugar"
        Me.lbl_lugar.Size = New System.Drawing.Size(37, 13)
        Me.lbl_lugar.TabIndex = 0
        Me.lbl_lugar.Text = "Lugar:"
        '
        'lblConvenio
        '
        Me.lblConvenio.AutoSize = True
        Me.lblConvenio.Location = New System.Drawing.Point(771, 95)
        Me.lblConvenio.Name = "lblConvenio"
        Me.lblConvenio.Size = New System.Drawing.Size(58, 13)
        Me.lblConvenio.TabIndex = 0
        Me.lblConvenio.Text = "Convenio :"
        '
        'pbx_6
        '
        Me.pbx_6.Image = Global.callCenter.My.Resources.Resources.Star0
        Me.pbx_6.Location = New System.Drawing.Point(387, 107)
        Me.pbx_6.Name = "pbx_6"
        Me.pbx_6.Size = New System.Drawing.Size(12, 13)
        Me.pbx_6.TabIndex = 43
        Me.pbx_6.TabStop = False
        Me.pbx_6.Visible = False
        '
        'lbl_codProm
        '
        Me.lbl_codProm.AutoSize = True
        Me.lbl_codProm.Location = New System.Drawing.Point(483, 95)
        Me.lbl_codProm.Name = "lbl_codProm"
        Me.lbl_codProm.Size = New System.Drawing.Size(62, 13)
        Me.lbl_codProm.TabIndex = 0
        Me.lbl_codProm.Text = "Cod. Prom.:"
        '
        'pbx_5
        '
        Me.pbx_5.Image = Global.callCenter.My.Resources.Resources._5
        Me.pbx_5.Location = New System.Drawing.Point(369, 107)
        Me.pbx_5.Name = "pbx_5"
        Me.pbx_5.Size = New System.Drawing.Size(12, 13)
        Me.pbx_5.TabIndex = 28
        Me.pbx_5.TabStop = False
        Me.pbx_5.Visible = False
        '
        'pbx_4
        '
        Me.pbx_4.Image = Global.callCenter.My.Resources.Resources._4
        Me.pbx_4.Location = New System.Drawing.Point(351, 107)
        Me.pbx_4.Name = "pbx_4"
        Me.pbx_4.Size = New System.Drawing.Size(12, 13)
        Me.pbx_4.TabIndex = 27
        Me.pbx_4.TabStop = False
        Me.pbx_4.Visible = False
        '
        'pbx_3
        '
        Me.pbx_3.Image = Global.callCenter.My.Resources.Resources._3
        Me.pbx_3.Location = New System.Drawing.Point(333, 107)
        Me.pbx_3.Name = "pbx_3"
        Me.pbx_3.Size = New System.Drawing.Size(12, 13)
        Me.pbx_3.TabIndex = 26
        Me.pbx_3.TabStop = False
        Me.pbx_3.Visible = False
        '
        'pbx_2
        '
        Me.pbx_2.Image = Global.callCenter.My.Resources.Resources._2
        Me.pbx_2.Location = New System.Drawing.Point(315, 107)
        Me.pbx_2.Name = "pbx_2"
        Me.pbx_2.Size = New System.Drawing.Size(12, 13)
        Me.pbx_2.TabIndex = 25
        Me.pbx_2.TabStop = False
        Me.pbx_2.Visible = False
        '
        'pbx_1
        '
        Me.pbx_1.Image = Global.callCenter.My.Resources.Resources._1
        Me.pbx_1.Location = New System.Drawing.Point(297, 107)
        Me.pbx_1.Name = "pbx_1"
        Me.pbx_1.Size = New System.Drawing.Size(12, 13)
        Me.pbx_1.TabIndex = 24
        Me.pbx_1.TabStop = False
        Me.pbx_1.Visible = False
        '
        'data_lbl_totalHoteles
        '
        Me.data_lbl_totalHoteles.AutoSize = True
        Me.data_lbl_totalHoteles.Location = New System.Drawing.Point(1148, 74)
        Me.data_lbl_totalHoteles.Name = "data_lbl_totalHoteles"
        Me.data_lbl_totalHoteles.Size = New System.Drawing.Size(58, 13)
        Me.data_lbl_totalHoteles.TabIndex = 0
        Me.data_lbl_totalHoteles.Text = "-----------------"
        '
        'lbl_totalHoteles
        '
        Me.lbl_totalHoteles.AutoSize = True
        Me.lbl_totalHoteles.Location = New System.Drawing.Point(1056, 74)
        Me.lbl_totalHoteles.Name = "lbl_totalHoteles"
        Me.lbl_totalHoteles.Size = New System.Drawing.Size(86, 13)
        Me.lbl_totalHoteles.TabIndex = 0
        Me.lbl_totalHoteles.Text = "Total de hoteles:"
        '
        'Uc_habitaciones1
        '
        T_Habitaciones1.TotalAdultos = 1
        T_Habitaciones1.TotalHabitaciones = 1
        T_Habitaciones1.TotalNinos = 0
        Me.Uc_habitaciones1.Habitaciones = T_Habitaciones1
        Me.Uc_habitaciones1.Location = New System.Drawing.Point(766, 8)
        Me.Uc_habitaciones1.Name = "Uc_habitaciones1"
        Me.Uc_habitaciones1.Size = New System.Drawing.Size(275, 88)
        Me.Uc_habitaciones1.TabIndex = 11
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.UltraGrid1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 143)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1242, 323)
        Me.Panel2.TabIndex = 3
        '
        'busquedaHotel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1242, 466)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "busquedaHotel"
        Me.Text = "Resultados de la b�squeda"
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.cmbHotel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_distancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ugb_desplegar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugb_desplegar.ResumeLayout(False)
        Me.ugb_desplegar.PerformLayout()
        CType(Me.txtMax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ugb_buscarPor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugb_buscarPor.ResumeLayout(False)
        Me.ugb_buscarPor.PerformLayout()
        CType(Me.txt_noches, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_codProm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbx_1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents data_lbl_totalHoteles As System.Windows.Forms.Label
    Friend WithEvents lbl_totalHoteles As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents pbx_5 As System.Windows.Forms.PictureBox
    Friend WithEvents pbx_4 As System.Windows.Forms.PictureBox
    Friend WithEvents pbx_3 As System.Windows.Forms.PictureBox
    Friend WithEvents pbx_2 As System.Windows.Forms.PictureBox
    Friend WithEvents pbx_1 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_codProm As System.Windows.Forms.Label
    Friend WithEvents pbx_6 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_lugar As System.Windows.Forms.Label
    Friend WithEvents txt_codProm As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_noches As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_noches As System.Windows.Forms.Label
    Friend WithEvents lbl_llegada As System.Windows.Forms.Label
    Friend WithEvents lbl_salida As System.Windows.Forms.Label
    Friend WithEvents dtp_out As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_in As System.Windows.Forms.DateTimePicker
    Friend WithEvents ugb_buscarPor As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents rbt_aeropuerto As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_ciudad As System.Windows.Forms.RadioButton
    Friend WithEvents ugb_desplegar As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents rbt_hotelesDisponibles As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_todosHoteles As System.Windows.Forms.RadioButton
    Friend WithEvents btn_buscar As System.Windows.Forms.Button
    Friend WithEvents ComboSearch1 As callCenter.ComboSearch
    Friend WithEvents lbl_moneda As System.Windows.Forms.Label
    Friend WithEvents uce_moneda As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_distancia As System.Windows.Forms.Label
    Friend WithEvents uce_distancia As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_millas As System.Windows.Forms.Label
    Friend WithEvents Uc_habitaciones1 As callCenter.uc_habitaciones
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents btn_detener As System.Windows.Forms.Button
    Friend WithEvents cmbArea As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMax As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMin As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbt_tarifas As System.Windows.Forms.RadioButton
    Friend WithEvents lblConvenio As System.Windows.Forms.Label
    Friend WithEvents btnHotel As System.Windows.Forms.Button
    Friend WithEvents rbt_Hotel As System.Windows.Forms.RadioButton
    Friend WithEvents cmbHotel As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents btnPasiva As System.Windows.Forms.Button
    Friend WithEvents txt_Conv As System.Windows.Forms.TextBox
    Friend WithEvents txt_Rateplan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    'Friend WithEvents ComboSearch1 As callCenter.ComboSearch
End Class
