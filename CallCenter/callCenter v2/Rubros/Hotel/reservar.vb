Imports System.Data.SqlClient

Public Class reservar
    Public datosBusqueda_hotel As dataBusqueda_hotel_test
    Public tipoAccion As enumReservar_TipoAccion = enumReservar_TipoAccion.reservar
    Public cambio_tar As Boolean = False
    Public cambiaron As Boolean = False
    Public cargando As Boolean = False
    Public dsRules As DataSet
    Public Ok As Boolean = False
    Public GuarDepOriginal As String
    Dim StartPrint As Boolean = False
    Dim pasiva As Boolean

    Public Function cargar(ByVal ds As DataSet) As Boolean
        pasiva = datosBusqueda_hotel.OnRequest 'IIf(datosBusqueda_hotel.RateCode = "" And dataOperador.Hoteles.Count > 0, True, False)
        If pasiva Then btn_reglas.Visible = False

        cambiaron = False
        cargando = True
        CmbSearch1.crea_lista(Me, 654, 308)
        Call CargaMedios()
        Call CargaSegmentos()
        dsRules = ds
        dsRules = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)
        datosBusqueda_hotel.AllowBankDeposit = False
        datosBusqueda_hotel.AllowBankDepositCCT = False
        If Not IsNothing(dsRules.Tables("Property")) AndAlso dsRules.Tables("Property").Rows.Count > 0 Then
            datosBusqueda_hotel.AllowBankDeposit = IIf(dsRules.Tables("Property").Rows(0).Item("AllowBankDeposit") = "Y", True, False)
        End If
        If Not IsNothing(dsRules.Tables("Property")) AndAlso dsRules.Tables("Property").Rows.Count > 0 Then
            datosBusqueda_hotel.AllowBankDepositCCT = IIf(dsRules.Tables("Property").Rows(0).Item("AllowBankDepositCCT") = "Y", True, False)
        End If
        datosBusqueda_hotel.AllowSpecificPayment = False
        If Not IsNothing(dsRules.Tables("Property")) AndAlso dsRules.Tables("Property").Rows.Count > 0 Then
            If dsRules.Tables("Property").Columns.Contains("AllowSpecificPayment") Then
                datosBusqueda_hotel.AllowSpecificPayment = IIf(dsRules.Tables("Property").Rows(0).Item("AllowSpecificPayment") = "Y", True, False)
            End If

        End If
        'forzar para mision
        If dataOperador.idCorporativo = "1" Then
            datosBusqueda_hotel.AllowBankDepositCCT = True
            datosBusqueda_hotel.AllowBankDeposit = True
            datosBusqueda_hotel.AllowSpecificPayment = True
        End If
        GuarDepOriginal = ""
        'Me.cmbEmpresas.Enabled = True
        Me.CmbSearch1.Enabled = True
        Me.cmbMedios.Enabled = True
        Me.cmbSegmento.Enabled = True

        CapaPresentacion.Idiomas.cambiar_reservar(Me)
        If misForms.newCall Is Nothing OrElse misForms.newCall.datos_call Is Nothing OrElse misForms.newCall.UltraButton1.Enabled Then
            'btn_reservar.Enabled = False
        End If
        Dim res As Boolean
        If tipoAccion = enumReservar_TipoAccion.reservar Then
            res = CapaPresentacion.Hotels.HotelBook.inicializar_controles(Me)
        Else
            res = CapaPresentacion.Hotels.HotelBook.display_controles(Me)
        End If
        If tipoAccion = enumReservar_TipoAccion.modificar Then
            'Me.cmbEmpresas.Enabled = False
            Me.CmbSearch1.Enabled = True
            Me.cmbMedios.Enabled = False
            Me.cmbSegmento.Enabled = False
            Me.txtFrecuentCode.Enabled = False
            Me.btnAltaSocio.Visible = False
            Me.btnBuscarSocio.Visible = False
            Me.lbl_info_tarifa.Visible = True
            Me.btn_actualizar.Visible = True
            Me.lbl_info_personasExtra.Visible = True

            'Me.chkDeposito.Enabled = False
            'If Not IsNothing(dsRules.Tables("Error")) AndAlso dsRules.Tables("Error").Rows.Count > 0 AndAlso dsRules.Tables("Error").Rows(0).Item("ErrorCode") = 3 Then
            '    MsgBox(dsRules.Tables("Error").Rows(0).Item("Message"), MsgBoxStyle.Information)
            'Else
            GuarDepOriginal = datosBusqueda_hotel.GuarDep
            'End If

            'If datosBusqueda_hotel.DepositType <> enumDepositType.CreditCard And datosBusqueda_hotel.DepositType <> enumDepositType.Other Then
            '    MsgBox(CapaPresentacion.Idiomas.get_str("str_413_Modif"), MsgBoxStyle.Information)
            '    Return False
            'End If
        End If
        cargando = False
        Return res
    End Function

    Private Sub losRBTs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbt_tarjetaCredito.CheckedChanged, rbt_ninguno.CheckedChanged, rbt_depositoBancario.CheckedChanged, rbt_otra.CheckedChanged
        If rbt_tarjetaCredito.Checked Then
            GroupBox2.Visible = False
            If Not rbt_garantia.Checked And Not rbt_deposito.Checked Then rbt_garantia.Checked = True
        Else
            GroupBox2.Visible = False
        End If

        If rbt_otra.Checked Then
            txtOtra.Visible = True
        Else
            txtOtra.Visible = False
        End If

        If Me.rbt_depositoBancario.Checked Then
            If datosBusqueda_hotel.AllowBankDepositCCT Then
                chkDeposito.Enabled = True
                chkDeposito.Visible = True
            End If
            If datosBusqueda_hotel.AllowBankDeposit Then
                Me.pnlDeposito.Visible = True
            Else
                If chkDeposito.Visible Then
                    chkDeposito.Checked = True
                    chkDeposito.Enabled = False
                End If
            End If
        Else
            Me.pnlDeposito.Visible = False
            Me.chkDeposito.Visible = False
            Me.chkDeposito.Checked = False
        End If
        'TODO oculto permanente para esta version
        'Me.pnlDeposito.Visible = False
        If chkDeposito.Checked Then
            Me.WebBrowser1.Visible = False
            txtDeposito.Visible = True
            Me.txtInfoDeposito.Visible = True
        Else
            Me.WebBrowser1.Visible = True
            txtDeposito.Visible = False
            Me.txtInfoDeposito.Visible = False
        End If
    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_out.ValueChanged, dtp_in.ValueChanged
        Try
            herramientas.fechas_inteligentes(dtp_in, dtp_out, sender, 1, Nothing)
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0027", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    '---------

    Private Sub btn_reservar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_reservar.Click
        If Timer1.Enabled Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_468"))
            Exit Sub
        End If
        If Me.rbt_otra.Checked Then
            If Me.txtOtra.Text.Trim = "" Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_417_OtherReq"))
                Exit Sub
            End If
        End If
        If Me.rbt_depositoBancario.Checked Then
            If Me.dtp_DepFecha.Value.ToString("yyyyMMdd") > datosBusqueda_hotel.llegada.ToString("yyyyMMdd") Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_418_LimitDateInv"))
                Exit Sub
            End If
        End If
        If Me.chkDeposito.Checked Then
            If Me.txtDeposito.Text.Trim = "" Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_428_amountinforeq"))
                Exit Sub
            End If
            If CDec(Me.txtDeposito.Text) < 0 Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_428_amountinforeq"))
                Exit Sub
            End If
            If Me.txtInfoDeposito.Text = "" Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_432_reqdep"))
                Exit Sub
            End If
        End If

        Dim res As callCenter.CallOption.CallResult = CapaLogicaNegocios.Calls.SelectOption()

        If res <> CallOption.CallResult.Cancel Then
            If tipoAccion = enumReservar_TipoAccion.reservar OrElse tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado Then
                CapaLogicaNegocios.Hotels.HotelBook.make_reservar(Me)
            Else
                CapaPresentacion.Hotels.HotelBook.refresh_datosBusqueda(Me)
                Dim frm As New datosCliente
                frm.ModificandoReservaHotel = True
                misForms.form1.datoscliente = New callCenter.dataBusqueda_paquete()
                If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    ' Close()
                Else
                    If CapaLogicaNegocios.Hotels.HotelBook.make_modify(Me) Then
                        misForms.form1.datoscliente = Nothing
                        Ok = True
                        MsgBox(CapaPresentacion.Idiomas.get_str("str_421_Modification"), MsgBoxStyle.Information)
                        Close()

                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        If Me.tipoAccion = enumReservar_TipoAccion.modificar Then
            misForms.newCall.datos_call.reservacion_paquete.cli_nombre = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_apellido = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_direccion = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_ciudad = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_estado = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_cp = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_pais = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_AreaCityCode = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneNumber = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_PhoneUseType = "R"

            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_AreaCityCode = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cli_telefono_adicional_PhoneNumber = String.Empty

            misForms.newCall.datos_call.reservacion_paquete.cli_email = String.Empty

            misForms.newCall.datos_call.reservacion_paquete.cc_tipo = Nothing
            misForms.newCall.datos_call.reservacion_paquete.cc_nombre_ = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cc_apellido = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cc_numero = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cc_numSeguridad = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.cc_fechaExpira = String.Empty
            misForms.newCall.datos_call.reservacion_paquete.codigo_referencia = String.Empty
        End If
        Close()
    End Sub

    Private Sub btn_cambiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cambiar.Click
        If datosBusqueda_hotel.OnRequest Then
            Dim frm As New tarifas_personalizadas
            frm.datosBusqueda_hotel = New dataBusqueda_hotel_test
            frm.datosBusqueda_hotel.PropertyNumber = datosBusqueda_hotel.PropertyNumber
            frm.datosBusqueda_hotel.PropertyName = datosBusqueda_hotel.PropertyName
            frm.datosBusqueda_hotel.habitaciones_ = datosBusqueda_hotel.habitaciones_
            frm.datosBusqueda_hotel.llegada = Me.dtp_in.Value
            frm.datosBusqueda_hotel.salida = Me.dtp_out.Value
            frm.datosBusqueda_hotel.OnRequest = True
            frm.ShowDialog()
            If frm.accept Then
                'datosBusqueda_hotel = frm.datosBusqueda_hotel
                datosBusqueda_hotel.habitaciones_ = frm.datosBusqueda_hotel.habitaciones_
                datosBusqueda_hotel.RateCode = frm.datosBusqueda_hotel.RateCode
                datosBusqueda_hotel.ServiceProvider = frm.datosBusqueda_hotel.ServiceProvider
                datosBusqueda_hotel.GuarDep = frm.datosBusqueda_hotel.GuarDep
                datosBusqueda_hotel.noches = frm.datosBusqueda_hotel.noches
                datosBusqueda_hotel.TotalAvgRate = frm.datosBusqueda_hotel.TotalAvgRate
                datosBusqueda_hotel.AvgRate = frm.datosBusqueda_hotel.AvgRate
                datosBusqueda_hotel.AltCurrency = frm.datosBusqueda_hotel.AltCurrency
                datosBusqueda_hotel.monedaBusqueda = frm.datosBusqueda_hotel.monedaBusqueda
                datosBusqueda_hotel.AltTotalStay = frm.datosBusqueda_hotel.AltTotalStay
                datosBusqueda_hotel.AltTax = frm.datosBusqueda_hotel.AltTax
                datosBusqueda_hotel.Tax = frm.datosBusqueda_hotel.Tax
                datosBusqueda_hotel.nombreHabitacion = frm.datosBusqueda_hotel.nombreHabitacion
                datosBusqueda_hotel.PlanName = frm.datosBusqueda_hotel.PlanName
                datosBusqueda_hotel.PlanDescription = frm.datosBusqueda_hotel.PlanDescription
                datosBusqueda_hotel.Segmento = frm.datosBusqueda_hotel.Segmento
                datosBusqueda_hotel.PlusTax = frm.datosBusqueda_hotel.PlusTax
                cargar(Nothing)
            End If
        Else
            Dim frm As New tarifas()
            frm.individual = True
            frm.datosBusqueda_hotel = datosBusqueda_hotel.Clone
            frm.datosBusqueda_hotel.llegada = dtp_in.Value
            frm.datosBusqueda_hotel.salida = dtp_out.Value
            frm.datosBusqueda_hotel.codProm = data_lbl_codProm.Text
            frm.datosBusqueda_hotel.Convenio = lblConvenioData.Text
            frm.datosBusqueda_hotel.habitaciones_ = Uc_habitaciones1.Habitaciones.copy 'si es nesesario

            'parche 
            'cuando se consulta la lista de tarifas solo aparece una
            'este parce es para que salga la lista te tarifas completa para el ratecode
            frm.datosBusqueda_hotel.RateCode = ""

            frm.cargar()
            frm.ShowDialog()

            If cambio_tar Then
                cambio_tar = False
                dsRules = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)
                datosBusqueda_hotel.AllowBankDeposit = False
                If Not IsNothing(dsRules.Tables("Property")) AndAlso dsRules.Tables("Property").Rows.Count > 0 Then
                    datosBusqueda_hotel.AllowBankDeposit = IIf(dsRules.Tables("Property").Rows(0).Item("AllowBankDepositCCT") = "Y", True, False)
                End If
                datosBusqueda_hotel.AllowSpecificPayment = False
                If Not IsNothing(dsRules.Tables("Property")) AndAlso dsRules.Tables("Property").Rows.Count > 0 Then
                    datosBusqueda_hotel.AllowSpecificPayment = IIf(dsRules.Tables("Property").Rows(0).Item("AllowSpecificPayment") = "Y", True, False)
                End If
                Uc_habitaciones1.Habitaciones = datosBusqueda_hotel.habitaciones_.copy ' si es nesesario por que la propiedad que regresa con copy solo es para Uc_habitaciones1
                CapaPresentacion.Hotels.HotelBook.fija_labelsTarifa(Me, datosBusqueda_hotel.AltTotalStay, datosBusqueda_hotel.AltTotalStay, datosBusqueda_hotel.AltCurrency, datosBusqueda_hotel.AltTax, datosBusqueda_hotel.PlusTax, datosBusqueda_hotel.AltTotalExtras)
                txt_codigoTarifa.Text = datosBusqueda_hotel.RateCode
                'si se esta modificano confirmada no se cambia la forma de pago
                If tipoAccion <> enumReservar_TipoAccion.modificar Then
                    rbt_ninguno.Checked = False
                    rbt_ninguno.Enabled = False
                    rbt_deposito.Checked = False
                    rbt_deposito.Enabled = False

                    Select Case datosBusqueda_hotel.GuarDep
                        Case "G"
                            rbt_garantia.Checked = True
                            rbt_tarjetaCredito.Checked = True
                        Case "D"
                            rbt_deposito.Checked = True
                            rbt_tarjetaCredito.Checked = False

                        Case "N"
                            rbt_ninguno.Checked = True
                            rbt_ninguno.Enabled = True
                            rbt_garantia.Checked = False
                            rbt_deposito.Checked = False
                        Case Else
                            rbt_ninguno.Checked = True
                            rbt_otra.Checked = False
                            rbt_garantia.Checked = False
                            rbt_deposito.Checked = False
                    End Select
                End If


                CapaPresentacion.Hotels.HotelBook.SetRules(Me)
            End If

        End If
    End Sub

    Private Sub btn_reglas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_reglas.Click
        Dim frm As New reglas
        frm.datosBusqueda_hotel = datosBusqueda_hotel.Clone
        frm.cargar(dsRules)
        frm.ShowDialog()
    End Sub

    Private Sub reservar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        misForms.reservar = Me
        btn_actualizar.ForeColor = Color.Black

        If tipoAccion <> enumReservar_TipoAccion.reservar AndAlso datosBusqueda_hotel.Status = "4" AndAlso (datosBusqueda_hotel.isOnlinePayment.ToLower = "true" Or datosBusqueda_hotel.isPayPalPayment.ToLower = "true" Or datosBusqueda_hotel.isPayUPayment.ToLower = "true") Then
            btnConfirm.Text = CapaPresentacion.Idiomas.get_str("str_538")
            btnConfirm.Visible = True
            btnConfirm.Location = New Point(140, 434)
        End If
        Timer1.Stop()
        'dtp_DepFecha.Value = Now
        'dtp_DepHora.Value = Now
        Dim fecha_menor As DateTime = DateTime.Now
        dtp_in.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_out.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day).AddDays(1)



        AddHandler CmbSearch1.llena, AddressOf CargaEmpresasSegmento

        If dataOperador.MostrarCatalogos Then
            pnlCatalogos.Visible = True
            If tipoAccion = enumReservar_TipoAccion.reservar Then
                cmbSegmento.Visible = False
                lblSegmento.Visible = False
            End If
            ' txt_comentario.Height = 39
        Else
            pnlCatalogos.Visible = False
        End If
        If dataOperador.idCorporativo = "1" Then
            Me.btnAltaSocio.Visible = True
            Me.btnBuscarSocio.Visible = True
        Else
            Me.btnAltaSocio.Visible = False
            Me.btnBuscarSocio.Visible = False
        End If
        Dim txt_hab As Infragistics.Win.UltraWinEditors.UltraTextEditor = Uc_habitaciones1.prop_txt_habitaciones
        Dim txt_adl As Infragistics.Win.UltraWinEditors.UltraTextEditor = Uc_habitaciones1.prop_txt_adultos
        Dim txt_nin As Infragistics.Win.UltraWinEditors.UltraTextEditor = Uc_habitaciones1.prop_txt_ninos
        AddHandler txt_hab.ValueChanged, AddressOf controls_ValueChanged
        AddHandler txt_adl.ValueChanged, AddressOf controls_ValueChanged
        AddHandler txt_nin.ValueChanged, AddressOf controls_ValueChanged
        'AddHandler txt_hab.Leave, AddressOf controls_Leave
        'AddHandler txt_adl.Leave, AddressOf controls_Leave
        'AddHandler txt_nin.Leave, AddressOf controls_Leave
    End Sub

    Private Sub reservar_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        misForms.reservar = Nothing
    End Sub

    Private Sub reservar_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        txt_peticion.Focus()
        txt_peticion.SelectionLength = 0
    End Sub

    Private Sub dtp_in_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CapaLogicaNegocios.Hotels.HotelBook.cambio_valor(Me)
    End Sub

    Private Sub controls_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_out.ValueChanged, dtp_in.ValueChanged
        cambiaron = True
        activaboton()
    End Sub

    Private Sub Uc_habitaciones1_BotonEspecificarClicked() Handles Uc_habitaciones1.BotonEspecificarClicked
        cambiaron = True
        activaboton()
    End Sub

    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        If (dtp_out.Value - dtp_in.Value).TotalDays > 90 Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_315_err99noches"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If
        CapaLogicaNegocios.Hotels.HotelBook.consulta(Me)
        Timer1.Stop()
        btn_actualizar.ForeColor = Color.Black
        btn_actualizar.Text = btn_actualizar.Text.Replace("<", "").Replace(">", "")

    End Sub

    Private Sub activaboton()
        If CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.modificar Then
            Me.Timer1.Start()
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        btn_actualizar.Text = btn_actualizar.Text.Replace("<", "").Replace(">", "")
        If btn_actualizar.ForeColor = Color.Blue Then
            btn_actualizar.ForeColor = Color.Black
        Else
            btn_actualizar.ForeColor = Color.Blue
            btn_actualizar.Text = ">>" & btn_actualizar.Text & "<<"
        End If

    End Sub

    Private Sub chkDeposito_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeposito.CheckedChanged
        If chkDeposito.Checked Then
            Me.WebBrowser1.Visible = False
            Me.txtDeposito.Visible = True
            Me.txtInfoDeposito.Visible = True
            pnlDeposito.Visible = False
        Else
            Me.WebBrowser1.Visible = True
            Me.txtDeposito.Text = ""
            Me.txtInfoDeposito.Text = ""
            Me.txtDeposito.Visible = False
            Me.txtInfoDeposito.Visible = False

            pnlDeposito.Visible = True
        End If
    End Sub

    Private Sub txtDeposito_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDeposito.Leave
        If IsNumeric(txtDeposito.Text) Then
            txtDeposito.Text = FormatCurrency(txtDeposito.Text, 2).Replace("$", "").Replace(",", "")
        Else
            MsgBox("invalido")
            txtDeposito.Text = ""
        End If
    End Sub

    Private Sub CargaMedios()
        cmbMedios.Items.Clear()
        cmbMedios.DisplayMember = "Name"
        cmbMedios.ValueMember = "_Value"
        cmbMedios.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_0108_ninguno"), ""))
        For Each dr As DataRow In herramientas.get_Medios(dataOperador.idCorporativo).Tables(0).Rows
            cmbMedios.Items.Add(New ListData(dr("Nombre"), dr("id_Medio")))
        Next
        cmbMedios.SelectedIndex = 0
    End Sub

    'Private Sub CargaEmpresas(ByVal idMedio As String)
    '    cmbEmpresas.Items.Clear()
    '    cmbEmpresas.DisplayMember = "Name"
    '    cmbEmpresas.ValueMember = "_Value"
    '    cmbEmpresas.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), ""))
    '    If idMedio <> "" Then
    '        For Each dr As DataRow In herramientas.get_Empresas(idMedio).Tables(0).Rows
    '            cmbEmpresas.Items.Add(New ListData(dr("Nombre"), dr("id_Empresa")))
    '        Next
    '    End If
    '    cmbEmpresas.SelectedIndex = 0
    'End Sub

    Private Sub CargaEmpresasSegmento()
        Dim id_Segmento As String = ""
        If Not cmbSegmento.SelectedItem Is Nothing Then
            id_Segmento = cmbSegmento.SelectedItem._value
        End If

        'cmbEmpresas.Items.Clear()
        'cmbEmpresas.DisplayMember = "Name"
        'cmbEmpresas.ValueMember = "_Value"
        'cmbEmpresas.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), ""))
        If IsNothing(CmbSearch1.ListBox1) Then Exit Sub

        CmbSearch1.ListBox1.Items.Clear()
        CmbSearch1.lista.Clear()
        CmbSearch1.ListBox1.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), ""))
        CmbSearch1.lista.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), ""))
        'If id_Segmento <> "" Then
        Dim ds As DataSet = herramientas.get_Empresas_filtro(CmbSearch1.TextBox1.Text) 'herramientas.get_Empresas_Segmento(id_Segmento, CmbSearch1.TextBox1.Text)
        If ds.Tables(0).Rows.Count = 1 Then
            CmbSearch1.ListBox1.Items.Clear()
            CmbSearch1.lista.Clear()
        End If

        For Each dr As DataRow In ds.Tables(0).Rows
            'cmbEmpresas.Items.Add(New ListData(dr("Nombre"), dr("id_Empresa")))
            CmbSearch1.ListBox1.Items.Add(New ListData(dr("Codigo").ToString & " - " & dr("Nombre").ToString, dr("id_Empresa").ToString))
            CmbSearch1.lista.Add(New ListData(dr("Nombre").ToString, dr("id_Empresa").ToString))
        Next
        'End If
        'cmbEmpresas.SelectedIndex = 0
        If CmbSearch1.ListBox1.Items.Count > 0 Then CmbSearch1.ListBox1.SelectedIndex = 0



    End Sub

    Private Sub CargaSegmentos()
        cmbSegmento.Items.Clear()
        cmbSegmento.DisplayMember = "Name"
        cmbSegmento.ValueMember = "_Value"
        For Each dr As DataRow In herramientas.get_Segmentos(dataOperador.idCorporativo).Tables(0).Rows
            cmbSegmento.Items.Add(New ListData(dr("Nombre"), dr("id_Segmento")))
        Next
        If cmbSegmento.Items.Count = 0 Then cmbSegmento.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_0108_ninguno"), ""))
        cmbSegmento.SelectedIndex = 0
    End Sub

    Private Sub cmbMedios_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMedios.SelectedValueChanged
        ' If Not cmbMedios.SelectedItem Is Nothing Then Call CargaEmpresas(cmbMedios.SelectedItem._Value)
    End Sub


    Private Sub cmbSegmento_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSegmento.SelectedValueChanged
        If Not cmbSegmento.SelectedItem Is Nothing Then Call CargaEmpresasSegmento()
        'CmbSearch1.TextBox1.Text = ""
        'CmbSearch1.ListBox1.SelectedIndex = -1

    End Sub

    Private Sub btnAltaSocio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaSocio.Click
        Dim frm As New AltaSociosRewards
        frm.ShowDialog()
        If frm.UltimoCreado <> "" Then
            Me.txtFrecuentCode.Text = frm.UltimoCreado
            Me.lblFrec_Code.Text = frm.UltimoCreado
            Me.lblFrec_Name.Text = frm.UltimoCreadoNombre
            Me.pnlFrecuentCode.Visible = True
        End If

    End Sub

    Private Sub btnBuscarSocio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarSocio.Click
        Dim frm As New BusquedaSocioRewards 'BusquedaSocio
        frm.ShowDialog()
        If frm.SocioSeleccionado <> "" Then
            Me.txtFrecuentCode.Text = frm.SocioSeleccionado
            Me.lblFrec_Code.Text = frm.SocioSeleccionado
            Me.lblFrec_Name.Text = frm.SocioSeleccionadoNombre
            Me.pnlFrecuentCode.Visible = True
        End If
    End Sub

    Private Sub btnFrecCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFrecCerrar.Click
        Me.pnlFrecuentCode.Visible = False
        Me.lblFrec_Code.Text = ""
        Me.lblFrec_Name.Text = ""
        Me.txtFrecuentCode.Text = ""
    End Sub

    Private Sub btn_imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_imprimir.Click

        'WebBrowser2.Document.Title = ""

        WebBrowser2.ShowPrintPreviewDialog()

        'WebBrowser1.ShowPrintDialog()
        'WebBrowser1.Print()
    End Sub

    Private Function agrega_hotel() As String

        Dim _Empresa, _Medio, _Segmento, _MedioPromocion As String
        _Empresa = ""
        _Medio = ""
        _Segmento = ""
        _MedioPromocion = ""

        Dim estado As String
        estado = ""

        Dim str_total As String = lbl_info_total.Text
        Dim fecha_res As String = Now.ToString("yyyy/MM/dd")
        Dim fecha_in_ As String = data_lbl_in.Text
        Dim fecha_out As String = data_lbl_out.Text
        Dim moneda As String = datosBusqueda_hotel.monedaBusqueda

        Dim FormaPago As String
        FormaPago = ""

        Dim datos As String = ""
        datos += "<table width='600px'>"
        datos += "    <tr>"
        datos += "        <td><h1>" & CapaPresentacion.Idiomas.get_str("str_444_quote") & "</h1></td>"
        datos += "    </tr>"
        datos += "    <tr>"
        datos += "        <td style='width:100px;'><img src='https://crs.univisit.com/Images/global/ImagesSystem/Logos/LogoCompany_" + dsRules.Tables("Property").Rows(0)("CompanyId").ToString() + "' /></td>"
        datos += "    </tr>"
        datos += "    <tr>"
        datos += "        <td ><br /><b>" + CapaPresentacion.Idiomas.get_str("str_445") + ":" & dataOperador.user & "</b></td>"
        datos += "    </tr>"
        datos += "    <tr>"
        datos += "        <td><b>" + data_lbl_hotel.Text + "</b></td>"
        datos += "    </tr>"
        datos += "    <tr>"
        ' datos += "        <td>" + dsRules.Tables("Property").Rows(0).Item("HotelName") + "</td>"
        datos += "        <td>" + data_lbl_hotel.Text + "</td>"
        datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_336_arrDate") + ": " + fecha_in_ + "</td>"
        datos += "    </tr>"
        datos += "    <tr>"
        If pasiva Then
            datos += "        <td>" + datosBusqueda_hotel.CityName + ", " + datosBusqueda_hotel.CountryCode + "</td>"
        Else
            datos += "        <td>" + dsRules.Tables("Property").Rows(0).Item("City") + ", " + dsRules.Tables("Property").Rows(0).Item("Country") + "</td>"
        End If

        datos += "        <td style='text-align: right'>" + CapaPresentacion.Idiomas.get_str("str_337_depDate") + ": " + fecha_out + "</td>"
        datos += "    </tr>"
        datos += "</table>"
        datos += ""
        datos += "<br />"
        datos += ""
        datos += "<table width='600px'>"
        Dim cont As Integer = 1
        'For Each r As DataRow In dsRules.Tables("RTRoom").Rows
        '    Dim str_tar_dias As String = ""
        '    Dim tar, ini, fin As String
        '    tar = ""
        '    ini = ""
        '    fin = ""


        '    datos += "    <tr>"
        '    datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + "</td>"
        '    datos += "        <td></td>"
        '    datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_341_tarNoche") + "</td>"
        '    datos += "    </tr>"
        '    datos += "    <tr>"
        '    datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_340_resBajo") + "</td>"
        '    datos += "        <td>" + r.Item("Adults") + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + r.Item("Children") + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + ", " + dsRules.Tables("Room").Rows(0).Item("NameRoom") + ", " + dsRules.Tables("Room").Rows(0).Item("RatePlanName") + "</td>"
        '    str_tar_dias = lbl_info_tarifa.Text
        '    datos += "        <td>" + str_tar_dias + "</td>"
        '    datos += "    </tr>"
        'Next
        'For Each hab As uc_habitaciones.T_Habitacion In datosBusqueda_hotel.habitaciones_.ListaHabitaciones
        '    Dim str_tar_dias As String = ""
        '    Dim tar, ini, fin As String
        '    tar = ""
        '    ini = ""
        '    fin = ""
        '    datos += "    <tr>"
        '    datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + "</td>"
        '    datos += "        <td></td>"
        '    datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_341_tarNoche") + "</td>"
        '    datos += "    </tr>"
        '    datos += "    <tr>"
        '    datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_340_resBajo") + "</td>"
        '    datos += "        <td>" + hab.NumAdultos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + hab.NumNinos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + ", " + dsRules.Tables("Room").Rows(0).Item("NameRoom") + ", " + dsRules.Tables("Room").Rows(0).Item("RatePlanName") + "</td>"
        '    str_tar_dias = lbl_info_tarifa.Text
        '    datos += "        <td>" + str_tar_dias + "</td>"
        '    datos += "    </tr>"
        '    cont = cont + 1
        'Next
        If pasiva Then
            For Each hab As uc_habitaciones.T_Habitacion In datosBusqueda_hotel.habitaciones_.ListaHabitaciones
                datos += "    <tr>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_318_habitacion") + " " + CStr(cont) + "</td>"
                datos += "        <td>" + hab.NumAdultos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + hab.NumNinos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + ", " + datosBusqueda_hotel.nombreHabitacion + "</td>"
                datos += "        <td>" + CapaPresentacion.Idiomas.get_str("str_341_tarNoche") + " " + FormatCurrency(datosBusqueda_hotel.AvgRate, 2) + "</td>"
                datos += "    </tr>"
                cont = cont + 1
            Next
        Else
            datos &= DesgloseHabitaciones()
        End If


        datos += "</table>"
        datos += ""
        datos += "<br />"
        datos += ""
        datos += "<table width='600px'>"
        datos += "    <tr>"
        datos += "        <td style='text-align: right'>" + Me.lbl_info_subtotal.Text + "</td>"
        datos += "    </tr>"
        datos += "    <tr>"
        datos += "        <td style='text-align: right'>" + Me.lbl_info_impuestos.Text + "</td>"
        datos += "    </tr>"
        datos += "    <tr>"
        datos += "        <td style='text-align: right'>" + lbl_info_total.Text + "</td>"
        datos += "    </tr>"
        datos += "</table>"
        datos += ""
        datos += "<br />"
        datos += ""
        datos += "<table width='600px'>"
        datos += "    <tr>"
        datos += "        <td>"
        If pasiva Then

        Else
            If dsRules.Tables("Room").Rows(0).Item("IsUvNetRate") Then
                datos += "              <p>" + CapaPresentacion.Idiomas.get_str("str_442_netrates") + "</p>"
            Else
                datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_0195_cancelPol") + "</b></p>"
                datos += "              <p>" + dsRules.Tables("Property").Rows(0).Item("CancelationPoliticies") + "</p>"
            End If
            datos += "              <p><b>" + CapaPresentacion.Idiomas.get_str("str_0143_descripcionTar") + "</b></p>"
            datos += "              <p>" + dsRules.Tables("Room").Rows(0).Item("RatePlanName") + "</p>"
            datos += "              <p>" + dsRules.Tables("Room").Rows(0).Item("RatePlanDescription") + "</p>"
        End If
        datos += "        </td>"
        datos += "    </tr>"
        datos += "</table>"

        Return datos
    End Function

    Private Function DesgloseHabitaciones() As String
        Dim tarifaTm As Double
        Dim tarifa As Double = Double.MinValue
        Dim tmptarifa As Double
        Dim tmptarifaNinios As Double = 0
        Dim idx As Integer
        Dim tmpdate, tmpAntDate As Date
        Dim subtotal As Double
        'Dim tbl As HtmlTable
        Dim drrates() As DataRow 'Hotel_HOR_RS.RateRow
        Dim drPrices() As DataRow 'Hotel_HOR_RS.PriceRow
        Dim arrRates As New ArrayList
        Dim tmptar As Double
        Dim dia As Integer
        Dim txttarifa As String
        Dim deposito As Double

        Dim cad As String = ""
        For Each drRoom As DataRow In dsRules.Tables("RTRoom").Rows
            Dim hab As uc_habitaciones.T_Habitacion = datosBusqueda_hotel.habitaciones_.ListaHabitaciones(idx)
            Dim _d As String = " " & hab.NumAdultos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0011_adultos") + ", " + hab.NumNinos.ToString + " " + CapaPresentacion.Idiomas.get_str("str_0012_ninosHabit") + ", " + dsRules.Tables("Room").Rows(0).Item("NameRoom") + ", " + dsRules.Tables("Room").Rows(0).Item("RatePlanName")
            arrRates.Add(CapaPresentacion.Idiomas.get_str("HOTEL000011") & " #" & (idx + 1).ToString & _d & "||" & "RatesBold")
            subtotal = 0
            tarifa = Double.MinValue
            dia = 0
            For Each dr As DataRow In dsRules.Tables("Rate").Rows
                dia += 1
                drPrices = dsRules.Tables("Price").Select("SeassonID=" & dr("SeassonId") & " and adults=" & drRoom("Adults") & _
                 " and children = " & drRoom("Children") & " and teens=" & drRoom("Teens"))

                If Not dr.IsNull("Exc") AndAlso dr("Exc").ToString.ToUpper = "Y" Then
                    tmptarifa = CDbl(drPrices(0).Item("AltRateAdultExc"))
                    tmptarifaNinios = CDbl(drPrices(0).Item("AltRateChildExc"))
                Else
                    tmptarifa = CDbl(drPrices(0).Item("AltRateAdult"))
                    tmptarifaNinios = CDbl(drPrices(0).Item("AltRateChild"))
                End If

                If dsRules.Tables("Price").Columns.Contains("DescPromotion") Then
                    Dim porcentaje As Double
                    If drPrices(0).Item("DescPromotion").ToString() = "" Then
                        porcentaje = 0
                    Else
                        porcentaje = drPrices(0).Item("DescPromotion") / 100
                    End If
                    tmptarifa = tmptarifa - (porcentaje * tmptarifa)
                    'End If
                End If

                If drRoom("ExtraAdults") > 0 Then
                    tmptarifa += CDbl(drPrices(0).Item("AltExtraRateAdult")) * CDbl(drRoom("ExtraAdults"))
                End If

                If drRoom("ExtraChildren") > 0 Then
                    tmptarifaNinios += CDbl(drPrices(0).Item("AltExtraRateChild")) * (drRoom("ExtraChildren"))
                End If
                If Not drRoom.IsNull("ExtraTeens") AndAlso drRoom("ExtraTeens") > 0 Then
                    tmptarifaNinios += CDbl(drPrices(0).Item("AltExtraRateTeen")) * (drRoom("ExtraTeens"))
                End If


                Dim DescChildren As Boolean = False
                If Not drPrices(0).IsNull("DescChildrenWhenApply") Then
                    DescChildren = drPrices(0).Item("DescChildrenWhenApply")
                End If

                tmptar = tmptarifa + tmptarifaNinios
                Dim DescPromotion As Double
                DescPromotion = dsRules.Tables("Room").Rows(0).Item("DescPromotion")
                'If Not drPrices(0).IsNull("DescPromotion") Then
                '    If drPrices(0).Item("DescPromotion") > 0 Then
                '        DescPromotion = drPrices(0).Item("DescPromotion")
                '    End If
                'End If


                If CInt(dsRules.Tables("Room").Rows(0).Item("DaysFree")) > 0 AndAlso dia Mod dsRules.Tables("Room").Rows(0).Item("DaysFree") = 0 Then
                    tmptarifa = 0
                ElseIf DescPromotion > 0 Then

                    If DescChildren Then
                        tmptarifa += tmptarifaNinios
                        tmptarifa -= (tmptarifa) * DescPromotion / 100
                    Else
                        'tmptarifa -= (tmptarifa * DescPromotion / 100)
                        tmptarifa += tmptarifaNinios
                    End If
                Else
                    tmptarifa += tmptarifaNinios
                End If
                txttarifa = ""
                If tarifa <> tmptarifa Then
                    If tarifa <> Double.MinValue Then
                        txttarifa = tarifa.ToString("#,##0.00")
                        If tarifa = 0 Then
                            txttarifa = CapaPresentacion.Idiomas.get_str("HOTEL000289")
                        ElseIf tarifaTm <> tarifa Then
                            txttarifa = "<span style='text-decoration:line-through'>" & tarifaTm.ToString("#,##0.00") & "</span>&nbsp;" & tarifa.ToString("#,##0.00")
                        End If


                        ' arrRates.Add(tmpAntDate.ToString("dd/MM") & "-" & tmpdate.ToString("dd/MM") & "|" & tarifa.ToString("#,##0.00") & " " & HotelLanguage.GetString( "000232") & "|" & "txtDataBold")
                        If CapaPresentacion.Idiomas.IsCulturaEspanol Then
                            If tmpAntDate <> tmpdate Then
                                arrRates.Add(tmpAntDate.ToString("MM/dd") & "-" & tmpdate.ToString("MM/dd") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                            Else
                                arrRates.Add(tmpAntDate.ToString("MM/dd") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                            End If
                        Else
                            If tmpAntDate <> tmpdate Then
                                arrRates.Add(tmpAntDate.ToString("dd/MM") & "-" & tmpdate.ToString("dd/MM") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                            Else
                                arrRates.Add(tmpAntDate.ToString("dd/MM") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                            End If

                        End If
                    End If
                    tmpAntDate = dr("Date")
                    tarifa = tmptarifa
                    tarifaTm = tmptar
                End If
                tmpdate = CDate(dr("Date"))
                subtotal += tmptarifa

            Next

            txttarifa = tarifa.ToString("#,##0.00")
            If tarifa = 0 Then
                txttarifa = CapaPresentacion.Idiomas.get_str("HOTEL000289")
            ElseIf tarifaTm <> tarifa Then
                txttarifa = "<span style='text-decoration:line-through'>" & tarifaTm.ToString("#,##0.00") & "</span>&nbsp;" & tarifa.ToString("#,##0.00")
            End If


            If CapaPresentacion.Idiomas.IsCulturaEspanol Then
                If tmpAntDate <> tmpdate Then
                    arrRates.Add(tmpAntDate.ToString("MM/dd") & "-" & tmpdate.ToString("MM/dd") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                Else
                    arrRates.Add(tmpAntDate.ToString("MM/dd") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                End If
            Else
                If tmpAntDate <> tmpdate Then
                    arrRates.Add(tmpAntDate.ToString("dd/MM") & "-" & tmpdate.ToString("dd/MM") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                Else
                    arrRates.Add(tmpAntDate.ToString("dd/MM") & "|" & Me.getstrFree(txttarifa) & "|" & "txtDataBold")
                End If

            End If
            'arrRates.Add("HOTEL000071" & "|" & subtotal.ToString("#,##0.00") & "|txtDataBold")
            idx += 1
        Next
        For x As Integer = 0 To arrRates.Count - 1
            cad += "    <tr>"
            cad += "        <td colspan='2'>" + arrRates(x).split("|")(0) + "</td>"
            cad += "        <td>" + arrRates(x).split("|")(1) + "</td>"
            cad += "    </tr>"

        Next
        Return cad
    End Function
    Private Function getstrFree(ByVal txt As String) As String
        If txt = CapaPresentacion.Idiomas.get_str("HOTEL000289") Then
            Return txt
        End If
        Return txt & " " & CapaPresentacion.Idiomas.get_str("HOTEL000232")
    End Function
    Private Sub btnCotizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCotizar.Click
        If btnCotizar.Tag = "" Then
            btnCotizar.Tag = "1"
            Dim cad As String
            cad = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml' ><head><title>Untitled Page</title></head><body style='font-family:tahoma; font-size: 8pt'>"
            cad &= agrega_hotel()
            cad &= "</body></html>"
            WebBrowser2.DocumentText = cad
            WebBrowser2.Visible = True
            btnCotizar.Text = "Ocultar Cotizacion"
            btn_imprimir.Visible = True
            btn_reservar.Visible = False
            btn_correo.Visible = True
            gbCorreo.Visible = True
            If Not datosBusqueda_hotel.confirmada Then
                btnConfirm.Visible = False
            End If
        Else
            WebBrowser2.DocumentText = ""
            WebBrowser2.Visible = False
            btnCotizar.Tag = ""
            btnCotizar.Text = "Mostrar Cotizacion"
            btn_imprimir.Visible = False
            btn_reservar.Visible = True
            btn_correo.Visible = False
            gbCorreo.Visible = False
            If Not datosBusqueda_hotel.confirmada Then
                If tipoAccion <> enumReservar_TipoAccion.reservar AndAlso datosBusqueda_hotel.Status = "4" AndAlso (datosBusqueda_hotel.isOnlinePayment.ToLower = "true" Or datosBusqueda_hotel.isPayPalPayment.ToLower = "true" Or datosBusqueda_hotel.isPayUPayment.ToLower = "true") Then
                    btnConfirm.Visible = True
                    btnConfirm.Location = New Point(140, 434)
                End If
            End If
        End If
    End Sub

    Private Sub btn_correo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_correo.Click
        Me.Cursor = Cursors.WaitCursor
        If Not System.Text.RegularExpressions.Regex.IsMatch(txtCorreo.Text, "^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_447"))
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        Dim mailMessage As System.Web.Mail.MailMessage
        Try
            mailMessage = New System.Web.Mail.MailMessage
            mailMessage.From = """Callcenter"" <" & dataOperador.user & ">"
            mailMessage.To = txtCorreo.Text
            mailMessage.Cc = dataOperador.user
            mailMessage.Bcc = "jara@oz.com.mx"
            mailMessage.Subject = CapaPresentacion.Idiomas.get_str("str_444_quote")
            mailMessage.BodyFormat = Web.Mail.MailFormat.Html
            mailMessage.Body = WebBrowser2.DocumentText

            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)
            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "smtpuser")
            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "smtpauthed")
            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 587)
            System.Web.Mail.SmtpMail.SmtpServer = "mail.univisit.com"
            System.Web.Mail.SmtpMail.Send(mailMessage)
        Catch ex As Exception
            MsgBox(ex.Message)
            Me.Cursor = Cursors.Default
            Exit Sub
        End Try
        MsgBox("Correo enviado")
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub CmbSearch1_ValueChanged() Handles CmbSearch1.ValueChanged
        If CmbSearch1.selected_valor <> "" Then
            Dim idSeg As String = herramientas.get_idSegmentoEmpresa(CmbSearch1.selected_valor)
            If idSeg <> "" Then
                For x As Integer = 0 To cmbSegmento.Items.Count - 1
                    If CType(cmbSegmento.Items(x), ListData)._Value = idSeg Then
                        cmbSegmento.SelectedIndex = x
                        Exit For
                    End If
                Next
                cmbSegmento.Visible = True
                lblSegmento.Visible = True
            End If
        Else
            cmbSegmento.Visible = False
            lblSegmento.Visible = False
        End If
    End Sub

    Private Sub btnAddCompany_Click(sender As System.Object, e As System.EventArgs)
        Dim frm As New SegmentCompany
        frm.ShowDialog()
    End Sub

    Private Sub btnConfirm_Click(sender As System.Object, e As System.EventArgs) Handles btnConfirm.Click
        Dim conf As New frm_Confirmar()
        Dim message As String = CapaPresentacion.Idiomas.get_str("str_535")
        Dim caption As String = CapaPresentacion.Idiomas.get_str("str_536")
        Dim buttons As MessageBoxButtons = MessageBoxButtons.YesNo
        Dim result As DialogResult = MessageBox.Show(message, caption, buttons)
        If result = Windows.Forms.DialogResult.Yes Then
            conf.datosBusqueda_hotel = datosBusqueda_hotel
            conf.ShowDialog()

            If conf.ds IsNot Nothing AndAlso conf.ds.Tables("HotelStatus") IsNot Nothing AndAlso conf.ds.Tables("HotelStatus").Rows.Count > 0 Then
                CapaLogicaNegocios.Hotels.HotelBook.consulta(Me)
                misForms.reservaciones.load_data()
                Timer1.Stop()
                Close()
            ElseIf conf.isCanceled = False Then
                If conf.ds IsNot Nothing AndAlso conf.ds.Tables("Error") IsNot Nothing AndAlso conf.ds.Tables("Error").Rows.Count > 0 Then
                    CapaLogicaNegocios.LogManager.agregar_registro("Error: Confirmar Autorización", conf.ds.GetXml())
                End If
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_537"))
            End If
        End If
    End Sub
End Class

Public Class ListData
    Private _Name As String
    Private __Value As String

    Public Sub New(ByVal Name As String, ByVal value As String)
        _Name = Name
        __Value = value
    End Sub

    Public Property Name()
        Get
            Return _Name
        End Get
        Set(ByVal value)
            _Name = value
        End Set
    End Property

    Public Property _Value()
        Get
            Return __Value
        End Get
        Set(ByVal value)
            __Value = value
        End Set
    End Property

End Class
