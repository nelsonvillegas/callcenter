<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mapa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pic_GRANDE = New System.Windows.Forms.PictureBox
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
        Me.txtInfo = New System.Windows.Forms.TextBox
        CType(Me.pic_GRANDE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pic_GRANDE
        '
        Me.pic_GRANDE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pic_GRANDE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_GRANDE.Location = New System.Drawing.Point(103, 12)
        Me.pic_GRANDE.Name = "pic_GRANDE"
        Me.pic_GRANDE.Size = New System.Drawing.Size(402, 302)
        Me.pic_GRANDE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pic_GRANDE.TabIndex = 2
        Me.pic_GRANDE.TabStop = False
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_aceptar.Location = New System.Drawing.Point(528, 523)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_aceptar.TabIndex = 4
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = True
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 3)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.ScrollBarsEnabled = False
        Me.WebBrowser1.Size = New System.Drawing.Size(591, 324)
        Me.WebBrowser1.TabIndex = 5
        '
        'txtInfo
        '
        Me.txtInfo.Location = New System.Drawing.Point(12, 327)
        Me.txtInfo.MaxLength = 0
        Me.txtInfo.Multiline = True
        Me.txtInfo.Name = "txtInfo"
        Me.txtInfo.ReadOnly = True
        Me.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfo.Size = New System.Drawing.Size(591, 190)
        Me.txtInfo.TabIndex = 6
        '
        'mapa
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_aceptar
        Me.ClientSize = New System.Drawing.Size(615, 558)
        Me.Controls.Add(Me.txtInfo)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Controls.Add(Me.pic_GRANDE)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mapa"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mapa"
        CType(Me.pic_GRANDE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pic_GRANDE As System.Windows.Forms.PictureBox
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents txtInfo As System.Windows.Forms.TextBox
End Class
