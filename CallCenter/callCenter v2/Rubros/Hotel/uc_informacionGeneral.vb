Public Class uc_informacionGeneral

    Public datosBusqueda_hotel As dataBusqueda_hotel_test

    Private Sub uc_informacionGeneral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'load_data()
    End Sub

    Public Sub load_data()
        Try
            Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOD(datosBusqueda_hotel)
            'inicializar_controles(ds)

            If ds Is Nothing Then
                data_lbl_nombreHotel.Text = ""
                WebBrowser1.DocumentText = CONST_WB_TOP + "" + CONST_WB_TOP
                Return
            End If

            If Not ds.Tables("error") Is Nothing Then
                Dim r As DataRow = ds.Tables("error").Rows(0)
                MessageBox.Show(r.Item("Message"), "Error: " + r.Item("ErrorCode"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Cursor = Cursors.Arrow
            Else
                Dim r_info As DataRow = ds.Tables("GeneralInformation").Rows(0)

                data_lbl_nombreHotel.Text = r_info.Item("Name")
                Dim desc As String = ""
                If ds.Tables.IndexOf("Description") <> -1 Then
                    For Each r_desc As DataRow In ds.Tables("Description").Rows
                        If r_desc.Item("Keyword").ToString.ToUpper = "DESC" Then
                            Dim temp As String = r_desc.Item("Value") 'herramientas.HTMLDecode(r_desc.Item("Value"))
                            If temp.Length > desc.Length Then
                                desc = r_desc.Item("Title") + ControlChars.CrLf + ControlChars.CrLf + temp + ControlChars.CrLf + ControlChars.CrLf + ControlChars.CrLf
                            End If
                        End If
                    Next
                End If
                If ds.Tables.IndexOf("Amenity") <> -1 Then
                    If ds.Tables("Amenity").Rows.Count > 0 Then
                        desc &= "<br /><b>" & CapaPresentacion.Idiomas.get_str("str_452") & ":</b><br /><UL>"
                        For Each r_desc As DataRow In ds.Tables("Amenity").Rows
                            desc &= "<LI>" & r_desc.Item("value") & "</LI>"
                        Next
                        desc &= "</UL>"
                    End If
                End If
                WebBrowser1.DocumentText = CONST_WB_TOP + desc + CONST_WB_TOP

                'txt_descripcion.Text = desc
                'txt_descripcion.SelectionStart = txt_descripcion.Text.Length
                'txt_descripcion.SelectionLength = 0
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0030", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Sub

    'Private Sub inicializar_controles(ByVal ds As DataSet)
    'End Sub

End Class
