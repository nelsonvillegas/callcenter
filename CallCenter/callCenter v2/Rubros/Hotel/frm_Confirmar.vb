﻿Public Class frm_Confirmar
    Public datosBusqueda_hotel As dataBusqueda_hotel_test
    Public ds As DataSet
    Public isCanceled As Boolean = False
    Private Sub frm_Confirmar_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        btnConfirmar.Text = CapaPresentacion.Idiomas.get_str("str_538")
        btnCancelar.Text = CapaPresentacion.Idiomas.get_str("str_0021_cancelar")
        lblAutorizacion.Text = CapaPresentacion.Idiomas.get_str("str_539") & ":"
        lblLetrero.Text = CapaPresentacion.Idiomas.get_str("str_540")
        Me.Text = CapaPresentacion.Idiomas.get_str("str_541")
        lblReservacion.Text = CapaPresentacion.Idiomas.get_str("str_542") & ":"

        lblNumeroReserv.Text = datosBusqueda_hotel.ConfirmNumber
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        isCanceled = True
        Me.Close()
    End Sub

    Private Sub btnConfirmar_Click(sender As System.Object, e As System.EventArgs) Handles btnConfirmar.Click
        If Not String.IsNullOrEmpty(txtAutorizacion.Text) Then
            ds = obtenGenerales_hoteles.obten_general_ConfirmReservation(datosBusqueda_hotel, txtAutorizacion.Text)
            Close()
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_540"))
        End If
    End Sub
End Class