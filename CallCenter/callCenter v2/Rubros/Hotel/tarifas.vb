Public Class tarifas

    Public botones() As String
    Public individual As Boolean = False
    Public taxInc As Boolean = False
    Public datosBusqueda_hotel As New dataBusqueda_hotel_test
    Public Thread_BuscaTarifas As Threading.Thread

    Public Sub cargar()
        Try
            CapaPresentacion.Common.Fija_CTLVISIBLE(PictureBox2, True)

            Thread_BuscaTarifas = New Threading.Thread(AddressOf load_data)
            Thread_BuscaTarifas.Start()
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0028", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Sub

    Private Sub load_data()
        Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOC(datosBusqueda_hotel)
        CapaPresentacion.Hotels.HotelRates.FIJA_FORM(Me, ds)
    End Sub

    Private Sub cambio()
        If UltraGrid1.ActiveRow Is Nothing Then Return
        If misForms.reservar.tipoAccion = enumReservar_TipoAccion.modificar Then
            If misForms.reservar.rbt_ninguno.Checked And UltraGrid1.ActiveRow.Cells("GuarDep").Value <> "N" Then
                MsgBox(CapaPresentacion.Idiomas.get_str("str_422_Restrict"), MsgBoxStyle.Information)
                Return
            End If
        End If
        misForms.reservar.datosBusqueda_hotel.habitaciones_ = datosBusqueda_hotel.habitaciones_.copy

        If UltraGrid1.ActiveRow.Cells.Exists("PlanCode") Then
            misForms.reservar.datosBusqueda_hotel.RateCode = UltraGrid1.ActiveRow.Cells("PlanCode").Value
        Else
            misForms.reservar.datosBusqueda_hotel.RateCode = UltraGrid1.ActiveRow.ParentRow.Cells("PlanCode").Value
        End If
        misForms.reservar.datosBusqueda_hotel.GuarDep = UltraGrid1.ActiveRow.Cells("GuarDep").Value
        misForms.reservar.datosBusqueda_hotel.TotalAvgRate = UltraGrid1.ActiveRow.Cells("TotalAvgRate").Value
        misForms.reservar.datosBusqueda_hotel.AvgRate = UltraGrid1.ActiveRow.Cells("AvgRate").Value
        misForms.reservar.datosBusqueda_hotel.AltTotalStay = UltraGrid1.ActiveRow.Cells("AltTotalStay").Value

        Try
            Dim ds As DataSet = UltraGrid1.DataSource
            Dim TaxIncluded As String
            If ds.Tables("property").Rows(0).Item("TaxIncluded").ToString.ToUpper = "N" Then TaxIncluded = "False" Else TaxIncluded = "True"

            misForms.reservar.datosBusqueda_hotel.PlusTax = TaxIncluded
            'misForms.reservar.datosBusqueda_hotel.AltTax = ds.Tables("property").Rows(0).Item("AltTax")
            'misForms.reservar.datosBusqueda_hotel.AltCurrency = ds.Tables("property").Rows(0).Item("AltCurrency")
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0029", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try

        misForms.reservar.cambio_tar = True
        Close()
    End Sub

    '

    Private Sub tarifas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CapaPresentacion.Hotels.HotelRates.InitializeForm(Me)
    End Sub

    Private Sub tarifas_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        UltraGrid1.Focus()
    End Sub

    Private Sub tarifas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If Not individual Then
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifas").SharedProps.Enabled = True

            For Each frm As Form In Me.MdiParent.MdiChildren
                If frm.Name = "busquedaHotel" Then
                    Dim frm_bro As busquedaHotel = CType(frm, busquedaHotel)
                    menuContextual.inicializar_items(frm_bro.ContextMenuStrip1, frm_bro.botones)
                End If
            Next

            misForms.tarifas = Nothing
        End If
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        If Not individual Then
            misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
        Else
            cambio()
        End If
    End Sub

    Private Sub UltraGrid1_InitializeRow(sender As Object, e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UltraGrid1.InitializeRow
        'If e.Row.Index > 0 Then
        If e.Row.Cells.IndexOf("RowType") <> -1 AndAlso Not String.IsNullOrEmpty(e.Row.Cells("RowType").Text) Then

            If e.Row.Cells("RowType").Text = "Desc" AndAlso e.Row.Cells.IndexOf("TotalTotalSinDesc") <> -1 AndAlso e.Row.Cells.IndexOf("TotalTotal") <> -1 Then
                e.Row.Cells("TotalTotal").Appearance.ForeColor = Color.Green
                e.Row.Cells("TotalTotalSinDesc").Appearance.FontData.Strikeout = Infragistics.Win.DefaultableBoolean.True
            ElseIf e.Row.Cells.IndexOf("TotalTotal") <> -1 Then
                e.Row.Cells("TotalTotal").Value = String.Empty
            End If
            'End If
        End If
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                If Not individual Then
                    misForms.exe_accion(ContextMenuStrip1, "btn_reservar")
                Else
                    cambio()
                End If
        End Select
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)
    End Sub

    Private Sub UltraGrid1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraGrid1.KeyPress
        Select Case Asc(e.KeyChar)
            Case Keys.Escape
                e.KeyChar = Chr(0)
        End Select
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow
        If Not individual Then
            misForms.exe_accion(ContextMenuStrip1, "btn_reservar")
        Else
            cambio()
        End If
    End Sub

    Private Sub data_txt_adultos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            btn_actualizar.PerformClick()
        End If
    End Sub

    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        datosBusqueda_hotel.habitaciones_ = Uc_habitaciones1.Habitaciones '.copy - no es nesesario por que la propiedad ya regresa con el copy

        cargar()
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        cambio()
    End Sub

    Private Sub UltraGrid1_AfterSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BandEventArgs) Handles UltraGrid1.AfterSortChange
        CapaPresentacion.Common.NumberGrid(UltraGrid1)
    End Sub

    Private Sub UltraGrid1_BeforeSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeSortChangeEventArgs) Handles UltraGrid1.BeforeSortChange
        For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In e.SortedColumns
            If col.Key = "TotalTotal" Then
                Dim sortAsc As Boolean = IIf(UltraGrid1.DisplayLayout.Bands(0).Columns("TotalOrdenar").SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, True, False)
                e.SortedColumns.Remove("TotalTotal")
                e.SortedColumns.Add("TotalOrdenar", sortAsc)

                Exit For
            End If
        Next
    End Sub

End Class