﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_disponibilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_disponibilidad))
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.lblHotel = New System.Windows.Forms.Label()
        Me.lblHotelData = New System.Windows.Forms.Label()
        Me.btnAtras = New System.Windows.Forms.Button()
        Me.btnAdelante = New System.Windows.Forms.Button()
        Me.rbDisponibles = New System.Windows.Forms.RadioButton()
        Me.rbOcupados = New System.Windows.Forms.RadioButton()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.cmbHotel = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.btnMostrar = New System.Windows.Forms.Button()
        CType(Me.cmbHotel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(5, 33)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(484, 267)
        Me.WebBrowser1.TabIndex = 0
        '
        'lblHotel
        '
        Me.lblHotel.AutoSize = True
        Me.lblHotel.Location = New System.Drawing.Point(2, 9)
        Me.lblHotel.Name = "lblHotel"
        Me.lblHotel.Size = New System.Drawing.Size(38, 13)
        Me.lblHotel.TabIndex = 1
        Me.lblHotel.Text = "Hotel :"
        '
        'lblHotelData
        '
        Me.lblHotelData.AutoSize = True
        Me.lblHotelData.ForeColor = System.Drawing.Color.Blue
        Me.lblHotelData.Location = New System.Drawing.Point(56, 9)
        Me.lblHotelData.Name = "lblHotelData"
        Me.lblHotelData.Size = New System.Drawing.Size(10, 13)
        Me.lblHotelData.TabIndex = 2
        Me.lblHotelData.Text = "."
        '
        'btnAtras
        '
        Me.btnAtras.Location = New System.Drawing.Point(392, 6)
        Me.btnAtras.Name = "btnAtras"
        Me.btnAtras.Size = New System.Drawing.Size(35, 23)
        Me.btnAtras.TabIndex = 4
        Me.btnAtras.Text = "<<"
        Me.btnAtras.UseVisualStyleBackColor = True
        '
        'btnAdelante
        '
        Me.btnAdelante.Location = New System.Drawing.Point(433, 6)
        Me.btnAdelante.Name = "btnAdelante"
        Me.btnAdelante.Size = New System.Drawing.Size(35, 23)
        Me.btnAdelante.TabIndex = 5
        Me.btnAdelante.Text = ">>"
        Me.btnAdelante.UseVisualStyleBackColor = True
        '
        'rbDisponibles
        '
        Me.rbDisponibles.AutoSize = True
        Me.rbDisponibles.Checked = True
        Me.rbDisponibles.Location = New System.Drawing.Point(2, 306)
        Me.rbDisponibles.Name = "rbDisponibles"
        Me.rbDisponibles.Size = New System.Drawing.Size(79, 17)
        Me.rbDisponibles.TabIndex = 6
        Me.rbDisponibles.TabStop = True
        Me.rbDisponibles.Text = "Disponibles"
        Me.rbDisponibles.UseVisualStyleBackColor = True
        '
        'rbOcupados
        '
        Me.rbOcupados.AutoSize = True
        Me.rbOcupados.Location = New System.Drawing.Point(2, 329)
        Me.rbOcupados.Name = "rbOcupados"
        Me.rbOcupados.Size = New System.Drawing.Size(74, 17)
        Me.rbOcupados.TabIndex = 6
        Me.rbOcupados.TabStop = True
        Me.rbOcupados.Text = "Ocupados"
        Me.rbOcupados.UseVisualStyleBackColor = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(87, 306)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 7
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(414, 303)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cancelar.TabIndex = 20
        Me.btn_Cancelar.Text = "Salir"
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'cmbHotel
        '
        Me.cmbHotel.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.cmbHotel.Location = New System.Drawing.Point(46, 5)
        Me.cmbHotel.Name = "cmbHotel"
        Me.cmbHotel.Size = New System.Drawing.Size(215, 21)
        Me.cmbHotel.TabIndex = 21
        '
        'btnMostrar
        '
        Me.btnMostrar.Location = New System.Drawing.Point(306, 303)
        Me.btnMostrar.Name = "btnMostrar"
        Me.btnMostrar.Size = New System.Drawing.Size(50, 23)
        Me.btnMostrar.TabIndex = 22
        Me.btnMostrar.Text = "Mostrar"
        Me.btnMostrar.UseVisualStyleBackColor = True
        '
        'frm_disponibilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(497, 349)
        Me.Controls.Add(Me.btnMostrar)
        Me.Controls.Add(Me.cmbHotel)
        Me.Controls.Add(Me.btn_Cancelar)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.rbOcupados)
        Me.Controls.Add(Me.rbDisponibles)
        Me.Controls.Add(Me.btnAdelante)
        Me.Controls.Add(Me.btnAtras)
        Me.Controls.Add(Me.lblHotelData)
        Me.Controls.Add(Me.lblHotel)
        Me.Controls.Add(Me.WebBrowser1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_disponibilidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Disponibilidad"
        CType(Me.cmbHotel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents lblHotel As System.Windows.Forms.Label
    Friend WithEvents lblHotelData As System.Windows.Forms.Label
    Friend WithEvents btnAtras As System.Windows.Forms.Button
    Friend WithEvents btnAdelante As System.Windows.Forms.Button
    Friend WithEvents rbDisponibles As System.Windows.Forms.RadioButton
    Friend WithEvents rbOcupados As System.Windows.Forms.RadioButton
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn_Cancelar As System.Windows.Forms.Button
    Friend WithEvents cmbHotel As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents btnMostrar As System.Windows.Forms.Button
End Class
