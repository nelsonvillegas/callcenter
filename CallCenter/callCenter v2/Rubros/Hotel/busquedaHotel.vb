Public Class busquedaHotel

    Public botones() As String
    Public lista_tarifas As New List(Of tarifas)
    Public datosBusqueda_hotel As New dataBusqueda_hotel_test
    Public Thread_BuscaHoteles As Threading.Thread
    Public Thread_AgregaTarifas As Threading.Thread
    Public Thread_BuscarConvenios As Threading.Thread
    Public Thread_ColoresTarifas As System.Threading.Thread

    'Public avance As Integer
    'Public total As Integer
    Private cont As Integer = 0
    Private ultima_letra_buscada As String
    Private ultimo_hotel_buscado As Integer = -1
    Public dsRateplans As DataSet
    'Delegate Sub Fija_BTNActualizar_BarCallback()

    Public Sub llena()
        Try
            herramientas.llenar(rbt_ciudad.Checked, ComboSearch1.TextBox1.Text, ComboSearch1)
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0004", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Public Sub ctlKeyPress(ByVal s As Object, ByVal e As KeyPressEventArgs)
        Try
            If Asc(e.KeyChar) = Keys.Enter Then
                btn_buscar.PerformClick()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0012", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub busquedaHotel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'If ws_login.getUserAccessInfo.IsidCorporativoNull OrElse ws_login.getUserAccessInfo().idCorporativo = -1 Then
            'txt_Conv.Text = String.Empty
            'txt_Conv.Visible = False
            'Else
            txt_Conv.Visible = True
            'End If
            lblConvenio.Visible = txt_Conv.Visible

            'CapaLogicaNegocios.Calls.VerificaNewCall()
            If dataOperador.Hoteles.Count = 0 Then btnPasiva.Visible = False

            CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
            CapaPresentacion.Hotels.SearchHotel.InitializeForm(Me)
            CargaAreas("")
            CargaHoteles()

            If datosBusqueda_hotel.IsCopy Then
                CapaPresentacion.Hotels.SearchHotel.loadCopyForm(Me)
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0005", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    '

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_out.ValueChanged, dtp_in.ValueChanged
        herramientas.fechas_inteligentes(dtp_in, dtp_out, sender, 1, txt_noches)
    End Sub

    Private Sub rbt_todosHoteles_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbt_todosHoteles.CheckedChanged, rbt_tarifas.CheckedChanged, rbt_hotelesDisponibles.CheckedChanged
        Try
            datosBusqueda_hotel.todosHoteles = rbt_todosHoteles.Checked
            If Me.rbt_tarifas.Checked Then
                If Not IsNumeric(txtMin.Text) Then txtMin.Text = "0"
                If Not IsNumeric(txtMax.Text) Then txtMax.Text = "0"
            End If
            CapaPresentacion.Hotels.SearchHotel.FilterGrid(Me)
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0014", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub busquedaHotel_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            CapaLogicaNegocios.Hotels.SearchHotel.StopSearch(Me)

            Do While lista_tarifas.Count > 0
                lista_tarifas(0).Close()            'si se elimina esta linea, tambien quitar lavariable de lista
                lista_tarifas.RemoveAt(0)
                If misForms.form1 Is Nothing Then
                    Application.Exit()
                End If
            Loop

            If misForms.form1 Is Nothing Then
                Return
            End If

            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_buscar").SharedProps.Enabled = True
            If misForms.form1.UltraDockManager1.DockAreas(0).Panes.Exists("informacionGeneral") Then misForms.form1.UltraDockManager1.DockAreas(0).Panes("informacionGeneral").Close()
            'CapaPresentacion.Common.FijaProgressBar(Me, 100, False)
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0278_errCloseWin"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        Try
            datosBusqueda_hotel.PropertyNumber = UltraGrid1.ActiveRow.Cells("PropertyNumber").Value.ToString
            CapaPresentacion.Hotels.SearchHotel.EnableButtons()
            CapaPresentacion.Hotels.SearchHotel.SetSearchItemData(datosBusqueda_hotel)
            CapaPresentacion.Idiomas.cambiar_busquedaHotel(Me)
            If Not IsNothing(misForms.form1.UltraDockManager1.PaneFromKey("informacionGeneral")) Then
                'misForms.form1.UltraDockManager1.PaneFromKey("informacionGeneral").Close()
                'misForms.show_UC("informacionGeneral", CapaPresentacion.Idiomas.get_str("str_0112_infoGeneralHotel"), GetType(uc_informacionGeneral).FullName)
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0015", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow, UltraGrid1.DoubleClickRow
        Try
            'esto se ejecuta 2 veces, por eso con CONT se controla para que solo entre una vez

            If cont = 0 Then
                misForms.exe_accion(ContextMenuStrip1, "btn_tarifas")
                cont += 1
            Else
                cont -= 1
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0016", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown, UltraGrid1.MouseDown
        Try
            herramientas.selRow(UltraGrid1, e)
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0017", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                misForms.exe_accion(ContextMenuStrip1, "btn_tarifas")
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0018", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub btn_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_buscar.Click
        Try
            If Not CapaLogicaNegocios.Hotels.SearchHotel.AllowNumberDays(Me) Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_315_err99noches"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Dim continuar As Boolean = True
            If cmbHotel.Visible Then

            Else
                If (ComboSearch1.ListBox1.SelectedIndex = -1 OrElse ComboSearch1.lista_lugares.Count = 0) Then
                    MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    continuar = False
                End If
            End If

            If continuar Then
                CapaPresentacion.Hotels.SearchHotel.SetControlsData(Me)

                'total = 0
                'CapaPresentacion.Common.FijaProgressBar(Me, total, True)

                UltraGrid1.DataSource = Nothing
                UltraGrid1.DataMember = ""
                data_lbl_totalHoteles.Text = ""

                CapaPresentacion.Common.Fija_CTLVISIBLE(PictureBox2, True)
                Me.rbt_todosHoteles.Enabled = False
                Me.rbt_tarifas.Enabled = False
                Me.rbt_hotelesDisponibles.Enabled = False
                Thread_BuscaHoteles = New Threading.Thread(AddressOf load_data)
                Thread_BuscaHoteles.Start()
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0019", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    '

    Private Sub load_data()
        Try
            Dim ds As New DataSet
            ds = obtenGenerales_hoteles.obten_general_HOI(datosBusqueda_hotel)
            CapaPresentacion.Hotels.SearchHotel.FIJA_FORM(Me, ds)
        Catch ex As Exception
            If ex.GetType IsNot GetType(System.Threading.ThreadAbortException) Then CapaLogicaNegocios.ErrorManager.Manage("E0020", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Private Sub btn_detener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_detener.Click
        CapaLogicaNegocios.Hotels.SearchHotel.StopSearch(Me)
    End Sub

    Private Sub UltraGrid1_AfterSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BandEventArgs) Handles UltraGrid1.AfterSortChange
        CapaPresentacion.Common.NumberGrid(UltraGrid1)
    End Sub

    Private Sub UltraGrid1_BeforeSortChange(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeSortChangeEventArgs) Handles UltraGrid1.BeforeSortChange
        For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In e.SortedColumns
            If col.Key = "laCategoria" Then
                Dim sortAsc As Boolean = IIf(UltraGrid1.DisplayLayout.Bands(0).Columns("laCategoriaNumero").SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, True, False)
                e.SortedColumns.Remove("laCategoria")
                e.SortedColumns.Add("laCategoriaNumero", sortAsc)

                Exit For
            End If

            If col.Key = "Tarifas" Then
                Dim sortAsc As Boolean = IIf(UltraGrid1.DisplayLayout.Bands(0).Columns("TarifaMenorOrdenar").SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, True, False)
                e.SortedColumns.Remove("Tarifas")
                e.SortedColumns.Add("TarifaMenorOrdenar", sortAsc)

                Exit For
            End If
        Next
    End Sub

    Private Sub uce_moneda_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uce_moneda.SelectionChanged
        moneda_selected = uce_moneda.SelectedItem.DataValue
    End Sub

    Private Sub ComboSearch1_Selected() Handles ComboSearch1.Selected
        Call CargaAreas(ComboSearch1.selected_valor_idCiudad)
    End Sub

    Private Sub CargaAreas(ByVal idCiudad As String)
        cmbArea.Items.Clear()
        cmbArea.Items.Add("", "--- " & CapaPresentacion.Idiomas.get_str("str_0259_cualquiera") & " ---")
        If idCiudad <> "" Then
            Dim ds As DataSet = herramientas.get_Areas(idCiudad)
            If Not IsNothing(ds) AndAlso ds.Tables.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    cmbArea.Items.Add(dr("idArea"), dr("Area"))
                Next
            End If
        End If
        cmbArea.SelectedIndex = 0
    End Sub

    Private Sub txtMin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMin.ValueChanged
        If Not IsNumeric(txtMin.Text) Then txtMin.Text = "0"
        If Me.rbt_tarifas.Checked Then CapaPresentacion.Hotels.SearchHotel.FilterGrid(Me)
    End Sub

    Private Sub txtMax_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMax.ValueChanged
        If Not IsNumeric(txtMax.Text) Then txtMax.Text = "0"
        If Me.rbt_tarifas.Checked Then CapaPresentacion.Hotels.SearchHotel.FilterGrid(Me)
    End Sub

    Private Sub btnHotel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHotel.Click
        Dim frm As New frm_HotelporNombre
        frm.ShowDialog()
        If frm.Ciudad <> "" Then
            ComboSearch1.TextBox1.Text = frm.Ciudad
            'ComboSearch1.TextBox1.Focus()
        End If
    End Sub

    Private Sub rbt_Hotel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbt_Hotel.CheckedChanged
        If rbt_Hotel.Checked Then
            lbl_lugar.Text = "Hotel:"
            cmbHotel.Visible = True
            ComboSearch1.Visible = False
        Else
            Dim rm As New System.Resources.ResourceManager("callCenter.Idiomas", System.Reflection.Assembly.GetExecutingAssembly())
            'System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(_cultura)
            lbl_lugar.Text = rm.GetString("str_0003_lugar") + ":"
            cmbHotel.Visible = False
            ComboSearch1.Visible = True
        End If
    End Sub

    Private Sub CargaHoteles()
        cmbHotel.Items.Clear()

        For Each dr As dataHotel In dataOperador.Hoteles
            cmbHotel.Items.Add(dr.index, dr.Nombre)
        Next
        cmbHotel.SelectedIndex = 0
    End Sub

    Private Sub btnPasiva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPasiva.Click
        If Not CapaLogicaNegocios.Hotels.SearchHotel.AllowNumberDays(Me) Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_315_err99noches"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Dim frm As New tarifas_personalizadas
        frm.datosBusqueda_hotel = datosBusqueda_hotel

        frm.datosBusqueda_hotel.PropertyNumber = dataOperador.Hoteles(cmbHotel.Value).idHotel
        frm.datosBusqueda_hotel.PropertyName = dataOperador.Hoteles(cmbHotel.Value).Nombre
        frm.datosBusqueda_hotel.ciudad_nombre = dataOperador.Hoteles(cmbHotel.Value).Ciudad
        frm.datosBusqueda_hotel.llegada = Me.dtp_in.Value
        frm.datosBusqueda_hotel.salida = Me.dtp_out.Value
        frm.datosBusqueda_hotel.habitaciones_ = Me.Uc_habitaciones1.Habitaciones
        frm.datosBusqueda_hotel.OnRequest = True
        frm.ShowDialog()
        If frm.accept Then
            Dim dat As dataBusqueda_hotel_test = frm.datosBusqueda_hotel
            Dim frm2 As New reservar()
            frm2.datosBusqueda_hotel = dat
            If frm2.cargar(Nothing) Then
                frm2.ShowDialog()
                CapaPresentacion.Calls.ActivaNewCall()
            Else
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_378_errServ"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        End If
    End Sub

    Private Sub txt_noches_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_noches.KeyPress
        If (e.KeyChar < Chr(48) Or e.KeyChar > Chr(57)) And e.KeyChar <> Chr(8) Then

            e.Handled = True

        End If

    End Sub

    Private Sub txt_noches_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_noches.ValueChanged
        If txt_noches.Text.Trim = "" Or txt_noches.Text.Trim = "0" Then txt_noches.Text = "1"
        txt_noches.Text = CInt(txt_noches.Text.Trim)
        dtp_out.Value = dtp_in.Value.AddDays(txt_noches.Text)
    End Sub

    Private Sub UltraGrid1_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout

    End Sub

    Private Sub txt_Conv_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_Conv.TextChanged
        If txt_Conv.Text.Length > 0 AndAlso (String.IsNullOrEmpty(ultima_letra_buscada) OrElse Not txt_Conv.Text.ToLower().StartsWith(ultima_letra_buscada)) Then
            ultima_letra_buscada = txt_Conv.Text.ToLower()(0)
            Thread_BuscaHoteles = New Threading.Thread(AddressOf buscar_convenios)
            Thread_BuscaHoteles.Start()

            'txt_Conv.Width = TextRenderer.MeasureText(txt_Conv.Text, txt_Conv.Font).Width + 10
        End If
    End Sub

    Private Sub buscar_convenios()
        Try
            '   Mostrar indicador de tarea asincrona
            Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_convenios(ultima_letra_buscada)
            CapaPresentacion.Hotels.SearchHotel.FIJA_CONVENIOS(Me, ds, ultima_letra_buscada)
        Catch ex As Exception
            'If ex.GetType IsNot GetType(System.Threading.ThreadAbortException) Then CapaLogicaNegocios.ErrorManager.Manage("E0020", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub


    Private Sub txt_Rateplan_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_Rateplan.TextChanged
        If txt_Rateplan.Text.Length > 0 AndAlso ultimo_hotel_buscado <> dataOperador.Hoteles(cmbHotel.Value).idHotel Then
            ultimo_hotel_buscado = dataOperador.Hoteles(cmbHotel.Value).idHotel
            Thread_BuscaHoteles = New Threading.Thread(AddressOf buscar_ratesplan)
            Thread_BuscaHoteles.Start()

            'txt_Conv.Width = TextRenderer.MeasureText(txt_Conv.Text, txt_Conv.Font).Width + 10
        End If
    End Sub

    Private Sub buscar_ratesplan()
        Try
            '   Borrar autocompletado anterior
            CapaPresentacion.Hotels.SearchHotel.FIJA_RATESPLAN(Me, Nothing)

            '   Mostrar indicador de tarea asincrona
            dsRateplans = CapaLogicaNegocios.GetWS.obten_ratesplans(ultimo_hotel_buscado)
            CapaPresentacion.Hotels.SearchHotel.FIJA_RATESPLAN(Me, dsRateplans)
        Catch ex As Exception
            'If ex.GetType IsNot GetType(System.Threading.ThreadAbortException) Then CapaLogicaNegocios.ErrorManager.Manage("E0020", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub
End Class