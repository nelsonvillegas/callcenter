﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Confirmar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnConfirmar = New System.Windows.Forms.Button()
        Me.lblReservacion = New System.Windows.Forms.Label()
        Me.lblLetrero = New System.Windows.Forms.Label()
        Me.lblAutorizacion = New System.Windows.Forms.Label()
        Me.txtAutorizacion = New System.Windows.Forms.TextBox()
        Me.lblNumeroReserv = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnConfirmar
        '
        Me.btnConfirmar.Location = New System.Drawing.Point(147, 127)
        Me.btnConfirmar.Name = "btnConfirmar"
        Me.btnConfirmar.Size = New System.Drawing.Size(75, 23)
        Me.btnConfirmar.TabIndex = 0
        Me.btnConfirmar.Text = "Confirmar"
        Me.btnConfirmar.UseVisualStyleBackColor = True
        '
        'lblReservacion
        '
        Me.lblReservacion.AutoSize = True
        Me.lblReservacion.Location = New System.Drawing.Point(37, 26)
        Me.lblReservacion.Name = "lblReservacion"
        Me.lblReservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblReservacion.TabIndex = 1
        Me.lblReservacion.Text = "Reservación:"
        '
        'lblLetrero
        '
        Me.lblLetrero.AutoSize = True
        Me.lblLetrero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLetrero.Location = New System.Drawing.Point(26, 55)
        Me.lblLetrero.Name = "lblLetrero"
        Me.lblLetrero.Size = New System.Drawing.Size(232, 13)
        Me.lblLetrero.TabIndex = 3
        Me.lblLetrero.Text = "Por favor, introduzca el número de autorización."
        '
        'lblAutorizacion
        '
        Me.lblAutorizacion.AutoSize = True
        Me.lblAutorizacion.Location = New System.Drawing.Point(40, 90)
        Me.lblAutorizacion.Name = "lblAutorizacion"
        Me.lblAutorizacion.Size = New System.Drawing.Size(68, 13)
        Me.lblAutorizacion.TabIndex = 4
        Me.lblAutorizacion.Text = "Autorización:"
        '
        'txtAutorizacion
        '
        Me.txtAutorizacion.Location = New System.Drawing.Point(114, 87)
        Me.txtAutorizacion.Name = "txtAutorizacion"
        Me.txtAutorizacion.Size = New System.Drawing.Size(126, 20)
        Me.txtAutorizacion.TabIndex = 5
        '
        'lblNumeroReserv
        '
        Me.lblNumeroReserv.AutoSize = True
        Me.lblNumeroReserv.Location = New System.Drawing.Point(114, 26)
        Me.lblNumeroReserv.Name = "lblNumeroReserv"
        Me.lblNumeroReserv.Size = New System.Drawing.Size(63, 13)
        Me.lblNumeroReserv.TabIndex = 6
        Me.lblNumeroReserv.Text = "NumReserv"
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(43, 127)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frm_Confirmar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(279, 177)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.lblNumeroReserv)
        Me.Controls.Add(Me.txtAutorizacion)
        Me.Controls.Add(Me.lblAutorizacion)
        Me.Controls.Add(Me.lblLetrero)
        Me.Controls.Add(Me.lblReservacion)
        Me.Controls.Add(Me.btnConfirmar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_Confirmar"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Confirmar Reservación"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnConfirmar As System.Windows.Forms.Button
    Friend WithEvents lblReservacion As System.Windows.Forms.Label
    Friend WithEvents lblLetrero As System.Windows.Forms.Label
    Friend WithEvents lblAutorizacion As System.Windows.Forms.Label
    Friend WithEvents txtAutorizacion As System.Windows.Forms.TextBox
    Friend WithEvents lblNumeroReserv As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
