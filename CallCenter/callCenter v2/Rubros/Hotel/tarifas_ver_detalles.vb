﻿Public Class tarifas_ver_detalles
    Public Sub Cargar(ByVal ratecode As String, ByVal ds As DataSet)
        If ds.Tables.Contains("RatePlan") Then
            With ds.Tables("RatePlan")
                For Each row As DataRow In .Rows
                    If row.RowState <> DataRowState.Deleted AndAlso Not String.IsNullOrEmpty(row("PlanCode")) AndAlso row("PlanCode") = ratecode Then
                        Dim prices() As DataRow = row.GetChildRows("RatePlan_Price")
                        lvDias.Columns.Clear()
                        lvDias.Items.Clear()
                        lvDias.Columns.Add(String.Empty)
                        Dim itemCnDesc As New ListViewItem
                        Dim itemSnDesc As New ListViewItem
                        itemCnDesc.Text = ratecode
                        itemSnDesc.Text = String.Format("{0} {1}", ratecode, CapaPresentacion.Idiomas.get_str("str_530_sin_desc"))
                        itemCnDesc.UseItemStyleForSubItems = False
                        itemSnDesc.UseItemStyleForSubItems = False

                        Dim cnDesc, snDesc As Decimal
                        For Each p As DataRow In prices
                            Try
                                lvDias.Columns.Add(p("Day"))

                                cnDesc = 0
                                snDesc = 0
                                If Not String.IsNullOrEmpty(p("AltTotal2")) Then cnDesc = Decimal.Parse(p("AltTotal2"))
                                If Not String.IsNullOrEmpty(p("AltTotal")) Then snDesc = Decimal.Parse(p("AltTotal"))

                                Dim subCnDesc As ListViewItem.ListViewSubItem
                                Dim subSnDesc As ListViewItem.ListViewSubItem

                                If cnDesc = 0 Then
                                    subCnDesc = itemCnDesc.SubItems.Add(CapaPresentacion.Idiomas.get_str("str_531_gratis"))
                                Else
                                    subCnDesc = itemCnDesc.SubItems.Add(String.Format("{0:c}", cnDesc))
                                End If
                                If snDesc = 0 Then
                                    subSnDesc = itemSnDesc.SubItems.Add(CapaPresentacion.Idiomas.get_str("str_531_gratis"))
                                Else
                                    subSnDesc = itemSnDesc.SubItems.Add(String.Format("{0:c}", snDesc))
                                End If



                                If cnDesc > 0 AndAlso cnDesc < snDesc Then
                                    subCnDesc.ForeColor = Color.Green
                                    subSnDesc.Font = New Font(subSnDesc.Font, subSnDesc.Font.Style Xor FontStyle.Strikeout)
                                Else
                                    subCnDesc.Text = -String.Empty
                                End If
                            Catch
                            End Try
                        Next
                        lvDias.Items.Add(itemCnDesc)
                        lvDias.Items.Add(itemSnDesc)
                        lvDias.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    End If
                Next
            End With
        End If
    End Sub

    Private Sub tarifas_ver_detalles_Resize(sender As System.Object, e As System.EventArgs) Handles MyBase.Resize
        lvDias.Width = Me.Width
        btn_ok.Left = Me.Width / 2 - btn_ok.Width / 2
    End Sub
End Class