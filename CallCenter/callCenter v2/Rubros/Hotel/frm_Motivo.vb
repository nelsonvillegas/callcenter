Public Class frm_Motivo
    Public texto As String

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox1.Text.Trim = "" Then
            MsgBox("Debe proporcionar un motivo de cancelación", MsgBoxStyle.Exclamation)
        Else
            texto = Me.TextBox1.Text.Trim
            Close()

        End If
    End Sub

    Private Sub frm_Motivo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = CapaPresentacion.Idiomas.get_str("str_465")
        Me.Button1.Text = CapaPresentacion.Idiomas.get_str("str_0060_aceptar")
        Me.lblTitle.Text = CapaPresentacion.Idiomas.get_str("str_466")
    End Sub
End Class