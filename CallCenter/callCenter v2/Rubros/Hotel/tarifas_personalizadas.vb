Public Class tarifas_personalizadas
    Public datosBusqueda_hotel As New dataBusqueda_hotel_test
    Public accept As Boolean = False

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim valid As Boolean = True
        If Not IsNumeric(txtTarifa.Text.Trim) Or txtTarifa.Text.Trim = "" Then
            valid = False
        Else
            If txtTarifa.Text.Trim <> "" AndAlso CDec(txtTarifa.Text.Trim) < 0 Then
                valid = False
            End If
        End If
        If Not valid Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_462"))
            Exit Sub
        End If
        If Not IsNumeric(txtTax.Text.Trim) Or txtTax.Text.Trim = "" Then
            valid = False
        Else
            If txtTax.Text.Trim <> "" AndAlso CDec(txtTax.Text.Trim) < 0 Then
                valid = False
            End If
        End If
        If Not valid Then
            MsgBox(CapaPresentacion.Idiomas.get_str("str_463"))
            Exit Sub
        End If
        datosBusqueda_hotel = get_datosBusqueda()
        accept = True
        Close()
    End Sub


    Private Function get_datosBusqueda() As dataBusqueda_hotel_test
        Dim dat_bus As dataBusqueda_hotel_test = datosBusqueda_hotel
        dat_bus.habitaciones_ = Me.Uc_habitaciones1.Habitaciones
        dat_bus.RateCode = cmbTipo.SelectedItem._value
        dat_bus.ServiceProvider = 0
        dat_bus.GuarDep = "N"
        dat_bus.noches = DateDiff(DateInterval.Day, CDate(dat_bus.llegada.ToString("dd/MMM/yyyy")), CDate(dat_bus.salida.ToString("dd/MMM/yyyy")))
        dat_bus.TotalAvgRate = CDec(txtTarifa.Text) * dat_bus.noches
        dat_bus.AvgRate = txtTarifa.Text
        dat_bus.AltCurrency = uce_moneda.SelectedItem.DataValue
        dat_bus.monedaBusqueda = dat_bus.AltCurrency
        dat_bus.AltTotalStay = CDec(dat_bus.AvgRate) * dat_bus.noches * dat_bus.habitaciones_.TotalHabitaciones
        dat_bus.PlusTax = "false"
        dat_bus.AltTax = dat_bus.AltTotalStay * (CDec(txtTax.Text.Trim) / 100)
        dat_bus.Tax = txtTax.Text.Trim
        dat_bus.nombreHabitacion = cmbTipo.Text
        dat_bus.PlanName = ""
        dat_bus.PlanDescription = ""
        dat_bus.Segmento = ""
        'dat_bus.PlusTax = True
        Return dat_bus
    End Function

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        accept = False
        Close()
    End Sub

    Private Sub tarifas_personalizadas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CapaPresentacion.Idiomas.cambiar_UC_habitaciones(Me.Uc_habitaciones1)
        Me.lblCheckin.Text = CapaPresentacion.Idiomas.get_str("str_0092_llegada")
        Me.lblCheckout.Text = CapaPresentacion.Idiomas.get_str("str_0093_salida")
        Me.lblTipo.Text = CapaPresentacion.Idiomas.get_str("str_0141_tipoRoom")
        Me.lblTarifa.Text = CapaPresentacion.Idiomas.get_str("str_341_tarNoche")

        CargaHabitaciones()
        CapaPresentacion.Common.moneda.FillUceMoneda(uce_moneda)
        Me.lblHotelName.Text = Me.datosBusqueda_hotel.PropertyName
        Me.lblLlegada.Text = Me.datosBusqueda_hotel.llegada.ToString("dd/MMM/yyyy")
        Me.lblSalida.Text = Me.datosBusqueda_hotel.salida.ToString("dd/MMM/yyyy")
        Me.Uc_habitaciones1.Habitaciones = Me.datosBusqueda_hotel.habitaciones_
        Me.btnAceptar.Text = CapaPresentacion.Idiomas.get_str("str_0232_addReservation")
        Me.btn_cancelar.Text = CapaPresentacion.Idiomas.get_str("str_0021_cancelar")
        Me.Text = CapaPresentacion.Idiomas.get_str("str_460")
        Me.lblTax.Text = CapaPresentacion.Idiomas.get_str("str_464")
    End Sub

    Private Sub CargaHabitaciones()
        cmbTipo.DisplayMember = "Name"
        cmbTipo.ValueMember = "_Value"
        Me.cmbTipo.Items.Clear()
        Dim ws As New WS_DESTINOSUPDATE.destinosUpdate_service
        ws.Url = CapaAccesoDatos.XML.wsData.UrlWsUpdate
        Dim ds As DataSet = Nothing
        Try
            ds = ws.obten_habitaciones(datosBusqueda_hotel.PropertyNumber, CapaPresentacion.Idiomas.idIdioma)
        Catch ex As Exception

        End Try

        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Me.cmbTipo.Items.Add(New ListData(dr("Nombre"), dr("Codigo")))
            Next
        End If
        If cmbTipo.Items.Count > 0 Then
            cmbTipo.SelectedIndex = 0
        Else
            MsgBox(CapaPresentacion.Idiomas.get_str("str_461"))
            Close()
        End If

    End Sub

    Private Sub txtTax_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTax.TextChanged
        If Not IsNumeric(txtTax.Text) Then txtTax.Text = ""
    End Sub

    Private Sub txtTarifa_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTarifa.TextChanged
        If Not IsNumeric(txtTarifa.Text) Then txtTarifa.Text = ""
    End Sub
End Class