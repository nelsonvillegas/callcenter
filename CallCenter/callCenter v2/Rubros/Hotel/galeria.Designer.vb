<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class galeria
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.UltraPanel1 = New Infragistics.Win.Misc.UltraPanel
        Me.UltraFlowLayoutManager1 = New Infragistics.Win.Misc.UltraFlowLayoutManager(Me.components)
        Me.pic_GRANDE = New System.Windows.Forms.PictureBox
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.UltraPanel1.SuspendLayout()
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_GRANDE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraPanel1
        '
        Me.UltraPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.UltraPanel1.AutoScroll = True
        Me.UltraPanel1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraPanel1.Location = New System.Drawing.Point(12, 12)
        Me.UltraPanel1.Name = "UltraPanel1"
        Me.UltraPanel1.Size = New System.Drawing.Size(330, 357)
        Me.UltraPanel1.TabIndex = 0
        Me.UltraPanel1.Text = "UltraPanel1"
        '
        'UltraFlowLayoutManager1
        '
        Me.UltraFlowLayoutManager1.ContainerControl = Me.UltraPanel1.ClientArea
        '
        'pic_GRANDE
        '
        Me.pic_GRANDE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pic_GRANDE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_GRANDE.Location = New System.Drawing.Point(348, 12)
        Me.pic_GRANDE.Name = "pic_GRANDE"
        Me.pic_GRANDE.Size = New System.Drawing.Size(446, 357)
        Me.pic_GRANDE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pic_GRANDE.TabIndex = 1
        Me.pic_GRANDE.TabStop = False
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_aceptar.Location = New System.Drawing.Point(719, 375)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_aceptar.TabIndex = 2
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = False
        '
        'galeria
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_aceptar
        Me.ClientSize = New System.Drawing.Size(806, 410)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Controls.Add(Me.pic_GRANDE)
        Me.Controls.Add(Me.UltraPanel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "galeria"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "galeria"
        Me.UltraPanel1.ResumeLayout(False)
        CType(Me.UltraFlowLayoutManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_GRANDE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraPanel1 As Infragistics.Win.Misc.UltraPanel
    Friend WithEvents UltraFlowLayoutManager1 As Infragistics.Win.Misc.UltraFlowLayoutManager
    Friend WithEvents pic_GRANDE As System.Windows.Forms.PictureBox
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
End Class
