﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Habitaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_msg = New System.Windows.Forms.Label
        Me.lbl_msg2 = New System.Windows.Forms.Label
        Me.chk_habitacion_1 = New System.Windows.Forms.CheckBox
        Me.cmb_adt_1 = New System.Windows.Forms.ComboBox
        Me.cmb_nin_1 = New System.Windows.Forms.ComboBox
        Me.lbl_adultos = New System.Windows.Forms.Label
        Me.lbl_ninos = New System.Windows.Forms.Label
        Me.cmb_nin_2 = New System.Windows.Forms.ComboBox
        Me.cmb_adt_2 = New System.Windows.Forms.ComboBox
        Me.chk_habitacion_2 = New System.Windows.Forms.CheckBox
        Me.cmb_nin_3 = New System.Windows.Forms.ComboBox
        Me.cmb_adt_3 = New System.Windows.Forms.ComboBox
        Me.chk_habitacion_3 = New System.Windows.Forms.CheckBox
        Me.lbl_nino_3 = New System.Windows.Forms.Label
        Me.cmb_hab1_nin3 = New System.Windows.Forms.ComboBox
        Me.cmb_hab1_nin1 = New System.Windows.Forms.ComboBox
        Me.lbl_nino_1 = New System.Windows.Forms.Label
        Me.lbl_nino_2 = New System.Windows.Forms.Label
        Me.cmb_hab1_nin2 = New System.Windows.Forms.ComboBox
        Me.lbl_habitacion_1 = New System.Windows.Forms.Label
        Me.cmb_hab2_nin2 = New System.Windows.Forms.ComboBox
        Me.cmb_hab2_nin3 = New System.Windows.Forms.ComboBox
        Me.cmb_hab2_nin1 = New System.Windows.Forms.ComboBox
        Me.lbl_habitacion_2 = New System.Windows.Forms.Label
        Me.cmb_hab3_nin2 = New System.Windows.Forms.ComboBox
        Me.cmb_hab3_nin3 = New System.Windows.Forms.ComboBox
        Me.cmb_hab3_nin1 = New System.Windows.Forms.ComboBox
        Me.lbl_habitacion_3 = New System.Windows.Forms.Label
        Me.btn_Cancelar = New System.Windows.Forms.Button
        Me.btn_Aceptar = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lbl_msg
        '
        Me.lbl_msg.AutoSize = True
        Me.lbl_msg.Location = New System.Drawing.Point(12, 9)
        Me.lbl_msg.Name = "lbl_msg"
        Me.lbl_msg.Size = New System.Drawing.Size(185, 13)
        Me.lbl_msg.TabIndex = 0
        Me.lbl_msg.Text = "Especifique datos de las habitaciones"
        '
        'lbl_msg2
        '
        Me.lbl_msg2.AutoSize = True
        Me.lbl_msg2.Location = New System.Drawing.Point(12, 148)
        Me.lbl_msg2.Name = "lbl_msg2"
        Me.lbl_msg2.Size = New System.Drawing.Size(297, 13)
        Me.lbl_msg2.TabIndex = 0
        Me.lbl_msg2.Text = "Especifique las edades de los menores que vienen en el viaje"
        '
        'chk_habitacion_1
        '
        Me.chk_habitacion_1.AutoSize = True
        Me.chk_habitacion_1.Location = New System.Drawing.Point(39, 55)
        Me.chk_habitacion_1.Name = "chk_habitacion_1"
        Me.chk_habitacion_1.Size = New System.Drawing.Size(86, 17)
        Me.chk_habitacion_1.TabIndex = 1
        Me.chk_habitacion_1.Text = "Habitación 1"
        Me.chk_habitacion_1.UseVisualStyleBackColor = True
        '
        'cmb_adt_1
        '
        Me.cmb_adt_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_adt_1.Enabled = False
        Me.cmb_adt_1.FormattingEnabled = True
        Me.cmb_adt_1.Location = New System.Drawing.Point(174, 53)
        Me.cmb_adt_1.Name = "cmb_adt_1"
        Me.cmb_adt_1.Size = New System.Drawing.Size(39, 21)
        Me.cmb_adt_1.TabIndex = 2
        '
        'cmb_nin_1
        '
        Me.cmb_nin_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_nin_1.Enabled = False
        Me.cmb_nin_1.FormattingEnabled = True
        Me.cmb_nin_1.Location = New System.Drawing.Point(264, 53)
        Me.cmb_nin_1.Name = "cmb_nin_1"
        Me.cmb_nin_1.Size = New System.Drawing.Size(39, 21)
        Me.cmb_nin_1.TabIndex = 3
        '
        'lbl_adultos
        '
        Me.lbl_adultos.AutoSize = True
        Me.lbl_adultos.Location = New System.Drawing.Point(171, 28)
        Me.lbl_adultos.Name = "lbl_adultos"
        Me.lbl_adultos.Size = New System.Drawing.Size(42, 13)
        Me.lbl_adultos.TabIndex = 0
        Me.lbl_adultos.Text = "Adultos"
        '
        'lbl_ninos
        '
        Me.lbl_ninos.AutoSize = True
        Me.lbl_ninos.Location = New System.Drawing.Point(261, 28)
        Me.lbl_ninos.Name = "lbl_ninos"
        Me.lbl_ninos.Size = New System.Drawing.Size(34, 13)
        Me.lbl_ninos.TabIndex = 0
        Me.lbl_ninos.Text = "Niños"
        '
        'cmb_nin_2
        '
        Me.cmb_nin_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_nin_2.Enabled = False
        Me.cmb_nin_2.FormattingEnabled = True
        Me.cmb_nin_2.Location = New System.Drawing.Point(264, 80)
        Me.cmb_nin_2.Name = "cmb_nin_2"
        Me.cmb_nin_2.Size = New System.Drawing.Size(39, 21)
        Me.cmb_nin_2.TabIndex = 6
        '
        'cmb_adt_2
        '
        Me.cmb_adt_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_adt_2.Enabled = False
        Me.cmb_adt_2.FormattingEnabled = True
        Me.cmb_adt_2.Location = New System.Drawing.Point(174, 80)
        Me.cmb_adt_2.Name = "cmb_adt_2"
        Me.cmb_adt_2.Size = New System.Drawing.Size(39, 21)
        Me.cmb_adt_2.TabIndex = 5
        '
        'chk_habitacion_2
        '
        Me.chk_habitacion_2.AutoSize = True
        Me.chk_habitacion_2.Enabled = False
        Me.chk_habitacion_2.Location = New System.Drawing.Point(39, 82)
        Me.chk_habitacion_2.Name = "chk_habitacion_2"
        Me.chk_habitacion_2.Size = New System.Drawing.Size(86, 17)
        Me.chk_habitacion_2.TabIndex = 4
        Me.chk_habitacion_2.Text = "Habitación 2"
        Me.chk_habitacion_2.UseVisualStyleBackColor = True
        '
        'cmb_nin_3
        '
        Me.cmb_nin_3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_nin_3.Enabled = False
        Me.cmb_nin_3.FormattingEnabled = True
        Me.cmb_nin_3.Location = New System.Drawing.Point(264, 107)
        Me.cmb_nin_3.Name = "cmb_nin_3"
        Me.cmb_nin_3.Size = New System.Drawing.Size(39, 21)
        Me.cmb_nin_3.TabIndex = 9
        '
        'cmb_adt_3
        '
        Me.cmb_adt_3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_adt_3.Enabled = False
        Me.cmb_adt_3.FormattingEnabled = True
        Me.cmb_adt_3.Location = New System.Drawing.Point(174, 107)
        Me.cmb_adt_3.Name = "cmb_adt_3"
        Me.cmb_adt_3.Size = New System.Drawing.Size(39, 21)
        Me.cmb_adt_3.TabIndex = 8
        '
        'chk_habitacion_3
        '
        Me.chk_habitacion_3.AutoSize = True
        Me.chk_habitacion_3.Enabled = False
        Me.chk_habitacion_3.Location = New System.Drawing.Point(39, 109)
        Me.chk_habitacion_3.Name = "chk_habitacion_3"
        Me.chk_habitacion_3.Size = New System.Drawing.Size(86, 17)
        Me.chk_habitacion_3.TabIndex = 7
        Me.chk_habitacion_3.Text = "Habitación 3"
        Me.chk_habitacion_3.UseVisualStyleBackColor = True
        '
        'lbl_nino_3
        '
        Me.lbl_nino_3.AutoSize = True
        Me.lbl_nino_3.Location = New System.Drawing.Point(261, 175)
        Me.lbl_nino_3.Name = "lbl_nino_3"
        Me.lbl_nino_3.Size = New System.Drawing.Size(38, 13)
        Me.lbl_nino_3.TabIndex = 0
        Me.lbl_nino_3.Text = "Niño 3"
        '
        'cmb_hab1_nin3
        '
        Me.cmb_hab1_nin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab1_nin3.Enabled = False
        Me.cmb_hab1_nin3.FormattingEnabled = True
        Me.cmb_hab1_nin3.Location = New System.Drawing.Point(264, 200)
        Me.cmb_hab1_nin3.Name = "cmb_hab1_nin3"
        Me.cmb_hab1_nin3.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab1_nin3.TabIndex = 12
        '
        'cmb_hab1_nin1
        '
        Me.cmb_hab1_nin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab1_nin1.Enabled = False
        Me.cmb_hab1_nin1.FormattingEnabled = True
        Me.cmb_hab1_nin1.Location = New System.Drawing.Point(174, 200)
        Me.cmb_hab1_nin1.Name = "cmb_hab1_nin1"
        Me.cmb_hab1_nin1.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab1_nin1.TabIndex = 10
        '
        'lbl_nino_1
        '
        Me.lbl_nino_1.AutoSize = True
        Me.lbl_nino_1.Location = New System.Drawing.Point(171, 175)
        Me.lbl_nino_1.Name = "lbl_nino_1"
        Me.lbl_nino_1.Size = New System.Drawing.Size(38, 13)
        Me.lbl_nino_1.TabIndex = 0
        Me.lbl_nino_1.Text = "Niño 1"
        '
        'lbl_nino_2
        '
        Me.lbl_nino_2.AutoSize = True
        Me.lbl_nino_2.Location = New System.Drawing.Point(216, 175)
        Me.lbl_nino_2.Name = "lbl_nino_2"
        Me.lbl_nino_2.Size = New System.Drawing.Size(38, 13)
        Me.lbl_nino_2.TabIndex = 0
        Me.lbl_nino_2.Text = "Niño 2"
        '
        'cmb_hab1_nin2
        '
        Me.cmb_hab1_nin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab1_nin2.Enabled = False
        Me.cmb_hab1_nin2.FormattingEnabled = True
        Me.cmb_hab1_nin2.Location = New System.Drawing.Point(219, 200)
        Me.cmb_hab1_nin2.Name = "cmb_hab1_nin2"
        Me.cmb_hab1_nin2.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab1_nin2.TabIndex = 11
        '
        'lbl_habitacion_1
        '
        Me.lbl_habitacion_1.AutoSize = True
        Me.lbl_habitacion_1.Location = New System.Drawing.Point(36, 203)
        Me.lbl_habitacion_1.Name = "lbl_habitacion_1"
        Me.lbl_habitacion_1.Size = New System.Drawing.Size(67, 13)
        Me.lbl_habitacion_1.TabIndex = 0
        Me.lbl_habitacion_1.Text = "Habitación 1"
        '
        'cmb_hab2_nin2
        '
        Me.cmb_hab2_nin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab2_nin2.Enabled = False
        Me.cmb_hab2_nin2.FormattingEnabled = True
        Me.cmb_hab2_nin2.Location = New System.Drawing.Point(219, 227)
        Me.cmb_hab2_nin2.Name = "cmb_hab2_nin2"
        Me.cmb_hab2_nin2.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab2_nin2.TabIndex = 14
        '
        'cmb_hab2_nin3
        '
        Me.cmb_hab2_nin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab2_nin3.Enabled = False
        Me.cmb_hab2_nin3.FormattingEnabled = True
        Me.cmb_hab2_nin3.Location = New System.Drawing.Point(264, 227)
        Me.cmb_hab2_nin3.Name = "cmb_hab2_nin3"
        Me.cmb_hab2_nin3.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab2_nin3.TabIndex = 15
        '
        'cmb_hab2_nin1
        '
        Me.cmb_hab2_nin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab2_nin1.Enabled = False
        Me.cmb_hab2_nin1.FormattingEnabled = True
        Me.cmb_hab2_nin1.Location = New System.Drawing.Point(174, 227)
        Me.cmb_hab2_nin1.Name = "cmb_hab2_nin1"
        Me.cmb_hab2_nin1.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab2_nin1.TabIndex = 13
        '
        'lbl_habitacion_2
        '
        Me.lbl_habitacion_2.AutoSize = True
        Me.lbl_habitacion_2.Location = New System.Drawing.Point(36, 230)
        Me.lbl_habitacion_2.Name = "lbl_habitacion_2"
        Me.lbl_habitacion_2.Size = New System.Drawing.Size(67, 13)
        Me.lbl_habitacion_2.TabIndex = 0
        Me.lbl_habitacion_2.Text = "Habitación 2"
        '
        'cmb_hab3_nin2
        '
        Me.cmb_hab3_nin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab3_nin2.Enabled = False
        Me.cmb_hab3_nin2.FormattingEnabled = True
        Me.cmb_hab3_nin2.Location = New System.Drawing.Point(219, 254)
        Me.cmb_hab3_nin2.Name = "cmb_hab3_nin2"
        Me.cmb_hab3_nin2.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab3_nin2.TabIndex = 17
        '
        'cmb_hab3_nin3
        '
        Me.cmb_hab3_nin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab3_nin3.Enabled = False
        Me.cmb_hab3_nin3.FormattingEnabled = True
        Me.cmb_hab3_nin3.Location = New System.Drawing.Point(264, 254)
        Me.cmb_hab3_nin3.Name = "cmb_hab3_nin3"
        Me.cmb_hab3_nin3.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab3_nin3.TabIndex = 18
        '
        'cmb_hab3_nin1
        '
        Me.cmb_hab3_nin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_hab3_nin1.Enabled = False
        Me.cmb_hab3_nin1.FormattingEnabled = True
        Me.cmb_hab3_nin1.Location = New System.Drawing.Point(174, 254)
        Me.cmb_hab3_nin1.Name = "cmb_hab3_nin1"
        Me.cmb_hab3_nin1.Size = New System.Drawing.Size(39, 21)
        Me.cmb_hab3_nin1.TabIndex = 16
        '
        'lbl_habitacion_3
        '
        Me.lbl_habitacion_3.AutoSize = True
        Me.lbl_habitacion_3.Location = New System.Drawing.Point(36, 257)
        Me.lbl_habitacion_3.Name = "lbl_habitacion_3"
        Me.lbl_habitacion_3.Size = New System.Drawing.Size(67, 13)
        Me.lbl_habitacion_3.TabIndex = 0
        Me.lbl_habitacion_3.Text = "Habitación 2"
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Cancelar.Location = New System.Drawing.Point(228, 315)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cancelar.TabIndex = 19
        Me.btn_Cancelar.Text = "Cancelar"
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Location = New System.Drawing.Point(147, 315)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Aceptar.TabIndex = 20
        Me.btn_Aceptar.Text = "Aceptar"
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'frm_Habitaciones
        '
        Me.AcceptButton = Me.btn_Aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_Cancelar
        Me.ClientSize = New System.Drawing.Size(322, 350)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.btn_Cancelar)
        Me.Controls.Add(Me.cmb_hab3_nin2)
        Me.Controls.Add(Me.cmb_hab3_nin3)
        Me.Controls.Add(Me.cmb_hab3_nin1)
        Me.Controls.Add(Me.lbl_habitacion_3)
        Me.Controls.Add(Me.cmb_hab2_nin2)
        Me.Controls.Add(Me.cmb_hab2_nin3)
        Me.Controls.Add(Me.cmb_hab2_nin1)
        Me.Controls.Add(Me.lbl_habitacion_2)
        Me.Controls.Add(Me.lbl_nino_2)
        Me.Controls.Add(Me.cmb_hab1_nin2)
        Me.Controls.Add(Me.lbl_nino_1)
        Me.Controls.Add(Me.lbl_nino_3)
        Me.Controls.Add(Me.cmb_hab1_nin3)
        Me.Controls.Add(Me.cmb_hab1_nin1)
        Me.Controls.Add(Me.cmb_nin_3)
        Me.Controls.Add(Me.cmb_adt_3)
        Me.Controls.Add(Me.chk_habitacion_3)
        Me.Controls.Add(Me.cmb_nin_2)
        Me.Controls.Add(Me.cmb_adt_2)
        Me.Controls.Add(Me.chk_habitacion_2)
        Me.Controls.Add(Me.lbl_ninos)
        Me.Controls.Add(Me.lbl_adultos)
        Me.Controls.Add(Me.cmb_nin_1)
        Me.Controls.Add(Me.cmb_adt_1)
        Me.Controls.Add(Me.chk_habitacion_1)
        Me.Controls.Add(Me.lbl_msg2)
        Me.Controls.Add(Me.lbl_msg)
        Me.Controls.Add(Me.lbl_habitacion_1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_Habitaciones"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Habitaciones"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_msg As System.Windows.Forms.Label
    Friend WithEvents lbl_msg2 As System.Windows.Forms.Label
    Friend WithEvents chk_habitacion_1 As System.Windows.Forms.CheckBox
    Friend WithEvents cmb_adt_1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_nin_1 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_adultos As System.Windows.Forms.Label
    Friend WithEvents lbl_ninos As System.Windows.Forms.Label
    Friend WithEvents cmb_nin_2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_adt_2 As System.Windows.Forms.ComboBox
    Friend WithEvents chk_habitacion_2 As System.Windows.Forms.CheckBox
    Friend WithEvents cmb_nin_3 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_adt_3 As System.Windows.Forms.ComboBox
    Friend WithEvents chk_habitacion_3 As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_nino_3 As System.Windows.Forms.Label
    Friend WithEvents cmb_hab1_nin3 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_hab1_nin1 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_nino_1 As System.Windows.Forms.Label
    Friend WithEvents lbl_nino_2 As System.Windows.Forms.Label
    Friend WithEvents cmb_hab1_nin2 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_habitacion_1 As System.Windows.Forms.Label
    Friend WithEvents cmb_hab2_nin2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_hab2_nin3 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_hab2_nin1 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_habitacion_2 As System.Windows.Forms.Label
    Friend WithEvents cmb_hab3_nin2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_hab3_nin3 As System.Windows.Forms.ComboBox
    Friend WithEvents cmb_hab3_nin1 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_habitacion_3 As System.Windows.Forms.Label
    Friend WithEvents btn_Cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_Aceptar As System.Windows.Forms.Button
End Class
