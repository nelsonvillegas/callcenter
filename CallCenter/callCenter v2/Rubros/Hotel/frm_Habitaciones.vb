﻿Public Class frm_Habitaciones

    Private _HabitacionesSettings As New uc_habitaciones.T_HabitacionSettings
    Private Temp_Habitaciones As List(Of uc_habitaciones.T_Habitacion)

    Public Property HabitacionesSettings() As uc_habitaciones.T_HabitacionSettings
        Get
            Return _HabitacionesSettings
        End Get
        Set(ByVal value As uc_habitaciones.T_HabitacionSettings)
            _HabitacionesSettings = value
        End Set
    End Property
    Public Property TotalHabitaciones() As Integer
        Get
            If chk_habitacion_3.Checked Then Return 3
            If chk_habitacion_2.Checked Then Return 2
            If chk_habitacion_1.Checked Then Return 1

            Return 0
        End Get
        Set(ByVal value As Integer)
            If value >= 1 Then chk_habitacion_1.Checked = True
            If value >= 2 Then chk_habitacion_2.Checked = True
            If value >= 3 Then chk_habitacion_3.Checked = True
        End Set
    End Property
    Public Property TotalAdultos() As Integer
        Get
            Dim total As Integer = 0

            If chk_habitacion_1.Checked Then total += CInt(cmb_adt_1.Text)
            If chk_habitacion_2.Checked Then total += CInt(cmb_adt_2.Text)
            If chk_habitacion_3.Checked Then total += CInt(cmb_adt_3.Text)

            Return total
        End Get
        Set(ByVal value As Integer)
            Dim arr() As Integer = herramientas.get_arrCuartos(TotalHabitaciones, value)

            If arr.Length >= 1 Then cmb_adt_1.SelectedItem = arr(0)
            If arr.Length >= 2 Then cmb_adt_2.SelectedItem = arr(1)
            If arr.Length >= 3 Then cmb_adt_3.SelectedItem = arr(2)
        End Set
    End Property
    Public Property TotalNinos() As Integer
        Get
            Dim total As Integer = 0

            If chk_habitacion_1.Checked Then total += CInt(cmb_nin_1.Text)
            If chk_habitacion_2.Checked Then total += CInt(cmb_nin_2.Text)
            If chk_habitacion_3.Checked Then total += CInt(cmb_nin_3.Text)

            Return total
        End Get
        Set(ByVal value As Integer)
            Dim arr() As Integer = herramientas.get_arrCuartos(TotalHabitaciones, value)

            If arr.Length >= 1 Then cmb_nin_1.SelectedItem = arr(0)
            If arr.Length >= 2 Then cmb_nin_2.SelectedItem = arr(1)
            If arr.Length >= 3 Then cmb_nin_3.SelectedItem = arr(2)
        End Set
    End Property
    Public Property Habitaciones() As List(Of uc_habitaciones.T_Habitacion)
        Get
            Dim lista As New List(Of uc_habitaciones.T_Habitacion)

            If chk_habitacion_1.Checked Then
                Dim hab As New uc_habitaciones.T_Habitacion
                hab.NumAdultos = CInt(cmb_adt_1.Text)
                hab.NumNinos = CInt(cmb_nin_1.Text)
                If CInt(cmb_nin_1.Text) >= 1 Then hab.EdadesNinos.Add(CInt(cmb_hab1_nin1.Text))
                If CInt(cmb_nin_1.Text) >= 2 Then hab.EdadesNinos.Add(CInt(cmb_hab1_nin2.Text))
                If CInt(cmb_nin_1.Text) >= 3 Then hab.EdadesNinos.Add(CInt(cmb_hab1_nin3.Text))

                lista.Add(hab)
            End If

            If chk_habitacion_2.Checked Then
                Dim hab As New uc_habitaciones.T_Habitacion
                hab.NumAdultos = CInt(cmb_adt_2.Text)
                hab.NumNinos = CInt(cmb_nin_2.Text)
                If CInt(cmb_nin_2.Text) >= 1 Then hab.EdadesNinos.Add(CInt(cmb_hab2_nin1.Text))
                If CInt(cmb_nin_2.Text) >= 2 Then hab.EdadesNinos.Add(CInt(cmb_hab2_nin2.Text))
                If CInt(cmb_nin_2.Text) >= 3 Then hab.EdadesNinos.Add(CInt(cmb_hab2_nin3.Text))

                lista.Add(hab)
            End If

            If chk_habitacion_3.Checked Then
                Dim hab As New uc_habitaciones.T_Habitacion
                hab.NumAdultos = CInt(cmb_adt_3.Text)
                hab.NumNinos = CInt(cmb_nin_3.Text)
                If CInt(cmb_nin_3.Text) >= 1 Then hab.EdadesNinos.Add(CInt(cmb_hab3_nin1.Text))
                If CInt(cmb_nin_3.Text) >= 2 Then hab.EdadesNinos.Add(CInt(cmb_hab3_nin2.Text))
                If CInt(cmb_nin_3.Text) >= 3 Then hab.EdadesNinos.Add(CInt(cmb_hab3_nin3.Text))

                lista.Add(hab)
            End If

            Return lista
        End Get
        Set(ByVal value As List(Of uc_habitaciones.T_Habitacion))
            If value.Count >= 1 Then
                chk_habitacion_1.Checked = True
                cmb_adt_1.SelectedItem = value(0).NumAdultos
                cmb_nin_1.SelectedItem = value(0).NumNinos
                If CInt(cmb_nin_1.Text) >= 1 Then If value(0).EdadesNinos.Count >= 1 Then cmb_hab1_nin1.SelectedItem = value(0).EdadesNinos(0) Else cmb_hab1_nin1.SelectedItem = _HabitacionesSettings.Min_EdadNinos
                If CInt(cmb_nin_1.Text) >= 2 Then If value(0).EdadesNinos.Count >= 2 Then cmb_hab1_nin2.SelectedItem = value(0).EdadesNinos(1) Else cmb_hab1_nin2.SelectedItem = _HabitacionesSettings.Min_EdadNinos
                If CInt(cmb_nin_1.Text) >= 3 Then If value(0).EdadesNinos.Count >= 3 Then cmb_hab1_nin3.SelectedItem = value(0).EdadesNinos(2) Else cmb_hab1_nin2.SelectedItem = _HabitacionesSettings.Min_EdadNinos
            Else
                chk_habitacion_1.Checked = False
            End If

            If value.Count >= 2 Then
                chk_habitacion_2.Checked = True
                cmb_adt_2.SelectedItem = value(1).NumAdultos
                cmb_nin_2.SelectedItem = value(1).NumNinos
                If CInt(cmb_nin_2.Text) >= 1 Then If value(1).EdadesNinos.Count >= 1 Then cmb_hab2_nin1.SelectedItem = value(1).EdadesNinos(0) Else cmb_hab2_nin1.SelectedItem = _HabitacionesSettings.Min_EdadNinos
                If CInt(cmb_nin_2.Text) >= 2 Then If value(1).EdadesNinos.Count >= 2 Then cmb_hab2_nin2.SelectedItem = value(1).EdadesNinos(1) Else cmb_hab2_nin2.SelectedItem = _HabitacionesSettings.Min_EdadNinos
                If CInt(cmb_nin_2.Text) >= 3 Then If value(1).EdadesNinos.Count >= 3 Then cmb_hab2_nin3.SelectedItem = value(1).EdadesNinos(2) Else cmb_hab2_nin3.SelectedItem = _HabitacionesSettings.Min_EdadNinos
            Else
                chk_habitacion_2.Checked = False
            End If

            If value.Count >= 3 Then
                chk_habitacion_3.Checked = True
                cmb_adt_3.SelectedItem = value(2).NumAdultos
                cmb_nin_3.SelectedItem = value(2).NumNinos
                If CInt(cmb_nin_3.Text) >= 1 Then If value(2).EdadesNinos.Count >= 1 Then cmb_hab3_nin1.SelectedItem = value(2).EdadesNinos(0) Else cmb_hab3_nin1.SelectedItem = _HabitacionesSettings.Min_EdadNinos
                If CInt(cmb_nin_3.Text) >= 2 Then If value(2).EdadesNinos.Count >= 2 Then cmb_hab3_nin2.SelectedItem = value(2).EdadesNinos(1) Else cmb_hab3_nin2.SelectedItem = _HabitacionesSettings.Min_EdadNinos
                If CInt(cmb_nin_3.Text) >= 3 Then If value(2).EdadesNinos.Count >= 3 Then cmb_hab3_nin3.SelectedItem = value(2).EdadesNinos(2) Else cmb_hab3_nin3.SelectedItem = _HabitacionesSettings.Min_EdadNinos
            Else
                chk_habitacion_3.Checked = False
            End If
        End Set
    End Property

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Temp_Habitaciones = Habitaciones

        chk_habitacion_1.Focus()
        chk_habitacion_1.Checked = True
        'TODO: comentado para que no marque error
        'CapaPresentacion.Idiomas.cambiar_frmHabitaciones(Me)
    End Sub

    Public Sub llena_combos()
        llena_cmb(cmb_adt_1, _HabitacionesSettings.Min_AdultosHabitacion, _HabitacionesSettings.Max_AdultosHabitacion)
        llena_cmb(cmb_adt_2, _HabitacionesSettings.Min_AdultosHabitacion, _HabitacionesSettings.Max_AdultosHabitacion)
        llena_cmb(cmb_adt_3, _HabitacionesSettings.Min_AdultosHabitacion, _HabitacionesSettings.Max_AdultosHabitacion)
        llena_cmb(cmb_nin_1, _HabitacionesSettings.Min_NinosHasbitacion, _HabitacionesSettings.Max_NinosHabitacion)
        llena_cmb(cmb_nin_2, _HabitacionesSettings.Min_NinosHasbitacion, _HabitacionesSettings.Max_NinosHabitacion)
        llena_cmb(cmb_nin_3, _HabitacionesSettings.Min_NinosHasbitacion, _HabitacionesSettings.Max_NinosHabitacion)

        llena_cmb(cmb_hab1_nin1, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab1_nin2, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab1_nin3, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab2_nin1, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab2_nin2, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab2_nin3, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab3_nin1, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab3_nin2, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
        llena_cmb(cmb_hab3_nin3, _HabitacionesSettings.Min_EdadNinos, _HabitacionesSettings.Max_EdadNinos)
    End Sub

    Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_habitacion_3.CheckedChanged, chk_habitacion_2.CheckedChanged, chk_habitacion_1.CheckedChanged
        Dim chk As CheckBox = CType(sender, CheckBox)

        Select Case chk.Name
            Case "chk_habitacion_1"
                cmb_adt_1.Enabled = chk.Checked
                cmb_nin_1.Enabled = chk.Checked
            Case "chk_habitacion_2"
                cmb_adt_2.Enabled = chk.Checked
                cmb_nin_2.Enabled = chk.Checked
            Case "chk_habitacion_3"
                cmb_adt_3.Enabled = chk.Checked
                cmb_nin_3.Enabled = chk.Checked
        End Select

        chk_habitacion_1.Enabled = True
        chk_habitacion_2.Enabled = chk_habitacion_1.Checked
        chk_habitacion_3.Enabled = chk_habitacion_2.Checked

        If Not chk_habitacion_1.Checked Then chk_habitacion_2.Checked = False
        If Not chk_habitacion_2.Checked Then chk_habitacion_3.Checked = False

        If cmb_nin_1.Enabled AndAlso CInt(cmb_nin_1.Text) >= 1 Then cmb_hab1_nin1.Enabled = True Else cmb_hab1_nin1.Enabled = False
        If cmb_nin_1.Enabled AndAlso CInt(cmb_nin_1.Text) >= 2 Then cmb_hab1_nin2.Enabled = True Else cmb_hab1_nin2.Enabled = False
        If cmb_nin_1.Enabled AndAlso CInt(cmb_nin_1.Text) >= 3 Then cmb_hab1_nin3.Enabled = True Else cmb_hab1_nin3.Enabled = False
        ''
        If cmb_nin_2.Enabled AndAlso CInt(cmb_nin_2.Text) >= 1 Then cmb_hab2_nin1.Enabled = True Else cmb_hab2_nin1.Enabled = False
        If cmb_nin_2.Enabled AndAlso CInt(cmb_nin_2.Text) >= 2 Then cmb_hab2_nin2.Enabled = True Else cmb_hab2_nin2.Enabled = False
        If cmb_nin_2.Enabled AndAlso CInt(cmb_nin_2.Text) >= 3 Then cmb_hab2_nin3.Enabled = True Else cmb_hab2_nin3.Enabled = False
        ''
        If cmb_nin_3.Enabled AndAlso CInt(cmb_nin_3.Text) >= 1 Then cmb_hab3_nin1.Enabled = True Else cmb_hab3_nin1.Enabled = False
        If cmb_nin_3.Enabled AndAlso CInt(cmb_nin_3.Text) >= 2 Then cmb_hab3_nin2.Enabled = True Else cmb_hab3_nin2.Enabled = False
        If cmb_nin_3.Enabled AndAlso CInt(cmb_nin_3.Text) >= 3 Then cmb_hab3_nin3.Enabled = True Else cmb_hab3_nin3.Enabled = False

        If Not chk_habitacion_1.Checked Then chk_habitacion_1.Checked = True
    End Sub

    Private Sub llena_cmb(ByVal cmb As ComboBox, ByVal min As Integer, ByVal max As Integer)
        cmb.Items.Clear()
        For i As Integer = min To max
            cmb.Items.Add(i)
        Next

        If cmb.Items.Count > 0 Then cmb.SelectedIndex = 0
    End Sub

    Private Sub losCMB_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_nin_3.SelectedIndexChanged, cmb_nin_2.SelectedIndexChanged, cmb_nin_1.SelectedIndexChanged
        Dim cmb As ComboBox = sender

        If cmb.Text = "" Then Return

        Select Case cmb.Name
            Case "cmb_nin_1"
                If CInt(cmb.Text) >= 1 Then cmb_hab1_nin1.Enabled = True Else cmb_hab1_nin1.Enabled = False
                If CInt(cmb.Text) >= 2 Then cmb_hab1_nin2.Enabled = True Else cmb_hab1_nin2.Enabled = False
                If CInt(cmb.Text) >= 3 Then cmb_hab1_nin3.Enabled = True Else cmb_hab1_nin3.Enabled = False

            Case "cmb_nin_2"
                If CInt(cmb.Text) >= 1 Then cmb_hab2_nin1.Enabled = True Else cmb_hab2_nin1.Enabled = False
                If CInt(cmb.Text) >= 2 Then cmb_hab2_nin2.Enabled = True Else cmb_hab2_nin2.Enabled = False
                If CInt(cmb.Text) >= 3 Then cmb_hab2_nin3.Enabled = True Else cmb_hab2_nin3.Enabled = False

            Case "cmb_nin_3"
                If CInt(cmb.Text) >= 1 Then cmb_hab3_nin1.Enabled = True Else cmb_hab3_nin1.Enabled = False
                If CInt(cmb.Text) >= 2 Then cmb_hab3_nin2.Enabled = True Else cmb_hab3_nin2.Enabled = False
                If CInt(cmb.Text) >= 3 Then cmb_hab3_nin3.Enabled = True Else cmb_hab3_nin3.Enabled = False
        End Select
    End Sub

    Private Sub btn_Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Cancelar.Click
        Habitaciones = Temp_Habitaciones
        Close()
    End Sub

    Private Sub btn_Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Aceptar.Click
        Close()
    End Sub

End Class