Public Class reglas

    Public datosBusqueda_hotel As dataBusqueda_hotel_test
    Public Thread_Rules As System.Threading.Thread
    Public ds As DataSet

    Private Sub reglas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CapaPresentacion.Idiomas.cambiar_reglas(Me)
        CapaPresentacion.Hotels.HotelRules.inicializar_controles(Me)
    End Sub

    Private Sub btn_aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_aceptar.Click
        Close()
    End Sub

    Public Sub cargar(ByVal dsRules As DataSet)
        ds = dsRules

        Thread_Rules = New Threading.Thread(AddressOf load_data)
        Thread_Rules.Start()
    End Sub

    Private Sub load_data()
        Try
            CapaPresentacion.Common.Fija_CTLVISIBLE(PictureBox2, True)
            'Dim ds As DataSet = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)
            If ds Is Nothing Then ds = obtenGenerales_hoteles.obten_general_HOR(datosBusqueda_hotel)
            CapaPresentacion.Hotels.HotelRules.FormatBrowser(Me, ds)
            CapaPresentacion.Common.Fija_CTLVISIBLE(PictureBox2, False)
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0026", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Sub

End Class