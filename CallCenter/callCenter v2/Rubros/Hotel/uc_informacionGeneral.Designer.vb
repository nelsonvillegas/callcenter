<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_informacionGeneral
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.data_lbl_nombreHotel = New System.Windows.Forms.Label
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
        Me.SuspendLayout()
        '
        'data_lbl_nombreHotel
        '
        Me.data_lbl_nombreHotel.AutoSize = True
        Me.data_lbl_nombreHotel.Dock = System.Windows.Forms.DockStyle.Top
        Me.data_lbl_nombreHotel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.data_lbl_nombreHotel.Location = New System.Drawing.Point(0, 0)
        Me.data_lbl_nombreHotel.Name = "data_lbl_nombreHotel"
        Me.data_lbl_nombreHotel.Size = New System.Drawing.Size(96, 25)
        Me.data_lbl_nombreHotel.TabIndex = 1
        Me.data_lbl_nombreHotel.Text = "------------"
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser1.Location = New System.Drawing.Point(0, 25)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(354, 403)
        Me.WebBrowser1.TabIndex = 3
        '
        'uc_informacionGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.data_lbl_nombreHotel)
        Me.Name = "uc_informacionGeneral"
        Me.Size = New System.Drawing.Size(354, 428)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents data_lbl_nombreHotel As System.Windows.Forms.Label
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser

End Class
