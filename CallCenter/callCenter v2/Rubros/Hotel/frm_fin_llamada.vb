﻿Public Class frm_fin_llamada
    Dim rb As String
    Public result As String
    Public hotelName As String = ""

    Private Sub rb_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rb1.CheckedChanged, rb5.CheckedChanged, rb4.CheckedChanged, rb3.CheckedChanged, rb2.CheckedChanged
        btnOk.Enabled = True
        Dim rbtn As RadioButton = DirectCast(sender, RadioButton)
        If rbtn.Checked Then
            rb = rbtn.Text
            If rb.StartsWith("5") Then
                txtObservaciones.Visible = True
                txtObservaciones.Top = GroupBox1.Top + GroupBox1.Height + 20
                btnOk.Top = txtObservaciones.Top + txtObservaciones.Height + 10

            Else
                txtObservaciones.Visible = False
                'txtObservaciones.Top = GroupBox1.Top + GroupBox1.Height + 20
                btnOk.Top = GroupBox1.Top + GroupBox1.Height + 20

            End If
            Me.Height = btnOk.Top + btnOk.Height + 35
        End If
    End Sub

    Private Sub btnOk_Click(sender As System.Object, e As System.EventArgs) Handles btnOk.Click
        If rb.StartsWith("5") Then
            result = txtObservaciones.Text
        Else
            result = rb.Substring(3)
            'cmbHoteles.SelectionStart = 1
            If cmbHoteles.Items.Count > 0 Then hotelName = cmbHoteles.SelectedItem.ToString()
        End If
        DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub frm_fin_llamada_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If dataOperador.idCorporativo > "0" AndAlso dataOperador.Hoteles.Count > 0 Then
            For Each dr As dataHotel In dataOperador.Hoteles
                cmbHoteles.Items.Add(dr.Nombre)
            Next
        End If
        If cmbHoteles.Items.Count > 0 AndAlso dataOperador.idCorporativo > "0" Then
            cmbHoteles.SelectedIndex = 0
        End If
    End Sub
End Class