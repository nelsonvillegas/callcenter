﻿Public Class uc_habitaciones

#Region "CLASES"
    Public Class T_Habitaciones
        Public ListaHabitaciones As New List(Of uc_habitaciones.T_Habitacion)
        Public _HabitacionesSettings As New uc_habitaciones.T_HabitacionSettings

        Public Property TotalHabitaciones() As Integer
            Get
                Return ListaHabitaciones.Count
            End Get
            Set(ByVal value As Integer)
                If value <> TotalHabitaciones Then
                    Dim SeModifico As Boolean = False

                    While ListaHabitaciones.Count > value
                        ListaHabitaciones.RemoveAt(ListaHabitaciones.Count - 1)

                        If Not SeModifico Then SeModifico = True
                    End While

                    While ListaHabitaciones.Count < value
                        Dim hab As New uc_habitaciones.T_Habitacion
                        hab.NumAdultos = _HabitacionesSettings.Min_AdultosHabitacion
                        hab.NumNinos = _HabitacionesSettings.Min_NinosHasbitacion
                        For i As Integer = 0 To hab.NumNinos - 1
                            hab.EdadesNinos.Add(_HabitacionesSettings.Min_EdadNinos)
                        Next

                        ListaHabitaciones.Add(hab)

                        If Not SeModifico Then SeModifico = True
                    End While

                    If SeModifico Then RaiseEvent TotalHabitacionesChanged()
                End If
            End Set
        End Property
        Public Property TotalAdultos() As Integer
            Get
                Dim total As Integer = 0

                For Each hab As T_Habitacion In ListaHabitaciones
                    total += hab.NumAdultos
                Next

                Return total
            End Get
            Set(ByVal value As Integer)
                If value <> TotalAdultos Then
                    Dim SeModifico As Boolean = False

                    Dim arr() As Integer = herramientas.get_arrCuartos(TotalHabitaciones, value)
                    For i As Integer = 0 To arr.Length - 1
                        ListaHabitaciones(i).NumAdultos = arr(i)

                        If Not SeModifico Then SeModifico = True
                    Next

                    If SeModifico Then RaiseEvent TotalAdultosChanged()
                End If
            End Set
        End Property
        Public Property TotalNinos() As Integer
            Get
                Dim total As Integer = 0

                For Each hab As T_Habitacion In ListaHabitaciones
                    total += hab.NumNinos
                Next

                Return total
            End Get
            Set(ByVal value As Integer)
                'Dim arr() As Integer = herramientas.get_arrCuartos(TotalHabitaciones, value)
                'If arr.Length >= 1 Then cmb_nin_1.SelectedItem = arr(0)
                'If arr.Length >= 2 Then cmb_nin_2.SelectedItem = arr(1)
                'If arr.Length >= 3 Then cmb_nin_3.SelectedItem = arr(2)

                If value <> TotalNinos Then
                    Dim SeModifico As Boolean = False

                    Dim arr() As Integer = herramientas.get_arrCuartos(TotalHabitaciones, value)
                    For i As Integer = 0 To arr.Length - 1
                        ListaHabitaciones(i).NumNinos = arr(i)

                        ListaHabitaciones(i).EdadesNinos.Clear()
                        For cont As Integer = 0 To ListaHabitaciones(i).NumNinos - 1
                            ListaHabitaciones(i).EdadesNinos.Add(_HabitacionesSettings.Min_EdadNinos)
                        Next

                        If Not SeModifico Then SeModifico = True
                    Next

                    If SeModifico Then RaiseEvent TotalNinosChanged()
                End If
            End Set
        End Property

        Public Property EdadesNinos() As String
            Get
                Dim edades As String = String.Empty
                For Each hab As T_Habitacion In ListaHabitaciones
                    edades += "," + hab.EdadesNinosSTR
                Next
                Return edades.Substring(1, edades.Length - 1)
            End Get
            Set(value As String)

            End Set
        End Property

        Public Function copy() As T_Habitaciones
            Dim temp As New T_Habitaciones

            For Each hab As T_Habitacion In ListaHabitaciones
                Dim temp_hab As T_Habitacion = hab.copy

                temp.ListaHabitaciones.Add(temp_hab)
            Next

            Return temp
        End Function

        Public Event TotalHabitacionesChanged()
        Public Event TotalAdultosChanged()
        Public Event TotalNinosChanged()
    End Class

    Public Class T_Habitacion
        Implements System.Runtime.Serialization.ISerializable

        Public NumAdultos As Integer
        Public NumNinos As Integer
        Public EdadesNinos As New List(Of Integer)

        Public ReadOnly Property EdadesNinosSTR() As String
            Get
                Dim str As String = ""

                For Each edad As Integer In EdadesNinos
                    If str = "" Then str += CStr(edad) Else str += "," + CStr(edad)
                Next

                Return str
            End Get
        End Property

        Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        End Sub

        Public Function copy() As T_Habitacion
            Dim temp As New T_Habitacion

            temp.NumAdultos = NumAdultos
            temp.NumNinos = NumNinos
            For Each edad As Integer In EdadesNinos
                temp.EdadesNinos.Add(edad.ToString)
            Next

            Return temp
        End Function
    End Class

    Public Class T_HabitacionSettings
        Implements System.Runtime.Serialization.ISerializable

        Public Max_AdultosHabitacion As Integer = 14
        Public Max_NinosHabitacion As Integer = 3
        Public Max_EdadNinos As Integer = 18
        Public Max_Habitaciones As Integer = 20

        Public Min_Habitaciones As Integer = 1
        'Public Min_AdultosTotal As Integer = 1
        'Public Min_NinosTotal As Integer = 0
        Public Min_AdultosHabitacion As Integer = 1
        Public Min_NinosHasbitacion As Integer = 0
        Public Min_EdadNinos As Integer = 1

        Public Def_Habitaciones As Integer = 1
        Public Def_Adultos As Integer = 1
        Public Def_Ninos As Integer = 0

        Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        End Sub
    End Class
#End Region


#Region "PROPIEDADES"
    Private WithEvents _Habitaciones As New T_Habitaciones
    Public _HabitacionesSettings As New uc_habitaciones.T_HabitacionSettings

    Public Property Habitaciones() As T_Habitaciones
        Get
            Return _Habitaciones.copy
        End Get
        Set(ByVal value As T_Habitaciones)
            _Habitaciones = value
            txt_habitaciones.Text = _Habitaciones.TotalHabitaciones
            txt_adultos.Text = _Habitaciones.TotalAdultos
            txt_ninos.Text = _Habitaciones.TotalNinos
            If SeLeAsignoHabitaciones Then HisoLoad = True
            SeLeAsignoHabitaciones = True
        End Set
    End Property

    Public ReadOnly Property prop_txt_adultos() As Infragistics.Win.UltraWinEditors.UltraTextEditor
        Get
            Return txt_adultos
        End Get
    End Property
    Public ReadOnly Property prop_txt_habitaciones() As Infragistics.Win.UltraWinEditors.UltraTextEditor
        Get
            Return txt_habitaciones
        End Get
    End Property
    Public ReadOnly Property prop_txt_ninos() As Infragistics.Win.UltraWinEditors.UltraTextEditor
        Get
            Return txt_ninos
        End Get
    End Property

    Private SeLeAsignoHabitaciones As Boolean = False
    Private HisoLoad As Boolean = False
#End Region


#Region "METODOS"
    Private Sub losTXT_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_ninos.Leave, txt_habitaciones.Leave, txt_adultos.Leave
        herramientas.formatea_campoNumerico(sender)
            ValidaCampos()
            'If CInt(txt_habitaciones.Text) = _Habitaciones.Count Then
            '    'si las habitaciones permanecen igual, no ahce nada para habitaciones
            'Else
            '    If CInt(txt_habitaciones.Text) < _Habitaciones.Count Then
            '    Else
            '        '
            '    End If
            'End If

            'pasar esto de abajo a cada propiedad correspondiente de "Public Class T_Habitaciones"
            'o si no se puede ver la forma de que los txt se actualicen cuando por programacion
            'se modifiquen esas propiedades o que pex?
            'todo este es por que si se le da BUSCAR sin presionar ESPECIFICAR, no genera las habitaciones

            _Habitaciones.TotalHabitaciones = CInt(txt_habitaciones.Text)
            _Habitaciones.TotalAdultos = CInt(txt_adultos.Text)
            _Habitaciones.TotalNinos = CInt(txt_ninos.Text)

            txt_habitaciones.Text = _Habitaciones.TotalHabitaciones
            txt_adultos.Text = _Habitaciones.TotalAdultos
            txt_ninos.Text = _Habitaciones.TotalNinos
    End Sub

    Private Sub _Habitaciones_TotalHabitacionesChanged() Handles _Habitaciones.TotalHabitacionesChanged
        If CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.modificar Then
            ValidaCampos()
        End If

    End Sub

    Private Sub _Habitaciones_TotalAdultosChanged() Handles _Habitaciones.TotalAdultosChanged
        If CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.modificar Then
            ValidaCampos()
        End If
    End Sub

    Private Sub _Habitaciones_TotalNinosChanged() Handles _Habitaciones.TotalNinosChanged
        If CapaConfiguracion.Instance.tipoAccion = enumReservar_TipoAccion.modificar Then
            ValidaCampos()
        End If
    End Sub

    Public Sub ValidaCampos()
        If txt_habitaciones.Text = "" Then txt_habitaciones.Text = _HabitacionesSettings.Def_Habitaciones
        If txt_adultos.Text = "" Then txt_adultos.Text = _HabitacionesSettings.Def_Adultos
        If txt_ninos.Text = "" Then txt_ninos.Text = _HabitacionesSettings.Def_Ninos

        If CInt(txt_habitaciones.Text) > _HabitacionesSettings.Max_Habitaciones Then txt_habitaciones.Text = _HabitacionesSettings.Max_Habitaciones
        If CInt(txt_habitaciones.Text) <> 0 AndAlso CInt(txt_adultos.Text) > CInt(txt_habitaciones.Text) * _HabitacionesSettings.Max_AdultosHabitacion Then txt_adultos.Text = CStr(CInt(txt_habitaciones.Text) * _HabitacionesSettings.Max_AdultosHabitacion)
        If CInt(txt_ninos.Text) > CInt(txt_habitaciones.Text) * _HabitacionesSettings.Max_NinosHabitacion Then txt_ninos.Text = CStr(CInt(txt_habitaciones.Text) * _HabitacionesSettings.Max_NinosHabitacion)

        If CInt(txt_habitaciones.Text) < _HabitacionesSettings.Min_Habitaciones Then txt_habitaciones.Text = _HabitacionesSettings.Min_Habitaciones
        If CInt(txt_adultos.Text) < _HabitacionesSettings.Min_AdultosHabitacion * _Habitaciones.TotalHabitaciones Then txt_adultos.Text = _HabitacionesSettings.Min_AdultosHabitacion * _Habitaciones.TotalHabitaciones
        If CInt(txt_ninos.Text) < _HabitacionesSettings.Min_NinosHasbitacion * _Habitaciones.TotalHabitaciones Then txt_ninos.Text = _HabitacionesSettings.Min_NinosHasbitacion * _Habitaciones.TotalHabitaciones
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub uc_habitaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not SeLeAsignoHabitaciones OrElse Not HisoLoad Then
            txt_habitaciones.Text = _HabitacionesSettings.Def_Habitaciones
            txt_adultos.Text = _HabitacionesSettings.Def_Adultos
            txt_ninos.Text = _HabitacionesSettings.Def_Ninos

            _Habitaciones.TotalHabitaciones = CInt(txt_habitaciones.Text)
            _Habitaciones.TotalAdultos = CInt(txt_adultos.Text)
            _Habitaciones.TotalNinos = CInt(txt_ninos.Text)
        Else
            txt_habitaciones.Text = _Habitaciones.TotalHabitaciones
            txt_adultos.Text = _Habitaciones.TotalAdultos
            txt_ninos.Text = _Habitaciones.TotalNinos
        End If

        HisoLoad = True
    End Sub

    Private Sub btn_especificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_especificar.Click
        Dim frm As New frm_Habitaciones_dinamico
        'frm.llena_combos()


        If _Habitaciones.ListaHabitaciones.Count = 0 Then
            frm.CreaControles(CInt(txt_adultos.Text), CInt(txt_ninos.Text), CInt(txt_habitaciones.Text))
            'frm.TotalHabitaciones = txt_habitaciones.Text
            'frm.TotalAdultos = txt_adultos.Text
            'frm.TotalNinos = txt_ninos.Text
        Else
            frm.CreaControles(CInt(txt_adultos.Text), CInt(txt_ninos.Text))
            frm.Habitaciones = _Habitaciones.ListaHabitaciones
        End If

        frm.ShowDialog()

        txt_habitaciones.Text = frm.TotalHabitaciones
        txt_adultos.Text = frm.TotalAdultos
        txt_ninos.Text = frm.TotalNinos
        _Habitaciones.ListaHabitaciones = frm.Habitaciones

        RaiseEvent BotonEspecificarClicked()
    End Sub

    Public Event BotonEspecificarClicked()

#End Region

End Class
