<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class busquedaAutos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(busquedaAutos))
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lbl_moneda = New System.Windows.Forms.Label
        Me.uce_moneda = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lbl_compania = New System.Windows.Forms.Label
        Me.uce_compania = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.cs_ciudadDevolver = New callCenter.ComboSearch
        Me.lbl_ciudadDevolver = New System.Windows.Forms.Label
        Me.lbl_tipo = New System.Windows.Forms.Label
        Me.uce_tipo = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.uce_horaDevolver = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.uce_horaRecoger = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.dtp_fechaDevolver = New System.Windows.Forms.DateTimePicker
        Me.lbl_devolver = New System.Windows.Forms.Label
        Me.cs_ciudadRecoger = New callCenter.ComboSearch
        Me.lbl_ciudad = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.dtp_fechaRecoger = New System.Windows.Forms.DateTimePicker
        Me.lbl_clase = New System.Windows.Forms.Label
        Me.lbl_recoger = New System.Windows.Forms.Label
        Me.uce_clase = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.btn_buscar = New System.Windows.Forms.Button
        Me.data_lbl_auto = New System.Windows.Forms.Label
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_compania, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_tipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_horaDevolver, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_horaRecoger, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_clase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraGrid1
        '
        Me.UltraGrid1.ContextMenuStrip = Me.ContextMenuStrip1
        Appearance30.BackColor = System.Drawing.Color.White
        Appearance30.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance30
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance31.ImageBackground = CType(resources.GetObject("Appearance31.ImageBackground"), System.Drawing.Image)
        Appearance31.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance31.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance31.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance31
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance28.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance28
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance29.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance29.FontData.Name = "Trebuchet MS"
        Appearance29.FontData.SizeInPoints = 9.0!
        Appearance29.ForeColor = System.Drawing.Color.White
        Appearance29.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance29
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance32.FontData.BoldAsString = "True"
        Appearance32.FontData.Name = "Trebuchet MS"
        Appearance32.FontData.SizeInPoints = 10.0!
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance32
        Appearance33.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance33
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance34.BackColor2 = System.Drawing.SystemColors.Control
        Appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance34.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance34
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance13.BackColor = System.Drawing.SystemColors.Window
        Appearance13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance13
        Appearance14.BackColor = System.Drawing.SystemColors.Highlight
        Appearance14.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance14
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance15.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance15
        Appearance16.BorderColor = System.Drawing.Color.Transparent
        Appearance16.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance16
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance17
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance21.BackColor = System.Drawing.SystemColors.Control
        Appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance21.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance21
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance22.FontData.BoldAsString = "True"
        Appearance22.FontData.Name = "Trebuchet MS"
        Appearance22.FontData.SizeInPoints = 10.0!
        Appearance22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance22.ImageBackground = CType(resources.GetObject("Appearance22.ImageBackground"), System.Drawing.Image)
        Appearance22.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance22.TextHAlignAsString = "Left"
        Appearance22.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance22
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance1
        Appearance23.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance24
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance25.BorderColor = System.Drawing.Color.Transparent
        Appearance25.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance25
        Appearance26.BorderColor = System.Drawing.Color.Transparent
        Appearance26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance26.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance26
        Appearance27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance27
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance35.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance37.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance38
        Appearance39.ImageBackground = CType(resources.GetObject("Appearance39.ImageBackground"), System.Drawing.Image)
        Appearance39.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance39.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance39
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 110)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(1081, 415)
        Me.UltraGrid1.TabIndex = 6
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.data_lbl_auto)
        Me.Panel1.Controls.Add(Me.lbl_moneda)
        Me.Panel1.Controls.Add(Me.uce_moneda)
        Me.Panel1.Controls.Add(Me.lbl_compania)
        Me.Panel1.Controls.Add(Me.uce_compania)
        Me.Panel1.Controls.Add(Me.cs_ciudadDevolver)
        Me.Panel1.Controls.Add(Me.lbl_ciudadDevolver)
        Me.Panel1.Controls.Add(Me.lbl_tipo)
        Me.Panel1.Controls.Add(Me.uce_tipo)
        Me.Panel1.Controls.Add(Me.uce_horaDevolver)
        Me.Panel1.Controls.Add(Me.uce_horaRecoger)
        Me.Panel1.Controls.Add(Me.dtp_fechaDevolver)
        Me.Panel1.Controls.Add(Me.lbl_devolver)
        Me.Panel1.Controls.Add(Me.cs_ciudadRecoger)
        Me.Panel1.Controls.Add(Me.lbl_ciudad)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.dtp_fechaRecoger)
        Me.Panel1.Controls.Add(Me.lbl_clase)
        Me.Panel1.Controls.Add(Me.lbl_recoger)
        Me.Panel1.Controls.Add(Me.uce_clase)
        Me.Panel1.Controls.Add(Me.btn_buscar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1081, 110)
        Me.Panel1.TabIndex = 5
        '
        'lbl_moneda
        '
        Me.lbl_moneda.AutoSize = True
        Me.lbl_moneda.Location = New System.Drawing.Point(565, 39)
        Me.lbl_moneda.Name = "lbl_moneda"
        Me.lbl_moneda.Size = New System.Drawing.Size(49, 13)
        Me.lbl_moneda.TabIndex = 0
        Me.lbl_moneda.Text = "Moneda:"
        '
        'uce_moneda
        '
        Me.uce_moneda.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_moneda.Location = New System.Drawing.Point(688, 35)
        Me.uce_moneda.Name = "uce_moneda"
        Me.uce_moneda.Size = New System.Drawing.Size(150, 21)
        Me.uce_moneda.TabIndex = 10
        '
        'lbl_compania
        '
        Me.lbl_compania.AutoSize = True
        Me.lbl_compania.Location = New System.Drawing.Point(565, 13)
        Me.lbl_compania.Name = "lbl_compania"
        Me.lbl_compania.Size = New System.Drawing.Size(117, 13)
        Me.lbl_compania.TabIndex = 0
        Me.lbl_compania.Text = "Compa�ia arrendadora:"
        '
        'uce_compania
        '
        Me.uce_compania.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_compania.Location = New System.Drawing.Point(688, 9)
        Me.uce_compania.Name = "uce_compania"
        Me.uce_compania.Size = New System.Drawing.Size(150, 21)
        Me.uce_compania.TabIndex = 9
        '
        'cs_ciudadDevolver
        '
        Me.cs_ciudadDevolver.BackColor = System.Drawing.Color.Transparent
        Me.cs_ciudadDevolver.Location = New System.Drawing.Point(373, 63)
        Me.cs_ciudadDevolver.Name = "cs_ciudadDevolver"
        Me.cs_ciudadDevolver.Size = New System.Drawing.Size(150, 21)
        Me.cs_ciudadDevolver.TabIndex = 8
        '
        'lbl_ciudadDevolver
        '
        Me.lbl_ciudadDevolver.AutoSize = True
        Me.lbl_ciudadDevolver.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudadDevolver.Location = New System.Drawing.Point(280, 66)
        Me.lbl_ciudadDevolver.Name = "lbl_ciudadDevolver"
        Me.lbl_ciudadDevolver.Size = New System.Drawing.Size(87, 13)
        Me.lbl_ciudadDevolver.TabIndex = 0
        Me.lbl_ciudadDevolver.Text = "Ciudad devolver:"
        '
        'lbl_tipo
        '
        Me.lbl_tipo.AutoSize = True
        Me.lbl_tipo.Location = New System.Drawing.Point(280, 39)
        Me.lbl_tipo.Name = "lbl_tipo"
        Me.lbl_tipo.Size = New System.Drawing.Size(31, 13)
        Me.lbl_tipo.TabIndex = 0
        Me.lbl_tipo.Text = "Tipo:"
        '
        'uce_tipo
        '
        Me.uce_tipo.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_tipo.Location = New System.Drawing.Point(373, 36)
        Me.uce_tipo.Name = "uce_tipo"
        Me.uce_tipo.Size = New System.Drawing.Size(150, 21)
        Me.uce_tipo.TabIndex = 7
        '
        'uce_horaDevolver
        '
        Me.uce_horaDevolver.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_horaDevolver.Location = New System.Drawing.Point(196, 36)
        Me.uce_horaDevolver.Name = "uce_horaDevolver"
        Me.uce_horaDevolver.Size = New System.Drawing.Size(57, 21)
        Me.uce_horaDevolver.TabIndex = 4
        '
        'uce_horaRecoger
        '
        Me.uce_horaRecoger.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_horaRecoger.Location = New System.Drawing.Point(196, 10)
        Me.uce_horaRecoger.Name = "uce_horaRecoger"
        Me.uce_horaRecoger.Size = New System.Drawing.Size(57, 21)
        Me.uce_horaRecoger.TabIndex = 2
        '
        'dtp_fechaDevolver
        '
        Me.dtp_fechaDevolver.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_fechaDevolver.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_fechaDevolver.Location = New System.Drawing.Point(103, 37)
        Me.dtp_fechaDevolver.Name = "dtp_fechaDevolver"
        Me.dtp_fechaDevolver.Size = New System.Drawing.Size(87, 20)
        Me.dtp_fechaDevolver.TabIndex = 3
        '
        'lbl_devolver
        '
        Me.lbl_devolver.AutoSize = True
        Me.lbl_devolver.BackColor = System.Drawing.Color.Transparent
        Me.lbl_devolver.Location = New System.Drawing.Point(15, 40)
        Me.lbl_devolver.Name = "lbl_devolver"
        Me.lbl_devolver.Size = New System.Drawing.Size(53, 13)
        Me.lbl_devolver.TabIndex = 0
        Me.lbl_devolver.Text = "Devolver:"
        '
        'cs_ciudadRecoger
        '
        Me.cs_ciudadRecoger.BackColor = System.Drawing.Color.Transparent
        Me.cs_ciudadRecoger.Location = New System.Drawing.Point(103, 63)
        Me.cs_ciudadRecoger.Name = "cs_ciudadRecoger"
        Me.cs_ciudadRecoger.Size = New System.Drawing.Size(150, 21)
        Me.cs_ciudadRecoger.TabIndex = 5
        '
        'lbl_ciudad
        '
        Me.lbl_ciudad.AutoSize = True
        Me.lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudad.Location = New System.Drawing.Point(15, 66)
        Me.lbl_ciudad.Name = "lbl_ciudad"
        Me.lbl_ciudad.Size = New System.Drawing.Size(82, 13)
        Me.lbl_ciudad.TabIndex = 0
        Me.lbl_ciudad.Text = "Ciudad recoger:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(688, 72)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 11
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'dtp_fechaRecoger
        '
        Me.dtp_fechaRecoger.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_fechaRecoger.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_fechaRecoger.Location = New System.Drawing.Point(103, 11)
        Me.dtp_fechaRecoger.Name = "dtp_fechaRecoger"
        Me.dtp_fechaRecoger.Size = New System.Drawing.Size(87, 20)
        Me.dtp_fechaRecoger.TabIndex = 1
        '
        'lbl_clase
        '
        Me.lbl_clase.AutoSize = True
        Me.lbl_clase.Location = New System.Drawing.Point(280, 13)
        Me.lbl_clase.Name = "lbl_clase"
        Me.lbl_clase.Size = New System.Drawing.Size(36, 13)
        Me.lbl_clase.TabIndex = 0
        Me.lbl_clase.Text = "Clase:"
        '
        'lbl_recoger
        '
        Me.lbl_recoger.AutoSize = True
        Me.lbl_recoger.BackColor = System.Drawing.Color.Transparent
        Me.lbl_recoger.Location = New System.Drawing.Point(15, 14)
        Me.lbl_recoger.Name = "lbl_recoger"
        Me.lbl_recoger.Size = New System.Drawing.Size(51, 13)
        Me.lbl_recoger.TabIndex = 0
        Me.lbl_recoger.Text = "Recoger:"
        '
        'uce_clase
        '
        Me.uce_clase.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_clase.Location = New System.Drawing.Point(373, 10)
        Me.uce_clase.Name = "uce_clase"
        Me.uce_clase.Size = New System.Drawing.Size(150, 21)
        Me.uce_clase.TabIndex = 6
        '
        'btn_buscar
        '
        Me.btn_buscar.Image = CType(resources.GetObject("btn_buscar.Image"), System.Drawing.Image)
        Me.btn_buscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_buscar.Location = New System.Drawing.Point(905, 10)
        Me.btn_buscar.Name = "btn_buscar"
        Me.btn_buscar.Size = New System.Drawing.Size(75, 56)
        Me.btn_buscar.TabIndex = 11
        Me.btn_buscar.Text = "Buscar"
        Me.btn_buscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_buscar.UseVisualStyleBackColor = True
        '
        'data_lbl_auto
        '
        Me.data_lbl_auto.AutoSize = True
        Me.data_lbl_auto.Location = New System.Drawing.Point(902, 69)
        Me.data_lbl_auto.Name = "data_lbl_auto"
        Me.data_lbl_auto.Size = New System.Drawing.Size(22, 13)
        Me.data_lbl_auto.TabIndex = 12
        Me.data_lbl_auto.Text = "-----"
        '
        'busquedaAutos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1081, 525)
        Me.Controls.Add(Me.UltraGrid1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "busquedaAutos"
        Me.Text = "Resultados de la b�squeda: Autos"
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.uce_moneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_compania, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_tipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_horaDevolver, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_horaRecoger, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_clase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dtp_fechaDevolver As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_devolver As System.Windows.Forms.Label
    Friend WithEvents cs_ciudadRecoger As callCenter.ComboSearch
    Friend WithEvents lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents dtp_fechaRecoger As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_clase As System.Windows.Forms.Label
    Friend WithEvents lbl_recoger As System.Windows.Forms.Label
    Friend WithEvents uce_clase As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents btn_buscar As System.Windows.Forms.Button
    Friend WithEvents uce_horaDevolver As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents uce_horaRecoger As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_moneda As System.Windows.Forms.Label
    Friend WithEvents uce_moneda As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_compania As System.Windows.Forms.Label
    Friend WithEvents uce_compania As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents cs_ciudadDevolver As callCenter.ComboSearch
    Friend WithEvents lbl_ciudadDevolver As System.Windows.Forms.Label
    Friend WithEvents lbl_tipo As System.Windows.Forms.Label
    Friend WithEvents uce_tipo As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents data_lbl_auto As System.Windows.Forms.Label
End Class
