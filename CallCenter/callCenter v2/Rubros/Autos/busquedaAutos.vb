Public Class busquedaAutos

    Public botones() As String
    Private cont As Integer = 0
    'Public datos_call As dataCall
    Public datosBusqueda_auto As New dataBusqueda_auto_test
    Public MI_Thread As Threading.Thread

    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
    End Sub

    Private Sub btn_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_buscar.Click
        Try
            If (dtp_fechaDevolver.Value - dtp_fechaRecoger.Value).TotalDays > 60 Then
                MsgBox("El rango de d�as a reservar no pueden exceder 60 d�as")
                Return
            End If

            load_data()
            UltraGrid1.Focus()
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0008", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        End Try
    End Sub

    Public Sub load_data()
        Try
            Me.Cursor = Cursors.WaitCursor

            If cs_ciudadRecoger.ListBox1.SelectedIndex = -1 OrElse cs_ciudadRecoger.lista_lugares.Count = 0 Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                datosBusqueda_auto.rubro = enumServices.Auto
                datosBusqueda_auto.confirmada = False
                '
                datosBusqueda_auto.fechaRecoger = dtp_fechaRecoger.Value
                datosBusqueda_auto.horaRecoger = uce_horaRecoger.SelectedItem.DataValue
                datosBusqueda_auto.ciudadRecoger = cs_ciudadRecoger.selected_valor_Codigo
                datosBusqueda_auto.fechaDevolver = dtp_fechaDevolver.Value
                datosBusqueda_auto.horaDevolver = uce_horaDevolver.SelectedItem.DataValue
                If cs_ciudadDevolver.selected_valor_Codigo = Nothing Then
                    'si no selecciono ciudad diferente, pone la ciudad de origen
                    datosBusqueda_auto.ciudadDevolver = cs_ciudadRecoger.selected_valor_Codigo
                Else
                    datosBusqueda_auto.ciudadDevolver = cs_ciudadDevolver.selected_valor_Codigo
                End If
                datosBusqueda_auto.clase = uce_clase.SelectedItem.DataValue
                datosBusqueda_auto.tipo = uce_tipo.SelectedItem.DataValue
                datosBusqueda_auto.compania = uce_compania.SelectedItem.DataValue
                datosBusqueda_auto.monedaBusqueda = uce_moneda.SelectedItem.DataValue
                datosBusqueda_auto.RefPoint = cs_ciudadRecoger.selected_valor_Ciudad.ToUpper

                PictureBox2.Visible = True
                MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                MI_Thread.Start()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0009", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        Finally
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

    Public Sub hacer_busqueda_async()
        Dim ds As New DataSet
        ds = obtenGenerales_autos.obten_general_CarAvail(datosBusqueda_auto.Clone)
        Dim str_resp As String
        If ds Is Nothing OrElse Not ds.Tables.Contains("Vendor") OrElse Not ds.Tables.Contains("Car") Then
            str_resp = "Total de empresas: 0"
        Else
            str_resp = "Total de empresas: " + CStr(ds.Tables("Vendor").Rows.Count)
        End If

        FIJA_GRID(UltraGrid1, ds)
        FIJA_PICT(PictureBox2, False)
        Fija_LBL(data_lbl_auto, str_resp)
    End Sub

    Delegate Sub FIJA_GRID_Callback(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
    Private Sub FIJA_GRID(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
        Try
            If grid.InvokeRequired Then
                Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                Me.Invoke(d, New Object() {grid, ds})
            Else
                formatea_grid(grid, ds)
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_PICT_Callback(ByVal pic As PictureBox, ByVal vis As Boolean)
    Private Sub FIJA_PICT(ByVal pic As PictureBox, ByVal vis As Boolean)
        Try
            If pic.InvokeRequired Then
                Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
                Me.Invoke(d, New Object() {pic, vis})
            Else
                pic.Visible = vis
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub Fija_LBL_Callback(ByVal lbl As Label, ByVal str As String)
    Private Sub Fija_LBL(ByVal lbl As Label, ByVal str As String)
        Try
            If lbl.InvokeRequired Then
                Dim d As New Fija_LBL_Callback(AddressOf Fija_LBL)
                Me.Invoke(d, New Object() {lbl, str})
            Else
                lbl.Text = str
            End If
        Catch ex As Exception
            'CapaLogicaNegocios.LogManager.agregar_registro("FijaProgressBar - busquedaHotel", ex.Message)
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
        'validaciones
        If ds Is Nothing Then
            grid.DataSource = Nothing
            Return
        End If
        If ds.Tables.Contains("Error") Then
            MessageBox.Show(ds.Tables("Error").Rows(0).Item("ErrorDescription"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            grid.DataSource = Nothing
            Return
        End If
        If Not ds.Tables.Contains("Office") Or Not ds.Tables.Contains("Vendor") Then
            grid.DataSource = Nothing
            Return
        End If

        'forma tabla
        Dim miDS As New DataSet("ds_autos")
        Dim miDT As New DataTable("dt_oficinas")
        miDT.Columns.AddRange(New DataColumn() {New DataColumn("idVendor"), New DataColumn("idOffice"), New DataColumn("idOfficeEnd"), New DataColumn("LocnCat"), New DataColumn("LocnNum"), New DataColumn("empresa"), New DataColumn("oficina"), New DataColumn("tarifas"), New DataColumn("tarifasNumero", GetType(Double)), New DataColumn("currency")})
        miDS.Tables.Add(miDT)

        Dim iteracion As Integer = 0

        For Each r1 As DataRow In ds.Tables("Office").Rows
            Try
                If iteracion = 5 Then
                    Dim i As Integer = 0
                End If

                Dim idVendo As String = r1.GetParentRow("Offices_Office").GetParentRow("Vendor_Offices").Item("idVendor")
                Dim idOffic As String = r1.Item("idOffice")
                Dim idOfEnd As String = r1.GetChildRows("Office_OfficeEnd")(0).Item("idOffice")
                Dim locnCat As String = r1.Item("LocnCat")
                Dim locnNum As String = r1.Item("LocnNum")
                Dim empresa As String = r1.GetParentRow("Offices_Office").GetParentRow("Vendor_Offices").Item("VendorName")
                Dim oficina As String = r1.Item("Address")
                Dim tarifas As String = ""
                Dim tariNum As String = ""
                Dim currenc As String = ""

                For Each r2 As DataRow In r1.GetChildRows("Office_Cars")
                    For Each r3 As DataRow In r2.GetChildRows("Cars_Car")

                        'AvailableRateGDS
                        If r3.GetChildRows("Car_AvailableRateGDS").Length > 0 Then
                            For Each r4 As DataRow In r3.GetChildRows("Car_AvailableRateGDS")
                                If tarifas = "" Then
                                    tarifas = r4.Item("RateAmtTotAproxCONV")
                                    tariNum = r4.Item("RateAmtTotAproxCONV")
                                    currenc = r4.Item("CUR")
                                End If

                                If CDbl(r4.Item("RateAmtTotAproxCONV")) < CDbl(tarifas) Then
                                    tarifas = r4.Item("RateAmtTotAproxCONV")
                                    tariNum = r4.Item("RateAmtTotAproxCONV")
                                    currenc = r4.Item("CUR")
                                End If
                            Next
                        End If

                        'AvailableRate
                        If r3.GetChildRows("Car_AvailableRate").Length > 0 Then
                            For Each r4 As DataRow In r3.GetChildRows("Car_AvailableRate")
                                If tarifas = "" Then
                                    tarifas = r4.Item("RateAmtTotAproxCONV")
                                    tariNum = r4.Item("RateAmtTotAproxCONV")
                                    currenc = r4.Item("CUR")
                                End If

                                If CDbl(r4.Item("RateAmtTotAproxCONV")) < CDbl(tarifas) Then
                                    tarifas = r4.Item("RateAmtTotAproxCONV")
                                    tariNum = r4.Item("RateAmtTotAproxCONV")
                                    currenc = r4.Item("CUR")
                                End If
                            Next
                        End If

                    Next
                Next
                tarifas = Format(CDbl(tarifas), "c") + " " + currenc

                miDT.Rows.Add(New String() {idVendo, idOffic, idOfEnd, locnCat, locnNum, empresa, oficina, tarifas, tariNum, currenc})

                iteracion += 1
            Catch ex As Exception
                MsgBox("formatea_grid - iteracion: " + CStr(iteracion))
            End Try
        Next

        'llena grid
        Dim dv As DataView = miDS.Tables("dt_oficinas").DefaultView
        dv.Sort = "tarifasNumero asc"
        grid.DataSource = dv

        CapaPresentacion.Idiomas.cambiar_busquedaAuto(Me)
    End Sub

    Private Sub cs_ciudad_llena1() Handles cs_ciudadRecoger.llena
        herramientas.llenar(True, cs_ciudadRecoger.TextBox1.Text, cs_ciudadRecoger)
    End Sub

    Private Sub cs_ciudad_llena2() Handles cs_ciudadDevolver.llena
        herramientas.llenar(True, cs_ciudadDevolver.TextBox1.Text, cs_ciudadDevolver)
    End Sub

    Private Sub busquedaAutos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        misForms.busquedaAutos_ultimoActivo = Nothing
    End Sub

    Private Sub busquedaAutos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'CapaLogicaNegocios.Calls.VerificaNewCall()

        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        'UltraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect

        CapaPresentacion.Common.SetTabColor(Me)
        'Dim miColor As Color = herramientas.color_disponible
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackColor = miColor
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackColor2 = Color.FromArgb(100, 191, 219, 255)
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackColor = miColor
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackColor2 = Color.White
        'misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical

        botones = New String() {"btn_tarifasAuto"}

        For Each ctl As Control In Panel1.Controls
            Select Case ctl.GetType.Name
                Case "Label", "PictureBox", "Button"
                    'estos no ejecutan el performClick
                Case Else
                    AddHandler ctl.KeyPress, AddressOf ctlKeyPress
            End Select
        Next

        cs_ciudadRecoger.crea_lista(Me, Panel1.Location.X, Panel1.Location.Y)
        AddHandler cs_ciudadRecoger.TextBox1.KeyPress, AddressOf ctlKeyPress
        AddHandler cs_ciudadRecoger.ListBox1.KeyPress, AddressOf ctlKeyPress

        cs_ciudadDevolver.crea_lista(Me, Panel1.Location.X, Panel1.Location.Y)
        AddHandler cs_ciudadDevolver.TextBox1.KeyPress, AddressOf ctlKeyPress
        AddHandler cs_ciudadDevolver.ListBox1.KeyPress, AddressOf ctlKeyPress

        llenarCombos()
        Dim fecha_menor As DateTime = DateTime.Now
        dtp_fechaRecoger.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_fechaDevolver.MinDate = New DateTime(fecha_menor.Year, fecha_menor.Month, fecha_menor.Day)
        dtp_fechaRecoger.Value = Now.AddDays(1)
        dtp_fechaDevolver.Value = dtp_fechaRecoger.Value.AddDays(1)

        data_lbl_auto.Text = "Total de empresas: "
    End Sub

    Private Sub ctlKeyPress(ByVal s As Object, ByVal e As KeyPressEventArgs)
        If Asc(e.KeyChar) = Keys.Enter Then
            btn_buscar.PerformClick()
        End If
    End Sub

    Public Sub llenarCombos()
        CapaPresentacion.Common.moneda.FillUceMoneda(uce_moneda)

        uce_horaRecoger.Items.Clear()
        uce_horaDevolver.Items.Clear()
        For i As Integer = 0 To 23
            Dim hr As String = Format(i, "00")
            Dim mi As String

            mi = "00"
            uce_horaRecoger.Items.Add(hr + mi, hr + ":" + mi)
            uce_horaDevolver.Items.Add(hr + mi, hr + ":" + mi)

            If i = 12 Then uce_horaRecoger.SelectedIndex = uce_horaRecoger.Items.Count - 1
            If i = 12 Then uce_horaDevolver.SelectedIndex = uce_horaDevolver.Items.Count - 1

            mi = "30"
            uce_horaRecoger.Items.Add(hr + mi, hr + ":" + mi)
            uce_horaDevolver.Items.Add(hr + mi, hr + ":" + mi)
        Next
        If uce_horaRecoger.SelectedIndex = -1 Then uce_horaRecoger.SelectedIndex = 0
        If uce_horaDevolver.SelectedIndex = -1 Then uce_horaDevolver.SelectedIndex = 0

        uce_clase.Items.Clear()
        uce_clase.Items.Add("0", "Sin preferencia")
        uce_clase.Items.Add("24", "Compacto")
        uce_clase.Items.Add("25", "Mini")
        uce_clase.Items.Add("26", "Econ�mico")
        uce_clase.Items.Add("27", "Intermedio")
        uce_clase.Items.Add("28", "Estandard")
        uce_clase.Items.Add("29", "Grande")
        uce_clase.Items.Add("44", "Pemium")
        uce_clase.Items.Add("45", "De Lujo")
        uce_clase.Items.Add("46", "Especial")
        If uce_clase.SelectedIndex = -1 Then uce_clase.SelectedIndex = 0

        uce_tipo.Items.Clear()
        uce_tipo.Items.Add("0", "Sin preferencias")
        uce_tipo.Items.Add("2", "2/4 Puertas")
        uce_tipo.Items.Add("4", "Vagoneta")
        uce_tipo.Items.Add("9", "4x4")
        uce_tipo.Items.Add("34", "Deportivo")
        uce_tipo.Items.Add("35", "2 Puertas")
        uce_tipo.Items.Add("54", "Van")
        uce_tipo.Items.Add("55", "Limosina")
        uce_tipo.Items.Add("56", "Pick up /  Utility")
        'uce_tipo.Items.Add("57", "Special")
        If uce_tipo.SelectedIndex = -1 Then uce_tipo.SelectedIndex = 0

        uce_compania.Items.Clear()
        uce_compania.Items.Add("0", "Sin preferencias")
        uce_compania.Items.Add("73", "AGA Rent a Car")
        uce_compania.Items.Add("81", "Europcar La Paz")
        uce_compania.Items.Add("57", "Budget")
        uce_compania.Items.Add("72", "Dollar")
        '------------------------------------------------------------------------
        'uce_compania.Items.Clear()
        'uce_compania.Items.Add("0", "Sin preferencias")
        'uce_compania.Items.Add("81", "Europcar La Paz")
        'uce_compania.Items.Add("EP", "Europcar")
        'uce_compania.Items.Add("ZL", "National Car Rental")
        'uce_compania.Items.Add("ZT", "Thrifty Car Rental")
        'uce_compania.Items.Add("ZR", "Dollar Rent A Car")
        'uce_compania.Items.Add("AL", "Alamo Rent A Car")
        'uce_compania.Items.Add("ZI", "Avis Rent A Car Syst")
        'uce_compania.Items.Add("ZD", "Budget Rent A Car")

        If uce_compania.SelectedIndex = -1 Then uce_compania.SelectedIndex = 0

    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        datosBusqueda_auto.idVendor = UltraGrid1.ActiveRow.Cells("idVendor").Value
        datosBusqueda_auto.idOffice = UltraGrid1.ActiveRow.Cells("idOffice").Value
        datosBusqueda_auto.idOfficeEnd = UltraGrid1.ActiveRow.Cells("idOfficeEnd").Value
        datosBusqueda_auto.LocnCat = UltraGrid1.ActiveRow.Cells("LocnCat").Value
        datosBusqueda_auto.LocnNum = UltraGrid1.ActiveRow.Cells("LocnNum").Value
        datosBusqueda_auto.empresa = UltraGrid1.ActiveRow.Cells("empresa").Value

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_tarifasAuto").SharedProps.Enabled = True

        CapaPresentacion.Idiomas.cambiar_busquedaAuto(Me)
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow, UltraGrid1.DoubleClickRow
        'por alguna extra�a razon se ejecuta 2 veces, por eso con CONT se controla para que solo entre una vez
        If cont = 0 Then
            misForms.exe_accion(ContextMenuStrip1, "btn_tarifasAuto")
            cont += 1
        Else
            cont -= 1
        End If
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown, UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        If e.KeyCode = Keys.Enter AndAlso UltraGrid1.ActiveRow IsNot Nothing Then
            misForms.exe_accion(ContextMenuStrip1, "btn_tarifasAuto")
        End If
    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_fechaRecoger.ValueChanged, dtp_fechaDevolver.ValueChanged
        herramientas.fechas_inteligentes(dtp_fechaRecoger, dtp_fechaDevolver, sender, 0, Nothing)
    End Sub

End Class