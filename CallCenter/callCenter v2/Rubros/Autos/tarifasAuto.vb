Public Class tarifasAuto

    Public botones() As String
    Private cont As Integer = 0
    'Public datos_call As dataCall
    Public datosBusqueda_auto As New dataBusqueda_auto_test
    Public MI_Thread As Threading.Thread

    Public Sub New(ByVal db As dataBusqueda_auto_test) 'ByVal dc As dataCall,

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
        datosBusqueda_auto = db.Clone
    End Sub

    Public Sub load_data()
        Try
            Me.Cursor = Cursors.WaitCursor
            inicializar_controles()

            If False Then
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                'datosBusqueda_actividad.ciudadActividad = cs_ciudad.selected_valor_Codigo
                'datosBusqueda_actividad.fecha = dtp_fecha.Value
                'datosBusqueda_actividad.moneda = uce_moneda.SelectedItem.DataValue
                'datosBusqueda_actividad.adultos = txt_adultos.Text

                PictureBox2.Visible = True
                MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                MI_Thread.Start()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0010", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
        Finally
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

    Public Sub hacer_busqueda_async()
        Dim ds As New DataSet
        ds = obtenGenerales_autos.obten_general_CarAvail(datosBusqueda_auto.Clone)

        FIJA_GRID(UltraGrid1, ds)
        FIJA_PICT(PictureBox2, False)
    End Sub

    Delegate Sub FIJA_GRID_Callback(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
    Private Sub FIJA_GRID(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
        Try
            If grid.InvokeRequired Then
                Dim d As New FIJA_GRID_Callback(AddressOf FIJA_GRID)
                Me.Invoke(d, New Object() {grid, ds})
            Else
                formatea_grid(grid, ds)
            End If
        Catch ex As Exception
            'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_PICT_Callback(ByVal pic As PictureBox, ByVal vis As Boolean)
    Private Sub FIJA_PICT(ByVal pic As PictureBox, ByVal vis As Boolean)
        Try
            If pic.InvokeRequired Then
                Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
                Me.Invoke(d, New Object() {pic, vis})
            Else
                pic.Visible = vis
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub formatea_grid(ByVal grid As Infragistics.Win.UltraWinGrid.UltraGrid, ByVal ds As DataSet)
        'validaciones
        If ds.Tables.Contains("Error") Then
            MessageBox.Show(ds.Tables("Error").Rows(0).Item("Message"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            grid.DataSource = Nothing
            Return
        End If
        If Not ds.Tables.Contains("Office") Or Not ds.Tables.Contains("Vendor") Then
            grid.DataSource = Nothing
            Return
        End If

        'forma tabla
        Dim miDS As New DataSet("ds_autos")
        Dim miDT As New DataTable("dt_car")
        miDT.Columns.Add(New DataColumn("clase"))
        miDT.Columns.Add(New DataColumn("tipo"))
        miDT.Columns.Add(New DataColumn("GDSType"))
        miDT.Columns.Add(New DataColumn("ac"))
        miDT.Columns.Add(New DataColumn("descripcion"))
        miDT.Columns.Add(New DataColumn("ServiceProvider"))
        miDT.Columns.Add(New DataColumn("tarifas"))
        miDT.Columns.Add(New DataColumn("RateAmtTotAproxCONV", GetType(Double)))
        miDT.Columns.Add(New DataColumn("currency"))
        miDT.Columns.Add(New DataColumn("RateDBKey"))
        miDT.Columns.Add(New DataColumn("RateCat"))
        miDT.Columns.Add(New DataColumn("RateType"))
        miDT.Columns.Add(New DataColumn("Rate"))
        miDT.Columns.Add(New DataColumn("Tax"))
        miDT.Columns.Add(New DataColumn("SIPPClassText"))
        miDT.Columns.Add(New DataColumn("SIPPTypeText"))
        miDT.Columns.Add(New DataColumn("RateAmtCONV"))
        miDT.Columns.Add(New DataColumn("CUR"))
        miDT.Columns.Add(New DataColumn("idTypeCar"))
        miDT.Columns.Add(New DataColumn("RateSource"))
        miDT.Columns.Add(New DataColumn("FeeName"))
        miDT.Columns.Add(New DataColumn("Percent"))
        miDT.Columns.Add(New DataColumn("Amount"))
        miDT.Columns.Add(New DataColumn("idRate"))
        miDS.Tables.Add(miDT)

        For Each r1 As DataRow In ds.Tables("Car").Rows
            Try
                Dim idVendo As String = r1.GetParentRow("Cars_Car").GetParentRow("Office_Cars").GetParentRow("Offices_Office").GetParentRow("Vendor_Offices").Item("idVendor")
                Dim idOffic As String = r1.GetParentRow("Cars_Car").GetParentRow("Office_Cars").Item("idOffice")

                If idVendo <> datosBusqueda_auto.idVendor OrElse idOffic <> datosBusqueda_auto.idOffice Then Continue For

                Dim clase___ As String = r1.Item("SIPPClassText")
                Dim tipo____ As String = r1.Item("SIPPTypeText")
                Dim GDSType_ As String = r1.Item("GDSType")
                Dim AC______ As String = r1.Item("AC")
                Dim descript As String = "" 'r1.Item("Description")
                Dim ServiceP As String = r1.Item("ServiceProvider")
                Dim tarifas_ As String = ""
                Dim RateAmtT As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateAmtTotAproxCONV")
                Dim currency As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("CUR")
                Dim rateDBKe As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateDBKey")
                Dim rateCat_ As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateCat")
                Dim rateType As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateType")
                Dim rate____ As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("Rate")
                Dim Tax_____ As String = r1.GetParentRow("Cars_Car").GetParentRow("Office_Cars").Item("Tax")
                Dim SIPPClas As String = r1.Item("SIPPClassText")
                Dim SIPPType As String = r1.Item("SIPPTypeText")
                Dim RateAmtC As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateAmtCONV")
                Dim CUR_____ As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("CUR")
                Dim idTypeCa As String = r1.Item("idTypeCar")
                Dim RateSour As String = "" 'r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateSource")
                'Dim idFee___ As String = ""
                Dim FeeName_ As String = ""
                Dim Percent_ As String = ""
                Dim Amount__ As String = ""
                Dim idRate__ As String = ""

                '---------------------
                If r1.GetChildRows("Car_AvailableRateGDS").Length > 0 Then
                    RateAmtT = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateAmtTotAproxCONV")
                    currency = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("CUR")
                    rateDBKe = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateDBKey")
                    rateCat_ = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateCat")
                    rateType = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateType")
                    rate____ = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("Rate")
                    RateAmtC = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateAmtCONV")
                    CUR_____ = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("CUR")
                    RateSour = r1.GetChildRows("Car_AvailableRateGDS")(0).Item("RateSource")
                    idRate__ = ""
                End If
                If r1.GetChildRows("Car_AvailableRate").Length > 0 Then
                    RateAmtT = r1.GetChildRows("Car_AvailableRate")(0).Item("RateAmtTotAproxCONV")
                    currency = r1.GetChildRows("Car_AvailableRate")(0).Item("CUR")
                    rateDBKe = ""
                    'rateCat_ = r1.GetChildRows("Car_AvailableRate")(0).Item("RateCat")
                    'rateType = r1.GetChildRows("Car_AvailableRate")(0).Item("RateType")
                    'rate____ = r1.GetChildRows("Car_AvailableRate")(0).Item("Rate")
                    RateAmtC = r1.GetChildRows("Car_AvailableRate")(0).Item("RateAmtCONV")
                    CUR_____ = r1.GetChildRows("Car_AvailableRate")(0).Item("CUR")
                    'RateSour = r1.GetChildRows("Car_AvailableRate")(0).Item("RateSource")
                    idRate__ = r1.GetChildRows("Car_AvailableRate")(0).Item("idRate")
                End If
                '---------------------
                tarifas_ = Format(CDbl(RateAmtT), "c") + " " + currency
                If r1.GetChildRows("Car_AvailableRate").Length > 0 AndAlso r1.GetChildRows("Car_AvailableRate")(0).GetChildRows("AvailableRate_AdditionalFees").Length > 0 AndAlso r1.GetChildRows("Car_AvailableRate")(0).GetChildRows("AvailableRate_AdditionalFees")(0).GetChildRows("AdditionalFees_AdditionalFee").Length > 0 Then
                    'idFee___ = r1.GetChildRows("Car_AvailableRate")(0).GetChildRows("AvailableRate_AdditionalFees")(0).GetChildRows("AdditionalFees_AdditionalFee")(0).Item("idFee")
                    FeeName_ = r1.GetChildRows("Car_AvailableRate")(0).GetChildRows("AvailableRate_AdditionalFees")(0).GetChildRows("AdditionalFees_AdditionalFee")(0).Item("FeeName")
                    Percent_ = r1.GetChildRows("Car_AvailableRate")(0).GetChildRows("AvailableRate_AdditionalFees")(0).GetChildRows("AdditionalFees_AdditionalFee")(0).Item("Percent")
                    Amount__ = r1.GetChildRows("Car_AvailableRate")(0).GetChildRows("AvailableRate_AdditionalFees")(0).GetChildRows("AdditionalFees_AdditionalFee")(0).Item("Amount")
                End If
                If r1.Item("Transmission") = "A" Then descript += "Transmisión automática" Else descript += "Transmisión semi-automática"
                descript += "," + r1.Item("SIPPTypeText")
                If Not r1.IsNull("Description") AndAlso r1.Item("Description") <> "" Then descript += "," + r1.Item("Description")
                descript += "."

                miDT.Rows.Add(New String() {clase___, tipo____, GDSType_, AC______, descript, ServiceP, tarifas_, RateAmtT, currency, rateDBKe, rateCat_, rateType, rate____, Tax_____, SIPPClas, SIPPType, RateAmtC, CUR_____, idTypeCa, RateSour, FeeName_, Percent_, Amount__, idRate__}) 'idFee___,
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next

        'llena grid
        Dim dv As DataView = miDS.Tables("dt_car").DefaultView
        dv.Sort = "RateAmtTotAproxCONV asc"
        grid.DataSource = dv

        CapaPresentacion.Idiomas.cambiar_tarifasAuto(Me)
        Dim si As String = "Si"
        Dim no As String = "No"
        For Each r As Infragistics.Win.UltraWinGrid.UltraGridRow In UltraGrid1.Rows
            If r.Cells("ac").Value = "Y" Then r.Cells("ac").Value = si Else r.Cells("ac").Value = no
        Next
    End Sub

    Private Sub tarifasAuto_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        misForms.tarifasAutos_ultimoActivo = Nothing
    End Sub

    Private Sub tarifasAuto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        'UltraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect

        'CapaPresentacion.Common.SetTabColor(Me)
        ''Dim miColor As Color = herramientas.color_disponible
        ''misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackColor = miColor
        ''misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackColor2 = Color.FromArgb(100, 191, 219, 255)
        ''misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        ''misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackColor = miColor
        ''misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackColor2 = Color.White
        ''misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Dim tem_settings As Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings = misForms.form1.UltraTabbedMdiManager1.TabFromForm(misForms.busquedaAutos_ultimoActivo).Settings
        misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.TabAppearance = tem_settings.TabAppearance
        misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me).Settings.SelectedTabAppearance = tem_settings.SelectedTabAppearance

        botones = New String() {"btn_reservarAuto", "btn_reglasAuto"}
        'CapaPresentacion.Idiomas.cambiar_tarifasAuto(Me)

        'For Each ctl As Control In Panel1.Controls
        '    Select Case ctl.GetType.Name
        '        Case "Label", "PictureBox", "Button"
        '            'estos no ejecutan el performClick
        '        Case Else
        '            AddHandler ctl.KeyPress, AddressOf ctlKeyPress
        '    End Select
        'Next

        load_data()
        'CapaPresentacion.Idiomas.cambiar_busquedaVuelo(Me)
        UltraGrid1.Focus()
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        get_data_row()
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow, UltraGrid1.DoubleClickRow
        'por alguna extraña razon se ejecuta 2 veces, por eso con CONT se controla para que solo entre una vez
        If cont = 0 Then
            get_data_row()
            misForms.exe_accion(ContextMenuStrip1, "btn_reservarAuto")
            cont += 1
        Else
            cont -= 1
        End If
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown, UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        If e.KeyCode = Keys.Enter AndAlso UltraGrid1.ActiveRow IsNot Nothing Then
            get_data_row()
            misForms.exe_accion(ContextMenuStrip1, "btn_reservarAuto")
        End If
    End Sub

    Private Sub inicializar_controles()
        'data_lbl_tipo.Text = datosBusqueda_auto.SIPPTypeText
        data_lbl_ciudad.Text = datosBusqueda_auto.RefPoint
        'data_lbl_empresa.Text = datosBusqueda_auto.SIPPClassText
        data_lbl_empresa.Text = datosBusqueda_auto.empresa
        data_lbl_fecha.Text = datosBusqueda_auto.fechaRecoger.ToString("dd/MMM/yyyy")
    End Sub

    Private Sub get_data_row()
        datosBusqueda_auto.ServiceProvider = UltraGrid1.ActiveRow.Cells("ServiceProvider").Value
        datosBusqueda_auto.RateDBKey = UltraGrid1.ActiveRow.Cells("RateDBKey").Value
        datosBusqueda_auto.idRate = UltraGrid1.ActiveRow.Cells("idRate").Value
        datosBusqueda_auto.GDSType = UltraGrid1.ActiveRow.Cells("GDSType").Value
        datosBusqueda_auto.RateCat = UltraGrid1.ActiveRow.Cells("RateCat").Value
        datosBusqueda_auto.RateType = UltraGrid1.ActiveRow.Cells("RateType").Value
        datosBusqueda_auto.Rate = UltraGrid1.ActiveRow.Cells("Rate").Value
        datosBusqueda_auto.Tax = UltraGrid1.ActiveRow.Cells("Tax").Value
        datosBusqueda_auto.SIPPClassText = UltraGrid1.ActiveRow.Cells("SIPPClassText").Value
        datosBusqueda_auto.SIPPTypeText = UltraGrid1.ActiveRow.Cells("SIPPTypeText").Value
        datosBusqueda_auto.RateAmtCONV = UltraGrid1.ActiveRow.Cells("RateAmtCONV").Value
        datosBusqueda_auto.CUR = UltraGrid1.ActiveRow.Cells("CUR").Value
        datosBusqueda_auto.idTypeCar = UltraGrid1.ActiveRow.Cells("idTypeCar").Value
        datosBusqueda_auto.RateSource = UltraGrid1.ActiveRow.Cells("RateSource").Value
        datosBusqueda_auto.FeeName = UltraGrid1.ActiveRow.Cells("FeeName").Value
        datosBusqueda_auto.Percent = UltraGrid1.ActiveRow.Cells("Percent").Value
        datosBusqueda_auto.Amount = UltraGrid1.ActiveRow.Cells("Amount").Value
        'datosBusqueda_auto.idfee = UltraGrid1.ActiveRow.Cells("idfee").Value
        datosBusqueda_auto.RateAmtTotAproxCONV = UltraGrid1.ActiveRow.Cells("RateAmtTotAproxCONV").Value

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservarAuto").SharedProps.Enabled = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reglasAuto").SharedProps.Enabled = True

        CapaPresentacion.Idiomas.cambiar_tarifasAuto(Me)
    End Sub

End Class