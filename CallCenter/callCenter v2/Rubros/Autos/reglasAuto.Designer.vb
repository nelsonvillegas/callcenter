<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reglasAuto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reglasAuto))
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.data_lbl_fecha = New System.Windows.Forms.Label
        Me.lbl_fecha = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.data_lbl_tipo = New System.Windows.Forms.Label
        Me.data_lbl_ciudad = New System.Windows.Forms.Label
        Me.data_lbl_clase = New System.Windows.Forms.Label
        Me.lbl_tipo = New System.Windows.Forms.Label
        Me.lbl_ciudad = New System.Windows.Forms.Label
        Me.lbl_clase = New System.Windows.Forms.Label
        Me.btn_aceptar = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 96)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(541, 432)
        Me.WebBrowser1.TabIndex = 11
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.data_lbl_fecha)
        Me.GroupBox1.Controls.Add(Me.lbl_fecha)
        Me.GroupBox1.Controls.Add(Me.PictureBox2)
        Me.GroupBox1.Controls.Add(Me.data_lbl_tipo)
        Me.GroupBox1.Controls.Add(Me.data_lbl_ciudad)
        Me.GroupBox1.Controls.Add(Me.data_lbl_clase)
        Me.GroupBox1.Controls.Add(Me.lbl_tipo)
        Me.GroupBox1.Controls.Add(Me.lbl_ciudad)
        Me.GroupBox1.Controls.Add(Me.lbl_clase)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(541, 78)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Informacion"
        '
        'data_lbl_fecha
        '
        Me.data_lbl_fecha.AutoSize = True
        Me.data_lbl_fecha.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_fecha.Location = New System.Drawing.Point(92, 35)
        Me.data_lbl_fecha.Name = "data_lbl_fecha"
        Me.data_lbl_fecha.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_fecha.TabIndex = 28
        Me.data_lbl_fecha.Text = "----"
        '
        'lbl_fecha
        '
        Me.lbl_fecha.AutoSize = True
        Me.lbl_fecha.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fecha.Location = New System.Drawing.Point(27, 35)
        Me.lbl_fecha.Name = "lbl_fecha"
        Me.lbl_fecha.Size = New System.Drawing.Size(40, 13)
        Me.lbl_fecha.TabIndex = 27
        Me.lbl_fecha.Text = "Fecha:"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(244, 51)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 26
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'data_lbl_tipo
        '
        Me.data_lbl_tipo.AutoSize = True
        Me.data_lbl_tipo.Location = New System.Drawing.Point(306, 22)
        Me.data_lbl_tipo.Name = "data_lbl_tipo"
        Me.data_lbl_tipo.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_tipo.TabIndex = 25
        Me.data_lbl_tipo.Text = "----"
        '
        'data_lbl_ciudad
        '
        Me.data_lbl_ciudad.AutoSize = True
        Me.data_lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_ciudad.Location = New System.Drawing.Point(92, 48)
        Me.data_lbl_ciudad.Name = "data_lbl_ciudad"
        Me.data_lbl_ciudad.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_ciudad.TabIndex = 24
        Me.data_lbl_ciudad.Text = "----"
        '
        'data_lbl_clase
        '
        Me.data_lbl_clase.AutoSize = True
        Me.data_lbl_clase.BackColor = System.Drawing.Color.Transparent
        Me.data_lbl_clase.Location = New System.Drawing.Point(92, 22)
        Me.data_lbl_clase.Name = "data_lbl_clase"
        Me.data_lbl_clase.Size = New System.Drawing.Size(19, 13)
        Me.data_lbl_clase.TabIndex = 23
        Me.data_lbl_clase.Text = "----"
        '
        'lbl_tipo
        '
        Me.lbl_tipo.AutoSize = True
        Me.lbl_tipo.Location = New System.Drawing.Point(241, 22)
        Me.lbl_tipo.Name = "lbl_tipo"
        Me.lbl_tipo.Size = New System.Drawing.Size(31, 13)
        Me.lbl_tipo.TabIndex = 22
        Me.lbl_tipo.Text = "Tipo:"
        '
        'lbl_ciudad
        '
        Me.lbl_ciudad.AutoSize = True
        Me.lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudad.Location = New System.Drawing.Point(27, 48)
        Me.lbl_ciudad.Name = "lbl_ciudad"
        Me.lbl_ciudad.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudad.TabIndex = 20
        Me.lbl_ciudad.Text = "Ciudad:"
        '
        'lbl_clase
        '
        Me.lbl_clase.AutoSize = True
        Me.lbl_clase.BackColor = System.Drawing.Color.Transparent
        Me.lbl_clase.Location = New System.Drawing.Point(27, 22)
        Me.lbl_clase.Name = "lbl_clase"
        Me.lbl_clase.Size = New System.Drawing.Size(36, 13)
        Me.lbl_clase.TabIndex = 21
        Me.lbl_clase.Text = "Clase:"
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_aceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_aceptar.Location = New System.Drawing.Point(478, 534)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(75, 23)
        Me.btn_aceptar.TabIndex = 9
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = False
        '
        'reglasAuto
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_aceptar
        Me.ClientSize = New System.Drawing.Size(565, 569)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "reglasAuto"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reglas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents data_lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents data_lbl_tipo As System.Windows.Forms.Label
    Friend WithEvents data_lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents data_lbl_clase As System.Windows.Forms.Label
    Friend WithEvents lbl_tipo As System.Windows.Forms.Label
    Friend WithEvents lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents lbl_clase As System.Windows.Forms.Label
End Class
