Public Class reservarAuto

    Public datosBusqueda_auto As dataBusqueda_auto_test
    'Public datos_call As dataCall
    Public tipoAccion As enumReservar_TipoAccion = enumReservar_TipoAccion.reservar

    Private Sub reservarActividad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If misForms.newCall Is Nothing Then btn_reservar.Enabled = False

        clear_controls(Me)
    End Sub

    Public Function cargar() As Boolean
        'If herramientas.cargar_paises(uce_paises) Then
        If tipoAccion = enumReservar_TipoAccion.reservar Then
            inicializar_controles()
        Else
            display_controles()
        End If

        Return True
        'Else
        '    Return False
        'End If
    End Function

    Private Sub btn_reservar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_reservar.Click
        'If uce_tipoTarjeta.SelectedIndex = 0 Then
        '    messagebox---("Debe seleccionar una tarjeta de cr�dito")
        '    Return
        'End If

        'datosBusqueda_auto.nombre = txt_nombreTraveler.Text
        'datosBusqueda_auto.apellido = txt_apellido.Text
        ''datosBusqueda_auto.nombre_prefijo = txt_nombre_p.Text
        ''datosBusqueda_auto.apellido_prefijo = txt_apellido_p.Text
        'datosBusqueda_auto.direccion = txt_direccion.Text
        ''datosBusqueda_auto.ciudad = txt_ciudad.Text
        ''datosBusqueda_auto.estado = txt_estado.Text
        'datosBusqueda_auto.cp = txt_cp.Text
        ''datosBusqueda_auto.pais = uce_paises.SelectedItem.DataValue
        ''datosBusqueda_auto.telefono_AreaCityCode = txt_area.Text
        ''datosBusqueda_auto.telefono_PhoneNumber = txt_telefono.Text
        ''datosBusqueda_auto.telefono_PhoneUseType = txt_telTipo.Text
        'datosBusqueda_auto.telefono = txt_telefono.Text
        'datosBusqueda_auto.email = txt_email.Text
        'datosBusqueda_auto.cc_tipo = uce_tipoTarjeta.SelectedItem.DataValue
        'datosBusqueda_auto.cc_nombre = txt_ccNombre.Text
        'datosBusqueda_auto.cc_numero = txt_ccNumero.Text
        'datosBusqueda_auto.cc_numSeguridad = txt_ccNumeroSeguridad.Text
        'datosBusqueda_auto.cc_fechaExpira = txt_MM.Text + txt_AAAA.Text

        If Not verifica_datos_call() Then Return

        If misForms.newCall.datos_call.add_rvaAuto(datosBusqueda_auto.Clone, False) Then
            CapaPresentacion.Calls.ActivaNewCall()
            Close()
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0297_soloUno"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub inicializar_controles()
        '--------
        Dim ds As DataSet = CapaLogicaNegocios.GetWS.obten_auto_CarRules(datosBusqueda_auto)
        'Dim TotalAdicional As Double = 0
        'For Each r As DataRow In ds.Tables("AdditionalFee").Rows
        '    TotalAdicional += CDbl(r.Item("AltAmount"))
        'Next
        '--------

        If ds Is Nothing Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_358_errRegAuto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            btn_reservar.Enabled = False
            Return
        End If

        If ds.Tables.Contains("Error") AndAlso ds.Tables("Error").Rows.Count > 0 AndAlso Not ds.Tables("Error").Rows(0).IsNull("ErrorNumber") AndAlso ds.Tables("Error").Rows(0).Item("ErrorNumber") <> "0" Then
            'MessageBox(ds.Tables("Error").Rows(0).Item("ErrorDescription"))
            MessageBox.Show(ds.Tables("Error").Rows(0).Item("ErrorDescription"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            btn_reservar.Enabled = False
            Return
        End If

        datosBusqueda_auto.TotalTotal = ds.Tables("Request").Rows(0).Item("AltTotalFinal")

        Dim subT As Double = CDbl(datosBusqueda_auto.RateAmtCONV)
        Dim tota As Double = CDbl(datosBusqueda_auto.TotalTotal)     'datosBusqueda_auto.RateAmtTotAproxCONV
        Dim impu As Double = tota - subT

        'tota += TotalAdicional

        lbl_subTotal.Text = "Subtotal: " + Format(subT, "c") + " " + datosBusqueda_auto.CUR
        lbl_impuestos.Text = "Impuestos: " + Format(impu, "c") + " " + datosBusqueda_auto.CUR
        lbl_total.Text = "Total: " + Format(tota, "c") + " " + datosBusqueda_auto.CUR

        'datosBusqueda_auto.TotalTotal = Format(tota, "c") + " " + datosBusqueda_auto.CUR 'lbl_total.Text
    End Sub

    Private Sub display_controles()

    End Sub

End Class