Public Class reglasAuto

    'Public datos_call As dataCall
    Public datosBusqueda_auto As New dataBusqueda_auto_test
    Public MI_Thread As Threading.Thread

    Public Sub New(ByVal db As dataBusqueda_auto_test) 'ByVal dc As dataCall,

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
        datosBusqueda_auto = db.Clone
    End Sub

    Public Sub load_data()
        Try
            Me.Cursor = Cursors.WaitCursor
            inicializar_controles()

            If False Then
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0122_debeSeleccionarLugar"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                'datosBusqueda_actividad.ciudadActividad = cs_ciudad.selected_valor_Codigo
                'datosBusqueda_actividad.fecha = dtp_fecha.Value
                'datosBusqueda_actividad.moneda = uce_moneda.SelectedItem.DataValue
                'datosBusqueda_actividad.adultos = txt_adultos.Text

                PictureBox2.Visible = True
                MI_Thread = New Threading.Thread(AddressOf hacer_busqueda_async)
                MI_Thread.Start()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0011", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        Finally
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

    Public Sub hacer_busqueda_async()
        Dim ds As New DataSet
        ds = obtenGenerales_autos.obten_general_CarRules(datosBusqueda_auto.Clone)

        FIJA_WEB(WebBrowser1, ds)
        FIJA_PICT(PictureBox2, False)
    End Sub

    Delegate Sub FIJA_WEB_Callback(ByVal web As WebBrowser, ByVal ds As DataSet)
    Private Sub FIJA_WEB(ByVal web As WebBrowser, ByVal ds As DataSet)
        Try
            If web.InvokeRequired Then
                Dim d As New FIJA_WEB_Callback(AddressOf FIJA_WEB)
                Me.Invoke(d, New Object() {web, ds})
            Else
                If ds.Tables.Contains("Error") AndAlso ds.Tables("Error").Rows.Count > 0 AndAlso Not ds.Tables("Error").Rows(0).IsNull("ErrorNumber") AndAlso ds.Tables("Error").Rows(0).Item("ErrorNumber") <> "0" Then
                    MessageBox.Show(ds.Tables("Error").Rows(0).Item("ErrorDescription"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If

                Dim r As DataRow = ds.Tables("Request").Rows(0)
                Dim str As String = ""

                str += "<p><b>Reglas</b></p>"
                str += "<p>" + r.Item("RulesGal") + "</p>"
                str += "<p>" + r.Item("Rules") + "</p>"
                str += "<p>" + r.Item("OfficeCheckInhoursOperation") + "</p>"
                'str += "<p><b>POLTICAS DE RESERVACIÓN</b></p>"
                'str += "<p>" + r.Item("Reser") + "</p>"
                'str += "<p><b>POLÍTICAS DE PAGO</b></p>"
                'str += "<p>" + r.Item("Pago") + "</p>"

                web.DocumentText = CONST_WB_TOP + str + CONST_WB_BOTTOM
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_PICT_Callback(ByVal pic As PictureBox, ByVal vis As Boolean)
    Private Sub FIJA_PICT(ByVal pic As PictureBox, ByVal vis As Boolean)
        Try
            If pic.InvokeRequired Then
                Dim d As New FIJA_PICT_Callback(AddressOf FIJA_PICT)
                Me.Invoke(d, New Object() {pic, vis})
            Else
                pic.Visible = vis
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub reglasActividad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        load_data()
        'CapaPresentacion.Idiomas.cambiar_busquedaVuelo(Me)
    End Sub

    Private Sub inicializar_controles()
        data_lbl_tipo.Text = datosBusqueda_auto.SIPPTypeText
        data_lbl_ciudad.Text = datosBusqueda_auto.RefPoint
        data_lbl_clase.Text = datosBusqueda_auto.SIPPClassText
        data_lbl_fecha.Text = datosBusqueda_auto.fechaRecoger.ToString("dd/MMM/yyyy")
    End Sub

End Class