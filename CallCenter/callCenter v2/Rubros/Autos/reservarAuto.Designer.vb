<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reservarAuto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reservarAuto))
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.btn_reservar = New System.Windows.Forms.Button
        Me.lbl_subTotal = New System.Windows.Forms.Label
        Me.lbl_impuestos = New System.Windows.Forms.Label
        Me.lbl_total = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btn_cancelar
        '
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(290, 62)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(90, 23)
        Me.btn_cancelar.TabIndex = 114
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'btn_reservar
        '
        Me.btn_reservar.Location = New System.Drawing.Point(155, 62)
        Me.btn_reservar.Name = "btn_reservar"
        Me.btn_reservar.Size = New System.Drawing.Size(129, 23)
        Me.btn_reservar.TabIndex = 113
        Me.btn_reservar.Text = "Agregar a la lista"
        Me.btn_reservar.UseVisualStyleBackColor = True
        '
        'lbl_subTotal
        '
        Me.lbl_subTotal.AutoSize = True
        Me.lbl_subTotal.Location = New System.Drawing.Point(12, 9)
        Me.lbl_subTotal.Name = "lbl_subTotal"
        Me.lbl_subTotal.Size = New System.Drawing.Size(49, 13)
        Me.lbl_subTotal.TabIndex = 115
        Me.lbl_subTotal.Text = "Subtotal:"
        '
        'lbl_impuestos
        '
        Me.lbl_impuestos.AutoSize = True
        Me.lbl_impuestos.Location = New System.Drawing.Point(12, 22)
        Me.lbl_impuestos.Name = "lbl_impuestos"
        Me.lbl_impuestos.Size = New System.Drawing.Size(55, 13)
        Me.lbl_impuestos.TabIndex = 116
        Me.lbl_impuestos.Text = "Impuestos"
        '
        'lbl_total
        '
        Me.lbl_total.AutoSize = True
        Me.lbl_total.Location = New System.Drawing.Point(12, 35)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(34, 13)
        Me.lbl_total.TabIndex = 117
        Me.lbl_total.Text = "Total:"
        '
        'reservarAuto
        '
        Me.AcceptButton = Me.btn_reservar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(392, 97)
        Me.Controls.Add(Me.lbl_total)
        Me.Controls.Add(Me.lbl_impuestos)
        Me.Controls.Add(Me.lbl_subTotal)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_reservar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "reservarAuto"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reservar auto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_reservar As System.Windows.Forms.Button
    Friend WithEvents lbl_subTotal As System.Windows.Forms.Label
    Friend WithEvents lbl_impuestos As System.Windows.Forms.Label
    Friend WithEvents lbl_total As System.Windows.Forms.Label
End Class
