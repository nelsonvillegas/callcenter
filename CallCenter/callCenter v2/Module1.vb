Module Module1

    Public username As String = "Univisit"
    Public password As String = "POXZ43dxcA34a"
    Public ws_login As webService_passport
    Public lista_colores As New List(Of Color)
    Public clear_test_controls = True
    Public busca_updates As Boolean = True
    Public moneda_selected As String = ""
    Public version_programadores As Boolean = False
    Public CONST_ENABLED_HOTELES As Boolean = True
    Public CONST_ENABLED_VUELOS As Boolean = False
    Public CONST_ENABLED_ACTIVIDADES As Boolean = False
    Public CONST_ENABLED_AUTOS As Boolean = False
    Public CONST_ENABLED_INGLES As Boolean = True

    Public CONST_WB_TOP As String = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml' ><head><title>Untitled Page</title></head><body style='font-family:tahoma; font-size: 8pt'>"
    Public CONST_WB_BOTTOM As String = "</body></html>"
    Public CONST_DB_NAME As String = "ventanaCallCenter"
    Public CONST_BD_VERSION As Integer = 11

    Public Sub clear_controls(ByVal frm As Form)
        If Not clear_test_controls Then Return

        For Each ctl As Control In frm.Controls
            'los txt
            If ctl.GetType.Name = "UltraTextEditor" Then ctl.Text = ""

            'los group box
            If ctl.GetType.Name = "UltraGroupBox" Then
                For Each ctl_2 As Control In ctl.Controls
                    If ctl_2.GetType.Name = "UltraTextEditor" Then ctl_2.Text = ""
                Next
            End If
				Next
    End Sub

    Public Function verifica_datos_call()
        MsgBox("este metodo ya no se usa")

        If misForms.newCall Is Nothing OrElse misForms.newCall.datos_call Is Nothing Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_359_errSessCall"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            Return False
        Else
            Return True
        End If
    End Function

End Module
