Public Class detalles
		Private MI_THREAD As System.Threading.Thread
		Private muestra_header As Boolean
		Public Imprimir As Boolean
		Public datosBusqueda_paquete As dataBusqueda_paquete
		Public datosBusqueda_vuelo As dataBusqueda_vuelo
		Public datosBusqueda_auto As dataBusqueda_auto_test
		Public datosBusqueda_hotel As dataBusqueda_hotel_test
		Public datosBusqueda_actividad As dataBusqueda_actividad_test

		Public Delegate Sub dTermino()
		Public TerminoImpr As dTermino
		Private cancelnav As Boolean
		Public Enum BitacoraAccion
				SinAccion
				Insertar
				Editar
        Cancelar
        Reactivar
		End Enum

		Public registraBitacora As BitacoraAccion
		Public DisplayRsXML_Antes As String
		Public DisplayRsXML_Despues As String

		Private Sub detalles_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
				Try
						If MI_THREAD IsNot Nothing Then
								MI_THREAD.Abort()
						End If
				Catch ex As Exception
						Dim i As Integer = 0
				End Try
		End Sub

		Private Sub detalles_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
				CapaPresentacion.Idiomas.cambiar_detalles(Me)
		End Sub

		Private Sub detalles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
				UltraGrid1.DataSource = Nothing
				btn_mostrar_bitacora.Enabled = False
				btn_solicita_transacciones.Enabled = False

				Imprimir = False
				cancelnav = False
				Me.WebBrowser1.Navigate("about:blank")
				Dim doc As HtmlDocument = Me.WebBrowser1.Document
				doc.Write(String.Empty)

				CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
				CapaPresentacion.Idiomas.cambiar_detalles(Me)
				CapaPresentacion.Common.moneda.FillUceMoneda(uce_moneda)
		End Sub

		Public Sub load_data(ByVal muestra As Boolean) '(ByVal datosBusqueda_paquete As dataBusqueda_paquete, ByVal datosBusqueda_vuelo As dataBusqueda_vuelo, ByVal datosBusqueda_auto As dataBusqueda_auto_test, ByVal datosBusqueda_hotel As dataBusqueda_hotel_test, ByVal datosBusqueda_actividad As dataBusqueda_actividad_test)
				muestra_header = muestra

				'If misForms.reservaciones IsNot Nothing AndAlso misForms.reservaciones.UltraGrid1.ActiveRow IsNot Nothing Then

				CapaLogicaNegocios.ReservationDetails.FIJA_LBL(Me, data_lbl_estado, "")
				CapaLogicaNegocios.ReservationDetails.FIJA_LBL(Me, data_lbl_origen, "")
				CapaLogicaNegocios.ReservationDetails.FIJA_LBL(Me, data_lbl_portal, "")
				CapaLogicaNegocios.ReservationDetails.FIJA_LBL(Me, data_lbl_rubro, "")

				If muestra_header Then
						btn_finalizar.Visible = False
				Else
						btn_finalizar.Visible = True
				End If

				If MI_THREAD IsNot Nothing Then MI_THREAD.Abort()
				CapaLogicaNegocios.ReservationDetails.FIJA_PIC(Me, PictureBox2, True)

				MI_THREAD = New System.Threading.Thread(AddressOf CapaLogicaNegocios.ReservationDetails.busca_paquete_async)
				MI_THREAD.Start(Me)
		End Sub

		Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
				If cmb_reservacion.SelectedItem IsNot Nothing AndAlso misForms.cancelar_reservacion(cmb_reservacion.SelectedItem.DataValue.ToString, datosBusqueda_vuelo, datosBusqueda_actividad, datosBusqueda_hotel, datosBusqueda_auto, datosBusqueda_paquete) Then
						Imprimir = False
						load_data(muestra_header)
				End If
		End Sub

		Private Sub btn_finalizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_finalizar.Click
				If misForms.newCall IsNot Nothing Then
						misForms.newCall.UltraButton2.PerformClick()
				Else
						CapaLogicaNegocios.Common.CloseAll()
				End If
		End Sub

		'Private Function politicas_fijas() As String
		'    Dim datos As String = ""

		'    datos += "<table width='800px'>"
		'    datos += "    <tr>"
		'    datos += "        <td>"
		'    datos += "              " + CapaPresentacion.Idiomas.get_str("str_370_politicas")
		'    datos += "        </td>"
		'    datos += "    </tr>"
		'    datos += "</table>"

		'    Return datos
		'End Function

		Private Sub btn_cambiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cambiar.Click
				Imprimir = False
				load_data(muestra_header)
		End Sub

		Private Sub uce_moneda_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uce_moneda.SelectionChanged
				moneda_selected = uce_moneda.SelectedItem.DataValue
		End Sub

		Private Sub btn_Imprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Imprimir.Click
				If PrintDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
						Imprimir = True
						TerminoImpr = New dTermino(AddressOf TerminoImpresion)
						load_data(muestra_header)
				End If
		End Sub

		Public Sub TerminoImpresion()
				WebBrowser1.Document.Title = ""

				'WebBrowser1.ShowPrintPreviewDialog()

				'WebBrowser1.ShowPrintDialog()

				WebBrowser1.Print()
				Imprimir = False
				load_data(muestra_header)
		End Sub

		Private Sub WebBrowser1_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted
				For Each h As HtmlElement In WebBrowser1.Document.Links
						AddHandler h.Click, AddressOf getClickedElement
				Next
		End Sub

		Private Sub getClickedElement(ByVal sender As Object, ByVal e As HtmlElementEventArgs)
				With WebBrowser1.Document.GetElementFromPoint(e.ClientMousePosition)
						Dim _ID As String = .GetAttribute("id").ToLower
						Dim _Rubro As String = .GetAttribute("name").ToLower
						Dim frm As New detalles()
						frm.MdiParent = misForms.form1
						_Rubro = _Rubro.ToUpper
						If _Rubro = "H" Then
								Dim temp As New dataBusqueda_hotel_test
								temp.ReservationId = _ID
								frm.datosBusqueda_hotel = temp
						End If
						If _Rubro = "C" Then
								Dim temp As New dataBusqueda_auto_test
								temp.IdReservation = _ID
								frm.datosBusqueda_auto = temp
						End If
						If _Rubro = "F" Then
								Dim temp As New dataBusqueda_vuelo
								temp.ReservationId = _ID
								frm.datosBusqueda_vuelo = temp
						End If
						If _Rubro = "A" Then
								Dim temp As New dataBusqueda_actividad_test
								temp.ReservationId = _ID
								frm.datosBusqueda_actividad = temp
						End If
						frm.load_data(True)
						frm.Show()
				End With
				cancelnav = True
		End Sub

		Private Sub WebBrowser1_Navigating(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserNavigatingEventArgs) Handles WebBrowser1.Navigating
				If cancelnav Then
						e.Cancel = True
						cancelnav = False
				End If

		End Sub

		Private Sub chkPorDia_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPorDia.CheckedChanged
				Imprimir = False
				load_data(muestra_header)
		End Sub

		Private Sub btn_mostrar_bitacora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_mostrar_bitacora.Click
				pnl_bitacora.Visible = True
				pnl_bitacora.Location = New Point((Me.Width / 2) - (pnl_bitacora.Width / 2), (Me.Height / 2) - (pnl_bitacora.Height / 2))
		End Sub

		Private Sub btn_ocultar_bitacora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ocultar_bitacora.Click
				pnl_bitacora.Visible = False
		End Sub

    Private Sub btn_ocultar_bitacora_detalles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ocultar_bitacora_detalles.Click
        pnl_bitacora.Visible = True
        pnl_bitacora_detalles.Visible = False
    End Sub

		Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
				CapaPresentacion.Idiomas.cambiar_detalles(Me)
		End Sub

    Private Sub btn_solicita_transacciones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_solicita_transacciones.Click
        CapaLogicaNegocios.ReservationDetails.pms_transaction_retry(datosBusqueda_hotel.ReservationId)
    End Sub


    Private Sub UltraGrid1_InitializeRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UltraGrid1.InitializeRow

        'Convertimos en link la columna de PMS
        With e.Row.Cells("DetailsTXT")
            .Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
            .Appearance.ForeColor = Color.Blue
            .Appearance.FontData.Underline = Infragistics.Win.DefaultableBoolean.True
            .Appearance.Cursor = Cursors.Hand
            .Appearance.TextHAlign = Infragistics.Win.HAlign.Center
        End With

    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)

        'Verificamos si se le dio clic a TransaccionPMS
        Dim element As Infragistics.Win.UIElement = Nothing
        element = UltraGrid1.DisplayLayout.UIElement.ElementFromPoint(e.Location)

        If element IsNot Nothing Then
            Dim cellUI As Infragistics.Win.UltraWinGrid.CellUIElement = element.GetAncestor(GetType(Infragistics.Win.UltraWinGrid.CellUIElement))
            If cellUI IsNot Nothing Then
                Dim cell As Infragistics.Win.UltraWinGrid.UltraGridCell = CType(cellUI.GetContext(GetType(Infragistics.Win.UltraWinGrid.UltraGridCell)), Infragistics.Win.UltraWinGrid.UltraGridCell)
                If cell IsNot Nothing Then
                    If cell.Column.Key = "DetailsTXT" Then

                        pnl_bitacora.Visible = False
                        pnl_bitacora_detalles.Visible = True
                        pnl_bitacora_detalles.Location = New Point((Me.Width / 2) - (pnl_bitacora_detalles.Width / 2), (Me.Height / 2) - (pnl_bitacora_detalles.Height / 2))

                        Dim ds As DataSet = CapaLogicaNegocios.ReservationDetails.log_getLogDetail(UltraGrid1.ActiveRow.Cells("idLog").Value)
                        UltraGrid2.DataSource = ds.Tables(0)
                        UltraGrid2.DataBind()
                        CapaPresentacion.Common.FormatUltraGrid(UltraGrid2)

                        UltraGrid2.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.AutoFixed

                        UltraGrid2.DisplayLayout.Override.RowSizingArea = Infragistics.Win.UltraWinGrid.RowSizingArea.EntireRow
                        UltraGrid2.DisplayLayout.Bands(0).Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.AutoFixed

                        UltraGrid2.DisplayLayout.Bands(0).Columns(0).Hidden = True
                        UltraGrid2.DisplayLayout.Bands(0).Columns(2).CellMultiLine = Infragistics.Win.DefaultableBoolean.True
                        UltraGrid2.DisplayLayout.Bands(0).Columns(3).CellMultiLine = Infragistics.Win.DefaultableBoolean.True
                        UltraGrid2.DisplayLayout.Bands(0).Columns(4).Hidden = True
                        UltraGrid2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                        UltraGrid2.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
                        UltraGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select


                        With UltraGrid2.DisplayLayout.Bands(0)
                            .Columns("DataColumn").Header.Caption = ""
                            .Columns("DataValueBefore").Header.Caption = CapaPresentacion.Idiomas.get_str("str_522")
                            .Columns("DataValueAfter").Header.Caption = CapaPresentacion.Idiomas.get_str("str_523")
                        End With


                    End If
                End If
            End If
        End If
    End Sub

    Private Sub UltraGrid2_InitializeRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UltraGrid2.InitializeRow
        If (e.Row.Cells("diff").Value.ToString().Trim() = "1") Then
            e.Row.Appearance.BackColor = Color.Navy
            e.Row.Appearance.BackColor2 = Color.RoyalBlue
            e.Row.Appearance.BackGradientStyle = Infragistics.Win.GradientStyle.GlassTop20

            e.Row.Appearance.ForeColor = Color.White
        End If
      
    End Sub
End Class