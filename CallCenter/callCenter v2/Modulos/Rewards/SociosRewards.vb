Imports System.Data.SqlClient

Public Class SociosRewards
    Private MI_THREAD As System.Threading.Thread
    ' Private termino_tread As Boolean = True

    Public Cliente As String
    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
    End Sub

    Public Sub load_data()
        UltraGrid1.DataSource = Nothing

        Me.Cursor = Cursors.WaitCursor
        PictureBox2.Visible = True
        Call CargaSocios()
        Me.PictureBox2.Visible = False
        Me.Cursor = Cursors.Default
       
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow
        UltraGrid1.ActiveRow = e.Row
        Dim frm As New SocioDetalleRewards
        frm.lblCodigo.Text = e.Row.Cells("referencenumber").Value
        'frm.load_data()
        frm.ShowDialog()
    End Sub

    Private Sub reservaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbPrograma.SelectedIndex = 0
    End Sub


    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        Me.load_data()
    End Sub

   

    Private Sub CargaSocios() '(ByVal param As Object)
        Try
            Dim idProg As Integer
            Select Case cmbPrograma.SelectedIndex
                Case 0
                    idProg = 2
                Case 1
                    idProg = 1
                Case 2
                    idProg = 3
            End Select

            Dim req As String = ""
            req &= "<BuscarSocioRQ_1_0>"
            req &= String.Format("    <Autentificacion Usuario='{0}' Contraseņa='{1}'/>", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER, CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            req &= "    <socios>"
            req &= "        <socio programa='" & idProg & "' filtro='" & txtNombre.Text & "'/>"
            req &= "    </socios>"
            req &= "</BuscarSocioRQ_1_0>"

            Dim ws As New wsRewards.wsRewards_1_0
            ws.Url = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER

            Dim r As Xml.XmlElement = ws.Procesa(req)

            Dim ds As New DataSet
            Dim xmlSR As System.IO.StringReader = New System.IO.StringReader(r.OuterXml)
            ds.ReadXml(xmlSR)

            Me.UltraGrid1.DataSource = ds.Tables("Respuesta")
            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns
                If col.Header.Caption.ToLower = "respuestas_id" Or col.Header.Caption.ToLower = "status" Then col.Hidden = True
                Select Case col.Header.Caption.ToLower
                    Case "referencenumber"
                        col.Header.Caption = "Referencia"
                    Case "nombre"
                        col.Header.Caption = "Nombre"
                    Case "apellido"
                        col.Header.Caption = "Apellido"
                    Case "email"
                        col.Header.Caption = "Correo Electronico"
                    Case "saldo"
                        col.Header.Caption = "Saldo de Puntos"
                End Select

                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Me.UltraGrid1.DataSource = New DataTable
        End Try
    End Sub

    
    Private Sub UltraGrid1_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout

    End Sub
End Class