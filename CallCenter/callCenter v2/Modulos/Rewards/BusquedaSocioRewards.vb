Imports System.Data.SqlClient

Public Class BusquedaSocioRewards
    Private MI_THREAD As System.Threading.Thread
    Public SocioSeleccionado As String
    Public SocioSeleccionadoNombre As String
    Private Sub BusquedaSocio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
    End Sub

    Public Sub load_data()

        UltraGrid1.DataSource = Nothing
        If Me.txtNombre.Text.Trim <> "" Then
            Me.Cursor = Cursors.WaitCursor
            PictureBox2.Visible = True
            Call CargaSocios()
            Me.PictureBox2.Visible = False
            Me.Cursor = Cursors.Default
        Else
            MsgBox("Proporcione la informacion a buscar")
        End If
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow
        UltraGrid1.ActiveRow = e.Row
        SocioSeleccionado = e.Row.Cells("idSocioPrograma").Value.ToString.Trim
        SocioSeleccionadoNombre = e.Row.Cells("Nombre").Value.ToString.Trim & " " & e.Row.Cells("Apellido").Value.ToString.Trim
        Close()
        'misForms.exe_accion(ContextMenuStrip1, "btn_detalles")
    End Sub

    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        Me.load_data()
    End Sub
    

    Private Sub CargaSocios() '(ByVal param As Object)
        Try
            Dim req As String = ""
            req &= "<BuscarSocioRQ_1_0>"
            req &= String.Format("    <Autentificacion Usuario='{0}' Contraseņa='{1}'/>", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER, CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            req &= "    <socios>"
            req &= "        <socio programa='-1' filtro='" & txtNombre.Text & "'/>"
            req &= "    </socios>"
            req &= "</BuscarSocioRQ_1_0>"

            Dim ws As New wsRewards.wsRewards_1_0
            ws.Url = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER

            Dim r As Xml.XmlElement = ws.Procesa(req)

            Dim ds As New DataSet
            Dim xmlSR As System.IO.StringReader = New System.IO.StringReader(r.OuterXml)
            ds.ReadXml(xmlSR)

            ds.Tables("Respuesta").DefaultView.RowFilter = "status=1"
            Me.UltraGrid1.DataSource = ds.Tables("Respuesta").DefaultView.ToTable
            Me.UltraGrid1.DisplayLayout.Bands(0).Columns("status").Hidden = True
            Me.UltraGrid1.DisplayLayout.Bands(0).Columns("idSocioPrograma").Header.Caption = "Codigo"
            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns

                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Me.UltraGrid1.DataSource = New DataTable
        End Try
        'FIJA_PIC(frm, frm.PictureBox2, False)

    End Sub

    Delegate Sub FIJA_PIC_Callback(ByVal frm As SociosRewards, ByVal pic As PictureBox, ByVal vis As Boolean)
    Private Shared Sub FIJA_PIC(ByVal frm As SociosRewards, ByVal pic As PictureBox, ByVal vis As Boolean)
        Try
            If pic.InvokeRequired Then
                Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                frm.Invoke(d, New Object() {frm, pic, vis})
            Else
                pic.Visible = vis
            End If
        Catch ex As Exception
            'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub txtNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.load_data()
        End If
    End Sub

    Private Sub txtNombre_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.ValueChanged

    End Sub
End Class