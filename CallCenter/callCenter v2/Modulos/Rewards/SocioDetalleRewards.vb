Imports System.Data.SqlClient

Public Class SocioDetalleRewards
    Private MI_THREAD As System.Threading.Thread
    Public SocioSeleccionado As String
    Public SocioSeleccionadoNombre As String
    Private Sub BusquedaSocio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
    End Sub


    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
    End Sub

    Public Sub load_data()

        UltraGrid1.DataSource = Nothing
        If Me.lblCodigo.Text.Trim <> "" Then
            Me.Cursor = Cursors.WaitCursor
            PictureBox2.Visible = True
            Call CargaSocio()
            Me.PictureBox2.Visible = False
            Me.Cursor = Cursors.Default
        Else
            MsgBox("Proporcione la informacion a buscar")
        End If
    End Sub

    Private Sub CargaSocio() '(ByVal param As Object)
        Try
            Dim req As String = ""
            req &= "<EstadoCuentaRQ_1_0>"
            req &= String.Format("    <Autentificacion Usuario='{0}' Contraseņa='{1}'/>", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER, CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            req &= "    <Socios>"
            req &= "        <Socio idSocioPrograma='" & lblCodigo.Text & "' desplegardetalle='1'/>"
            req &= "    </Socios>"
            req &= "</EstadoCuentaRQ_1_0>"

            Dim ws As New wsRewards.wsRewards_1_0
            ws.Url = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER

            Dim r As Xml.XmlElement = ws.Procesa(req)

            Dim ds As New DataSet
            Dim xmlSR As System.IO.StringReader = New System.IO.StringReader(r.OuterXml)
            ds.ReadXml(xmlSR)

            If Not ds.Tables("Respuesta") Is Nothing AndAlso ds.Tables("Respuesta").Rows.Count > 0 Then
                lblNombre.Text = ds.Tables("Respuesta").Rows(0).Item("nombre")
                lblCorreo.Text = ds.Tables("Respuesta").Rows(0).Item("correo")
                If Not ds.Tables("Transaccion") Is Nothing AndAlso ds.Tables("Transaccion").Rows.Count > 0 Then
                    Me.UltraGrid1.DataSource = ds.Tables("Transaccion")
                    UltraGrid1.DisplayLayout.Bands(0).Columns("transacciones_id").Hidden = True
                    For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns
                        col.Header.Caption = StrConv(col.Header.Caption, VbStrConv.ProperCase)
                        col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
                    Next
                End If
            Else
                Me.UltraGrid1.DataSource = Nothing
            End If


        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Me.UltraGrid1.DataSource = New DataTable
        End Try
        'FIJA_PIC(frm, frm.PictureBox2, False)

    End Sub

    Delegate Sub FIJA_PIC_Callback(ByVal frm As SociosRewards, ByVal pic As PictureBox, ByVal vis As Boolean)
    Private Shared Sub FIJA_PIC(ByVal frm As SociosRewards, ByVal pic As PictureBox, ByVal vis As Boolean)
        Try
            If pic.InvokeRequired Then
                Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                frm.Invoke(d, New Object() {frm, pic, vis})
            Else
                pic.Visible = vis
            End If
        Catch ex As Exception
            'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub


    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        Close()
    End Sub
End Class