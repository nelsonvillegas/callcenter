Imports System.Data.SqlClient

Public Class AltaSociosRewards
    Dim strConn As String
    Private MI_THREAD As System.Threading.Thread
    Public UltimoCreado As String
    Public UltimoCreadoNombre As String

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub


    Private Sub CargaProcedencias(ByVal idPrograma As Integer, ByVal cmb As ComboBox)

        Try
            Dim ws As New wsRewards.wsRewards_1_0
            ws.Url = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER
            Dim Req As String = ""
            req &= ""
            req &= "<ListadoProcedenciasRQ_1_0>"
            req &= String.Format("    <Autentificacion Usuario='{0}' Contrase�a='{1}'/>", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER, CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            req &= "    <prodedencias>"
            req &= "        <procedencia programa='" & idPrograma & "' /> "
            req &= "    </prodedencias>"
            Req &= "</ListadoProcedenciasRQ_1_0>"

            Dim r As Xml.XmlElement = ws.Procesa(req)

            cmb.Items.Clear()
            For Each p As Xml.XmlNode In r.SelectNodes("//Respuesta")
                cmb.Items.Add(New ListData(p.Attributes("descripcion").Value, p.Attributes("codigo").Value))
            Next
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)

        End Try
    End Sub

    Private Sub CargaEstados()
        Try
            cmbEstados.Items.Clear()
            cmbEstados.Items.Add(New ListData("Aguascalientes", "AG"))
            cmbEstados.Items.Add(New ListData("Baja California", "BJ"))
            cmbEstados.Items.Add(New ListData("Baja California Sur", "BS"))
            cmbEstados.Items.Add(New ListData("Campeche", "CM"))
            cmbEstados.Items.Add(New ListData("Chiapas", "CS"))
            cmbEstados.Items.Add(New ListData("Chihuahua", "CH"))
            cmbEstados.Items.Add(New ListData("Coahuila", "CU"))
            cmbEstados.Items.Add(New ListData("Colima", "CL"))
            cmbEstados.Items.Add(New ListData("Distrito Federal", "DF"))
            cmbEstados.Items.Add(New ListData("Durango", "DU"))
            cmbEstados.Items.Add(New ListData("Estado de M�xico", "MX"))
            cmbEstados.Items.Add(New ListData("Guanajuato", "GJ"))
            cmbEstados.Items.Add(New ListData("Guerrero", "GU"))
            cmbEstados.Items.Add(New ListData("Hidalgo", "HO"))
            cmbEstados.Items.Add(New ListData("Jalisco", "JA"))
            cmbEstados.Items.Add(New ListData("Michoac�n", "MC"))
            cmbEstados.Items.Add(New ListData("Morelos", "MR"))
            cmbEstados.Items.Add(New ListData("Nayarit", "NA"))
            cmbEstados.Items.Add(New ListData("Nuevo Le�n", "NL"))
            cmbEstados.Items.Add(New ListData("Oaxaca", "OX"))
            cmbEstados.Items.Add(New ListData("Puebla", "PB"))
            cmbEstados.Items.Add(New ListData("Quer�taro", "QU"))
            cmbEstados.Items.Add(New ListData("Quintana Roo", "QR"))
            cmbEstados.Items.Add(New ListData("San Luis Potos�", "SP"))
            cmbEstados.Items.Add(New ListData("Sinaloa", "SI"))
            cmbEstados.Items.Add(New ListData("Sonora", "SO"))
            cmbEstados.Items.Add(New ListData("Tabasco", "TA"))
            cmbEstados.Items.Add(New ListData("Tamaulipas", "TM"))
            cmbEstados.Items.Add(New ListData("Tlaxcala", "TL"))
            cmbEstados.Items.Add(New ListData("Veracruz", "VL"))
            cmbEstados.Items.Add(New ListData("Yucat�n", "YN"))
            cmbEstados.Items.Add(New ListData("Zacatecas", "ZC"))
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)

        End Try
    End Sub

    Private Sub CargaEstados(idPais As String)
        cmbEstados.Items.Clear()
        For Each dr As DataRow In herramientas.get_Estados(idPais).Tables(0).Rows
            cmbEstados.Items.Add(New ListData(dr("Nombre"), dr("idEstado")))
        Next
        cmbEstados.SelectedIndex = 0
    End Sub

    Private Sub CargaPaises(Optional idPaisSelected As String = "")
        cmbPais.Items.Clear()
        For Each dr As DataRow In herramientas.get_Paises.Tables(0).Rows
            cmbPais.Items.Add(New ListData(dr("Nombre"), dr("idPais")))
            If dr("idPais") = idPaisSelected Then cmbPais.SelectedIndex = cmbPais.Items.Count - 1
        Next

    End Sub

    Private Function GuardarSocio() As Boolean
        Try
            Dim idPrograma As Integer = 0
            Dim Empresa As String = ""
            Dim Apellidos As String = "."
            Dim idProcedencia As String = ""
            Dim Puesto As String = ""
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    idPrograma = 2
                    Empresa = txtAgencia.Text.Trim
                    idProcedencia = cmbProcedenciasAg.SelectedItem._value
                    Puesto = txtPuestoAg.Text.Trim
                Case "clientes"
                    idPrograma = 1
                    Apellidos = txtApellidos.Text.Trim
                    idProcedencia = cmbProcedenciasCl.SelectedItem._value
                Case "professionals"
                    idPrograma = 3
                    Empresa = txtEmpresa.Text.Trim
                    Apellidos = txtApellidosProf.Text.Trim
                    idProcedencia = cmbProcedenciasPr.SelectedItem._value
                    Puesto = txtPuesto.Text.Trim
            End Select

            Dim req As String = ""
            req &= "<RegistroSocioRQ_1_0>"
            req &= String.Format("    <Autentificacion Usuario='{0}' Contrase�a='{1}'/>", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER, CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            req &= "    <socios>"
            req &= "        <socio nombre='{0}' apellido='{1}' direccion='{2}' telefono='{3}' email='{4}' clave='{5}' cumplea�os='{6}' procedencia='{7}' enviarcorreo='1' programa='{8}' empresa='{9}' puesto='{10}' codestado='{11}' ciudad='{12}' comoseenterodelprograma='{13}' conquien='{14}' quienatendio='{15}' comentarios='{16}' fax='{17}' celular='{18}' pais='{19}' />"
            req &= "    </socios>"
            req &= "</RegistroSocioRQ_1_0>"

            req = req.Replace("{0}", txtNombre.Text)
            req = req.Replace("{1}", Apellidos)
            req = req.Replace("{2}", txtDireccion.Text.Trim)
            req = req.Replace("{3}", txtTelefono.Text.Trim)
            req = req.Replace("{4}", txtEmail.Text.Trim)
            req = req.Replace("{5}", txtPassword1.Text)
            req = req.Replace("{6}", dtpFecha.Value.ToString("yyyyMMdd"))
            req = req.Replace("{7}", idProcedencia)
            req = req.Replace("{8}", idPrograma)
            req = req.Replace("{9}", Empresa)
            req = req.Replace("{10}", Puesto)
            req = req.Replace("{11}", cmbEstados.SelectedItem._value)
            req = req.Replace("{12}", txtCiudad.Text.Trim)
            req = req.Replace("{13}", cmbComoSeEntero.Text)
            req = req.Replace("{14}", txtCualseentero.Text)
            req = req.Replace("{15}", txtQuienatendio.Text)
            req = req.Replace("{16}", "Ingresado por Callcenter")
            req = req.Replace("{17}", txtFax.Text.Trim)
            req = req.Replace("{18}", txtCelular.Text.Trim)
            req = req.Replace("{19}", cmbPais.SelectedItem._value)

            Dim ws As New wsRewards.wsRewards_1_0
            ws.Url = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER
            Dim r As Xml.XmlElement = ws.Procesa(req)
            If r.SelectNodes("//Respuesta").Item(0).Attributes("Status").Value = "1" Then
                MsgBox("Registrado Correctamente" & vbCrLf & "El numero de Socio asignado es :" & r.SelectNodes("//Respuesta").Item(0).Attributes("idSocioPrograma").Value)
                Close()
            Else
                MsgBox("No se pudo registrar socio" & vbCrLf & r.SelectNodes("//Respuesta").Item(0).Attributes("Error").Value)
            End If

            Return True
        Catch ex As Exception
            'CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Function ExisteSocio(Optional ByVal Empresa As Boolean = False) As Boolean
        Try
            Dim idPrograma As Integer = 0
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    idPrograma = 2
                Case "clientes"
                    idPrograma = 1
                Case "professionals"
                    idPrograma = 3
            End Select
            Dim ws As New wsRewards.wsRewards_1_0
            ws.Url = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER
            Dim Req As String = ""
            Req &= ""
            Req &= "<BuscarSocioRQ_1_0>"
            Req &= String.Format("    <Autentificacion Usuario='{0}' Contrase�a='{1}'/>", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER, CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            Req &= "    <Socios>"
            If idPrograma = 2 Then
                Req &= "        <Socio programa='" & idPrograma & "' filtro='" & txtAgencia.Text & "' /> "
            Else
                Req &= "        <Socio programa='" & idPrograma & "' filtro='" & txtNombre.Text & "' /> "
            End If

            Req &= "    </Socios>"
            Req &= "</BuscarSocioRQ_1_0>"

            Dim r As Xml.XmlElement = ws.Procesa(Req)
            If r.SelectNodes("//Respuesta").Item(0).Attributes("Status").Value = "1" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)

            Return True
        End Try
    End Function

    Private Sub AltaSocios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UltimoCreado = ""

        'If TestConnection() Then
        cmbProcedenciasPr.DisplayMember = "Name"
        cmbProcedenciasPr.ValueMember = "_Value"
        cmbProcedenciasCl.DisplayMember = "Name"
        cmbProcedenciasCl.ValueMember = "_Value"
        cmbProcedenciasAg.DisplayMember = "Name"
        cmbProcedenciasAg.ValueMember = "_Value"
        cmbEstados.DisplayMember = "Name"
        cmbEstados.ValueMember = "_Value"
        cmbPais.DisplayMember = "Name"
        cmbPais.ValueMember = "_Value"
        Call CargaPaises("MX")
        Call CargaProcedencias(3, cmbProcedenciasPr)
        Call CargaProcedencias(2, cmbProcedenciasAg)
        Call CargaProcedencias(1, cmbProcedenciasCl)
        Call CargaEstados()
        If cmbProcedenciasPr.Items.Count > 0 Then cmbProcedenciasPr.SelectedIndex = 0
        If cmbProcedenciasCl.Items.Count > 0 Then cmbProcedenciasCl.SelectedIndex = 0
        If cmbProcedenciasAg.Items.Count > 0 Then cmbProcedenciasAg.SelectedIndex = 0
        If cmbEstados.Items.Count > 0 Then cmbEstados.SelectedIndex = 0
        cmbTipoSocio.SelectedIndex = 0
        cmbTipoSocioP.SelectedIndex = 0
        cmbComoSeEntero.SelectedIndex = 0
        Call Habilita()
        'Else
        'Me.btn_registrar.Enabled = False
        'End If

    End Sub

    Private Sub btn_registrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_registrar.Click
        If Valida() Then
            Dim guardar As Boolean = False
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    If Not ExisteSocio() And Not ExisteSocio(True) Then guardar = True
                Case "clientes"
                    If Not ExisteSocio() Then guardar = True
                Case "professionals"
                    If Not ExisteSocio() And Not ExisteSocio(True) Then guardar = True
            End Select
            If guardar Then
                Call GuardarSocio()
            Else
                MsgBox("Ya existe el socio en el programa seleccionado")
            End If
        End If
    End Sub

    Private Function Valida() As Boolean
        If txtNombre.Text.Trim = "" Then
            MsgBox("Nombre del Socio es requerido")
            Return False
        End If
        If txtDireccion.Text.Trim = "" Then
            MsgBox("Direccion es requerida")
            Return False
        End If
        If txtCiudad.Text.Trim = "" Then
            MsgBox("Ciudad es requerida")
            Return False
        End If
        If txtCodigoPostal.Text.Trim = "" Then
            MsgBox("Codigo Postal es requerido")
            Return False
        End If
        If txtTelefono.Text.Trim = "" Then
            MsgBox("Telefono es requerido")
            Return False
        End If
        If txtEmail.Text.Trim = "" Then
            MsgBox("Email es requerido")
            Return False
        End If
        If txtPassword.Text.Trim = "" Then
            MsgBox("Password es requerido")
            Return False
        End If
        If txtPassword.Text.Trim <> txtPassword1.Text Then
            MsgBox("El Password y la confirmacion no coinciden")
            Return False
        End If
        Select Case Tabs.SelectedTab.Key
            Case "agencias"
                If txtAgencia.Text.Trim = "" Then
                    MsgBox("Nombre de Agencia es requerida")
                    Return False
                End If
                If txtPuestoAg.Text.Trim = "" Then
                    MsgBox("Puesto es requerido")
                    Return False
                End If
            Case "clientes"
                If txtApellidos.Text.Trim = "" Then
                    MsgBox("Apellidos es requerido")
                    Return False
                End If
            Case "professionals"
                If txtApellidosProf.Text.Trim = "" Then
                    MsgBox("Apellidos es requerido")
                    Return False
                End If
                If txtEmpresa.Text.Trim = "" Then
                    MsgBox("Empresa es requerida")
                    Return False
                End If
                If txtPuesto.Text.Trim = "" Then
                    MsgBox("Puesto es requerido")
                    Return False
                End If
        End Select
        Return True
    End Function

    Private Sub Habilita()
        txtNombre.Enabled = True
        txtApellidos.Enabled = True
        txtApellidosProf.Enabled = True
        txtAgencia.Enabled = True
        txtEmpresa.Enabled = True
        txtColonia.Enabled = True
        txtPuesto.Enabled = True
        txtPuestoAg.Enabled = True
        txtDireccion.Enabled = True
        txtCiudad.Enabled = True
        txtCodigoPostal.Enabled = True
        txtTelefono.Enabled = True
        txtFax.Enabled = True
        txtEmail.Enabled = True
        txtCualseentero.Enabled = True
        txtQuienatendio.Enabled = True
        txtCelular.Enabled = True
        txtPassword.Enabled = True
        txtPassword1.Enabled = True
        txtRFC.Enabled = True
        txtRazonSocial.Enabled = True
    End Sub


    Private Sub cmbPais_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbPais.SelectedIndexChanged
        CargaEstados(cmbPais.SelectedItem._value)
    End Sub


End Class
