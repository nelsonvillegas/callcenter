<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CallOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CallOption))
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Me.btn_NewCall = New Infragistics.Win.Misc.UltraButton
        Me.lbl_WhatToDo = New System.Windows.Forms.Label
        Me.btn_BookWithNoCall = New Infragistics.Win.Misc.UltraButton
        Me.btn_cancel = New Infragistics.Win.Misc.UltraButton
        Me.SuspendLayout()
        '
        'btn_NewCall
        '
        Me.btn_NewCall.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance1.BackColor = System.Drawing.Color.Transparent
        Appearance1.Image = CType(resources.GetObject("Appearance1.Image"), Object)
        Appearance1.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance1.TextHAlignAsString = "Left"
        Me.btn_NewCall.Appearance = Appearance1
        Me.btn_NewCall.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_NewCall.ImageSize = New System.Drawing.Size(48, 48)
        Me.btn_NewCall.Location = New System.Drawing.Point(63, 53)
        Me.btn_NewCall.Name = "btn_NewCall"
        Me.btn_NewCall.Size = New System.Drawing.Size(171, 54)
        Me.btn_NewCall.TabIndex = 2
        Me.btn_NewCall.Text = "Nueva llamada"
        Me.btn_NewCall.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'lbl_WhatToDo
        '
        Me.lbl_WhatToDo.AutoSize = True
        Me.lbl_WhatToDo.Location = New System.Drawing.Point(12, 9)
        Me.lbl_WhatToDo.Name = "lbl_WhatToDo"
        Me.lbl_WhatToDo.Size = New System.Drawing.Size(267, 13)
        Me.lbl_WhatToDo.TabIndex = 3
        Me.lbl_WhatToDo.Text = "No hay sesi�n de llamada iniciada. �Que desea hacer?"
        '
        'btn_BookWithNoCall
        '
        Me.btn_BookWithNoCall.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Appearance2.Image = CType(resources.GetObject("Appearance2.Image"), Object)
        Appearance2.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance2.TextHAlignAsString = "Left"
        Me.btn_BookWithNoCall.Appearance = Appearance2
        Me.btn_BookWithNoCall.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_BookWithNoCall.ImageSize = New System.Drawing.Size(48, 48)
        Me.btn_BookWithNoCall.Location = New System.Drawing.Point(63, 113)
        Me.btn_BookWithNoCall.Name = "btn_BookWithNoCall"
        Me.btn_BookWithNoCall.Size = New System.Drawing.Size(171, 54)
        Me.btn_BookWithNoCall.TabIndex = 4
        Me.btn_BookWithNoCall.Text = "Reservar sin llamada"
        Me.btn_BookWithNoCall.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'btn_cancel
        '
        Me.btn_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance9.BackColor = System.Drawing.Color.Transparent
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance9.TextHAlignAsString = "Left"
        Me.btn_cancel.Appearance = Appearance9
        Me.btn_cancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_cancel.ImageSize = New System.Drawing.Size(48, 48)
        Me.btn_cancel.Location = New System.Drawing.Point(63, 173)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(171, 54)
        Me.btn_cancel.TabIndex = 5
        Me.btn_cancel.Text = "Cancelar"
        Me.btn_cancel.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'CallOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(294, 241)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_cancel)
        Me.Controls.Add(Me.btn_BookWithNoCall)
        Me.Controls.Add(Me.lbl_WhatToDo)
        Me.Controls.Add(Me.btn_NewCall)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CallOption"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Call Option"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_NewCall As Infragistics.Win.Misc.UltraButton
    Friend WithEvents lbl_WhatToDo As System.Windows.Forms.Label
    Friend WithEvents btn_BookWithNoCall As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_cancel As Infragistics.Win.Misc.UltraButton
End Class
