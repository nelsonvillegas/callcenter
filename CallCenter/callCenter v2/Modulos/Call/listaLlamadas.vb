Imports System.Data.SqlClient

Public Class listaLlamadas

    Public botones() As String

    Private Sub los_TXT_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_reservaciones.TextChanged, txt_duracion.TextChanged, txt_comentarios.TextChanged, txt_nombre.TextChanged, txtOperador.TextChanged
        update_filtros()
    End Sub

    Public Function load_data() As Boolean
        Try
            Dim f1 As String
            Dim f2 As String
            If dtp_ini.Checked Then f1 = dtp_ini.Value.ToString(CapaPresentacion.Common.DateDataFormat) Else f1 = Nothing
            If dtp_fin.Checked Then f2 = dtp_fin.Value.ToString(CapaPresentacion.Common.DateDataFormat) Else f2 = Nothing

            Dim wsCall As New webService_call()
            Dim dt As DataTable = wsCall.get_calls(f1, f2)

            For Each r As DataRow In dt.Rows
                If r.IsNull("ayuda") Then Continue For

                Select Case r.Item("ayuda")
                    Case 1
                        r.Item("ayuda2") = CapaPresentacion.Idiomas.get_str("str_0216_reservar")
                    Case 2
                        r.Item("ayuda2") = CapaPresentacion.Idiomas.get_str("str_0217_verDisponibilidad")
                    Case 3
                        r.Item("ayuda2") = CapaPresentacion.Idiomas.get_str("str_0218_solicitarInfo")
                    Case 4
                        r.Item("ayuda2") = CapaPresentacion.Idiomas.get_str("str_0219_otro")
                    Case Else
                        r.Item("ayuda2") = ""
                End Select
                r.Item("sexo2") = IIf(r.Item("sexo") = "M", CapaPresentacion.Idiomas.get_str("str_0208_masculino"), CapaPresentacion.Idiomas.get_str("str_0209_femenino"))
            Next

            UltraGrid1.DataSource = dt

            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns
                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next

            UltraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn

            Return True
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0013", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)

            Me.Cursor = Cursors.Arrow
            UltraGrid1.DataSource = Nothing

            Return False
        End Try
    End Function

    Private Sub update_filtros()
        Dim dv As DataView
        Try
            dv = CType(UltraGrid1.DataSource, DataTable).DefaultView
        Catch ex As Exception
            dv = UltraGrid1.DataSource
        End Try

        Dim filtros As New List(Of String)
        If txt_nombre.Text <> "" Then filtros.Add("nombreCliente like '*" + txt_nombre.Text + "*'")
        If txt_duracion.Text <> "" Then filtros.Add("duracion2 like '*" + txt_duracion.Text + "*'")
        If txt_reservaciones.Text <> "" Then filtros.Add("reservaciones like '*" + txt_reservaciones.Text + "*'")
        If txt_comentarios.Text <> "" Then filtros.Add("comentarios like '*" + txt_comentarios.Text + "*'")
        If txtOperador.Text <> "" Then filtros.Add("operador like '*" + txtOperador.Text + "*'")
        dv.RowFilter = ""
        For Each f As String In filtros
            If dv.RowFilter = "" Then dv.RowFilter += f Else dv.RowFilter += " and " + f
        Next

        UltraGrid1.DataSource = dv
    End Sub

    Private Sub clear_filtros()
        txt_nombre.Clear()
        txt_duracion.Clear()
        txt_reservaciones.Clear()
        txt_comentarios.Clear()
        txtOperador.Clear()
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        ''temp 
        ''If Not misForms.form1.UltraDockManager1.PaneFromKey("detallesReservacion") Is Nothing Then
        ''    CType(CType(misForms.form1.UltraDockManager1.PaneFromKey("detallesReservacion"), Infragistics.Win.UltraWinDock.DockableControlPane).Control, uc_detallesReservacion).load_data()
        ''End If
        ''temp

        ''''''''''''''''''''''''''''''''''''''
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_editarLlamada").SharedProps.Enabled = True
        '''''''''''''''''''''''''''''''''

        'CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = True
        'CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = True

        'If UltraGrid1.ActiveRow.Cells("cancelado").Value.ToString = "Cancelada" Then
        '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = False
        '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = False
        'Else
        '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = True
        '    CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = True
        'End If

        menuContextual.inicializar_items(Me.ContextMenuStrip1, Me.botones)

        ''''''' temp
        'For Each dc As dataCliente In calls_list
        '    If dc.idCall = UltraGrid1.ActiveRow.Cells("idCall").Value Then
        '        datos_cliente = dc
        '    End If
        'Next
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow
        UltraGrid1.ActiveRow = e.Row
        misForms.exe_accion(ContextMenuStrip1, "btn_editarLlamada")
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown
        'herramientas.selRow(UltraGrid1, e)
    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        'If e.KeyCode = Keys.Enter Then
        '    misForms.exe_accion(ContextMenuStrip1, "btn_editarLlamada")
        'End If
    End Sub

    Private Sub listaLlamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        misForms.listaLlamadas = Me

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_listaLlamadas").SharedProps.Enabled = False

        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        'UltraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect

        botones = New String() {"btn_editarLlamada"} ' {"btn_detalles", "btn_modificar", "btn_cancelar"}

        load_data()
        CapaPresentacion.Idiomas.cambiar_listaLlamadas(Me)
    End Sub

    Private Sub listaLlamadas_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        misForms.listaLlamadas = Nothing

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_listaLlamadas").SharedProps.Enabled = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_editarLlamada").SharedProps.Enabled = False
    End Sub

    Private Sub listaLlamadas_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        'txt_cliente.Focus()
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        ContextMenuStrip1.Visible = False
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub btn_buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_buscar.Click
        Me.load_data()
        update_filtros()
        'CapaPresentacion.Idiomas.cambiar_listaLlamadas(Me)
    End Sub

    Private Sub btn_limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_limpiar.Click
        clear_filtros()
        Me.load_data()
        'CapaPresentacion.Idiomas.cambiar_listaLlamadas(Me)
    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_fin.ValueChanged, dtp_ini.ValueChanged
        herramientas.fechas_inteligentes(dtp_ini, dtp_fin, sender, 0, Nothing)
    End Sub

    Private Sub txt_nombre_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_nombre.ValueChanged

    End Sub

    Private Sub txtOperador_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOperador.ValueChanged

    End Sub
End Class