Public Class newCall

    Public WithEvents datos_call As New dataCall_
    Public modificar As Boolean = False
    Private hora_ini As DateTime
    Private MI_THREAD As Threading.Thread
    Public ReservationOption As CallOption.CallResult = CallOption.CallResult.Cancel
    Private selecciono_reservar As Boolean = False
    Private JoinThreadConfirmar As Boolean = False
    Dim comentariosSinReservar As String
    Dim hotelSinReservar As String
    Dim reservo As Boolean

    Public Sub inicializa_controles()
        Dim idCall As Integer = misForms.listaLlamadas.UltraGrid1.ActiveRow.Cells("idCall").Value

        Dim wsCall As New webService_call()
        Dim dc As dataCall_ = wsCall.get_call(idCall, UltraGrid1, btn_hotel, btn_vuelos, btn_actividades, btn_autos)

        txt_nombre.Text = dc.nombre
        If dc.masculino Then rbt_masculino.Checked = True Else rbt_femenino.Checked = True
        If dc.desea = 1 Then rbt_reservar.Checked = True
        If dc.desea = 2 Then rbt_disponibilidad.Checked = True
        If dc.desea = 3 Then rbt_info.Checked = True
        If dc.desea = 4 Then rbt_otro.Checked = True
        txt_telefono.Text = dc.telefono
        txt_email.Text = dc.correo
        txt_comentarios.Text = dc.comentarios
        Dim ts As TimeSpan = dc.fechaTermina - dc.fechaInicio
        lbl_horas.Text = ts.Hours.ToString("00") + ":" + ts.Minutes.ToString("00") + ":" + ts.Seconds.ToString("00")

        UltraButton1.Enabled = False


    End Sub

    Private Sub update_controls()
        lbl_saludos.Text = CapaPresentacion.Idiomas.get_str("str_311_tipoAyuda") + ":"
        datos_call.nombre = txt_nombre.Text + " " + txt_apellido.Text
        datos_call.masculino = rbt_masculino.Checked
        datos_call.telefono = txt_telefono.Text
        datos_call.correo = txt_email.Text
        datos_call.comentarios = txt_comentarios.Text
        datos_call.id_MedioPromocion = cmbMedios.Value
        If rbt_reservar.Checked Then datos_call.desea = 1
        If rbt_disponibilidad.Checked Then datos_call.desea = 2
        If rbt_info.Checked Then datos_call.desea = 3
        If rbt_otro.Checked Then datos_call.desea = 4
    End Sub

    Private Sub habilita_controles(ByVal enab As Boolean)
        For Each ctl As Control In ugb_contacto.Controls
            If ctl.Name = "UltraButton1" Then Continue For
            If ctl.Name = "lbl_horas" Then Continue For
            If ctl.Name = "PictureBox2" Then Continue For

            If ctl.Name = "btn_hotel" AndAlso Not CONST_ENABLED_HOTELES Then Continue For
            If ctl.Name = "btn_vuelos" AndAlso Not CONST_ENABLED_VUELOS Then Continue For
            If ctl.Name = "btn_actividades" AndAlso Not CONST_ENABLED_ACTIVIDADES Then Continue For
            If ctl.Name = "btn_autos" AndAlso Not CONST_ENABLED_AUTOS Then Continue For

            FIJA_CTL(ctl, enab)
        Next

    End Sub

    Delegate Sub FIJA_CTL_Callback(ByVal ctl As Control, ByVal enab As Boolean)
    Private Sub FIJA_CTL(ByVal ctl As Control, ByVal enab As Boolean)
        Try
            If ctl.InvokeRequired Then
                Dim d As New FIJA_CTL_Callback(AddressOf FIJA_CTL)
                Me.Invoke(d, New Object() {ctl, enab})
            Else
                ctl.Enabled = enab
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_LBL_Callback(ByVal lbl As Label, ByVal txt As String)
    Private Sub FIJA_LBL(ByVal lbl As Label, ByVal txt As String)
        Try
            If lbl.InvokeRequired Then
                Dim d As New FIJA_LBL_Callback(AddressOf FIJA_LBL)
                Me.Invoke(d, New Object() {lbl, txt})
            Else
                lbl.Text = txt
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_PIC_Callback(ByVal ctl As Control, ByVal vis As Boolean)
    Private Sub FIJA_PIC(ByVal ctl As Control, ByVal vis As Boolean)
        Try
            If ctl.InvokeRequired Then
                Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                Me.Invoke(d, New Object() {ctl, vis})
            Else
                ctl.Visible = vis
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_FRM_Callback(ByVal ctl As Control, ByVal noConfirm As String, ByVal Rubro As enumRubrosPassport)
    Private Sub FIJA_FRM(ByVal ctl As Control, ByVal noConfirm As String, ByVal Rubro As enumRubrosPassport)
        Try
            If ctl.InvokeRequired Then
                Dim d As New FIJA_FRM_Callback(AddressOf FIJA_FRM)
                Me.Invoke(d, New Object() {ctl, noConfirm, Rubro})
            Else
                'herramientas.ConfirmNumberToDetailsForm(noConfirm, DateTime.Today.AddDays(-1))
                

                Select Case Rubro
                    Case enumRubrosPassport.Actividades
                        Dim frm As New detalles()
                        frm.MdiParent = misForms.form1
                        frm.datosBusqueda_actividad = New dataBusqueda_actividad_test()
                        frm.datosBusqueda_actividad.ReservationID = noConfirm
                        frm.load_data(False)
                        frm.Show()
                        'Case enumRubrosPassport.Autos
                        '    frm.datosBusqueda_auto = New dataBusqueda_auto_test()
                        '    frm.datosBusqueda_auto.IdReservation = noConfirm
                        '    frm.load_data(False)
                        '    frm.Show()
                    Case enumRubrosPassport.Hotel
                        If noConfirm.Contains(",") Then
                            For Each nores As String In noConfirm.Split(",")
                                Dim frm As New detalles()
                                frm.MdiParent = misForms.form1
                                frm.registraBitacora = detalles.BitacoraAccion.Insertar
                                frm.datosBusqueda_hotel = New dataBusqueda_hotel_test()
                                frm.datosBusqueda_hotel.ReservationId = nores 'noConfirm
                                frm.load_data(False)
                                frm.Show()
                            Next
                        Else
                            herramientas.ConfirmNumberToDetailsForm(noConfirm, DateTime.Today.AddDays(-1))
                        End If
                        'Case enumRubrosPassport.Vuelos
                        '    frm.datosBusqueda_vuelo = New dataBusqueda_vuelo
                        '    frm.datosBusqueda_vuelo.ReservationId = noConfirm
                        '    frm.load_data(False)
                        '    frm.Show()
                    Case Else
                        herramientas.ConfirmNumberToDetailsForm(noConfirm, DateTime.Today.AddDays(-1))
                End Select
                
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_END_Callback(ByVal frm As Form)
    Private Sub FIJA_END(ByVal frm As Form)
        Try
            If frm.InvokeRequired Then
                Dim d As New FIJA_END_Callback(AddressOf FIJA_END)
                Me.Invoke(d, New Object() {frm})
            Else
                finaliza_controles()
                ' CapaLogicaNegocios.Common.CloseAll("newCall")
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Delegate Sub FIJA_LNG_Callback(ByVal frm As Form)
    Private Sub FIJA_LNG(ByVal frm As Form)
        Try
            If frm.InvokeRequired Then
                Dim d As New FIJA_LNG_Callback(AddressOf FIJA_LNG)
                Me.Invoke(d, New Object() {frm})
            Else
                CapaPresentacion.Idiomas.cambiar_newCall(frm)
            End If
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Function tiene_noConfirm() As Boolean
        For Each data As dataBusqueda_hotel_test In datos_call.reservacion_paquete.lista_Hoteles
            If Not data.confirmada Then
                Return True
            End If
        Next

        For Each data As dataBusqueda_auto_test In datos_call.reservacion_paquete.lista_Autos
            If Not data.confirmada Then
                Return True
            End If
        Next

        For Each data As dataBusqueda_actividad_test In datos_call.reservacion_paquete.lista_Actividades
            If Not data.confirmada Then
                Return True
            End If
        Next

        For Each data As dataBusqueda_vuelo In datos_call.reservacion_paquete.lista_Vuelos
            If Not data.confirmada Then
                Return True
            End If
        Next

        Return False
    End Function

    Private Sub rem_noConfirm()
        While tiene_noConfirm()
            Dim idTOrem As Integer = -1

            For Each data As dataBusqueda_hotel_test In datos_call.reservacion_paquete.lista_Hoteles
                If Not data.confirmada Then
                    idTOrem = data.t_id
                End If
            Next
            If idTOrem <> -1 Then datos_call.rem_rvaHotel(idTOrem)
            idTOrem = -1

            For Each data As dataBusqueda_auto_test In datos_call.reservacion_paquete.lista_Autos
                If Not data.confirmada Then
                    idTOrem = data.t_id
                End If
            Next
            If idTOrem <> -1 Then datos_call.rem_rvaAuto(idTOrem)
            idTOrem = -1

            For Each data As dataBusqueda_actividad_test In datos_call.reservacion_paquete.lista_Actividades
                If Not data.confirmada Then
                    idTOrem = data.t_id
                End If
            Next
            If idTOrem <> -1 Then datos_call.rem_rvaActividad(idTOrem)
            idTOrem = -1

            For Each data As dataBusqueda_vuelo In datos_call.reservacion_paquete.lista_Vuelos
                If Not data.confirmada Then
                    idTOrem = data.t_id
                End If
            Next
            If idTOrem <> -1 Then datos_call.rem_rvaVuelo(idTOrem)
            idTOrem = -1
        End While
    End Sub

    Private Function finaliza_llamada() As Boolean
        If tiene_noConfirm() Then
            If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0242_faltanConfirmar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                rem_noConfirm()
            Else
                Return False
            End If
        End If
        If Not String.IsNullOrEmpty(comentariosSinReservar) Then
            datos_call.comentarios = String.Format("Comentario final:{0};{1}", comentariosSinReservar, datos_call.comentarios)
            datos_call.nombreHotel = String.Format("Nombre del Hotel:{0};{1}", hotelSinReservar, datos_call.nombreHotel)
        End If

        datos_call.fechaTermina = Now
        Timer1.Enabled = False
        If Not datos_call.iniciada Then Return True

        Dim wsCall As New webService_call()
        Dim res As Boolean = wsCall.finalizar(datos_call)

        If res Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub finaliza_controles()
        UltraGrid1.DataSource = Nothing
        datos_call.vacia()
        datos_call = New dataCall_
        datos_call.grid = UltraGrid1
        datos_call.btnHot = btn_hotel
        datos_call.btnVue = btn_vuelos
        datos_call.btnAut = btn_autos
        datos_call.btnAct = btn_actividades
        update_controls()

        txt_nombre.Text = ""
        txt_apellido.Text = ""
        rbt_masculino.Checked = True
        rbt_reservar.Checked = True
        txt_telefono.Text = ""
        txt_email.Text = ""
        txt_comentarios.Text = ""
        Timer1.Enabled = False
        lbl_horas.Text = "00:00:00"

        If cmbMedios.Items.Count > 0 Then cmbMedios.SelectedIndex = 0
        habilita_controles(False)
        UltraButton1.Enabled = True
        UltraButton1.Focus()

        ReservationOption = CallOption.CallResult.Cancel
    End Sub

    Private Sub confirma()
        datos_call.confirmando = True
        'Threading.Thread.Sleep(1000 * 60 * 15)

        habilita_controles(False)
        FIJA_PIC(PictureBox2, True)

        Dim wsCall As New webService_call()
        Dim noConfirm As String = wsCall.addReservations(datos_call)

        If noConfirm IsNot Nothing Then
            FIJA_LBL(lbl_msg, CapaPresentacion.Idiomas.get_str("str_0233_exitoReservaciones"))
        Else
            FIJA_LBL(lbl_msg, "")
        End If

        Try
            FIJA_PIC(PictureBox2, False)
        Catch ex As Exception
        End Try

        Try
            Select Case ReservationOption
                Case CallOption.CallResult.BookNoCall
                    PrepareBookNoCall()
                Case CallOption.CallResult.NewCall
                    habilita_controles(True)
            End Select
        Catch ex As Exception
        End Try

        If noConfirm IsNot Nothing Then
            datos_call.confirma_rvas()
            datos_call.confirmando = False
            reservo = True
            MsgBox(CapaPresentacion.Idiomas.get_str("str_420_Reservation"), MsgBoxStyle.Information)
            'FIJA_FRM(btn_confirmar, noConfirm)

            ''
            datos_call.reservacion_paquete.lista_Actividades.Clear()
            datos_call.reservacion_paquete.lista_Autos.Clear()
            datos_call.reservacion_paquete.lista_Hoteles.Clear()
            datos_call.reservacion_paquete.lista_Vuelos.Clear()
            ''

            MI_THREAD = New Threading.Thread(AddressOf fija_formulario)
            MI_THREAD.Start(New Object() {btn_confirmar, noConfirm, datos_call.reservacion_paquete.Rubro})
        Else
            '
            datos_call.confirmando = False
        End If

    End Sub

    Private Sub fija_formulario(ByVal parm As Object)
        FIJA_FRM(parm(0), parm(1), parm(2))
        FIJA_LBL(lbl_msg, "")

        datos_call.confirmando = False
    End Sub

    Private Function TieneNoConfirmadas()
        'esta funcion es para ver si hay reservaciones por confirmar

        Dim tiene_otro As Boolean = False

        'hoteles
        For Each datos_busqueda As dataBusqueda_hotel_test In datos_call.reservacion_paquete.lista_Hoteles
            If datos_busqueda.confirmada Then Continue For
            tiene_otro = True
        Next

        'actividades
        For Each datos_busqueda As dataBusqueda_actividad_test In datos_call.reservacion_paquete.lista_Actividades
            If datos_busqueda.confirmada Then Continue For
            tiene_otro = True
        Next

        'autos
        For Each datos_busqueda As dataBusqueda_auto_test In datos_call.reservacion_paquete.lista_Autos
            If datos_busqueda.confirmada Then Continue For
            tiene_otro = True
        Next

        'vuelos
        For Each datos_busqueda As dataBusqueda_vuelo In datos_call.reservacion_paquete.lista_Vuelos
            If datos_busqueda.confirmada Then Continue For
            tiene_otro = True
        Next

        Return tiene_otro
    End Function

    Private Function TieneFechasAtrasadasPaquetes()
        'por regla, los paquetes deben de reservarse almenos 1 semana en adelante
        'por eso se verifica si es un paquete(mas de una reservacion) (por que los no paquetes
        'pueden ser de un dia en adelante, incluso el mismo dia, dependiendo del rubro) y si
        'la fecha de alguna de todas las reservaciones no es valida regresa TRUE (true de que
        'si tiene fechas atrazadas o invalidas par apaquete) de lo contrario FALSE (que
        'no hay fechas incorrectar)

        Dim Num_NoConf As Integer = 0
        Dim FechaMinim As DateTime = New DateTime(Now.Year, Now.Month, Now.Day).AddDays(7)
        Dim EsFechaMin As Boolean = True

        'hoteles
        For Each datos_busqueda As dataBusqueda_hotel_test In datos_call.reservacion_paquete.lista_Hoteles
            If datos_busqueda.confirmada Then Continue For
            Num_NoConf += 1
            If datos_busqueda.llegada < FechaMinim Then EsFechaMin = False
            Exit For
        Next

        'actividades
        For Each datos_busqueda As dataBusqueda_actividad_test In datos_call.reservacion_paquete.lista_Actividades
            If datos_busqueda.confirmada Then Continue For
            Num_NoConf += 1

            Dim parse_from As String = ""

            If datos_busqueda.DayReservar.Length = 8 Then
                parse_from += CapaPresentacion.Common.DateDataFormat
            Else
                parse_from += "yyyyMMddHHmm"
            End If

            If DateTime.ParseExact(datos_busqueda.DayReservar, parse_from, Nothing) < FechaMinim Then EsFechaMin = False
            Exit For
        Next

        'autos
        For Each datos_busqueda As dataBusqueda_auto_test In datos_call.reservacion_paquete.lista_Autos
            If datos_busqueda.confirmada Then Continue For
            Num_NoConf += 1
            If datos_busqueda.fechaRecoger < FechaMinim Then EsFechaMin = False
            Exit For
        Next

        'vuelos
        For Each datos_busqueda As dataBusqueda_vuelo In datos_call.reservacion_paquete.lista_Vuelos
            If datos_busqueda.confirmada Then Continue For
            Num_NoConf += 1
            If datos_busqueda.partidaFecha < FechaMinim Then EsFechaMin = False
            Exit For
        Next

        If Num_NoConf > 1 And Not EsFechaMin Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub PrepareBookNoCall()
        FIJA_CTL(UltraGrid1, True)
        FIJA_CTL(btn_confirmar, True)
        FIJA_CTL(btn_cancelar, True)
        FIJA_CTL(btn_modificar, True)
        FIJA_CTL(UltraButton1, False)
        FIJA_CTL(UltraButton2, True)

        'Dim tab As Infragistics.Win.UltraWinTabbedMdi.MdiTab = misForms.form1.UltraTabbedMdiManager1.TabFromForm(Me)
        'misForms.form1.UltraTabbedMdiManager1.TabGroups(tab.TabGroup.Index).SelectedTab = tab
    End Sub

    '----

    Private Sub datos_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_nombre.ValueChanged, rbt_masculino.CheckedChanged, txt_telefono.ValueChanged, txt_email.ValueChanged, txt_comentarios.ValueChanged, txt_apellido.ValueChanged, cmbMedios.ValueChanged
        update_controls()
    End Sub

    Private Sub losRBT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbt_otro.CheckedChanged, rbt_disponibilidad.CheckedChanged, rbt_info.CheckedChanged, rbt_reservar.CheckedChanged
        update_controls()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim ts As TimeSpan = (DateTime.Now - hora_ini)
        lbl_horas.Text = Format(ts.Hours, "00") + ":" + Format(ts.Minutes, "00") + ":" + Format(ts.Seconds, "00")
    End Sub

    Private Sub newCall_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If Not modificar Then CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_nuevaLlamada").SharedProps.Enabled = True
        If Not modificar Then misForms.newCall = Nothing
    End Sub

    Private Sub newCall_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not datos_call.iniciada Then Exit Sub

        If datos_call.confirmando Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_404_NoCancelWhileConfirm"))
            e.Cancel = True
        Else
            e.Cancel = Not finaliza_llamada()
        End If
    End Sub

    Private Sub newCall_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        misForms.newCall = Me
        If dataOperador.MostrarCatalogos Then
            lblMedioPromocion.Visible = True
            cmbMedios.Visible = True
            CargaMedios()
        End If

        If Not modificar Then CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_nuevaLlamada").SharedProps.Enabled = False

        CapaPresentacion.Idiomas.cambiar_newCall(Me)
        'lbl_saludos.Text = CapaPresentacion.Idiomas.get_str("str_311_tipoAyuda")
        datos_call.grid = UltraGrid1
        datos_call.btnHot = btn_hotel
        datos_call.btnVue = btn_vuelos
        datos_call.btnAut = btn_autos
        datos_call.btnAct = btn_actividades

        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        'UltraGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None
        'UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect

        lbl_fecha.Text = ""
        lbl_msg.Text = ""
    End Sub

    Private Sub UltraButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraButton1.Click
        Dim ws As New webService_call()
        Dim idCall As Integer = ws.iniciar

        If idCall = -1 Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0239_errIniCall"), CapaPresentacion.Idiomas.get_str("str_0154_error"), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Timer1.Enabled = True
        hora_ini = DateTime.Now
        habilita_controles(True)
        txt_nombre.Focus()
        UltraButton1.Enabled = False
        UltraButton2.Enabled = True

        datos_call.idCall = idCall
        datos_call.fechaInicio = Now

        reservo = False
    End Sub

    Private Sub UltraButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraButton2.Click

        If Not reservo AndAlso datos_call.reservacion_paquete.lista_Hoteles.Count = 0 Then
            Dim frm As frm_fin_llamada = New frm_fin_llamada
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                comentariosSinReservar = frm.result
                hotelSinReservar = frm.hotelName
            Else
                Return
            End If
        End If


        Dim ThreadFinalizar As New Threading.Thread(AddressOf finalizar)
        ThreadFinalizar.Start()
    End Sub

    Private Sub finalizar()
        Dim reservar As Boolean = False
        For Each item As dataBusqueda_hotel_test In datos_call.reservacion_paquete.lista_Hoteles
            If Not item.confirmada Then reservar = True
        Next
        For Each item As dataBusqueda_vuelo In datos_call.reservacion_paquete.lista_Vuelos
            If Not item.confirmada Then reservar = True
        Next
        For Each item As dataBusqueda_actividad_test In datos_call.reservacion_paquete.lista_Actividades
            If Not item.confirmada Then reservar = True
        Next
        For Each item As dataBusqueda_auto_test In datos_call.reservacion_paquete.lista_Autos
            If Not item.confirmada Then reservar = True
        Next
        selecciono_reservar = False
        JoinThreadConfirmar = False
        If reservar Then confirmar()

        If JoinThreadConfirmar Then
            MI_THREAD.Join()
            JoinThreadConfirmar = False
        End If

        If selecciono_reservar OrElse Not reservar Then
            If datos_call.confirmando = False AndAlso Not finaliza_llamada() Then  'If ReservationOption = CallOption.CallResult.NewCall AndAlso Not finaliza_llamada() Then
                'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_312_errFinLlamada"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                FIJA_END(Me)
            End If
        End If
    End Sub

    Private Sub btn_hotel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hotel.Click
        misForms.exe_accion(Nothing, "btn_buscar")
        misForms.form1.UltraToolbarsManager1.Ribbon.SelectedTab = misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon2")
    End Sub

    Private Sub btn_vuelos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_vuelos.Click
        misForms.exe_accion(Nothing, "btn_buscar_vuelo")
        misForms.form1.UltraToolbarsManager1.Ribbon.SelectedTab = misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon5")
    End Sub

    Private Sub btn_actividades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actividades.Click
        misForms.exe_accion(Nothing, "btn_buscarActividad")
        misForms.form1.UltraToolbarsManager1.Ribbon.SelectedTab = misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon6")
    End Sub

    Private Sub btn_autos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_autos.Click
        misForms.exe_accion(Nothing, "btn_buscarAuto")
        misForms.form1.UltraToolbarsManager1.Ribbon.SelectedTab = misForms.form1.UltraToolbarsManager1.Ribbon.Tabs("ribbon7")
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        If UltraGrid1.ActiveRow IsNot Nothing Then
            If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0163_seguroCancel"), CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                Dim t As Integer = UltraGrid1.ActiveRow.Cells("id").Value

                Select Case UltraGrid1.ActiveRow.Cells("tipo").Value
                    Case "Hotel"
                        datos_call.rem_rvaHotel(t)
                    Case "Vuelo"
                        datos_call.rem_rvaVuelo(t)
                    Case "Actividad"
                        datos_call.rem_rvaActividad(t)
                    Case "Auto"
                        datos_call.rem_rvaAuto(t)
                End Select
            End If
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0271_errAgResCancelar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub btn_confirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_confirmar.Click
        confirmar(True)
    End Sub

    Private Sub confirmar(Optional ByVal PreguntarMantenerInfo As Boolean = False)
        If Not TieneNoConfirmadas() Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_313_errNoRes"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If TieneFechasAtrasadasPaquetes() Then
            Dim str As String = CapaPresentacion.Idiomas.get_str("str_314_errFechasPack") + " " + New DateTime(Now.Year, Now.Month, Now.Day).AddDays(7).ToString("dd MMMM yyyy")
            MessageBox.Show(str, CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If UltraGrid1.ActiveRow Is Nothing Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0272_errAgResConfirmar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Dim frm As New datosCliente
        frm.Preguntar = PreguntarMantenerInfo

        If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            selecciono_reservar = False
            JoinThreadConfirmar = False
            Return
        End If

        selecciono_reservar = True
        JoinThreadConfirmar = True

        MI_THREAD = New Threading.Thread(AddressOf confirma)
        MI_THREAD.Start()
    End Sub

    Private Sub btn_modificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_modificar.Click
        'autos no tienen parametros para modificar

        If UltraGrid1.ActiveRow Is Nothing Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0273_errAgResModificar"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If UltraGrid1.ActiveRow.Cells("Tipo").Value = "Auto" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0277_errResAutosParam"), CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Dim dataH As dataBusqueda_hotel_test = Nothing
        Dim dataF As dataBusqueda_vuelo = Nothing
        Dim dataA As dataBusqueda_actividad_test = Nothing
        'Dim dataC As dataBusqueda_auto_test = Nothing
        Dim t_id As Integer = UltraGrid1.ActiveRow.Cells("id").Value

        If UltraGrid1.ActiveRow IsNot Nothing Then
            dataH = datos_call.get_dataBusquedaHotel(t_id)
            dataF = datos_call.get_dataBusquedaVuelo(t_id)
            dataA = datos_call.get_dataBusquedaActividad(t_id)
            'dataC = datos_call.get_dataBusquedaAuto(t_id)
        End If

        If Not dataH Is Nothing Then
            Dim frm As New reservar()
            frm.datosBusqueda_hotel = dataH.Clone
            frm.tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado
            'checar eso del nothing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            If frm.cargar(Nothing) Then frm.ShowDialog()
        End If

        If Not dataF Is Nothing Then
            Dim frm As New ReservarVuelo
            frm.datosBusqueda_vuelo = dataF.Clone
            frm.tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado
            If frm.cargar() Then frm.ShowDialog()
        End If

        If Not dataA Is Nothing Then
            Dim frm2 As New reservarActividad
            frm2.datosBusqueda_actividad = dataA.Clone
            frm2.tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado
            If frm2.cargar() Then frm2.ShowDialog()
        End If

        'If Not dataC Is Nothing Then
        '    Dim frm2 As New reservarAuto
        '    frm2.datos_call = datos_call
        '    frm2.datosBusqueda_auto = dataC.clone
        '    frm2.tipoAccion = enumReservar_TipoAccion.modificarNoConfirmado
        '    If frm2.cargar() Then frm2.ShowDialog()
        'End If

    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Dim t As DateTime = DateTime.Now
        lbl_fecha.Text = t.ToString("dd/MMM/yyyy    hh:mm:ss")
    End Sub

    Private Sub losTXT_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_nombre.KeyUp, txt_apellido.KeyUp
        herramientas.capitaliza(txt_nombre)
        herramientas.capitaliza(txt_apellido)
    End Sub

    Private Sub DatosCall_After_UpdateGrid() Handles datos_call.After_UpdateGrid
        FIJA_LNG(Me)
    End Sub

    Private Sub btn_hotel_EnabledChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_vuelos.EnabledChanged, btn_hotel.EnabledChanged, btn_autos.EnabledChanged, btn_actividades.EnabledChanged
        CType(sender, Infragistics.Win.Misc.UltraButton).Visible = CType(sender, Infragistics.Win.Misc.UltraButton).Enabled
    End Sub

    'Private Sub newCall_ChangeUICues(ByVal sender As System.Object, ByVal e As System.Windows.Forms.UICuesEventArgs) Handles MyBase.ChangeUICues
    '    MsgBox("newCall_ChangeUICues")
    'End Sub

    'Private Sub newCall_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
    '    MsgBox("newCall_Activated")
    'End Sub

    'Private Sub newCall_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Enter
    '    MsgBox("newCall_Enter")
    'End Sub

    'Private Sub newCall_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
    '    MsgBox("newCall_Paint")
    'End Sub

    'Private Sub newCall_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
    '    MsgBox("newCall_Shown")
    'End Sub

    'Private Sub newCall_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Validated
    '    MsgBox("newCall_Validated")
    'End Sub

    'Private Sub newCall_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.VisibleChanged
    '    MsgBox("newCall_VisibleChanged")
    'End Sub

    Private Sub CargaMedios()
        cmbMedios.Items.Clear()
        'cmbMedios.DisplayMember = "Name"
        'cmbMedios.ValueMember = "_Value"
        cmbMedios.Items.Add("0", CapaPresentacion.Idiomas.get_str("str_0108_ninguno")) '(New ListData(CapaPresentacion.Idiomas.get_str("str_0108_ninguno"), ""))
        Dim ds As DataSet = herramientas.get_MediosPromocion(dataOperador.idCorporativo)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                cmbMedios.Items.Add(dr("id_MedioPromocion"), dr("Nombre"))
            Next
        End If
        cmbMedios.SelectedIndex = 0
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        If UltraGrid1.ActiveRow.Cells(1).Text = "Actividad" Then
            btn_modificar.Enabled = False
        Else
            btn_modificar.Enabled = True
        End If
    End Sub
End Class