<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class newCall
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(newCall))
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Me.ugb_contacto = New Infragistics.Win.Misc.UltraGroupBox
        Me.lbl_Apellidos = New System.Windows.Forms.Label
        Me.cmbMedios = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        Me.lblMedioPromocion = New System.Windows.Forms.Label
        Me.lbl_msg = New System.Windows.Forms.Label
        Me.txt_apellido = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_fecha = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.btn_confirmar = New Infragistics.Win.Misc.UltraButton
        Me.UltraButton2 = New Infragistics.Win.Misc.UltraButton
        Me.btn_modificar = New Infragistics.Win.Misc.UltraButton
        Me.btn_cancelar = New Infragistics.Win.Misc.UltraButton
        Me.lbl_revasActuales = New System.Windows.Forms.Label
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.txt_comentarios = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_comentarios = New System.Windows.Forms.Label
        Me.btn_hotel = New Infragistics.Win.Misc.UltraButton
        Me.btn_vuelos = New Infragistics.Win.Misc.UltraButton
        Me.btn_actividades = New Infragistics.Win.Misc.UltraButton
        Me.btn_autos = New Infragistics.Win.Misc.UltraButton
        Me.UltraButton1 = New Infragistics.Win.Misc.UltraButton
        Me.lbl_horas = New System.Windows.Forms.Label
        Me.txt_email = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_email = New System.Windows.Forms.Label
        Me.txt_telefono = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_telefono = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.rbt_masculino = New System.Windows.Forms.RadioButton
        Me.rbt_femenino = New System.Windows.Forms.RadioButton
        Me.rbt_otro = New System.Windows.Forms.RadioButton
        Me.rbt_info = New System.Windows.Forms.RadioButton
        Me.rbt_disponibilidad = New System.Windows.Forms.RadioButton
        Me.rbt_reservar = New System.Windows.Forms.RadioButton
        Me.lbl_saludos = New System.Windows.Forms.Label
        Me.txt_nombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_nombre = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.ugb_contacto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugb_contacto.SuspendLayout()
        CType(Me.cmbMedios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_apellido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_comentarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_email, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_telefono, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txt_nombre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ugb_contacto
        '
        Me.ugb_contacto.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ugb_contacto.Controls.Add(Me.lbl_Apellidos)
        Me.ugb_contacto.Controls.Add(Me.cmbMedios)
        Me.ugb_contacto.Controls.Add(Me.lblMedioPromocion)
        Me.ugb_contacto.Controls.Add(Me.lbl_msg)
        Me.ugb_contacto.Controls.Add(Me.txt_apellido)
        Me.ugb_contacto.Controls.Add(Me.lbl_fecha)
        Me.ugb_contacto.Controls.Add(Me.PictureBox2)
        Me.ugb_contacto.Controls.Add(Me.btn_confirmar)
        Me.ugb_contacto.Controls.Add(Me.UltraButton2)
        Me.ugb_contacto.Controls.Add(Me.btn_modificar)
        Me.ugb_contacto.Controls.Add(Me.btn_cancelar)
        Me.ugb_contacto.Controls.Add(Me.lbl_revasActuales)
        Me.ugb_contacto.Controls.Add(Me.UltraGrid1)
        Me.ugb_contacto.Controls.Add(Me.txt_comentarios)
        Me.ugb_contacto.Controls.Add(Me.lbl_comentarios)
        Me.ugb_contacto.Controls.Add(Me.btn_hotel)
        Me.ugb_contacto.Controls.Add(Me.btn_vuelos)
        Me.ugb_contacto.Controls.Add(Me.btn_actividades)
        Me.ugb_contacto.Controls.Add(Me.btn_autos)
        Me.ugb_contacto.Controls.Add(Me.UltraButton1)
        Me.ugb_contacto.Controls.Add(Me.lbl_horas)
        Me.ugb_contacto.Controls.Add(Me.txt_email)
        Me.ugb_contacto.Controls.Add(Me.lbl_email)
        Me.ugb_contacto.Controls.Add(Me.txt_telefono)
        Me.ugb_contacto.Controls.Add(Me.lbl_telefono)
        Me.ugb_contacto.Controls.Add(Me.Panel1)
        Me.ugb_contacto.Controls.Add(Me.rbt_otro)
        Me.ugb_contacto.Controls.Add(Me.rbt_info)
        Me.ugb_contacto.Controls.Add(Me.rbt_disponibilidad)
        Me.ugb_contacto.Controls.Add(Me.rbt_reservar)
        Me.ugb_contacto.Controls.Add(Me.lbl_saludos)
        Me.ugb_contacto.Controls.Add(Me.txt_nombre)
        Me.ugb_contacto.Controls.Add(Me.lbl_nombre)
        Me.ugb_contacto.Location = New System.Drawing.Point(44, 38)
        Me.ugb_contacto.Name = "ugb_contacto"
        Me.ugb_contacto.Size = New System.Drawing.Size(891, 502)
        Me.ugb_contacto.TabIndex = 0
        Me.ugb_contacto.Text = "Informaci�n de contacto"
        Me.ugb_contacto.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'lbl_Apellidos
        '
        Me.lbl_Apellidos.AutoSize = True
        Me.lbl_Apellidos.BackColor = System.Drawing.Color.Transparent
        Me.lbl_Apellidos.Enabled = False
        Me.lbl_Apellidos.Location = New System.Drawing.Point(34, 144)
        Me.lbl_Apellidos.Name = "lbl_Apellidos"
        Me.lbl_Apellidos.Size = New System.Drawing.Size(61, 13)
        Me.lbl_Apellidos.TabIndex = 49
        Me.lbl_Apellidos.Text = "�Apellidos?"
        '
        'cmbMedios
        '
        Me.cmbMedios.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.cmbMedios.Enabled = False
        Me.cmbMedios.Location = New System.Drawing.Point(146, 463)
        Me.cmbMedios.Name = "cmbMedios"
        Me.cmbMedios.Size = New System.Drawing.Size(255, 21)
        Me.cmbMedios.TabIndex = 12
        Me.cmbMedios.Visible = False
        '
        'lblMedioPromocion
        '
        Me.lblMedioPromocion.AutoSize = True
        Me.lblMedioPromocion.BackColor = System.Drawing.Color.Transparent
        Me.lblMedioPromocion.Enabled = False
        Me.lblMedioPromocion.Location = New System.Drawing.Point(38, 467)
        Me.lblMedioPromocion.Name = "lblMedioPromocion"
        Me.lblMedioPromocion.Size = New System.Drawing.Size(107, 13)
        Me.lblMedioPromocion.TabIndex = 48
        Me.lblMedioPromocion.Text = "Medio de Promocion:"
        Me.lblMedioPromocion.Visible = False
        '
        'lbl_msg
        '
        Me.lbl_msg.AutoSize = True
        Me.lbl_msg.BackColor = System.Drawing.Color.Transparent
        Me.lbl_msg.ForeColor = System.Drawing.Color.Green
        Me.lbl_msg.Location = New System.Drawing.Point(440, 117)
        Me.lbl_msg.Name = "lbl_msg"
        Me.lbl_msg.Size = New System.Drawing.Size(94, 13)
        Me.lbl_msg.TabIndex = 22
        Me.lbl_msg.Text = "-----------------------------"
        '
        'txt_apellido
        '
        Me.txt_apellido.AlwaysInEditMode = True
        Me.txt_apellido.Enabled = False
        Me.txt_apellido.Location = New System.Drawing.Point(146, 141)
        Me.txt_apellido.Name = "txt_apellido"
        Me.txt_apellido.Size = New System.Drawing.Size(148, 21)
        Me.txt_apellido.TabIndex = 3
        '
        'lbl_fecha
        '
        Me.lbl_fecha.AutoSize = True
        Me.lbl_fecha.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fecha.Enabled = False
        Me.lbl_fecha.Location = New System.Drawing.Point(761, 1)
        Me.lbl_fecha.Name = "lbl_fecha"
        Me.lbl_fecha.Size = New System.Drawing.Size(82, 13)
        Me.lbl_fecha.TabIndex = 21
        Me.lbl_fecha.Text = "-------------------------"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.status_anim
        Me.PictureBox2.Location = New System.Drawing.Point(37, 77)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(45, 11)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'btn_confirmar
        '
        Me.btn_confirmar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance8.BackColor = System.Drawing.Color.Transparent
        Appearance8.Image = CType(resources.GetObject("Appearance8.Image"), Object)
        Appearance8.ImageHAlign = Infragistics.Win.HAlign.Left
        Me.btn_confirmar.Appearance = Appearance8
        Me.btn_confirmar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button
        Me.btn_confirmar.Enabled = False
        Me.btn_confirmar.Location = New System.Drawing.Point(599, 160)
        Me.btn_confirmar.Name = "btn_confirmar"
        Me.btn_confirmar.Size = New System.Drawing.Size(89, 26)
        Me.btn_confirmar.TabIndex = 12
        Me.btn_confirmar.Text = "Confirmar"
        Me.btn_confirmar.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'UltraButton2
        '
        Me.UltraButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance7.BackColor = System.Drawing.Color.Transparent
        Appearance7.Image = CType(resources.GetObject("Appearance7.Image"), Object)
        Appearance7.ImageHAlign = Infragistics.Win.HAlign.Right
        Me.UltraButton2.Appearance = Appearance7
        Me.UltraButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.UltraButton2.Enabled = False
        Me.UltraButton2.ImageSize = New System.Drawing.Size(48, 48)
        Me.UltraButton2.Location = New System.Drawing.Point(742, 30)
        Me.UltraButton2.Name = "UltraButton2"
        Me.UltraButton2.Size = New System.Drawing.Size(112, 54)
        Me.UltraButton2.TabIndex = 20
        Me.UltraButton2.Text = "Finalizar"
        Me.UltraButton2.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'btn_modificar
        '
        Me.btn_modificar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance5.BackColor = System.Drawing.Color.Transparent
        Appearance5.Image = CType(resources.GetObject("Appearance5.Image"), Object)
        Appearance5.ImageHAlign = Infragistics.Win.HAlign.Left
        Me.btn_modificar.Appearance = Appearance5
        Me.btn_modificar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button
        Me.btn_modificar.Enabled = False
        Me.btn_modificar.Location = New System.Drawing.Point(777, 160)
        Me.btn_modificar.Name = "btn_modificar"
        Me.btn_modificar.Size = New System.Drawing.Size(77, 26)
        Me.btn_modificar.TabIndex = 14
        Me.btn_modificar.Text = "Modificar"
        Me.btn_modificar.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance10.BackColor = System.Drawing.Color.Transparent
        Appearance10.Image = CType(resources.GetObject("Appearance10.Image"), Object)
        Appearance10.ImageHAlign = Infragistics.Win.HAlign.Left
        Me.btn_cancelar.Appearance = Appearance10
        Me.btn_cancelar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.VisualStudio2005Button
        Me.btn_cancelar.Enabled = False
        Me.btn_cancelar.Location = New System.Drawing.Point(694, 160)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(77, 26)
        Me.btn_cancelar.TabIndex = 13
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'lbl_revasActuales
        '
        Me.lbl_revasActuales.AutoSize = True
        Me.lbl_revasActuales.BackColor = System.Drawing.Color.Transparent
        Me.lbl_revasActuales.Enabled = False
        Me.lbl_revasActuales.Location = New System.Drawing.Point(437, 171)
        Me.lbl_revasActuales.Name = "lbl_revasActuales"
        Me.lbl_revasActuales.Size = New System.Drawing.Size(121, 13)
        Me.lbl_revasActuales.TabIndex = 19
        Me.lbl_revasActuales.Text = "Reservaciones actuales"
        '
        'UltraGrid1
        '
        Appearance30.BackColor = System.Drawing.Color.White
        Appearance30.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance30
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance31.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance31.ImageBackground = CType(resources.GetObject("Appearance31.ImageBackground"), System.Drawing.Image)
        Appearance31.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance31.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance31.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance31
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance28.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance28
        Me.UltraGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance29.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance29.FontData.Name = "Trebuchet MS"
        Appearance29.FontData.SizeInPoints = 9.0!
        Appearance29.ForeColor = System.Drawing.Color.White
        Appearance29.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance29
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance32.FontData.BoldAsString = "True"
        Appearance32.FontData.Name = "Trebuchet MS"
        Appearance32.FontData.SizeInPoints = 10.0!
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance32
        Appearance33.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance33
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance34.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance34.BackColor2 = System.Drawing.SystemColors.Control
        Appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance34.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance34
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance13.BackColor = System.Drawing.SystemColors.Window
        Appearance13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance13
        Appearance14.BackColor = System.Drawing.SystemColors.Highlight
        Appearance14.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance14
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance15.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance15
        Appearance16.BorderColor = System.Drawing.Color.Transparent
        Appearance16.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance16
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance17
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance21.BackColor = System.Drawing.SystemColors.Control
        Appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance21.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance21
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance22.FontData.BoldAsString = "True"
        Appearance22.FontData.Name = "Trebuchet MS"
        Appearance22.FontData.SizeInPoints = 10.0!
        Appearance22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance22.ImageBackground = CType(resources.GetObject("Appearance22.ImageBackground"), System.Drawing.Image)
        Appearance22.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance22.TextHAlignAsString = "Left"
        Appearance22.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance22
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance1
        Appearance23.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance24
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance25.BorderColor = System.Drawing.Color.Transparent
        Appearance25.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance25
        Appearance26.BorderColor = System.Drawing.Color.Transparent
        Appearance26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance26.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance26
        Appearance27.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance27
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance35.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance37.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance38
        Appearance39.ImageBackground = CType(resources.GetObject("Appearance39.ImageBackground"), System.Drawing.Image)
        Appearance39.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance39.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance39
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Enabled = False
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(440, 187)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(414, 202)
        Me.UltraGrid1.TabIndex = 15
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'txt_comentarios
        '
        Me.txt_comentarios.AlwaysInEditMode = True
        Me.txt_comentarios.Enabled = False
        Me.txt_comentarios.Location = New System.Drawing.Point(37, 387)
        Me.txt_comentarios.Multiline = True
        Me.txt_comentarios.Name = "txt_comentarios"
        Me.txt_comentarios.Size = New System.Drawing.Size(364, 66)
        Me.txt_comentarios.TabIndex = 11
        '
        'lbl_comentarios
        '
        Me.lbl_comentarios.AutoSize = True
        Me.lbl_comentarios.BackColor = System.Drawing.Color.Transparent
        Me.lbl_comentarios.Enabled = False
        Me.lbl_comentarios.Location = New System.Drawing.Point(34, 371)
        Me.lbl_comentarios.Name = "lbl_comentarios"
        Me.lbl_comentarios.Size = New System.Drawing.Size(65, 13)
        Me.lbl_comentarios.TabIndex = 16
        Me.lbl_comentarios.Text = "Comentarios"
        '
        'btn_hotel
        '
        Appearance6.BackColor = System.Drawing.Color.Transparent
        Appearance6.Image = CType(resources.GetObject("Appearance6.Image"), Object)
        Appearance6.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance6.ImageVAlign = Infragistics.Win.VAlign.Top
        Me.btn_hotel.Appearance = Appearance6
        Me.btn_hotel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_hotel.Enabled = False
        Me.btn_hotel.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_hotel.Location = New System.Drawing.Point(512, 418)
        Me.btn_hotel.Name = "btn_hotel"
        Me.btn_hotel.Size = New System.Drawing.Size(81, 66)
        Me.btn_hotel.TabIndex = 16
        Me.btn_hotel.Text = "Buscar Hotel"
        Me.btn_hotel.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'btn_vuelos
        '
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Appearance2.Image = CType(resources.GetObject("Appearance2.Image"), Object)
        Appearance2.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance2.ImageVAlign = Infragistics.Win.VAlign.Top
        Me.btn_vuelos.Appearance = Appearance2
        Me.btn_vuelos.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_vuelos.Enabled = False
        Me.btn_vuelos.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_vuelos.Location = New System.Drawing.Point(599, 418)
        Me.btn_vuelos.Name = "btn_vuelos"
        Me.btn_vuelos.Size = New System.Drawing.Size(81, 66)
        Me.btn_vuelos.TabIndex = 17
        Me.btn_vuelos.Text = "Buscar Vuelos"
        Me.btn_vuelos.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'btn_actividades
        '
        Appearance3.BackColor = System.Drawing.Color.Transparent
        Appearance3.Image = CType(resources.GetObject("Appearance3.Image"), Object)
        Appearance3.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance3.ImageVAlign = Infragistics.Win.VAlign.Top
        Me.btn_actividades.Appearance = Appearance3
        Me.btn_actividades.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_actividades.Enabled = False
        Me.btn_actividades.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_actividades.Location = New System.Drawing.Point(686, 418)
        Me.btn_actividades.Name = "btn_actividades"
        Me.btn_actividades.Size = New System.Drawing.Size(81, 66)
        Me.btn_actividades.TabIndex = 18
        Me.btn_actividades.Text = "Buscar Actividades"
        Me.btn_actividades.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'btn_autos
        '
        Appearance4.BackColor = System.Drawing.Color.Transparent
        Appearance4.Image = CType(resources.GetObject("Appearance4.Image"), Object)
        Appearance4.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance4.ImageVAlign = Infragistics.Win.VAlign.Top
        Me.btn_autos.Appearance = Appearance4
        Me.btn_autos.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_autos.Enabled = False
        Me.btn_autos.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_autos.Location = New System.Drawing.Point(773, 418)
        Me.btn_autos.Name = "btn_autos"
        Me.btn_autos.Size = New System.Drawing.Size(81, 66)
        Me.btn_autos.TabIndex = 19
        Me.btn_autos.Text = "Buscar Autos"
        Me.btn_autos.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'UltraButton1
        '
        Me.UltraButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance9.BackColor = System.Drawing.Color.Transparent
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Right
        Me.UltraButton1.Appearance = Appearance9
        Me.UltraButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.UltraButton1.ImageSize = New System.Drawing.Size(48, 48)
        Me.UltraButton1.Location = New System.Drawing.Point(634, 30)
        Me.UltraButton1.Name = "UltraButton1"
        Me.UltraButton1.Size = New System.Drawing.Size(102, 54)
        Me.UltraButton1.TabIndex = 1
        Me.UltraButton1.Text = "Iniciar"
        Me.UltraButton1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'lbl_horas
        '
        Me.lbl_horas.AutoSize = True
        Me.lbl_horas.BackColor = System.Drawing.Color.Transparent
        Me.lbl_horas.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_horas.Location = New System.Drawing.Point(31, 34)
        Me.lbl_horas.Name = "lbl_horas"
        Me.lbl_horas.Size = New System.Drawing.Size(127, 33)
        Me.lbl_horas.TabIndex = 0
        Me.lbl_horas.Text = "00:00:00"
        '
        'txt_email
        '
        Me.txt_email.AlwaysInEditMode = True
        Me.txt_email.Enabled = False
        Me.txt_email.Location = New System.Drawing.Point(146, 337)
        Me.txt_email.Name = "txt_email"
        Me.txt_email.Size = New System.Drawing.Size(255, 21)
        Me.txt_email.TabIndex = 10
        '
        'lbl_email
        '
        Me.lbl_email.AutoSize = True
        Me.lbl_email.BackColor = System.Drawing.Color.Transparent
        Me.lbl_email.Enabled = False
        Me.lbl_email.Location = New System.Drawing.Point(34, 341)
        Me.lbl_email.Name = "lbl_email"
        Me.lbl_email.Size = New System.Drawing.Size(93, 13)
        Me.lbl_email.TabIndex = 0
        Me.lbl_email.Text = "Correo electr�nico"
        '
        'txt_telefono
        '
        Me.txt_telefono.AlwaysInEditMode = True
        Me.txt_telefono.Enabled = False
        Me.txt_telefono.Location = New System.Drawing.Point(146, 310)
        Me.txt_telefono.Name = "txt_telefono"
        Me.txt_telefono.Size = New System.Drawing.Size(255, 21)
        Me.txt_telefono.TabIndex = 9
        '
        'lbl_telefono
        '
        Me.lbl_telefono.AutoSize = True
        Me.lbl_telefono.BackColor = System.Drawing.Color.Transparent
        Me.lbl_telefono.Enabled = False
        Me.lbl_telefono.Location = New System.Drawing.Point(34, 314)
        Me.lbl_telefono.Name = "lbl_telefono"
        Me.lbl_telefono.Size = New System.Drawing.Size(49, 13)
        Me.lbl_telefono.TabIndex = 0
        Me.lbl_telefono.Text = "Tel�fono"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.rbt_masculino)
        Me.Panel1.Controls.Add(Me.rbt_femenino)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(37, 160)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(183, 33)
        Me.Panel1.TabIndex = 4
        '
        'rbt_masculino
        '
        Me.rbt_masculino.AutoSize = True
        Me.rbt_masculino.BackColor = System.Drawing.Color.Transparent
        Me.rbt_masculino.Checked = True
        Me.rbt_masculino.Location = New System.Drawing.Point(3, 8)
        Me.rbt_masculino.Name = "rbt_masculino"
        Me.rbt_masculino.Size = New System.Drawing.Size(73, 17)
        Me.rbt_masculino.TabIndex = 0
        Me.rbt_masculino.TabStop = True
        Me.rbt_masculino.Text = "Masculino"
        Me.rbt_masculino.UseVisualStyleBackColor = False
        '
        'rbt_femenino
        '
        Me.rbt_femenino.AutoSize = True
        Me.rbt_femenino.BackColor = System.Drawing.Color.Transparent
        Me.rbt_femenino.Location = New System.Drawing.Point(100, 8)
        Me.rbt_femenino.Name = "rbt_femenino"
        Me.rbt_femenino.Size = New System.Drawing.Size(71, 17)
        Me.rbt_femenino.TabIndex = 0
        Me.rbt_femenino.Text = "Femenino"
        Me.rbt_femenino.UseVisualStyleBackColor = False
        '
        'rbt_otro
        '
        Me.rbt_otro.AutoSize = True
        Me.rbt_otro.BackColor = System.Drawing.Color.Transparent
        Me.rbt_otro.Enabled = False
        Me.rbt_otro.Location = New System.Drawing.Point(67, 294)
        Me.rbt_otro.Name = "rbt_otro"
        Me.rbt_otro.Size = New System.Drawing.Size(45, 17)
        Me.rbt_otro.TabIndex = 8
        Me.rbt_otro.Text = "Otro"
        Me.rbt_otro.UseVisualStyleBackColor = False
        '
        'rbt_info
        '
        Me.rbt_info.AutoSize = True
        Me.rbt_info.BackColor = System.Drawing.Color.Transparent
        Me.rbt_info.Enabled = False
        Me.rbt_info.Location = New System.Drawing.Point(67, 271)
        Me.rbt_info.Name = "rbt_info"
        Me.rbt_info.Size = New System.Drawing.Size(137, 17)
        Me.rbt_info.TabIndex = 7
        Me.rbt_info.Text = "Solicitud de informaci�n"
        Me.rbt_info.UseVisualStyleBackColor = False
        '
        'rbt_disponibilidad
        '
        Me.rbt_disponibilidad.AutoSize = True
        Me.rbt_disponibilidad.BackColor = System.Drawing.Color.Transparent
        Me.rbt_disponibilidad.Enabled = False
        Me.rbt_disponibilidad.Location = New System.Drawing.Point(67, 248)
        Me.rbt_disponibilidad.Name = "rbt_disponibilidad"
        Me.rbt_disponibilidad.Size = New System.Drawing.Size(135, 17)
        Me.rbt_disponibilidad.TabIndex = 6
        Me.rbt_disponibilidad.Text = "Consultar disponibilidad"
        Me.rbt_disponibilidad.UseVisualStyleBackColor = False
        '
        'rbt_reservar
        '
        Me.rbt_reservar.AutoSize = True
        Me.rbt_reservar.BackColor = System.Drawing.Color.Transparent
        Me.rbt_reservar.Checked = True
        Me.rbt_reservar.Enabled = False
        Me.rbt_reservar.Location = New System.Drawing.Point(67, 225)
        Me.rbt_reservar.Name = "rbt_reservar"
        Me.rbt_reservar.Size = New System.Drawing.Size(142, 17)
        Me.rbt_reservar.TabIndex = 5
        Me.rbt_reservar.TabStop = True
        Me.rbt_reservar.Text = "Realizar una reservaci�n"
        Me.rbt_reservar.UseVisualStyleBackColor = False
        '
        'lbl_saludos
        '
        Me.lbl_saludos.AutoSize = True
        Me.lbl_saludos.BackColor = System.Drawing.Color.Transparent
        Me.lbl_saludos.Enabled = False
        Me.lbl_saludos.Location = New System.Drawing.Point(34, 209)
        Me.lbl_saludos.Name = "lbl_saludos"
        Me.lbl_saludos.Size = New System.Drawing.Size(131, 13)
        Me.lbl_saludos.TabIndex = 0
        Me.lbl_saludos.Text = "Tipo de ayuda que desea:"
        '
        'txt_nombre
        '
        Me.txt_nombre.AlwaysInEditMode = True
        Me.txt_nombre.Enabled = False
        Me.txt_nombre.Location = New System.Drawing.Point(146, 117)
        Me.txt_nombre.Name = "txt_nombre"
        Me.txt_nombre.Size = New System.Drawing.Size(148, 21)
        Me.txt_nombre.TabIndex = 2
        '
        'lbl_nombre
        '
        Me.lbl_nombre.AutoSize = True
        Me.lbl_nombre.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nombre.Enabled = False
        Me.lbl_nombre.Location = New System.Drawing.Point(34, 121)
        Me.lbl_nombre.Name = "lbl_nombre"
        Me.lbl_nombre.Size = New System.Drawing.Size(106, 13)
        Me.lbl_nombre.TabIndex = 0
        Me.lbl_nombre.Text = "�Cu�l es su nombre?"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Timer2
        '
        Me.Timer2.Enabled = True
        Me.Timer2.Interval = 1000
        '
        'newCall
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(979, 582)
        Me.Controls.Add(Me.ugb_contacto)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "newCall"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nueva llamada"
        CType(Me.ugb_contacto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugb_contacto.ResumeLayout(False)
        Me.ugb_contacto.PerformLayout()
        CType(Me.cmbMedios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_apellido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_comentarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_email, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_telefono, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.txt_nombre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ugb_contacto As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents lbl_nombre As System.Windows.Forms.Label
    Friend WithEvents txt_nombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents rbt_femenino As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_masculino As System.Windows.Forms.RadioButton
    Friend WithEvents lbl_saludos As System.Windows.Forms.Label
    Friend WithEvents rbt_info As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_disponibilidad As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_reservar As System.Windows.Forms.RadioButton
    Friend WithEvents rbt_otro As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_email As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_email As System.Windows.Forms.Label
    Friend WithEvents txt_telefono As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_telefono As System.Windows.Forms.Label
    Friend WithEvents UltraButton1 As Infragistics.Win.Misc.UltraButton
    Friend WithEvents lbl_horas As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btn_hotel As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_vuelos As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_actividades As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_autos As Infragistics.Win.Misc.UltraButton
    Friend WithEvents txt_comentarios As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_comentarios As System.Windows.Forms.Label
    Friend WithEvents lbl_revasActuales As System.Windows.Forms.Label
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents btn_cancelar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_modificar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents UltraButton2 As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_confirmar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents lbl_fecha As System.Windows.Forms.Label
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents txt_apellido As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_msg As System.Windows.Forms.Label
    Friend WithEvents cmbMedios As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lblMedioPromocion As System.Windows.Forms.Label
    Friend WithEvents lbl_Apellidos As System.Windows.Forms.Label
    'Friend WithEvents ComboSearch1 As callCenter.ComboSearch
End Class
