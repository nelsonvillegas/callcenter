Public Class CallOption
    Public Enum CallResult
        NewCall
        BookNoCall
        Cancel
    End Enum

    Public Result As CallResult

    Private Sub CallOption_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CapaPresentacion.Idiomas.cambiar_CallOption(Me)
    End Sub

    Private Sub btn_NewCall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_NewCall.Click
        'Me.DialogResult = Windows.Forms.DialogResult.OK
        Result = CallResult.NewCall
        Close()
    End Sub

    Private Sub btn_BookWithNoCall_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_BookWithNoCall.Click
        'Me.DialogResult = Windows.Forms.DialogResult.OK
        Result = CallResult.BookNoCall
        Close()
    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        'Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Result = CallResult.Cancel
        Close()
    End Sub

End Class