<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class listaLlamadas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(listaLlamadas))
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim UltraGridBand1 As Infragistics.Win.UltraWinGrid.UltraGridBand = New Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1)
        Dim UltraGridColumn1 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("Nombre")
        Dim UltraGridColumn2 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("N�m. reservaci�n")
        Dim UltraGridColumn3 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("Fecha reservaci�n")
        Dim UltraGridColumn4 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("Total")
        Dim UltraGridColumn5 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("Entrada")
        Dim UltraGridColumn6 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("Salida")
        Dim UltraGridColumn7 As Infragistics.Win.UltraWinGrid.UltraGridColumn = New Infragistics.Win.UltraWinGrid.UltraGridColumn("Operador", 0)
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox
        Me.btn_buscar = New Infragistics.Win.Misc.UltraButton
        Me.dtp_fin = New System.Windows.Forms.DateTimePicker
        Me.dtp_ini = New System.Windows.Forms.DateTimePicker
        Me.lbl_ini = New System.Windows.Forms.Label
        Me.lbl_fin = New System.Windows.Forms.Label
        Me.btn_limpiar = New System.Windows.Forms.Button
        Me.txt_comentarios = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_reservaciones = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_duracion = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txt_nombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lbl_comentarios = New System.Windows.Forms.Label
        Me.lbl_reservaciones = New System.Windows.Forms.Label
        Me.lbl_duracion = New System.Windows.Forms.Label
        Me.lbl_nombre = New System.Windows.Forms.Label
        Me.lblOperador = New System.Windows.Forms.Label
        Me.txtOperador = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.txt_comentarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_reservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_duracion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOperador, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraGrid1
        '
        Me.UltraGrid1.ContextMenuStrip = Me.ContextMenuStrip1
        Appearance1.BackColor = System.Drawing.Color.White
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance1
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance2.ImageBackground = CType(resources.GetObject("Appearance2.ImageBackground"), System.Drawing.Image)
        Appearance2.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance2.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance2.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance2
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance3.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance3
        UltraGridColumn1.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append
        UltraGridColumn1.Header.VisiblePosition = 0
        UltraGridColumn2.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append
        UltraGridColumn2.Header.VisiblePosition = 1
        UltraGridColumn3.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append
        UltraGridColumn3.Header.VisiblePosition = 2
        UltraGridColumn4.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append
        UltraGridColumn4.Header.VisiblePosition = 3
        UltraGridColumn5.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append
        UltraGridColumn5.Header.VisiblePosition = 4
        UltraGridColumn6.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append
        UltraGridColumn6.Header.VisiblePosition = 5
        UltraGridColumn7.Header.VisiblePosition = 6
        UltraGridBand1.Columns.AddRange(New Object() {UltraGridColumn1, UltraGridColumn2, UltraGridColumn3, UltraGridColumn4, UltraGridColumn5, UltraGridColumn6, UltraGridColumn7})
        Me.UltraGrid1.DisplayLayout.BandsSerializer.Add(UltraGridBand1)
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance4.FontData.Name = "Trebuchet MS"
        Appearance4.FontData.SizeInPoints = 9.0!
        Appearance4.ForeColor = System.Drawing.Color.White
        Appearance4.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance4
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance5.FontData.BoldAsString = "True"
        Appearance5.FontData.Name = "Trebuchet MS"
        Appearance5.FontData.SizeInPoints = 10.0!
        Appearance5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance5.ImageBackground = CType(resources.GetObject("Appearance5.ImageBackground"), System.Drawing.Image)
        Appearance5.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance5.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance5
        Appearance6.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance6
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance7.BackColor2 = System.Drawing.SystemColors.Control
        Appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance7.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance7
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance8.BackColor = System.Drawing.SystemColors.Window
        Appearance8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance8
        Appearance29.BackColor = System.Drawing.SystemColors.Highlight
        Appearance29.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance29
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance10.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance10
        Appearance11.BorderColor = System.Drawing.Color.Transparent
        Appearance11.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance11
        Appearance12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance12.ImageBackground = CType(resources.GetObject("Appearance12.ImageBackground"), System.Drawing.Image)
        Appearance12.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance12.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance12
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance13.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance13
        Appearance14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance14.ImageBackground = CType(resources.GetObject("Appearance14.ImageBackground"), System.Drawing.Image)
        Appearance14.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance14
        Appearance15.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance15.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance15
        Appearance16.BackColor = System.Drawing.SystemColors.Control
        Appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance16.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance16.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance16
        Appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance17.FontData.BoldAsString = "True"
        Appearance17.FontData.Name = "Trebuchet MS"
        Appearance17.FontData.SizeInPoints = 10.0!
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance17.TextHAlignAsString = "Left"
        Appearance17.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance17
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance28.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance28
        Appearance18.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance18
        Appearance19.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance19
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance20.BorderColor = System.Drawing.Color.Transparent
        Appearance20.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance20
        Appearance21.BorderColor = System.Drawing.Color.Transparent
        Appearance21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance21.ImageBackground = CType(resources.GetObject("Appearance21.ImageBackground"), System.Drawing.Image)
        Appearance21.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance21.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance21
        Appearance22.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance22
        Appearance23.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance23.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance23
        Appearance24.ImageBackground = CType(resources.GetObject("Appearance24.ImageBackground"), System.Drawing.Image)
        Appearance24.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance24
        Appearance25.ImageBackground = CType(resources.GetObject("Appearance25.ImageBackground"), System.Drawing.Image)
        Appearance25.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance25.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance25
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance26
        Appearance27.ImageBackground = CType(resources.GetObject("Appearance27.ImageBackground"), System.Drawing.Image)
        Appearance27.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance27.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance27
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 105)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(1141, 285)
        Me.UltraGrid1.TabIndex = 1
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.UltraGroupBox1)
        Me.Panel1.Controls.Add(Me.btn_limpiar)
        Me.Panel1.Controls.Add(Me.txtOperador)
        Me.Panel1.Controls.Add(Me.txt_comentarios)
        Me.Panel1.Controls.Add(Me.txt_reservaciones)
        Me.Panel1.Controls.Add(Me.txt_duracion)
        Me.Panel1.Controls.Add(Me.lblOperador)
        Me.Panel1.Controls.Add(Me.txt_nombre)
        Me.Panel1.Controls.Add(Me.lbl_comentarios)
        Me.Panel1.Controls.Add(Me.lbl_reservaciones)
        Me.Panel1.Controls.Add(Me.lbl_duracion)
        Me.Panel1.Controls.Add(Me.lbl_nombre)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1141, 105)
        Me.Panel1.TabIndex = 0
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Controls.Add(Me.btn_buscar)
        Me.UltraGroupBox1.Controls.Add(Me.dtp_fin)
        Me.UltraGroupBox1.Controls.Add(Me.dtp_ini)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_ini)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_fin)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(316, 80)
        Me.UltraGroupBox1.TabIndex = 13
        Me.UltraGroupBox1.Text = "Filtros"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'btn_buscar
        '
        Me.btn_buscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance9.BackColor = System.Drawing.Color.Transparent
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Right
        Me.btn_buscar.Appearance = Appearance9
        Me.btn_buscar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_buscar.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_buscar.Location = New System.Drawing.Point(208, 24)
        Me.btn_buscar.Name = "btn_buscar"
        Me.btn_buscar.Size = New System.Drawing.Size(102, 46)
        Me.btn_buscar.TabIndex = 14
        Me.btn_buscar.Text = "Buscar"
        Me.btn_buscar.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'dtp_fin
        '
        Me.dtp_fin.Checked = False
        Me.dtp_fin.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_fin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_fin.Location = New System.Drawing.Point(70, 50)
        Me.dtp_fin.Name = "dtp_fin"
        Me.dtp_fin.ShowCheckBox = True
        Me.dtp_fin.Size = New System.Drawing.Size(124, 20)
        Me.dtp_fin.TabIndex = 12
        '
        'dtp_ini
        '
        Me.dtp_ini.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_ini.Location = New System.Drawing.Point(70, 24)
        Me.dtp_ini.Name = "dtp_ini"
        Me.dtp_ini.ShowCheckBox = True
        Me.dtp_ini.Size = New System.Drawing.Size(124, 20)
        Me.dtp_ini.TabIndex = 11
        '
        'lbl_ini
        '
        Me.lbl_ini.AutoSize = True
        Me.lbl_ini.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ini.Location = New System.Drawing.Point(12, 26)
        Me.lbl_ini.Name = "lbl_ini"
        Me.lbl_ini.Size = New System.Drawing.Size(35, 13)
        Me.lbl_ini.TabIndex = 10
        Me.lbl_ini.Text = "Entre:"
        '
        'lbl_fin
        '
        Me.lbl_fin.AutoSize = True
        Me.lbl_fin.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fin.Location = New System.Drawing.Point(12, 52)
        Me.lbl_fin.Name = "lbl_fin"
        Me.lbl_fin.Size = New System.Drawing.Size(17, 13)
        Me.lbl_fin.TabIndex = 9
        Me.lbl_fin.Text = "Y:"
        '
        'btn_limpiar
        '
        Me.btn_limpiar.Location = New System.Drawing.Point(831, 61)
        Me.btn_limpiar.Name = "btn_limpiar"
        Me.btn_limpiar.Size = New System.Drawing.Size(75, 23)
        Me.btn_limpiar.TabIndex = 8
        Me.btn_limpiar.Text = "Limpiar"
        Me.btn_limpiar.UseVisualStyleBackColor = True
        '
        'txt_comentarios
        '
        Me.txt_comentarios.AlwaysInEditMode = True
        Me.txt_comentarios.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_comentarios.Location = New System.Drawing.Point(831, 12)
        Me.txt_comentarios.Name = "txt_comentarios"
        Me.txt_comentarios.Size = New System.Drawing.Size(192, 19)
        Me.txt_comentarios.TabIndex = 6
        '
        'txt_reservaciones
        '
        Me.txt_reservaciones.AlwaysInEditMode = True
        Me.txt_reservaciones.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_reservaciones.Location = New System.Drawing.Point(449, 63)
        Me.txt_reservaciones.Name = "txt_reservaciones"
        Me.txt_reservaciones.Size = New System.Drawing.Size(107, 19)
        Me.txt_reservaciones.TabIndex = 5
        '
        'txt_duracion
        '
        Me.txt_duracion.AlwaysInEditMode = True
        Me.txt_duracion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_duracion.Location = New System.Drawing.Point(449, 38)
        Me.txt_duracion.Name = "txt_duracion"
        Me.txt_duracion.Size = New System.Drawing.Size(141, 19)
        Me.txt_duracion.TabIndex = 4
        '
        'txt_nombre
        '
        Me.txt_nombre.AlwaysInEditMode = True
        Me.txt_nombre.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_nombre.Location = New System.Drawing.Point(449, 12)
        Me.txt_nombre.Name = "txt_nombre"
        Me.txt_nombre.Size = New System.Drawing.Size(201, 19)
        Me.txt_nombre.TabIndex = 3
        '
        'lbl_comentarios
        '
        Me.lbl_comentarios.AutoSize = True
        Me.lbl_comentarios.Location = New System.Drawing.Point(739, 15)
        Me.lbl_comentarios.Name = "lbl_comentarios"
        Me.lbl_comentarios.Size = New System.Drawing.Size(68, 13)
        Me.lbl_comentarios.TabIndex = 0
        Me.lbl_comentarios.Text = "Comentarios:"
        '
        'lbl_reservaciones
        '
        Me.lbl_reservaciones.AutoSize = True
        Me.lbl_reservaciones.Location = New System.Drawing.Point(350, 66)
        Me.lbl_reservaciones.Name = "lbl_reservaciones"
        Me.lbl_reservaciones.Size = New System.Drawing.Size(81, 13)
        Me.lbl_reservaciones.TabIndex = 0
        Me.lbl_reservaciones.Text = "Reservaciones:"
        '
        'lbl_duracion
        '
        Me.lbl_duracion.AutoSize = True
        Me.lbl_duracion.Location = New System.Drawing.Point(350, 41)
        Me.lbl_duracion.Name = "lbl_duracion"
        Me.lbl_duracion.Size = New System.Drawing.Size(53, 13)
        Me.lbl_duracion.TabIndex = 0
        Me.lbl_duracion.Text = "Duraci�n:"
        '
        'lbl_nombre
        '
        Me.lbl_nombre.AutoSize = True
        Me.lbl_nombre.Location = New System.Drawing.Point(350, 15)
        Me.lbl_nombre.Name = "lbl_nombre"
        Me.lbl_nombre.Size = New System.Drawing.Size(47, 13)
        Me.lbl_nombre.TabIndex = 0
        Me.lbl_nombre.Text = "Nombre:"
        '
        'lblOperador
        '
        Me.lblOperador.AutoSize = True
        Me.lblOperador.Location = New System.Drawing.Point(739, 38)
        Me.lblOperador.Name = "lblOperador"
        Me.lblOperador.Size = New System.Drawing.Size(54, 13)
        Me.lblOperador.TabIndex = 0
        Me.lblOperador.Text = "Operador:"
        '
        'txtOperador
        '
        Me.txtOperador.AlwaysInEditMode = True
        Me.txtOperador.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtOperador.Location = New System.Drawing.Point(831, 35)
        Me.txtOperador.Name = "txtOperador"
        Me.txtOperador.Size = New System.Drawing.Size(192, 19)
        Me.txtOperador.TabIndex = 6
        '
        'listaLlamadas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1141, 390)
        Me.Controls.Add(Me.UltraGrid1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "listaLlamadas"
        Me.Text = "Lista de llamadas"
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.txt_comentarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_reservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_duracion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOperador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lbl_comentarios As System.Windows.Forms.Label
    Friend WithEvents lbl_reservaciones As System.Windows.Forms.Label
    Friend WithEvents lbl_duracion As System.Windows.Forms.Label
    Friend WithEvents lbl_nombre As System.Windows.Forms.Label
    Friend WithEvents txt_duracion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_nombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_comentarios As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_reservaciones As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_limpiar As System.Windows.Forms.Button
    Friend WithEvents dtp_fin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_fin As System.Windows.Forms.Label
    Friend WithEvents lbl_ini As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents btn_buscar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents txtOperador As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lblOperador As System.Windows.Forms.Label
End Class
