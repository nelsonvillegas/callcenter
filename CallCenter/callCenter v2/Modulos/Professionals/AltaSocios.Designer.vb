<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AltaSocios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim UltraTab3 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab
        Dim UltraTab1 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab
        Dim UltraTab2 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AltaSocios))
        Me.UltraTabPageControl3 = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl
        Me.cmbProcedenciasAg = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtPuestoAg = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtRFC = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtAgencia = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtRazonSocial = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label20 = New System.Windows.Forms.Label
        Me.cmbTipoSocio = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.cmbComoSeEntero = New System.Windows.Forms.ComboBox
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker
        Me.cmbEstados = New System.Windows.Forms.ComboBox
        Me.txtQuienatendio = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtCualseentero = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtPassword1 = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtPassword = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtEmail = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtFax = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtCodigoPostal = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtCelular = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txtCiudad = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.txtTelefono = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtDireccion = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtColonia = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.lblNombre = New System.Windows.Forms.Label
        Me.UltraTabPageControl1 = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl
        Me.cmbProcedenciasCl = New System.Windows.Forms.ComboBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtApellidos = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label18 = New System.Windows.Forms.Label
        Me.UltraTabPageControl2 = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl
        Me.cmbProcedenciasPr = New System.Windows.Forms.ComboBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.cmbTipoSocioP = New System.Windows.Forms.ComboBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtApellidosProf = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label17 = New System.Windows.Forms.Label
        Me.pnlProfessionals = New System.Windows.Forms.Panel
        Me.txtPuesto = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtEmpresa = New Infragistics.Win.UltraWinEditors.UltraTextEditor
        Me.Label26 = New System.Windows.Forms.Label
        Me.btn_registrar = New System.Windows.Forms.Button
        Me.btn_cancelar = New System.Windows.Forms.Button
        Me.UltraTabSharedControlsPage1 = New Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
        Me.Tabs = New Infragistics.Win.UltraWinTabControl.UltraTabControl
        Me.UltraTabPageControl3.SuspendLayout()
        CType(Me.txtPuestoAg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRFC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRazonSocial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtQuienatendio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCualseentero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPassword1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPassword, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoPostal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCelular, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCiudad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtColonia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabPageControl1.SuspendLayout()
        CType(Me.txtApellidos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabPageControl2.SuspendLayout()
        CType(Me.txtApellidosProf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProfessionals.SuspendLayout()
        CType(Me.txtPuesto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tabs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tabs.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraTabPageControl3
        '
        Me.UltraTabPageControl3.AutoScroll = True
        Me.UltraTabPageControl3.Controls.Add(Me.cmbProcedenciasAg)
        Me.UltraTabPageControl3.Controls.Add(Me.Label11)
        Me.UltraTabPageControl3.Controls.Add(Me.txtPuestoAg)
        Me.UltraTabPageControl3.Controls.Add(Me.Label22)
        Me.UltraTabPageControl3.Controls.Add(Me.txtRFC)
        Me.UltraTabPageControl3.Controls.Add(Me.Label21)
        Me.UltraTabPageControl3.Controls.Add(Me.txtAgencia)
        Me.UltraTabPageControl3.Controls.Add(Me.Label19)
        Me.UltraTabPageControl3.Controls.Add(Me.txtRazonSocial)
        Me.UltraTabPageControl3.Controls.Add(Me.Label20)
        Me.UltraTabPageControl3.Controls.Add(Me.cmbTipoSocio)
        Me.UltraTabPageControl3.Controls.Add(Me.Label23)
        Me.UltraTabPageControl3.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabPageControl3.Name = "UltraTabPageControl3"
        Me.UltraTabPageControl3.Size = New System.Drawing.Size(493, 467)
        '
        'cmbProcedenciasAg
        '
        Me.cmbProcedenciasAg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProcedenciasAg.FormattingEnabled = True
        Me.cmbProcedenciasAg.Location = New System.Drawing.Point(113, 275)
        Me.cmbProcedenciasAg.Name = "cmbProcedenciasAg"
        Me.cmbProcedenciasAg.Size = New System.Drawing.Size(136, 21)
        Me.cmbProcedenciasAg.TabIndex = 21
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Location = New System.Drawing.Point(3, 278)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(70, 13)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Procedencia:"
        '
        'txtPuestoAg
        '
        Me.txtPuestoAg.AlwaysInEditMode = True
        Me.txtPuestoAg.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtPuestoAg.Enabled = False
        Me.txtPuestoAg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuestoAg.Location = New System.Drawing.Point(323, 75)
        Me.txtPuestoAg.MaxLength = 50
        Me.txtPuestoAg.Name = "txtPuestoAg"
        Me.txtPuestoAg.Size = New System.Drawing.Size(163, 19)
        Me.txtPuestoAg.TabIndex = 4
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(254, 78)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(50, 13)
        Me.Label22.TabIndex = 48
        Me.Label22.Text = "Puesto:"
        '
        'txtRFC
        '
        Me.txtRFC.AlwaysInEditMode = True
        Me.txtRFC.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtRFC.Enabled = False
        Me.txtRFC.Location = New System.Drawing.Point(323, 52)
        Me.txtRFC.MaxLength = 20
        Me.txtRFC.Name = "txtRFC"
        Me.txtRFC.Size = New System.Drawing.Size(163, 19)
        Me.txtRFC.TabIndex = 3
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Location = New System.Drawing.Point(254, 56)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(31, 13)
        Me.Label21.TabIndex = 47
        Me.Label21.Text = "RFC:"
        '
        'txtAgencia
        '
        Me.txtAgencia.AlwaysInEditMode = True
        Me.txtAgencia.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtAgencia.Enabled = False
        Me.txtAgencia.Location = New System.Drawing.Point(323, 3)
        Me.txtAgencia.MaxLength = 50
        Me.txtAgencia.Name = "txtAgencia"
        Me.txtAgencia.Size = New System.Drawing.Size(163, 19)
        Me.txtAgencia.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(254, 6)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(57, 13)
        Me.Label19.TabIndex = 49
        Me.Label19.Text = "Agencia:"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.AlwaysInEditMode = True
        Me.txtRazonSocial.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtRazonSocial.Enabled = False
        Me.txtRazonSocial.Location = New System.Drawing.Point(323, 27)
        Me.txtRazonSocial.MaxLength = 50
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(163, 19)
        Me.txtRazonSocial.TabIndex = 2
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Location = New System.Drawing.Point(254, 30)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(73, 13)
        Me.Label20.TabIndex = 49
        Me.Label20.Text = "Razon Social:"
        '
        'cmbTipoSocio
        '
        Me.cmbTipoSocio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoSocio.FormattingEnabled = True
        Me.cmbTipoSocio.Items.AddRange(New Object() {"Individual", "Empresarial"})
        Me.cmbTipoSocio.Location = New System.Drawing.Point(333, 100)
        Me.cmbTipoSocio.Name = "cmbTipoSocio"
        Me.cmbTipoSocio.Size = New System.Drawing.Size(121, 21)
        Me.cmbTipoSocio.TabIndex = 5
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Location = New System.Drawing.Point(251, 103)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(76, 13)
        Me.Label23.TabIndex = 22
        Me.Label23.Text = "Tipo de Socio:"
        '
        'cmbComoSeEntero
        '
        Me.cmbComoSeEntero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbComoSeEntero.FormattingEnabled = True
        Me.cmbComoSeEntero.Items.AddRange(New Object() {"Internet", "Hotel", "Ejecutivo", "Otro"})
        Me.cmbComoSeEntero.Location = New System.Drawing.Point(113, 352)
        Me.cmbComoSeEntero.Name = "cmbComoSeEntero"
        Me.cmbComoSeEntero.Size = New System.Drawing.Size(124, 21)
        Me.cmbComoSeEntero.TabIndex = 24
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "yyyy/MM/dd"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(113, 252)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(100, 20)
        Me.dtpFecha.TabIndex = 20
        '
        'cmbEstados
        '
        Me.cmbEstados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEstados.FormattingEnabled = True
        Me.cmbEstados.Location = New System.Drawing.Point(86, 100)
        Me.cmbEstados.Name = "cmbEstados"
        Me.cmbEstados.Size = New System.Drawing.Size(163, 21)
        Me.cmbEstados.TabIndex = 14
        '
        'txtQuienatendio
        '
        Me.txtQuienatendio.AlwaysInEditMode = True
        Me.txtQuienatendio.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtQuienatendio.Enabled = False
        Me.txtQuienatendio.Location = New System.Drawing.Point(113, 402)
        Me.txtQuienatendio.MaxLength = 30
        Me.txtQuienatendio.Name = "txtQuienatendio"
        Me.txtQuienatendio.Size = New System.Drawing.Size(136, 19)
        Me.txtQuienatendio.TabIndex = 26
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Location = New System.Drawing.Point(3, 405)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(77, 13)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Quien Atendi�:"
        '
        'txtCualseentero
        '
        Me.txtCualseentero.AlwaysInEditMode = True
        Me.txtCualseentero.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtCualseentero.Enabled = False
        Me.txtCualseentero.Location = New System.Drawing.Point(113, 377)
        Me.txtCualseentero.MaxLength = 30
        Me.txtCualseentero.Name = "txtCualseentero"
        Me.txtCualseentero.Size = New System.Drawing.Size(136, 19)
        Me.txtCualseentero.TabIndex = 25
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Location = New System.Drawing.Point(3, 380)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 13)
        Me.Label15.TabIndex = 23
        Me.Label15.Text = "Que Hotel:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Location = New System.Drawing.Point(3, 355)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(84, 13)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Como se enter�:"
        '
        'txtPassword1
        '
        Me.txtPassword1.AlwaysInEditMode = True
        Me.txtPassword1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtPassword1.Enabled = False
        Me.txtPassword1.Location = New System.Drawing.Point(113, 327)
        Me.txtPassword1.MaxLength = 15
        Me.txtPassword1.Name = "txtPassword1"
        Me.txtPassword1.Size = New System.Drawing.Size(108, 19)
        Me.txtPassword1.TabIndex = 23
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(3, 330)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(118, 13)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "Confirme Password:"
        '
        'txtPassword
        '
        Me.txtPassword.AlwaysInEditMode = True
        Me.txtPassword.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtPassword.Enabled = False
        Me.txtPassword.Location = New System.Drawing.Point(113, 302)
        Me.txtPassword.MaxLength = 15
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(108, 19)
        Me.txtPassword.TabIndex = 22
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(3, 305)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 13)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Password:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Location = New System.Drawing.Point(3, 255)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(111, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Fecha de Nacimiento:"
        '
        'txtEmail
        '
        Me.txtEmail.AlwaysInEditMode = True
        Me.txtEmail.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtEmail.Enabled = False
        Me.txtEmail.Location = New System.Drawing.Point(86, 227)
        Me.txtEmail.MaxLength = 50
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(121, 19)
        Me.txtEmail.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 230)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Email:"
        '
        'txtFax
        '
        Me.txtFax.AlwaysInEditMode = True
        Me.txtFax.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtFax.Enabled = False
        Me.txtFax.Location = New System.Drawing.Point(86, 176)
        Me.txtFax.MaxLength = 15
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(121, 19)
        Me.txtFax.TabIndex = 17
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Location = New System.Drawing.Point(3, 179)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(27, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Fax:"
        '
        'txtCodigoPostal
        '
        Me.txtCodigoPostal.AlwaysInEditMode = True
        Me.txtCodigoPostal.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtCodigoPostal.Enabled = False
        Me.txtCodigoPostal.Location = New System.Drawing.Point(86, 125)
        Me.txtCodigoPostal.MaxLength = 15
        Me.txtCodigoPostal.Name = "txtCodigoPostal"
        Me.txtCodigoPostal.Size = New System.Drawing.Size(100, 19)
        Me.txtCodigoPostal.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 128)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "C�digo Postal:"
        '
        'txtCelular
        '
        Me.txtCelular.AlwaysInEditMode = True
        Me.txtCelular.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtCelular.Enabled = False
        Me.txtCelular.Location = New System.Drawing.Point(86, 202)
        Me.txtCelular.MaxLength = 20
        Me.txtCelular.Name = "txtCelular"
        Me.txtCelular.Size = New System.Drawing.Size(121, 19)
        Me.txtCelular.TabIndex = 18
        '
        'txtCiudad
        '
        Me.txtCiudad.AlwaysInEditMode = True
        Me.txtCiudad.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtCiudad.Enabled = False
        Me.txtCiudad.Location = New System.Drawing.Point(86, 76)
        Me.txtCiudad.MaxLength = 50
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(121, 19)
        Me.txtCiudad.TabIndex = 13
        '
        'txtTelefono
        '
        Me.txtTelefono.AlwaysInEditMode = True
        Me.txtTelefono.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtTelefono.Enabled = False
        Me.txtTelefono.Location = New System.Drawing.Point(86, 151)
        Me.txtTelefono.MaxLength = 15
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(121, 19)
        Me.txtTelefono.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Ciudad:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Location = New System.Drawing.Point(3, 205)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Celular:"
        '
        'txtDireccion
        '
        Me.txtDireccion.AlwaysInEditMode = True
        Me.txtDireccion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtDireccion.Enabled = False
        Me.txtDireccion.Location = New System.Drawing.Point(86, 28)
        Me.txtDireccion.MaxLength = 200
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(163, 19)
        Me.txtDireccion.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 154)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Tel�fono:"
        '
        'txtColonia
        '
        Me.txtColonia.AlwaysInEditMode = True
        Me.txtColonia.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtColonia.Enabled = False
        Me.txtColonia.Location = New System.Drawing.Point(86, 51)
        Me.txtColonia.MaxLength = 100
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.Size = New System.Drawing.Size(163, 19)
        Me.txtColonia.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(3, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Estado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Direcci�n:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(3, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Colonia:"
        '
        'txtNombre
        '
        Me.txtNombre.AlwaysInEditMode = True
        Me.txtNombre.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtNombre.Enabled = False
        Me.txtNombre.Location = New System.Drawing.Point(86, 3)
        Me.txtNombre.MaxLength = 50
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(163, 19)
        Me.txtNombre.TabIndex = 0
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(3, 6)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(54, 13)
        Me.lblNombre.TabIndex = 10
        Me.lblNombre.Text = "Nombre:"
        '
        'UltraTabPageControl1
        '
        Me.UltraTabPageControl1.Controls.Add(Me.cmbProcedenciasCl)
        Me.UltraTabPageControl1.Controls.Add(Me.Label27)
        Me.UltraTabPageControl1.Controls.Add(Me.txtApellidos)
        Me.UltraTabPageControl1.Controls.Add(Me.Label18)
        Me.UltraTabPageControl1.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabPageControl1.Name = "UltraTabPageControl1"
        Me.UltraTabPageControl1.Size = New System.Drawing.Size(493, 467)
        '
        'cmbProcedenciasCl
        '
        Me.cmbProcedenciasCl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProcedenciasCl.FormattingEnabled = True
        Me.cmbProcedenciasCl.Location = New System.Drawing.Point(113, 275)
        Me.cmbProcedenciasCl.Name = "cmbProcedenciasCl"
        Me.cmbProcedenciasCl.Size = New System.Drawing.Size(136, 21)
        Me.cmbProcedenciasCl.TabIndex = 56
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Location = New System.Drawing.Point(3, 278)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(70, 13)
        Me.Label27.TabIndex = 55
        Me.Label27.Text = "Procedencia:"
        '
        'txtApellidos
        '
        Me.txtApellidos.AlwaysInEditMode = True
        Me.txtApellidos.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtApellidos.Enabled = False
        Me.txtApellidos.Location = New System.Drawing.Point(307, 3)
        Me.txtApellidos.MaxLength = 50
        Me.txtApellidos.Name = "txtApellidos"
        Me.txtApellidos.Size = New System.Drawing.Size(163, 19)
        Me.txtApellidos.TabIndex = 6
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Location = New System.Drawing.Point(254, 6)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 46
        Me.Label18.Text = "Apellidos:"
        '
        'UltraTabPageControl2
        '
        Me.UltraTabPageControl2.Controls.Add(Me.cmbProcedenciasPr)
        Me.UltraTabPageControl2.Controls.Add(Me.Label28)
        Me.UltraTabPageControl2.Controls.Add(Me.cmbTipoSocioP)
        Me.UltraTabPageControl2.Controls.Add(Me.Label24)
        Me.UltraTabPageControl2.Controls.Add(Me.txtApellidosProf)
        Me.UltraTabPageControl2.Controls.Add(Me.Label17)
        Me.UltraTabPageControl2.Controls.Add(Me.pnlProfessionals)
        Me.UltraTabPageControl2.Controls.Add(Me.cmbComoSeEntero)
        Me.UltraTabPageControl2.Controls.Add(Me.dtpFecha)
        Me.UltraTabPageControl2.Controls.Add(Me.cmbEstados)
        Me.UltraTabPageControl2.Controls.Add(Me.txtQuienatendio)
        Me.UltraTabPageControl2.Controls.Add(Me.Label16)
        Me.UltraTabPageControl2.Controls.Add(Me.txtCualseentero)
        Me.UltraTabPageControl2.Controls.Add(Me.Label15)
        Me.UltraTabPageControl2.Controls.Add(Me.Label14)
        Me.UltraTabPageControl2.Controls.Add(Me.txtPassword1)
        Me.UltraTabPageControl2.Controls.Add(Me.Label13)
        Me.UltraTabPageControl2.Controls.Add(Me.txtPassword)
        Me.UltraTabPageControl2.Controls.Add(Me.Label12)
        Me.UltraTabPageControl2.Controls.Add(Me.Label10)
        Me.UltraTabPageControl2.Controls.Add(Me.txtEmail)
        Me.UltraTabPageControl2.Controls.Add(Me.Label9)
        Me.UltraTabPageControl2.Controls.Add(Me.txtFax)
        Me.UltraTabPageControl2.Controls.Add(Me.Label7)
        Me.UltraTabPageControl2.Controls.Add(Me.txtCodigoPostal)
        Me.UltraTabPageControl2.Controls.Add(Me.Label5)
        Me.UltraTabPageControl2.Controls.Add(Me.txtCelular)
        Me.UltraTabPageControl2.Controls.Add(Me.txtCiudad)
        Me.UltraTabPageControl2.Controls.Add(Me.txtTelefono)
        Me.UltraTabPageControl2.Controls.Add(Me.Label3)
        Me.UltraTabPageControl2.Controls.Add(Me.Label8)
        Me.UltraTabPageControl2.Controls.Add(Me.txtDireccion)
        Me.UltraTabPageControl2.Controls.Add(Me.Label6)
        Me.UltraTabPageControl2.Controls.Add(Me.txtColonia)
        Me.UltraTabPageControl2.Controls.Add(Me.Label4)
        Me.UltraTabPageControl2.Controls.Add(Me.Label1)
        Me.UltraTabPageControl2.Controls.Add(Me.Label2)
        Me.UltraTabPageControl2.Controls.Add(Me.txtNombre)
        Me.UltraTabPageControl2.Controls.Add(Me.lblNombre)
        Me.UltraTabPageControl2.Location = New System.Drawing.Point(1, 22)
        Me.UltraTabPageControl2.Name = "UltraTabPageControl2"
        Me.UltraTabPageControl2.Size = New System.Drawing.Size(493, 467)
        '
        'cmbProcedenciasPr
        '
        Me.cmbProcedenciasPr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProcedenciasPr.FormattingEnabled = True
        Me.cmbProcedenciasPr.Location = New System.Drawing.Point(113, 275)
        Me.cmbProcedenciasPr.Name = "cmbProcedenciasPr"
        Me.cmbProcedenciasPr.Size = New System.Drawing.Size(136, 21)
        Me.cmbProcedenciasPr.TabIndex = 56
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Location = New System.Drawing.Point(3, 278)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(70, 13)
        Me.Label28.TabIndex = 55
        Me.Label28.Text = "Procedencia:"
        '
        'cmbTipoSocioP
        '
        Me.cmbTipoSocioP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoSocioP.FormattingEnabled = True
        Me.cmbTipoSocioP.Items.AddRange(New Object() {"Individual", "Empresarial"})
        Me.cmbTipoSocioP.Location = New System.Drawing.Point(336, 76)
        Me.cmbTipoSocioP.Name = "cmbTipoSocioP"
        Me.cmbTipoSocioP.Size = New System.Drawing.Size(121, 21)
        Me.cmbTipoSocioP.TabIndex = 10
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Location = New System.Drawing.Point(254, 79)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(76, 13)
        Me.Label24.TabIndex = 51
        Me.Label24.Text = "Tipo de Socio:"
        '
        'txtApellidosProf
        '
        Me.txtApellidosProf.AlwaysInEditMode = True
        Me.txtApellidosProf.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtApellidosProf.Enabled = False
        Me.txtApellidosProf.Location = New System.Drawing.Point(307, 3)
        Me.txtApellidosProf.MaxLength = 50
        Me.txtApellidosProf.Name = "txtApellidosProf"
        Me.txtApellidosProf.Size = New System.Drawing.Size(163, 19)
        Me.txtApellidosProf.TabIndex = 7
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(254, 6)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(62, 13)
        Me.Label17.TabIndex = 49
        Me.Label17.Text = "Apellidos:"
        '
        'pnlProfessionals
        '
        Me.pnlProfessionals.Controls.Add(Me.txtPuesto)
        Me.pnlProfessionals.Controls.Add(Me.Label25)
        Me.pnlProfessionals.Controls.Add(Me.txtEmpresa)
        Me.pnlProfessionals.Controls.Add(Me.Label26)
        Me.pnlProfessionals.Location = New System.Drawing.Point(257, 22)
        Me.pnlProfessionals.Name = "pnlProfessionals"
        Me.pnlProfessionals.Size = New System.Drawing.Size(254, 54)
        Me.pnlProfessionals.TabIndex = 48
        '
        'txtPuesto
        '
        Me.txtPuesto.AlwaysInEditMode = True
        Me.txtPuesto.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtPuesto.Enabled = False
        Me.txtPuesto.Location = New System.Drawing.Point(50, 29)
        Me.txtPuesto.MaxLength = 50
        Me.txtPuesto.Name = "txtPuesto"
        Me.txtPuesto.Size = New System.Drawing.Size(163, 19)
        Me.txtPuesto.TabIndex = 9
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(-3, 32)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(50, 13)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Puesto:"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.AlwaysInEditMode = True
        Me.txtEmpresa.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtEmpresa.Enabled = False
        Me.txtEmpresa.Location = New System.Drawing.Point(50, 6)
        Me.txtEmpresa.MaxLength = 50
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(163, 19)
        Me.txtEmpresa.TabIndex = 8
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(-3, 9)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(59, 13)
        Me.Label26.TabIndex = 7
        Me.Label26.Text = "Empresa:"
        '
        'btn_registrar
        '
        Me.btn_registrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_registrar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_registrar.Location = New System.Drawing.Point(278, 496)
        Me.btn_registrar.Name = "btn_registrar"
        Me.btn_registrar.Size = New System.Drawing.Size(127, 23)
        Me.btn_registrar.TabIndex = 27
        Me.btn_registrar.Text = "Registrar Socio"
        Me.btn_registrar.UseVisualStyleBackColor = False
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(408, 496)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancelar.TabIndex = 28
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'UltraTabSharedControlsPage1
        '
        Me.UltraTabSharedControlsPage1.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabSharedControlsPage1.Name = "UltraTabSharedControlsPage1"
        Me.UltraTabSharedControlsPage1.Size = New System.Drawing.Size(493, 467)
        '
        'Tabs
        '
        Me.Tabs.Controls.Add(Me.UltraTabSharedControlsPage1)
        Me.Tabs.Controls.Add(Me.UltraTabPageControl3)
        Me.Tabs.Controls.Add(Me.UltraTabPageControl1)
        Me.Tabs.Controls.Add(Me.UltraTabPageControl2)
        Me.Tabs.Dock = System.Windows.Forms.DockStyle.Top
        Me.Tabs.Location = New System.Drawing.Point(0, 0)
        Me.Tabs.Name = "Tabs"
        Me.Tabs.SharedControls.AddRange(New System.Windows.Forms.Control() {Me.cmbComoSeEntero, Me.dtpFecha, Me.cmbEstados, Me.txtQuienatendio, Me.Label16, Me.txtCualseentero, Me.Label15, Me.Label14, Me.txtPassword1, Me.Label13, Me.txtPassword, Me.Label12, Me.Label10, Me.txtEmail, Me.Label9, Me.txtFax, Me.Label7, Me.txtCodigoPostal, Me.Label5, Me.txtCelular, Me.txtCiudad, Me.txtTelefono, Me.Label3, Me.Label8, Me.txtDireccion, Me.Label6, Me.txtColonia, Me.Label4, Me.Label1, Me.Label2, Me.txtNombre, Me.lblNombre})
        Me.Tabs.SharedControlsPage = Me.UltraTabSharedControlsPage1
        Me.Tabs.Size = New System.Drawing.Size(495, 490)
        Me.Tabs.TabIndex = 0
        UltraTab3.Key = "agencias"
        UltraTab3.TabPage = Me.UltraTabPageControl3
        UltraTab3.Text = "Agencias de Viajes"
        UltraTab1.Key = "clientes"
        UltraTab1.TabPage = Me.UltraTabPageControl1
        UltraTab1.Text = "Clientes Frecuentes"
        UltraTab2.Key = "professionals"
        UltraTab2.TabPage = Me.UltraTabPageControl2
        UltraTab2.Text = "Empresas"
        Me.Tabs.Tabs.AddRange(New Infragistics.Win.UltraWinTabControl.UltraTab() {UltraTab3, UltraTab1, UltraTab2})
        Me.Tabs.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007
        '
        'AltaSocios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(495, 531)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_registrar)
        Me.Controls.Add(Me.Tabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AltaSocios"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Alta de Socios"
        Me.UltraTabPageControl3.ResumeLayout(False)
        Me.UltraTabPageControl3.PerformLayout()
        CType(Me.txtPuestoAg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRFC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRazonSocial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtQuienatendio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCualseentero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPassword1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPassword, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoPostal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCelular, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCiudad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtColonia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabPageControl1.ResumeLayout(False)
        Me.UltraTabPageControl1.PerformLayout()
        CType(Me.txtApellidos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabPageControl2.ResumeLayout(False)
        Me.UltraTabPageControl2.PerformLayout()
        CType(Me.txtApellidosProf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProfessionals.ResumeLayout(False)
        Me.pnlProfessionals.PerformLayout()
        CType(Me.txtPuesto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tabs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tabs.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_registrar As System.Windows.Forms.Button
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents UltraTabPageControl3 As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Friend WithEvents UltraTabSharedControlsPage1 As Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
    Friend WithEvents Tabs As Infragistics.Win.UltraWinTabControl.UltraTabControl
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbEstados As System.Windows.Forms.ComboBox
    Friend WithEvents txtQuienatendio As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtCualseentero As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPassword1 As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtFax As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoPostal As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCelular As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txtCiudad As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txtTelefono As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtColonia As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents UltraTabPageControl1 As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Friend WithEvents UltraTabPageControl2 As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Friend WithEvents txtApellidos As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtApellidosProf As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents pnlProfessionals As System.Windows.Forms.Panel
    Friend WithEvents txtPuesto As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoSocio As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoSocioP As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cmbComoSeEntero As System.Windows.Forms.ComboBox
    Friend WithEvents txtPuestoAg As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtRFC As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtRazonSocial As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtAgencia As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents cmbProcedenciasAg As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbProcedenciasCl As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cmbProcedenciasPr As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
End Class
