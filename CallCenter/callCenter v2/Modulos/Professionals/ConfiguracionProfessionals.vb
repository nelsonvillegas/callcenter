Public Class ConfiguracionProfessionals
    Private Sub ConfiguracionProfessionals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub btn_guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_guardar.Click
        If guardar Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0181_infoActualizada"), CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0182_errCamWS") + vbCrLf + CapaPresentacion.Idiomas.get_str("str_0183_errReadFile") + " " + My.Application.Info.DirectoryPath + "\" + CapaAccesoDatos.XML.localData.XMLfile_name, CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Function load_data() As Boolean
        Try
            txtServidor.Text = CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER
            txtUsuario.Text = CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER
            txtContrase�a.Text = CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD
            txtBD.Text = CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0090", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Function

    Private Function guardar() As Boolean
        Try
            CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER = txtServidor.Text
            CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER = txtUsuario.Text
            CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD = txtContrase�a.Text
            CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB = txtBD.Text
            busca_updates = True
            Return True
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0091", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return False
        End Try
    End Function
End Class