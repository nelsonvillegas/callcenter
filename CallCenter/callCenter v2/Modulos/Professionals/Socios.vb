Imports System.Data.SqlClient

Public Class Socios
    Private MI_THREAD As System.Threading.Thread
    ' Private termino_tread As Boolean = True
    
    Public Cliente As String
    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
    End Sub

    Public Sub load_data()
        UltraGrid1.DataSource = Nothing

        Me.Cursor = Cursors.WaitCursor
        PictureBox2.Visible = True
        Call CargaSocios()
        Me.PictureBox2.Visible = False
        Me.Cursor = Cursors.Default
        'If MI_THREAD IsNot Nothing Then
        '    MI_THREAD.Abort()
        '    MI_THREAD = Nothing
        'End If

        ''termino_tread = False
        'MI_THREAD = New System.Threading.Thread(AddressOf CargaSocios)
        'MI_THREAD.Start(New Object() {Me})
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow
        'UltraGrid1.ActiveRow = e.Row
        'misForms.exe_accion(ContextMenuStrip1, "btn_detalles")
    End Sub

    Private Sub reservaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmbPrograma.SelectedIndex = 0
        TestConnection()
    End Sub


    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        Me.load_data()
    End Sub

    Private Sub TestConnection()
        Dim conn As New SqlConnection()
        'Dim frm As Socios = param(0)
        If CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER.Trim = "" Or _
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER.Trim = "" Or _
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD.Trim = "" Or _
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB.Trim = "" Then
            MsgBox("El servidor SQL no se ha configurado")
            btn_actualizar.Enabled = False
            Exit Sub
        End If
        Try
            Dim sConn As String = "server=[SERVER];User id=[USER];password=[PASS];database=[DB]"
            sConn = sConn.Replace("[SERVER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER)
            sConn = sConn.Replace("[USER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER)
            sConn = sConn.Replace("[PASS]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            sConn = sConn.Replace("[DB]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB)
            conn.ConnectionString = sConn

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT Getdate()"
            cmd.Connection = conn

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim strDate As String = cmd.ExecuteScalar

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        Catch ex As Exception
            conn.Close()
            MsgBox("El servidor SQL no responde, verifique la configuracion")
            btn_actualizar.Enabled = False
        End Try
    End Sub

    Private Sub CargaSocios() '(ByVal param As Object)
        Dim conn As New SqlConnection()
        'Dim frm As Socios = param(0)
        Try
            Dim sConn As String = "server=[SERVER];User id=[USER];password=[PASS];database=[DB]"
            sConn = sConn.Replace("[SERVER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER)
            sConn = sConn.Replace("[USER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER)
            sConn = sConn.Replace("[PASS]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            sConn = sConn.Replace("[DB]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB)
            conn.ConnectionString = sConn

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            Dim idProg As Integer
            Select Case cmbPrograma.SelectedIndex
                Case 0
                    idProg = 2
                Case 1
                    idProg = 3
                Case 2
                    idProg = 1
            End Select
            cmd.CommandText = "SELECT idSocioPrograma as Socio,NombreSocio as Nombre,Apellidos,Empresa,Puesto,Direccion,Colonia,CodigoPostal,Telefono,Email,SaldoDisponible as Saldo,RazonSocial,RFC from tblprofsocios  WHERE IDPrograma=" & idProg & " AND (nombresocio+' '+isnull(apellidos,'') LIKE '%" & txtNombre.Text.Trim & "%' or empresa like '%" & txtNombre.Text & "%')"
            cmd.Connection = conn

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            Dim dt As New DataTable
            da.Fill(dt)

            Me.UltraGrid1.DataSource = dt
            For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns

                col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
            Next

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            conn.Close()
            Me.UltraGrid1.DataSource = New DataTable
        End Try
        'FIJA_PIC(frm, frm.PictureBox2, False)

    End Sub

    Delegate Sub FIJA_PIC_Callback(ByVal frm As Socios, ByVal pic As PictureBox, ByVal vis As Boolean)
    Private Shared Sub FIJA_PIC(ByVal frm As Socios, ByVal pic As PictureBox, ByVal vis As Boolean)
        Try
            If pic.InvokeRequired Then
                Dim d As New FIJA_PIC_Callback(AddressOf FIJA_PIC)
                frm.Invoke(d, New Object() {frm, pic, vis})
            Else
                pic.Visible = vis
            End If
        Catch ex As Exception
            'MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0276_errModObjeto"), CapaPresentacion.Idiomas.get_ str("str_ 0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class