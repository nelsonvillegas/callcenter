Imports System.Data.SqlClient

Public Class AltaSocios
    Dim strConn As String
    Private MI_THREAD As System.Threading.Thread
    Public UltimoCreado As String
    Public UltimoCreadoNombre As String

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub


    Private Sub CargaProcedencias(ByVal idPrograma As Integer, ByVal cmb As ComboBox)
        Dim conn As New SqlConnection()
        Try
            conn.ConnectionString = strConn
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT procedencia,idProcedencia  FROM dbo.tblprofprocedencias WHERE IDPrograma=" & idPrograma & " AND estatus='A'"
            cmd.Connection = conn

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim dr As SqlDataReader = cmd.ExecuteReader
            cmb.Items.Clear()
            While dr.Read
                cmb.Items.Add(New ListData(dr("procedencia"), dr("idPRocedencia")))
            End While

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            conn.Close()
        End Try
    End Sub

    Private Sub CargaEstados()
        Dim conn As New SqlConnection()
        Try
            conn.ConnectionString = strConn
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT estado,idEstado FROM dbo.tblprofestados"
            cmd.Connection = conn

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim dr As SqlDataReader = cmd.ExecuteReader
            cmbEstados.Items.Clear()
            While dr.Read
                cmbEstados.Items.Add(New ListData(dr("estado"), dr("idEstado")))
            End While

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            conn.Close()
        End Try
    End Sub

    Private Function GuardarSocio() As Boolean
        Dim conn As New SqlConnection()
        Dim trans As Boolean = False
        Dim sqltrans As SqlTransaction
        Try
            Dim NumeroProfessionals As String
            Dim sql As String
            Dim folio As Integer
            Dim idProc As String
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    idProc = cmbProcedenciasAg.SelectedItem._value
                    NumeroProfessionals = NuevaClaveSocio(cmbProcedenciasAg.SelectedItem._value, folio)
                    Me.UltimoCreadoNombre = txtNombre.Text
                    sql = "INSERT INTO tblprofSocios (IDSocioPrograma, IDPrograma, Empresa, NombreSocio, Puesto, Direccion, Ciudad, Colonia, IDEstado, CodigoPostal,Telefono, Fax, Celular,IDProcedencia, Email, Comoseentero, Cualseentero, FechaNacimiento,Password,  IDTipoSocio,RazonSocial,RFC, SaldoInicial,SaldoReserva,SaldoDisponible, FechaAlta)"
                    sql = sql & "Values("
                    sql = sql & "'" & NumeroProfessionals & "',"
                    sql = sql & "2,"
                    sql = sql & "'" & txtAgencia.Text & "',"
                    sql = sql & "'" & txtNombre.Text & "',"
                    sql = sql & "'" & txtPuestoAg.Text & "',"
                    sql = sql & "'" & txtDireccion.Text & "',"
                    sql = sql & "'" & txtCiudad.Text & "',"
                    sql = sql & "'" & txtColonia.Text & "',"
                    sql = sql & "" & cmbEstados.SelectedItem._value & ","
                    sql = sql & "'" & txtCodigoPostal.Text & "',"
                    sql = sql & "'" & txtTelefono.Text & "',"
                    sql = sql & "'" & txtFax.Text & "',"
                    sql = sql & "'" & txtCelular.Text & "',"
                    sql = sql & "" & cmbProcedenciasAg.SelectedItem._value & ","
                    sql = sql & "'" & txtEmail.Text & "',"
                    sql = sql & "'" & cmbComoSeEntero.Text & "',"
                    sql = sql & "'" & txtCualseentero.Text & "',"
                    'sql = sql & "'" & txtFechaNacimiento",","	
                    sql = sql & "'" & dtpFecha.Value.ToString("yyyyMMdd") & "',"
                    sql = sql & "'" & txtPassword1.Text & "',"
                    sql = sql & "'" & IIf(cmbTipoSocio.SelectedIndex = 0, "I", "E") & "',"
                    sql = sql & "'" & txtRazonSocial.Text & "',"
                    sql = sql & "'" & txtRFC.Text & "',"
                    sql = sql & "0,"
                    sql = sql & "0,"
                    sql = sql & "600,"
                    sql = sql & "'" & Now.ToString("yyyyMMdd") & "')"
                Case "clientes"
                    idProc = cmbProcedenciasCl.SelectedItem._value
                    NumeroProfessionals = NuevaClaveSocio(cmbProcedenciasCl.SelectedItem._value, folio)
                    Me.UltimoCreadoNombre = txtNombre.Text & " " & txtApellidos.Text
                    sql = "INSERT INTO tblprofSocios (IDSocioPrograma, IDPrograma,  NombreSocio, Apellidos, Direccion, Ciudad, Colonia, IDEstado, CodigoPostal,Telefono, Fax, Celular,IDProcedencia, Email, Comoseentero, Cualseentero, Quienatendio,  FechaNacimiento,Password,  IDTipoSocio, SaldoInicial,SaldoReserva,SaldoDisponible, FechaAlta)"
                    sql = sql & "Values("
                    sql = sql & "'" & NumeroProfessionals & "',"
                    sql = sql & "3,"
                    sql = sql & "'" & txtNombre.Text & "',"
                    sql = sql & "'" & txtApellidos.Text & "',"
                    sql = sql & "'" & txtDireccion.Text & "',"
                    sql = sql & "'" & txtCiudad.Text & "',"
                    sql = sql & "'" & txtColonia.Text & "',"
                    sql = sql & "" & cmbEstados.SelectedItem._Value & ","
                    sql = sql & "'" & txtCodigoPostal.Text & "',"
                    sql = sql & "'" & txtTelefono.Text & "',"
                    sql = sql & "'" & txtFax.Text & "',"
                    sql = sql & "'" & txtCelular.Text & "',"
                    sql = sql & "" & cmbProcedenciasCl.SelectedItem._value & ","
                    sql = sql & "'" & txtEmail.Text & "',"
                    sql = sql & "'" & cmbComoSeEntero.Text & "',"
                    sql = sql & "'" & txtCualseentero.Text & "',"
                    sql = sql & "'" & txtQuienatendio.Text & "',"
                    sql = sql & "'" & dtpFecha.Value.ToString("yyyyMMdd") & "',"
                    sql = sql & "'" & txtPassword1.Text & "',"
                    sql = sql & "'I',"
                    sql = sql & "0,"
                    sql = sql & "0,"
                    sql = sql & "600,"
                    sql = sql & "'" & Now.ToString("yyyyMMdd") & "')"

                Case "professionals"
                    idProc = cmbProcedenciasPr.SelectedItem._value
                    NumeroProfessionals = NuevaClaveSocio(cmbProcedenciasPr.SelectedItem._value, folio)
                    Me.UltimoCreadoNombre = txtNombre.Text & " " & txtApellidosProf.Text
                    sql = "INSERT INTO tblprofSocios (IDSocioPrograma, IDPrograma, Empresa, NombreSocio, Apellidos, Puesto, Direccion, Ciudad, Colonia, IDEstado, CodigoPostal,Telefono, Fax, Celular,IDProcedencia, Email, Comoseentero, Cualseentero, Quienatendio, FechaNacimiento, Password, IDTipoSocio, FechaAlta,SaldoDisponible)"
                    sql = sql & "Values("
                    sql = sql & "'" & NumeroProfessionals & "',"
                    sql = sql & "1,"
                    sql = sql & "'" & txtEmpresa.Text & "',"
                    sql = sql & "'" & txtNombre.Text & "',"
                    sql = sql & "'" & txtApellidosProf.Text & "',"
                    sql = sql & "'" & txtPuesto.Text & "',"
                    sql = sql & "'" & txtDireccion.Text & "',"
                    sql = sql & "'" & txtCiudad.Text & "',"
                    sql = sql & "'" & txtColonia.Text & "',"
                    sql = sql & "" & cmbEstados.SelectedItem._value & ","
                    sql = sql & "'" & txtCodigoPostal.Text & "',"
                    sql = sql & "'" & txtTelefono.Text & "',"
                    sql = sql & "'" & txtFax.Text & "',"
                    sql = sql & "'" & txtCelular.Text & "',"
                    sql = sql & "" & cmbProcedenciasPr.SelectedItem._value & ","
                    sql = sql & "'" & txtEmail.Text & "',"
                    sql = sql & "'" & cmbComoSeEntero.Text & "',"
                    sql = sql & "'" & txtCualseentero.Text & "',"
                    sql = sql & "'" & txtQuienatendio.Text & "',"
                    sql = sql & "'" & dtpFecha.Value.ToString("yyyyMMdd") & "',"
                    sql = sql & "'" & txtPassword1.Text & "',"
                    sql = sql & "'" & IIf(cmbTipoSocioP.SelectedIndex = 0, "I", "E") & "',"
                    sql = sql & "'" & Now.ToString("yyyyMMdd") & "',600)"
            End Select


            conn.ConnectionString = strConn

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = sql
            cmd.Connection = conn

            Dim rowCount As Integer
            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
                sqltrans = conn.BeginTransaction
                cmd.Transaction = sqltrans
                trans = True
            End If
            
            rowCount = cmd.ExecuteNonQuery()
            If rowCount > 0 Then
                cmd.CommandText = "SELECT @@IDENTITY"
                Dim id As String = cmd.ExecuteScalar
                If id <> "" Then
                    sql = "INSERT INTO tblproftransacciones(IDTipoTransaccion, IDSocio, IDprograma,  Fecha, TotalPuntos,  FechaHoraTransaccion, IDClaveUsuario) "
                    sql = sql & "Values ('B', " & id & ", 1,  GetDate(),  600,  GetDate(),  'ipiedras') "
                    cmd.CommandText = sql
                    rowCount = cmd.ExecuteNonQuery
                    If rowCount > 0 Then
                        sql = "update tblprofprocedencias set Ultimofolio=" & folio & " where idProcedencia=" & idProc
                        cmd.CommandText = sql
                        rowCount = cmd.ExecuteNonQuery
                        If rowCount > 0 Then
                            sqltrans.Commit()
                            'hacer asincrono
                            MI_THREAD = New System.Threading.Thread(AddressOf EnviaCorreo)
                            MI_THREAD.Start(New Object() {NumeroProfessionals})
                            UltimoCreado = NumeroProfessionals
                            MsgBox("Registrado Correctamente" & vbCrLf & "El numero de Socio asignado es :" & NumeroProfessionals)
                            Close()
                        Else
                            sqltrans.Rollback()
                            MsgBox("No se pudo actualizar el folio de procedencias")
                        End If
                    Else
                        sqltrans.Rollback()
                        MsgBox("No se pudo registrar transaccion de puntos")
                    End If
                Else
                    sqltrans.Rollback()
                    MsgBox("No se pudo obtener id del socio")
                End If
            Else
                sqltrans.Rollback()
                MsgBox("No se pudo registrar socio")
            End If

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If

            Return True
        Catch ex As Exception
            'CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            If trans Then sqltrans.Rollback()
            MsgBox(ex.Message)
            conn.Close()
            Return False
        End Try
    End Function

    Private Function ExisteSocio(Optional ByVal Empresa As Boolean = False) As Boolean
        Dim conn As New SqlConnection()
        Try
            conn.ConnectionString = strConn
            Dim idPrograma As Integer = 0
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    idPrograma = 2
                Case "clientes"
                    idPrograma = 3
                Case "professionals"
                    idPrograma = 1
            End Select
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    'If cmbTipoSocio.SelectedIndex = 0 Then
                    'Else
                    If Empresa Then
                        cmd.CommandText = "SELECT idSocio From tblProfSocios where empresa='" & txtAgencia.Text.Trim & "' and idPrograma=2"
                    Else
                        cmd.CommandText = "SELECT idSocio From tblProfSocios where nombresocio='" & txtNombre.Text.Trim & "' and idPrograma=2"
                    End If

                    'End If

                Case "clientes"
                    cmd.CommandText = "SELECT idSocio From tblProfSocios where nombresocio='" & txtNombre.Text.Trim & "' and apellidos='" & txtApellidos.Text & "' and idPrograma=3"
                Case "professionals"
                    If Empresa Then
                        cmd.CommandText = "SELECT idSocio From tblProfSocios where empresa='" & txtEmpresa.Text & "' and idPrograma=1"
                    Else
                        cmd.CommandText = "SELECT idSocio From tblProfSocios where nombresocio='" & txtNombre.Text.Trim & "' and apellidos='" & txtApellidosProf.Text & "' and idPrograma=1"
                    End If

            End Select

            cmd.Connection = conn

            Dim idSocio As Integer
            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            idSocio = cmd.ExecuteScalar


            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
            If idSocio > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            conn.Close()
            Return True
        End Try
    End Function

    Private Function NuevaClaveSocio(ByVal idProcedencia As Integer, ByRef rFolio As Integer) As String
        Dim conn As New SqlConnection()
        Try
            conn.ConnectionString = strConn
            Dim folio As String = ""

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT siglas,ultimofolio  FROM dbo.tblprofprocedencias WHERE IDProcedencia=" & idProcedencia
            cmd.Connection = conn

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.Read Then
                rFolio = dr("ultimofolio") + 1
                folio = dr("siglas") & (rFolio)
            End If

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
            Return folio
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0033", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            conn.Close()
            Return ""
        End Try
    End Function

    Private Sub AltaSocios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UltimoCreado = ""
        Dim sConn As String = "server=[SERVER];User id=[USER];password=[PASS];database=[DB]"
        sConn = sConn.Replace("[SERVER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER)
        sConn = sConn.Replace("[USER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER)
        sConn = sConn.Replace("[PASS]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
        sConn = sConn.Replace("[DB]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB)
        strConn = sConn
        If TestConnection() Then
            cmbProcedenciasPr.DisplayMember = "Name"
            cmbProcedenciasPr.ValueMember = "_Value"
            cmbProcedenciasCl.DisplayMember = "Name"
            cmbProcedenciasCl.ValueMember = "_Value"
            cmbProcedenciasAg.DisplayMember = "Name"
            cmbProcedenciasAg.ValueMember = "_Value"
            cmbEstados.DisplayMember = "Name"
            cmbEstados.ValueMember = "_Value"
            Call CargaProcedencias(1, cmbProcedenciasPr)
            Call CargaProcedencias(2, cmbProcedenciasAg)
            Call CargaProcedencias(3, cmbProcedenciasCl)
            Call CargaEstados()
            If cmbProcedenciasPr.Items.Count > 0 Then cmbProcedenciasPr.SelectedIndex = 0
            If cmbProcedenciasCl.Items.Count > 0 Then cmbProcedenciasCl.SelectedIndex = 0
            If cmbProcedenciasAg.Items.Count > 0 Then cmbProcedenciasAg.SelectedIndex = 0
            If cmbEstados.Items.Count > 0 Then cmbEstados.SelectedIndex = 0
            cmbTipoSocio.SelectedIndex = 0
            cmbTipoSocioP.SelectedIndex = 0
            cmbComoSeEntero.SelectedIndex = 0
            Call Habilita()
        Else
            Me.btn_registrar.Enabled = False
        End If

    End Sub

    Private Sub btn_registrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_registrar.Click
        If Valida() Then
            Dim guardar As Boolean = False
            Select Case Tabs.SelectedTab.Key
                Case "agencias"
                    If Not ExisteSocio() And Not ExisteSocio(True) Then guardar = True
                Case "clientes"
                    If Not ExisteSocio() Then guardar = True
                Case "professionals"
                    If Not ExisteSocio() And Not ExisteSocio(True) Then guardar = True
            End Select
            If guardar Then
                Call GuardarSocio()
            Else
                MsgBox("Ya existe el socio en el programa seleccionado")
            End If
        End If
    End Sub

    Private Function Valida() As Boolean
        If txtNombre.Text.Trim = "" Then
            MsgBox("Nombre del Socio es requerido")
            Return False
        End If
        If txtDireccion.Text.Trim = "" Then
            MsgBox("Direccion es requerida")
            Return False
        End If
        If txtCiudad.Text.Trim = "" Then
            MsgBox("Ciudad es requerida")
            Return False
        End If
        If txtCodigoPostal.Text.Trim = "" Then
            MsgBox("Codigo Postal es requerido")
            Return False
        End If
        If txtTelefono.Text.Trim = "" Then
            MsgBox("Telefono es requerido")
            Return False
        End If
        If txtEmail.Text.Trim = "" Then
            MsgBox("Email es requerido")
            Return False
        End If
        If txtPassword.Text.Trim = "" Then
            MsgBox("Password es requerido")
            Return False
        End If
        If txtPassword.Text.Trim <> txtPassword1.Text Then
            MsgBox("El Password y la confirmacion no coinciden")
            Return False
        End If
        Select Case Tabs.SelectedTab.Key
            Case "agencias"
                If txtAgencia.Text.Trim = "" Then
                    MsgBox("Nombre de Agencia es requerida")
                    Return False
                End If
                If txtPuestoAg.Text.Trim = "" Then
                    MsgBox("Puesto es requerido")
                    Return False
                End If
            Case "clientes"
            Case "professionals"
                If txtApellidosProf.Text.Trim = "" Then
                    MsgBox("Apellidos es requerido")
                    Return False
                End If
                If txtEmpresa.Text.Trim = "" Then
                    MsgBox("Empresa es requerida")
                    Return False
                End If
                If txtPuesto.Text.Trim = "" Then
                    MsgBox("Puesto es requerido")
                    Return False
                End If
        End Select
        Return True
    End Function

    Private Sub Habilita()
        txtNombre.Enabled = True
        txtApellidos.Enabled = True
        txtApellidosProf.Enabled = True
        txtAgencia.Enabled = True
        txtEmpresa.Enabled = True
        txtColonia.Enabled = True
        txtPuesto.Enabled = True
        txtPuestoAg.Enabled = True
        txtDireccion.Enabled = True
        txtCiudad.Enabled = True
        txtCodigoPostal.Enabled = True
        txtTelefono.Enabled = True
        txtFax.Enabled = True
        txtEmail.Enabled = True
        txtCualseentero.Enabled = True
        txtQuienatendio.Enabled = True
        txtCelular.Enabled = True
        txtPassword.Enabled = True
        txtPassword1.Enabled = True
        txtRFC.Enabled = True
        txtRazonSocial.Enabled = True
    End Sub

    Private Sub EnviaCorreo(ByVal param As Object)
        Dim NumeroProfessionals As String = param(0)
        Dim message As String
        Dim subject As String
        Select Case Tabs.SelectedTab.Key
            Case "agencias"
                subject = "Inscripci�n a Professional's Misi�n Agencias de Viajes"
                message = "Estimado (a) Agente:" & txtNombre.Text & " " & txtApellidos.Text & vbCrLf & vbCrLf
                message = message & "Le damos la mas cordial bienvenida al Programa de Professional's Misi�n, Agencias de Viajes. " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Su clave de socio Professional's Misi�n Agencias de Viajes es: " & NumeroProfessionals & vbCrLf
                message = message & "Su contrase�a para entrar al sistema es: " & txtPassword1.Text & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Usted podr� consultar su saldo y operaciones, asi como solicitar sus reservaciones y premios en http://www.hotelesmision.com/professionals" & vbCrLf
                message = message & "Hoteles Misi�n a trav�s de su programa de cliente frecuente Professional's Misi�n Agencias de Viajes." & vbCrLf
                message = message & "le brinda los siguientes beneficios: " & vbCrLf
                message = message & " " & vbCrLf
                message = message & "- 600 puntos como bono de regalo" & vbCrLf
                message = message & "- Tarifas preferenciales " & vbCrLf
                message = message & "- Invitaci�n a eventos especiales" & vbCrLf
                message = message & "- Puntaje adicional por promociones" & vbCrLf
                message = message & "- Sorteos y Rifas " & vbCrLf
                message = message & "- Acceso en linea" & vbCrLf
                message = message & "- Participaci�n en promociones especiales" & vbCrLf
                message = message & "- Participar en Fam Trips (viajes) para conocer nuestros hoteles. " & vbCrLf
                message = message & " " & vbCrLf
                message = message & "Recuerde que por cada cuarto noche efectivo que usted registre en alguna propiedad de Hoteles Misi�n, " & vbCrLf
                message = message & "obtiene 100 puntos acumulables los cuales podr� canjear por monederos electr�nicos Liverpool o F�bricas de Francia, Cifra Walmart o Sanborns, y   " & vbCrLf
                message = message & "as� como estancias en los Hoteles Misi�n , solicitando su premio por medio de nuestra p�gina." & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Asimismo le recordamos que usted podr� realizar sus reservaciones a trav�s de nuestra central de  Reservaciones en los tel�fonos: 5209 1770 � 01800 900 3800 disponible las 24 hrs. del d�a, por medio de nuestra p�gina de Internet en www.hotelesmision.com llenando los datos que se solicitan, o al llegar al hotel con solo indicar su Clave de Socio Professional's Misi�n Agencias de Viajes para que el hotel reporte su producci�n de cuartos noche, y autom�ticamente sean registrados sus puntos en la base de datos del programa Professional�s Misi�n, Agencias de Viajes.  " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Hoteles Misi�n agradece su preferencia, y le reiteramos nuestro deseo de atenderle en sus pr�ximos viajes de negocios o de placer ya que Hoteles Misi�n tiene El 'Arte de la Hospitalidad'.  " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "A T E N T A M E N T E " & vbCrLf
                message = message & "Fabiola Guerrero" & vbCrLf
                message = message & "Gerente de Programas de Lealtad" & vbCrLf
                message = message & "Hoteles Misi�n" & vbCrLf
                message = message & "El arte de la Hospitalidad" & vbCrLf
                message = message & "Praga #60 Col. Juarez DF" & vbCrLf
                message = message & "professionals@hotelesmision.com.mx" & vbCrLf
                message = message & "www.hotelesmision.com" & vbCrLf
                message = message & "Cel: 04455 4193 4118 " & vbCrLf
                message = message & "Tel: (55)520917734  " & vbCrLf
                message = message & "Del interior sin costo: 01 800 900 38 00 ext. 2304 " & vbCrLf
            Case "clientes"
                subject = "Inscripci�n a Professional's Misi�n Clientes Frecuentes"
                message = "Estimado (a) Socio(a):" & txtNombre.Text & " " & txtApellidos.Text & vbCrLf & vbCrLf
                message = message & "Le damos la mas cordial bienvenida al Programa de Professional's Misi�n, Cliente Frecuente. " & vbCrLf
                message = message & " " & vbCrLf
                message = message & "Su clave de socio Professional's Misi�n  Cliente Frecuente es: " & NumeroProfessionals & vbCrLf
                message = message & "Su contrase�a para entrar al sistema es: " & txtPassword1.Text & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Usted podr� consultar su saldo y operaciones, asi como solicitar sus reservaciones y premios en http://www.hotelesmision.com/professionals" & vbCrLf
                message = message & "Hoteles Misi�n a trav�s de su programa de Professional's Misi�n Cliente Frecuente." & vbCrLf
                message = message & "le brinda los siguientes beneficios: " & vbCrLf
                message = message & " " & vbCrLf
                message = message & "- 600 puntos como bono de regalo" & vbCrLf
                message = message & "- Tarifas preferenciales " & vbCrLf
                message = message & "- Invitaci�n a eventos especiales" & vbCrLf
                message = message & "- Puntaje adicional por promociones" & vbCrLf
                message = message & "- Sorteos y Rifas " & vbCrLf
                message = message & "- Acceso en linea" & vbCrLf
                message = message & "- Participaci�n en promociones especiales" & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Recuerde que por cada cuarto noche efectivo que usted registre en alguna propiedad de Hoteles Misi�n, " & vbCrLf
                message = message & "obtiene 100 puntos acumulables los cuales podr� canjear por monederos electr�nicos Liverpool o F�bricas de Francia, Cifra Walmart o Sanborns, y   " & vbCrLf
                message = message & "as� como estancias en los Hoteles Misi�n , solicitando su premio por medio de nuestra p�gina." & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Asimismo le recordamos que usted podr� realizar sus reservaciones a trav�s de nuestra central de  Reservaciones en los tel�fonos: 5209 1770 � 01800 900 3800 disponible las 24 hrs. del d�a, por medio de nuestra p�gina de Internet en www.hotelesmision.com llenando los datos que se solicitan, o al llegar al hotel con solo indicar su Clave de Socio Professional�s Misi�n Cliente Frecuente para que el hotel reporte su producci�n de cuartos noche, y autom�ticamente sean registrados sus puntos en la base de datos del programa Professional�s Misi�n, Cliente Frecuente. " & vbCrLf
                message = message & "  " & vbCrLf
                message = message & "Hoteles Misi�n agradece su preferencia, y le reiteramos nuestro deseo de atenderle en sus pr�ximos viajes de negocios o de placer ya que Hoteles Misi�n tiene 'El Arte de la Hospitalidad'.  " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "A T E N T A M E N T E " & vbCrLf
                message = message & "Fabiola Guerrero" & vbCrLf
                message = message & "Gerente de Programas de Lealtad" & vbCrLf
                message = message & "Hoteles Misi�n" & vbCrLf
                message = message & "El arte de la Hospitalidad" & vbCrLf
                message = message & "Praga #60 Col. Juarez DF" & vbCrLf
                message = message & "professionals@hotelesmision.com.mx" & vbCrLf
                message = message & "www.hotelesmision.com" & vbCrLf
                message = message & "Cel: 04455 4193 4118 " & vbCrLf
                message = message & "Tel: (55)52091734  " & vbCrLf
                message = message & "Del interior sin costo: 01 800 900 38 00 ext. 2304 " & vbCrLf
            Case "professionals"
                subject = "Inscripci�n a Professional's Misi�n Profesionales con Decisi�n"
                message = "Estimado(a) Socio(a)  " & txtNombre.Text & " " & txtApellidos.Text & "/" & txtEmpresa.Text & vbCrLf & vbCrLf
                message = message & "Le damos la mas cordial bienvenida al Programa de Professional's Mision, Profesionales con Decisi�n.  " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Su clave de socio Professional's Misi�n Profesionales con Decisi�n es: " & NumeroProfessionals & vbCrLf
                message = message & "Su contrase�a para entrar al sistema es: " & txtPassword1.Text & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Usted podr� consultar su saldo y operaciones, asi como solicitar sus reservaciones y premios en http://www.hotelesmision.com/professionals" & vbCrLf
                message = message & "Hoteles Misi�n a trav�s de su programa de cliente frecuente Professional's Misi�n Profesionales con Decisi�n." & vbCrLf
                message = message & "le brinda los siguientes beneficios: " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "- 600 puntos como bono de regalo" & vbCrLf
                message = message & "- Tarifas preferenciales " & vbCrLf
                message = message & "- Invitaci�n a eventos especiales" & vbCrLf
                message = message & "- Puntaje adicional por promociones" & vbCrLf
                message = message & "- Sorteos y Rifas " & vbCrLf
                message = message & "- Acceso en linea" & vbCrLf
                message = message & "- Participaci�n en promociones especiales" & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Recuerde que por cada cuarto noche efectivo que usted registre en alguna propiedad de Hoteles Misi�n, " & vbCrLf
                message = message & "obtiene 100 puntos acumulables los cuales podr� canjear por monederos electr�nicos Liverpool o F�bricas de Francia, Cifra Walmart o Sanborns, y   " & vbCrLf
                message = message & "as� como estancias en los Hoteles Misi�n , solicitando su premio por medio de nuestra p�gina." & vbCrLf
                message = message & " " & vbCrLf
                message = message & "Asimismo le recordamos que usted podr� realizar sus reservaciones a trav�s de nuestra central de  Reservaciones en los tel�fonos: 5209 1770 � 01800 900 3800 disponible las 24 hrs. del d�a, por medio de nuestra p�gina de Internet en www.hotelesmision.com llenando los datos que se solicitan, o al llegar al hotel con solo indicar su Clave de Socio Professional's Misi�n Profesionales con Decisi�n para que el hotel reporte su producci�n de cuartos noche, y autom�ticamente sean registrados sus puntos en la base de datos del programa Professional�s Misi�n, Profesionales con Decisi�n.  " & vbCrLf
                message = message & "" & vbCrLf
                message = message & "Hoteles Misi�n agradece su preferencia, y le reiteramos nuestro deseo de atenderle en sus pr�ximos viajes de negocios o de placer ya que Hoteles Misi�n tiene 'El Arte de la Hospitalidad'" & vbCrLf
                message = message & "" & vbCrLf
                message = message & "A T E N T A M E N T E " & vbCrLf
                message = message & "Fabiola Guerrero" & vbCrLf
                message = message & "Gerente de Programas de Lealtad" & vbCrLf
                message = message & "Hoteles Misi�n" & vbCrLf
                message = message & "El arte de la Hospitalidad" & vbCrLf
                message = message & "Praga #60 Col. Juarez DF" & vbCrLf
                message = message & "professionals@hotelesmision.com.mx" & vbCrLf
                message = message & "www.hotelesmision.com" & vbCrLf
                message = message & "Cel: 04455 4193 4118 " & vbCrLf
                message = message & "Tel: (55)52091734 " & vbCrLf
                message = message & "Del interior sin costo: 01 800 900 38 00 ext. 2304 " & vbCrLf
        End Select

        Dim mailMessage As System.Web.Mail.MailMessage
        Try
            mailMessage = New System.Web.Mail.MailMessage
            mailMessage.From = """Sitio Hoteles Misi�n"" <info@hotelesmision.com.mx>"
            mailMessage.To = txtEmail.Text

            mailMessage.Bcc = "javier@oz.com.mx;jara@oz.com.mx"
            mailMessage.Subject = subject
            mailMessage.BodyFormat = Web.Mail.MailFormat.Text
            mailMessage.Body = message

            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)
            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "smtpuser")
            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "smtpauthed")
            mailMessage.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25)
            System.Web.Mail.SmtpMail.SmtpServer = "mail.univisit.com"
            System.Web.Mail.SmtpMail.Send(mailMessage)
        Catch ex As Exception

        End Try

        Select Case Tabs.SelectedTab.Key
            Case "agencias"
                subject = "Solicitud de Inscripci�n a Professional's Misi�n Agencias de Viajes"
                message = "La siguiente inscripci�n fu� mandada desde el Call Center " & vbCrLf
                message = message & "Nombre: " & txtNombre.Text & vbCrLf
                message = message & "Empresa: " & txtEmpresa.Text & vbCrLf
                message = message & "Puesto: " & txtPuestoAg.Text & vbCrLf
                message = message & "Direcci�n: " & txtDireccion.Text & vbCrLf
                message = message & "Colonia: " & txtColonia.Text & vbCrLf
                message = message & "Ciudad: " & txtCiudad.Text & vbCrLf
                message = message & "Estado: " & cmbEstados.Text & vbCrLf
                message = message & "C�digo Postal: " & txtCodigoPostal.Text & vbCrLf
                message = message & "Correo Electr�nico: " & txtEmail.Text & vbCrLf
                message = message & "Tel�fono: " & txtTelefono.Text & vbCrLf
                message = message & "Celular: " & txtCelular.Text & vbCrLf
                message = message & "Fax: " & txtFax.Text & vbCrLf
                message = message & "Password solicitado: " & txtPassword1.Text & vbCrLf
                message = message & "Razon Social: " & txtRazonSocial.Text & vbCrLf
                message = message & "RFC: " & txtRFC.Text & vbCrLf
            Case "clientes"
                subject = "Solicitud de Inscripci�n a Professional's Misi�n Clientes Frecuentes"
                message = "La siguiente inscripci�n fu� mandada desde el Call Center" & vbCrLf
                message = message & "Nombre: " & txtNombre.Text & vbCrLf
                message = message & "Apellidos: " & txtApellidos.Text & vbCrLf
                message = message & "Direcci�n: " & txtDireccion.Text & vbCrLf
                message = message & "Colonia: " & txtColonia.Text & vbCrLf
                message = message & "Ciudad: " & txtCiudad.Text & vbCrLf
                message = message & "Estado: " & cmbEstados.Text & vbCrLf
                message = message & "C�digo Postal: " & txtCodigoPostal.Text & vbCrLf
                message = message & "Correo Electr�nico: " & txtEmail.Text & vbCrLf
                message = message & "Tel�fono: " & txtTelefono.Text & vbCrLf
                message = message & "Celular: " & txtCelular.Text & vbCrLf
                message = message & "Fax: " & txtFax.Text & vbCrLf
                message = message & "Password solicitado: " & txtPassword1.Text & vbCrLf

            Case "professionals"
                subject = "Solicitud de Inscripci�n a Professionals"
                message = "La siguiente inscripci�n fu� mandada desde el Call Center" & vbCrLf
                message = message & "Nombre Completo: " & txtNombre.Text & " " & txtApellidos.Text & vbCrLf
                message = message & "Empresa: " & txtEmpresa.Text & vbCrLf
                message = message & "Puesto: " & txtPuesto.Text & vbCrLf
                message = message & "Direcci�n: " & txtDireccion.Text & vbCrLf
                message = message & "Colonia: " & txtColonia.Text & vbCrLf
                message = message & "Ciudad: " & txtCiudad.Text & vbCrLf
                message = message & "Estado: " & cmbEstados.Text & vbCrLf
                message = message & "C�digo Postal: " & txtCodigoPostal.Text & vbCrLf
                message = message & "Correo Electr�nico: " & txtEmail.Text & vbCrLf
                message = message & "Tel�fono: " & txtTelefono.Text & vbCrLf
                message = message & "Celular: " & txtCelular.Text & vbCrLf
                message = message & "Fax: " & txtFax.Text & vbCrLf
                message = message & "Fecha de Nacimiento: " & dtpFecha.Value.ToString("dd/MMM/yyyy") & vbCrLf
                message = message & "Password solicitado: " & txtPassword1.Text & vbCrLf
        End Select
        Dim mailMessage2 As System.Web.Mail.MailMessage
        Try
            mailMessage2 = New System.Web.Mail.MailMessage
            mailMessage2.From = """Sitio Hoteles Misi�n"" <info@hotelesmision.com.mx>"
            'TODO: poner el bueno al liberar en produccion
            mailMessage2.To = "professionals@hotelesmision.com.mx"
            'mailMessage2.To = "jara@oz.com.mx " '"professionals@hotelesmision.com.mx"

            mailMessage2.Bcc = "javier@oz.com.mx;jara@oz.com.mx"
            mailMessage2.Subject = subject
            mailMessage2.BodyFormat = Web.Mail.MailFormat.Text
            mailMessage2.Body = message

            mailMessage2.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1)
            mailMessage2.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "smtpuser")
            mailMessage2.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "smtpauthed")
            mailMessage2.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25)
            System.Web.Mail.SmtpMail.SmtpServer = "mail.univisit.com"
            System.Web.Mail.SmtpMail.Send(mailMessage2)
        Catch ex As Exception

        End Try

    End Sub

    Private Function TestConnection() As Boolean
        Dim conn As New SqlConnection()
        'Dim frm As Socios = param(0)
        If CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER.Trim = "" Or _
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER.Trim = "" Or _
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD.Trim = "" Or _
        CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB.Trim = "" Then
            MsgBox("El servidor SQL no se ha configurado")
            Return False
        End If

        Try

            Dim sConn As String = "server=[SERVER];User id=[USER];password=[PASS];database=[DB]"
            sConn = sConn.Replace("[SERVER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_SERVER)
            sConn = sConn.Replace("[USER]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_USER)
            sConn = sConn.Replace("[PASS]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_PASSWORD)
            sConn = sConn.Replace("[DB]", CapaAccesoDatos.XML.localData.ProfessionalsSQL_DB)
            conn.ConnectionString = sConn

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT Getdate()"
            cmd.Connection = conn

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            Dim strDate As String = cmd.ExecuteScalar

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
            Return True
        Catch ex As Exception
            conn.Close()
            MsgBox("El servidor SQL no responde, verifique la configuracion")
            Return False
        End Try
    End Function
End Class
