﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductionReport
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductionReport))
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook()
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbl_segmento = New System.Windows.Forms.Label()
        Me.cmbSegmento = New System.Windows.Forms.ComboBox()
        Me.lbl_llegada = New System.Windows.Forms.Label()
        Me.dtp_in = New System.Windows.Forms.DateTimePicker()
        Me.lbl_salida = New System.Windows.Forms.Label()
        Me.dtp_out = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbl_hotel = New System.Windows.Forms.Label()
        Me.txt_hotel = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.txt_agente = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.chkGroupByAgent = New Infragistics.Win.UltraWinEditors.UltraCheckEditor()
        Me.lbl_agente = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btn_actualizar = New Infragistics.Win.Misc.UltraButton()
        Me.btn_excelExport = New Infragistics.Win.Misc.UltraButton()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.uce_source = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraGridExcelExporter1 = New Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(Me.components)
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.CmbSearch1 = New callCenter.cmbSearch()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txt_hotel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_agente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_source, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(761, 132)
        Me.Panel1.TabIndex = 1
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox3)
        Me.FlowLayoutPanel1.Controls.Add(Me.uce_source)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1121, 160)
        Me.FlowLayoutPanel1.TabIndex = 21
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CmbSearch1)
        Me.GroupBox1.Controls.Add(Me.lblEmpresa)
        Me.GroupBox1.Controls.Add(Me.lbl_segmento)
        Me.GroupBox1.Controls.Add(Me.cmbSegmento)
        Me.GroupBox1.Controls.Add(Me.lbl_llegada)
        Me.GroupBox1.Controls.Add(Me.dtp_in)
        Me.GroupBox1.Controls.Add(Me.lbl_salida)
        Me.GroupBox1.Controls.Add(Me.dtp_out)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 2)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(10, 2, 2, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(291, 122)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'lbl_segmento
        '
        Me.lbl_segmento.AutoSize = True
        Me.lbl_segmento.BackColor = System.Drawing.Color.Transparent
        Me.lbl_segmento.Location = New System.Drawing.Point(5, 73)
        Me.lbl_segmento.Name = "lbl_segmento"
        Me.lbl_segmento.Size = New System.Drawing.Size(58, 13)
        Me.lbl_segmento.TabIndex = 17
        Me.lbl_segmento.Text = "Segmento:"
        '
        'cmbSegmento
        '
        Me.cmbSegmento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSegmento.FormattingEnabled = True
        Me.cmbSegmento.Location = New System.Drawing.Point(71, 70)
        Me.cmbSegmento.Name = "cmbSegmento"
        Me.cmbSegmento.Size = New System.Drawing.Size(121, 21)
        Me.cmbSegmento.TabIndex = 16
        '
        'lbl_llegada
        '
        Me.lbl_llegada.AutoSize = True
        Me.lbl_llegada.BackColor = System.Drawing.Color.Transparent
        Me.lbl_llegada.Location = New System.Drawing.Point(5, 16)
        Me.lbl_llegada.Name = "lbl_llegada"
        Me.lbl_llegada.Size = New System.Drawing.Size(41, 13)
        Me.lbl_llegada.TabIndex = 0
        Me.lbl_llegada.Text = "Desde:"
        '
        'dtp_in
        '
        Me.dtp_in.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_in.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_in.Location = New System.Drawing.Point(71, 13)
        Me.dtp_in.Name = "dtp_in"
        Me.dtp_in.ShowCheckBox = True
        Me.dtp_in.Size = New System.Drawing.Size(121, 20)
        Me.dtp_in.TabIndex = 1
        '
        'lbl_salida
        '
        Me.lbl_salida.AutoSize = True
        Me.lbl_salida.BackColor = System.Drawing.Color.Transparent
        Me.lbl_salida.Location = New System.Drawing.Point(5, 44)
        Me.lbl_salida.Name = "lbl_salida"
        Me.lbl_salida.Size = New System.Drawing.Size(38, 13)
        Me.lbl_salida.TabIndex = 0
        Me.lbl_salida.Text = "Hasta:"
        '
        'dtp_out
        '
        Me.dtp_out.Checked = False
        Me.dtp_out.CustomFormat = "dd/MMM/yyyy"
        Me.dtp_out.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_out.Location = New System.Drawing.Point(71, 41)
        Me.dtp_out.Name = "dtp_out"
        Me.dtp_out.ShowCheckBox = True
        Me.dtp_out.Size = New System.Drawing.Size(121, 20)
        Me.dtp_out.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lbl_hotel)
        Me.GroupBox2.Controls.Add(Me.txt_hotel)
        Me.GroupBox2.Controls.Add(Me.txt_agente)
        Me.GroupBox2.Controls.Add(Me.chkGroupByAgent)
        Me.GroupBox2.Controls.Add(Me.lbl_agente)
        Me.GroupBox2.Location = New System.Drawing.Point(313, 2)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(10, 2, 2, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(243, 122)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        '
        'lbl_hotel
        '
        Me.lbl_hotel.AutoSize = True
        Me.lbl_hotel.BackColor = System.Drawing.Color.Transparent
        Me.lbl_hotel.Location = New System.Drawing.Point(2, 57)
        Me.lbl_hotel.Name = "lbl_hotel"
        Me.lbl_hotel.Size = New System.Drawing.Size(35, 13)
        Me.lbl_hotel.TabIndex = 18
        Me.lbl_hotel.Text = "Hotel:"
        '
        'txt_hotel
        '
        Me.txt_hotel.AlwaysInEditMode = True
        Me.txt_hotel.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_hotel.Location = New System.Drawing.Point(62, 54)
        Me.txt_hotel.Name = "txt_hotel"
        Me.txt_hotel.Size = New System.Drawing.Size(162, 19)
        Me.txt_hotel.TabIndex = 2
        '
        'txt_agente
        '
        Me.txt_agente.AlwaysInEditMode = True
        Appearance12.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.txt_agente.Appearance = Appearance12
        Me.txt_agente.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.txt_agente.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_agente.Enabled = False
        Me.txt_agente.Location = New System.Drawing.Point(62, 26)
        Me.txt_agente.Name = "txt_agente"
        Me.txt_agente.Size = New System.Drawing.Size(162, 19)
        Me.txt_agente.TabIndex = 2
        '
        'chkGroupByAgent
        '
        Me.chkGroupByAgent.Location = New System.Drawing.Point(6, 9)
        Me.chkGroupByAgent.Name = "chkGroupByAgent"
        Me.chkGroupByAgent.Size = New System.Drawing.Size(120, 20)
        Me.chkGroupByAgent.TabIndex = 23
        Me.chkGroupByAgent.Text = "Agrupar por agente"
        '
        'lbl_agente
        '
        Me.lbl_agente.AutoSize = True
        Me.lbl_agente.BackColor = System.Drawing.Color.Transparent
        Me.lbl_agente.Location = New System.Drawing.Point(2, 31)
        Me.lbl_agente.Name = "lbl_agente"
        Me.lbl_agente.Size = New System.Drawing.Size(44, 13)
        Me.lbl_agente.TabIndex = 0
        Me.lbl_agente.Text = "Agente:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_actualizar)
        Me.GroupBox3.Controls.Add(Me.btn_excelExport)
        Me.GroupBox3.Controls.Add(Me.PictureBox2)
        Me.GroupBox3.Location = New System.Drawing.Point(568, 2)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(10, 2, 2, 2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(124, 120)
        Me.GroupBox3.TabIndex = 21
        Me.GroupBox3.TabStop = False
        '
        'btn_actualizar
        '
        Appearance9.BackColor = System.Drawing.Color.Transparent
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_actualizar.Appearance = Appearance9
        Me.btn_actualizar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_actualizar.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_actualizar.Location = New System.Drawing.Point(6, 73)
        Me.btn_actualizar.Name = "btn_actualizar"
        Me.btn_actualizar.Size = New System.Drawing.Size(109, 41)
        Me.btn_actualizar.TabIndex = 19
        Me.btn_actualizar.Text = "Buscar"
        '
        'btn_excelExport
        '
        Appearance2.Image = "logo_excel.png"
        Appearance2.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_excelExport.Appearance = Appearance2
        Me.btn_excelExport.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_excelExport.Enabled = False
        Me.btn_excelExport.ImageList = Me.ImageList1
        Me.btn_excelExport.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_excelExport.Location = New System.Drawing.Point(4, 31)
        Me.btn_excelExport.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_excelExport.Name = "btn_excelExport"
        Me.btn_excelExport.Size = New System.Drawing.Size(111, 41)
        Me.btn_excelExport.TabIndex = 13
        Me.btn_excelExport.Text = "Exportar "
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "logo_excel.png")
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.loading
        Me.PictureBox2.Location = New System.Drawing.Point(8, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Padding = New System.Windows.Forms.Padding(1)
        Me.PictureBox2.Size = New System.Drawing.Size(102, 17)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'uce_source
        '
        Me.uce_source.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_source.Location = New System.Drawing.Point(697, 3)
        Me.uce_source.Name = "uce_source"
        Me.uce_source.Size = New System.Drawing.Size(121, 21)
        Me.uce_source.TabIndex = 14
        Me.uce_source.Visible = False
        '
        'UltraGrid1
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance1
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance3.ImageBackground = CType(resources.GetObject("Appearance3.ImageBackground"), System.Drawing.Image)
        Appearance3.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance3.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance3.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance3
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance4.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance4
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance6.FontData.Name = "Trebuchet MS"
        Appearance6.FontData.SizeInPoints = 9.0!
        Appearance6.ForeColor = System.Drawing.Color.White
        Appearance6.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance6
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance7.FontData.BoldAsString = "True"
        Appearance7.FontData.Name = "Trebuchet MS"
        Appearance7.FontData.SizeInPoints = 10.0!
        Appearance7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance7.ImageBackground = CType(resources.GetObject("Appearance7.ImageBackground"), System.Drawing.Image)
        Appearance7.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance7.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance7
        Appearance8.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance8
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance5.BackColor2 = System.Drawing.SystemColors.Control
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance5.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance5
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance10.BackColor = System.Drawing.SystemColors.Window
        Appearance10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance10
        Appearance11.BackColor = System.Drawing.SystemColors.Highlight
        Appearance11.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance11
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance13.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance13
        Appearance15.BorderColor = System.Drawing.Color.Transparent
        Appearance15.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance15
        Appearance16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance16.ImageBackground = CType(resources.GetObject("Appearance16.ImageBackground"), System.Drawing.Image)
        Appearance16.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance16.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance16
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance18
        Appearance19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance20.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance20
        Appearance22.BackColor = System.Drawing.SystemColors.Control
        Appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance22.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance22
        Appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance23.FontData.BoldAsString = "True"
        Appearance23.FontData.Name = "Trebuchet MS"
        Appearance23.FontData.SizeInPoints = 10.0!
        Appearance23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance23.ImageBackground = CType(resources.GetObject("Appearance23.ImageBackground"), System.Drawing.Image)
        Appearance23.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance23.TextHAlignAsString = "Left"
        Appearance23.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance23
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance28.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance28
        Appearance29.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance29
        Appearance30.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance30
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance31.BorderColor = System.Drawing.Color.Transparent
        Appearance31.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance31
        Appearance32.BorderColor = System.Drawing.Color.Transparent
        Appearance32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance32.ImageBackground = CType(resources.GetObject("Appearance32.ImageBackground"), System.Drawing.Image)
        Appearance32.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance32.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance32
        Appearance33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance33
        Appearance34.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance34.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance34
        Appearance35.ImageBackground = CType(resources.GetObject("Appearance35.ImageBackground"), System.Drawing.Image)
        Appearance35.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance35
        Appearance36.ImageBackground = CType(resources.GetObject("Appearance36.ImageBackground"), System.Drawing.Image)
        Appearance36.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance36.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance36
        Appearance37.ImageBackground = CType(resources.GetObject("Appearance37.ImageBackground"), System.Drawing.Image)
        Appearance37.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance37
        Appearance38.ImageBackground = CType(resources.GetObject("Appearance38.ImageBackground"), System.Drawing.Image)
        Appearance38.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance38.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance38
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 132)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(761, 364)
        Me.UltraGrid1.TabIndex = 2
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpresa.Location = New System.Drawing.Point(5, 99)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(48, 13)
        Me.lblEmpresa.TabIndex = 45
        Me.lblEmpresa.Text = "Empresa"
        '
        'CmbSearch1
        '
        Me.CmbSearch1.Location = New System.Drawing.Point(69, 95)
        Me.CmbSearch1.Name = "CmbSearch1"
        Me.CmbSearch1.Size = New System.Drawing.Size(218, 19)
        Me.CmbSearch1.TabIndex = 44
        '
        'ProductionReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(761, 496)
        Me.Controls.Add(Me.UltraGrid1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ProductionReport"
        Me.Text = "Reporte de producción"
        Me.Panel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txt_hotel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_agente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_source, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents uce_source As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_llegada As System.Windows.Forms.Label
    Friend WithEvents dtp_in As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_salida As System.Windows.Forms.Label
    Friend WithEvents dtp_out As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_segmento As System.Windows.Forms.Label
    Friend WithEvents lbl_hotel As System.Windows.Forms.Label
    Friend WithEvents cmbSegmento As System.Windows.Forms.ComboBox
    Friend WithEvents txt_hotel As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_agente As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_agente As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_actualizar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_excelExport As Infragistics.Win.Misc.UltraButton
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents UltraGridExcelExporter1 As Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents chkGroupByAgent As Infragistics.Win.UltraWinEditors.UltraCheckEditor
    Friend WithEvents CmbSearch1 As callCenter.cmbSearch
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
End Class
