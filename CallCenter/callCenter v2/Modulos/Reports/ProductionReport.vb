﻿
Public Class ProductionReport
    Private MI_THREAD As System.Threading.Thread
    Private id_empresa_sel As String = "-1"

#Region "Extra Functions and Events"
    Public Sub load_data()
        UltraGrid1.DataSource = Nothing
        btn_excelExport.Enabled = False
        PictureBox2.Visible = True


        Dim ini As DateTime
        Dim fin As DateTime
        If dtp_in.Checked Then ini = dtp_in.Value Else ini = DateTime.MinValue
        If dtp_out.Checked Then fin = dtp_out.Value Else fin = DateTime.Now


        If MI_THREAD IsNot Nothing Then
            MI_THREAD.Abort()
            MI_THREAD = Nothing
        End If

        MI_THREAD = New System.Threading.Thread(AddressOf CapaLogicaNegocios.Reports.busca_reservaciones_produccion)
        'MI_THREAD.Start(New Object() {ini, fin, uce_source.SelectedItem.DataValue, txt_agente.Text.Trim(), txt_hotel.Text.Trim(), DirectCast(cmbSegmento.SelectedItem, ListData)._Value, chkGroupByAgent.Checked, id_empresa_sel, Me})
        MI_THREAD.Start(New Object() {ini, fin, ",CCT,", txt_agente.Text.Trim(), txt_hotel.Text.Trim(), DirectCast(cmbSegmento.SelectedItem, ListData)._Value, chkGroupByAgent.Checked, id_empresa_sel, Me})
    End Sub


#End Region

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub


    Private Sub ProductionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        CapaLogicaNegocios.Reports.llena_combos(Me)
        CapaPresentacion.Idiomas.cambiar_reporteProduccion(Me)
        CmbSearch1.crea_lista(Me, GroupBox1.Left + CmbSearch1.Left, GroupBox1.Top)
        AddHandler CmbSearch1.llena, AddressOf CargaEmpresasSegmento
    End Sub

    Private Sub ProductionReport_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        txt_agente.Focus()
    End Sub

    Private Sub ProductionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reporte_produccion").SharedProps.Enabled = True
    End Sub

    Private Sub chkGroupByAgent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupByAgent.CheckedChanged
        If chkGroupByAgent.Checked = True Then
            txt_agente.Enabled = True
            txt_agente.Focus()
            txt_agente.Appearance.BackColor = System.Drawing.SystemColors.Window
        Else
            txt_agente.Text = ""
            txt_agente.Enabled = False
            txt_agente.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace
        End If
    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_out.ValueChanged, dtp_in.ValueChanged
        herramientas.fechas_inteligentes(dtp_in, dtp_out, sender, 0, Nothing)
    End Sub

    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        Me.load_data()
    End Sub

    Private Sub UltraGrid1_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout
        'Habilitamos o no la exportacion a excel si tiene o no registros el grid
        btn_excelExport.Enabled = (e.Layout.Rows.Count > 0)
        CapaPresentacion.Idiomas.cambiar_reporteProduccion(Me)
    End Sub

    Private Sub btn_excelExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_excelExport.Click
        Try
            With SaveFileDialog1
                .FileName = CapaPresentacion.Idiomas.get_str("str_474_excelFileName")
                .DefaultExt = "xls"
                .Filter = CapaPresentacion.Idiomas.get_str("str_475_excelBook") + " (*.xls)|*.xls"
                .FilterIndex = 0
                .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                If .ShowDialog = DialogResult.OK Then
                    UltraGridExcelExporter1.Export(UltraGrid1, .FileName)
                    If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_476_excelMessage"), CapaPresentacion.Idiomas.get_str("str_477_excelMessageTitle"), MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                        System.Diagnostics.Process.Start(.FileName)
                    End If
                Else
                    Exit Sub
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_478_excelError"), CapaPresentacion.Idiomas.get_str("str_479_excelErrorTitle"))
        End Try

    End Sub

    Private Sub CargaEmpresasSegmento()
        Dim id_Segmento As String = ""
        If Not cmbSegmento.SelectedItem Is Nothing Then
            id_Segmento = cmbSegmento.SelectedItem._value
        End If

        'cmbEmpresas.Items.Clear()
        'cmbEmpresas.DisplayMember = "Name"
        'cmbEmpresas.ValueMember = "_Value"
        'cmbEmpresas.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), ""))
        If IsNothing(CmbSearch1.ListBox1) Then Exit Sub

        CmbSearch1.ListBox1.Items.Clear()
        CmbSearch1.lista.Clear()
        CmbSearch1.ListBox1.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), ""))
        CmbSearch1.lista.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_437_ninguna"), Nothing))
        'If id_Segmento <> "" Then
        Dim ds As DataSet = herramientas.get_Empresas_filtro(CmbSearch1.TextBox1.Text) 'herramientas.get_Empresas_Segmento(id_Segmento, CmbSearch1.TextBox1.Text)
        If ds.Tables(0).Rows.Count = 1 Then
            CmbSearch1.ListBox1.Items.Clear()
            CmbSearch1.lista.Clear()
        End If

        For Each dr As DataRow In ds.Tables(0).Rows
            'cmbEmpresas.Items.Add(New ListData(dr("Nombre"), dr("id_Empresa")))
            CmbSearch1.ListBox1.Items.Add(New ListData(dr("Codigo").ToString & " - " & dr("Nombre").ToString, dr("id_Empresa").ToString))
            CmbSearch1.lista.Add(New ListData(dr("Nombre").ToString, dr("id_Empresa").ToString))
        Next
        'End If
        'cmbEmpresas.SelectedIndex = 0
        If CmbSearch1.ListBox1.Items.Count > 0 Then CmbSearch1.ListBox1.SelectedIndex = 0
    End Sub

    Private Sub CmbSearch1_ValueChanged() Handles CmbSearch1.ValueChanged
        If CmbSearch1.selected_valor <> "" Then
            id_empresa_sel = CmbSearch1.selected_valor

        Else
            id_empresa_sel = "-1"
        End If
    End Sub

End Class