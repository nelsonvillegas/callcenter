﻿Public Class SegmentCompanyList
    Private MI_THREAD As System.Threading.Thread

    Private Sub btn_actualizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_actualizar.Click
        If txtNombre.Text.Trim.Length < 3 Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_513"))
            Exit Sub
        End If
        UltraGrid1.DataSource = Nothing
        PictureBox2.Visible = True
        btn_Modify.Visible = False
        btn_Delete.Visible = False

        If MI_THREAD IsNot Nothing Then
            MI_THREAD.Abort()
            MI_THREAD = Nothing
        End If

        MI_THREAD = New System.Threading.Thread(AddressOf CapaLogicaNegocios.SegmentsCompany.SegmentsCompanyList)
        MI_THREAD.Start(New Object() {txtNombre.Text.Trim, DirectCast(cmbSegmento.SelectedItem, ListData)._Value, Me})
    End Sub

    'Private Sub CargaInfo()
    '    Dim ws As New webService_call
    '    Dim ds As Call_SegmentsCompany_List_RS = ws.SegmentCompanyList(txtNombre.Text.Trim, DirectCast(cmbSegmento.SelectedItem, ListData)._Value, dataOperador.idCorporativo)
    '    Me.UltraGrid1.DataSource = ds

    '    For Each col As Infragistics.Win.UltraWinGrid.UltraGridColumn In UltraGrid1.DisplayLayout.Bands(0).Columns
    '        col.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, True)
    '    Next
    '    PictureBox2.Visible = False
    '    btn_Modify.Enabled = False
    '    CapaPresentacion.Idiomas.cambiar_SegmentCompanyList(Me)

    'End Sub

    Private Sub SegmentCompanyList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call CargaSegmentos()
        CapaPresentacion.Idiomas.cambiar_SegmentCompanyList(Me)
    End Sub


    Private Sub CargaSegmentos()
        cmbSegmento.Items.Clear()
        cmbSegmento.DisplayMember = "Name"
        cmbSegmento.ValueMember = "_Value"
        cmbSegmento.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_354_todos"), "0"))
        For Each dr As DataRow In herramientas.get_Segmentos(dataOperador.idCorporativo).Tables(0).Rows
            cmbSegmento.Items.Add(New ListData(dr("Nombre"), dr("id_Segmento")))
        Next
        If cmbSegmento.Items.Count > 0 Then cmbSegmento.SelectedIndex = 0
    End Sub

    Private Sub UltraGrid1_BeforeRowActivate(sender As Object, e As Infragistics.Win.UltraWinGrid.RowEventArgs) Handles UltraGrid1.BeforeRowActivate
        btn_Modify.Visible = True
        btn_Delete.Visible = True
    End Sub

    Private Sub UltraGrid1_InitializeLayout(sender As System.Object, e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout
        CapaPresentacion.Idiomas.cambiar_SegmentCompanyList(Me)
    End Sub

    Private Sub btn_Modify_Click(sender As System.Object, e As System.EventArgs) Handles btn_Modify.Click
        Dim frm As New SegmentCompany
        frm.Cargar(UltraGrid1.ActiveRow.Cells("idCompany").Value, UltraGrid1.ActiveRow.Cells("idSegment").Value, UltraGrid1.ActiveRow.Cells("Name").Value, UltraGrid1.ActiveRow.Cells("Code").Value)
        frm.ShowDialog()
    End Sub

    Private Sub btn_add_Click(sender As System.Object, e As System.EventArgs) Handles btn_add.Click
        Dim frm As New SegmentCompany
        frm.ShowDialog()
    End Sub

    Private Sub Eliminar()
        Dim id As Integer = 0
        Dim ws As New webService_call
        Dim rs As Call_SegmentsCompany_Delete_RS = ws.SegmentCompanyDelete(UltraGrid1.ActiveRow.Cells("idCompany").Value)
        If Not rs Is Nothing Then
            If rs.Response.Rows.Count > 0 Then
                id = 1
            Else
                If rs._Error.Rows.Count > 0 Then
                    MessageBox.Show(rs._Error(0).Message)
                End If
            End If
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_500")) 'No se pudo eliminar
        End If
        If id > 0 Then
            If herramientas.EmpresaSegmentoEliminar(UltraGrid1.ActiveRow.Cells("idCompany").Value) Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_501")) 'Eliminada Correctamente
            Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_502") & vbCrLf & CapaPresentacion.Idiomas.get_str("str_503"))
            End If
            UltraGrid1.ActiveRow.Delete(False)
            If UltraGrid1.Rows.Count = 0 Then
                btn_Modify.Visible = False
                btn_Delete.Visible = False
            End If
        End If
    End Sub

    Private Sub btn_Delete_Click(sender As System.Object, e As System.EventArgs) Handles btn_Delete.Click
        If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_504") & vbCrLf & UltraGrid1.ActiveRow.Cells("Name").Value & "?", CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Eliminar()
        End If
    End Sub

    Private Sub btn_cancel_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancel.Click
        Close()
    End Sub
End Class