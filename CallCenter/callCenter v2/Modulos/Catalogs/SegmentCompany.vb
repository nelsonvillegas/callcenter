﻿Public Class SegmentCompany
    Dim operacion As Integer
    Dim idEmpresa As String

    Public Sub Cargar(_idEmpresa As String, idSegmento As String, Nombre As String, Codigo As String)
        operacion = 1
        txtNombre.Text = Nombre
        idEmpresa = _idEmpresa
        lblCodigo.Text = Codigo
        btn_registrar.Text = CapaPresentacion.Idiomas.get_str("str_0020_modificar") ' "Actualizar"
        Call CargaSegmentos(idSegmento)
    End Sub

    Private Sub btn_registrar_Click(sender As System.Object, e As System.EventArgs) Handles btn_registrar.Click
        If Valida() Then
            If operacion = 0 Then
                Call Insertar()
            Else
                Call Actualizar()
            End If

        End If
    End Sub

    Private Sub Insertar()
        Dim id As Integer = -1
        Dim codigo As String = ""
        Dim ws As New webService_call
        Dim rs As Call_SegmentsCompany_Insert_RS = ws.SegmentCompanyInsert(txtNombre.Text, -1, DirectCast(cmbSegmento.SelectedItem, ListData)._Value, dataOperador.idCorporativo)
        If Not rs Is Nothing Then
            If rs.Response.Rows.Count > 0 Then
                id = rs.Response(0).idCompany
                codigo = rs.Response(0).Code
            Else
                If rs._Error.Rows.Count > 0 Then
                    MessageBox.Show(rs._Error(0).Message)
                End If
            End If
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_507"))
        End If
        If id > 0 Then
            If herramientas.EmpresaSegmentoInsertar(id, codigo, txtNombre.Text, DirectCast(cmbSegmento.SelectedItem, ListData)._Value, dataOperador.idCorporativo) Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_508") & ": " & codigo)
            Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_509") & ": " & codigo & vbCrLf & CapaPresentacion.Idiomas.get_str("str_503"))
            End If
            'Call Limpiar()
            Close()
        End If
    End Sub

    Private Sub Actualizar()
        Dim id As Integer = 0
        Dim ws As New webService_call
        Dim rs As Call_SegmentsCompany_Update_RS = ws.SegmentCompanyUpdate(idEmpresa, txtNombre.Text, -1, DirectCast(cmbSegmento.SelectedItem, ListData)._Value, dataOperador.idCorporativo)
        If Not rs Is Nothing Then
            If rs.Response.Rows.Count > 0 Then
                id = 1
            Else
                If rs._Error.Rows.Count > 0 Then
                    MessageBox.Show(rs._Error(0).Message)
                End If
            End If
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_510"))
        End If
        If id > 0 Then
            If herramientas.EmpresaSegmentoActualizar(idEmpresa, txtNombre.Text, DirectCast(cmbSegmento.SelectedItem, ListData)._Value) Then
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_511"))
            Else
                MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_512") & vbCrLf & CapaPresentacion.Idiomas.get_str("str_503"))
            End If
            'Call Limpiar()
            Close()
        End If
    End Sub

    Private Sub btn_cancelar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub SegmentCompany_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If operacion = 0 Then
            btn_registrar.Text = CapaPresentacion.Idiomas.get_str("str_440_reg")
            Call Limpiar()
            Call CargaSegmentos()
        End If
        btn_cancelar.Text = CapaPresentacion.Idiomas.get_str("str_0021_cancelar")
        Me.Text = CapaPresentacion.Idiomas.get_str("str_0167_empresa")
        Me.lbl_segmento.Text = CapaPresentacion.Idiomas.get_str("str_490") + ":"
        Me.lbl_nombre.Text = CapaPresentacion.Idiomas.get_str("str_0168_nombre") & ":"
    End Sub

    Private Sub Limpiar()
        txtNombre.Text = ""
        idEmpresa = ""
        lblCodigo.Text = ""
        If cmbSegmento.Items.Count > 0 Then cmbSegmento.SelectedIndex = 0
        operacion = 0
    End Sub

    Private Sub CargaSegmentos(Optional idSegmento As String = "")
        cmbSegmento.Items.Clear()
        cmbSegmento.DisplayMember = "Name"
        cmbSegmento.ValueMember = "_Value"
        For Each dr As DataRow In herramientas.get_Segmentos(dataOperador.idCorporativo).Tables(0).Rows
            cmbSegmento.Items.Add(New ListData(dr("Nombre"), dr("id_Segmento")))
            If idSegmento <> "" AndAlso idSegmento = dr("id_segmento") Then cmbSegmento.SelectedIndex = cmbSegmento.Items.Count - 1
        Next
        If cmbSegmento.Items.Count > 0 And cmbSegmento.SelectedIndex = -1 Then cmbSegmento.SelectedIndex = 0
    End Sub

    Private Function Valida() As Boolean
        If txtNombre.Text.Trim = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_505"))
            Return False
        End If
        If cmbSegmento.Text.Trim = "" Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_506"))
            Return False
        End If
        Return True
    End Function
End Class