﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SegmentCompanyList
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SegmentCompanyList))
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook()
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.btn_actualizar = New Infragistics.Win.Misc.UltraButton()
        Me.lbl_segmento = New System.Windows.Forms.Label()
        Me.cmbSegmento = New System.Windows.Forms.ComboBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.txtNombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_nombre = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btn_Delete = New Infragistics.Win.Misc.UltraButton()
        Me.btn_Modify = New Infragistics.Win.Misc.UltraButton()
        Me.btn_cancel = New Infragistics.Win.Misc.UltraButton()
        Me.btn_add = New Infragistics.Win.Misc.UltraButton()
        Me.UltraGrid1 = New Infragistics.Win.UltraWinGrid.UltraGrid()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Controls.Add(Me.btn_actualizar)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_segmento)
        Me.UltraGroupBox1.Controls.Add(Me.cmbSegmento)
        Me.UltraGroupBox1.Controls.Add(Me.PictureBox2)
        Me.UltraGroupBox1.Controls.Add(Me.txtNombre)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_nombre)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(12, 7)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(583, 61)
        Me.UltraGroupBox1.TabIndex = 1
        Me.UltraGroupBox1.Text = "Filtros:"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'btn_actualizar
        '
        Appearance31.BackColor = System.Drawing.Color.Transparent
        Appearance31.Image = CType(resources.GetObject("Appearance31.Image"), Object)
        Appearance31.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance31.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_actualizar.Appearance = Appearance31
        Me.btn_actualizar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_actualizar.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_actualizar.Location = New System.Drawing.Point(258, 16)
        Me.btn_actualizar.Name = "btn_actualizar"
        Me.btn_actualizar.Size = New System.Drawing.Size(102, 41)
        Me.btn_actualizar.TabIndex = 20
        Me.btn_actualizar.Text = "Buscar"
        '
        'lbl_segmento
        '
        Me.lbl_segmento.AutoSize = True
        Me.lbl_segmento.BackColor = System.Drawing.Color.Transparent
        Me.lbl_segmento.Location = New System.Drawing.Point(6, 19)
        Me.lbl_segmento.Name = "lbl_segmento"
        Me.lbl_segmento.Size = New System.Drawing.Size(58, 13)
        Me.lbl_segmento.TabIndex = 19
        Me.lbl_segmento.Text = "Segmento:"
        '
        'cmbSegmento
        '
        Me.cmbSegmento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSegmento.FormattingEnabled = True
        Me.cmbSegmento.Location = New System.Drawing.Point(72, 16)
        Me.cmbSegmento.Name = "cmbSegmento"
        Me.cmbSegmento.Size = New System.Drawing.Size(121, 21)
        Me.cmbSegmento.TabIndex = 18
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.callCenter.My.Resources.Resources.loading
        Me.PictureBox2.Location = New System.Drawing.Point(366, 41)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(100, 15)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'txtNombre
        '
        Me.txtNombre.AlwaysInEditMode = True
        Me.txtNombre.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txtNombre.Location = New System.Drawing.Point(72, 38)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(170, 19)
        Me.txtNombre.TabIndex = 2
        '
        'lbl_nombre
        '
        Me.lbl_nombre.AutoSize = True
        Me.lbl_nombre.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nombre.Location = New System.Drawing.Point(6, 41)
        Me.lbl_nombre.Name = "lbl_nombre"
        Me.lbl_nombre.Size = New System.Drawing.Size(50, 13)
        Me.lbl_nombre.TabIndex = 0
        Me.lbl_nombre.Text = "Nombre :"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.UltraGroupBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(619, 74)
        Me.Panel1.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btn_Delete)
        Me.Panel2.Controls.Add(Me.btn_Modify)
        Me.Panel2.Controls.Add(Me.btn_cancel)
        Me.Panel2.Controls.Add(Me.btn_add)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 272)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(619, 43)
        Me.Panel2.TabIndex = 8
        '
        'btn_Delete
        '
        Appearance9.BackColor = System.Drawing.Color.Transparent
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_Delete.Appearance = Appearance9
        Me.btn_Delete.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_Delete.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_Delete.Location = New System.Drawing.Point(3, 0)
        Me.btn_Delete.Name = "btn_Delete"
        Me.btn_Delete.Size = New System.Drawing.Size(102, 41)
        Me.btn_Delete.TabIndex = 21
        Me.btn_Delete.Text = "Eliminar"
        Me.btn_Delete.Visible = False
        '
        'btn_Modify
        '
        Appearance32.BackColor = System.Drawing.Color.Transparent
        Appearance32.Image = CType(resources.GetObject("Appearance32.Image"), Object)
        Appearance32.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance32.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_Modify.Appearance = Appearance32
        Me.btn_Modify.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_Modify.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_Modify.Location = New System.Drawing.Point(111, 0)
        Me.btn_Modify.Name = "btn_Modify"
        Me.btn_Modify.Size = New System.Drawing.Size(102, 41)
        Me.btn_Modify.TabIndex = 21
        Me.btn_Modify.Text = "Modificar"
        Me.btn_Modify.Visible = False
        '
        'btn_cancel
        '
        Appearance30.BackColor = System.Drawing.Color.Transparent
        Appearance30.Image = CType(resources.GetObject("Appearance30.Image"), Object)
        Appearance30.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance30.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_cancel.Appearance = Appearance30
        Me.btn_cancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_cancel.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_cancel.Location = New System.Drawing.Point(513, 0)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(102, 41)
        Me.btn_cancel.TabIndex = 21
        Me.btn_cancel.Text = "Cancelar"
        '
        'btn_add
        '
        Appearance33.BackColor = System.Drawing.Color.Transparent
        Appearance33.Image = CType(resources.GetObject("Appearance33.Image"), Object)
        Appearance33.ImageHAlign = Infragistics.Win.HAlign.Left
        Appearance33.ImageVAlign = Infragistics.Win.VAlign.Middle
        Me.btn_add.Appearance = Appearance33
        Me.btn_add.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2003ToolbarButton
        Me.btn_add.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_add.Location = New System.Drawing.Point(405, 0)
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Size = New System.Drawing.Size(102, 41)
        Me.btn_add.TabIndex = 21
        Me.btn_add.Text = "Agregar"
        '
        'UltraGrid1
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.Appearance = Appearance1
        Me.UltraGrid1.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance2.ImageBackground = CType(resources.GetObject("Appearance2.ImageBackground"), System.Drawing.Image)
        Appearance2.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance2.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance2.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonAppearance = Appearance2
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance3.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Appearance = Appearance3
        Me.UltraGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance4.FontData.Name = "Trebuchet MS"
        Appearance4.FontData.SizeInPoints = 9.0!
        Appearance4.ForeColor = System.Drawing.Color.White
        Appearance4.TextHAlignAsString = "Right"
        Me.UltraGrid1.DisplayLayout.CaptionAppearance = Appearance4
        Me.UltraGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UltraGrid1.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UltraGrid1.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UltraGrid1.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance5.FontData.BoldAsString = "True"
        Appearance5.FontData.Name = "Trebuchet MS"
        Appearance5.FontData.SizeInPoints = 10.0!
        Appearance5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance5.ImageBackground = CType(resources.GetObject("Appearance5.ImageBackground"), System.Drawing.Image)
        Appearance5.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance5.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.GroupByBox.Appearance = Appearance5
        Appearance6.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance6
        Me.UltraGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraGrid1.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.GroupByBox.Hidden = True
        Appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance7.BackColor2 = System.Drawing.SystemColors.Control
        Appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance7.ForeColor = System.Drawing.SystemColors.GrayText
        Me.UltraGrid1.DisplayLayout.GroupByBox.PromptAppearance = Appearance7
        Me.UltraGrid1.DisplayLayout.MaxColScrollRegions = 1
        Me.UltraGrid1.DisplayLayout.MaxRowScrollRegions = 1
        Appearance8.BackColor = System.Drawing.SystemColors.Window
        Appearance8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UltraGrid1.DisplayLayout.Override.ActiveCellAppearance = Appearance8
        Appearance10.BackColor = System.Drawing.SystemColors.Highlight
        Appearance10.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.UltraGrid1.DisplayLayout.Override.ActiveRowAppearance = Appearance10
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraGrid1.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance11.BackColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.CardAreaAppearance = Appearance11
        Appearance12.BorderColor = System.Drawing.Color.Transparent
        Appearance12.FontData.Name = "Verdana"
        Me.UltraGrid1.DisplayLayout.Override.CellAppearance = Appearance12
        Appearance13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance13.ImageBackground = CType(resources.GetObject("Appearance13.ImageBackground"), System.Drawing.Image)
        Appearance13.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance13.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.CellButtonAppearance = Appearance13
        Me.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect
        Me.UltraGrid1.DisplayLayout.Override.CellPadding = 0
        Appearance14.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.FilterCellAppearance = Appearance14
        Appearance15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance15.ImageBackground = CType(resources.GetObject("Appearance15.ImageBackground"), System.Drawing.Image)
        Appearance15.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UltraGrid1.DisplayLayout.Override.FilterClearButtonAppearance = Appearance15
        Appearance16.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance16.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UltraGrid1.DisplayLayout.Override.FilterRowPromptAppearance = Appearance16
        Appearance17.BackColor = System.Drawing.SystemColors.Control
        Appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance17.BorderColor = System.Drawing.SystemColors.Window
        Me.UltraGrid1.DisplayLayout.Override.GroupByRowAppearance = Appearance17
        Appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance18.FontData.BoldAsString = "True"
        Appearance18.FontData.Name = "Trebuchet MS"
        Appearance18.FontData.SizeInPoints = 10.0!
        Appearance18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance18.ImageBackground = CType(resources.GetObject("Appearance18.ImageBackground"), System.Drawing.Image)
        Appearance18.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance18.TextHAlignAsString = "Left"
        Appearance18.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraGrid1.DisplayLayout.Override.HeaderAppearance = Appearance18
        Me.UltraGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.UltraGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance19.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.UltraGrid1.DisplayLayout.Override.RowAlternateAppearance = Appearance19
        Appearance20.BorderColor = System.Drawing.Color.Transparent
        Me.UltraGrid1.DisplayLayout.Override.RowAppearance = Appearance20
        Appearance21.BackColor = System.Drawing.Color.White
        Me.UltraGrid1.DisplayLayout.Override.RowSelectorAppearance = Appearance21
        Me.UltraGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance22.BorderColor = System.Drawing.Color.Transparent
        Appearance22.ForeColor = System.Drawing.Color.Black
        Me.UltraGrid1.DisplayLayout.Override.SelectedCellAppearance = Appearance22
        Appearance23.BorderColor = System.Drawing.Color.Transparent
        Appearance23.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance23.ImageBackground = CType(resources.GetObject("Appearance23.ImageBackground"), System.Drawing.Image)
        Appearance23.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance23.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UltraGrid1.DisplayLayout.Override.SelectedRowAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.SystemColors.ControlLight
        Me.UltraGrid1.DisplayLayout.Override.TemplateAddRowAppearance = Appearance24
        Appearance25.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance25.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance25
        Appearance26.ImageBackground = CType(resources.GetObject("Appearance26.ImageBackground"), System.Drawing.Image)
        Appearance26.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance26
        Appearance27.ImageBackground = CType(resources.GetObject("Appearance27.ImageBackground"), System.Drawing.Image)
        Appearance27.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance27.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance27
        Appearance28.ImageBackground = CType(resources.GetObject("Appearance28.ImageBackground"), System.Drawing.Image)
        Appearance28.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance28
        Appearance29.ImageBackground = CType(resources.GetObject("Appearance29.ImageBackground"), System.Drawing.Image)
        Appearance29.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance29.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance29
        Me.UltraGrid1.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UltraGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.UltraGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.UltraGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.UltraGrid1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGrid1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UltraGrid1.Location = New System.Drawing.Point(0, 74)
        Me.UltraGrid1.Name = "UltraGrid1"
        Me.UltraGrid1.Size = New System.Drawing.Size(619, 198)
        Me.UltraGrid1.TabIndex = 9
        Me.UltraGrid1.Text = "Grid Caption Area"
        Me.UltraGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'SegmentCompanyList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(619, 315)
        Me.Controls.Add(Me.UltraGrid1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SegmentCompanyList"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Empresas Segmentos"
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.UltraGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents txtNombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nombre As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lbl_segmento As System.Windows.Forms.Label
    Friend WithEvents cmbSegmento As System.Windows.Forms.ComboBox
    Friend WithEvents btn_actualizar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btn_Modify As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_add As Infragistics.Win.Misc.UltraButton
    Friend WithEvents UltraGrid1 As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents btn_Delete As Infragistics.Win.Misc.UltraButton
    Friend WithEvents btn_cancel As Infragistics.Win.Misc.UltraButton
End Class
