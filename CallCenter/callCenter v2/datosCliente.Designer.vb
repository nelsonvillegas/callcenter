<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class datosCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ValueListItem4 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem5 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem6 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem7 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(datosCliente))
        Dim ValueListItem1 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem2 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Me.txt_telefono = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_telefono = New System.Windows.Forms.Label()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.txt_ccApellido = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.ckb_modificar = New System.Windows.Forms.CheckBox()
        Me.uce_tipoTarjeta = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.lbl_AAAA2 = New System.Windows.Forms.Label()
        Me.txt_AAAA = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_MM2 = New System.Windows.Forms.Label()
        Me.txt_MM = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_fechaExpiracion = New System.Windows.Forms.Label()
        Me.txt_ccNumeroSeguridad = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_codigoSeguridad = New System.Windows.Forms.Label()
        Me.txt_ccNumero = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_numTarjeta = New System.Windows.Forms.Label()
        Me.txt_ccNombre = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_nombre2 = New System.Windows.Forms.Label()
        Me.lbl_tipoTarjeta = New System.Windows.Forms.Label()
        Me.uce_paises = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.lbl_pais = New System.Windows.Forms.Label()
        Me.txt_email = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_email = New System.Windows.Forms.Label()
        Me.txt_area = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.txt_cp = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_cp = New System.Windows.Forms.Label()
        Me.txt_estado = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_estado = New System.Windows.Forms.Label()
        Me.txt_ciudad = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_ciudad = New System.Windows.Forms.Label()
        Me.txt_direccion = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_direccion = New System.Windows.Forms.Label()
        Me.txt_apellido = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_apellido = New System.Windows.Forms.Label()
        Me.txt_nombreTraveler = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_nombre = New System.Windows.Forms.Label()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.btn_reservar = New System.Windows.Forms.Button()
        Me.btn_importar = New Infragistics.Win.Misc.UltraButton()
        Me.txt_telefono_adicional1 = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_telefono_adicional1 = New System.Windows.Forms.Label()
        Me.txt_area_adicional1 = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.uce_estados = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.txt_codigo = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_codigo = New System.Windows.Forms.Label()
        Me.lbl_val_email = New System.Windows.Forms.Label()
        Me.lbl_homoclave = New System.Windows.Forms.Label()
        Me.txt_homoclave = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.lbl_Canales = New System.Windows.Forms.Label()
        Me.uce_Canales = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.txt_Canal = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        CType(Me.txt_telefono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.txt_ccApellido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_tipoTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_AAAA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_MM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ccNumeroSeguridad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ccNumero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ccNombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_paises, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_email, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_area, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_cp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_estado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ciudad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_direccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_apellido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nombreTraveler, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_telefono_adicional1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_area_adicional1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_estados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_codigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_homoclave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uce_Canales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Canal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_telefono
        '
        Me.txt_telefono.AlwaysInEditMode = True
        Me.txt_telefono.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_telefono.Location = New System.Drawing.Point(184, 187)
        Me.txt_telefono.MaxLength = 20
        Me.txt_telefono.Name = "txt_telefono"
        Me.txt_telefono.Size = New System.Drawing.Size(132, 19)
        Me.txt_telefono.TabIndex = 32
        Me.txt_telefono.Text = "612-12-12345"
        '
        'lbl_telefono
        '
        Me.lbl_telefono.AutoSize = True
        Me.lbl_telefono.BackColor = System.Drawing.Color.Transparent
        Me.lbl_telefono.Location = New System.Drawing.Point(15, 190)
        Me.lbl_telefono.Name = "lbl_telefono"
        Me.lbl_telefono.Size = New System.Drawing.Size(52, 13)
        Me.lbl_telefono.TabIndex = 21
        Me.lbl_telefono.Text = "Tel�fono:"
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Controls.Add(Me.txt_ccApellido)
        Me.UltraGroupBox1.Controls.Add(Me.ckb_modificar)
        Me.UltraGroupBox1.Controls.Add(Me.uce_tipoTarjeta)
        Me.UltraGroupBox1.Controls.Add(Me.Label36)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_AAAA2)
        Me.UltraGroupBox1.Controls.Add(Me.txt_AAAA)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_MM2)
        Me.UltraGroupBox1.Controls.Add(Me.txt_MM)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_fechaExpiracion)
        Me.UltraGroupBox1.Controls.Add(Me.txt_ccNumeroSeguridad)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_codigoSeguridad)
        Me.UltraGroupBox1.Controls.Add(Me.txt_ccNumero)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_numTarjeta)
        Me.UltraGroupBox1.Controls.Add(Me.txt_ccNombre)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_nombre2)
        Me.UltraGroupBox1.Controls.Add(Me.lbl_tipoTarjeta)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(138, 317)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(378, 201)
        Me.UltraGroupBox1.TabIndex = 37
        Me.UltraGroupBox1.Text = "Tarjeta de cr�dito"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'txt_ccApellido
        '
        Me.txt_ccApellido.AlwaysInEditMode = True
        Me.txt_ccApellido.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ccApellido.Location = New System.Drawing.Point(209, 72)
        Me.txt_ccApellido.MaxLength = 50
        Me.txt_ccApellido.Name = "txt_ccApellido"
        Me.txt_ccApellido.Size = New System.Drawing.Size(152, 19)
        Me.txt_ccApellido.TabIndex = 3
        Me.txt_ccApellido.Text = "tstr"
        '
        'ckb_modificar
        '
        Me.ckb_modificar.AutoSize = True
        Me.ckb_modificar.BackColor = System.Drawing.Color.Transparent
        Me.ckb_modificar.Location = New System.Drawing.Point(20, 22)
        Me.ckb_modificar.Name = "ckb_modificar"
        Me.ckb_modificar.Size = New System.Drawing.Size(69, 17)
        Me.ckb_modificar.TabIndex = 0
        Me.ckb_modificar.Text = "Modificar"
        Me.ckb_modificar.UseVisualStyleBackColor = False
        Me.ckb_modificar.Visible = False
        '
        'uce_tipoTarjeta
        '
        Me.uce_tipoTarjeta.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.uce_tipoTarjeta.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        ValueListItem4.DataValue = "ValueListItem0"
        ValueListItem4.DisplayText = "-- Seleccione la tarjeta de cr�dito --"
        ValueListItem5.DataValue = "CA"
        ValueListItem5.DisplayText = "Master Card"
        ValueListItem6.DataValue = "VI"
        ValueListItem6.DisplayText = "Visa"
        ValueListItem7.DataValue = "AX"
        ValueListItem7.DisplayText = "American Express"
        Me.uce_tipoTarjeta.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem4, ValueListItem5, ValueListItem6, ValueListItem7})
        Me.uce_tipoTarjeta.Location = New System.Drawing.Point(101, 44)
        Me.uce_tipoTarjeta.Name = "uce_tipoTarjeta"
        Me.uce_tipoTarjeta.Size = New System.Drawing.Size(260, 19)
        Me.uce_tipoTarjeta.TabIndex = 1
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Location = New System.Drawing.Point(176, 177)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(12, 13)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "/"
        '
        'lbl_AAAA2
        '
        Me.lbl_AAAA2.AutoSize = True
        Me.lbl_AAAA2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_AAAA2.Location = New System.Drawing.Point(191, 157)
        Me.lbl_AAAA2.Name = "lbl_AAAA2"
        Me.lbl_AAAA2.Size = New System.Drawing.Size(35, 13)
        Me.lbl_AAAA2.TabIndex = 0
        Me.lbl_AAAA2.Text = "AAAA"
        '
        'txt_AAAA
        '
        Me.txt_AAAA.AlwaysInEditMode = True
        Me.txt_AAAA.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_AAAA.Location = New System.Drawing.Point(194, 173)
        Me.txt_AAAA.MaxLength = 4
        Me.txt_AAAA.Name = "txt_AAAA"
        Me.txt_AAAA.Size = New System.Drawing.Size(54, 19)
        Me.txt_AAAA.TabIndex = 7
        Me.txt_AAAA.Text = "2010"
        '
        'lbl_MM2
        '
        Me.lbl_MM2.AutoSize = True
        Me.lbl_MM2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_MM2.Location = New System.Drawing.Point(127, 157)
        Me.lbl_MM2.Name = "lbl_MM2"
        Me.lbl_MM2.Size = New System.Drawing.Size(25, 13)
        Me.lbl_MM2.TabIndex = 0
        Me.lbl_MM2.Text = "MM"
        '
        'txt_MM
        '
        Me.txt_MM.AlwaysInEditMode = True
        Me.txt_MM.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_MM.Location = New System.Drawing.Point(130, 173)
        Me.txt_MM.MaxLength = 2
        Me.txt_MM.Name = "txt_MM"
        Me.txt_MM.Size = New System.Drawing.Size(40, 19)
        Me.txt_MM.TabIndex = 6
        Me.txt_MM.Text = "10"
        '
        'lbl_fechaExpiracion
        '
        Me.lbl_fechaExpiracion.AutoSize = True
        Me.lbl_fechaExpiracion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_fechaExpiracion.Location = New System.Drawing.Point(17, 177)
        Me.lbl_fechaExpiracion.Name = "lbl_fechaExpiracion"
        Me.lbl_fechaExpiracion.Size = New System.Drawing.Size(106, 13)
        Me.lbl_fechaExpiracion.TabIndex = 0
        Me.lbl_fechaExpiracion.Text = "Fecha de expiraci�n:"
        '
        'txt_ccNumeroSeguridad
        '
        Me.txt_ccNumeroSeguridad.AlwaysInEditMode = True
        Me.txt_ccNumeroSeguridad.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ccNumeroSeguridad.Location = New System.Drawing.Point(130, 124)
        Me.txt_ccNumeroSeguridad.MaxLength = 4
        Me.txt_ccNumeroSeguridad.Name = "txt_ccNumeroSeguridad"
        Me.txt_ccNumeroSeguridad.Size = New System.Drawing.Size(40, 19)
        Me.txt_ccNumeroSeguridad.TabIndex = 5
        Me.txt_ccNumeroSeguridad.Text = "123"
        '
        'lbl_codigoSeguridad
        '
        Me.lbl_codigoSeguridad.AutoSize = True
        Me.lbl_codigoSeguridad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_codigoSeguridad.Location = New System.Drawing.Point(17, 128)
        Me.lbl_codigoSeguridad.Name = "lbl_codigoSeguridad"
        Me.lbl_codigoSeguridad.Size = New System.Drawing.Size(107, 13)
        Me.lbl_codigoSeguridad.TabIndex = 0
        Me.lbl_codigoSeguridad.Text = "C�digo de seguridad:"
        '
        'txt_ccNumero
        '
        Me.txt_ccNumero.AlwaysInEditMode = True
        Me.txt_ccNumero.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ccNumero.Location = New System.Drawing.Point(130, 97)
        Me.txt_ccNumero.MaxLength = 16
        Me.txt_ccNumero.Name = "txt_ccNumero"
        Me.txt_ccNumero.Size = New System.Drawing.Size(118, 19)
        Me.txt_ccNumero.TabIndex = 4
        Me.txt_ccNumero.Text = "4242424242424242"
        '
        'lbl_numTarjeta
        '
        Me.lbl_numTarjeta.AutoSize = True
        Me.lbl_numTarjeta.BackColor = System.Drawing.Color.Transparent
        Me.lbl_numTarjeta.Location = New System.Drawing.Point(17, 101)
        Me.lbl_numTarjeta.Name = "lbl_numTarjeta"
        Me.lbl_numTarjeta.Size = New System.Drawing.Size(105, 13)
        Me.lbl_numTarjeta.TabIndex = 0
        Me.lbl_numTarjeta.Text = "N�mero en la tarjeta:"
        '
        'txt_ccNombre
        '
        Me.txt_ccNombre.AlwaysInEditMode = True
        Me.txt_ccNombre.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ccNombre.Location = New System.Drawing.Point(70, 72)
        Me.txt_ccNombre.MaxLength = 50
        Me.txt_ccNombre.Name = "txt_ccNombre"
        Me.txt_ccNombre.Size = New System.Drawing.Size(133, 19)
        Me.txt_ccNombre.TabIndex = 2
        Me.txt_ccNombre.Text = "jose"
        '
        'lbl_nombre2
        '
        Me.lbl_nombre2.AutoSize = True
        Me.lbl_nombre2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nombre2.Location = New System.Drawing.Point(17, 76)
        Me.lbl_nombre2.Name = "lbl_nombre2"
        Me.lbl_nombre2.Size = New System.Drawing.Size(47, 13)
        Me.lbl_nombre2.TabIndex = 0
        Me.lbl_nombre2.Text = "Nombre:"
        '
        'lbl_tipoTarjeta
        '
        Me.lbl_tipoTarjeta.AutoSize = True
        Me.lbl_tipoTarjeta.BackColor = System.Drawing.Color.Transparent
        Me.lbl_tipoTarjeta.Location = New System.Drawing.Point(17, 48)
        Me.lbl_tipoTarjeta.Name = "lbl_tipoTarjeta"
        Me.lbl_tipoTarjeta.Size = New System.Drawing.Size(78, 13)
        Me.lbl_tipoTarjeta.TabIndex = 0
        Me.lbl_tipoTarjeta.Text = "Tipo de tarjeta:"
        '
        'uce_paises
        '
        Me.uce_paises.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.uce_paises.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_paises.Location = New System.Drawing.Point(138, 112)
        Me.uce_paises.Name = "uce_paises"
        Me.uce_paises.Size = New System.Drawing.Size(179, 19)
        Me.uce_paises.TabIndex = 28
        '
        'lbl_pais
        '
        Me.lbl_pais.AutoSize = True
        Me.lbl_pais.BackColor = System.Drawing.Color.Transparent
        Me.lbl_pais.Location = New System.Drawing.Point(16, 115)
        Me.lbl_pais.Name = "lbl_pais"
        Me.lbl_pais.Size = New System.Drawing.Size(32, 13)
        Me.lbl_pais.TabIndex = 19
        Me.lbl_pais.Text = "Pa�s:"
        '
        'txt_email
        '
        Me.txt_email.AlwaysInEditMode = True
        Me.txt_email.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_email.Location = New System.Drawing.Point(137, 237)
        Me.txt_email.MaxLength = 80
        Me.txt_email.Name = "txt_email"
        Me.txt_email.Size = New System.Drawing.Size(179, 19)
        Me.txt_email.TabIndex = 35
        Me.txt_email.Text = "jose@oz.com.mx"
        '
        'lbl_email
        '
        Me.lbl_email.AutoSize = True
        Me.lbl_email.BackColor = System.Drawing.Color.Transparent
        Me.lbl_email.Location = New System.Drawing.Point(15, 240)
        Me.lbl_email.Name = "lbl_email"
        Me.lbl_email.Size = New System.Drawing.Size(96, 13)
        Me.lbl_email.TabIndex = 14
        Me.lbl_email.Text = "Correo electr�nico:"
        '
        'txt_area
        '
        Me.txt_area.AlwaysInEditMode = True
        Me.txt_area.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_area.Location = New System.Drawing.Point(137, 187)
        Me.txt_area.MaxLength = 3
        Me.txt_area.Name = "txt_area"
        Me.txt_area.Size = New System.Drawing.Size(41, 19)
        Me.txt_area.TabIndex = 31
        Me.txt_area.Text = "52"
        '
        'txt_cp
        '
        Me.txt_cp.AlwaysInEditMode = True
        Me.txt_cp.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_cp.Location = New System.Drawing.Point(138, 87)
        Me.txt_cp.MaxLength = 5
        Me.txt_cp.Name = "txt_cp"
        Me.txt_cp.Size = New System.Drawing.Size(107, 19)
        Me.txt_cp.TabIndex = 27
        Me.txt_cp.Text = "29100"
        '
        'lbl_cp
        '
        Me.lbl_cp.AutoSize = True
        Me.lbl_cp.BackColor = System.Drawing.Color.Transparent
        Me.lbl_cp.Location = New System.Drawing.Point(16, 90)
        Me.lbl_cp.Name = "lbl_cp"
        Me.lbl_cp.Size = New System.Drawing.Size(74, 13)
        Me.lbl_cp.TabIndex = 17
        Me.lbl_cp.Text = "Codigo postal:"
        '
        'txt_estado
        '
        Me.txt_estado.AlwaysInEditMode = True
        Me.txt_estado.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_estado.Location = New System.Drawing.Point(138, 137)
        Me.txt_estado.MaxLength = 60
        Me.txt_estado.Name = "txt_estado"
        Me.txt_estado.Size = New System.Drawing.Size(282, 19)
        Me.txt_estado.TabIndex = 28
        Me.txt_estado.Text = "bcs"
        '
        'lbl_estado
        '
        Me.lbl_estado.AutoSize = True
        Me.lbl_estado.BackColor = System.Drawing.Color.Transparent
        Me.lbl_estado.Location = New System.Drawing.Point(16, 140)
        Me.lbl_estado.Name = "lbl_estado"
        Me.lbl_estado.Size = New System.Drawing.Size(43, 13)
        Me.lbl_estado.TabIndex = 16
        Me.lbl_estado.Text = "Estado:"
        '
        'txt_ciudad
        '
        Me.txt_ciudad.AlwaysInEditMode = True
        Me.txt_ciudad.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_ciudad.Location = New System.Drawing.Point(138, 162)
        Me.txt_ciudad.MaxLength = 50
        Me.txt_ciudad.Name = "txt_ciudad"
        Me.txt_ciudad.Size = New System.Drawing.Size(282, 19)
        Me.txt_ciudad.TabIndex = 30
        Me.txt_ciudad.Text = "la paz"
        '
        'lbl_ciudad
        '
        Me.lbl_ciudad.AutoSize = True
        Me.lbl_ciudad.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ciudad.Location = New System.Drawing.Point(16, 165)
        Me.lbl_ciudad.Name = "lbl_ciudad"
        Me.lbl_ciudad.Size = New System.Drawing.Size(43, 13)
        Me.lbl_ciudad.TabIndex = 18
        Me.lbl_ciudad.Text = "Ciudad:"
        '
        'txt_direccion
        '
        Me.txt_direccion.AlwaysInEditMode = True
        Me.txt_direccion.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_direccion.Location = New System.Drawing.Point(137, 62)
        Me.txt_direccion.MaxLength = 300
        Me.txt_direccion.Name = "txt_direccion"
        Me.txt_direccion.Size = New System.Drawing.Size(282, 19)
        Me.txt_direccion.TabIndex = 26
        Me.txt_direccion.Text = "calle malecon #2345"
        '
        'lbl_direccion
        '
        Me.lbl_direccion.AutoSize = True
        Me.lbl_direccion.BackColor = System.Drawing.Color.Transparent
        Me.lbl_direccion.Location = New System.Drawing.Point(15, 65)
        Me.lbl_direccion.Name = "lbl_direccion"
        Me.lbl_direccion.Size = New System.Drawing.Size(55, 13)
        Me.lbl_direccion.TabIndex = 15
        Me.lbl_direccion.Text = "Direcci�n:"
        '
        'txt_apellido
        '
        Me.txt_apellido.AlwaysInEditMode = True
        Me.txt_apellido.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_apellido.Location = New System.Drawing.Point(137, 37)
        Me.txt_apellido.MaxLength = 50
        Me.txt_apellido.Name = "txt_apellido"
        Me.txt_apellido.Size = New System.Drawing.Size(282, 19)
        Me.txt_apellido.TabIndex = 25
        Me.txt_apellido.Text = "test book"
        '
        'lbl_apellido
        '
        Me.lbl_apellido.AutoSize = True
        Me.lbl_apellido.BackColor = System.Drawing.Color.Transparent
        Me.lbl_apellido.Location = New System.Drawing.Point(15, 40)
        Me.lbl_apellido.Name = "lbl_apellido"
        Me.lbl_apellido.Size = New System.Drawing.Size(47, 13)
        Me.lbl_apellido.TabIndex = 23
        Me.lbl_apellido.Text = "Apellido:"
        '
        'txt_nombreTraveler
        '
        Me.txt_nombreTraveler.AlwaysInEditMode = True
        Me.txt_nombreTraveler.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_nombreTraveler.Location = New System.Drawing.Point(137, 12)
        Me.txt_nombreTraveler.MaxLength = 50
        Me.txt_nombreTraveler.Name = "txt_nombreTraveler"
        Me.txt_nombreTraveler.Size = New System.Drawing.Size(282, 19)
        Me.txt_nombreTraveler.TabIndex = 24
        Me.txt_nombreTraveler.Text = "Jose TEST"
        '
        'lbl_nombre
        '
        Me.lbl_nombre.AutoSize = True
        Me.lbl_nombre.BackColor = System.Drawing.Color.Transparent
        Me.lbl_nombre.Location = New System.Drawing.Point(15, 15)
        Me.lbl_nombre.Name = "lbl_nombre"
        Me.lbl_nombre.Size = New System.Drawing.Size(47, 13)
        Me.lbl_nombre.TabIndex = 22
        Me.lbl_nombre.Text = "Nombre:"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Location = New System.Drawing.Point(425, 526)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(90, 23)
        Me.btn_cancelar.TabIndex = 39
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'btn_reservar
        '
        Me.btn_reservar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_reservar.Location = New System.Drawing.Point(290, 526)
        Me.btn_reservar.Name = "btn_reservar"
        Me.btn_reservar.Size = New System.Drawing.Size(129, 23)
        Me.btn_reservar.TabIndex = 38
        Me.btn_reservar.Text = "Aceptar"
        Me.btn_reservar.UseVisualStyleBackColor = True
        '
        'btn_importar
        '
        Appearance4.BackColor = System.Drawing.Color.Transparent
        Appearance4.Image = CType(resources.GetObject("Appearance4.Image"), Object)
        Appearance4.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance4.ImageVAlign = Infragistics.Win.VAlign.Top
        Me.btn_importar.Appearance = Appearance4
        Me.btn_importar.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton
        Me.btn_importar.Enabled = False
        Me.btn_importar.ImageSize = New System.Drawing.Size(32, 32)
        Me.btn_importar.Location = New System.Drawing.Point(439, 12)
        Me.btn_importar.Name = "btn_importar"
        Me.btn_importar.Size = New System.Drawing.Size(76, 69)
        Me.btn_importar.TabIndex = 38
        Me.btn_importar.Text = "Usar datos de entrega"
        Me.btn_importar.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        '
        'txt_telefono_adicional1
        '
        Me.txt_telefono_adicional1.AlwaysInEditMode = True
        Me.txt_telefono_adicional1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_telefono_adicional1.Location = New System.Drawing.Point(184, 212)
        Me.txt_telefono_adicional1.MaxLength = 20
        Me.txt_telefono_adicional1.Name = "txt_telefono_adicional1"
        Me.txt_telefono_adicional1.Size = New System.Drawing.Size(132, 19)
        Me.txt_telefono_adicional1.TabIndex = 34
        Me.txt_telefono_adicional1.Text = "612-12-12345"
        '
        'lbl_telefono_adicional1
        '
        Me.lbl_telefono_adicional1.AutoSize = True
        Me.lbl_telefono_adicional1.BackColor = System.Drawing.Color.Transparent
        Me.lbl_telefono_adicional1.Location = New System.Drawing.Point(15, 215)
        Me.lbl_telefono_adicional1.Name = "lbl_telefono_adicional1"
        Me.lbl_telefono_adicional1.Size = New System.Drawing.Size(42, 13)
        Me.lbl_telefono_adicional1.TabIndex = 39
        Me.lbl_telefono_adicional1.Text = "Celular:"
        '
        'txt_area_adicional1
        '
        Me.txt_area_adicional1.AlwaysInEditMode = True
        Me.txt_area_adicional1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_area_adicional1.Location = New System.Drawing.Point(137, 212)
        Me.txt_area_adicional1.MaxLength = 3
        Me.txt_area_adicional1.Name = "txt_area_adicional1"
        Me.txt_area_adicional1.Size = New System.Drawing.Size(41, 19)
        Me.txt_area_adicional1.TabIndex = 33
        Me.txt_area_adicional1.Text = "52"
        '
        'uce_estados
        '
        Me.uce_estados.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.uce_estados.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        Me.uce_estados.Location = New System.Drawing.Point(138, 137)
        Me.uce_estados.Name = "uce_estados"
        Me.uce_estados.Size = New System.Drawing.Size(179, 19)
        Me.uce_estados.TabIndex = 29
        '
        'txt_codigo
        '
        Me.txt_codigo.AlwaysInEditMode = True
        Me.txt_codigo.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_codigo.Location = New System.Drawing.Point(137, 262)
        Me.txt_codigo.MaxLength = 10
        Me.txt_codigo.Name = "txt_codigo"
        Me.txt_codigo.Size = New System.Drawing.Size(179, 19)
        Me.txt_codigo.TabIndex = 36
        '
        'lbl_codigo
        '
        Me.lbl_codigo.AutoSize = True
        Me.lbl_codigo.BackColor = System.Drawing.Color.Transparent
        Me.lbl_codigo.Location = New System.Drawing.Point(15, 265)
        Me.lbl_codigo.Name = "lbl_codigo"
        Me.lbl_codigo.Size = New System.Drawing.Size(43, 13)
        Me.lbl_codigo.TabIndex = 40
        Me.lbl_codigo.Text = "C�digo:"
        '
        'lbl_val_email
        '
        Me.lbl_val_email.AutoSize = True
        Me.lbl_val_email.BackColor = System.Drawing.Color.Transparent
        Me.lbl_val_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_val_email.ForeColor = System.Drawing.Color.Red
        Me.lbl_val_email.Location = New System.Drawing.Point(322, 240)
        Me.lbl_val_email.Name = "lbl_val_email"
        Me.lbl_val_email.Size = New System.Drawing.Size(13, 16)
        Me.lbl_val_email.TabIndex = 41
        Me.lbl_val_email.Text = "*"
        Me.lbl_val_email.Visible = False
        '
        'lbl_homoclave
        '
        Me.lbl_homoclave.AutoSize = True
        Me.lbl_homoclave.BackColor = System.Drawing.Color.Transparent
        Me.lbl_homoclave.Location = New System.Drawing.Point(16, 289)
        Me.lbl_homoclave.Name = "lbl_homoclave"
        Me.lbl_homoclave.Size = New System.Drawing.Size(64, 13)
        Me.lbl_homoclave.TabIndex = 42
        Me.lbl_homoclave.Text = "Homoclave:"
        '
        'txt_homoclave
        '
        Me.txt_homoclave.AlwaysInEditMode = True
        Me.txt_homoclave.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_homoclave.Location = New System.Drawing.Point(137, 286)
        Me.txt_homoclave.MaxLength = 2
        Me.txt_homoclave.Name = "txt_homoclave"
        Me.txt_homoclave.Size = New System.Drawing.Size(24, 19)
        Me.txt_homoclave.TabIndex = 37
        '
        'lbl_Canales
        '
        Me.lbl_Canales.AutoSize = True
        Me.lbl_Canales.Location = New System.Drawing.Point(188, 289)
        Me.lbl_Canales.Name = "lbl_Canales"
        Me.lbl_Canales.Size = New System.Drawing.Size(48, 13)
        Me.lbl_Canales.TabIndex = 43
        Me.lbl_Canales.Text = "Canales:"
        '
        'uce_Canales
        '
        Me.uce_Canales.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.uce_Canales.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList
        ValueListItem1.DataValue = "0"
        ValueListItem1.DisplayText = "Hotel"
        ValueListItem2.DataValue = "1"
        ValueListItem2.DisplayText = "Visite Hoteles de M�xico"
        Me.uce_Canales.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem1, ValueListItem2})
        Me.uce_Canales.Location = New System.Drawing.Point(242, 286)
        Me.uce_Canales.Name = "uce_Canales"
        Me.uce_Canales.Size = New System.Drawing.Size(179, 19)
        Me.uce_Canales.TabIndex = 37
        '
        'txt_Canal
        '
        Me.txt_Canal.AlwaysInEditMode = True
        Me.txt_Canal.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.txt_Canal.Location = New System.Drawing.Point(242, 286)
        Me.txt_Canal.MaxLength = 60
        Me.txt_Canal.Name = "txt_Canal"
        Me.txt_Canal.Size = New System.Drawing.Size(282, 19)
        Me.txt_Canal.TabIndex = 44
        '
        'datosCliente
        '
        Me.AcceptButton = Me.btn_reservar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btn_cancelar
        Me.ClientSize = New System.Drawing.Size(528, 553)
        Me.Controls.Add(Me.uce_Canales)
        Me.Controls.Add(Me.lbl_Canales)
        Me.Controls.Add(Me.txt_homoclave)
        Me.Controls.Add(Me.lbl_homoclave)
        Me.Controls.Add(Me.lbl_val_email)
        Me.Controls.Add(Me.txt_codigo)
        Me.Controls.Add(Me.lbl_codigo)
        Me.Controls.Add(Me.uce_estados)
        Me.Controls.Add(Me.txt_telefono_adicional1)
        Me.Controls.Add(Me.lbl_telefono_adicional1)
        Me.Controls.Add(Me.txt_area_adicional1)
        Me.Controls.Add(Me.btn_importar)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_reservar)
        Me.Controls.Add(Me.txt_telefono)
        Me.Controls.Add(Me.lbl_telefono)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.uce_paises)
        Me.Controls.Add(Me.lbl_pais)
        Me.Controls.Add(Me.txt_email)
        Me.Controls.Add(Me.lbl_email)
        Me.Controls.Add(Me.txt_area)
        Me.Controls.Add(Me.txt_cp)
        Me.Controls.Add(Me.lbl_cp)
        Me.Controls.Add(Me.txt_estado)
        Me.Controls.Add(Me.lbl_estado)
        Me.Controls.Add(Me.txt_ciudad)
        Me.Controls.Add(Me.lbl_ciudad)
        Me.Controls.Add(Me.txt_direccion)
        Me.Controls.Add(Me.lbl_direccion)
        Me.Controls.Add(Me.txt_apellido)
        Me.Controls.Add(Me.lbl_apellido)
        Me.Controls.Add(Me.txt_nombreTraveler)
        Me.Controls.Add(Me.lbl_nombre)
        Me.Controls.Add(Me.txt_Canal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "datosCliente"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos del cliente"
        CType(Me.txt_telefono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.txt_ccApellido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_tipoTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_AAAA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_MM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ccNumeroSeguridad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ccNumero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ccNombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_paises, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_email, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_area, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_cp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_estado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ciudad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_direccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_apellido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nombreTraveler, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_telefono_adicional1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_area_adicional1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_estados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_codigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_homoclave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uce_Canales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Canal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_telefono As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_telefono As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents ckb_modificar As System.Windows.Forms.CheckBox
    Friend WithEvents uce_tipoTarjeta As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents lbl_AAAA2 As System.Windows.Forms.Label
    Friend WithEvents txt_AAAA As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_MM2 As System.Windows.Forms.Label
    Friend WithEvents txt_MM As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_fechaExpiracion As System.Windows.Forms.Label
    Friend WithEvents txt_ccNumeroSeguridad As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_codigoSeguridad As System.Windows.Forms.Label
    Friend WithEvents txt_ccNumero As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_numTarjeta As System.Windows.Forms.Label
    Friend WithEvents txt_ccNombre As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nombre2 As System.Windows.Forms.Label
    Friend WithEvents lbl_tipoTarjeta As System.Windows.Forms.Label
    Friend WithEvents uce_paises As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents lbl_pais As System.Windows.Forms.Label
    Friend WithEvents txt_email As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_email As System.Windows.Forms.Label
    Friend WithEvents txt_area As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents txt_cp As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_cp As System.Windows.Forms.Label
    Friend WithEvents txt_estado As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_estado As System.Windows.Forms.Label
    Friend WithEvents txt_ciudad As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_ciudad As System.Windows.Forms.Label
    Friend WithEvents txt_direccion As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_direccion As System.Windows.Forms.Label
    Friend WithEvents txt_apellido As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_apellido As System.Windows.Forms.Label
    Friend WithEvents txt_nombreTraveler As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_nombre As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_reservar As System.Windows.Forms.Button
    Friend WithEvents txt_ccApellido As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents btn_importar As Infragistics.Win.Misc.UltraButton
    Friend WithEvents txt_telefono_adicional1 As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_telefono_adicional1 As System.Windows.Forms.Label
    Friend WithEvents txt_area_adicional1 As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents uce_estados As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents txt_codigo As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_codigo As System.Windows.Forms.Label
    Friend WithEvents lbl_val_email As System.Windows.Forms.Label
    Friend WithEvents lbl_homoclave As System.Windows.Forms.Label
    Friend WithEvents txt_homoclave As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents lbl_Canales As System.Windows.Forms.Label
    Friend WithEvents uce_Canales As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents txt_Canal As Infragistics.Win.UltraWinEditors.UltraTextEditor
End Class
