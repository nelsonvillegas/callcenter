Imports System.Data.SqlClient

Public Class Reservaciones
    Private MI_THREAD As System.Threading.Thread
    ' Private termino_tread As Boolean = True
    Public botones() As String
    Public datosBusqueda_paquete As dataBusqueda_paquete
    Public datosBusqueda_vuelo As dataBusqueda_vuelo
    Public datosBusqueda_auto As dataBusqueda_auto_test
    Public datosBusqueda_hotel As dataBusqueda_hotel_test
    Public datosBusqueda_actividad As dataBusqueda_actividad_test
    Public Cliente As String
    Public Sub New() 'ByVal dc As dataCall

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'datos_call = dc
        uce_tipoFecha.SelectedIndex = 2
        uce_rubro.SelectedIndex = 0
        opt_transacciones.CheckedIndex = 0
    End Sub

    Private Sub los_TXT_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_ciudad.TextChanged, txt_compania.TextChanged
        ',txt_numConfirm.TextChanged,txt_cliente.TextChanged()
        CapaLogicaNegocios.Reservations.update_filtros(Me)
    End Sub

    Private Sub los_TXT_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_ciudad.KeyDown, txt_compania.KeyDown
        ', txt_numConfirm.KeyDown, txt_cliente.KeyDown
        If UltraGrid1.Rows.Count = 0 Then Return
        If UltraGrid1.ActiveRow Is Nothing Then UltraGrid1.ActiveRow = UltraGrid1.Rows(0)

        Select Case e.KeyCode
            Case Keys.Down
                If UltraGrid1.ActiveRow.Index + 1 = UltraGrid1.Rows.Count Then UltraGrid1.ActiveRow = UltraGrid1.Rows(0) Else UltraGrid1.ActiveRow = UltraGrid1.Rows(UltraGrid1.ActiveRow.Index + 1)
            Case Keys.Up
                If UltraGrid1.ActiveRow.Index = 0 Then UltraGrid1.ActiveRow = UltraGrid1.Rows(UltraGrid1.Rows.Count - 1) Else UltraGrid1.ActiveRow = UltraGrid1.Rows(UltraGrid1.ActiveRow.Index - 1)
            Case Keys.Enter
                misForms.exe_accion(ContextMenuStrip1, "btn_detalles")
        End Select

        '----

        Dim txt As Infragistics.Win.UltraWinEditors.UltraTextEditor = sender
        Select Case e.KeyCode
            Case Keys.Down
                txt.SelectionStart -= 1
            Case Keys.Up
                txt.SelectionStart += 1
        End Select
    End Sub

    Private Sub losDTP_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_out.ValueChanged, dtp_in.ValueChanged
        herramientas.fechas_inteligentes(dtp_in, dtp_out, sender, 0, Nothing)
    End Sub

    Public Sub load_data()
        UltraGrid1.DataSource = Nothing
        btn_excelExport.Enabled = False
        PictureBox2.Visible = True

            Dim ini As DateTime
            Dim fin As DateTime
            If dtp_in.Checked Then ini = dtp_in.Value Else ini = DateTime.MinValue
        If dtp_out.Checked Then fin = dtp_out.Value Else fin = DateTime.Now
        Dim opt_transaction_selected As String = Nothing
        If (opt_transacciones.CheckedItem IsNot Nothing) Then
            opt_transaction_selected = opt_transacciones.CheckedItem.DataValue
        End If

        If MI_THREAD IsNot Nothing Then
            MI_THREAD.Abort()
            MI_THREAD = Nothing
        End If

        'termino_tread = False
        Dim idEmpresa As Integer = 0        
        MI_THREAD = New System.Threading.Thread(AddressOf CapaLogicaNegocios.Reservations.busca_reservaciones)
        If dataOperador.Hoteles.Count > 0 Then
            MI_THREAD.Start(New Object() {ini, fin, uce_tipoFecha.SelectedItem.DataValue, uce_rubro.SelectedItem.DataValue, txt_cliente.Text, txt_numConfirm.Text, Me, uce_source.SelectedItem.DataValue, opt_transaction_selected, CmbSearch1.getText().Trim(), DirectCast(cmbSegmento.SelectedItem, ListData)._Value, ws_login.UserId, dataOperador.Hoteles(0).idEmpresa})
        Else
            MI_THREAD.Start(New Object() {ini, fin, uce_tipoFecha.SelectedItem.DataValue, uce_rubro.SelectedItem.DataValue, txt_cliente.Text, txt_numConfirm.Text, Me, uce_source.SelectedItem.DataValue, opt_transaction_selected, CmbSearch1.getText().Trim(), DirectCast(cmbSegmento.SelectedItem, ListData)._Value, ws_login.UserId, Nothing})
        End If
    End Sub

    Private Sub UltraGrid1_AfterRowActivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UltraGrid1.AfterRowActivate
        'este while y la ver "termino_tread", parece que no eran nesesarios
        'While Not termino_tread
        '    Threading.Thread.Sleep(20)
        'End While

        'paquete
        Cliente = ""
        If UltraGrid1.ActiveRow.Cells("idRubro").Value = enumRubrosPassport.Paquetes Then
            datosBusqueda_paquete = New dataBusqueda_paquete
            datosBusqueda_paquete.itinerario = UltraGrid1.ActiveRow.Cells("NoReservacion").Value
        Else
            datosBusqueda_paquete = Nothing
        End If

        'vuelo
        If UltraGrid1.ActiveRow.Cells("idRubro").Value = enumRubrosPassport.Vuelos Then
            datosBusqueda_vuelo = New dataBusqueda_vuelo
            datosBusqueda_vuelo.ReservationId = UltraGrid1.ActiveRow.Cells("idReservacion").Value
            datosBusqueda_vuelo.ConfirmNumber = UltraGrid1.ActiveRow.Cells("NoReservacion").Value
            datosBusqueda_vuelo.status = UltraGrid1.ActiveRow.Cells("Status").Value
        Else
            datosBusqueda_vuelo = Nothing
        End If

        'auto
        If UltraGrid1.ActiveRow.Cells("idRubro").Value = enumRubrosPassport.Autos Then
            datosBusqueda_auto = New dataBusqueda_auto_test
            datosBusqueda_auto.IdReservation = UltraGrid1.ActiveRow.Cells("idReservacion").Value
            datosBusqueda_auto.ConfirmNumber = UltraGrid1.ActiveRow.Cells("NoReservacion").Value
            datosBusqueda_auto.Status = UltraGrid1.ActiveRow.Cells("Status").Value
        Else
            datosBusqueda_auto = Nothing
        End If

        'hotel
        If UltraGrid1.ActiveRow.Cells("idRubro").Value = enumRubrosPassport.Hotel Then
            Cliente = UltraGrid1.ActiveRow.Cells("Cliente").Value
            datosBusqueda_hotel = New dataBusqueda_hotel_test
            datosBusqueda_hotel.ReservationId = UltraGrid1.ActiveRow.Cells("idReservacion").Value
            datosBusqueda_hotel.ConfirmNumber = UltraGrid1.ActiveRow.Cells("NoReservacion").Value
            datosBusqueda_hotel.Status = UltraGrid1.ActiveRow.Cells("Status").Value
            'datosBusqueda_hotel.ServiceProvider = UltraGrid1.ActiveRow.Cells("ServiceProvider").Value
            If UltraGrid1.ActiveRow.Cells("ServiceProvider").Value IsNot System.DBNull.Value Then datosBusqueda_hotel.ServiceProvider = UltraGrid1.ActiveRow.Cells("ServiceProvider").Value
        Else
            datosBusqueda_hotel = Nothing
        End If

        'actividad
        If UltraGrid1.ActiveRow.Cells("idRubro").Value = enumRubrosPassport.Actividades Then
            datosBusqueda_actividad = New dataBusqueda_actividad_test
            datosBusqueda_actividad.ReservationID = UltraGrid1.ActiveRow.Cells("idReservacion").Value
            datosBusqueda_actividad.ConfirmNumber = UltraGrid1.ActiveRow.Cells("NoReservacion").Value
            datosBusqueda_actividad.status = UltraGrid1.ActiveRow.Cells("Status").Value
        Else
            datosBusqueda_actividad = Nothing
        End If




        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_detalles").SharedProps.Enabled = True
        ''CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = True
        'CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = True

        'Not ws_login.getUserAccessInfo.IsidCorporativoNull AndAlso ws_login.getUserAccessInfo.idCorporativo <> -1
        If UltraGrid1.ActiveRow.Cells("Status").Value.ToString = "3" OrElse UltraGrid1.ActiveRow.Cells("source").Value = "WIZ" OrElse UltraGrid1.ActiveRow.Cells("source").Value = "ADS" OrElse UltraGrid1.ActiveRow.Cells("source").Value = "IDS" Then
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Enabled = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambiar").SharedProps.Enabled = False
            If UltraGrid1.ActiveRow.Cells("source").Value = "WIZ" Or UltraGrid1.ActiveRow.Cells("source").Value = "ADS" OrElse UltraGrid1.ActiveRow.Cells("source").Value = "IDS" Then
            Else
                CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reactivar").SharedProps.Enabled = True

            End If
        Else
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reactivar").SharedProps.Enabled = False
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Enabled = True
            CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambios").SharedProps.Enabled = True
        End If
        Dim index As Integer = Me.uce_rubro.SelectedIndex

        CapaPresentacion.Idiomas.cambiar_reservaciones(Me)
        menuContextual.inicializar_items(Me.ContextMenuStrip1, Me.botones)
        Me.uce_rubro.SelectedIndex = index
        'misForms.exe_accion(ContextMenuStrip1, "btn_detalles")
        'UltraGrid1.Focus()
    End Sub

    Private Sub UltraGrid1_DoubleClickRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs) Handles UltraGrid1.DoubleClickRow
        UltraGrid1.ActiveRow = e.Row
        misForms.exe_accion(ContextMenuStrip1, "btn_detalles")
    End Sub

    Private Sub UltraGrid1_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles UltraGrid1.MouseDown
        herramientas.selRow(UltraGrid1, e)

        'Verificamos si se le dio clic a TransaccionPMS
        Dim element As Infragistics.Win.UIElement = Nothing
        element = UltraGrid1.DisplayLayout.UIElement.ElementFromPoint(e.Location)

        If element IsNot Nothing Then
            Dim cellUI As Infragistics.Win.UltraWinGrid.CellUIElement = element.GetAncestor(GetType(Infragistics.Win.UltraWinGrid.CellUIElement))
            If cellUI IsNot Nothing Then
                Dim cell As Infragistics.Win.UltraWinGrid.UltraGridCell = CType(cellUI.GetContext(GetType(Infragistics.Win.UltraWinGrid.UltraGridCell)), Infragistics.Win.UltraWinGrid.UltraGridCell)
                If cell IsNot Nothing Then
                    If cell.Column.Key = "PendientesPMSTXT" And UltraGrid1.ActiveRow.Cells("PendientesPMS").Value = "1" Then
                        CapaLogicaNegocios.ReservationDetails.pms_transaction_retry(datosBusqueda_hotel.ReservationId)
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub UltraGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraGrid1.KeyDown
        If e.KeyCode = Keys.Enter Then
            misForms.exe_accion(ContextMenuStrip1, "btn_detalles")
        End If
    End Sub

    Private Sub UltraGrid1_InitializeRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UltraGrid1.InitializeRow
        If (e.Row.Cells("status").Value.ToString().Trim() = "3") Then
            e.Row.Appearance.ForeColor = Color.Gray
            e.Row.Cells("StatusTXT").Appearance.ForeColor = Color.Red
        End If
        If (e.Row.Cells("status").Value.ToString().Trim() = "1") Then
            e.Row.Cells("StatusTXT").Appearance.ForeColor = Color.Navy
        End If
        If (e.Row.Cells("status").Value.ToString().Trim() = "4") Then
            e.Row.Cells("StatusTXT").Appearance.ForeColor = Color.DarkGreen
        End If
        If (e.Row.Cells("PendientesPMS").Value.ToString() = "1") Then
            'Convertimos en link la columna de PMS
            With e.Row.Cells("PendientesPMSTXT")
                .Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                .Appearance.ForeColor = Color.Blue
                .Appearance.FontData.Underline = Infragistics.Win.DefaultableBoolean.True
                .Appearance.Cursor = Cursors.Hand
                .Appearance.TextHAlign = Infragistics.Win.HAlign.Center
            End With
        Else
            With e.Row.Cells("PendientesPMSTXT")
                .Appearance.ForeColor = Color.DarkGray
                .Appearance.TextHAlign = Infragistics.Win.HAlign.Center
            End With
        End If

    End Sub

    Private Sub UltraGrid1_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout
        'Habilitamos o no la exportacion a excel si tiene o no registros el grid
        btn_excelExport.Enabled = (e.Layout.Rows.Count > 0)

    End Sub

    Private Sub reservaciones_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave

    End Sub

    Private Sub reservaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        misForms.reservaciones = Me
        Call CargaSegmentos()
        CapaPresentacion.Common.FormatUltraGrid(UltraGrid1)
        botones = New String() {"btn_detalles", "btn_modificar", "btn_cancelar", "btn_reactivar", "btn_copiar", "btn_cambios"}
        'botones = New String() {"btn_detalles", "btn_modificar", "btn_cancelar", "btn_reactivar"}
        CapaPresentacion.Idiomas.cambiar_reservaciones(Me)
        CmbSearch1.crea_lista(Me, GroupBox2.Left + CmbSearch1.Left, GroupBox2.Top)
        AddHandler CmbSearch1.llena, AddressOf CargaEmpresasSegmento
    End Sub

    Private Sub reservaciones_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        misForms.reservaciones = Nothing

        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reservaciones").SharedProps.Enabled = True
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_detalles").SharedProps.Enabled = False
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_modificar").SharedProps.Enabled = False
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cancelar").SharedProps.Enabled = False
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_reactivar").SharedProps.Enabled = False
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_copiar").SharedProps.Enabled = False
        CapaPresentacion.Common.RibbonTools.get_ButtonTool("btn_cambios").SharedProps.Enabled = False
        If misForms.form1 IsNot Nothing Then
            If misForms.form1.UltraDockManager1.DockAreas(0).Panes.Exists("detallesReservacion") Then
                misForms.form1.UltraDockManager1.DockAreas(0).Panes("detallesReservacion").Close()
            End If
        End If
    End Sub

    Private Sub reservaciones_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        txt_cliente.Focus()
    End Sub

    Private Sub ContextMenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ContextMenuStrip1.ItemClicked
        ContextMenuStrip1.Visible = False
        misForms.exe_accion(ContextMenuStrip1, e.ClickedItem.Name)
    End Sub

    Private Sub btn_actualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_actualizar.Click
        actualize()
    End Sub

    Private Sub btn_limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_limpiar.Click
        CapaLogicaNegocios.Reservations.clear_filtros(Me)

        'PictureBox2.Visible = True
        'Me.load_data()
    End Sub

    Private Sub btn_excelExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_excelExport.Click
        Try
            With SaveFileDialog1
                .FileName = CapaPresentacion.Idiomas.get_str("str_474_excelFileName")
                .DefaultExt = "xls"
                .Filter = CapaPresentacion.Idiomas.get_str("str_475_excelBook") + " (*.xls)|*.xls"
                .FilterIndex = 0
                .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                If .ShowDialog = DialogResult.OK Then
                    UltraGridExcelExporter1.Export(UltraGrid1, .FileName)
                    If MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_476_excelMessage"), CapaPresentacion.Idiomas.get_str("str_477_excelMessageTitle"), MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                        System.Diagnostics.Process.Start(.FileName)
                    End If
                Else
                    Exit Sub
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_478_excelError"), CapaPresentacion.Idiomas.get_str("str_479_excelErrorTitle"))
        End Try

    End Sub

    Private Sub CargaSegmentos()
        cmbSegmento.Items.Clear()
        cmbSegmento.DisplayMember = "Name"
        cmbSegmento.ValueMember = "_Value"

        cmbSegmento.Items.Add(New ListData(CapaPresentacion.Idiomas.get_str("str_354_todos"), ""))
        For Each dr As DataRow In herramientas.get_Segmentos(dataOperador.idCorporativo).Tables(0).Rows
            cmbSegmento.Items.Add(New ListData(dr("Nombre"), dr("id_Segmento")))
        Next
        cmbSegmento.SelectedIndex = 0
    End Sub

    Private Sub CargaEmpresasSegmento()
        Dim id_Segmento As String = ""
        If Not cmbSegmento.SelectedItem Is Nothing Then
            id_Segmento = cmbSegmento.SelectedItem._value
        End If

        If IsNothing(CmbSearch1.ListBox1) Then Exit Sub

        CmbSearch1.ListBox1.Items.Clear()
        CmbSearch1.lista.Clear()
        
        Dim ds As DataSet = herramientas.get_Empresas_filtro(CmbSearch1.TextBox1.Text) 'herramientas.get_Empresas_Segmento(id_Segmento, CmbSearch1.TextBox1.Text)
        If ds.Tables(0).Rows.Count = 1 Then
            CmbSearch1.ListBox1.Items.Clear()
            CmbSearch1.lista.Clear()
        End If

        For Each dr As DataRow In ds.Tables(0).Rows
            'cmbEmpresas.Items.Add(New ListData(dr("Nombre"), dr("id_Empresa")))
            CmbSearch1.ListBox1.Items.Add(New ListData(dr("Nombre").ToString, dr("id_Empresa").ToString))
            CmbSearch1.lista.Add(New ListData(dr("Nombre").ToString, dr("id_Empresa").ToString))
        Next
        'End If
        'cmbEmpresas.SelectedIndex = 0
        If CmbSearch1.ListBox1.Items.Count > 0 Then CmbSearch1.ListBox1.SelectedIndex = 0
    End Sub

    Public Sub actualize()
        Me.load_data()
        CapaLogicaNegocios.Reservations.update_filtros(Me)
    End Sub

End Class