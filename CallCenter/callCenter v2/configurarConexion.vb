Public Class configurarConexion

    Private Sub configurarConexion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_data()
        CapaPresentacion.Idiomas.cambiar_configurarConexion(Me)
    End Sub

    Private Sub btn_cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub

    Private Sub btn_guardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_guardar.Click
        If guardar Then
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0181_infoActualizada"), CapaPresentacion.Idiomas.get_str("str_0164_confirmacion"), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        Else
            MessageBox.Show(CapaPresentacion.Idiomas.get_str("str_0182_errCamWS") + vbCrLf + CapaPresentacion.Idiomas.get_str("str_0183_errReadFile") + " " + My.Application.Info.DirectoryPath + "\" + CapaAccesoDatos.XML.localData.XMLfile_name, CapaPresentacion.Idiomas.get_str("str_0155_atencion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Function load_data() As Boolean
        Try
            'Dim xmlD As System.Xml.XmlDocument = CapaAccesoDatos.XML.localData.ws_doc()

            'For Each ws_nodo As System.Xml.XmlNode In xmlD.DocumentElement.ChildNodes
            '    Select Case ws_nodo.Attributes("name").Value
            '        Case "services"
            '            txt_wsServices.Text = ws_nodo.InnerText

            '        Case "passport"
            '            txt_wsPassport.Text = ws_nodo.InnerText

            '        Case "calls"
            '            txt_wsAdminCalls.Text = ws_nodo.InnerText

            '        Case "update"
            '            txt_wsDestinosUpdate.Text = ws_nodo.InnerText

            '        Case "servidorSQL"
            '            txt_servidorSQL.Text = ws_nodo.InnerText
            '    End Select
            'Next

            txt_wsServices.Text = CapaAccesoDatos.XML.wsData.UrlWsServices
            txt_wsPassport.Text = CapaAccesoDatos.XML.wsData.UrlWsPassport
            txt_wsAdminCalls.Text = CapaAccesoDatos.XML.wsData.UrlWsCalls
            txt_wsDestinosUpdate.Text = CapaAccesoDatos.XML.wsData.UrlWsUpdate
            txt_servidorSQL.Text = CapaAccesoDatos.XML.localData.ServidorSQL
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0090", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.LogAndShow)
        End Try
    End Function

    Private Function guardar() As Boolean
        Try
            CapaAccesoDatos.XML.wsData.UrlWsServices = txt_wsServices.Text
            CapaAccesoDatos.XML.wsData.UrlWsPassport = txt_wsPassport.Text
            CapaAccesoDatos.XML.wsData.UrlWsCalls = txt_wsAdminCalls.Text
            CapaAccesoDatos.XML.wsData.UrlWsUpdate = txt_wsDestinosUpdate.Text
            CapaAccesoDatos.XML.localData.ServidorSQL = txt_servidorSQL.Text

            'como posiblemente cambio el WS de destinosUpdates o la cadena
            'de conexion, se pone en true (por que alomejro entro desde el
            'boton Configurar del Login) para que busque con los nuevos parametros
            busca_updates = True

            Return True
        Catch ex As Exception
            CapaLogicaNegocios.ErrorManager.Manage("E0091", ex.Message, CapaLogicaNegocios.ErrorManager.ErrorAction.Log)
            Return False
        End Try
    End Function

End Class