SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[destinos_fecha]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[destinos_fecha](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [smalldatetime] NULL,
 CONSTRAINT [PK_destinos_fecha] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
