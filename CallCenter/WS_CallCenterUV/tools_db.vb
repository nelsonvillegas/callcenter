Imports System.Data.SqlClient

Public Class tools_db

    Public Shared Function str_conn() As String 'ByVal db As String
        'Return "Data Source=" + el_servidor + ";database=" + db + ";Integrated Security=SSPI;Persist Security Info=False;"
        Return System.Configuration.ConfigurationManager.AppSettings("oz_uv_conn")
    End Function

    Public Shared Function exe_db_scal(ByVal q As String) As Object
        Try
            Dim conn As New SqlConnection()
            conn.ConnectionString = str_conn()

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = q
            cmd.Connection = conn

            Dim result As Object
            Dim previousConnectionState As ConnectionState = conn.State

            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteScalar()

            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If

            Return result
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function exe_db_fill(ByVal q As String) As DataSet
        Try
            Dim conn As New SqlConnection()
            conn.ConnectionString = str_conn()

            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = q
            cmd.Connection = conn

            Dim adapter As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            adapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function getCompanys(ByVal idRubro As String, ByVal Nombre As String, Optional ByVal idEstado As String = "", Optional ByVal idMunicipio As String = "", Optional ByVal idCiudad As String = "", Optional ByVal status As String = "", Optional ByVal idCorporativo As String = "", Optional ByVal idUsiarioCallcenter As Integer = 0) As DataSet
        Dim conection As New SqlConnection(str_conn())
        Dim command As New SqlCommand("spCompanySearchCompanys", conection)
        If idEstado = "" Then idEstado = "-1"
        If idMunicipio = "" Then idMunicipio = "-1"
        If idCiudad = "" Then idCiudad = "-1"
        With command
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add(New SqlParameter("@idRubro", idRubro))
            .Parameters.Add(New SqlParameter("@Nombre", Nombre.Replace("'", "")))
            .Parameters.Add(New SqlParameter("@idEstado", idEstado))
            .Parameters.Add(New SqlParameter("@idMunicipio", idMunicipio))
            .Parameters.Add(New SqlParameter("@idCiudad", idCiudad))
            .Parameters.Add(New SqlParameter("@status", status))
            If idUsiarioCallcenter <> 0 Then .Parameters.Add(New SqlParameter("@idUsaurioCallcenter", idUsiarioCallcenter)) 'campo agregado para filtrar hoteles por usarios
            If idCorporativo <> "" Then .Parameters.Add(New SqlParameter("@idCorporativo", idCorporativo))



        End With
        Dim adapter As New SqlDataAdapter(command)
        Dim dRes As New DataSet
        adapter.Fill(dRes)
        Return dRes
    End Function


    Public Shared Function getRooms(ByVal idHotel As Integer, ByVal idioma As Integer) As DataSet
        Dim conection As New SqlConnection(str_conn())
        Dim command As New SqlCommand("sp_uvcc_getRooms", conection)
        With command
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add(New SqlParameter("@idHotel", idHotel))
            .Parameters.Add(New SqlParameter("@ididioma", idioma))
        End With
        Dim adapter As New SqlDataAdapter(command)
        Dim dRes As New DataSet
        adapter.Fill(dRes)
        Return dRes
    End Function
End Class
