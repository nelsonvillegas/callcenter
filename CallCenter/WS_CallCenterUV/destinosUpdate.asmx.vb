Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data.SqlClient

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> <System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> <ToolboxItem(False)> _
Public Class destinosUpdate_service
    Inherits System.Web.Services.WebService

    <WebMethod()> Public Function IsClient_Outdated(ByVal fecha As String) As Boolean
        'la fecha debe ser ddMMyyyy
        Dim cliente_update As DateTime = DateTime.ParseExact(fecha, "ddMMyyyy", Nothing)
        Dim server_update As DateTime = tools_db.exe_db_scal("select top 1 fecha from destinos_fecha")
        If server_update.Year = 1 Then Return False

        If cliente_update < server_update Then Return True Else Return False
    End Function

    <WebMethod()> Public Function GetServer_Date_NoFormat() As String
        Dim server_update As String = tools_db.exe_db_scal("select top 1 fecha from destinos_fecha")
        Return server_update
    End Function

    <WebMethod()> Public Function GetServer_Date() As String
        '
        '   OJO - formato fecha cambio
        '
        'la fecha retornada debe ser yyyyMMdd

        Dim server_update As String = tools_db.exe_db_scal("select top 1 convert(nvarchar(8),fecha,112) from destinos_fecha")
        Return server_update
    End Function

    <WebMethod()> Public Function GetServer_Date_Catalogos(ByVal idCorporativo As String) As String
        '
        '   OJO - formato fecha cambio
        '
        'la fecha retornada debe ser yyyyMMdd

        Dim server_update As String = tools_db.exe_db_scal("select top 1 convert(nvarchar(8),fecha,112)+REPLACE(convert(nvarchar(5),fecha,114),':','') from uvcc_actualizacion_fecha where idCorporativo=" & idCorporativo)
        Return server_update
    End Function


    <WebMethod()> Public Function obten_actualizaciones() As Byte()
        Dim server_update As DateTime = tools_db.exe_db_scal("select top 1 fecha from destinos_fecha")
        Dim nombre As String = "uv_cc_data_" + Str(server_update.Year) + "_" + Str(server_update.Month) + "_" + Str(server_update.Day) + "_" + Str(server_update.Hour) + "_" + Str(server_update.Minute)

        Dim zip_ruta As String = System.IO.Path.GetTempPath() + nombre + ".rar"

        If Not System.IO.File.Exists(zip_ruta) Then
            Dim zip As New Ionic.Utils.Zip.ZipFile(zip_ruta)
            add_file("Table", "Aeropuertos", zip)
            add_file("Table", "Ciudades", zip)
            add_file("Table", "Estados", zip)
            add_file("Table", "Municipios", zip)
            add_file("Table", "Paises", zip)
            add_file("Table", "Zonas", zip)
            add_file("Table", "PlacesDictionary", zip)
            add_file("Table", "Areas", zip)
            add_file("Table", "AreasCiudad", zip)
            add_file("Table", "Diccionario", zip, " WHERE idDiccionario IN (select idDiccionarioNombre from Areas)")
            zip.Save()
        End If

        Dim zip_bytes() As Byte = get_zip_bytes(zip_ruta)

        Return zip_bytes
    End Function

    <WebMethod()> Public Function obten_actualizaciones_catalogos(ByVal idCorporativo As String) As Byte()
        Dim server_update As DateTime = tools_db.exe_db_scal("select top 1 fecha from uvcc_actualizacion_fecha where idCorporativo=" & idCorporativo)
        Dim nombre As String = "uv_cc_data2_" + Str(server_update.Year) + "_" + Str(server_update.Month) + "_" + Str(server_update.Day) + "_" + Str(server_update.Hour) + "_" + Str(server_update.Minute)

        Dim zip_ruta As String = System.IO.Path.GetTempPath() + nombre + ".rar"

        If Not System.IO.File.Exists(zip_ruta) Then
            Dim zip As New Ionic.Utils.Zip.ZipFile(zip_ruta)
            add_file("Table", "uvcc_mediospromocion", zip, " WHERE idCorporativo=" & idCorporativo)
            add_file("Table", "uvcc_medios", zip, " WHERE idCorporativo=" & idCorporativo)
            add_file("Table", "uvcc_empresas", zip, " WHERE idCorporativo=" & idCorporativo)
            add_file("Table", "uvcc_segmentos", zip, " WHERE idCorporativo=" & idCorporativo)
            zip.Save()
        End If

        Dim zip_bytes() As Byte = get_zip_bytes(zip_ruta)

        Return zip_bytes
    End Function

    <WebMethod(Description:="Busqueda de empresa por nombre y rubro")> Public Function obten_empresas(ByVal idRubro As String, ByVal Nombre As String, ByVal idEstado As String, ByVal idMunicipio As String, ByVal idCiudad As String, ByVal status As String) As DataSet
        Dim res As DataSet
        idRubro = idRubro.Trim
        Nombre = Nombre.Trim
        idEstado = idEstado.Trim
        idMunicipio = idMunicipio.Trim
        idCiudad = idCiudad.Trim
        status = status.Trim
        res = tools_db.getCompanys(idRubro, Nombre, idEstado, idMunicipio, idCiudad, status)
        Return res
    End Function

    <WebMethod(Description:="Busqueda de empresa por nombre y rubro")> Public Function obten_empresas_corporativo(ByVal idRubro As String, ByVal Nombre As String, ByVal idEstado As String, ByVal idMunicipio As String, ByVal idCiudad As String, ByVal status As String, ByVal idCorporativo As String, ByVal idUsuarioCallcenter As Integer) As DataSet
        Dim res As DataSet
        idRubro = idRubro.Trim
        Nombre = Nombre.Trim
        idEstado = idEstado.Trim
        idMunicipio = idMunicipio.Trim
        idCiudad = idCiudad.Trim
        status = status.Trim
        res = tools_db.getCompanys(idRubro, Nombre, idEstado, idMunicipio, idCiudad, status, idCorporativo, idUsuarioCallcenter)
        Return res
    End Function

    <WebMethod(Description:="Busqueda de habitaciones de un hotel")> Public Function obten_habitaciones(ByVal idHotel As Integer, ByVal idIdioma As Integer) As DataSet
        Dim res As DataSet
        res = tools_db.getRooms(idHotel, idIdioma)
        Return res
    End Function


    Private Sub add_file(ByVal tbl As String, ByVal tabla_name As String, ByVal zip As Ionic.Utils.Zip.ZipFile, Optional ByVal filter As String = "")
        'obtiene los datos
        Dim ds As DataSet = tools_db.exe_db_fill("SELECT * FROM " + tabla_name + " " + filter)

        'si no hay datos
        If ds Is Nothing Then
            Dim noDat As New System.IO.MemoryStream
            noDat.Write(New Byte() {}, 0, 0)
            zip.AddFileStream(tabla_name + ".xml", "", noDat)
            Return
        End If

        'copia los datos a un datase
        Dim new_ds As New DataSet
        new_ds.Tables.Add(ds.Tables(tbl).Copy)

        'del dataset se obtiene datos y su estructura en xml
        Dim datos() As Byte = System.Text.Encoding.UTF8.GetBytes(new_ds.GetXml)
        Dim dat_stream As New System.IO.MemoryStream
        dat_stream.Write(datos, 0, datos.Length)

        'se agrega directamentew a una archivo en un archivo zip
        zip.AddFileStream(tabla_name + ".xml", "", dat_stream)
    End Sub

    Public Shared Function get_zip_bytes(ByVal ruta_zip As String) As Byte()
        Dim objfilestream As New System.IO.FileStream(ruta_zip, System.IO.FileMode.Open, System.IO.FileAccess.Read)
        Dim len As Integer = CInt(objfilestream.Length)
        Dim documentcontents(len) As Byte
        objfilestream.Read(documentcontents, 0, len)
        objfilestream.Close()

        Return documentcontents
    End Function

End Class